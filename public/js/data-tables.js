const createActionsColumn = (resource, data, includeViewOption = true, includeDeleteOption = true, includeVoidOption = false, approvedOption = false, includeViewProductionOrderOption = false,makeProductionButtons=false, showEditButton=true) => {
    actionTd = '<i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>';
    actionTd += '<div class="dropdown-menu"><ul>';
    if (includeViewOption) {
        actionTd += '<li><a href="' + SITE_URL + '/' + resource + '/' + data.id + '">' + trans('lang.labels.view') + '</a></li>';
    }
    // actionTd += '<li><a href="' + SITE_URL + '/' + resource + '/' + data.id + '/edit">' + trans('lang.labels.edit') + '</a></li>';
    if(showEditButton){
        actionTd += '<li><a href="' + SITE_URL + '/' + resource + '/' + data.id + '/edit">' + trans('lang.labels.edit') + '</a></li>';
    }
    if (includeDeleteOption) {
        actionTd += '<li><a class="delete_resource" data-resource="destroy-' + resource + '-form-' + data.id + '" href="' + SITE_URL + '/' + resource + '/' + data.id + '">' + trans('lang.labels.delete') + '</a><form method="POST" action="' + SITE_URL + '/' + resource + '/' + data.id + '" accept-charset="UTF-8" id="destroy-' + resource + '-form-' + data.id + '" style="display: none"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '"></form></li>';
    }
    if (includeVoidOption) {
        actionTd += '<li><a class="void_resource" data-resource="void-' + resource + '-form-' + data.id + '" href="' + SITE_URL + '/' + resource + '/' + data.id + '/void">' + trans('lang.labels.void') + '</a><form method="POST" action="' + SITE_URL + '/' + resource + '/' + data.id + '/void" accept-charset="UTF-8" id="void-' + resource + '-form-' + data.id + '" style="display: none"><input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '"></form></li>';
    }
    if (approvedOption) {
        actionTd += '<li><a class="approve_resource" data-resource="approve-' + resource + '-form-' + data.id + '" href="' + SITE_URL + '/' + resource + '/' + data.id + '/approve">' + trans('lang.labels.approved') + '</a><form method="POST" action="' + SITE_URL + '/' + resource + '/' + data.id + '/approve" accept-charset="UTF-8" id="approve-' + resource + '-form-' + data.id + '" style="display: none"><input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '"></form></li>';
    }
    if (includeViewProductionOrderOption) {
        actionTd += '<li><a href="' + SITE_URL + '/' + resource + '/' + data.id + '?view=production-order">' + trans('lang.labels.view_production_order') + '</a></li>';
    }
    if(makeProductionButtons){
        actionTd += '<li><a href="' + SITE_URL + '/' + resource + '/' + data.id + '/makeProduction">' + trans('lang.labels.send_to_production') + '</a></li>';
    }
    actionTd += '</ul></div>';
    return actionTd;
}

const zeroRecordsMsg = (resource, resourceName, extraResouce = '', new_label = undefined, new_btn = true) => {
    if (!resource) {
        return '<div class="message"><p>' + trans('lang.messages.no_records_were_found') + '</p></div>';
    }
    new_label = new_label || trans('lang.labels.new');
    resource = resource == 'users' ? 'user-management/'+resource : resource;
    let zeroMessageLabel = resource == 'inventories' ? trans('lang.labels.new_inventory') : new_label + ' ' + resourceName;
    let recordNotCreated = resource == 'inventories' ? trans('lang.labels.you_have_not_yet_created_inventory') : (wordBeginsWithAVowel(resourceName) ? trans('lang.labels.you_have_not_yet_created_an') : trans('lang.labels.you_have_not_yet_created')) + ' ' + resourceName;
    let anchorId = resource == 'inventories' ? 'create-new-product-btn' : null;
    
    // forcefully change the no records found message as per client suggest.
    recordNotCreated = trans('lang.messages.no_records_were_found')
    if (!new_btn) {
        return '<div class="message"><p>' + recordNotCreated + '</p></div><div class="invoice-btns"></div>';
    }

    return '<div class="message"><p>' + recordNotCreated + '</p></div><div class="invoice-btns"><a href="' + SITE_URL + '/' + resource + '/create/' + extraResouce + '" class="btn-custom" id="'+anchorId+'"><i class="fa fa-plus" aria-hidden="true"></i> ' + zeroMessageLabel + ' </a></div>';
}

const wordBeginsWithAVowel = (str) => {
    return /^[aeiou].*/i.test(str);
}

const zeroSearchRecordsMsg = () => '<div class="message"><p>' + trans('lang.labels.no_records_search') + '</p></div>';

const generateEmptyTable = (settings, msg) => {
    var $tbody = $('#' + settings.sTableId + ' tbody');
    $tbody.empty();
    var $tr = $('<tr class="no-data-row" role="row"></tr>');
    $tr.append('<td colspan="' + settings.aoColumns.length + '" rowspan="2" align="center">' + msg + '</td>');
    $tbody.append($tr);
}

var dataTableLanguages = {
    'es': 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json',
    'en': 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/English.json',
}
var language = dataTableLanguages[locale];

let dataTableOptions = {
    "processing": true,
    "lengthChange": true,
    "pageLength": 20,
    "lengthMenu": [
        [10, 20, 50, 100, -1],
        [10, 20, 50, 100, "All"]
    ]
}

if (language) {
    if (typeof language === 'string') {
        dataTableOptions.language = {
            url: language
        };
    } else {
        dataTableOptions.language = language;
    }
}

function addRow(index, data) {
    msg = '<row r="' + index + '">';
    for (i = 0; i < data.length; i++) {
        var key = data[i].k;
        var value = data[i].v;
        msg += '<c t="inlineStr" r="' + key + index + '" s="2">';
        msg += '<is>';
        msg += '<t>' + value + '</t>';
        msg += '</is>';
        msg += '</c>';
    }
    msg += '</row>';
    return msg;
}

const exportExcelPdf = (exportListTable, moduleLabel, customize, visibleColumns = null, needPrintBtn = false) => {
    let buttons = [{
            extend: 'excel',
            text: trans('lang.labels.export_in_excel'),
            title: COMPANY_NAME + ' | ' + trans('lang.labels.' + moduleLabel),
            className: 'btn-custom outline-btn',
            defaultStyle: {
                alignment: 'center',
            },
            filename: appConfig.app_name +" - " + trans('lang.labels.' + moduleLabel) + " " + moment().format('DD-MM-YYYY'),
            exportOptions: {
                modifier: {
                    page: 'all',
                    // search: 'none'
                },
                columns: visibleColumns ? visibleColumns : 'th:not(:last-child)',
                orthogonal: 'export',
                format: {
                    body: function ( data, row, column, node ) {
                       if (false) {
                            //need to change double quotes to single
                            data = data.replace( /"/g, "'" );
                            //split at each new line
                            splitData = data.split('\n');
                            console.log(splitData);
                            data = '';
                            for (i=0; i < splitData.length; i++) {
                                //add escaped double quotes around each line
                                data += '\"' + splitData[i] + '\"';
                                //if its not the last line add CHAR(13)
                                if (i + 1 < splitData.length) {
                                    data += ', CHAR(13), ';
                                }
                            }
                            console.log(data);
                            //Add concat function
                            data = 'CONCATENATE(' + data + ')';
                            data = "Sachin \n Kumar";
                            // data.replace( /<br\s*\/?>/ig, "\n" );
                            return data;
                       }
                       return typeof data == "string" ? data.replace(/(&nbsp;|<([^>]+)>)/ig, "") : data;
                    }
                }
            },
            customize
        },
        {
            extend: 'pdf',
            footer: true,
            text: trans('lang.labels.export_in_pdf'),
            title: COMPANY_NAME + ' | ' + trans('lang.labels.' + moduleLabel),
            className: 'btn-custom outline-btn',
            defaultStyle: {
                alignment: 'center',
            },
            filename: appConfig.app_name + " - " + trans('lang.labels.' + moduleLabel) + " " + moment().format('DD-MM-YYYY'),
            exportOptions: {
                modifier: {
                    page: 'all'
                },
                columns: visibleColumns ? visibleColumns : 'th:not(:last-child)',
            },
            customize: function(doc) {
                //Remove the title created by datatTables
                doc.content.splice(0, 1);
                //Create a date string that we use in the footer. Format is dd-mm-yyyy
                var now = new Date();
                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                // Logo converted to base64
                // var logo = getBase64FromImageUrl('https://datatables.net/media/images/logo.png');
                // The above call should work, but not when called from codepen.io
                // So we use a online converter and paste the string in.
                // Done on http://codebeautify.org/image-to-base64-converter
                // It's a LONG string scroll down to see the rest of the code !!!
                var logo = '';
                // A documentation reference can be found at
                // https://github.com/bpampuch/pdfmake#getting-started
                // Set page margins [left,top,right,bottom] or [horizontal,vertical]
                // or one number for equal spread
                // It's important to create enough space at the top for a header !!!
                doc.pageMargins = [20, 60, 20, 30];
                // Set the font size fot the entire document
                doc.defaultStyle.fontSize = 8;
                // Set the fontsize for the table header
                doc.styles.tableHeader.fontSize = 10;
                // Create a header object with 3 columns
                // Left side: Logo
                // Middle: brandname
                // Right side: A document title
                doc['header'] = (function() {
                    return {
                        columns: [
                            /*{
                                image: logo,
                                width: 24
                            },
                            {
                                alignment: 'left',
                                italics: true,
                                text: 'dataTables',
                                fontSize: 18,
                                margin: [10,0]
                            },*/
                            {
                                alignment: 'center',
                                fontSize: 14,
                                text: appConfig.app_name + ' | ' + trans('lang.labels.' + moduleLabel)
                            }
                        ],
                        margin: 20
                    }
                });
                // Create a footer object with 2 columns
                // Left side: report creation date
                // Right side: current page and total pages
                doc['footer'] = (function(page, pages) {
                    return {
                        columns: [
                            /*{
                                alignment: 'left',
                                text: ['Created on: ', { text: jsDate.toString() }]
                            },*/
                            {
                                alignment: 'right',
                                text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                            }
                        ],
                        margin: 20
                    }
                });
                // Change dataTable layout (Table styling)
                // To use predefined layouts uncomment the line below and comment the custom lines below
                // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                var objLayout = {};
                objLayout['hLineWidth'] = function(i) { return .5; };
                objLayout['vLineWidth'] = function(i) { return .5; };
                objLayout['hLineColor'] = function(i) { return '#aaa'; };
                objLayout['vLineColor'] = function(i) { return '#aaa'; };
                objLayout['paddingLeft'] = function(i) { return 4; };
                objLayout['paddingRight'] = function(i) { return 4; };
                doc.content[0].layout = objLayout;
            },
            /*customize: function(win) {
                var tbody = $(document).find( 'table tbody' );
                $(tbody).append($(tbody).find('tr:eq(0)').clone());
                var row = $(tbody).find('tr').last();
                $(row).find('td').text('');
                $(row).find('td:eq(0)').text('');
                $(row).find('td:eq(1)').text('');
                win['footer'] = (function (page, pages) {
                    return {
                        columns: [
                                UsuarioData(),
                            {
                                // This is the right column
                                alignment: 'right',
                                text: ['Página ', { text: page.toString() }, ' de ', { text: pages.toString() }]
                            }
                        ],
                        //margin: [10, 0]
                    }
                });
            }*/
        },
    ];
    if (needPrintBtn) {
        let printCustom = {
            extend: 'print',
            text: trans('lang.labels.print'),
            autoPrint: true,
            title: COMPANY_NAME + ' | ' + trans('lang.labels.' + moduleLabel),
            className: 'btn-custom outline-btn',
            defaultStyle: {
                alignment: 'center',
            },
            filename: appConfig.app_name +" - " + trans('lang.labels.' + moduleLabel) + " " + moment().format('DD-MM-YYYY'),
            exportOptions: {
                modifier: {
                    page: 'all',
                    // search: 'none'
                },
                columns: visibleColumns ? visibleColumns : 'th:not(:last-child)',
                orthogonal: 'print',
            },
            header: true,
            footer: true,
            customize: function (win, config, api) {
                // console.log(config)
                // console.log(api)
                // console.log(api.column( 4 ).data().sum())
                let baseTotal = api.column( 4, {page:'all', search: 'applied'} ).data().sum();
                let paidTotal = api.column( 5, {page:'all', search: 'applied'} ).data().sum();
                let pendingTotal = api.column( 6, {page:'all', search: 'applied'} ).data().sum();
                let taxTotal = api.column( 7, {page:'all', search: 'applied'} ).data().sum();
                let discountTotal = api.column( 8, {page:'all', search: 'applied'} ).data().sum();
                /*$(win.document.body).find('table').addClass('display').css('font-size', '9px');
                $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                    $(this).css('background-color','#D0D0D0');
                });
                $(win.document.body).find('h1').css('text-align','center');*/
                let totalTr = `<tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>${trans('lang.labels.total')}</b></td>
                        <td><b>${moneyFormat(baseTotal, true)}</b></td>
                        <td><b>${moneyFormat(paidTotal, true)}</b></td>
                        <td><b>${moneyFormat(pendingTotal, true)}</b></td>
                        <td><b>${moneyFormat(taxTotal, true)}</b></td>
                        <td><b>${moneyFormat(discountTotal, true)}</b></td>
                        <td></td>
                        <td></td>
                        </tr>
                `;
                $(win.document.body).find('table > tbody').append(totalTr);
            }
            // message: "<h1>Here</h1>"
        };
        buttons.push(printCustom);
    }
    return new $.fn.dataTable.Buttons(exportListTable, {
        buttons
        // dom: 'Bfrtip',
    }).container().appendTo($('#export-buttons'));
    //return exportTableBtn;
}


const dateFilter = (fieldNumber) => {
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var min = $('#date-filter-min').datepicker("getDate");
            var max = $('#date-filter-max').datepicker("getDate");
            if (locale == "es") {
                date_format = "DD/MM/YYYY"
            } else {
                date_format = "MM/DD/YYYY"
            }
            var startDate = moment(data[fieldNumber], date_format).toDate()
            // var startDate = new Date(data[fieldNumber]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true; }
            if (max == null && startDate >= min) { return true; }
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
    );
}

$(document).ready(function() {
    $('#clients-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/clients",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "tax_id", "searchable": true,  render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "phone",  "searchable": true, render: function(data, type, row) { return showIfAvailable(data) } },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('clients', data);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('clients', trans('lang.labels.client'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#suppliers-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/suppliers",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "tax_id", "searchable": true, "orderable": false, render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "phone", "orderable": false, "searchable": true, render: function(data, type, row) { return showIfAvailable(data) } },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('suppliers', data);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('suppliers', trans('lang.labels.supplier'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#taxes-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/taxes",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "percentage" },
            { "data": "description", "orderable": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('taxes', data, false);
                }
            },
        ],
        columnDefs: [
            { className: "dt-left", targets: [2] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('taxes', trans('lang.labels.tax'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#item-categories-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/item-categories",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "description", "orderable": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('item-categories', data, false);
                }
            },
        ],
        columnDefs: [
            { className: "dt-left", targets: [1] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('item-categories', trans('lang.labels.item_category')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        },
        "language": {
            // zeroRecords: '<div class="message"><p>There is no records match with your searching</p></div>',
            // emptyTable: '<div class="message"><p>You have not yet created a category!</p></div><div class="invoice-btns"><a href="'+SITE_URL+'/item-categories/create" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> New category </a></div>'
        },
        "oLanguage": {
            // "sZeroRecords": '<div class="message"><p>You have not yet created a category!</p></div><div class="invoice-btns"><a href="'+SITE_URL+'/item-categories/create" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> New Item Category </a></div>' 
        }
    }));

    $('#inventory-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/inventories",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "reference", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "sale_price", render: function(data, type, row) { return moneyFormat(data, true) } },
            { "data": "cost", render: function(data, type, row) { return data ? moneyFormat(data, true) : showIfAvailable(data) } },
            { "data": "quantity", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "description", "orderable": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('inventories', data);
                }
            },
            { "data": "item_category.name", render: function(data, type, row) { return showIfAvailable(data) }, "searchable": true, "visible": false },
        ],
        columnDefs: [
            { className: "dt-left", targets: [5] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('inventories', trans('lang.labels.inventory'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    if ($('#invoice-list-table').length) {
        let invoiceListTable = $('#invoice-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $(".invoice-list-table_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#invoice-list-table_filter');
                $("#dates-filters").show();
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html('$' + 121);
            },
            "ajax": {
                // "url": SITE_URL + "/invoices",
                "url": location.href,
                "contentType": "application/json",
                "type": "GET"
            },
            "deferRender": true,
            columns: [
                { "data": "internal_number" },
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "expiration_date", render: function(data, type, row) { return dateFormat(data) } },
                {
                    "data": "calculation.total",
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data :
                            moneyFormat(data, true)
                    }
                },
                {
                    "data": "calculation.paidAmount",
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data :
                            moneyFormat(data, true)
                    }
                },
                {
                    "data": "calculation.pendingAmount",
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data :
                            moneyFormat(data, true)
                    }
                },
                {
                    "data": "calculation.taxAmount",
                    "orderable": false,
                    "searchable": false,
                    "visible": false,
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data :
                            moneyFormat(data, true)
                    }
                },
                {
                    "data": "calculation.discountAmount",
                    "orderable": false,
                    "searchable": false,
                    "visible": false,
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data :
                            moneyFormat(data, true)
                    }
                },
                // { "data": "contact.phone", "orderable": false, "searchable": false, "visible": false },
                // { "data": "contact.tax_id", "orderable": false, "searchable": false, "visible": false },
                { "data": "term_in_locale", "orderable": false, "searchable": false, "visible": false },
                { "data": "calculation.status", "orderable": true, "searchable": true },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('invoices', data, true, true, true, false, false,true,false,true);
                    }
                },
                { "data": "notes", "searchable": true, "visible": false },
                { "data": "tac", "searchable": true, "visible": false },
                { "data": "privacy_policy", "searchable": true, "visible": false },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('invoices', trans('lang.labels.invoice'), extraResouce));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                    invoiceListTable.buttons('.buttons-html5').enable();
                } else {
                    invoiceListTable.buttons('.buttons-html5').disable();
                    $('.dataTables_paginate').hide();
                }
            }
        }));

        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfInvoiceTotal = 0;
            var sumOfInvoicePaid = 0;
            var sumOfInvoicePending = 0;
            var sumOfInvoiceTax = 0;
            var sumOfInvoiceDiscount = 0;
            // Loop over the cells in column `F`
            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoiceTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="F"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoicePaid += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="G"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoicePending += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="I"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoiceTax += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="H"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoiceDiscount += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });
            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,

            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOfInvoiceTotal, true)
            }]);
            var r3 = addRow(rowNumOfTotal, [{
                k: 'F',
                v: moneyFormat(sumOfInvoicePaid, true)
            }]);
            var r4 = addRow(rowNumOfTotal, [{
                k: 'G',
                v: moneyFormat(sumOfInvoicePending, true)
            }]);
            var r5 = addRow(rowNumOfTotal, [{
                k: 'I',
                v: moneyFormat(sumOfInvoiceTax, true)
            }]);
            var r6 = addRow(rowNumOfTotal, [{
                k: 'H',
                v: moneyFormat(sumOfInvoiceDiscount, true)
            }]);
            var r7 = addRow(1, [{
                k: 'L',
                v: trans('lang.labels.date') + ": " + moment().format('DD-MM-YYYY')
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2 + r3 + r4 + r5 + r6;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            /* $('row c[r^="I"]', sheet).each(formatColumnAndRow);
            $('row c[r^="J"]', sheet).each(formatColumnAndRow);
            $('row c[r^="K"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            } */
        }
        exportExcelPdf(invoiceListTable, 'invoices', customize, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], true);
        dateFilter(2);
        var start = moment().subtract(29, 'days');
        var end = moment();
        $('#filter').daterangepicker({
            autoUpdateInput: false,
            startDate: start,
            endDate: end,
            ranges: {
                [`${trans('lang.labels.paid')}`] : [moment().subtract(2, 'days'), moment().subtract(2, 'days')],
                [`${trans('lang.labels.pending')}`]: [moment().subtract(3, 'days'), moment().subtract(3, 'days')],
                [`${trans('lang.labels.overdue')}`]: [moment().subtract(4, 'days'), moment().subtract(4, 'days')],
                [`${trans('lang.labels.draft')}`]: [moment().subtract(5, 'days'), moment().subtract(5, 'days')],
                [`${trans('lang.labels.today')}`]: [moment(), moment()],
                [`${trans('lang.labels.yesterday')}`]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                [`${trans('lang.labels.last_7_days')}`]: [moment().subtract(6, 'days'), moment()],
                [`${trans('lang.labels.this_month')}`]: [moment().startOf('month'), moment().endOf('month')],
                [`${trans('lang.labels.last_month')}`]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                [`${trans('lang.labels.last_30_days')}`]: [moment().subtract(29, 'days'), moment()],
                [`${trans('lang.labels.last_60_days')}`]: [moment().subtract(59, 'days'), moment()],
                [`${trans('lang.labels.last_90_days')}`]: [moment().subtract(89, 'days'), moment()],
                [`${trans('lang.labels.last_180_days')}`]: [moment().subtract(179, 'days'), moment()],
            }
        },
        function (start, end, label) {
        var s = moment(start.toISOString());
        var e = moment(end.toISOString());
        $('#filter').val(label);
        if(label == 'Paid' || label == 'Pending' || label == 'Overdue' || label == 'Draft'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            invoiceListTable.search( label ).draw();
        } else if (label == 'Pagado' || label == 'Pendiente' || label == 'Atrasada' || label == 'Sequía'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            invoiceListTable.search( label ).draw();
        }else{
            invoiceListTable.search( "" ).draw();
            $('#date-filter-min').datepicker("setDate", new Date(s));
            $('#date-filter-max').datepicker("setDate", new Date(e));
        }
        });
        $('#date-filter-min, #date-filter-max').change(function() {
            invoiceListTable.draw();
        });
    }

    
    if ($('#account-recivable-list-table').length) {
        let accountRecivableListTable = $('#account-recivable-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $(".invoice-list-table_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#account-recivable-list-table_filter');
                $("#dates-filters").show();
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html('$' + 121);
            },
            "ajax": {
                // "url": SITE_URL + "/invoices",
                "url": location.href,
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                 { "data": "id" },
                { "data": "contact" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "expiration_date", render: function(data, type, row) { return dateFormat(data) } },
                {
                    "data": "total",
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data : moneyFormat(data, true)
                    }
                },
                { "data": "item_paid" },
                { "data": "item_pending", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "status", "sClass": "desc-td", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('accountAdjustments', data);
                    }
                },
            ],
            drawCallback: function(settings) {
              
                var api = this.api();
                  console.log(api);
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('receivable', trans('lang.labels.account_receivable')));
                }
                

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
         

        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfInvoiceTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoiceTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,

            });

            function addRow(index, data) {
                msg = '<row r="' + index + '">';
                for (i = 0; i < data.length; i++) {
                    var key = data[i].k;
                    var value = data[i].v;
                    msg += '<c t="inlineStr" r="' + key + index + '" s="2">';
                    msg += '<is>';
                    msg += '<t>' + value + '</t>';
                    msg += '</is>';
                    msg += '</c>';
                }
                msg += '</row>';
                return msg;
            }
            console.log(msg);
            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOfInvoiceTotal)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            $('row c[r^="I"]', sheet).each(formatColumnAndRow);
            $('row c[r^="J"]', sheet).each(formatColumnAndRow);
            $('row c[r^="K"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            }
         }
         exportExcelPdf(accountRecivableListTable, 'accountAdjustments');
         dateFilter(2);
        $('#date-filter-min, #date-filter-max').change(function() {

            accountRecivableListTable.draw();
        });
    }
    if ($('#recurring-invoice-list-table').length) {
        let recurringInvoiceListTable = $('#recurring-invoice-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#invoice-list-table_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#recurring-invoice-list-table_filter');
                $("#dates-filters").show();
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html('$' + 121);
            },
            "ajax": {
                // "url": SITE_URL + "/invoices",
                "url": location.href,
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "internal_number" },
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "end_date", render: function(data, type, row) { return dateFormat(data) } },
                {
                    "data": "calculation.total",
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data : moneyFormat(data, true)
                    }
                },
                { "data": "frequency" },
                { "data": "frequency_type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "observation", "sClass": "desc-td", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('invoices', data);
                    }
                },
                { "data": "notes", "searchable": true, "visible": false },
                { "data": "tac", "searchable": true, "visible": false },
                { "data": "privacy_policy", "searchable": true, "visible": false },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('invoices', trans('lang.labels.recurring_invoice'), extraResouce));
                }
                

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            },
            fnRowCallback : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
            },
        }));
         

        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfInvoiceTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfInvoiceTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,

            });

            function addRow(index, data) {
                msg = '<row r="' + index + '">';
                for (i = 0; i < data.length; i++) {
                    var key = data[i].k;
                    var value = data[i].v;
                    msg += '<c t="inlineStr" r="' + key + index + '" s="2">';
                    msg += '<is>';
                    msg += '<t>' + value + '</t>';
                    msg += '</is>';
                    msg += '</c>';
                }
                msg += '</row>';
                return msg;
            }

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOfInvoiceTotal)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            $('row c[r^="I"]', sheet).each(formatColumnAndRow);
            $('row c[r^="J"]', sheet).each(formatColumnAndRow);
            $('row c[r^="K"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            }
        }
        exportExcelPdf(recurringInvoiceListTable, 'recurring_invoices', customize, [0, 1, 2, 3, 4, 5, 6, 7]);
        dateFilter(2);
        $('#date-filter-min, #date-filter-max').change(function() {
            recurringInvoiceListTable.draw();
        });
    }
    
    if ($('#supplier-invoice-list-table').length) {
        let supplierInvoiceListTable = $('#supplier-invoice-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#invoice-list-table_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#supplier-invoice-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                // "url": SITE_URL + "/invoices",
                "url": location.href,
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "internal_number" },
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "expiration_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "calculation.total", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.paidAmount", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.supplierPendingAmount", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.status", "orderable": false, "searchable": true },
                { "data": "calculation.taxAmount", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.discountAmount", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('invoices', data, true, true, true);
                    }
                },
                { "data": "notes", "searchable": true, "visible": false },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('invoices', trans('lang.labels.invoice'), extraResouce));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfSupplierInvoiceTotal = 0;
            var sumOfSupplierInvoiceTax = 0;
            var sumOfSupplierInvoiceDiscount = 0;
            // Loop over the cells in column `F`
            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfSupplierInvoiceTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="I"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfSupplierInvoiceTax += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="J"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfSupplierInvoiceDiscount += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });
            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,

            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOfSupplierInvoiceTotal)
            }]);
            var r3 = addRow(rowNumOfTotal, [{
                k: 'I',
                v: moneyFormat(sumOfSupplierInvoiceTax)
            }]);
            var r4 = addRow(rowNumOfTotal, [{
                k: 'J',
                v: moneyFormat(sumOfSupplierInvoiceDiscount)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2 + r3 + r4;
        }
        exportExcelPdf(supplierInvoiceListTable, 'supplier_invoices', customize, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        dateFilter(2);
        var start = moment().subtract(29, 'days');
        var end = moment();
        $('#supplier-filter').daterangepicker({
            autoUpdateInput: false,
            startDate: start,
            endDate: end,
            ranges: {
                [`${trans('lang.labels.paid')}`] : [moment().subtract(2, 'days'), moment().subtract(2, 'days')],
                [`${trans('lang.labels.pending')}`]: [moment().subtract(3, 'days'), moment().subtract(3, 'days')],
                [`${trans('lang.labels.overdue')}`]: [moment().subtract(4, 'days'), moment().subtract(4, 'days')],
                [`${trans('lang.labels.draft')}`]: [moment().subtract(5, 'days'), moment().subtract(5, 'days')],
                [`${trans('lang.labels.today')}`]: [moment(), moment()],
                [`${trans('lang.labels.yesterday')}`]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                [`${trans('lang.labels.last_7_days')}`]: [moment().subtract(6, 'days'), moment()],
                [`${trans('lang.labels.this_month')}`]: [moment().startOf('month'), moment().endOf('month')],
                [`${trans('lang.labels.last_month')}`]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                [`${trans('lang.labels.last_30_days')}`]: [moment().subtract(29, 'days'), moment()],
                [`${trans('lang.labels.last_60_days')}`]: [moment().subtract(59, 'days'), moment()],
                [`${trans('lang.labels.last_90_days')}`]: [moment().subtract(89, 'days'), moment()],
                [`${trans('lang.labels.last_180_days')}`]: [moment().subtract(179, 'days'), moment()],
            }
        },
        function (start, end, label) {
        var s = moment(start.toISOString());
        var e = moment(end.toISOString());
        $('#supplier-filter').val(label);
        if(label == 'Paid' || label == 'Pending' || label == 'Overdue' || label == 'Draft'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            supplierInvoiceListTable.search( label ).draw();
        } else if (label == 'Pagado' || label == 'Pendiente' || label == 'Atrasada' || label == 'Sequía'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            supplierInvoiceListTable.search( label ).draw();
        }else{
            supplierInvoiceListTable.search( "" ).draw();
            $('#date-filter-min').datepicker("setDate", new Date(s));
            $('#date-filter-max').datepicker("setDate", new Date(e));
        }
        });

        $('#date-filter-min, #date-filter-max').change(function() {
            supplierInvoiceListTable.draw();
        });
    }

    if ($('#credit-notes-list-table').length) {
        let creditNoteListTable = $('#credit-notes-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#credit-notes-list_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#credit-notes-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/credit-notes",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "contact", render: function(data, type, row) { return data ? showIfAvailable(data.name) : showIfAvailable() } },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "remaining", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "total", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.taxAmount", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.discountAmount", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "notes", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" } },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('credit-notes', data);
                    }
                },
                { "data": "notes", "searchable": true, "visible": false },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('credit-notes', trans('lang.labels.credit_note')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfCreditNoteTotal = 0;
            var sumOfCreditNoteTax = 0;
            var sumOfCreditNoteDiscount = 0;
            var sumOfCreditNoteRemainingTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="C"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfCreditNoteRemainingTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="D"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfCreditNoteTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfCreditNoteTax += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="F"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfCreditNoteDiscount += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });
            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,

            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'B',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'C',
                v: moneyFormat(sumOfCreditNoteRemainingTotal)
            }]);
            var r3 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: moneyFormat(sumOfCreditNoteTotal)
            }]);
            var r4 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOfCreditNoteTax)
            }]);
            var r5 = addRow(rowNumOfTotal, [{
                k: 'F',
                v: moneyFormat(sumOfCreditNoteDiscount)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2 + r3 + r4 + r5;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            $('row c[r^="E"]', sheet).each(formatColumnAndRow);
            $('row c[r^="F"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            }
        }
        exportExcelPdf(creditNoteListTable, 'credit_notes', customize, [0, 1, 2, 3, 4, 5, 6]);
        dateFilter(1);
        $('#date-filter-min, #date-filter-max').change(function() {
            creditNoteListTable.draw();
        });
    }

    if ($('#estimates-list-table').length) {
        let estimateListTable = $('#estimates-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#estimates-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/estimates",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "internal_number" },
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "calculation.total", render: function(data, type, row) { return moneyFormat(data, true) } },
                { "data": "status" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('estimates', data, true, true, false, false);
                    }
                },
                { "data": "notes", "searchable": true, "visible": false },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('estimates', trans('lang.labels.estimate')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        dateFilter(2);
        var start = moment().subtract(29, 'days');
        var end = moment();
        // var paid = 'Paid';
        $('#estimates-filter').daterangepicker({
            autoUpdateInput: false,
            startDate: start,
            endDate: end,
            ranges: {
                // 'Paid': 'Paid',
                // 'Pending': 'Pending',
                // 'Overdue': 'Overdue',
                // 'Draft': 'Draft',
                [`${trans('lang.labels.expired')}`]: [moment().subtract(2, 'days'), moment().subtract(2, 'days')],
                [`${trans('lang.labels.active')}`]: [moment().subtract(3, 'days'), moment().subtract(3, 'days')],
                [`${trans('lang.labels.today')}`]: [moment(), moment()],
                [`${trans('lang.labels.yesterday')}`]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                [`${trans('lang.labels.last_7_days')}`]: [moment().subtract(6, 'days'), moment()],
                [`${trans('lang.labels.this_month')}`]: [moment().startOf('month'), moment().endOf('month')],
                [`${trans('lang.labels.last_month')}`]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                [`${trans('lang.labels.last_30_days')}`]: [moment().subtract(29, 'days'), moment()],
                [`${trans('lang.labels.last_60_days')}`]: [moment().subtract(59, 'days'), moment()],
                [`${trans('lang.labels.last_90_days')}`]: [moment().subtract(89, 'days'), moment()],
                [`${trans('lang.labels.last_180_days')}`]: [moment().subtract(179, 'days'), moment()],
            }
        },
        function (start, end, label) {
        var s = moment(start.toISOString());
        var e = moment(end.toISOString());
        $('#estimates-filter').val(label);
        if(label == 'Expired' || label == 'Active'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            estimateListTable.search( label ).draw();
        }else if (label == 'Caducada' || label == 'Activa'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            estimateListTable.search( label ).draw();
        }else{
            estimateListTable.search( "" ).draw();
            $('#date-filter-min').datepicker("setDate", new Date(s));
            $('#date-filter-max').datepicker("setDate", new Date(e));
        }
        });
        $('#date-filter-min, #date-filter-max').change(function() {
            estimateListTable.draw();
        });
    }

    if ($('#debit-notes-list-table').length) {
        let debitNoteListTable = $('#debit-notes-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#debit-notes-list_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#debit-notes-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/debit-notes",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "contact", render: function(data, type, row) { return data ? showIfAvailable(data.name) : showIfAvailable() } },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "remaining", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "total", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.taxAmount", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "calculation.discountAmount", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "notes", "orderable": false, "searchable": false, "visible": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" } },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('debit-notes', data);
                    }
                },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('debit-notes', trans('lang.labels.debit_note')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfDebitNoteTotal = 0;
            var sumOfDebitNoteTax = 0;
            var sumOfDebitNoteDiscount = 0;

            // Loop over the cells in column `F`
            $('row c[r^="C"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfDebitNoteTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="D"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfDebitNoteTax += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfDebitNoteDiscount += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });
            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,
            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'B',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'C',
                v: moneyFormat(sumOfDebitNoteTotal)
            }]);
            var r3 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: moneyFormat(sumOfDebitNoteTax)
            }]);
            var r4 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOfDebitNoteDiscount)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2 + r3 + r4;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            $('row c[r^="F"]', sheet).each(formatColumnAndRow);
            $('row c[r^="G"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            }
        }
        exportExcelPdf(debitNoteListTable, 'debit_notes', customize, [0, 1, 2, 3, 4, 5, 6]);
        dateFilter(1);
        $('#date-filter-min, #date-filter-max').change(function() {
            debitNoteListTable.draw();
        });
    }

    if ($('#purchase-order-list-table').length) {
        let purchaseOrderListTable = $('#purchase-order-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#purchase-order-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/purchase-orders",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "expiration_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "calculation.total", render: function(data, type, row) { return moneyFormat(data, true) } },
                { "data": "status"},
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('purchase-orders', data, true, true, false);
                    }
                }
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('purchase-orders', trans('lang.labels.purchase_order')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        dateFilter(1);
        $('#date-filter-min, #date-filter-max').change(function() {
            purchaseOrderListTable.draw();
        });
    }

    if ($('#transactions-received-list-table').length) {
        let transactionReceivedListTable = $('#transactions-received-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#transactions-received-list_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#transactions-received-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/transactions/in",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "receipt_number" },
                { "data": "contact.name" },
                { "data": "detail_line" ,"sClass": "descrip-notes", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" } },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "bankAccount", render: function(data, type, row) { return data ? showIfAvailable(data.name[locale]) : showIfAvailable() } },
                {
                    "data": "amount",
                    render: function(data, type, row) {
                        return type === 'export' ?
                            data : moneyFormat(data, true)
                    }
                },
                { "data": "annotation", "sClass": "desc-td", "orderable": false, "searchable": true, "visible": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('transactions', data);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('transactions', trans('lang.labels.received_payment'), extraResouce, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfTransactionReceivedTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="F"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfTransactionReceivedTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,
            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'F',
                v: moneyFormat(sumOfTransactionReceivedTotal)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            /* $('row c[r^="E"]', sheet).each(formatColumnAndRow);
            $('row c[r^="F"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            } */
        }
        exportExcelPdf(transactionReceivedListTable, 'received_payments', customize);
        dateFilter(3);
        $('#date-filter-min, #date-filter-max').change(function() {
            transactionReceivedListTable.draw();
        });
    }

    if ($('#transactions-out-list-table').length) {
        let transactionOutListTable = $('#transactions-out-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#transactions-out-list_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#transactions-out-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/transactions/out",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "voucher_number" },
                { "data": "contact", render: function(data, type, row) { return data ? showIfAvailable(data.name) : showIfAvailable() } },
                { "data": "detail_line" ,"sClass": "descrip-notes", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" } },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "bankAccount", render: function(data, type, row) { return data ? showIfAvailable(data.name[locale]) : showIfAvailable() } },
                { "data": "amount", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "annotation", "sClass": "desc-td", "orderable": true, "searchable": false, "visible": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('transactions', data);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('transactions', trans('lang.labels.payment'), extraResouce, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfTransactionPaymentsTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="F"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfTransactionPaymentsTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,
            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'F',
                v: moneyFormat(sumOfTransactionPaymentsTotal)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2;

            $('row c[r^="G"]', sheet).each(function(i, v) {
                // console.log(v);
                // $(this).val("test")
                // $(this).text("test");
                // $(this).text($(this).text().replace(/(&nbsp;|<([^>]+)>)/ig, ""))
                // console.log();
                // console.log($(this).text());
            });

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            /* $('row c[r^="E"]', sheet).each(formatColumnAndRow);
            $('row c[r^="F"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            } */
        }
        exportExcelPdf(transactionOutListTable, 'payments', customize);
        dateFilter(3);
        $('#date-filter-min, #date-filter-max').change(function() {
            transactionOutListTable.draw();
        });
    }

    if ($('#transactions-recurring-list-table').length) {
        let transactionRecurringListTable = $('#transactions-recurring-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#transactions-recurring-list_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#transactions-recurring-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/transactions/recurring",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "id" },
                { "data": "contact", render: function(data, type, row) { return data ? showIfAvailable(data.name) : showIfAvailable() } },
                { "data": "bankAccount", render: function(data, type, row) { return data ? showIfAvailable(data.name[locale]) : showIfAvailable() } },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "end_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "frequency" },
                { "data": "frequency_type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "amount", render: function(data, type, row) { return type === 'export' ? data : moneyFormat(data, true) } },
                { "data": "observation", "sClass": "desc-td", "orderable": false, "searchable": true, "visible": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('transactions', data);
                    }
                },
                { "data": "detail_line", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('transactions', trans('lang.labels.recurring_payment'), extraResouce, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            },
            fnRowCallback : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
            },
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOfRecurringPaymentTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="H"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOfRecurringPaymentTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,
            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'G',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'H',
                v: moneyFormat(sumOfRecurringPaymentTotal)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            /* $('row c[r^="E"]', sheet).each(formatColumnAndRow);
            $('row c[r^="F"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            } */
        }
        exportExcelPdf(transactionRecurringListTable, 'recurring_payments', customize, [0, 1, 2, 3, 4, 5, 6, 7, 8]);
        dateFilter(3);
        $('#date-filter-min, #date-filter-max').change(function() {
            transactionRecurringListTable.draw();
        });
    }

    $('#inventory-adjustments-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/inventory-adjustments",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "sr_no" },
            { "data": "adjustment_date", render: function(data, type, row) { return dateFormat(data) } },
            { "data": "total", render: function(data, type, row) { return moneyFormat(data, true) } },
            { "data": "observations", "sClass": "desc-td", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('inventory-adjustments', data, true);
                }
            },
        ],
        columnDefs: [
            { className: "dt-left", targets: [3] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('inventory-adjustments', trans('lang.labels.inventory_adjustments')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#accounting-adjustments-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/accounting/adjustments",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "sr_no" },
            { "data": "reference", "sClass": "desc-td", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            { "data": "adjustment_date", render: function(data, type, row) { return dateFormat(data) } },
            { "data": "observations", "sClass": "desc-td", render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            { "data": "total", render: function(data, type, row) { return moneyFormat(data, true) } },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('accounting/adjustments', data, true);
                }
            },
        ],
        columnDefs: [
            { className: "dt-left", targets: [3] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('accounting/adjustments', trans('lang.labels.accounting_adjustments')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#ncfs-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/ncfs",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "initial_number" },
            { "data": "description", "orderable": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('ncfs', data, false);
                }
            },
        ],
        columnDefs: [
            { className: "dt-left", targets: [2] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('ncfs', trans('lang.labels.ncfs'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#user-currency-rates-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/currency-exchange-rates",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "currency", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "rate", render: function(data, type, row) { return moneyFormat(data, true) } },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg());
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#users-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/user-management/users",
            "contentType": "application/json",
            "type": "GET"
        },
        "aoColumns": [
           { "bSortable": true },
           { "bSortable": true },
           { "bSortable": true },
           { "bSortable": false }
        ],
        "columns": [
            { "data": "name", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "email", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "role", render: function(data, type, row) { return showIfAvailable(data) } },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('user-management/users', data, false);
                }
            },
        ],
        columnDefs: [
            // { className: "dt-center", targets: [4] },
            { targets: 3, className: 'to-show' }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('users', trans('lang.labels.users')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#admin-users-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/admin/users",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "email", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "account_verified", render: function(data, type, row) { return showIfAvailable(data) } },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));
   
    if ($('#remissions-list-table').length) {
        let estimateListTable = $('#remissions-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#remissions-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/remissions",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "internal_number" },
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "calculation.total", render: function(data, type, row) { return moneyFormat(data, true) } },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('remissions', data, true, true, false, false);
                    }
                },
                { "data": "notes", "searchable": true, "visible": false },
                { "data": "contact.phone", "searchable": true, "visible": false },
                { "data": "calculation.baseTotal", "searchable": true, "visible": false },
                { "data": "contact.tax_id", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('remissions', trans('lang.labels.remission'), undefined, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        dateFilter(2);
        var start = moment().subtract(29, 'days');
        var end = moment();
        // var paid = 'Paid';
        $('#remissions-filter').daterangepicker({
            autoUpdateInput: false,
            startDate: start,
            endDate: end,
            ranges: {
                // 'Paid': 'Paid',
                // 'Pending': 'Pending',
                // 'Overdue': 'Overdue',
                // 'Draft': 'Draft',
                [`${trans('lang.labels.today')}`]: [moment(), moment()],
                [`${trans('lang.labels.yesterday')}`]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                [`${trans('lang.labels.last_7_days')}`]: [moment().subtract(6, 'days'), moment()],
                [`${trans('lang.labels.this_month')}`]: [moment().startOf('month'), moment().endOf('month')],
                [`${trans('lang.labels.last_month')}`]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                [`${trans('lang.labels.last_30_days')}`]: [moment().subtract(29, 'days'), moment()],
                [`${trans('lang.labels.last_60_days')}`]: [moment().subtract(59, 'days'), moment()],
                [`${trans('lang.labels.last_90_days')}`]: [moment().subtract(89, 'days'), moment()],
                [`${trans('lang.labels.last_180_days')}`]: [moment().subtract(179, 'days'), moment()],
            }
        },
        function (start, end, label) {
        var s = moment(start.toISOString());
        var e = moment(end.toISOString());
        $('#remissions-filter').val(label);
        if(label == 'Paid' || label == 'Pending' || label == 'Overdue' || label == 'Draft'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            estimateListTable.search( label ).draw();
        }else{
            estimateListTable.search( "" ).draw();
            $('#date-filter-min').datepicker("setDate", new Date(s));
            $('#date-filter-max').datepicker("setDate", new Date(e));
        }
        });
        $('#date-filter-min, #date-filter-max').change(function() {
            estimateListTable.draw();
        });
    }

    $('#countries-list-table').DataTable(Object.assign({}, dataTableOptions));    
    $('#sectors-list-table').DataTable(Object.assign({}, dataTableOptions));
    $('#currencies-list-table').DataTable(Object.assign({}, dataTableOptions));
    $('#identification-types-list-table').DataTable(Object.assign({}, dataTableOptions));

    $('#employees-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/employees",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "id_number", "orderable": true, "searchable": true, render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "hiring_date", "orderable": false, "searchable": true, render: function(data, type, row) { return dateFormat(data) } },
            { "data": "salary", "orderable": false, "searchable": true, render: function(data, type, row) { return moneyFormat(data) } },
            { "data": "phone", "orderable": false, "searchable": true, render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "role", "orderable": false, "searchable": true, render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "schedules" },
            { "data": "status" , render: function(data, type, row) { return trans('lang.labels.' + data) } },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('employees', data);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('employees', trans('lang.labels.employee'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#departments-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/departments",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "custom_name" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('departments', data, false);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('departments', trans('lang.labels.department'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#designations-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/designations",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "custom_name" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('designations', data, false);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('designations', trans('lang.labels.designation'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#insurance-brands-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/insurance-brands",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "custom_name" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('insurance-brands', data, false);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('insurance-brands', trans('lang.labels.insurance_brand'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#leave-types-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/leave-types",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "custom_name" },
            { "data": "number_of_days" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('leave-types', data, false);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('leave-types', trans('lang.labels.leave_type'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#leave-applications-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/leave-applications",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "leave_type" },
            { "data": "request_duration" },
            { "data": "number_of_days" },
            { "data": "status" },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('leave-types', trans('lang.labels.leave_type'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));
    
    if ($('#leave-reports-list-table').length) {
        let leaveReportsListTable = $('#leave-reports-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#leave-reports-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/leave-reports",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "employee" },
                { "data": "leave_type" },
                { "data": "application_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "request_duration" },
                { "data": "approve_by", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "approve_date", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "purpose" },
                { "data": "number_of_days" },
                { "data": "from_date", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('leave-report', trans('lang.labels.leave_reports'), undefined, trans('lang.labels.new_label'), false));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        dateFilter(9);
        $('#date-filter-min, #date-filter-max').change(function() {
            leaveReportsListTable.draw();
        });
    }

    if ($('#summary-report-list-table').length) {
        let leaveReportsListTable = $('#summary-report-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#summary-report-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/leave-reports",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "leave_type" },
                { "data": "number_of_days" },
                { "data": "remaining_leaves" },
                { "data": "taken_leaves" },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('leave-types', trans('lang.labels.summary_report'), undefined, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        dateFilter(0);
        $('#date-filter-min, #date-filter-max').change(function() {
            leaveReportsListTable.draw();
        });
    }

    if ($('#my-leave-reports-list-table').length) {
        let leaveReportsListTable = $('#my-leave-reports-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#my-leave-reports-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/leave-reports",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "leave_type" },
                { "data": "application_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "request_duration" },
                { "data": "approve_by", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "approve_date", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "purpose" },
                { "data": "number_of_days" },
                { "data": "from_date", "searchable": true, "visible": false },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('leave-types', trans('lang.labels.leave_reports'), undefined, trans('lang.labels.new_label'), false));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        dateFilter(8);
        $('#date-filter-min, #date-filter-max').change(function() {
            leaveReportsListTable.draw();
        });
    }
    if ($('#benefits-list-table').length) {
        let benefitsListTable = $('#benefits-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#benefits-list-table_filter');
                $("#dates-filters").show();
            }, 
            "ajax": {
                "url": SITE_URL + "/benefits",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "employee.name" },
                { "data": "type" , render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "start_date" },
                { "data": "end_date" },
                { "data": "frequency", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "frequency_type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "amount" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('benefits', data, false);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('benefits', trans('lang.labels.benefit'), undefined, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
    }
    if ($('#with-holdings-list-table').length) {
        let withHoldingsListTable = $('#with-holdings-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#with-holdings-list-table_filter');
                $("#dates-filters").show();
            }, 
            "ajax": {
                "url": SITE_URL + "/with-holdings",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "employee.name" },
                { "data": "type" },
                { "data": "start_date" },
                { "data": "end_date" },
                { "data": "frequency", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "frequency_type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "amount" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('with-holdings', data, false);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('with-holdings', trans('lang.labels.with_holding'), undefined, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
    }
    if ($('#discounts-list-table').length) {
        let discountListTable = $('#discounts-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#discounts-list-table_filter');
                $("#dates-filters").show();
            }, 
            "ajax": {
                "url": SITE_URL + "/discounts",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "employee.name" },
                { "data": "type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "start_date" },
                { "data": "end_date" },
                { "data": "frequency", render: function(data, type, row) { return showIfAvailable(data) } },
                { "data": "frequency_type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "amount" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('discounts', data, false);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('discounts', trans('lang.labels.discount'), undefined, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
    }
    if ($('#extra-hours-list-table').length) {
        let extraHourListTable = $('#extra-hours-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#dates-filters").detach().prependTo('#extra-hours-list-table_filter');
                $("#dates-filters").show();
            }, 
            "ajax": {
                "url": SITE_URL + "/extra-hours",
                "contentType": "application/json",
                "type": "GET"
            },
            "columns": [
                { "data": "sr_no" },
                { "data": "employee.name" },
                { "data": "type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
                { "data": "start_time", render: function(data, type, row) { return timeFormat(data, true) } },
                { "data": "end_time", render: function(data, type, row) { return timeFormat(data, true) } },
                { "data": "total_extra_hours" },
                { "data": "hours_price", render: function(data, type, row) { return moneyFormat(data, true) } },
                { "data": "extra_hours_total", render: function(data, type, row) { return moneyFormat(data, true) } },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('extra-hours', data, false);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('extra-hours', trans('lang.labels.extra_hour'), undefined, trans('lang.labels.new_label')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
    }
    /* $('#extra-hours-list-table').DataTable(Object.assign({}, dataTableOptions, {
        initComplete: function() {
            $("#dates-filters").detach().prependTo('#extra-hours-list-table_filter');
            $("#dates-filters").show();
        },    
    })); */
    
    $('#accounting-journals-list-table').DataTable(Object.assign({}, dataTableOptions));
    
    $('#leaves-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/leaves",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "employee" },
            { "data": "leave_type", render: function(data, type, row) { return trans('lang.labels.' + data) } },
            { "data": "from_date", render: function(data, type, row) { return dateFormat(data) } },
            { "data": "to_date", render: function(data, type, row) { return dateFormat(data) } },
            { "data": "number_of_days" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('leaves', data, false);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('leaves', trans('lang.labels.leave'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    $('#memberships-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/memberships",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "name" },
            { "data": "discount", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "point_percentage", render: function(data, type, row) { return showIfAvailable(data) } },
            { "data": "created_date", render: function(data, type, row) { return dateFormat(data) } },
            { "data": "point_expire_date", render: function(data, type, row) { return dateFormat(data) } },
            { "data": "description", "orderable": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('memberships', data, false);
                }
            },
        ],
        columnDefs: [
            { className: "dt-left", targets: [2] }
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();

            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('memberships', trans('lang.labels.membership'), undefined, trans('lang.labels.new_label')));
            }

            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));

    if ($('#table-list-table').length) {
        let tableListTable = $('#table-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $("#table-list_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#table-list-table_filter');
                $("#dates-filters").show();
            },
            "ajax": {
                "url": SITE_URL + "/restaurant-table",
                "contentType": "application/json",
                "type": "GET"
            },
            columns: [
                { "data": "table_name" },
                { "data": "zone_id" },
                { "data": "date" , render: function(data, type, row) { return dateFormat(data) }},
                { "data": "description", "orderable": false, render: function(data, type, row) { return "<span>" + showIfAvailable(data) + "</span>" }, "sClass": "descrip-notes" },
                { "data": "chairs_number" },
                { "data": "table_number" },
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('restaurant-table', data);
                    }
                },
            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroRecordsMsg('restaurant-table', trans('lang.labels.table')));
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                } else {
                    $('.dataTables_paginate').hide();
                }
            }
        }));
        let customize = function(xlsx) {
            let rowNumOfTotal = 0;
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            // read each row
            var sumOftableListTableTotal = 0;
            var sumOftableListTableTax = 0;
            var sumOftableListTableDiscount = 0;
            var sumOftableListTableRemainingTotal = 0;
            // Loop over the cells in column `F`
            $('row c[r^="C"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOftableListTableRemainingTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="D"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOftableListTableTotal += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                    rowNumOfTotal = i;
                }
            });

            $('row c[r^="E"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOftableListTableTax += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });

            $('row c[r^="F"]', sheet).each(function(i, v) {
                if (i > 0) {
                    // Get the value and strip the non numeric characters
                    sumOftableListTableDiscount += parseFloat(parseFloat($(this).text().replaceAll(CURRENCY_SYMBOL, "").replaceAll(',', '')));
                }
            });
            rowNumOfTotal += 4;
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,

            });

            //insert
            var r1 = addRow(rowNumOfTotal, [{
                k: 'B',
                v: trans('lang.labels.total')
            }]);
            var r2 = addRow(rowNumOfTotal, [{
                k: 'C',
                v: moneyFormat(sumOftableListTableRemainingTotal)
            }]);
            var r3 = addRow(rowNumOfTotal, [{
                k: 'D',
                v: moneyFormat(sumOftableListTableTotal)
            }]);
            var r4 = addRow(rowNumOfTotal, [{
                k: 'E',
                v: moneyFormat(sumOftableListTableTax)
            }]);
            var r5 = addRow(rowNumOfTotal, [{
                k: 'F',
                v: moneyFormat(sumOftableListTableDiscount)
            }]);

            // comment this line if we need total as seprate row for each total.
            sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r1 + r2 + r3 + r4 + r5;

            // sheet.childNodes[0].childNodes[1].innerHTML = sheet.childNodes[0].childNodes[1].innerHTML + r5;

            // $('row c[r^="I"]', sheet).attr( 's', '50' );
            $('row c[r^="E"]', sheet).each(formatColumnAndRow);
            $('row c[r^="F"]', sheet).each(formatColumnAndRow);

            function formatColumnAndRow(i, v) {
                if (i == 0) {
                    $(this).attr('s', '2');
                } else {
                    $(this).attr('s', '50');
                }
            }
        }
        exportExcelPdf(tableListTable, 'restaurant-table', customize, [0, 1, 2, 3, 4, 5]);
        dateFilter(2);
        $('#date-filter-min, #date-filter-max').change(function() {
            tableListTable.draw();
        });
    }

    if ($('#order-production-list-table').length) {
        let invoiceListTable = $('#order-production-list-table').DataTable(Object.assign({}, dataTableOptions, {
            initComplete: function() {
                $(".order-production-list-table_filter").detach().appendTo('#export-buttons');
                $("#export-buttons").show();
                $("#dates-filters").detach().prependTo('#order-production-list-table_filter');
                $("#dates-filters").show();
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html('$' + 121);
            },
            "ajax": {
                // "url": SITE_URL + "/invoices",
                "url": location.href,
                "contentType": "application/json",
                "type": "GET"
            },
            "deferRender": true,
            columns: [
                { "data": "contact.name" },
                { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "expiration_date", render: function(data, type, row) { return dateFormat(data) } },
                { "data": "term"},
                { "data": "notes"},
                { "data": "internal_number"},
                { "data": "status"},
                {
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": "",
                    "sClass": "to-show",
                    "mRender": function(data, type, row) {
                        return createActionsColumn('production', data, false, true, false, false, true, false);
                    }
                },

            ],
            drawCallback: function(settings) {
                var api = this.api();
                var searchText = api.search();
                var currentPageDataSet = api.rows({ page: 'current' }).data();

                let extraResouce = location.href.substring(location.href.lastIndexOf('/') + 1);
                extraResouce = extraResouce == '/' ? '' : extraResouce;

                if (searchText != '' && currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, zeroSearchRecordsMsg());
                } else if (currentPageDataSet.length == 0) {
                    generateEmptyTable(settings, '<div class="message"><p>' + trans('lang.labels.no_records_found') + '</p></div>');
                }

                if (currentPageDataSet.length) {
                    $('.dataTables_paginate').show();
                    invoiceListTable.buttons('.buttons-html5').enable();
                } else {
                    invoiceListTable.buttons('.buttons-html5').disable();
                    $('.dataTables_paginate').hide();
                }
            }
        }));

        exportExcelPdf(invoiceListTable, 'Production order', null, [0, 1, 2, 3, 4, 5, 6], false);
        dateFilter(1);

        var start = moment().subtract(29, 'days');
        var end = moment();
        var paid = 'Paid';
        $('#order-production-filter').daterangepicker({
            autoUpdateInput: false,
            startDate: start,
            endDate: end,
            ranges: {
                [`${trans('lang.labels.done')}`]: [moment().subtract(2, 'days'), moment().subtract(2, 'days')],
                [`${trans('lang.labels.in_progress')}`]: [moment().subtract(3, 'days'), moment().subtract(3, 'days')],
                [`${trans('lang.labels.pending')}`]: [moment().subtract(4, 'days'), moment().subtract(4, 'days')],
                [`${trans('lang.labels.confirmed')}`]: [moment().subtract(5, 'days'), moment().subtract(5, 'days')],
                [`${trans('lang.labels.today')}`]: [moment(), moment()],
                [`${trans('lang.labels.yesterday')}`]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                [`${trans('lang.labels.last_7_days')}`]: [moment().subtract(6, 'days'), moment()],
                [`${trans('lang.labels.this_month')}`]: [moment().startOf('month'), moment().endOf('month')],
                [`${trans('lang.labels.last_month')}`]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                [`${trans('lang.labels.last_30_days')}`]: [moment().subtract(29, 'days'), moment()],
                [`${trans('lang.labels.last_60_days')}`]: [moment().subtract(59, 'days'), moment()],
                [`${trans('lang.labels.last_90_days')}`]: [moment().subtract(89, 'days'), moment()],
                [`${trans('lang.labels.last_180_days')}`]: [moment().subtract(179, 'days'), moment()],
            }
        },
        function (start, end, label) {
        var s = moment(start.toISOString());
        var e = moment(end.toISOString());
        $('#order-production-filter').val(label);
        if(label == 'Done' || label == 'Pending' || label == 'In Progress' || label == 'Confirmed'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            invoiceListTable.search( label ).draw();
        } else if (label == 'En curso' || label == 'Pendiente' || label == 'Hecho' || label == 'Confirmado'){
            $('#date-filter-min').datepicker("setDate", "");
            $('#date-filter-max').datepicker("setDate", "");
            invoiceListTable.search( label ).draw();
        }else{
            invoiceListTable.search( "" ).draw();
            $('#date-filter-min').datepicker("setDate", new Date(s));
            $('#date-filter-max').datepicker("setDate", new Date(e));
        }
        });

        $('#date-filter-min, #date-filter-max').change(function() {
            invoiceListTable.draw();
        });
    }



if ($('#unit-list-table').length) {
    let unitListTable = $('#unit-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/units",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "unit" },
            { "data": "shortcut" },
            {
                "orderable": false,
                "searchable": false,
                "data": null,
                "defaultContent": "",
                "sClass": "to-show",
                "mRender": function(data, type, row) {
                    return createActionsColumn('units', data);
                }
            },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();
            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('units', trans('lang.labels.units'), undefined, trans('lang.labels.new_label'), false));
            }
            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));
}

$('#shipping-adjustments-list-table').DataTable(Object.assign({}, dataTableOptions, {
    "ajax": {
        "url": SITE_URL + "/shipping",
        "contentType": "application/json",
        "type": "GET"
    },
    "columns": [
        { "data": "id" },
        { "data": "contact.name" },
        { "data": "start_date", render: function(data, type, row) { return dateFormat(data) } },
        { "data": "expiration_date", render: function(data, type, row) { return dateFormat(data) } },
        { "data": "internal_number"},
        { "data": "notes", "searchable": true, "visible": true },
        { "data": "calculation.status", "orderable": true, "searchable": true },
        {
            "orderable": false,
            "searchable": false,
            "data": null,
            "defaultContent": "",
            "sClass": "to-show",
            "mRender": function(data) {
                return createActionsColumn('shipping', data, true , false,false, false, false, false, false);
                 
            }
        },
    ],
    columnDefs: [
        { className: "dt-left", targets: [3] }
    ],
    drawCallback: function(settings) {
        var api = this.api();
        var searchText = api.search();
        var currentPageDataSet = api.rows({ page: 'current' }).data();
        if (searchText != '' && currentPageDataSet.length == 0) {
            generateEmptyTable(settings, zeroSearchRecordsMsg());
        } else if (currentPageDataSet.length == 0) {
            generateEmptyTable(settings, zeroRecordsMsg('shipping', trans('lang.labels.shipping')));
        }
        if (currentPageDataSet.length) {
            $('.dataTables_paginate').show();
        } else {
            $('.dataTables_paginate').hide();
        }
    }
}));


if ($('#history-list-table').length) {
    let leaveReportsListTable = $('#history-list-table').DataTable(Object.assign({}, dataTableOptions, {
        "ajax": {
            "url": SITE_URL + "/history",
            "contentType": "application/json",
            "type": "GET"
        },
        "columns": [
            { "data": "id" },
            { "data": "UserName" },
            { "data": "action_type" },
            { "data": "record_id" },
            { "data": "record_type" },
            { "data": "created_at", render: function(data, type, row) { return dateFormat(data) } },
            // {
            //     "orderable": false,
            //     "searchable": false,
            //     "data": null,
            //     "defaultContent": "",
            //     "sClass": "to-show",
            //     "mRender": function(data, type, row) {
            //         return createActionsColumn('sector', data);
            //     }
            // },
        ],
        drawCallback: function(settings) {
            var api = this.api();
            var searchText = api.search();
            var currentPageDataSet = api.rows({ page: 'current' }).data();
            if (searchText != '' && currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroSearchRecordsMsg());
            } else if (currentPageDataSet.length == 0) {
                generateEmptyTable(settings, zeroRecordsMsg('history', trans('lang.labels.histories'), undefined, trans('lang.labels.new_label'), false));
            }
            if (currentPageDataSet.length) {
                $('.dataTables_paginate').show();
            } else {
                $('.dataTables_paginate').hide();
            }
        }
    }));
}
});