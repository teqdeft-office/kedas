function addZeroes(num, basedOnLocale = false, fractionDigits = 2) {
    num = typeof num == 'number' ? num.toString() : num;
    const dec = num.split('.')[1]
        // const len = dec && dec.length > 2 ? dec.length : 2
    const len = 2;
    if (basedOnLocale) {
        let fullLocale = locale == 'es' ? 'es-DO' : 'en-US';
        const formatter = new Intl.NumberFormat(fullLocale, {
            minimumFractionDigits: fractionDigits,
        });
        return formatter.format(num);
    }
    return Number(num).toFixed(len)
}

function dateFormat(date, from_format = "YYYY-MM-DD", to_format = "DD/MM/YYYY") {
    if (!date) {
        return '<i>N/A</i>';
    }
    if (locale == "en") {
        to_format = "MM/DD/YYYY"
    }
    return moment(date, from_format).format(to_format);
}

function dateTimeFormat(dateTime) {
    from_format = "YYYY-MM-DD";
    to_format = "DD/MM/YYYY HH:mm a";
    if (locale == "en") {
        to_format = "MM/DD/YYYY HH:mm a"
    }
    client_tz = moment.tz.guess() ? moment.tz.guess() : SERVER_TZ;
    return moment.tz(dateTime, client_tz).format(to_format);
}

function moneyFormat(money = 0.00, basedOnLocale = false) {
    if (!CURRENCY_SYMBOL) {
        CURRENCY_SYMBOL = '$';
    }
    if (basedOnLocale) {
        let fullLocale = locale == 'es' ? 'en-US' : 'en-US';
        let formatter = new Intl.NumberFormat(fullLocale, {
            minimumFractionDigits: 2,
        });
        return CURRENCY_SYMBOL + formatter.format(money);
    }
    return CURRENCY_SYMBOL + parseFloat(money).toFixed(2);
}

function showIfAvailable(value, defaultVal = '<i>N/A</i>') {
    return value ? value : defaultVal;
}

function trans(string, args) {
    let value = _.get(window.i18n, string);

    let defaultVal = _.last(_.split(string, '.'));
    if (value) {
        _.eachRight(args, (paramVal, paramKey) => {
            value = _.replace(value, `:${paramKey}`, paramVal);
        });
    } else {
        value = defaultVal;
    }

    return value;
};

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

function timeFormat(time, format = 'H:i A') {
    return moment(time, 'HH:mm:ss').format("LT");
}