function getItemCost(price, quantity = 1, discount = 0, taxInfo = undefined, item_id = 0) {
    let baseTotal = price * quantity;
    let discountAmount = 0.00
    let taxAmount = 0.00
    if (discount) {
        discountAmount = baseTotal * (discount / 100);
    }
    if (taxInfo !== undefined) {
        let { tax_per, tax_id, tax_name } = taxInfo;
        taxAmount = (baseTotal - discountAmount) * (tax_per / 100);
        taxObj = {};
        taxObj['tax'] = addZeroes(taxAmount);
        taxObj['tax_id'] = tax_id;
        taxObj['tax_name'] = tax_name;
    }

    return {
        baseTotal: addZeroes(baseTotal),
        discountAmount: addZeroes(discountAmount),
        taxInfo: taxObj,
        taxAmount: addZeroes(taxAmount),
        subtotal: addZeroes(baseTotal - discountAmount),
        total: addZeroes((baseTotal - discountAmount) + taxAmount),
        item_id,
        quantity
    }
}

function getInvoiceTotal(itemList,shippingCost=0) {
    itemList = itemList.filter(Boolean);
    let baseTotal = itemList.reduce(function(acc, num) {
        return Number(num.baseTotal) + acc;
    }, 0);
    let discountTotal = itemList.reduce(function(acc, num) {
        return Number(num.discountAmount) + acc;
    }, 0);
    let subTotal = itemList.reduce(function(acc, num) {
        return Number(num.subtotal) + acc;
    }, 0);
    let total = itemList.reduce(function(acc, num) {
        return Number(num.total) + acc;
    }, 0);
    let totalTaxAmount = 0;
    var taxInformation = {};

    itemList.forEach(function(d) {
        if (taxInformation.hasOwnProperty(d.taxInfo.tax_id)) {
            taxInformation[d.taxInfo.tax_id]['amount'] = Number(taxInformation[d.taxInfo.tax_id]['amount']) + Number(d.taxInfo.tax);
        } else {
            taxInformation[d.taxInfo.tax_id] = {};
            taxInformation[d.taxInfo.tax_id]['amount'] = Number(d.taxInfo.tax);
        }
        taxInformation[d.taxInfo.tax_id]['amount'] = addZeroes(taxInformation[d.taxInfo.tax_id]['amount']);
        taxInformation[d.taxInfo.tax_id]['name'] = d.taxInfo.tax_name;
    });
    for (var key in taxInformation) {
        if (taxInformation.hasOwnProperty(key)) {
            totalTaxAmount += Number(taxInformation[key].amount);
        }
    }

    return {
        baseTotal: addZeroes(baseTotal),
        discountTotal: addZeroes(discountTotal),
        subTotal: addZeroes(subTotal),
        total: addZeroes(total+shippingCost),
        totalTaxAmount: addZeroes(totalTaxAmount),
        taxInformation
    }
}


function getInventoryPriceAfterMembership(salePrice, costPrice, discountPer) {
    let profit = salePrice - costPrice;
    if (profit >= 0) {
        let discountAmt = profit * (discountPer / 100);
        salePrice -= discountAmt;
    }
    return addZeroes(salePrice);
}