$(document).ready(function() {
	$(document).on('change', 'form#add-edit-fixed-assets-form input[name="price"]', populateFixedAssetsTotal);
	$(document).on("click", "a.add-depreciation-row", async function(e) {
        let nextRowNumber = $('a.remove-depr-row').last().length ? $('a.remove-depr-row').last().data('row_number') + 1 : 1;
        await generateDeprItemRow(nextRowNumber, 'depreciation_items');
        updateYearNumber();
    });
    $(document).on("click", "a.remove-depr-row", function(e) {
        $(this).closest('tr').remove();
        updateYearNumber();
    });

    updateYearNumber();

    $(document).on('change', 'form#add-edit-fixed-assets-form select[name="cal_type"]', function() {
    	if ($(this).val() == "units_produced") {
    		$("div#unit_produced_info").show();
    	} else {
    		$("div#unit_produced_info").hide();
    	}
    });

    $(document).on('change', 'form#add-edit-fixed-assets-form input[name="same_for_year"]', function() {
    	if ($(this).val() == "0") {
    		$("div#per_year_prod_section").show();
    	} else {
    		$("div#per_year_prod_section").hide();
    		$("input[name='per_year_prod']").val();
    	}
    });

    $(document).on('change', 'form#add-edit-fixed-assets-form input[name="price"], input[name="years"], input[name="scrap_value"], select[name="cal_type"], input[name="total_unit_produced"], input[name="unit_produced_per_year"], input[name="same_for_year"], input[name="per_year_prod"]', async function() {
        let total = $('input[name="price"]').val();
        let years = $('input[name="years"]').val();
        let scrap_value = $('input[name="scrap_value"]').val();
        let cal_type = $('select[name="cal_type"] :selected').val();

        let total_unit_produced = $('input[name="total_unit_produced"]').val() || null;
        let unit_produced_per_year = $('input[name="unit_produced_per_year"]').val() || null;
        let same_for_year = $('input[name="same_for_year"]:checked').val() || null;
        let per_year_prod = $('input[name="per_year_prod"]').val() || null;

        await generateDepreciationTimeline(years, total, cal_type, scrap_value, total_unit_produced, unit_produced_per_year, same_for_year, per_year_prod);
    });

   	if ($("form#add-edit-fixed-assets-form").length) {
        let cal_type = $('form#add-edit-fixed-assets-form select[name="cal_type"] :selected').val();
        let same_for_year = $('form#add-edit-fixed-assets-form input[name="same_for_year"]:checked').val();

    	if (cal_type == "units_produced") {
    		$("div#unit_produced_info").show();
    	} else {
    		$("div#unit_produced_info").hide();
    	}

    	if (same_for_year == "0") {
    		$("div#per_year_prod_section").show();
    	} else {
    		$("div#per_year_prod_section").hide();
    		$("input[name='per_year_prod']").val();
    	}
    }

    $(document).on('change', 'form#add-edit-fixed-assets-form input[name="depr_should_cal"]', function(e) {
        let depr_should_cal = $(this).val();
        if (depr_should_cal == "1") {
            $('div#depr_info_section').show();
        } else {
            $('div#depr_info_section').hide();
        }
    });

    if ($("form#add-edit-fixed-assets-form").length) {
        let depr_should_cal = $('form#add-edit-fixed-assets-form input[name="depr_should_cal"]:checked').val();
        if (depr_should_cal == "1") {
            $('div#depr_info_section').show();
        } else if (depr_should_cal == "0") {
            $('div#depr_info_section').hide();
        }
    }
});

async function generateDepreciationTimeline(years, total, cal_type, scrap_percentage, total_unit_produced = null, unit_produced_per_year = null, same_for_year = null, per_year_prod = null) {
    
    if (years && total && cal_type && scrap_percentage) {
        let result;
        try {
            result = await $.ajax({
                url: `${SITE_URL}/app/get-depreciation-timeline?years=${years}&total=${total}&cal_type=${cal_type}&scrap_percentage=${scrap_percentage}&total_unit_produced=${total_unit_produced}&unit_produced_per_year=${unit_produced_per_year}&same_for_year=${same_for_year}&per_year_prod=${per_year_prod}`,
                type: "GET",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                success:function(response) {
                    $('#depreciation_timeline_table tbody').html("");
                    var trHTML = '';
                    var data = response.data;
                    if (data) {
	                    for(var i = 0; i < data.length; i++)
	                    {
	                        trHTML += 
	                        '<tr><td>' + data[i].year + 
	                        '</td><td>' + moneyFormat(data[i].opening_value) + 
	                        '</td><td>' + moneyFormat(data[i].depreciation) + 
	                        '</td><td>' + moneyFormat(data[i].closing_value) +
	                        '</td></tr>';
	                    };
	                    $('#depreciation_timeline_table tbody').append(trHTML);
                    }
                },
            });
            return result;
        } catch (error) {
            // console.log(error)
        }
    }
}

async function generateDeprItemRow(id, tableId) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/generate-depr-item-row",
            type: "POST",
            data: { id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(newRowContent) {
                $("#" + tableId + " tbody").append(newRowContent);
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

function populateFixedAssetsTotal() {
    let quantity = 1;
    let discount = 0
    let price = $('input[name="price"]').val();
    let calculationResult = getItemCost(price, 1, discount);
    if (price) {
        $('h3[id="item_total"]').html(CURRENCY_SYMBOL + calculationResult.total);
    }
}

function updateYearNumber() {
    $("#depreciation_items > tbody tr").each(function(key, ele){
        let year = key + 1;
        $(this).find("td:eq(0) > input").val(year);
    });
}