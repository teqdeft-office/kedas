$(document).ready(function() {
    let segments = location.pathname.split('/').filter(Boolean).filter(function(segment) {
        return !segment.includes('kedas') && !segment.includes('admin') && isNaN(segment)
    });
    let joinedSegment = segments.join('-');

    let resource_type_ele = $("input[name='resource_type']");
    if (resource_type_ele.length) {
        let resource_type = resource_type_ele.val();
        if (resource_type) {
            joinedSegment += '-' + resource_type;
        }
    }

    let menuOption = mapSegmentToMenuOption[joinedSegment] || "dashboard";
    let menuSlug = mapSegmentToMenuSlug[joinedSegment] || joinedSegment;

    $.expr[':'].containsexactly = function(obj, index, meta, stack) {
        return $(obj).text() === meta[3];
    };

    if (menuOption == 'dashboard') {
        $('a#dashboard_link').addClass('active');
    } else {
        $('div#'+menuOption).collapse('show');
        $('a[href="#' + menuOption + '"]').addClass('active');
        $('a[data-resource_slug=' + menuSlug + ']').addClass('active');
        // $('a[href="#' + menuOption + '"]').removeClass('collapsed');
        // $('div#' + menuOption + '').addClass('show');
        // $('a:containsexactly('+menuText+')').addClass('active');
    }
});

let mapSegmentToMenuOption = [];
let mapSegmentToMenuSlug = [];

mapSegmentToMenuOption['invoices-sale'] = 'sales';
mapSegmentToMenuOption['invoices-create-sale'] = 'sales';
mapSegmentToMenuOption['invoices-edit-sale'] = 'sales';

mapSegmentToMenuOption['invoices-recurring'] = 'sales';
mapSegmentToMenuOption['invoices-create-recurring'] = 'sales';
mapSegmentToMenuOption['invoices-edit-recurring'] = 'sales';

mapSegmentToMenuOption['transactions-in'] = 'sales';
mapSegmentToMenuOption['transactions-create-in'] = 'sales';
mapSegmentToMenuOption['transactions-edit-in'] = 'sales';

mapSegmentToMenuOption['credit-notes'] = 'sales';
mapSegmentToMenuOption['credit-notes-create'] = 'sales';
mapSegmentToMenuOption['credit-notes-edit'] = 'sales';

mapSegmentToMenuOption['debit-notes'] = 'expenses';
mapSegmentToMenuOption['debit-notes-create'] = 'expenses';
mapSegmentToMenuOption['debit-notes-edit'] = 'expenses';

mapSegmentToMenuOption['purchase-orders'] = 'expenses';
mapSegmentToMenuOption['purchase-orders-create'] = 'expenses';
mapSegmentToMenuOption['purchase-orders-edit'] = 'expenses';

mapSegmentToMenuOption['transactions-out'] = 'expenses';
mapSegmentToMenuOption['transactions-create-out'] = 'expenses';
mapSegmentToMenuOption['transactions-edit-out'] = 'expenses';

mapSegmentToMenuOption['transactions-recurring'] = 'expenses';
mapSegmentToMenuOption['transactions-create-recurring'] = 'expenses';
mapSegmentToMenuOption['transactions-edit-recurring'] = 'expenses';

mapSegmentToMenuOption['invoices-supplier'] = 'expenses';
mapSegmentToMenuOption['invoices-create-supplier'] = 'expenses';
mapSegmentToMenuOption['invoices-edit-supplier'] = 'expenses';

mapSegmentToMenuOption['estimates'] = 'sales';
mapSegmentToMenuOption['estimates-create'] = 'sales';
mapSegmentToMenuOption['estimates-edit'] = 'sales';

mapSegmentToMenuOption['clients'] = 'contacts';
mapSegmentToMenuOption['clients-create'] = 'contacts';
mapSegmentToMenuOption['clients-edit'] = 'contacts';
mapSegmentToMenuOption['import-clients'] = 'contacts';

mapSegmentToMenuOption['employees'] = 'contacts';
mapSegmentToMenuOption['employees-create'] = 'contacts';
mapSegmentToMenuOption['employees-edit'] = 'contacts';
mapSegmentToMenuOption['import-employees'] = 'contacts';

mapSegmentToMenuOption['suppliers'] = 'contacts';
mapSegmentToMenuOption['suppliers-create'] = 'contacts';
mapSegmentToMenuOption['suppliers-edit'] = 'contacts';
mapSegmentToMenuOption['import-suppliers'] = 'contacts';

mapSegmentToMenuOption['inventories'] = 'inventory';
mapSegmentToMenuOption['inventories-create'] = 'inventory';
mapSegmentToMenuOption['inventories-edit'] = 'inventory';
mapSegmentToMenuOption['import-inventories'] = 'inventory';

mapSegmentToMenuOption['taxes'] = 'inventory';
mapSegmentToMenuOption['taxes-create'] = 'inventory';
mapSegmentToMenuOption['taxes-edit'] = 'inventory';

mapSegmentToMenuOption['item-categories'] = 'inventory';
mapSegmentToMenuOption['item-categories-create'] = 'inventory';
mapSegmentToMenuOption['item-categories-edit'] = 'inventory';

mapSegmentToMenuOption['settings-user'] = 'setting';
mapSegmentToMenuOption['settings-company'] = 'setting';
mapSegmentToMenuOption['ncfs'] = 'setting';
mapSegmentToMenuOption['ncfs-create'] = 'setting';
mapSegmentToMenuOption['ncfs-edit'] = 'setting';
mapSegmentToMenuOption['currency-exchange-rates'] = 'setting';

mapSegmentToMenuOption['inventory-adjustments'] = 'inventory';
mapSegmentToMenuOption['inventory-adjustments-edit'] = 'inventory';
mapSegmentToMenuOption['inventory-adjustments-create'] = 'inventory';

mapSegmentToMenuOption['user-management-users'] = 'users_management';
mapSegmentToMenuOption['user-management-users-create'] = 'users_management';
mapSegmentToMenuOption['user-management-users-edit'] = 'users_management';

mapSegmentToMenuOption['roles'] = 'users_management';
mapSegmentToMenuOption['roles-edit'] = 'users_management';
mapSegmentToMenuOption['roles-create'] = 'users_management';

mapSegmentToMenuOption['accounting-charts'] = 'accounting';

mapSegmentToMenuOption['remissions'] = 'sales';
mapSegmentToMenuOption['remissions-create'] = 'sales';
mapSegmentToMenuOption['remissions-edit'] = 'sales';

mapSegmentToMenuOption['users'] = 'users_management';

mapSegmentToMenuOption['countries'] = 'content_management';
mapSegmentToMenuOption['countries-edit'] = 'content_management';
mapSegmentToMenuOption['sectors'] = 'content_management';
mapSegmentToMenuOption['sectors-edit'] = 'content_management';
mapSegmentToMenuOption['currencies'] = 'content_management';
mapSegmentToMenuOption['currencies-edit'] = 'content_management';
mapSegmentToMenuOption['identification-types'] = 'content_management';
mapSegmentToMenuOption['identification-types-edit'] = 'content_management';

mapSegmentToMenuOption['departments'] = 'employees';
mapSegmentToMenuOption['departments-create'] = 'employees';
mapSegmentToMenuOption['departments-edit'] = 'employees';
mapSegmentToMenuOption['import-departments'] = 'employees';

mapSegmentToMenuOption['designations'] = 'employees';
mapSegmentToMenuOption['designations-create'] = 'employees';
mapSegmentToMenuOption['designations-edit'] = 'employees';
mapSegmentToMenuOption['import-designations'] = 'employees';

mapSegmentToMenuOption['insurance-brands'] = 'employees';
mapSegmentToMenuOption['insurance-brands-create'] = 'employees';
mapSegmentToMenuOption['insurance-brands-edit'] = 'employees';
mapSegmentToMenuOption['import-insurance-brands'] = 'employees';

mapSegmentToMenuOption['leave-types'] = 'leave-management';
mapSegmentToMenuOption['leave-types-create'] = 'leave-management';
mapSegmentToMenuOption['leave-types-edit'] = 'leave-management';
mapSegmentToMenuOption['import-leave-types'] = 'leave-management';

mapSegmentToMenuOption['leave-applications'] = 'leave-management';
mapSegmentToMenuOption['leave-applications-create'] = 'leave-management';

mapSegmentToMenuOption['leave-reports'] = 'leave-management';

mapSegmentToMenuOption['summary-report'] = 'leave-management';

mapSegmentToMenuOption['my-leave-report'] = 'leave-management';

mapSegmentToMenuOption['requested-applications'] = 'leave-management';

mapSegmentToMenuOption['accounting-adjustments'] = 'accounting';
mapSegmentToMenuOption['accounting-adjustments-create'] = 'accounting';
mapSegmentToMenuOption['accounting-adjustments-edit'] = 'accounting';

mapSegmentToMenuOption['benefits'] = 'payroll';
mapSegmentToMenuOption['benefits-create'] = 'payroll';
mapSegmentToMenuOption['benefits-edit'] = 'payroll';

mapSegmentToMenuOption['with-holdings'] = 'payroll';
mapSegmentToMenuOption['with-holdings-create'] = 'payroll';
mapSegmentToMenuOption['with-holdings-edit'] = 'payroll';

mapSegmentToMenuOption['discounts'] = 'payroll';
mapSegmentToMenuOption['discounts-create'] = 'payroll';
mapSegmentToMenuOption['discounts-edit'] = 'payroll';

mapSegmentToMenuOption['extra-hours'] = 'payroll';
mapSegmentToMenuOption['extra-hours-create'] = 'payroll';
mapSegmentToMenuOption['extra-hours-edit'] = 'payroll';

mapSegmentToMenuOption['leaves'] = 'payroll';
mapSegmentToMenuOption['leaves-create'] = 'payroll';
mapSegmentToMenuOption['leaves-edit'] = 'payroll';

mapSegmentToMenuOption['commissions'] = 'payroll';
mapSegmentToMenuOption['unemployment'] = 'payroll';

mapSegmentToMenuOption['payroll-reports'] = 'payroll';
mapSegmentToMenuOption['payroll-reports-create'] = 'payroll';
mapSegmentToMenuOption['accounting-journals'] = 'accounting';
mapSegmentToMenuOption['accounting-reports'] = 'accounting';

mapSegmentToMenuOption['fixed-assets'] = 'accounting';
mapSegmentToMenuOption['fixed-assets-create'] = 'accounting';
mapSegmentToMenuOption['fixed-assets-edit'] = 'accounting';

mapSegmentToMenuOption['accounting-reports-balance-sheet'] = 'accounting';
mapSegmentToMenuOption['accounting-reports-income-statement'] = 'accounting';
mapSegmentToMenuOption['accounting-reports-general-ledger'] = 'accounting';
mapSegmentToMenuOption['accounting-reports-journal-report'] = 'accounting';
mapSegmentToMenuOption['reports-general'] = 'accounting';

mapSegmentToMenuOption['memberships'] = 'setting';
mapSegmentToMenuOption['memberships-create'] = 'setting';
mapSegmentToMenuOption['memberships-edit'] = 'setting';

mapSegmentToMenuOption['activity-logs'] = 'users_management';

// Slug options.
mapSegmentToMenuSlug['invoices-sale'] = 'invoices';
mapSegmentToMenuSlug['invoices-create-sale'] = 'invoices';
mapSegmentToMenuSlug['invoices-edit-sale'] = 'invoices';

mapSegmentToMenuSlug['invoices-recurring'] = 'invoices-recurring';
mapSegmentToMenuSlug['invoices-create-recurring'] = 'invoices-recurring';
mapSegmentToMenuSlug['invoices-edit-recurring'] = 'invoices-recurring';

mapSegmentToMenuSlug['transactions-in'] = 'transactions-in';
mapSegmentToMenuSlug['transactions-create-in'] = 'transactions-in';
mapSegmentToMenuSlug['transactions-edit-in'] = 'transactions-in';

mapSegmentToMenuSlug['transactions-out'] = 'transactions-out';
mapSegmentToMenuSlug['transactions-create-out'] = 'transactions-out';
mapSegmentToMenuSlug['transactions-edit-out'] = 'transactions-out';

mapSegmentToMenuSlug['transactions-recurring'] = 'transactions-recurring';
mapSegmentToMenuSlug['transactions-create-recurring'] = 'transactions-recurring';
mapSegmentToMenuSlug['transactions-edit-recurring'] = 'transactions-recurring';

mapSegmentToMenuSlug['credit-notes'] = 'credit-notes';
mapSegmentToMenuSlug['credit-notes-create'] = 'credit-notes';
mapSegmentToMenuSlug['credit-notes-edit'] = 'credit-notes';

mapSegmentToMenuSlug['debit-notes'] = 'debit-notes';
mapSegmentToMenuSlug['debit-notes-create'] = 'debit-notes';
mapSegmentToMenuSlug['debit-notes-edit'] = 'debit-notes';

mapSegmentToMenuSlug['estimates'] = 'estimates';
mapSegmentToMenuSlug['estimates-create'] = 'estimates';
mapSegmentToMenuSlug['estimates-edit'] = 'estimates';

mapSegmentToMenuSlug['purchase-orders'] = 'purchase-orders';
mapSegmentToMenuSlug['purchase-orders-create'] = 'purchase-orders';
mapSegmentToMenuSlug['purchase-orders-edit'] = 'purchase-orders';

mapSegmentToMenuSlug['clients'] = 'clients';
mapSegmentToMenuSlug['clients-create'] = 'clients';
mapSegmentToMenuSlug['clients-edit'] = 'clients';
mapSegmentToMenuSlug['import-clients'] = 'clients';

mapSegmentToMenuSlug['invoices-supplier'] = 'invoices-supplier';
mapSegmentToMenuSlug['invoices-create-supplier'] = 'invoices-supplier';
mapSegmentToMenuSlug['invoices-edit-supplier'] = 'invoices-supplier';

mapSegmentToMenuSlug['suppliers'] = 'suppliers';
mapSegmentToMenuSlug['suppliers-create'] = 'suppliers';
mapSegmentToMenuSlug['suppliers-edit'] = 'suppliers';
mapSegmentToMenuSlug['import-suppliers'] = 'suppliers';

mapSegmentToMenuSlug['inventories'] = 'inventories';
mapSegmentToMenuSlug['inventories-create'] = 'inventories';
mapSegmentToMenuSlug['inventories-edit'] = 'inventories';
mapSegmentToMenuSlug['import-inventories'] = 'inventories';

mapSegmentToMenuSlug['taxes'] = 'taxes';
mapSegmentToMenuSlug['taxes-create'] = 'taxes';
mapSegmentToMenuSlug['taxes-edit'] = 'taxes';

mapSegmentToMenuSlug['item-categories'] = 'item-categories';
mapSegmentToMenuSlug['item-categories-create'] = 'item-categories';
mapSegmentToMenuSlug['item-categories-edit'] = 'item-categories';

mapSegmentToMenuSlug['settings-user'] = 'settings-user';
mapSegmentToMenuSlug['settings-company'] = 'settings-company';

mapSegmentToMenuSlug['ncfs'] = 'ncfs';
mapSegmentToMenuSlug['ncfs-create'] = 'ncfs';
mapSegmentToMenuSlug['ncfs-edit'] = 'ncfs';

mapSegmentToMenuSlug['inventory-adjustments'] = 'inventory-adjustments';
mapSegmentToMenuSlug['inventory-adjustments-edit'] = 'inventory-adjustments';
mapSegmentToMenuSlug['inventory-adjustments-create'] = 'inventory-adjustments';

mapSegmentToMenuSlug['user-management-users'] = 'users-list';
mapSegmentToMenuSlug['user-management-users-create'] = 'users-list';
mapSegmentToMenuSlug['user-management-users-edit'] = 'users-list';

mapSegmentToMenuSlug['roles'] = 'roles-list';
mapSegmentToMenuSlug['roles-edit'] = 'roles-list';
mapSegmentToMenuSlug['roles-create'] = 'roles-list';

mapSegmentToMenuSlug['currency-exchange-rates'] = 'currency-exchange-rates';

mapSegmentToMenuSlug['accounting-charts'] = 'chart-of-accounts';

mapSegmentToMenuSlug['remissions'] = 'remissions';
mapSegmentToMenuSlug['remissions-create'] = 'remissions';
mapSegmentToMenuSlug['remissions-edit'] = 'remissions';

mapSegmentToMenuSlug['users'] = 'users-list';

mapSegmentToMenuSlug['countries'] = 'countries-list';
mapSegmentToMenuSlug['countries-edit'] = 'countries-list';

mapSegmentToMenuSlug['employees'] = 'employees';
mapSegmentToMenuSlug['employees-create'] = 'employees';
mapSegmentToMenuSlug['employees-edit'] = 'employees';
mapSegmentToMenuSlug['import-employees'] = 'employees';

mapSegmentToMenuSlug['departments'] = 'departments';
mapSegmentToMenuSlug['departments-create'] = 'departments';
mapSegmentToMenuSlug['departments-edit'] = 'departments';
mapSegmentToMenuSlug['departments-employees'] = 'departments';
mapSegmentToMenuSlug['sectors'] = 'sectors-list';
mapSegmentToMenuSlug['sectors-edit'] = 'sectors-list';

mapSegmentToMenuSlug['currencies'] = 'currencies-list';
mapSegmentToMenuSlug['currencies-edit'] = 'currencies-list';

mapSegmentToMenuSlug['identification-types'] = 'identification_types-list';
mapSegmentToMenuSlug['identification-types-edit'] = 'identification_types-list';

mapSegmentToMenuSlug['designations'] = 'designations';
mapSegmentToMenuSlug['designations-create'] = 'designations';
mapSegmentToMenuSlug['designations-edit'] = 'designations';
mapSegmentToMenuSlug['designations-employees'] = 'designations';

mapSegmentToMenuSlug['insurance-brands'] = 'insurance-brands';
mapSegmentToMenuSlug['insurance-brands-create'] = 'insurance-brands';
mapSegmentToMenuSlug['insurance-brands-edit'] = 'insurance-brands';
mapSegmentToMenuSlug['insurance-brands-employees'] = 'insurance-brands';

mapSegmentToMenuSlug['leave-types'] = 'leave-types';
mapSegmentToMenuSlug['leave-types-create'] = 'leave-types';
mapSegmentToMenuSlug['leave-types-edit'] = 'leave-types';
mapSegmentToMenuSlug['leave-types-employees'] = 'leave-types';

mapSegmentToMenuSlug['leave-applications'] = 'leave-applications';
mapSegmentToMenuSlug['leave-applications-create'] = 'leave-applications';

mapSegmentToMenuSlug['leave-reports'] = 'leave-reports';

mapSegmentToMenuSlug['summary-report'] = 'summary-report';

mapSegmentToMenuSlug['my-leave-report'] = 'my-leave-report';

mapSegmentToMenuSlug['requested-applications'] = 'requested-applications';

mapSegmentToMenuSlug['accounting-adjustments'] = 'account-adjustments';
mapSegmentToMenuSlug['accounting-adjustments-create'] = 'account-adjustments';
mapSegmentToMenuSlug['accounting-adjustments-edit'] = 'account-adjustments';

mapSegmentToMenuSlug['benefits'] = 'benefits';
mapSegmentToMenuSlug['benefits-create'] = 'benefits';
mapSegmentToMenuSlug['benefits-edit'] = 'benefits';

mapSegmentToMenuSlug['with-holdings'] = 'with-holdings';
mapSegmentToMenuSlug['with-holdings-create'] = 'with-holdings';
mapSegmentToMenuSlug['with-holdings-edit'] = 'with-holdings';

mapSegmentToMenuSlug['discounts'] = 'discounts';
mapSegmentToMenuSlug['discounts-create'] = 'discounts';
mapSegmentToMenuSlug['discounts-edit'] = 'discounts';

mapSegmentToMenuSlug['extra-hours'] = 'extra-hours';
mapSegmentToMenuSlug['extra-hours-create'] = 'extra-hours';
mapSegmentToMenuSlug['extra-hours-edit'] = 'extra-hours';

mapSegmentToMenuSlug['leaves'] = 'leaves';
mapSegmentToMenuSlug['leaves-create'] = 'leaves';
mapSegmentToMenuSlug['leaves-edit'] = 'leaves';

mapSegmentToMenuSlug['commissions'] = 'commissions';
mapSegmentToMenuSlug['unemployment'] = 'unemployment';

mapSegmentToMenuSlug['payroll-reports'] = 'payroll-reports';
mapSegmentToMenuSlug['payroll-reports-create'] = 'payroll-reports';
mapSegmentToMenuSlug['accounting-journals'] = 'account-journals';
mapSegmentToMenuSlug['accounting-reports'] = 'account-reports';

mapSegmentToMenuSlug['fixed-assets'] = 'fixed-assets';
mapSegmentToMenuSlug['fixed-assets-create'] = 'fixed-assets';
mapSegmentToMenuSlug['fixed-assets-edit'] = 'fixed-assets';
mapSegmentToMenuSlug['accounting-reports-balance-sheet'] = 'account-reports';
mapSegmentToMenuSlug['accounting-reports-income-statement'] = 'account-reports';
mapSegmentToMenuSlug['accounting-reports-general-ledger'] = 'account-reports';
mapSegmentToMenuSlug['accounting-reports-journal-report'] = 'account-reports';

mapSegmentToMenuSlug['memberships'] = 'memberships';
mapSegmentToMenuSlug['memberships-create'] = 'memberships';
mapSegmentToMenuSlug['memberships-edit'] = 'memberships';

mapSegmentToMenuSlug['user-management-activity-logs'] = 'activity-logs';

mapSegmentToMenuSlug['reports-general'] = 'general-reports';

mapSegmentToMenuSlug['restaurant-table'] = 'restaurant-table';
mapSegmentToMenuSlug['restaurant-table-create'] = 'restaurant-table';
mapSegmentToMenuSlug['restaurant-table-edit'] = 'restaurant-table';

mapSegmentToMenuOption['restaurant-table'] = 'sales';
mapSegmentToMenuOption['restaurant-table-create'] = 'sales';
mapSegmentToMenuOption['restaurant-table-edit'] = 'sales';

mapSegmentToMenuOption['production-create'] = 'sales';
mapSegmentToMenuOption['production-edit'] = 'sales';
mapSegmentToMenuOption['production'] = 'sales';


mapSegmentToMenuSlug['production'] = 'production';
mapSegmentToMenuSlug['production-create'] = 'production';
mapSegmentToMenuSlug['production-edit'] = 'production';

mapSegmentToMenuOption['units'] = 'inventory';
mapSegmentToMenuOption['units-create'] = 'inventory';
mapSegmentToMenuOption['units-edit'] = 'inventory';

mapSegmentToMenuSlug['units'] = 'units';
mapSegmentToMenuSlug['units-create'] = 'units';
mapSegmentToMenuSlug['units-edit'] = 'units';
mapSegmentToMenuSlug['import-units'] = 'units';

