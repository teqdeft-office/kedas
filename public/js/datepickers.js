let date = new Date(moment.now());
let today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
let tomorrow = new Date(+date + 86400000);
let lastDayofMonth = moment().clone().endOf('month').format('YYYY-MM-DD');
let startDayofMonth = moment().clone().startOf('month').format('YYYY-MM-DD');
let startOfTheYear = moment().startOf('year').format('YYYY-MM-DD');

let pickerOptsGeneral = {
    uiLibrary: 'bootstrap4',
    format: "yyyy-mm-dd",
    date: getDateInFormat(today),
    autoclose: true,
    minView: 2,
    maxView: 2,
    todayHighlight: true,
    templates: {
        leftArrow: '<i class="fa fa-chevron-left"></i>',
        rightArrow: '<i class="fa fa-chevron-right"></i>',
    },
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-clock-o",
        clear: "fa fa-trash-o"
    },
    language: locale
};

function getDateInFormat(date = null) {
    if (date) {
        return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    }
}

function setDateAndStartDate(terms) {
    if ($("#recurring_invoice_start_date") && $("#recurring_invoice_start_date").length) {
        let startDate = $("#recurring_invoice_start_date").datepicker().val();
        if (startDate) {
            endDate = new Date(startDate);
            endDate.setDate(endDate.getDate() + terms);
            endDateString = endDate.getFullYear() + '-' + ('0' + (endDate.getMonth() + 1)).slice(-2) + '-' + ('0' + endDate.getDate()).slice(-2);
            $('#recurring_invoice_expiration_date').datepicker('setStartDate', endDate);
            $('#recurring_invoice_expiration_date').datepicker('setDate', endDate);
        }
    } else {
        let startDate = $("#invoice_start_date").datepicker().val(); // date in yyyy-mm-dd format
        // let startDate = $("#invoice_start_date").datepicker('getDate'); // full date with time
        if (startDate) {
            endDate = new Date(startDate);
            endDate.setDate(endDate.getDate() + terms);
            endDateString = endDate.getFullYear() + '-' + ('0' + (endDate.getMonth() + 1)).slice(-2) + '-' + ('0' + endDate.getDate()).slice(-2);
            $('#invoice_expiration_date').datepicker('setStartDate', endDate);
            $('#invoice_expiration_date').datepicker("setDate", endDate);
        }
    }
}

$(document).ready(function() {
    // On manual days change set the value of the date and start date option for invoice_expiration_date, recurring_invoice_expiration_date.
    $('input[name="manual_days"]').on('change', function(e) {
        let terms = +$(this).val() || 1;
        setDateAndStartDate(terms);
    });

    // On term change set the value of the date and start date option for invoice_expiration_date, recurring_invoice_expiration_date.
    $('select[name="term"]').on('change', function(e) {
        let terms = 0;
        let selectedTerm = $(this).find('option:selected').val();
        if (selectedTerm == "manual") {
            $('span#manual_days_section').show();
            terms = +$('input[name="manual_days"]').val() || 1;
        } else {
            $('span#manual_days_section').hide();
            terms = +selectedTerm || 0;
        }
        setDateAndStartDate(terms);
    });

    // todo:- need to consider this function if required as init.
    function populateEndDates() {
        let terms = $('select[name="term"]').val() == "manual" ? (+$('input[name="manual_days"]').val() || 1) : (+$('select[name="term"]').val() || 0);

        if ($("#recurring_invoice_start_date") && $("#recurring_invoice_start_date").length) {
            let invoiceStartDate = $("#recurring_invoice_start_date").datepicker('getDate');
            let invoiceEndDate = $("#recurring_invoice_end_date").datepicker('getDate');

            if (invoiceStartDate) {
                if (!invoiceEndDate || invoiceStartDate > invoiceEndDate) {
                    $('#recurring_invoice_end_date').datepicker("setDate", invoiceStartDate);
                }
                $('#recurring_invoice_end_date').datepicker('setStartDate', invoiceStartDate);
            }
        } else if ($("#invoice_start_date") && $("#invoice_start_date").length) {
            let invoiceStartDate = $('#invoice_start_date').datepicker('getDate');
            let invoiceExpirationDate = $('#invoice_expiration_date').datepicker('getDate');

            endDate = new Date(invoiceStartDate);
            endDate.setDate(endDate.getDate() + terms);

            if (invoiceStartDate) {
                if (!invoiceExpirationDate) {
                    $('#invoice_expiration_date').datepicker("setDate", endDate);
                } else {
                    if (invoiceStartDate > invoiceExpirationDate) {
                        $('#invoice_expiration_date').datepicker("setDate", invoiceStartDate);
                    }
                }
                $('#invoice_expiration_date').datepicker('setStartDate', endDate);
            }
        } else if ($("#estimate_start_date") && $("#estimate_start_date").length) {
            let estimateStartDate = $('#estimate_start_date').datepicker('getDate');
            let estimateExpirationDate = $('#estimate_expiration_date').datepicker('getDate');

            endDate = new Date(estimateStartDate);
            endDate.setDate(endDate.getDate() + terms);

            if (estimateStartDate) {
                if (!estimateExpirationDate) {
                    $('#estimate_expiration_date').datepicker("setDate", endDate);
                } else {
                    if (estimateStartDate > estimateExpirationDate) {
                        $('#estimate_expiration_date').datepicker("setDate", estimateStartDate);
                    }
                }
                $('#estimate_expiration_date').datepicker('setStartDate', endDate);
            }
        } else if ($("#purchase_order_date") && $("#purchase_order_date").length) {
            let purchaseOrderStartDate = $('#purchase_order_date').datepicker('getDate');
            let purchaseOrderExpirationDate = $('#purchase_order_expiration_date').datepicker('getDate');

            endDate = new Date(purchaseOrderStartDate);
            endDate.setDate(endDate.getDate() + terms);

            if (purchaseOrderStartDate) {
                if (!purchaseOrderExpirationDate) {
                    $('#purchase_order_expiration_date').datepicker("setDate", endDate);
                } else {
                    if (purchaseOrderStartDate > purchaseOrderExpirationDate) {
                        $('#purchase_order_expiration_date').datepicker("setDate", purchaseOrderStartDate);
                    }
                }
                $('#purchase_order_expiration_date').datepicker('setStartDate', endDate);
            }
        } else if ($("#payment_recurring_start_date") && $("#payment_recurring_start_date").length) {
            let paymentStartDate = $("#payment_recurring_start_date").datepicker('getDate');
            let paymentEndDate = $("#payment_recurring_end_date").datepicker('getDate');

            if (paymentStartDate) {
                if (!paymentEndDate || paymentStartDate > paymentEndDate) {
                    $('#payment_recurring_end_date').datepicker("setDate", paymentStartDate);
                }
                $('#payment_recurring_end_date').datepicker('setStartDate', paymentStartDate);
            }
        }
    }

    $(function() {
        $('#invoice_expiration_date').datepicker(pickerOptsGeneral);
        $('#invoice_start_date').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
            endDate = new Date(ev.date);
            terms = $('select[name="term"]').val() == "manual" ? (+$('input[name="manual_days"]').val() || 1) : (+$('select[name="term"]').val() || 0);
            endDate.setDate(endDate.getDate() + terms);

            $('#invoice_expiration_date').datepicker('setStartDate', endDate);

            let invoiceStartDate = $(this).datepicker('getDate');
            let invoiceExpirationDate = $('#invoice_expiration_date').datepicker('getDate');

            if (!invoiceExpirationDate) {
                $('#invoice_expiration_date').datepicker("setDate", endDate);
            } else {
                if (invoiceStartDate > invoiceExpirationDate) {
                    $('#invoice_expiration_date').datepicker("setDate", invoiceStartDate);
                } else {
                    $('#invoice_expiration_date').datepicker("setDate", endDate);
                }
            }
        });

        $('#recurring_invoice_end_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: tomorrow }));
        $('#recurring_invoice_expiration_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: tomorrow }));
        $('#recurring_invoice_start_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: tomorrow })).on('changeDate', function(ev) {
            endDate = new Date(ev.date);
            $('#recurring_invoice_end_date').datepicker('setStartDate', endDate);

            terms = $('select[name="term"]').val() == "manual" ? (+$('input[name="manual_days"]').val() || 1) : (+$('select[name="term"]').val() || 0);
            endDate.setDate(endDate.getDate() + terms);

            let invoiceStartDate = $(this).datepicker('getDate');
            let invoiceExpirationDate = $('#recurring_invoice_expiration_date').datepicker('getDate');
            let invoiceEndDate = $('#recurring_invoice_end_date').datepicker('getDate');

            if (invoiceStartDate && (!invoiceEndDate || invoiceStartDate > invoiceEndDate)) {
                $('#recurring_invoice_end_date').datepicker("setDate", invoiceStartDate);
            }

            $('#recurring_invoice_end_date').datepicker("setStartDate", invoiceStartDate);
            // if (invoiceStartDate > invoiceExpirationDate) {
            //     $('#recurring_invoice_expiration_date').datepicker("setDate", endDate);
            // }

        });
        $('#payment_received_start_date').datepicker(pickerOptsGeneral);
        $('#credit_note_date').datepicker(pickerOptsGeneral);
        $('#debit_note_date').datepicker(pickerOptsGeneral);

        $('#estimate_expiration_date').datepicker(pickerOptsGeneral);
        $('#estimate_start_date').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
            endDate = new Date(ev.date);

            $('#estimate_expiration_date').datepicker('setStartDate', endDate);

            let estimateStartDate = $(this).datepicker('getDate');
            let estimateExpirationDate = $('#estimate_expiration_date').datepicker('getDate');

            if (estimateStartDate > estimateExpirationDate) {
                $('#estimate_expiration_date').datepicker("setDate", endDate);
            }
        });
        $('#purchase_order_expiration_date').datepicker(pickerOptsGeneral);

        $('#purchase_order_date').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
            endDate = new Date(ev.date);

            $('#purchase_order_expiration_date').datepicker('setStartDate', endDate);

            let purchaseOrderStartDate = $(this).datepicker('getDate');
            let purchaseOrderExpirationDate = $('#purchase_order_expiration_date').datepicker('getDate');

            if (purchaseOrderStartDate > purchaseOrderExpirationDate) {
                $('#purchase_order_expiration_date').datepicker("setDate", endDate);
            }
        });

        $('#payment_recurring_end_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: tomorrow }));
        $('#payment_recurring_start_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: tomorrow })).on('changeDate', function(ev) {
            endDate = new Date(ev.date);

            $('#payment_recurring_end_date').datepicker('setStartDate', endDate);

            let paymentStartDate = $(this).datepicker('getDate');
            let paymentEndDate = $('#payment_recurring_end_date').datepicker('getDate');

            if (paymentStartDate) {
                if (!paymentEndDate || (paymentStartDate > paymentEndDate)) {
                    $('#payment_recurring_end_date').datepicker("setDate", paymentStartDate);
                }
            }
        });

        // when the page is loaded again with populated values then set the value of datepickers
        if ($("form#add-edit-invoices-form").length) {
            let terms = 0;
            let selectedTerm = $('form#add-edit-invoices-form select[name="term"] :selected').val();
            if (selectedTerm == "manual") {
                $('span#manual_days_section').show();
                terms = +$('input[name="manual_days"]').val() || 1;
            } else {
                $('span#manual_days_section').hide();
                terms = +selectedTerm || 0;
            }
            if (terms) {
                setDateAndStartDate(terms);
            }
        }

        // populate each end or ExpirationDate's startdate and default date value.
        populateEndDates();
        
        $('#init_quantity_date').datepicker(pickerOptsGeneral);
        $('#hiring_date').datepicker(pickerOptsGeneral);
        $('#final_date').datepicker(pickerOptsGeneral);
        /* $('#date_of_birth').datepicker(pickerOptsGeneral); */
        $('#date_of_birth').datepicker(Object.assign({}, pickerOptsGeneral, {
            endDate: new Date()
        }));
        $('#date_of_purchase').datepicker(Object.assign({}, pickerOptsGeneral, {
            startDate: startOfTheYear,
            endDate: new Date()
        }));
    });

    $('#date-filter-min').datepicker(Object.assign({}, pickerOptsGeneral, {
        changeMonth: true,
        changeYear: true
    }));
    $('#date-filter-max').datepicker(Object.assign({}, pickerOptsGeneral, {
        changeMonth: true,
        changeYear: true
    }));
    $('#date-filter-min').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
        endDate = new Date(ev.date);

        $('#date-filter-max').datepicker('setStartDate', endDate);

        let fromtDate = $(this).datepicker('getDate');
        let toDate = $('#date-filter-max').datepicker('getDate');

        if (fromtDate > toDate) {
            $('#date-filter-max').val("");
        }
    });
    $('#remission_start_date').datepicker(pickerOptsGeneral);
    $('#remission_expiration_date').datepicker(pickerOptsGeneral);
    $('#leave_from_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: new Date() }));
    $('#leave_to_date').datepicker(Object.assign({}, pickerOptsGeneral, { startDate: new Date() }));
    $('#leave_from_date, #leave_to_date').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
        endDate = $('#leave_from_date').val();

        $('#leave_to_date').datepicker('setStartDate', endDate);

        let fromtDate = $(this).datepicker('getDate');
        let toDate = $('#leave_to_date').datepicker('getDate');

        if (fromtDate > toDate) {
            $('#leave_to_date').val("");
            $("#number_of_days").val("");
        }
        let start = new Date( $('#leave_from_date').val() ).getTime(),
        end = new Date( $('#leave_to_date').val() ).getTime();
        let days = (end - start) / 86400000;
        if (start && end) {
            $("#number_of_days").val(days+1);
        }
    });
    
    $('#account_adjustment_date').datepicker(pickerOptsGeneral);
    /* $('#leave_to_date').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
       let start = new Date( $('#leave_from_date').val() ).getTime(),
       end = new Date( $('#leave_to_date').val() ).getTime();
       let days = (end - start) / 86400000;
       if (start && end) {
         $("#number_of_days").val(days+1);
       }
    }); */
    $('#payroll_start_date').datepicker(pickerOptsGeneral);
     $('#payroll_start_date').datepicker(pickerOptsGeneral).on('changeDate', function(ev) {
            endDate = new Date(ev.date);
            $('#payroll_end_date').datepicker('setStartDate', endDate);
    });
    $('#payroll_end_date').datepicker(pickerOptsGeneral);
    $('#extra_hours_start_time').datetimepicker({
        format: 'HH:mm A',
       /*  defaultDate: moment(), */
    });
    $('#extra_hours_end_time').datetimepicker({
        format: 'HH:mm A',
      /*   defaultDate: moment(), */
    });

    // for balance sheet
    $('#bs_end_date').datepicker(Object.assign({}, pickerOptsGeneral, { date: lastDayofMonth }));
    // for income statement
    $('#is_start_date').datepicker(Object.assign({}, pickerOptsGeneral, { date: startDayofMonth }));
    $('#is_end_date').datepicker(Object.assign({}, pickerOptsGeneral, { date: lastDayofMonth }));
    // for memeberships
    $('#membership_created_date').datepicker(Object.assign({}, pickerOptsGeneral, { date: today }));
    $('#membership_point_expire_date').datepicker(Object.assign({}, pickerOptsGeneral, { date: lastDayofMonth }));
    $('#table_date').datepicker(pickerOptsGeneral);
});

/* $(document).on("change","#leave_from_date,#leave_to_date  ",function(){
    let start = new Date( $('#leave_from_date').val() ).getTime(),
    end = new Date( $('#leave_to_date').val() ).getTime();
    let days = (end - start) / 86400000;
    if (end < start) {
        $("#leave_from_date").val();
    }
    else {
        $("#number_of_days").val(days+1);
    }
}); */

