$(document).ready(function() {
    populateExchangePrice();
});

// Currency Exchange //
$(document).on("keyup, change", "#exchange_rate", function() {
    populateExchangePrice();
});

$(document).on("change", "select[name='exchange_currency']", function() {
    let $this = $(this);
    let user_ex_rate = $this.find(':selected').attr('data-rate');
    if ($("select[name='exchange_currency'] option:selected").val() && user_ex_rate) {
        let ex_currency = $this.find(':selected').attr('data-code');
        $("input[name='exchange_rate']").val(user_ex_rate);
        let ex_rate = parseFloat($("#exchange_rate").val());
        $(".exchange_currency_label").text(ex_currency);
        $(".exchange_currency_rate").text(addZeroes(ex_rate, true, 6));

        $(".currency_exchange_inner").show();
    } else {
        $(".exchange_currency_rate").text('');
        $("input[name='exchange_rate']").val('');
        $(".currency_exchange_inner").hide();
    }
    invoiceCalulation = getInvoiceTotal(invoiceItems);
    populateTotalExchangePrice(invoiceCalulation.total);
});

const populateTotalExchangePrice = function(total = null) {
    if (($("select[name='exchange_currency'] option:selected").val() && $("#exchange_rate").val()) || ($("input[name='exchange_currency']").val() && $("#exchange_rate").val())) {
        let ex_currency = $('select[name="exchange_currency"]').find(':selected').attr('data-code');
        let ex_currency_symbol = $('select[name="exchange_currency"]').find(':selected').attr('data-symbol');
        let ex_rate = parseFloat($("#exchange_rate").val());
        $(".total-ex-currency-label").text(ex_currency);
        $(".total-ex-currency-label-symbol").text(ex_currency + " " + ex_currency_symbol);
        if (total) {
            let ex_value = (total / ex_rate);
            $("#total-ex-currency-rate").text(addZeroes(ex_value, true, 6));
        }
        $(".total_exchanged_rate").show();
        $(".total_currency_exchange_inner").show();
    } else {
        $(".total_exchanged_rate").hide();
        $(".total_currency_exchange_inner").hide();
    }
}

const populateExchangePrice = function() {
    if ($("select[name='exchange_currency'] option:selected").val() && $("#exchange_rate").val()) {
        let ex_rate = $("#exchange_rate").val();
        let ex_currency = $('select[name="exchange_currency"]').find(':selected').data('code');
        $(".exchange_currency_rate").text(addZeroes(ex_rate, true, 6));
        $(".exchange_currency_label").text(ex_currency);
        invoiceCalulation = getInvoiceTotal(invoiceItems);
        populateTotalExchangePrice(invoiceCalulation.total);
        $(".currency_exchange_inner").show();
    } else {
        $(".exchange_currency_rate").text('');
        $(".currency_exchange_inner").hide();
    }
}