$(document).ready(function() {
    generateHierarchySelect('concept-multilevelselect-0', 'input[name="items[0][concept-item]"]');
    generateHierarchySelect('concept-multilevelselect-1', 'input[name="items[1][concept-item]"]');

    $(document).on('keyup change', 'input[name^="items"]', function(e) {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        if (needToReplace == 'debit_amount' || needToReplace == 'credit_amount') {
            let otherElement = needToReplace == 'debit_amount' ? 
                        $('input[name="items['+key+'][credit_amount]"]') : 
                        $('input[name="items['+key+'][debit_amount]"]');
            if (Boolean($(this).val())) {
                otherElement.attr("readonly", "readonly");
                otherElement.val("");
            } else {
                otherElement.removeAttr("readonly")
            }
        }
        populateAdjustmentTotal();
    });

    $(".add-account-adjustment-row").click(async function() {
        let nextRowNumber = $('a.remove-account-adjustment-row').last().length ? $('a.remove-account-adjustment-row').last().data('row_number') + 1 : 1;
        $(this).attr("disabled", "disabled").addClass('btn-disabled');
        await generateAccountAdjustmentRow(nextRowNumber, 'account_adjustment_items');
    });
    $(document).on("click", "a.remove-account-adjustment-row", function(e) {
        $(this).closest('tr').remove();
    });

    $(document).click(function() {
        populateAdjustmentTotal();
    });

    populateAdjustmentTotal();
});

function populateAdjustmentTotal() {
    let debitAmounts = {};
    let creditAmounts = {};
    let debitAmountSum = 0.00;
    let creditAmountSum = 0.00;
    $('table#account_adjustment_items input[name^="items"]').each(function() {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        if (needToReplace == 'debit_amount' && $(this).val() != undefined && Boolean($(this).val())) {
            debitAmounts[key] = $(this).val();
        }
        if (needToReplace == 'credit_amount' && $(this).val() != undefined && Boolean($(this).val())) {
            creditAmounts[key] = $(this).val();
        }

        if (needToReplace == 'debit_amount' || needToReplace == 'credit_amount') {
            let otherElement = needToReplace == 'debit_amount' ? 
                        $('input[name="items['+key+'][credit_amount]"]') : 
                        $('input[name="items['+key+'][debit_amount]"]');
            if (Boolean($(this).val())) {
                otherElement.attr("readonly", "readonly");
                otherElement.val("");
            } else {
                otherElement.removeAttr("readonly")
            }
        }

    });
    var aKeys = Object.keys(debitAmounts).sort();
    var bKeys = Object.keys(creditAmounts).sort();

    if (aKeys.length && bKeys.length) {
        const invalidRows = aKeys.filter(value => bKeys.includes(value));
        if (invalidRows.length == 0) {
            debitAmountSum = Object.values(debitAmounts).map(Number).reduce((a, b) => a + b);
            creditAmountSum = Object.values(creditAmounts).map(Number).reduce((a, b) => a + b);
            $('td[id="debit_total"]').html(moneyFormat(debitAmountSum, true));
            $('td[id="credit_total"]').html(moneyFormat(creditAmountSum, true));
        }
    }
    $('td[id="debit_total"]').html(moneyFormat(debitAmountSum, true));
    $('td[id="credit_total"]').html(moneyFormat(creditAmountSum, true));
}
