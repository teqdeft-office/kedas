moment.locale(locale);
var pendingChart, paidChart, salesChart, expensesChart;

let colors = ['rgba(53, 156, 223, 0.6)', 'rgba(0, 75, 135, 0.9)', 'rgba(48, 227, 202, 0.7)', 'rgba(255, 230, 146, , 0.8)', 'rgba(249, 216, 99, 0.5)', 'rgb(53, 156, 223, 0.6)']
let borderColors = ['rgba(53, 156, 223, 0.8)', 'rgba(0, 64, 117, 0.9)', 'rgba(48, 227, 202, 0.9)', 'rgba(255, 230, 146, 0.9)'];
$(document).ready(async function() {
    getChartElements();
});

function getChartElements() {
    let salesExpensesPendingChartEle = $("#sales-expenses-pending-chart");
    let salesExpensesPaidChartEle = $("#sales-expenses-paid-chart");
    let salesChartEle = $("#sales-pie-chart");
    let expensesChartEle = $("#expenses-pie-chart");

    if (salesExpensesPendingChartEle.length || salesExpensesPaidChartEle.length || salesChartEle.length || expensesChartEle.length) {
        generateCharts(salesExpensesPendingChartEle, salesExpensesPaidChartEle, salesChartEle, expensesChartEle);
    }
}

async function generateCharts(salesExpensesPendingChartEle, salesExpensesPaidChartEle, salesChartEle, expensesChartEle) {
    let reportData = await getReportData();
    if (pendingChart) {
        pendingChart.destroy();
    }
    if (paidChart) {
        paidChart.destroy();
    }
    if (salesChart) {
        salesChart.destroy();
    }
    if (expensesChart) {
        expensesChart.destroy();
    }
    let pendingChartOptions = generateBarChartOptions(reportData.data.sales.pending, reportData.data.expenses.pending, salesExpensesPendingChartEle, 'sales-expenses-pending-no-data');
    let paidChartOptions = generateBarChartOptions(reportData.data.sales.paid, reportData.data.expenses.paid, salesExpensesPaidChartEle, 'sales-expenses-paid-no-data');
    let salesChartOptions = generatePieChartOptions(reportData.data.sales.best_clients, salesChartEle, 'sales-no-data');
    let expensesChartOptions = generatePieChartOptions(reportData.data.expenses.suppliers, expensesChartEle, 'expenses-no-data');

    if (salesExpensesPendingChartEle.length) {
        pendingChart = new Chart(salesExpensesPendingChartEle, pendingChartOptions);
    }
    if (salesExpensesPaidChartEle.length) {
        paidChart = new Chart(salesExpensesPaidChartEle, paidChartOptions);
    }
    salesChart = new Chart(salesChartEle, salesChartOptions);
    expensesChart = new Chart(expensesChartEle, expensesChartOptions);
}

function generateBarChartOptions(sales, expenses, chartEle, noDataEle) {
    let chart_type = $("select[name='filter-graph-data']").val();
    let months = moment.months();

    /** Get days in month */
    var daysInMonth = [];
    if (chart_type == "this_month") {
        var monthDate = moment().startOf('month');
    } else if (chart_type == "other_pass_months") {
        let graphTypeLastMonths = $("select[name='last-months-drop']").val();
        var monthDate = moment(graphTypeLastMonths, 'YYYY-MM').startOf('month');
    } else {
        var monthDate = moment().subtract(1, 'months').startOf('month');
    }
    for (let i = 0; i < monthDate.daysInMonth(); i++) {
        let newDay = monthDate.clone().add(i, 'days');
        daysInMonth.push(newDay.format('DD'));
    }

    /** Get weekdays */
    let weekdays = moment.weekdays();

    /** Get last seven days */
    let today = moment();
    let sevenDays = Array(7).fill().map(
        () => today.subtract(1, 'd').format('DD')
    );
    let lastSevenDays = sevenDays.reverse();

    //let chart_type = "this_week";
    let chart_label = "";
    if (chart_type == "this_week") {
        chart_label = weekdays;
    } else if (chart_type == "this_month" || chart_type == "last_month" || chart_type == "other_pass_months") {
        chart_label = daysInMonth;
    } else if (chart_type == "last_7_days") {
        chart_label = lastSevenDays;
    } else {
        chart_label = months;
    }

    let salesData = {
        label: trans('lang.labels.sales'),
        data: sales,
        backgroundColor: 'rgba(255, 230, 146, 0.9)',
        borderWidth: 0,
        yAxisID: "y-axis-sales",
        pointRadius: 0,
    };

    let expensesData = {
        label: trans('lang.labels.expenses'),
        data: expenses,
        backgroundColor: 'rgba(48, 227, 202, 0.9)',
        borderWidth: 0,
        yAxisID: "y-axis-expenses",
        pointRadius: 0,
    };

    let minYScalesData = salesData.data.concat(expensesData.data);
    let minYScale = Math.min.apply(null, minYScalesData);
    let maxYScale = Math.max.apply(null, minYScalesData);

    let salesExpensesChartData = {
        labels: chart_label,
        datasets: [salesData, expensesData]
    };

    let chartOptions = {
        responsive: true,
        elements: {
            point: {
                radius: 0
            },
            line: {
                fill: false
            }
        },
        title: {
            display: true,
            position: 'bottom',
            text: trans('lang.labels.sales_expenses'),
            fontSize: 14
        },
        scales: {
            xAxes: [{
                display: true,
                barPercentage: 1,
                categoryPercentage: 0.6,
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                display: false,
                type: "linear",
                id: "y-axis-sales",
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true,
                    stepSize: 100,
                    max: maxYScale
                }
                /*scaleLabel: {
                    display: true,
                    labelString: 'USD',
                    beginAtZero: true,
                },*/
            }, {
                display: true,
                type: "linear",
                id: "y-axis-expenses",
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true,
                    stepSize: 100,
                    max: maxYScale
                }
            }]
        },
        animation: {
            onComplete: function(animation) {
                let firstSet = animation.chart.config.data.datasets[0].data;
                let dataSum = firstSet.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
                if (typeof firstSet !== "object" || dataSum === 0) {
                    $('#' + noDataEle).css("display", 'block');
                    chartEle.css("display", 'none');
                } else {
                    $('#' + noDataEle).css("display", 'none');
                    chartEle.css("display", 'block');
                }
            }
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += moneyFormat(tooltipItem.value);
                    return label;
                }
            }
        }
    };
    return {
        type: 'bar',
        data: salesExpensesChartData,
        options: chartOptions
    };
}

function generatePieChartOptions(data, chartEle, noDataEle) {
    let labels = [];
    let chartValues = [];
    let chartPerValues = [];
    if (data.filter(Boolean).length) {
        labels = data.map(n => n.name);
        chartValues = data.map(n => n.total);
        chartPerValues = data.map(n => n.percentage);
    }
    return {
        type: 'pie',
        data: {
            labels,
            datasets: [{
                // label: '# of Votes',
                data: chartValues,
                // '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0'),
                backgroundColor: colors.slice(0, 6),
                pointRadius: 0,
                borderWidth: 1,
                hoverBorderWidth: 2,
                borderColor: borderColors.slice(0, 6),
                borderAlign: 'inner',
            }]
        },
        options: {
            // title: {
            //     display: false,
            //     text: 'Overall Activity'
            // },
            scales: {
                yAxes: [{
                    id: "y-axis-density",
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    }
                }]
            },
            animation: {
                onComplete: function(animation) {
                    let firstSet = animation.chart.config.data.datasets[0].data;
                    let dataSum = firstSet.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
                    if (typeof firstSet !== "object" || dataSum === 0) {
                        $('#' + noDataEle).css("display", 'block');
                        chartEle.css("display", 'none');
                    } else {
                        $('#' + noDataEle).css("display", 'none');
                        chartEle.css("display", 'block');
                    }
                }
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(tooltipItem, data) {
                        let datasets = data.datasets[tooltipItem.datasetIndex];
                        let label = data.labels[tooltipItem.index];
                        let value = moneyFormat(datasets.data[tooltipItem.index]) + ' (' + chartPerValues[tooltipItem.index] + '%)';

                        if (label) {
                            label += ': ';
                        }
                        label += value;
                        return label;
                    }
                }
            }
        }
    }
}

async function getReportData(type = null) {
    let result;
    let graphType = $("select[name='filter-graph-data']").val();
    let graphTypeLastMonths = $("select[name='last-months-drop']").val();
    if (graphTypeLastMonths) {
        var url = SITE_URL + "/dashboard?v=" + Date.now() + "&type=" + graphType + "&graphTypeLastMonths=" + graphTypeLastMonths;
    } else {
        var url = SITE_URL + "/dashboard?v=" + Date.now() + "&type=" + graphType;
    }
    try {
        result = await $.ajax({
            url: url,
            type: "GET",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(response) {
                // console.log(response);
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.error(errorObj);
    }
}

// Graph data filter //
$(document).on("change", "select[name='filter-graph-data']", function(e) {
    let val = $(this).val();
    $('#last-months-drop').hide();
    $('#last-months-drop').val("");
    if (val == "other_pass_months") {
        let month = moment();
        let lastMonths = Array(12).fill().map(
            () => month.subtract(1, 'months').format('MMM YYYY')
        );
        let option = '';
        for (let i = 0; i < lastMonths.length; i++) {
            //let value = moment(lastMonths[i]).format('YYYY-MM');
            let value = moment(lastMonths[i], 'MMM YYYY').format("YYYY-MM");
            option += '<option value="' + value + '">' + lastMonths[i] + '</option>';
        }
        $('#last-months-drop').append(option);
        $('#last-months-drop').show();
    } else {
        getChartElements();
    }
});

$(document).on("change", "select[name='last-months-drop']", function(e) {
    getChartElements();
});