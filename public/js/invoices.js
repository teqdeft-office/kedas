let shippingCost = 0;
$(document).ready(function(){
    shippingCost = Number($('#shippingValues').val());
    if(isNaN(shippingCost)){
        shippingCost = 0;
    }
    $('#shippingInput').click(function(){
        if($(this).prop("checked") == true){
            shippingCost = Number($('#shippingValues').val());
            console.log('asd', shippingCost);
            $('#shippingDiv').show();
        }
        else if($(this).prop("checked") == false){
            shippingCost = 0;
            console.log('asdf', shippingCost);
            $('#shippingDiv').hide();
            $('#shippingValues').val(0)
        }
    });
    $('#shippingValues').change(function(){
        shippingCost = Number($(this).val());
        
    });
    $("#shippingInput").prop( "checked", true );

});
let invoiceItems = [];
let contact_membership_id;

$(document).ready(function() {
    $(document).on('change', 'select[name="contact_id"]', function(e) {
        contact_tax_id = $(this).find('option:selected').data("tax_id");
        contact_phone = $(this).find('option:selected').data("phone");
        contact_membership_id = $(this).find('option:selected').data("membership_id");
        $('#contact-phone').val(contact_phone);
        $('#contact-tax-id').val(contact_tax_id);

        $('select[name$="[item]"]').each(function(k, ele) {
        	updatePriceBasedOnMembership(contact_membership_id, ele);
        });
   	});

    $(document).on('change', 'input[name^="products"]', function(e) {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        if (needToReplace == 'quantity') {
            alertForOutOfStock();
        }

        populateTotalPrice(eleName, needToReplace, key);
    });

    $(document).on('change', 'select[name^="products"]', async function(e) {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        if (needToReplace === "item") {
            let price = $(this).find('option:selected').data("sale_price");
            let tax_id = $(this).find('option:selected').data("tax_id") || 0;

            $('input[name="' + eleName.replace(needToReplace, 'shippingCost') + '"]').val(shippingCost);
            $('input[name="' + eleName.replace(needToReplace, 'reference') + '"]').val($(this).find('option:selected').data("reference"));
            $('textarea[name="' + eleName.replace(needToReplace, 'description') + '"]').val($(this).find('option:selected').data("description"));
            $('select[name="' + eleName.replace(needToReplace, 'tax') + '"]').val(tax_id);
            $('input[name="' + eleName.replace(needToReplace, 'price') + '"]').val(price);
            // solution:- in case of select2 the tax element value is changed but the labels are not updating
            $('select[name="' + eleName.replace(needToReplace, 'tax') + '"]').trigger('change')

            await alertForOutOfStock();

        	updatePriceBasedOnMembership(contact_membership_id, this);
        }

        populateTotalPrice(eleName, needToReplace, key);
    });

    $(document).click(function() {
        populateInvoiceTotal();
    });

    $(".add-row").click(async function() {
        // let nextRowNumber = $('#invoice_items > tbody tr').length;
        let nextRowNumber = $('a.remove-row').last().length ? $('a.remove-row').last().data('row_number') + 1 : 1;
        await generateItemRow(nextRowNumber, 'invoice_items');
    });
    $(document).on("click", "a.remove-row", function(e) {
        $(this).closest('tr').remove();
        delete invoiceItems[$(this).data('row_number')];
        populateInvoiceTotal();
    });
    $("table#invoice_items > tbody tr").each(function(i, item) {
        let eleName = $(this).find('input[name^="products"]').first().attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        /*let price = $(this).find("input[name='"+eleBaseName+"[price]']").val();
        let discount = $(this).find("input[name='"+eleBaseName+"[discount]']").val() || 0;
        let quantity = $(this).find("input[name='"+eleBaseName+"[quantity]']").val() || 1;
        let tax = $(this).find("select[name='"+eleBaseName+"[tax]'] :selected").data('percentage') || 0;*/
        populateTotalPrice(eleName, needToReplace, key);
    });

    // barcode search feature while creating invoice.
    $(document).on('keypress', 'input[name="barcode"]', async function(e) {
        // as space is not allowed.
        if (e.which == 32){
            return false;
        }
        // On enter press.
        if (e.keyCode == 13) {
            e.preventDefault(); 
            let productInItems = false;
            let barcodeVal = $(this).val().trim();

            // as the barcode should minimum 6 characters long
            if (barcodeVal.length >= 6) {
                $(this).attr("disabled", "disabled");
                searchResponse = await productSearch(barcodeVal);
                if (searchResponse.data.length) {
                    searchedProduct = searchResponse.data[0];

                    $('select[name$="[item]"]').each(function(k, ele) {
                        if ($(ele).val() == searchedProduct.id) {
                            productInItems = true;
                            elementBaseName = $(ele).attr('name').replace("[item]", "");
                            $("input[name='" + elementBaseName + "[quantity]']").val(+$("input[name='" + elementBaseName + "[quantity]']").val() + 1).change();
                            return false;
                        }
                    });

                    if (!productInItems) {
                    	let productAdded = false;
                        $('select[name$="[item]"]').each(function(k, ele) {
                            if (!$(ele).val()) {
                            	setTimeout(function() {
	                                $("select[name='"+$(ele).attr('name')+"']").val(String(searchedProduct.id)).trigger('change');
	                            }, 500);
                                productAdded = true;
                                return false;
                            }
                        });
                        if (!productAdded) {
                            let keyOfNewRow = $('select[name$="[item]"]').length;
                            $(".add-row").click();
                            setTimeout(function() {
                                $("select[name='products["+keyOfNewRow+"][item]']").val(String(searchedProduct.id)).trigger('change');
                            }, 500);
                        }
                    }
                } else {
                    toastr.info(trans('lang.messages.no_product_found'));
                }
            } else {
                toastr.error(trans('lang.validation.min.string', { attribute: trans('lang.labels.barcode'), min: 6 }));
            }
            return false;
        }
    });

    $('select[name$="[item]"]').each(function(k, ele) {
    	contact_membership_id = $('select[name="contact_id"]').find('option:selected').data("membership_id");
    	updatePriceBasedOnMembership(contact_membership_id, ele);
    });
});

var populateInvoiceTotal = function() {
    invoiceCalulation = getInvoiceTotal(invoiceItems, shippingCost);
    $('td[id="invoice_basetotal"]').html(moneyFormat(invoiceCalulation.baseTotal, true));
    $('td[id="invoice_discount"]').html(moneyFormat(invoiceCalulation.discountTotal, true));
    $('td[id="shippingCost"]').html(moneyFormat(shippingCost, true));
    $('td[id="invoice_subtotal"]').html(moneyFormat(invoiceCalulation.subTotal, true));
    $('td[id="invoice_total"]').html(moneyFormat(invoiceCalulation.total, true));
    $('input[id="invoice_total_hidden"]').val(invoiceCalulation.total);
    $('input[id="credit_note_total_hidden"]').val(invoiceCalulation.total);
    if (Object.keys(invoiceCalulation.taxInformation).length && invoiceCalulation.taxInformation.constructor === Object) {
        let obj = invoiceCalulation.taxInformation;
        $(".tax_row").remove();
        // Instead of showing each tax row show the single row as tax
        // for (var key in obj) {
        //     if (obj.hasOwnProperty(key)) {
        //         $('<tr class="tax_row"><th>'+obj[key].name+'</th><td>'+CURRENCY_SYMBOL+obj[key].amount+'</td></tr>').insertBefore($('td[id="invoice_total"]').closest('tr'));
        //     }
        // }
        $('<tr class="tax_row"><th>' + trans('lang.labels.tax') + '</th><td>' + moneyFormat(invoiceCalulation.totalTaxAmount, true) + '</td></tr>').insertBefore($('td[id="invoice_total"]').closest('tr'));
    }

    // Show Exchange Currency //
    populateTotalExchangePrice(invoiceCalulation.total);
}

var populateTotalPrice = function(eleName, needToReplace, key = 0) {
    let quantity = $('input[name="' + eleName.replace(needToReplace, 'quantity') + '"]').val();
    let discount = $('input[name="' + eleName.replace(needToReplace, 'discount') + '"]').val()
    let price = $('input[name="' + eleName.replace(needToReplace, 'price') + '"]').val();

    let tax_per = $('select[name="' + eleName.replace(needToReplace, 'tax') + '"] :selected').data("percentage");
    let tax_id = $('select[name="' + eleName.replace(needToReplace, 'tax') + '"] :selected').val();
    let tax_name = $('select[name="' + eleName.replace(needToReplace, 'tax') + '"] :selected').html();

    let item_id = $('select[name="' + eleName.replace(needToReplace, 'item') + '"] :selected').val();

    let calculationResult = getItemCost(price, quantity, discount, { tax_per, tax_id, tax_name }, item_id);

    if (!isNaN(calculationResult.baseTotal)) {
        $('span[id="' + eleName.replace(needToReplace, 'total') + '"]').html(CURRENCY_SYMBOL + addZeroes(calculationResult.total, true));
        $('input[name="' + eleName.replace(needToReplace, 'tax_amount') + '"]').val(calculationResult.taxAmount);
    } else {
        $('span[id="' + eleName.replace(needToReplace, 'total') + '"]').html(CURRENCY_SYMBOL + "0.00");
        $('input[name="' + eleName.replace(needToReplace, 'tax_amount') + '"]').val("0.00");
    }
    invoiceItems[key] = calculationResult;
    populateInvoiceTotal();
}

function updatePriceBasedOnMembership(contact_membership_id, ele) {
    if (typeof mapInventoryWithMembership == "undefined") {
        return false;
    }
	// if the row has selected product.
	if ($(ele).val()) {
		let costPrice = $(ele).find('option:selected').data("unit_cost");
		let salePrice = $(ele).find('option:selected').data("sale_price");
		let elementBaseName = $(ele).attr('name').replace("[item]", "");
		let inventoryMemberships = mapInventoryWithMembership[$(ele).val()];
		if (contact_membership_id) {
			// check if the selected product has membership and include contact's membership.
			if (inventoryMemberships && inventoryMemberships.length && inventoryMemberships.includes(contact_membership_id)) {
				let discountPer = mapMembershipWithDiscount[contact_membership_id];
				let finalPrice = getInventoryPriceAfterMembership(salePrice, costPrice, discountPer);
				
            	$("input[name='" + elementBaseName + "[price]']").val(finalPrice).change();
			} else {
				// reset to the sale price.
				$("input[name='" + elementBaseName + "[price]']").val(salePrice).change();
			}
		} else {
			// reset to the sale price.
			$("input[name='" + elementBaseName + "[price]']").val(salePrice).change();
		}
	}
}

var alertForOutOfStock = async function() {
    // Show out of stock alert box.
    let inventoryDetail = await getInventories();
    inventoryDetail = inventoryDetail.data.filter(id => id.track_inventory == "1");

    if (inventoryDetail) {
        let quantityInInvoice = {};
        let outOfStock = [];

        invoiceItems.forEach(function(item) {
            if (quantityInInvoice.hasOwnProperty(item.item_id)) {
                quantityInInvoice[item.item_id] = Number(quantityInInvoice[item.item_id]) + Number(item.quantity)
            } else {
                quantityInInvoice[item.item_id] = Number(item.quantity);
            }
        });

        for (let itemId in quantityInInvoice) {
            if (quantityInInvoice.hasOwnProperty(itemId)) {
                if (itemId) {
                    let inventoryItem = inventoryDetail.find(invIt => invIt['id'] == itemId);
                    if (inventoryItem && quantityInInvoice[itemId] > inventoryItem.quantity) {
                        outOfStock.push(inventoryItem)
                    }
                }
            }
        }

        if (outOfStock.length) {
            toastr.warning(trans('lang.messages.item_out_of_stock'));
        }
    }
}

async function productSearch(barcode) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/inventories?barcode=" + barcode,
            type: "GET",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(inventories) {

            },
        });
        $('input[name="barcode"]').attr("disabled", false).focus();
        return result;
    } catch (error) {
        $('input[name="barcode"]').attr("disabled", false).focus();
        errorObj = $.parseJSON(error.responseText);
        toastr.error(errorObj);
    }
}