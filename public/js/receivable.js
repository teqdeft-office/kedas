let invoiceItems = [];
let contact_membership_id;

$(document).ready(function() {
	$(document).on('change', 'select[name="contact_id"]', function(e) {
        contact_tax_id = $(this).find('option:selected').data("tax_id");
        contact_phone = $(this).find('option:selected').data("phone");
        contact_membership_id = $(this).find('option:selected').data("membership_id");
        $('#contact-phone').val(contact_phone);
        $('#contact-tax-id').val(contact_tax_id);

        // $('select[name$="[item]"]').each(function(k, ele) {
        // 	updatePriceBasedOnMembership(contact_membership_id, ele);
        // });
   	});
    $(document).on('change', 'input[name^="concepts"]', function(e) {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));
        populateTotalPrice(eleName, needToReplace, key);
    });

    $(document).on('change', 'input[name^="concepts"]', function(e) {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        populateTotalPrice(eleName, needToReplace, key);
    });

    $(document).on('change', 'select[name^="concepts"]', function(e) {
        let eleName = $(this).attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        if (needToReplace === "item") {
            let price = $(this).find('option:selected').data("unit_cost");
            let tax_id = $(this).find('option:selected').data("tax_id") || 0;

            $('select[name="' + eleName.replace(needToReplace, 'tax') + '"]').val(tax_id);
            $('input[name="' + eleName.replace(needToReplace, 'price') + '"]').val(price);
            // solution:- in case of select2 the tax element value is changed but the labels are not updating
            $('select[name="' + eleName.replace(needToReplace, 'tax') + '"]').trigger('change')
        }

        populateTotalPrice(eleName, needToReplace, key);
    });

    $(document).click(function() {
        populateInvoiceTotal();
    });

    $(".add-concept-row").click(async function() {
        let nextRowNumber = $('a.remove-concept-row').last().length ? $('a.remove-concept-row').last().data('row_number') + 1 : 1;
        await generateConceptRow(nextRowNumber, 'concept_items');
    });
    $(".add-concept-with-product-row").click(async function() {
        let nextRowNumber = $('a.remove-concept-row').last().length ? $('a.remove-concept-row').last().data('row_number') + 1 : 1;
        await generateConceptRow(nextRowNumber, 'concept_items', true);
    });
    $(document).on("click", "a.remove-concept-row", function(e) {
        $(this).closest('tr').remove();
        delete invoiceItems[$(this).data('row_number')];
        populateInvoiceTotal();
    });
    $("table#concept_items > tbody tr").each(function(i, item) {
        let eleName = $(this).find('input[name^="concepts"]').first().attr('name');
        let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
        let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

        populateTotalPrice(eleName, needToReplace, key);
    });
});

var populateInvoiceTotal = function() {
    invoiceCalulation = getInvoiceTotal(invoiceItems);
    $('td[id="invoice_basetotal"]').html(moneyFormat(invoiceCalulation.baseTotal, true));
    $('td[id="invoice_discount"]').html(moneyFormat(invoiceCalulation.discountTotal, true));
    $('td[id="invoice_subtotal"]').html(moneyFormat(invoiceCalulation.subTotal, true));
    $('td[id="invoice_total"]').html(moneyFormat(invoiceCalulation.total, true));
    $('input[id="invoice_total_hidden"]').val(invoiceCalulation.total);
    if (Object.keys(invoiceCalulation.taxInformation).length && invoiceCalulation.taxInformation.constructor === Object) {
        let obj = invoiceCalulation.taxInformation;
        $(".tax_row").remove();
        /*for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                $('<tr class="tax_row"><th>'+obj[key].name+'</th><td>'+CURRENCY_SYMBOL+obj[key].amount+'</td></tr>').insertBefore($('td[id="invoice_total"]').closest('tr'));
            }
        }*/
        $('<tr class="tax_row"><th>' + trans('lang.labels.tax') + '</th><td>' + moneyFormat(invoiceCalulation.totalTaxAmount, true) + '</td></tr>').insertBefore($('td[id="invoice_total"]').closest('tr'));
    }
    // Show Exchange Currency //
    populateTotalExchangePrice(invoiceCalulation.total);
}

var populateTotalPrice = function(eleName, needToReplace, key = 0) {
    let quantity = $('input[name="' + eleName.replace(needToReplace, 'quantity') + '"]').val();
    let discount = $('input[name="' + eleName.replace(needToReplace, 'discount') + '"]').val()
    let price = $('input[name="' + eleName.replace(needToReplace, 'price') + '"]').val();

    let tax_per = $('select[name="' + eleName.replace(needToReplace, 'tax') + '"] :selected').data("percentage");
    let tax_id = $('select[name="' + eleName.replace(needToReplace, 'tax') + '"] :selected').val();
    let tax_name = $('select[name="' + eleName.replace(needToReplace, 'tax') + '"] :selected').html();

    let item_id = $('select[name="' + eleName.replace(needToReplace, 'item') + '"] :selected').val();

    let calculationResult = getItemCost(price, quantity, discount, { tax_per, tax_id, tax_name }, item_id);

    if (!isNaN(calculationResult.baseTotal)) {
        $('span[id="' + eleName.replace(needToReplace, 'total') + '"]').html(CURRENCY_SYMBOL + addZeroes(calculationResult.total, true));
        $('input[name="' + eleName.replace(needToReplace, 'tax_amount') + '"]').val(calculationResult.taxAmount);
    } else {
        $('span[id="' + eleName.replace(needToReplace, 'total') + '"]').html(CURRENCY_SYMBOL + "0.00");
        $('input[name="' + eleName.replace(needToReplace, 'tax_amount') + '"]').val("0.00");
    }
    invoiceItems[key] = calculationResult;
    populateInvoiceTotal();
}