$(document).ready(function() {
    let userProfileFormEle = $("#user-profile-form");
    let inventoryTypeFormEle = $("#inventory-type-form");
    $('input[type=radio][name=user_type]').change(function() {
        $(this).parents(".steps-form").hide();
        $(this).parents(".steps-form").next().show();
    });

    $(".user-profile-close").click(function() {
        $("body").removeClass('body-fix');
        // todo:- redirect to show the sidebar options for company owner or find alternative.
        setTimeout(function() {
            location.reload();
        }, 1000);
    })

    $("button[name=next]").click(async function() {
        userProfileFormEle.submit();
        if (userProfileFormEle.valid()) {
            $(this).toggleClass('btn-disabled');
            $("span#loader").show();
            data = userProfileFormEle.serialize();
            let result = await submitUserProfile(data);

            if (result && result['success']) {
                $("span#loader").hide();
                $(this).parents(".steps-form").hide();
                $(this).parents(".steps-form").next().show();
            }
        }
    });
    $("button[name=back]").click(function() {
        $(this).parents(".steps-form").hide();
        $(this).parents(".steps-form").prev().show();
    });

    $("button[name=start]").click(function() {
        $(this).parents(".welcome-popup-outer").hide();
    });

    $("button[name=close-steps]").click(function() {
        $(this).parents(".welcome-popup-outer").hide();
    });

    $(".file-input input[type='file']").change(function(e) {
        let uplocvfile = '';
        if (e.target.files.length) {
            uplocvfile = e.target.files[0].name;
        }
        $('#' + $(this).data('label_id')).text(uplocvfile);
    });

    $(".menu-icon").click(function() {
        $(".main-wrapper .side-blue-sec").fadeToggle("");
        $(".main-wrapper .dashboard-right").toggleClass('dashboard-right-small');
    });
    $(".customize-options a").click(function() {
        $(".dropdown-menu.dropdown-menu-custom").fadeToggle("");
    });

    $(".file-upload-preview").on('change', function() {
        readURL(this, $(this).data('img_id'));
        let label_id = '#' + $(this).data('label_id');
        $(label_id).hide();
    });
    $(".upload-button").on('click', function(e) {
        e.preventDefault();
        $("#" + $(this).data('preview_section')).click();
    });

    $(document).on('change', 'form#add-payment-out-form input[name="transaction_with_invoice"], form#edit-payment-out-form input[name="transaction_with_invoice"]', function(e) {
        let transaction_with_invoice = $(this).val();
        if (transaction_with_invoice == "1") {
            $('section#transaction_concepts_rows').hide();
            $('div#transaction_invoices_rows').show();
        } else {
            $('div#transaction_invoices_rows').hide();
            $('section#transaction_concepts_rows').show();
        }
    });

    $(document).on('change', 'form#add-payment-received-form select[name="contact_id"]', function(e) {
        let contact_id = $(this).find('option:selected').val();
        if (contact_id) {
            getContactInvoicesRows(contact_id, 'sale', 'create', 'client_invoices');
            getContactCreditNotes(contact_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.client_invoice_without_contact') + '</p></td></tr>';
            $("#client_invoices tbody").html(emptyRow);
            $('#credit_note_balance').html('');
        }
    });

    $(document).on('change', 'form#edit-payment-received-form select[name="contact_id"]', function(e) {
        let contact_id = $(this).find('option:selected').val();
        if (contact_id) {
            let transaction_id = $("form#edit-payment-received-form").data("transaction-id");
            getContactInvoicesRows(contact_id, 'sale', 'edit', 'client_invoices', transaction_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.client_invoice_without_contact') + '</p></td></tr>';
            $("#client_invoices tbody").html(emptyRow);
            $('#credit_note_balance').html('');
        }
    });

    $(document).on('change', 'form#add-payment-out-form select[name="contact_id"]', function(e) {
        let contact_id = $(this).find('option:selected').val();
        if (contact_id) {
            getContactInvoicesRows(contact_id, 'supplier', 'create', 'supplier_invoices');
            getContactDebitNotes(contact_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.supplier_invoice_without_contact') + '</p></td></tr>';
            $("#supplier_invoices tbody").html(emptyRow);
            $('#debit_note_balance').html('');
        }
    });

    $(document).on('change', 'form#edit-payment-out-form select[name="contact_id"]', function(e) {
        let contact_id = $(this).find('option:selected').val();
        if (contact_id) {
            let transaction_id = $("form#edit-payment-out-form").data("transaction-id");
            getContactInvoicesRows(contact_id, 'supplier', 'edit', 'supplier_invoices', transaction_id);
            getContactDebitNotes(contact_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.supplier_invoice_without_contact') + '</p></td></tr>';
            $("#supplier_invoices tbody").html(emptyRow);
            $('#debit_note_balance').html('');
        }
    });

    $(document).on('change', 'form#edit-credit-notes-form select[name="contact_id"]', function(e) {
        let contact_id = $(this).find('option:selected').val();
        if (contact_id) {
            let credit_note_id = $("form#edit-credit-notes-form").data("credit-note-id");
            getContactInvoicesRows(contact_id, 'sale', 'edit', 'client_invoices', null, credit_note_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.credit_notes_without_contact') + '</p></td></tr>';
            $("#client_invoices tbody").html(emptyRow);
        }
    });

    if ($("form#add-payment-received-form").length) {
        let contact_id = $('form#add-payment-received-form select[name="contact_id"] :selected').val();
        if (contact_id) {
            getContactInvoicesRows(contact_id, 'sale', 'create', 'client_invoices');
            getContactCreditNotes(contact_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.client_invoice_without_contact') + '</p></td></tr>';
            $("#client_invoices tbody").html(emptyRow);
            $('#credit_note_balance').html('');
        }
    }

    if ($("form#edit-payment-received-form").length) {
        let contact_id = $('form#edit-payment-received-form select[name="contact_id"] :selected').val();
        if (contact_id) {
            let transaction_id = $("form#edit-payment-received-form").data("transaction-id");
            getContactInvoicesRows(contact_id, 'sale', 'edit', 'client_invoices', transaction_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.client_invoice_without_contact') + '</p></td></tr>';
            $("#client_invoices tbody").html(emptyRow);
            $('#credit_note_balance').html('');
        }
    }

    if ($("form#edit-credit-notes-form").length) {
        let contact_id = $('form#edit-credit-notes-form select[name="contact_id"] :selected').val();
        if (contact_id) {
            let credit_note_id = $("form#edit-credit-notes-form").data("credit-note-id");
            getContactInvoicesRows(contact_id, 'sale', 'edit', 'client_invoices', null, credit_note_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.credit_notes_without_contact') + '</p></td></tr>';
            $("#client_invoices tbody").html(emptyRow);
        }
    }

    if ($("form#add-payment-out-form").length) {
        let contact_id = $('form#add-payment-out-form select[name="contact_id"] :selected').val();
        if (contact_id) {
            getContactInvoicesRows(contact_id, 'supplier', 'create', 'supplier_invoices');
            getContactDebitNotes(contact_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.supplier_invoice_without_contact') + '</p></td></tr>';
            $("#supplier_invoices tbody").html(emptyRow);
            $('#debit_note_balance').html('');
        }
    }

    if ($("form#edit-payment-out-form").length) {
        let contact_id = $('form#edit-payment-out-form select[name="contact_id"] :selected').val();
        if (contact_id) {
            let transaction_id = $("form#edit-payment-out-form").data("transaction-id");
            getContactInvoicesRows(contact_id, 'supplier', 'edit', 'client_invoices', transaction_id);
            getContactDebitNotes(contact_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.supplier_invoice_without_contact') + '</p></td></tr>';
            $("#supplier_invoices tbody").html(emptyRow);
            $('#debit_note_balance').html('');
        }
    }

    /*if ($("form#add-edit-invoices-form").length) {
        let term = $('form#add-edit-invoices-form select[name="term"] :selected').val();
        if (term == "manual") {
            $('span#manual_days_section').show();
        } else {
            $('span#manual_days_section').hide();
        }
    }*/
    if ($("form#add-payment-out-form").length) {
        let transaction_with_invoice = $('form#add-payment-out-form input[name="transaction_with_invoice"]:checked').val();
        if (transaction_with_invoice == "1") {
            $('section#transaction_concepts_rows').hide();
            $('div#transaction_invoices_rows').show();
        } else if (transaction_with_invoice == "0") {
            $('div#transaction_invoices_rows').hide();
            $('section#transaction_concepts_rows').show();
        }
    }
    if ($("form#edit-payment-out-form").length) {
        let transaction_with_invoice = $('form#edit-payment-out-form input[name="transaction_with_invoice"]:checked').val();
        if (transaction_with_invoice == "1") {
            $('section#transaction_concepts_rows').hide();
            $('div#transaction_invoices_rows').show();
        } else if (transaction_with_invoice == "0") {
            $('div#transaction_invoices_rows').hide();
            $('section#transaction_concepts_rows').show();
        }
    }

    $(document).on("click", "a.delete_resource", function(e) {
        e.preventDefault();
        formId = $(e.currentTarget).data("resource");

        $.confirm({
            buttons: {
                tryAgain: {
                    text: trans('lang.labels.yes_delete'),
                    btnClass: 'btn-red',
                    action: function() {
                        document.getElementById(formId).submit()
                    }
                },
                cancel: {
                    text: trans('lang.labels.cancel'),
                    btnClass: 'btn-default',
                    action: function() {}
                },
            },
            icon: 'fa fa-exclamation-triangle',
            title: trans('lang.labels.are_you_sure'),
            content: trans('lang.labels.are_you_sure_delete_line'),
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            theme: 'modern',
            animation: 'scale',
            backgroundDismissAnimation: 'shake',
            draggable: false
        });
    });

    $(document).on("click", "a.void_resource", function(e) {
        e.preventDefault();
        formId = $(e.currentTarget).data("resource");

        $.confirm({
            buttons: {
                tryAgain: {
                    text: trans('lang.labels.yes_void'),
                    btnClass: 'btn-red',
                    action: function() {
                        document.getElementById(formId).submit()
                    }
                },
                cancel: {
                    text: trans('lang.labels.cancel'),
                    btnClass: 'btn-default',
                    action: function() {}
                },
            },
            icon: 'fa fa-exclamation-triangle',
            title: trans('lang.labels.are_you_sure'),
            content: trans('lang.labels.are_you_sure_void_line'),
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            theme: 'modern',
            animation: 'scale',
            backgroundDismissAnimation: 'shake',
            draggable: false
        });
    });

    $(document).on("click", "a.approve_resource", function(e) {
        e.preventDefault();
        formId = $(e.currentTarget).data("resource");

        $.confirm({
            buttons: {
                tryAgain: {
                    text: trans('lang.labels.yes_approved'),
                    btnClass: 'btn-red',
                    action: function() {
                        document.getElementById(formId).submit()
                    }
                },
                cancel: {
                    text: trans('lang.labels.cancel'),
                    btnClass: 'btn-default',
                    action: function() {}
                },
            },
            icon: 'fa fa-exclamation-triangle',
            title: trans('lang.labels.are_you_sure'),
            content: trans('lang.labels.are_you_sure_approve_line'),
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            theme: 'modern',
            animation: 'scale',
            backgroundDismissAnimation: 'shake',
            draggable: false
        });
    });

    $(document).on("click", "a#change-password-link", function(e) {
        if ($("div#change-password-section").hasClass('show-section')) {
            $("div#change-password-section").addClass('hide-section');
            $("div#change-password-section").removeClass('show-section');
            $("input[name=current_password]").rules( 'remove' );
            $("input[name=password]").rules( 'remove' );
            $("input[name=password_confirmation]").rules( 'remove' );
        } else {
            $("div#change-password-section").removeClass('hide-section');
            $("div#change-password-section").addClass('show-section');
            $("input[name=current_password]").rules("add", {
                required: true,
                minlength: 6,
                messages: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.password'), min: 6 })
                }
            });
            $("input[name=password]").rules("add", {
                required: true,
                minlength: 6,
                notEqualTo: "#current_password",
                messages: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.new')+" "+trans('lang.labels.password'), }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.new')+" "+trans('lang.labels.password'), min: 6 }),
                    notEqualTo: trans('lang.validation.different', { attribute: trans('lang.labels.new')+" "+trans('lang.labels.password'), other: trans('lang.labels.password')})
                }
            });
            $("input[name=password_confirmation]").rules("add", {
                required: true,
                minlength: 6,
                equalTo: "#password",
                messages: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.confirm_password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.confirm_password'), min: 6 }),
                    equalTo: trans('lang.validation.confirmed', { attribute: trans('lang.labels.new')+" "+trans('lang.labels.password') })
                }
            });
        }
    });

    $(document).on('change', 'form#add-edit-inventories-form select[name="tax_id"], form#add-edit-inventories-form input[name="sale_price"]', populateProductTotal);
    $('#invoiceEmailModal').on('hidden.bs.modal', function(e) {
        $("form#send-invoice-mail")[0].reset();
    });

    generateSelect2("single-search-selection");
    generateSelect2("small-single-search-selection", {
        selectionCssClass: 'small-selector-selection',
        dropdownCssClass: 'small-selector-dropdown',
        width: '295px'
    });
    generateSelect2("multiple-search-selection");

    /*$(".multiselect-custom").on('change', function() {
        $("option", $(this)).each(function (index) {
            if ($(this).is(":selected")) {
                $(this).css({backgroundColor: "#ddd"});
            }
            else {
                $(this).css({backgroundColor: "#f7f7f7"});
            }
        });
    });*/

    $(document).on('change', 'form#add-edit-inventories-form input[name="track_inventory"]', function(e) {
        if (this.checked) {
            $('div#inventory_details').show();
        } else {
            $('div#inventory_details').hide();
        }
    });

    $(document).on('change', 'form#add-edit-inventories-form input[name="is_product_sale"]', function(e) {
        if (this.checked) {
            $('div#product_for_sale_accounts').show();
        } else {
            $('div#product_for_sale_accounts').hide();
        }
    });

    $(document).on('change', 'form#add-edit-inventories-form input[name="is_product_purchase"]', function(e) {
        if (this.checked) {
            $('div#product_for_purchase_accounts').show();
        } else {
            $('div#product_for_purchase_accounts').hide();
        }
    });

    if ($("form#add-edit-inventories-form").length) {
        /*if ($('form#add-edit-inventories-form input[name="track_inventory"]').is(":checked")) {
            $('div#inventory_details').show();
        } else {
            $('div#inventory_details').hide();
        }*/

        if ($('form#add-edit-inventories-form input[name="is_product_sale"]').is(":checked")) {
            $('div#product_for_sale_accounts').show();
        }

        if ($('form#add-edit-inventories-form input[name="is_product_purchase"]').is(":checked")) {
            $('div#product_for_purchase_accounts').show();
        }
    }

    $(document).on('click', 'a#create-new-product-btn', function(e) {
        e.preventDefault();
        inventoryTypeFormEle[0].reset();
        $('#inventory-type-container').show();        
    });

    $('form#inventory-type-form input[type=radio][name=inventory_type]').change(function() {
        let inventory_type = $('form#inventory-type-form input[type=radio][name=inventory_type]:checked').val();
        let url = $('a#create-new-product-btn').attr('href');
        url += "?type="+inventory_type;
        window.location = url;
    });
    populateProductTotal();
    populateFixedAssetsTotal();

    $(document).on('click', 'div#inventory-type-container', function ( e ) {
        if (!$(e.target).closest('form#inventory-type-form').length) {
            // ... clicked on the 'body', but not inside of #menutop
            $(this).hide();
        }
    });

    $(document).on('keydown', function ( e ) {
        if (e.keyCode === 27) { // ESC
            $('#inventory-type-container').hide();
        }
    });
    // Debit note include to invoice //
    if ($("form#edit-debit-notes-form").length) {
        let contact_id = $('form#edit-debit-notes-form select[name="contact_id"] :selected').val();
        if (contact_id) {
            let debit_note_id = $("form#edit-debit-notes-form").data("debit-note-id");
            getContactInvoicesRows(contact_id, 'supplier', 'edit', 'supplier_invoices', null, null, debit_note_id);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.debit_notes_without_contact') + '</p></td></tr>';
            $("#supplier_invoices tbody").html(emptyRow);
        }
    }
    if ($("form#add-edit-employee-form").length) {  
        let status = $('form#add-edit-employee-form select[name="status"] :selected').val();
        if (status == "not_working") {
            $('#emp-final-date').show();
        } else {
            $('#emp-final-date').hide();
        }
    }
    $('form#add-edit-employee-form select[name="status"]').on('change', function(e) {
        let status = $(this).find('option:selected').val();
        if (status == "not_working") {
            $('#emp-final-date').show();
        } else {
            $('#emp-final-date').hide();
        }
    });


    $('#to-print').on("click", function(event) {
        event.preventDefault();
        $('#accounting-table, #accounting-head').printThis({
            importStyle: true,
            importCSS: true,
            printContainer: true,
            loadCSS: "",
            formValues: true,
            copyTagClasses: true,
        });
    });
    generateHierarchySelect('assets-account-multilevelselect', 'input[name="asset_account"]');
    generateHierarchySelect('income-account-multilevelselect', 'input[name="income_account"]');
    generateHierarchySelect('expense-account-multilevelselect', 'input[name="expense_account"]');
    generateHierarchySelect('legal_category-multilevelselect', 'input[name="legal_category"]');

    generateHierarchySelect('concept-multilevelselect-0', 'input[name="concepts[0][item]"]');
    generateHierarchySelect('account-multilevelselect', 'input[name="bank_account"]');

    generateHierarchySelect('sales-account-multilevelselect', 'input[name="sales_account"]');
    generateHierarchySelect('purchases-account-multilevelselect', 'input[name="purchases_account"]');

    generateHierarchySelect('fixed-asset-account-multilevelselect', 'input[name="account"]');
    generateHierarchySelect('type-account-multilevelselect', 'input[name="type_account"]');

    $(document).on('change', 'select[name="template_number"]', function(e) {
        let template_number = $(this).find('option:selected').val() || 15;
        $("a#invoice-download-btn").attr("href", updateQueryStringParameter($("a#invoice-download-btn").attr("href"), "template_number", template_number));
        $("input[name=template_number]").val(template_number);
    });

    $(document).on("click", ".print-this-invoice", function(e) {
        try {
            let invoiceID = $(this).data("id");
            let invoiceType = $(this).data("type");
            let template_number = $('select[name="template_number"]').find('option:selected').val() || 15;
            result = $.ajax({
                url: SITE_URL + "/invoices/" + invoiceID + "/print/template/"+template_number+"/" + invoiceType,
                type: "GET",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                success: function(templateData) {
                    $(".print-invoice-template").html(templateData.invoice_template)
                    $(".print-invoice-template").printThis({
                        importCSS: false,
                        importStyle: true,
                    });
                },
            });
            return result;
        } catch (error) {
            errorObj = $.parseJSON(error.responseText);
            console.log(errorObj);
        }
    });
 $(document).on("click", ".print-this-recivable", function() {
        alert("asdas");
        try {
            let recivableID = $(this).data("id");
            result = $.ajax({
                url: SITE_URL + "/invoices/print/" + recivableID,
                type: "GET",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                success: function(templateData) {
                    $(".print-recivable-template").html(templateData.invoice_template)
                    $(".print-recivable-template").printThis({
                        importCSS: false,
                        importStyle: true,
                    });
                },
            });
            return result;
        } catch (error) {
            errorObj = $.parseJSON(error.responseText);
            console.log(errorObj);
        }
    });
    $(document).on('change', 'form#edit-debit-notes-form select[name="contact_id"]', function(e) {
        let contact_id = $(this).find('option:selected').val();
        if (contact_id) {
            let debit_note_id = $("form#edit-debit-notes-form").data("debit-note-id");
            let rows = getContactInvoicesRows(contact_id, 'supplier', 'edit', 'supplier_invoices', null, null, debit_note_id);
            console.log(rows);
        } else {
            // clear the table
            let emptyRow = '<tr class="empty-row"><td colspan="5"><p>' + trans('lang.labels.debit_notes_without_contact') + '</p></td></tr>';
            $("#supplier_invoices tbody").html(emptyRow);
        }
    });

    // Get CUrrent Balance //
    $(document).on("change",".leave-application-type-drop",function(e){
        let leave_type_id = $(this).val();
        let result = getContactLeaveBalance(leave_type_id);
    });
});

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/; domain=" + getServerName();
}

function getClientTz() {
    return moment.tz.guess() ? moment.tz.guess() : getServerTz();
}

function getServerTz() {
    return SERVER_TZ;
    // return "<?= date_default_timezone_get() ?>";
}

function getServerName() {
    return SERVER_NAME;
    // return "<?= $_SERVER['SERVER_NAME'] ?>";
}

function generateSelect2(className = 'single-search-selection', config = {}) {
    $("." + className).select2(config);
}

function generateHierarchySelect(id = 'multilevelselect', inputEle = null) {
    $('#' + id).hierarchySelect({
        // hierarchy: false,
        search: true,
        initialValueSet: true,
        onChange: function (value) {
            $(inputEle).val(value);
        }
    });
}

function populateProductTotal() {
    let quantity = 1;
    let discount = 0
    let price = $('input[name="sale_price"]').val();
    let tax_per = $('select[name="tax_id"] :selected').data("percentage");
    let tax_id = $('select[name="tax_id"] :selected').val();
    let tax_name = $('select[name="tax_id"] :selected').html();
    let calculationResult = getItemCost(price, 1, discount, { tax_per, tax_id, tax_name });
    if (price) {
        $('h3[id="item_total"]').html(CURRENCY_SYMBOL + calculationResult.total);
    }
}

function populateFixedAssetsTotal() {
    let quantity = 1;
    let discount = 0
    let price = $('input[name="price"]').val();
    let calculationResult = getItemCost(price, 1, discount);
    if (price) {
        $('h3[id="item_total"]').html(CURRENCY_SYMBOL + calculationResult.total);
    }
}

var readURL = function(input, elementId) {
    if (input.files && input.files[0] && $("#" + elementId)) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $("#" + elementId).attr('src', e.target.result).css('display', 'block');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

async function submitUserProfile(data) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/user/profile",
            type: "POST",
            data,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            // success:function(response){
            //     console.log(response);
            // },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        let alertElement = '<div class="alert alert-danger alert-dismissible" role="alert">' + errorObj.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        $('#profile-update-error').html(alertElement);
        $("button[name=next]").toggleClass('btn-disabled');
        $("span#loader").hide();
    }
}

async function generateItemRow(id, tableId) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/generate-item-row",
            type: "POST",
            data: { id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(newRowContent) {
                $("#" + tableId + " tbody").append(newRowContent);
                generateSelect2("small-single-search-selection", {
                    selectionCssClass: 'small-selector-selection',
                    dropdownCssClass: 'small-selector-dropdown',
                    width: '180px'
                });
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function getContactInvoicesRows(contact, type, action, tableId, transaction_id = null, credit_note_id = null, debit_note_id = null) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/contact-invoices-row",
            type: "POST",
            data: { contact, type, action, transaction_id, credit_note_id, debit_note_id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(rowContent) {
                $("#" + tableId + " tbody").html(rowContent);
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function getContactCreditNotes(contact) {
    $('#credit_note_balance').html('');
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/contact-credit-notes",
            type: "POST",
            data: { contact },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(response) {
                if (response.data.remaining_amount) {
                    $('#credit_note_balance').html(trans('lang.messages.credit_note_balance', { "amount": moneyFormat(response.data.remaining_amount) }));
                } else {
                    $('#credit_note_balance').html('');
                }
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function generateConceptRow(id, tableId, concept_with_products = null) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/generate-concept-row",
            type: "POST",
            data: { id, concept_with_products },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(newRowContent) {
                $("#" + tableId + " tbody").append(newRowContent);
                generateHierarchySelect('concept-multilevelselect-'+id, 'input[name="concepts['+id+'][item]"]');
                generateSelect2("small-single-search-selection", {
                    selectionCssClass: 'small-selector-selection',
                    dropdownCssClass: 'small-selector-dropdown',
                    width: '180px'
                });
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function getInventories() {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/inventories",
            type: "GET",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(inventoryDetails) {},
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function getContactDebitNotes(contact) {
    $('#debit_note_balance').html('');
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/contact-debit-notes",
            type: "POST",
            data: { contact },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(response) {
                if (response.data.remaining_amount) {
                    $('#debit_note_balance').html(trans('lang.messages.debit_note_balance', { "amount": moneyFormat(response.data.remaining_amount) }));
                } else {
                    $('#debit_note_balance').html('');
                }
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function getContactLeaveBalance(leave_type_id) {
    $('#leaves_current_balance').val('');
    let result;
    try {
        result = $.ajax({
            url: SITE_URL + "/app/contact-leave-balance",
            type: "GET",
            data: { leave_type_id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(response) {
                if (response.success) {
                    $('#leaves_current_balance').val(response.data);
                } else {
                    $('#leaves_current_balance').val('');
                }
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function generateAccountAdjustmentRow(id, tableId) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/generate-account-adjustment-row",
            type: "POST",
            data: { id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(newRowContent) {
                $("#" + tableId + " tbody").append(newRowContent);
                generateSelect2("single-search-selection", {
                    selectionCssClass: 'selector-selection',
                    dropdownCssClass: 'selector-dropdown',
                    width: '383px'
                });
                $("a.add-account-adjustment-row").removeAttr("disabled").removeClass('btn-disabled');
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

createCookie("client_tz", getClientTz());

$(document).on("click", "a.paid_resource", function(e) {
        e.preventDefault();
        formId = $(e.currentTarget).data("resource");

        $.confirm({
            buttons: {
                tryAgain: {
                    text: trans('lang.labels.yes_paid'),
                    btnClass: 'btn-red',
                    action: function() {
                        document.getElementById(formId).submit()
                    }
                },
                cancel: {
                    text: trans('lang.labels.cancel'),
                    btnClass: 'btn-default',
                    action: function() {}
                },
            },
            icon: 'fa fa-exclamation-triangle',
            title: trans('lang.labels.are_you_sure'),
            content: trans('lang.labels.are_you_sure_you_paid_to_employee'),
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            theme: 'modern',
            animation: 'scale',
            backgroundDismissAnimation: 'shake',
            draggable: false
        });
    });


$(document).on("click", "a.update_status", function(e) {
    e.preventDefault();
    formId = $(e.currentTarget).data("resource");
    document.getElementById(formId).submit()
});