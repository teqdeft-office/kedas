$(document).ready(function() {
    // let phone_pattern = "([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})";
    // let phone_pattern = "^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$";
    // let phone_pattern = "^[\+\d]?(?:[\d-.\s()]*)$";
    let phone_pattern = "^(?=.*[0-9])[- +()0-9]+$";
    let registerFormEle = $("#sign-up-form");
    let loginFormEle = $("#sign-in-form");
    let forgotPasswordFormEle = $("#forgot-password-form");
    let resetPasswordFormEle = $("#reset-password-form");
    let userProfileFormEle = $("#user-profile-form");
    let clientFormEle = $("#add-edit-client-form");
    let supplierFormEle = $("#add-edit-supplier-form");
    let resourceImportFormEle = $("#resource-import-form");
    let taxFormEle = $("#add-edit-tax-form");
    let itemCategoryFormEle = $("#add-edit-item-category-form");
    let itemInventoryFormEle = $("#add-edit-inventories-form");
    let userFormEle = $("#edit-user-form");
    let companyFormEle = $("#edit-company-form");
    let invoiceFormEle = $("#add-edit-invoices-form");
    let creditNoteFormEle = [$("#add-credit-notes-form"), $("#edit-credit-notes-form")];
    let debitNoteFormEle = $("#add-edit-debit-notes-form");
    let estimateFormEle = $("#add-edit-estimates-form");
    let purchaseOrderFormEle = $("#add-edit-purchase-order-form");
    let paymentReceivedFormEle = [$("#add-payment-received-form"), $("#edit-payment-received-form")];
    let paymentOutFormEle = [$("#add-payment-out-form"), $("#edit-payment-out-form")];
    // let paymentReceivedFormEle = $("#add-edit-payment-received-form");
    // let paymentOutFormEle = $("#add-edit-payment-out-form");
    let paymentRecurringFormEle = $("#add-edit-recurring-payment-form");
    let ncfFormEle = $("#add-edit-ncf-form");
    let inventoryAdjustmentFormEle = $("#adjustment-form");
    let accountAdjustmentFormEle = $("#add-edit-account-adjustment-form");
    let addPrimaryAccountingHeadFormEle = $("#add-primary-accounting-head-form");
    let addSecondaryAccountingHeadFormEle = $("#add-secondary-accounting-head-form");
    let editAccountingHeadFormEle = $("#edit-accounting-head-form");
    let addCompanyUserFormEle = $("#add-company-user-form");
    let editCompanyUserFormEle = $("#edit-company-user-form");
    let empFormEle = $("#add-edit-employee-form");
    let departmentFormEle = $("#add-edit-department-form");
    let designationFormEle = $("#add-edit-designation-form");
    let insuranceBrandFormEle = $("#add-edit-insurance-brands-form");
    let leaveTypeFormEle = $("#add-edit-leave-type-form");
    let leaveApplicationFormEle = $("#add-edit-leave-application-form");
    let roleFormEle = $("#add-edit-role-form");
    let benefitFormEle = $("#add-edit-benefit-form");
    let withHoldingFormEle = $("#add-edit-with-holdings-form");
    let discountFormEle = $("#add-edit-discount-form");
    let extrahourFormEle = $("#add-edit-extra-hours-form");
    let leavesFormEle = $("#add-edit-leaves-form");
    let tableFormEle = $("#add-edit-table-form");//for the table form

    $.validator.addMethod("validate_email", function(value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, function(params, element) {
        return trans('lang.validation.email', { attribute: trans('lang.labels.' + element.name) })
    });

    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    });

    $.validator.addMethod("in_list", function(value, element, params) {
        return params.split(",").map(Number).includes(parseInt(value))
    }, function(params, element) {
        return trans('lang.validation.in', { attribute: trans('lang.labels.' + element.name) })
    });

    $.validator.addMethod("string_in_list", function(value, element, params) {
        return params.split(",").map(String).includes(value);
    }, function(params, element) {
        return trans('lang.validation.in', { attribute: trans('lang.labels.' + element.name) });
    });

    $.validator.addMethod('matches', function(value, element, param) {
        let re = new RegExp(param);
        return this.optional(element) || re.test(value);
    }, function(params, element) {
        return trans('lang.validation.regex', { attribute: trans('lang.labels.' + element.name) });
    });

    $.validator.addMethod("date_format", function(value, element, param) {
        return this.optional(element) || moment(value, param, true).isValid();
    }, function(params, element) {
        return trans('lang.validation.date_format', { attribute: trans('lang.labels.' + element.name), format: params });
    });

    $.validator.addMethod("notEqualTo", function(value, element, param) {
        var target = $( param );
        if ( this.settings.onfocusout ) {
            target.unbind( ".validate-equalTo" ).bind( "blur.validate-equalTo", function() {
                $( element ).valid();
            });
        }
        return this.optional(element) || value !== target.val();
    }, function(params, element) {
        return trans('lang.validation.different', { attribute: trans('lang.labels.' + element.name), other: params });
    });

    // Register Form
    if (registerFormEle[0]) {
        registerFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                password: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.password'), min: 6 })
                },
                password_confirmation: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.confirm_password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.confirm_password'), min: 6 }),
                    equalTo: trans('lang.validation.confirmed', { attribute: trans('lang.labels.password') })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Login Form
    if (loginFormEle[0]) {
        loginFormEle.validate({
            rules: {
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                password: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.password'), min: 6 })
                }
            },
            errorElement: 'span'
        });
    }

    // Forgot Password Form
    if (forgotPasswordFormEle[0]) {
        forgotPasswordFormEle.validate({
            rules: {
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                }
            },
            messages: {
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                }
            },
            errorElement: 'span'
        });
    }

    // Reset Password Form
    if (resetPasswordFormEle[0]) {
        resetPasswordFormEle.validate({
            rules: {
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                password: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.password'), min: 6 })
                },
                password_confirmation: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.confirm_password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.confirm_password'), min: 6 }),
                    equalTo: trans('lang.validation.confirmed', { attribute: trans('lang.labels.password') })
                }
            },
            errorElement: 'span'
        });
    }

    // User profile Form
    if (userProfileFormEle[0]) {
        userProfileFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                role: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                sector: {
                    required: true
                },
                country: {
                    required: true
                },
                currency: {
                    required: true
                },
                tax_id: {
                    required: true
                },
                address: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                city: {
                    // required: true,
                    minlength: 3,
                    maxlength: 255
                },
                state: {
                    // required: true,
                    minlength: 3,
                    maxlength: 255
                },
                phone: {
                    required: true,
                    // digits: true,
                    matches: phone_pattern,
                    minlength: 10,
                    maxlength: 19
                },
                /*fax: {
                    required: true,
                    digits: true,
                    minlength: 5,
                    maxlength: 10
                },*/
                mobile: {
                    // required: true,
                    matches: phone_pattern,
                    minlength: 10,
                    maxlength: 19
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                role: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.role') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.role'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.role'), max: 255 })
                },
                sector: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.sector') })
                },
                country: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.country') })
                },
                currency: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.currency') })
                },
                tax_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.tax') })
                },
                address: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.address') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.address'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.address'), max: 255 })
                },
                city: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.city') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.city'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.city'), max: 255 })
                },
                state: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.state') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.state'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.state'), max: 255 })
                },
                phone: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.phone') }),
                    // digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.phone')}),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                },
                /*fax: {
                    required: trans('lang.validation.required', {attribute: trans('lang.labels.fax')}),
                    digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.fax')}),
                    minlength: trans('lang.validation.min.string', {attribute: trans('lang.labels.fax'), min: 5}),
                    maxlength: trans('lang.validation.max.string', {attribute: trans('lang.labels.fax'), max: 10})
                },*/
                mobile: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.mobile') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Client add and edit form validation
    if (clientFormEle[0]) {
        clientFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                address: {
                    minlength: 3,
                    maxlength: 255
                },
                phone: {
                    // digits: true,
                    minlength: 10,
                    maxlength: 19
                },
                mobile: {
                    // digits: true,
                    minlength: 10,
                    maxlength: 19
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                address: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.address'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.address'), max: 255 })
                },
                phone: {
                    // digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.phone')}),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                },
                mobile: {
                    // digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.phone')}),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Supplier add and edit form validation
    if (supplierFormEle[0]) {
        supplierFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                address: {
                    minlength: 3,
                    maxlength: 255
                },
                phone: {
                    // digits: true,
                    minlength: 10,
                    maxlength: 19
                },
                mobile: {
                    // digits: true,
                    minlength: 10,
                    maxlength: 19
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                address: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.address'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.address'), max: 255 })
                },
                phone: {
                    // digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.phone')}),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                },
                mobile: {
                    // digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.phone')}),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // resource import Form
    if (resourceImportFormEle[0]) {
        resourceImportFormEle.validate({
            ignore: [],
            rules: { // User profile Form
                file: {
                    required: true,
                    extension: "xls|xlsx"
                        // extension: "xls|xlsx|xlm|xla|xlc|xlt|xlw"
                        // filesize: 2000000
                }
            },
            messages: {
                file: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.file') }),
                    extension: trans('lang.validation.mimes', { attribute: trans('lang.labels.file'), values: "xls,xlsx" })
                        // extension: trans('lang.messages.invalid_excel_file_error')
                        // filesize: "The selected file should be less than 2 MB."
                },
            }
        });
    }

    // Tax add and edit form validation
    if (taxFormEle[0]) {
        taxFormEle.validate({
            ignore: [],
            lang: locale,
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    // matches: /^[a-zA-Z\s]+$/
                },
                percentage: {
                    required: true,
                    number: true,
                    // integer: true,
                    range: [0.1, 99.99]
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 }),
                    // matches: 'name format is invalid'
                },
                percentage: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.percentage') }),
                    number: trans('lang.validation.numeric', { attribute: trans('lang.labels.percentage') }),
                    range: trans('lang.validation.in', { attribute: trans('lang.labels.percentage') })
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Item Category add and edit form validation
    if (itemCategoryFormEle[0]) {
        itemCategoryFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    // matches: /^[a-zA-Z0-9\s]+$/
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 }),
                    // matches: 'name format is invalid'
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Inventory add and edit form validation
    if (itemInventoryFormEle[0]) {
        itemInventoryFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                        // matches: /^[a-zA-Z0-9-_\s]+$/
                },
                reference: {
                    minlength: 3,
                    maxlength: 255
                        // matches: /^[a-zA-Z0-9-_]+$/
                },
                description: {
                    minlength: 3,
                    maxlength: 255
                },
                barcode: {
                    minlength: 6,
                    maxlength: 20
                },
                sale_price: {
                    required: true,
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,2})?$/
                },
                cost: {
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,2})?$/
                },
                image: {
                    extension: "jpeg|bmp|png|jpg"
                        // filesize: 2000000
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 }),
                },
                reference: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.reference'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.reference'), max: 255 }),
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                },
                barcode: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.barcode'), min: 6 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.barcode'), max: 20 })
                },
                sale_price: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.sale_price') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.sale_price'), max: 16 })
                },
                cost: {
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.cost'), max: 16 })
                },
                image: {
                    extension: trans('lang.validation.mimes', { attribute: trans('lang.labels.image_of_item'), values: "jpeg,bmp,png,jpg" })
                        // filesize: "The selected file should be less than 2 MB."
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // User edit form validation
    if (userFormEle[0]) {
        userFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                language: {
                    string_in_list: 'en,es'
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 }),
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // invoice Form
    if (invoiceFormEle[0]) {
        invoiceFormEle.validate({
            ignore: [],
            rules: {
                contact_id: {
                    required: true
                },
                invoice_start_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                invoice_expiration_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                invoice_end_date: {
                    date_format: 'YYYY-MM-DD'
                },
                ncf_number: {
                    minlength: 11,
                    maxlength: 11,
                    matches: /^[A-Za-z]{1,1}\d{10}$/
                },
                frequency: {
                    digits: true,
                    min: 1,
                },
                frequency_type: {
                    string_in_list: 'month,day'
                },
                exchange_rate: {
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,6})?$/
                },
                exchange_rate: {
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,6})?$/
                },
            },
            messages: {
                contact_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.contact') })
                },
                invoice_start_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                invoice_expiration_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.expiration_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.expiration_date'), format: 'YYYY-MM-DD' })
                },
                invoice_end_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.end_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.end_date'), format: 'YYYY-MM-DD' })
                },
                ncf_number: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.ncf_number'), min: 11 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.ncf_number'), max: 11 })
                },
                frequency: {
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.frequency') }),
                    min: trans('lang.validation.min.numeric', { attribute: trans('lang.labels.frequency'), min: 1 })
                },
                exchange_rate: {
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.rate'), max: 16 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }
    // Company Form
    if (companyFormEle[0]) {
        companyFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                role: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                address: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                city: {
                    //required: true,
                    minlength: 3,
                    maxlength: 255
                },
                state: {
                    //required: true,
                    minlength: 3,
                    maxlength: 255
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 12
                },
                fax: {
                    //required: true,
                    digits: true,
                    minlength: 5,
                    maxlength: 10
                },
                tax_id: {
                    required: true
                },
                support_email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                logo: {
                    extension: "jpeg|bmp|png|jpg"
                        // filesize: 2000000
                },
                signature: {
                    extension: "jpeg|bmp|png|jpg"
                        // filesize: 2000000
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                role: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.role') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.role'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.role'), max: 255 })
                },
                address: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.address') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.address'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.address'), max: 255 })
                },
                city: {
                    //required: trans('lang.validation.required', { attribute: trans('lang.labels.city') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.city'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.city'), max: 255 })
                },
                state: {
                    //required: trans('lang.validation.required', { attribute: trans('lang.labels.state') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.state'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.state'), max: 255 })
                },
                country: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.country') })
                },
                phone: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.phone') }),
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.phone') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 12 })
                },
                fax: {
                    //required: trans('lang.validation.required', { attribute: trans('lang.labels.fax') }),
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.fax') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.fax'), min: 5 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.fax'), max: 10 })
                },
                tax_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.tax') })
                },
                support_email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.support_email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.support_email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.support_email'), max: 255 })
                },
                logo: {
                    extension: trans('lang.validation.mimes', { attribute: trans('lang.labels.logo'), values: "jpeg,bmp,png,jpg" })
                        // filesize: "The selected file should be less than 2 MB."
                },
                signature: {
                    extension: trans('lang.validation.mimes', { attribute: trans('lang.labels.signature'), values: "jpeg,bmp,png,jpg" })
                        // filesize: "The selected file should be less than 2 MB."
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Credit Note Form
    creditNoteFormEle = creditNoteFormEle.filter(ele => ele.length);
    creditNoteFormEle.forEach(function(formEle) {
        if (formEle[0]) {
            formEle.validate({
                ignore: [],
                rules: {
                    contact_id: {
                        required: true
                    },
                    credit_note_date: {
                        required: true,
                        date_format: 'YYYY-MM-DD'
                    },
                    exchange_rate: {
                        maxlength: 16,
                        matches: /^\d+(\.\d{1,6})?$/
                    }
                },
                messages: {
                    contact_id: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.contact') })
                    },
                    credit_note_date: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                        date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                    },
                    exchange_rate: {
                        maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.rate'), max: 16 })
                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    let type = $(element).attr("type");
                    if (type === "checkbox") {
                        error.insertAfter(element.next());
                    } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                        error.insertAfter(element.next());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        }
    });

    // estimate Form
    if (estimateFormEle[0]) {
        estimateFormEle.validate({
            ignore: [],
            rules: {
                contact_id: {
                    required: true
                },
                estimate_start_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                estimate_expiration_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                exchange_rate: {
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,6})?$/
                }
            },
            messages: {
                contact_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.contact') })
                },
                estimate_start_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                estimate_expiration_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.expiration_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.expiration_date'), format: 'YYYY-MM-DD' })
                },
                exchange_rate: {
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.rate'), max: 16 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // debit Note Form
    if (debitNoteFormEle[0]) {
        debitNoteFormEle.validate({
            ignore: [],
            rules: {
                contact_id: {
                    required: true
                },
                debit_note_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                exchange_rate: {
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,6})?$/
                }
            },
            messages: {
                contact_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.contact') })
                },
                debit_note_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                exchange_rate: {
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.rate'), max: 16 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // purchase Order Form
    if (purchaseOrderFormEle[0]) {
        purchaseOrderFormEle.validate({
            ignore: [],
            rules: {
                contact_id: {
                    required: true
                },
                purchase_order_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                purchase_order_expiration_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                exchange_rate: {
                    maxlength: 16,
                    matches: /^\d+(\.\d{1,6})?$/
                }
            },
            messages: {
                contact_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.contact') })
                },
                purchase_order_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                purchase_order_expiration_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.expiration_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.expiration_date'), format: 'YYYY-MM-DD' })
                },
                exchange_rate: {
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.rate'), max: 16 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // payment Received Form
    paymentReceivedFormEle = paymentReceivedFormEle.filter(ele => ele.length);
    paymentReceivedFormEle.forEach(function(formEle) {
        if (formEle[0]) {
            formEle.validate({
                ignore: [],
                rules: {
                    contact_id: {
                        required: true
                    },
                    start_date: {
                        required: true,
                        date_format: 'YYYY-MM-DD'
                    },
                    bank_account: {
                        required: true
                    },
                    payment_method: {
                        required: true
                    }
                },
                messages: {
                    contact_id: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.contact') })
                    },
                    start_date: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                        date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                    },
                    bank_account: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.bank_account') })
                    },
                    payment_method: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.payment_method') })
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    let type = $(element).attr("type");
                    if (type === "checkbox") {
                        error.insertAfter(element.next());
                    } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                        error.insertAfter(element.next());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        }
    });

    // payment out Form
    paymentOutFormEle = paymentOutFormEle.filter(ele => ele.length);
    paymentOutFormEle.forEach(function(formEle) {
        if (formEle[0]) {
            formEle.validate({
                // ignore: [],
                rules: {
                    start_date: {
                        required: true,
                        date_format: 'YYYY-MM-DD'
                    },
                    bank_account: {
                        required: true
                    },
                    payment_method: {
                        required: true
                    },
                    transaction_with_invoice: {
                        required: true
                    }
                },
                messages: {
                    start_date: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                        date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                    },
                    bank_account: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.bank_account') })
                    },
                    payment_method: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.payment_method') })
                    },
                    transaction_with_invoice: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.transaction_type') })
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    let type = $(element).attr("type");
                    if (type === "checkbox") {
                        error.insertAfter(element.next());
                    } else if (type === "radio") {
                        error.appendTo($(element).parent());
                    } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                        error.insertAfter(element.next());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        }
    });

    // payment recurring Form
    if (paymentRecurringFormEle[0]) {
        paymentRecurringFormEle.validate({
            ignore: [],
            rules: {
                start_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                end_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                bank_account: {
                    required: true
                },
                frequency: {
                    required: true,
                    digits: true,
                    min: 1,
                },
                frequency_type: {
                    required: true,
                    string_in_list: 'month,day'
                }
            },
            messages: {
                start_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                end_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.end_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.end_date'), format: 'YYYY-MM-DD' })
                },
                bank_account: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.bank_account') })
                },
                frequency: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.frequency') }),
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.frequency') }),
                    min: trans('lang.validation.min.numeric', { attribute: trans('lang.labels.frequency'), min: 1 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // NCF Form
    if (ncfFormEle[0]) {
        ncfFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    matches: /^[a-zA-Z\s]+$/
                },
                ncf_number: {
                    required: true,
                    minlength: 11,
                    maxlength: 11,
                    matches: /^[A-Za-z]{1,1}\d{10}$/
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                ncf_number: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.ncf_number') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.ncf_number'), min: 11 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.ncf_number'), max: 11 })
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Inventory Adjustment form
    if (inventoryAdjustmentFormEle[0]) {
        inventoryAdjustmentFormEle.validate({
            rules: {
                adjustment_date: {
                    required: true
                }
            },
            messages: {
                adjustment_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.adjustment_date') })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // accounting Head form
    if (addPrimaryAccountingHeadFormEle[0]) {
        primaryAccountingHeadFormValidator = addPrimaryAccountingHeadFormEle.validate({
            rules: {
                'account-name': {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                },
                code: {
                    matches: /^[0-9]+$/
                }
            },
            messages: {
                'account-name': {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.account_name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.account_name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.account_name'), max: 255 })
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    if (addSecondaryAccountingHeadFormEle[0]) {
        secondaryAccountingHeadFormValidator = addSecondaryAccountingHeadFormEle.validate({
            rules: {
                'account-name': {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                },
                code: {
                    required: true,
                    matches: /^[0-9]+$/
                },
                opening_balance: {
                    matches: /^\d+(\.\d{1,2})?$/
                },
                'chart-type': {
                    required: true
                }
            },
            messages: {
                'account-name': {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.account_name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.account_name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.account_name'), max: 255 })
                },
                'chart-type': {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.type') }),
                },
                code: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.code') }),
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "radio") {
                    error.appendTo($(element).parent());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    if (editAccountingHeadFormEle[0]) {
        editAccountingHeadFormValidator = editAccountingHeadFormEle.validate({
            rules: {
                'account-name': {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                },
                code: {
                    required: true,
                    matches: /^[0-9]+$/
                }
            },
            messages: {
                'account-name': {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.account_name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.account_name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.account_name'), max: 255 })
                },
                code: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.code') }),
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    /*accountingHeadFormEle = accountingHeadFormEle.filter(ele => ele.length);
    accountingHeadFormEle.forEach(function(formEle) {
        
    });*/

    // company user add form validation
    if (addCompanyUserFormEle[0]) {
        addCompanyUserFormEle.validate({            
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                password: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.password'), min: 6 })
                },
                password_confirmation: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.confirm_password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.confirm_password'), min: 6 }),
                    equalTo: trans('lang.validation.confirmed', { attribute: trans('lang.labels.password') })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // company user add form validation
    if (editCompanyUserFormEle[0]) {
        editCompanyUserFormEle.validate({            
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                password: {
                    required: false,
                    minlength: 6
                },
                password_confirmation: {
                    required: false,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                password: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.password'), min: 6 })
                },
                password_confirmation: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.confirm_password') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.confirm_password'), min: 6 }),
                    equalTo: trans('lang.validation.confirmed', { attribute: trans('lang.labels.password') })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Employee add and edit form validation
    if (empFormEle[0]) {
        empFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    required: true,
                    validate_email: true,
                    maxlength: 255
                },
                password: {
                    minlength: 6
                },
                address: {
                    minlength: 3,
                    maxlength: 255
                },
                phone: {
                    minlength: 10,
                    maxlength: 19
                },
                mobile: {
                    required: true,
                    minlength: 10,
                    maxlength: 19
                },
                birth_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                username: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    matches: /^[A-Za-z_-][A-Za-z0-9_]*$/
                },
                department_id: {
                    required: true,
                },
                designation_id: {
                    required: true,
                },
                salary: {
                    required: true,
                    matches: /^\d+(\.\d{1,2})?$/
                },
                afp: {
                    matches: /^\d+(\.\d{1,2})?$/
                },
                role: {
                    required: true,
                },
                status: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
                email: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.email') }),
                    email: trans('lang.validation.email', { attribute: trans('lang.labels.email') }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.email'), max: 255 })
                },
                address: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.address'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.address'), max: 255 })
                },
                phone: {
                    // digits: trans('lang.validation.numeric', {attribute: trans('lang.labels.phone')}),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.phone'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.phone'), max: 19 })
                },
                mobile: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.mobile') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.mobile'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.mobile'), max: 19 })
                },
                birth_date: {
                        required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                        date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                username: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.username') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.username'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.username'), max: 255 })
                },
                username: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.status') }),
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Department add and edit form validation
    if (departmentFormEle[0]) {
        departmentFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    //matches: /^[a-zA-Z0-9\s]+$/
                },
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Designation add and edit form validation
    if (designationFormEle[0]) {
        designationFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    //matches: /^[a-zA-Z0-9\s]+$/
                },
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // insurance brands add and edit form validation
    if (insuranceBrandFormEle[0]) {
        insuranceBrandFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                    //matches: /^[a-zA-Z0-9\s]+$/
                },
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Leave Type add and edit form validation
    if (leaveTypeFormEle[0]) {
        leaveTypeFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                },
                number_of_days: {
                    required: true,
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 }),
                },
                number_of_days: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.number_of_days') }),
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // leave Application Form
    if (leaveApplicationFormEle[0]) {
        leaveApplicationFormEle.validate({
            ignore: [],
            rules: {
                leave_type_id: {
                    required: true
                },
                from_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                to_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                purpose: {
                    required: true,
                    minlength: 10,
                    maxlength: 255
                },
                current_balance: {
                    required: true
                },
                number_of_days: {
                    required: true
                },
            },
            messages: {
                leave_type_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.leave_type') })
                },
                from_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.from_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.from_date'), format: 'YYYY-MM-DD' })
                },
                to_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.to_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.to_date'), format: 'YYYY-MM-DD' })
                },
                purpose: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.purpose') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.purpose'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.purpose'), max: 255 })
                },  
                current_balance: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.current_balance') })
                },
                number_of_days: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.number_of_days') })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    if (roleFormEle[0]) {
        roleFormEle.validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                },
                "permissions[]": {
                    required: true,
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.name'), max: 255 }),
                },
                "permissions[]": {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.permissions') }),
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // account Adjustment form
    if (accountAdjustmentFormEle[0]) {
        accountAdjustmentFormEle.validate({
            rules: {
                adjustment_date: {
                    required: true
                }
            },
            messages: {
                adjustment_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.adjustment_date') })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler : function(form) {
                let conceptValid = true;
                $('input[name$="[concept-item]"]').each(function() {
                    if ($(this).val() == undefined || !$(this).val()) {
                        conceptValid = false;
                        return false;
                    }
                });
                if (!conceptValid) {
                    toastr.error(trans('lang.messages.please_select_account'));
                    return false;
                }

                let debitAmounts = {};
                let creditAmounts = {};
                $('input[name^="items"]').each(function() {
                    let eleName = $(this).attr('name');
                    let needToReplace = eleName.slice(eleName.lastIndexOf("[") + 1, eleName.lastIndexOf("]"));
                    let key = eleName.slice(eleName.indexOf("[") + 1, eleName.indexOf("]"));

                    if (needToReplace == 'debit_amount' && $(this).val() != undefined && Boolean($(this).val())) {
                        debitAmounts[key] = $(this).val();
                    }
                    if (needToReplace == 'credit_amount' && $(this).val() != undefined && Boolean($(this).val())) {
                        creditAmounts[key] = $(this).val();
                    }

                });
                var aKeys = Object.keys(debitAmounts).sort();
                var bKeys = Object.keys(creditAmounts).sort();

                if (!aKeys.length || !bKeys.length) {
                    toastr.error(trans('lang.messages.je_with_no_amount'));
                    return false;
                }

                debitAmountSum = Object.values(debitAmounts).map(Number).reduce((a, b) => a + b);
                creditAmountSum = Object.values(creditAmounts).map(Number).reduce((a, b) => a + b);
                const invalidRows = aKeys.filter(value => bKeys.includes(value));

                if (debitAmountSum !== creditAmountSum) {
                    toastr.error(trans('lang.messages.debit_credit_amount_incorrect'));
                    return false;
                }
                
                if (invalidRows.length) {
                    toastr.error(trans('lang.messages.invalid_account_entry'));
                    return false;
                }
                return true;
            }
        });
    }

    // benefit Form
    if (benefitFormEle[0]) {
        benefitFormEle.validate({
            ignore: [],
            rules: {
                employee_id: {
                    required: true
                },
                start_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                end_date: {
                    date_format: 'YYYY-MM-DD'
                },
                frequency: {
                    digits: true,
                    min: 1,
                },
                frequency_type: {
                    required: true,
                    string_in_list: 'monthly,biweekly,weekly,daily'
                },
                expenses: {
                    required: true
                },
                type: {
                    required: true,
                    string_in_list: 'food,biweekly,bonus,transport,vacation,commission'
                },
                amount: {
                    required: true,
                    matches: /^\d+(\.\d{1,2})?$/
                },
            },
            messages: {
                employee_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.employee') })
                },
               start_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                end_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.end_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.end_date'), format: 'YYYY-MM-DD' })
                },
                frequency: {
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.frequency') }),
                    min: trans('lang.validation.min.numeric', { attribute: trans('lang.labels.frequency'), min: 1 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

     // with holding Form
    if (withHoldingFormEle[0]) {
        withHoldingFormEle.validate({
            ignore: [],
            rules: {
                employee_id: {
                    required: true
                },
                start_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                end_date: {
                    date_format: 'YYYY-MM-DD'
                },
                frequency: {
                    digits: true,
                    min: 1,
                },
                frequency_type: {
                    required: true,
                    string_in_list: 'monthly,biweekly,weekly,daily'
                },
                incomes: {
                    required: true
                },
                type: {
                    required: true,
                    string_in_list: 'AFP,SDSS,ISR,SS,HI,Others'
                },
                amount: {
                    required: true,
                    matches: /^\d+(\.\d{1,2})?$/
                },
            },
            messages: {
                employee_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.employee') })
                },
               start_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                end_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.end_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.end_date'), format: 'YYYY-MM-DD' })
                },
                frequency: {
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.frequency') }),
                    min: trans('lang.validation.min.numeric', { attribute: trans('lang.labels.frequency'), min: 1 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Discount Form
    if (discountFormEle[0]) {
        discountFormEle.validate({
            ignore: [],
            rules: {
                employee_id: {
                    required: true
                },
                start_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                end_date: {
                    date_format: 'YYYY-MM-DD'
                },
                frequency: {
                    digits: true,
                    min: 1,
                },
                frequency_type: {
                    required: true,
                    string_in_list: 'monthly,biweekly,weekly,daily'
                },
                incomes: {
                    required: true
                },
                type: {
                    required: true,
                },
                amount: {
                    required: true,
                    matches: /^\d+(\.\d{1,2})?$/
                },
            },
            messages: {
                employee_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.employee') })
                },
               start_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                end_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.end_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.end_date'), format: 'YYYY-MM-DD' })
                },
                frequency: {
                    digits: trans('lang.validation.numeric', { attribute: trans('lang.labels.frequency') }),
                    min: trans('lang.validation.min.numeric', { attribute: trans('lang.labels.frequency'), min: 1 })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }
    // Extra Hours Form
    if (extrahourFormEle[0]) {
        extrahourFormEle.validate({
            ignore: [],
            rules: {
                employee_id: {
                    required: true
                },
                start_time: {
                    required: true,
                },
                end_time: {
                    required: true,
                },
                hours_price: {
                    required: true,
                    matches: /^\d+(\.\d{1,2})?$/
                },
                incomes: {
                    required: true,
                },
                type: {
                    required: true,
                },
            },
            messages: {
                employee_id: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.employee') })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // leave Application Form
    if (leavesFormEle[0]) {
        leavesFormEle.validate({
            ignore: [],
            rules: {
                employee_id: {
                    required: true,
                },
                leave_type: {
                    required: true,
                    string_in_list: 'vacation,sick_leave,event_leave_or_other'
                },
                from_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                to_date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                note: {
                    minlength: 10,
                    maxlength: 255
                },
                number_of_days: {
                    required: true
                },
                leave_type: {
                    required: true
                },
            },
            messages: {
                leave_type: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.leave_type') })
                },
                from_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.from_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.from_date'), format: 'YYYY-MM-DD' })
                },
                to_date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.to_date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.to_date'), format: 'YYYY-MM-DD' })
                },
                note: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.purpose') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.purpose'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.purpose'), max: 255 })
                },  
                number_of_days: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.number_of_days') })
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    if (tableFormEle[0]) {
        tableFormEle.validate({
            ignore: [],
            rules: {
                table_name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255,
                },
                date: {
                    required: true,
                    date_format: 'YYYY-MM-DD'
                },
                zone: {
                    required: true,
                },
                table_number: {
                    required: true,
                },
                chairs_number: {
                    required: true,
                },
                description: {
                    minlength: 10,
                    maxlength: 255
                }
            },
            messages: {
                name: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.table_name') }),
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.table_name'), min: 3 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.table_name'), max: 255 }),
                },
                date: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.date') }),
                    date_format: trans('lang.validation.date_format', { attribute: trans('lang.labels.date'), format: 'YYYY-MM-DD' })
                },
                table_number: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.table_number') }),
                },
                chairs_number: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.chairs_number') }),
                },
                zone: {
                    required: trans('lang.validation.required', { attribute: trans('lang.labels.zone') }),
                },
                description: {
                    minlength: trans('lang.validation.min.string', { attribute: trans('lang.labels.description'), min: 10 }),
                    maxlength: trans('lang.validation.max.string', { attribute: trans('lang.labels.description'), max: 255 })
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                let type = $(element).attr("type");
                if (type === "checkbox") {
                    error.insertAfter(element.next());
                } else if ($(element).is("select") && $(element).attr("class").includes('search-selection')) {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

});