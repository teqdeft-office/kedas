-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 31, 2020 at 02:28 AM
-- Server version: 10.3.22-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teqdeftdev_kedas`
--

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `addressadder_id`, `addressadder_type`, `type`, `line_1`, `line_2`, `city`, `province`, `zip`, `lat`, `lng`, `country_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'App\\User', 'general', 'wwww', NULL, 'wwww', NULL, NULL, NULL, NULL, 216, '2020-08-10 14:56:42', '2020-08-10 14:56:42', NULL),
(2, 1, 'App\\Supplier', 'general', '7801 NW 37TH ST', NULL, NULL, '33166', NULL, NULL, NULL, 227, '2020-08-10 16:44:27', '2020-08-10 16:44:27', NULL);

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `user_id`, `name`, `reference`, `description`, `item_category_id`, `tax_id`, `type_of_item`, `account`, `image`, `sale_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Kia Sorento Full', '5197', 'Kia Sorento 2014 Full 4x4', 1, NULL, 'simple', 'other_income', '1-item-3432a5f58775f1f.jpg', 900000.00, '2020-08-10 16:41:20', '2020-08-10 17:04:29', NULL);

--
-- Dumping data for table `item_categories`
--

INSERT INTO `item_categories` (`id`, `user_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Vehiculos', 'Vehiculos Vendidos por la Empresa', '2020-08-10 15:53:01', '2020-08-10 15:53:01', NULL),
(2, 1, 'Hardware', 'Equipos Fisicos de Tecnologia Perifericos y Ordenadores', '2020-08-10 15:56:24', '2020-08-10 15:56:24', NULL);

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `user_id`, `identification_type_id`, `tax_id`, `name`, `email`, `sector_id`, `phone`, `fax`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'a123456', 'Elvis Rodriguez', 'ing.elvisrodriguez@gmail.com', 8, '9013109529', '9013109529', '2020-08-10 16:44:27', '2020-08-10 16:44:27', NULL);

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `user_id`, `name`, `percentage`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'ITBIS', 18.00, 'Impuestos que se cobran en la republica dominicana', '2020-08-10 15:57:07', '2020-08-10 15:57:07', NULL);

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sachin kumar', 'sachin@yopmail.com', '2020-08-10 14:53:34', '$2y$10$HpB1PWJjpJOkLYIJYhYo8OFEmO3.TgmZdsUM6WkWA7zW4haW7CWIu', 'l6V9TWdpUXJPz7bZ79Q1xw421XPuD2YuvbePQu6jAbKD4973eUKh1XOxKW9S', '2020-08-10 14:53:34', '2020-08-10 14:53:34'),
(2, 'sumit kumar', 'sumit@yopmail.com', '2020-08-10 14:53:34', '$2y$10$ZaPDHIPTU32aONIHo2.zoul8x2yF1CUquNB8CStPPIPgpQDBEJn22', NULL, '2020-08-10 14:53:34', '2020-08-10 14:53:34'),
(3, 'My Name', 'test@newmail.com', NULL, '$2y$10$dgPxm1MdzO3jKVMZQTSjBeade0yIOVLBUUuSI4m3.guCHF4XZ.3uu', NULL, '2020-08-11 13:12:06', '2020-08-11 13:12:06');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
