<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AdminOrSuperAdmin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/counter', 'AppController@counter');

Route::group(['middleware' => ['setLocale']], function ($router) {
	Route::get('/', 'HomeController@index');

	Auth::routes(['verify' => true]);
});

Route::group(['middleware' => ['auth', 'verified', 'completed', 'setLocale', 'xss_protection']], function ($router) {

	Route::get('/logout', 'Auth\LoginController@logout');

	Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->withoutMiddleware(['completed']);

	Route::match(['get', 'post'], '/user/profile', 'UserController@profile')->name('user_profile')->withoutMiddleware(['completed']);

	Route::group(['middleware' => ['permission:manage sales']], function () {
		Route::resource('credit-notes', 'CreditNoteController');
		Route::get('credit-notes/{credit_note}/pdf', 'CreditNoteController@pdf')->name('credit-notes.pdf');
		Route::get('credit-notes/{credit_note}/print', 'CreditNoteController@print')->name('credit-notes.print');
		Route::post('credit-notes/{credit_note}/mail', 'CreditNoteController@mail')->name('credit-notes.mail');

		Route::resource('estimates', 'EstimateController');
		/* Route::resource('remissions', 'RemissionController'); */
		Route::get('estimates/{estimate}/pdf', 'EstimateController@pdf')->name('estimates.pdf');
		Route::get('estimates/{estimate}/print', 'EstimateController@print')->name('estimates.print');
		Route::post('estimates/{estimate}/mail', 'EstimateController@mail')->name('estimates.mail');
		Route::prefix('pos')->group(function () {
			Route::get('/', 'PosController@index')->name('pos.index');
			Route::post('/order/create/{mode?}', 'PosController@createOrder')->where('mode', 'online|offline')->name('pos.createOrder');
		});
		Route::post('estimates/{estimate}/approve', 'EstimateController@getClone')->name('estimates.approve');
		Route::resource('remissions', 'RemissionController');
	});

	Route::group(['middleware' => ['permission:manage expenses']], function () {
		Route::resource('debit-notes', 'DebitNoteController');
		Route::resource('purchase-orders', 'PurchaseOrderController');
		Route::resource('ncfs', 'NcfController');
	});

	/* invoices and transaction related routes*/
	Route::group(['middleware' => ['redirect_for_invoices']], function () {
		Route::get('invoices', function() {
			return redirect()->route('invoices.index', ['type' => 'sale']);
		});
		Route::get('invoices/create/{type}', 'InvoiceController@create')->where('type', 'sale|recurring|supplier')->name('invoices.create');
		Route::post('invoices/{type}', 'InvoiceController@store')->where('type', 'sale|recurring|supplier')->name('invoices.store');
		Route::get('invoices/{type}', 'InvoiceController@index')->where('type', 'sale|recurring|supplier')->name('invoices.index');
		Route::post('invoices/{invoice}/void', 'InvoiceController@void')->name('invoices.void');

		Route::resource('invoices', 'InvoiceController')->except([
		    'create', 'store', 'index'
		]);

		Route::get('invoices/{invoice}/pdf', 'InvoiceController@pdf')->name('invoices.pdf');
		Route::get('invoices/{invoice}/print', 'InvoiceController@print')->name('invoices.print');
		Route::get('invoices/{invoice}/print/template/{template_no}/{view}', 'InvoiceController@printWithTemplate');
		Route::post('invoices/{invoice}/mail', 'InvoiceController@mail')->name('invoices.mail');
		Route::get('invoices/{invoice}/makeProduction', 'InvoiceController@makeProductionOrder')->name('invoices.makeProduction');
		Route::get('invoices/{invoice}/clone', 'InvoiceController@clone')->name('invoices.clone');
		Route::get('invoices/{invoice}/recurring', 'InvoiceController@recurring')->name('invoices.recurring');
		Route::post('invoices/{invoice}/makeRecurring', 'InvoiceController@convertInvoice')->name('invoices.makeRecurring');
		Route::resource('shipping', 'ShippingController');
	});
	Route::resource('restaurant-table', 'TableController');
	//Route::group(['middleware' => ['redirect_for_accountrecivable']], function () {
		Route::get('receivable','AccountReceivableController@index')->name('receivable.index');
		Route::get('receivable/create','AccountReceivableController@create')->name('receivable.create');
		Route::post('receivable','AccountReceivableController@store')->name('receivable.store');
		Route::get('delete/{id}','AccountReceivableController@destroy')->name('receivable.delete');
		Route::get('pdf/{id}','AccountReceivableController@downloadpdf')->name('receivable.pdf');
		Route::get('view/{id}','AccountReceivableController@show')->name('receivable.show');
		

		//});



	Route::group(['middleware' => ['redirect_for_transactions']], function () {
		Route::get('transactions', function() {
			return redirect()->route('transactions.index', ['type' => 'in']);
		});
		
		Route::get('transactions/{type}', 'TransactionController@index')->where('type', 'in|out|recurring')->name('transactions.index');
		Route::get('transactions/create/{type}', 'TransactionController@create')->where('type', 'in|out|recurring')->name('transactions.create');
		Route::post('transactions/{type}', 'TransactionController@store')->where('type', 'in|out|recurring')->name('transactions.store');

		Route::resource('transactions', 'TransactionController')->except([
		    'create', 'store', 'index'
		]);
		Route::get('transactions/{transaction}/print', 'TransactionController@print')->name('transactions.print');
	});

	Route::group(['middleware' => ['permission:manage contacts']], function () {
		Route::resource('clients', 'ClientController');
		Route::resource('suppliers', 'SupplierController');
	});
		
	Route::group(['middleware' => ['permission:manage inventories']], function () {
		Route::resource('taxes', 'TaxController')->except([
		    'show'
		]);
		Route::get('taxes/{tax}', function() {
			return redirect()->route('taxes.index');
		});
		Route::resource('item-categories', 'ItemCategoryController')->except([
		    'show'
		]);
		Route::get('item-categories/{item_category}', function() {
			return redirect()->route('item-categories.index');
		});
		Route::any('inventories/search', 'InventoryController@search')->name('inventories.search')->withoutMiddleware(['permission:manage inventories']);
		Route::resource('inventories', 'InventoryController');
		Route::resource('inventory-adjustments', 'InventoryAdjustmentController');
		Route::resource('units', 'UnitController');
	});

	Route::middleware([AdminOrSuperAdmin::class])->group(function () {
		Route::get('/settings/user', 'UserController@edit')->name('users.edit');
		Route::post('/settings/user', 'UserController@update')->name('users.update');
		Route::get('/settings/company', 'UserController@company')->name('company.edit');
		Route::post('/settings/company', 'UserController@companyUpdate')->name('company.update');
		Route::resource('employees', 'EmployeeController');
		Route::resource('departments', 'DepartmentController');
		Route::resource('designations', 'DesignationController');
		Route::resource('insurance-brands', 'InsuranceBrandController');
		Route::resource('leave-types', 'LeaveTypeController');
		Route::resource('leave-applications', 'LeaveApplicationController');
		Route::get('leave-reports', 'LeaveReportController@index')->name('leave-reports.index');
		Route::get('summary-report', 'LeaveReportController@summary')->name('summary-report.summary');
		Route::get('my-leave-report', 'LeaveReportController@my_leave_report')->name('my-leave-report.my-leave');
		Route::get('requested-applications', 'LeaveApplicationController@requested_applications')->name('requested-applications.requested');

		Route::resource('benefits', 'BenefitController');
		Route::resource('with-holdings', 'WithHoldingController');
		Route::resource('discounts', 'DiscountController');
		Route::resource('extra-hours', 'ExtraHourController');
		Route::resource('payroll-reports', 'PayrollReportController');

		Route::resource('roles', 'RoleController')->except([
		    'show'
		]);
		Route::get('roles/{role}', function() {
			return redirect()->route('roles.index');
		});

		Route::resource('memberships', 'MembershipController');

		/* Route::resource('leaves', 'LeaveController'); */

		Route::resource('leaves', 'LeaveController', ['parameters' => [
    		'leaves' => 'leave'
		]]);

		Route::post('/paid', 'PayrollReportController@paid')->name('employees.paid');
		Route::resource('history', 'HistoryController');
	});

	Route::prefix('user-management')->middleware([AdminOrSuperAdmin::class])->group(function () {
		 Route::get('users', 'UserManagementController@users')->name('user-management.users');
		Route::resource('users', 'UserManagementController')->except([
		    'index',
		    'show',
		])->names([
		    'create' => 'user-management.users.create',
		    'store' => 'user-management.users.store',
		    'edit' => 'user-management.users.edit',
		    'update' => 'user-management.users.update',
		    'destroy' => 'user-management.users.destroy'
		])->parameters([
		    'users' => 'company_user'
		]); 

		/*Route::resource('roles', 'RoleController')->except([
		    'show'
		])->names([
		    'index' => 'user-management.roles.create',
		    'create' => 'user-management.roles.create',
		    'store' => 'user-management.roles.store',
		    'edit' => 'user-management.roles.edit',
		    'update' => 'user-management.roles.update',
		    'destroy' => 'user-management.roles.destroy'
		]);*/
		

		// Route::get('roles', 'UserManagementController@roles')->name('user-management.roles');
		// Route::match(['get', 'post'], 'roles/create', 'UserManagementController@createRole')->name('user-management.roles.create');
		// Route::get('roles/{role}', 'UserManagementController@editRole')->name('user-management.roles.edit');
		// Route::put('roles/{role}/update', 'UserManagementController@assignPermissionsToRole')->name('user-management.roles.update');
	});

	Route::group(['middleware' => ['permission:manage inventories|manage contacts']], function () {
		Route::get('/import/{resource}', 'ImportController@showForm')->where('resource', 'clients|suppliers|inventories|item-categories|banks')->name('import_resource');
		Route::post('/import/{resource}', 'ImportController@processImport')->where('resource', 'clients|suppliers|inventories|item-categories|banks')->name('import_process');

		Route::get('/export/{resource}', 'ExportController@export')->where('resource', 'clients|suppliers|inventories|item-categories')->name('export_resource');
	});

	Route::prefix('app')->group(function () {
	    Route::post('generate-item-row', 'AppController@generateItemRow');
	    Route::post('generate-concept-row', 'AppController@generateConceptRow');
	    Route::post('contact-invoices-row', 'AppController@getContactInvoicesRows');
	    Route::post('contact-credit-notes', 'AppController@getContactCreditNotes');
	    Route::post('contact-debit-notes', 'AppController@getContactDebitNotes');
	    // Route::post('set-locale', 'AppController@setLocale')->withoutMiddleware(['completed']);
	    Route::get('clear-cache', 'AppController@clearCache');
	    Route::get('coming-soon', 'AppController@comingSoon')->name('coming-soon');
		Route::get('contact-leave-balance', 'AppController@getContactLeaveBalance');
	    Route::post('generate-account-adjustment-row', 'AppController@generateAccountAdjustmentRow');
	    Route::post('generate-depr-item-row', 'AppController@generateDeprItemRow');
	    Route::get('get-depreciation-timeline', 'AppController@getDepreciationTimeline');
	});

	Route::get('generate-invoices-payments', 'AppController@generateInvoicesPayments');
	Route::post('documents/upload-files','DocumentController@uploadFiles');
	Route::post('documents/delete-file','DocumentController@deleteFile');
	Route::get('currency-exchange-rates', 'CurrencyExchangeRateController@index')->name('currency-exchange-rates.index');

	Route::resource('fixed-assets', 'FixedAssetController');

	


	Route::prefix('accounting')->group(function () {
		Route::resource('charts', 'UserAccountingHeadController', ['names' => [
		    'index' => 'accounting.charts'
		]])->except([
		    'show'
		]);
		Route::get('charts/generate-control-options/{chart}', 'UserAccountingHeadController@generateControlOptions')->name('accounting.generate-control-options');
		Route::resource('adjustments', 'AccountAdjustmentController')->names([
		    'index' => 'accounting.adjustments.index',
		    'create' => 'accounting.adjustments.create',
		    'store' => 'accounting.adjustments.store',
		    'show' => 'accounting.adjustments.show',
		    'edit' => 'accounting.adjustments.edit',
		    'update' => 'accounting.adjustments.update',
		    'destroy' => 'accounting.adjustments.destroy'
		])->parameters([
		    'adjustments' => 'account_adjustment'
		]);
		Route::get('journals', 'AccountingReportController@journals')->name('accounting.journals');
		Route::get('reports', 'AccountingReportController@index')->name('accounting.reports');
		Route::get('reports/balance-sheet', 'AccountingReportController@balanceSheet')->name('accounting.balance-sheet');
		Route::get('reports/income-statement', 'AccountingReportController@incomeStatement')->name('accounting.income-statement');
		Route::get('reports/general-ledger', 'AccountingReportController@generalLedgerReport')->name('accounting.general-ledger-report');
		Route::get('reports/journal-report', 'AccountingReportController@journals')->name('accounting.journal-report');
		// Route::post('charts/{user_accounting_head}', 'AccountingController@updateHead')->name('accounting.update-head');
	});
	Route::get('reports/general', 'AccountingReportController@generalReports')->name('accounting.general-reports');

});

Route::get('delete-user/{user}', 'UserController@deleteUser')->name('delete-user');
Route::post('app/set-locale', 'AppController@setLocale');


Route::group(['middleware' => ['setLocale']], function ($router) {
	// Localization
	Route::get('/js/lang.js', function () {
	    // $strings = Cache::rememberForever('lang.js', function () {
	        $lang = app()->getLocale();
	        $files = glob(resource_path('lang/' . $lang . '/*.php'));
	        $strings = [];
	        foreach ($files as $file) {
	            $name = basename($file, '.php');
	            if ($name !== "lang") {
	                $new_keys = require $file;
	                $strings = array_merge($strings, [$name => $new_keys]);
	            }
	        }
	    //     return $strings;
	    // });
	    header('Content-Type: text/javascript');
	    echo ('window.i18n = ' . json_encode(array("lang" => $strings)) . ';');
	    exit();
	})->name('assets.lang');
});

Route::get('/update-accounts', 'ConceptController@updateUserAccounts');
Route::get('/add-accounts', 'ConceptController@addMoreAccounts');
// Route::get('/fix-inventory-entries', 'ConceptController@fixInventoryEntries');
// Route::get('/fix-inventory-adj-entries', 'ConceptController@fixInventoryAdjEntries');
// Route::get('/fix-tax', 'ConceptController@fixTax');
// Route::get('/fix-inventory', 'ConceptController@fixInventory');
// Route::get('/fix-roles', 'ConceptController@fixRoles');
// Route::get('/create-accounts', 'ConceptController@createChartOfAccountsForAllUsers');
// Route::get('/map-concept-to-accounts', 'ConceptController@mapConceptToAccounts');
// Route::get('/fix-concept', 'ConceptController@fixTree');
// Route::get('/fix-users', 'ConceptController@fixUsers');


Route::get('/js/lang/{locale}/{file_name}', function ($locale, $file_name) {
    $lang = $locale;
    $files = glob(resource_path('lang/' . $lang . '/*.php'));
    $strings = [];
    foreach ($files as $file) {
        $name = basename($file, '.php');
        if ($name == $file_name) {
            $new_keys = require $file;
            $strings = array_merge($strings, [$name => $new_keys]);
        }
    }
    prePrint($strings);
    return response()->json($strings);
});

Route::group(['middleware' => ['auth', 'setLocale', 'xss_protection', 'super_admin']], function ($router) {

	Route::get('/logout', 'Auth\LoginController@logout');

	Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
		Route::get('/', function() {
			return redirect('/admin/dashboard');
		});
		Route::get('/dashboard', 'DashboardController@index')->name('admin-dashboard');
		Route::get('/settings/user', 'UserController@edit')->name('admin.edit');
		Route::post('/settings/user', 'UserController@update')->name('admin.update');
		Route::get('/users', 'UserController@list')->name('admin.users');

		Route::get('/countries', 'ContentManagementController@countries')->name('admin.countries');
		Route::match(['get', 'put'], '/countries/{country}/edit', 'ContentManagementController@updateCountry')->name('country_edit_update');

		Route::get('/sectors', 'ContentManagementController@sectors')->name('admin.sectors');
		Route::match(['get', 'put'], '/sectors/{sector}/edit', 'ContentManagementController@updateSector')->name('sector_edit_update');

		Route::get('/currencies', 'ContentManagementController@currencies')->name('admin.currencies');
		Route::match(['get', 'put'], '/currencies/{currency}/edit', 'ContentManagementController@updateCurrency')->name('currency_edit_update');

		Route::get('/identification-types', 'ContentManagementController@identificationTypes')->name('admin.identification_types');
		Route::match(['get', 'put'], '/identification-types/{identification_type}/edit', 'ContentManagementController@updateIdentificationType')->name('identification_type_edit_update');
		Route::get('/activity-logs', 'UserController@activityLogs')->name('admin.activity_logs');
	});
});

Route::group(['middleware' => ['setLocale']], function ($router) {
	Route::fallback(function () {
	    return view('errors.404');
	});
});

Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');


Route::group(['middleware' => ['auth', 'verified', 'completed', 'setLocale', 'xss_protection']], function ($router) {
    Route::resource('production', 'ProductionController');
	Route::resource('banks', 'BanksController');
});

Route::get('/clearCache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});