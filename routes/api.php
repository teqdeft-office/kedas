<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Http\Middleware\AdminOrSuperAdmin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'setLocale', 'namespace' => 'Api'], function(){

	// Authentication related routes.
	Route::post('/register','AuthenticateController@register');
	Route::post('/forgot-password', 'AuthenticateController@forgotPassword');
	Route::post('/login', 'AuthenticateController@login');
	Route::post('/verify-account', 'AuthenticateController@verifyAccount');
	Route::post('/reset-password', 'AuthenticateController@resetPassword');
	Route::post('/resend-code', 'AuthenticateController@resendCode');

	Route::group(['middleware' => ['auth:api', 'app_user', 'verify_email_token']], function () {
		Route::post('/set-locale', 'UserController@setLocale'); // For update profile data
		Route::post('/profile', 'UserController@updateProfile'); // For update profile data

		Route::group(['middleware' => ['verify_profile_update']], function () {
			Route::get('/profile', 'UserController@index'); // For profile page

			Route::post('/change-password', 'AuthenticateController@changePassword');
			Route::post('/logout', 'AuthenticateController@logout');
			Route::post('/refresh-token', 'AuthenticateController@refreshToken');

			
			Route::get('/stores', 'StoreController@index'); // Listing the store data
			Route::get('/stores/{store}', 'StoreController@show')->where('store', '[0-9]+'); // single store data
			Route::get('/stores/favourites', 'StoreController@clientFavouriteStores'); // list favourite store data

			Route::post('/stores/favourites', 'StoreController@store'); // save favourite store data
			Route::put('/stores/{id}/make-primary', 'StoreController@savePrimaryStore');
			
			Route::delete('/stores/favourites/{id}', 'StoreController@destroy'); // delete favourite store data			

			Route::group(['prefix' => 'clients', 'namespace' => 'Client'], function(){
				Route::post('/invoices/{invoice}/upload-receipt', 'InvoiceController@uploadReceipt');
				Route::post('/invoices/{invoice}/pay-now', 'InvoiceController@payNow');
				Route::get('/invoices/{invoice}', 'InvoiceController@show')->where('invoice', '[0-9]+');
				Route::get('/invoices/{type}', 'InvoiceController@index')->where('type', 'sale|recurring');
				Route::get('/invoices/{invoice}/download', 'InvoiceController@download');
				Route::get('/estimates', 'EstimateController@index');
				Route::get('/credit-notes', 'CreditNoteController@index');
				Route::get('/debit-notes', 'DebitNoteController@index');
				Route::get('/payments', 'PaymentController@index');
				Route::get('/payments/{transaction}', 'PaymentController@show')->where('transaction', '[0-9]+');
			});

			Route::post('/ratings', 'RatingController@store');
		});
	});

	Route::get('/static-content-old', function () {
		// Route::get('/js/lang.js', function () {
	    // $strings = Cache::rememberForever('lang.js', function () {
		$lang = app()->getLocale();
		$language = array_keys(Config::get('constants.languages'));

		foreach ($language as $lang) {
			app()->setLocale($lang);
			$files = glob(resource_path('lang/' . $lang . '/*.php'));
			$strings = [];
			foreach ($files as $file) {
				$name = basename($file, '.php');
				if ($name !== "lang") {
					$new_keys = require $file;
					$strings = array_merge($strings, [$name => $new_keys]);
				}
			}
			$allLangArray[$lang] = $strings;

			$allLangArray[$lang]['permissions'] = Cache::get('permissions')->map(function($permission) {
				return $permission->only('custom_name', 'id', 'name');
			});

			$allLangArray[$lang]['countries'] = Cache::get('countries')->map(function($country) {
				return $country->only('name', 'id');
			});

			$allLangArray[$lang]['currencies'] = Cache::get('currencies')->map(function($currency) {
				return $currency->only('name', 'id', 'formatted_name', 'symbol');
			});

			$allLangArray[$lang]['sectors'] = Cache::get('sectors')->map(function($sector) {
				return $sector->only('name', 'id');
			});

			$allLangArray[$lang]['user_types'] = [
				['value' => "business_owner", 'text' => trans('labels.own_a_business')],
				['value' => "independent", 'text' => trans('labels.an_independent')],
				['value' => "student", 'text' => trans('labels.an_student')],
				['value' => "accountant", 'text' => trans('labels.an_accountant')],
				['value' => "other", 'text' => trans('labels.others')]
			];
		}

		$staticContentArray = [
			'lang' => $allLangArray
		];

		header('Content-Type: text/json');
		echo json_encode($staticContentArray);
		exit();
	})->name('assets.lang');

	Route::post('set-locale', 'UserController@setLocale');

	Route::group(['prefix' => 'auth'], function() {
		Route::post('register','AuthController@register');
		Route::post('login', 'AuthController@login');
		Route::post('password/reset', 'AuthController@forgotPassword');
		Route::post('logout', 'AuthController@logout')->middleware('auth:api');
		Route::post('refresh-token', 'AuthController@refreshToken')->middleware('auth:api');
	});

	Route::group(['middleware' => ['auth:api', 'setLocale', 'verified', 'completed']], function () {
		Route::post('auth/email/resend', 'AuthController@resendVerifyEmail')->withoutMiddleware(['verified', 'completed']);
		Route::post('user/profile', 'UserController@completeProfile')->withoutMiddleware(['completed']);

		Route::middleware([AdminOrSuperAdmin::class])->group(function () {

			Route::match(['get', 'put'], 'user/me', 'UserController@profile');
			Route::match(['get', 'post'], 'user/company', 'UserController@company');
			Route::apiResource('ncfs', 'NcfController');
			Route::apiResource('taxes', 'TaxController');
			Route::apiResource('item-categories', 'ItemCategoryController');
			Route::apiResource('inventory-adjustments', 'InventoryAdjustmentController');
			Route::apiResource('inventories', 'InventoryController');
			Route::get('user/currency-exchange-rates', 'UserController@currencyExchangeRates');
			Route::get('accounting/charts', 'UserAccountingHeadController@index');
			Route::apiResource('clients', 'ClientController');
			Route::apiResource('suppliers', 'SupplierController');

			Route::prefix('user-management')->group(function () {
				Route::apiResource('users', 'UserManagementController')->parameters([
				    'users' => 'company_user'
				]);

				Route::get('roles', 'UserManagementController@roles');
				Route::get('roles/{role}', 'UserManagementController@showRole');
				Route::put('roles/{role}', 'UserManagementController@assignPermissionsToRole');
			});
			Route::apiResource('memberships', 'MembershipController');
			Route::apiResource('roles', 'RoleController');
			Route::apiResource('credit-notes', 'CreditNoteController');
			Route::apiResource('estimates', 'EstimateController');
			Route::apiResource('remissions', 'RemissionController');
			Route::apiResource('debit-notes', 'DebitNoteController');
			Route::apiResource('purchase-orders', 'PurchaseOrderController');

			Route::get('invoices/{type}', 'InvoiceController@index')->where('type', 'sale|recurring|supplier');
			Route::post('invoices/{type}', 'InvoiceController@store')->where('type', 'sale|recurring|supplier');
			Route::post('invoices/{invoice}/void', 'InvoiceController@void');

			Route::apiResource('invoices', 'InvoiceController')->except([
			    'store', 'index'
			]);

			Route::get('transactions/{type}', 'TransactionController@index')->where('type', 'in|out|recurring');
			Route::post('transactions/{type}', 'TransactionController@store')->where('type', 'in|out|recurring');

			Route::resource('transactions', 'TransactionController')->except([
			    'store', 'index'
			]);

			Route::apiResource('purchase-orders', 'PurchaseOrderController');
		});
	});
});

Route::group(['prefix' => 'v1', 'middleware' => 'setLocale'], function() {
	Route::get('/static-content', 'AppController@staticContent');
	Route::fallback(function () {
		return response()->json(['status' => false, 'message' => trans('messages.resource_not_found')], 404);
	});
});