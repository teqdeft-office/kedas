<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            [
                'user_id' => '1',
                'identification_type_id' => '7',
                'tax_id' => '123456',
                'name' => 'John Smith(Client)',
                'email' => 'johnsmith@yopmail.com',
                'sector_id' => '6',
                'phone' => '1234567890',
                'fax' => '888888',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'identification_type_id' => '5',
                'tax_id' => '123456',
                'name' => 'JK motors client',
                'email' => 'jkmotor@yopmail.com',
                'sector_id' => '5',
                'phone' => '0987654321',
                'fax' => '999999',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'identification_type_id' => '5',
                'tax_id' => '654321',
                'name' => 'John Deo(Client)',
                'email' => 'johndeo@yopmail.com',
                'sector_id' => '5',
                'phone' => '0987654321',
                'fax' => '999999',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
