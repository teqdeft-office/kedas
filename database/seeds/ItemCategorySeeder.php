<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ItemCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_categories')->insert([
            [
                'user_id' => '1',
                'name' => 'Bike spare parts',
                'description' => 'bike spare parts description',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'name' => 'Car spare parts',
                'description' => 'car spare parts description',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
