<?php

use Illuminate\Database\Seeder;

class UpdateItemPriceWithInventoryPrice extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->join('inventories', 'items.inventory_id', '=', 'inventories.id')->update(['items.price' => DB::raw('inventories.sale_price')]);
    }
}
