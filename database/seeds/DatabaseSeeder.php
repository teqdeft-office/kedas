<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $seederClasses = [
            SectorSeeder::class,  
            CountrySeeder::class,
            CurrencySeeder::class,  
            IdentificationTypeSeeder::class,
            // ConceptSeeder::class,
            PermissionSeeder::class,
            // The following AccountingHeadSeeder is the latest one.
            AccountingHeadSeeder::class,
            UpdateAccountingHeadsSeeder::class,
        ];

        if (App::Environment('local')) {
            $seederClasses = array_merge($seederClasses, [
                UserSeeder::class,
                ItemCategorySeeder::class,
                TaxSeeder::class,
                ClientSeeder::class,
                SupplierSeeder::class,
                InventorySeeder::class
            ]);
        }

        $this->call($seederClasses);
    }
}
