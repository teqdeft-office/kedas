<?php

use Illuminate\Database\Seeder;

class UpdateCustomNameForRolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("permissions")->update(["custom_name" => DB::raw('CASE 
        WHEN (name = "manage sales") THEN \'{"en":"manage sales", "es":"administrar ventas"}\'
        WHEN (name = "manage expenses") THEN \'{"en":"manage expenses", "es":"administrar gastos"}\'
        WHEN (name = "manage inventories") THEN \'{"en":"manage inventories", "es":"administrar inventarios"}\'
        WHEN (name = "manage contacts") THEN \'{"en":"manage contacts", "es":"gestionar contactos"}\'
        end')]);

        DB::table("roles")->update(["custom_name" => DB::raw('CASE 
        WHEN (name = "admin") THEN \'{"en":"admin", "es":"administración"}\'
        WHEN (name = "manager") THEN \'{"en":"manager", "es":"gerente"}\'
        WHEN (name = "employee") THEN \'{"en":"employee", "es":"empleada"}\'
        end')]);
    }
}
