<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class AddNewPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'manage payroll',
                'custom_name' => '{"en":"Manage Payroll", "es":"Administrar nómina"}',
                'guard_name' => 'web',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'manage accounts',
                'custom_name' => '{"en":"Manage Accounts", "es":"Cuentas de administración"}',
                'guard_name' => 'web',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
