<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'sachin kumar',
                'email' => 'sachin@yopmail.com',
                'password' => Hash::make('123456'),
                'email_verified_at' => Carbon::now(),
                'user_type' => 'independent',
                'user_role' => 'admin',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'sumit kumar',
                'email' => 'sumit@yopmail.com',
                'password' => Hash::make('123456'),
                'email_verified_at' => Carbon::now(),
                'user_type' => 'independent',
                'user_role' => 'admin',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ]
        ]);
        /*DB::table('companies')->insert([
            [
                'user_id' => '1',
                'name' => 'Sk spare parts pvt ltd.',
                'role' => 'Accountant',
                'tax_id' => '9034327894',
                'support_email' => 'sachin@yopmail.com',
                'phone' => '9034327894',
                'fax' => '123456',
                'sector_id' => '16',
                'currency_id' => '113',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
        ]);*/
    }
}
