<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\AccountingHead;


class UpdateAccountingHeadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $obe = AccountingHead::where('code', 3006)->first();

        $obeChildren = AccountingHead::create([
            'name' =>  [
                'en' => 'Initial adjustment in inventory',
                'es' => 'Ajuste inicial en inventario'
            ],
            'incr_nature' => 'credit',
            'decr_nature' => 'debit',
            'slug' => strToSlug('Initial adjustment in inventory', '-'),
            'description' => null,
            'code' => 3501,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $obe->appendNode($obeChildren);

        $invAdj = AccountingHead::where('code', 5011)->first();
        $invAdj->code = 3502;
        $invAdj->incr_nature = 'credit';
        $invAdj->decr_nature = 'debit';
        $invAdj->appendToNode($obe)->save();

        AccountingHead::where('code', 1002)->first()->setTranslation('name', 'es', 'Comprobacion')->save();
        AccountingHead::where('code', 3)->first()->setTranslation('name', 'es', 'Patrimonio')->save();
        AccountingHead::where('code', 3000)->first()->setTranslation('name', 'es', 'Patrimonio')->save();
        AccountingHead::where('code', 1002)->first()->setTranslation('name', 'es', 'Cuentas corrientes')->setTranslation('name', 'en', 'Checking Accounts')->save();
        AccountingHead::where('code', 1005)->first()->setTranslation('name', 'es', 'Cuenta de Ahorros')->setTranslation('name', 'en', 'Savings Account')->save();
        AccountingHead::where('code', 1001)->first()->setTranslation('name', 'es', 'Caja')->save();
    }
}
