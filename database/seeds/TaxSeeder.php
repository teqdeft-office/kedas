<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxes')->insert([
            [
                'user_id' => '1',
                'name' => 'GST',
                'percentage' => '18',
                'description' => 'GST India',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'name' => 'CGST',
                'percentage' => '9',
                'description' => 'CGST India',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'name' => 'SGST',
                'percentage' => '9',
                'description' => 'SGST India',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
