<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\AccountingHead;

class AccountingHeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AccountingHead::create([
            'name' =>  [
                'en' => 'Assets',
                'es' => 'Activos'
            ],
            'incr_nature' => 'debit',
            'decr_nature' => 'credit',
            'slug' => strToSlug('Assets', '-'),
            'description' => null,
            'code' => 1,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'children' => [
                [
                    'name' =>  [
                        'en' => 'Accounts receivable',
                        'es' => 'Cuentas por cobrar'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Accounts receivable', '-'),
                    'description' => null,
                    'code' => 1100,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Accounts receivable',
                                'es' => 'Cuentas por cobrar'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Accounts receivable', '-'),
                            'description' => null,
                            'code' => 1101,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],

                [
                    'name' =>  [
                        'en' => 'Other current assets',
                        'es' => 'Otros activos circulantes'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Other current assets', '-'),
                    'description' => null,
                    'code' => 1200,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Allowance for bad debts',
                                'es' => 'Subsidio para insolvencias'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Allowance for bad debts', '-'),
                            'description' => null,
                            'code' => 1201,
                            'has_exception' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Development costs',
                                'es' => 'Costos de desarrollo'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Development costs', '-'),
                            'description' => null,
                            'code' => 1202,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Employee cash advances',
                                'es' => 'Anticipos en efectivo para empleados'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Employee cash advances', '-'),
                            'description' => null,
                            'code' => 1203,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Inventory',
                                'es' => 'Inventario'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Inventory', '-'),
                            'description' => null,
                            'code' => 1204,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Investments - Mortgage/real estate loans',
                                'es' => 'Inversiones - Préstamos hipotecarios / inmobiliarios'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Investments - Mortgage/real estate loans', '-'),
                            'description' => null,
                            'code' => 1205,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Investments - Tax-exempt securities',
                                'es' => 'Inversiones: valores exentos de impuestos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Investments - Tax-exempt securities', '-'),
                            'description' => null,
                            'code' => 1206,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Investments - U.S. government obligations',
                                'es' => 'Inversiones: obligaciones del gobierno de EE. UU.'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Investments - U.S. government obligations', '-'),
                            'description' => null,
                            'code' => 1207,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Investments - Other',
                                'es' => 'Inversiones - Otros'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Investments - Other', '-'),
                            'description' => null,
                            'code' => 1208,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Loans to officers',
                                'es' => 'Préstamos a los oficiales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Loans to officers', '-'),
                            'description' => null,
                            'code' => 1209,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Loans to others',
                                'es' => 'Préstamos a otros'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Loans to others', '-'),
                            'description' => null,
                            'code' => 1210,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Loans to stockholders',
                                'es' => 'Préstamos a accionistas'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Loans to stockholders', '-'),
                            'description' => null,
                            'code' => 1211,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other current assets',
                                'es' => 'Otros activos circulantes'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other current assets', '-'),
                            'description' => null,
                            'code' => 1212,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Prepaid expenses',
                                'es' => 'Gastos pagados por anticipado'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Prepaid expenses', '-'),
                            'description' => null,
                            'code' => 1213,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Retainage',
                                'es' => 'Retención'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Retainage', '-'),
                            'description' => null,
                            'code' => 1214,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Undeposited fund',
                                'es' => 'Fondo no depositado'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Undeposited fund', '-'),
                            'description' => null,
                            'code' => 1215,
                            'visibility' => true,
                            'can_be_parent' => false,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],                      
                    ],
                ],

                [
                    'name' =>  [
                        'en' => 'Bank',
                        'es' => 'Banco'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Bank', '-'),
                    'description' => null,
                    'code' => 1000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Cash on hand',
                                'es' => 'Dinero en mano'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Cash on hand', '-'),
                            'description' => null,
                            'code' => 1001,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Checking',
                                'es' => 'Comprobacion'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Checking', '-'),
                            'description' => null,
                            'code' => 1002,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Money market',
                                'es' => 'Mercado de dinero'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Money market', '-'),
                            'description' => null,
                            'code' => 1003,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Rents held in trust',
                                'es' => 'Alquileres mantenidos en fideicomiso'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Rents held in trust', '-'),
                            'description' => null,
                            'code' => 1004,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Savings',
                                'es' => 'Ahorros'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Savings', '-'),
                            'description' => null,
                            'code' => 1005,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Trust accounts',
                                'es' => 'Cuentas fiduciarias'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Trust accounts', '-'),
                            'description' => null,
                            'code' => 1006,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],

                [
                    'name' =>  [
                        'en' => 'Fixed Assets',
                        'es' => 'Activos fijos'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Fixed Assets', '-'),
                    'description' => null,
                    'code' => 1500,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Accumulated amortization',
                                'es' => 'Amortización acumulada'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Accumulated amortization', '-'),
                            'description' => null,
                            'code' => 1501,
                            'has_exception' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Accumulated depletion',
                                'es' => 'Agotamiento acumulado'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Accumulated depletion', '-'),
                            'description' => null,
                            'code' => 1502,
                            'has_exception' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Accumulated depreciation',
                                'es' => 'Depreciación acumulada'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Accumulated depreciation', '-'),
                            'description' => null,
                            'code' => 1503,
                            'has_exception' => 1,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Buildings',
                                'es' => 'Edificios'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Buildings', '-'),
                            'description' => null,
                            'code' => 1504,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Depletable assets',
                                'es' => 'Activos agotables'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Depletable assets', '-'),
                            'description' => null,
                            'code' => 1505,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Assets Computers',
                                'es' => 'Computadoras de activos fijos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Assets Computers', '-'),
                            'description' => null,
                            'code' => 1506,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Assets copiers',
                                'es' => 'Copiadoras de activos fijos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Assets copiers', '-'),
                            'description' => null,
                            'code' => 1507,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Assets Furniture',
                                'es' => 'Mobiliario de activos fijos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Assets Furniture', '-'),
                            'description' => null,
                            'code' => 1508,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Assets other Tools Equipment',
                                'es' => 'Activos fijos Otras herramientas Equipo'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Assets other Tools Equipment', '-'),
                            'description' => null,
                            'code' => 1509,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Assets Phone',
                                'es' => 'Teléfono de activos fijos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Assets Phone', '-'),
                            'description' => null,
                            'code' => 1510,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Asset photo video',
                                'es' => 'Video fotográfico de activos fijos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Asset photo video', '-'),
                            'description' => null,
                            'code' => 1511,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Fixed Asset Software',
                                'es' => 'Software de activos fijos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Fixed Asset Software', '-'),
                            'description' => null,
                            'code' => 1512,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Furniture & fixtures',
                                'es' => 'Muebles y Accesorios'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Furniture & fixtures', '-'),
                            'description' => null,
                            'code' => 1513,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Intangible assets',
                                'es' => 'Activos intangibles'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Intangible assets', '-'),
                            'description' => null,
                            'code' => 1514,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Land',
                                'es' => 'Tierra'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Land', '-'),
                            'description' => null,
                            'code' => 1515,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Leasehold improvements',
                                'es' => 'Mejoras arrendatarias'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Leasehold improvements', '-'),
                            'description' => null,
                            'code' => 1516,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Machinery & equipment',
                                'es' => 'Equipo de maquinaria'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Machinery & equipment', '-'),
                            'description' => null,
                            'code' => 1517,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other fixed asset',
                                'es' => 'Otro activo fijo'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other fixed asset', '-'),
                            'description' => null,
                            'code' => 1518,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles',
                                'es' => 'Vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles', '-'),
                            'description' => null,
                            'code' => 1519,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],

                [
                    'name' =>  [
                        'en' => 'Other Asset',
                        'es' => 'Otros activos'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Other Asset', '-'),
                    'description' => null,
                    'code' => 1800,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Accumulated amortization of other assets',
                                'es' => 'Amortización acumulada de otros activos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Accumulated amortization of other assets', '-'),
                            'description' => null,
                            'code' => 1801,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Goodwill',
                                'es' => 'Buena voluntad'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Goodwill', '-'),
                            'description' => null,
                            'code' => 1802,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Lease buyout',
                                'es' => 'Compra de arrendamiento'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Lease buyout', '-'),
                            'description' => null,
                            'code' => 1803,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Licenses',
                                'es' => 'Licencias'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Licenses', '-'),
                            'description' => null,
                            'code' => 1804,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Organizational costs',
                                'es' => 'Costos organizacionales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Organizational costs', '-'),
                            'description' => null,
                            'code' => 1805,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other long-term assets',
                                'es' => 'Otros activos a largo plazo'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other long-term assets', '-'),
                            'description' => null,
                            'code' => 1806,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Security deposits',
                                'es' => 'Depositos de seguridad'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Security deposits', '-'),
                            'description' => null,
                            'code' => 1807,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ]
            ],
        ]);

        AccountingHead::create([
            'name' =>  [
                'en' => 'Liabilities',
                'es' => 'Pasivo'
            ],
            'incr_nature' => 'credit',
            'decr_nature' => 'debit',
            'slug' => strToSlug('Liabilities', '-'),
            'description' => null,
            'code' => 2,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'children' => [
                [
                    'name' =>  [
                        'en' => 'Accounts payable',
                        'es' => 'Cuentas por pagar'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Accounts payable', '-'),
                    'description' => null,
                    'code' => 2000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Accounts payable',
                                'es' => 'Cuentas por pagar'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Accounts payable', '-'),
                            'description' => null,
                            'code' => 2001,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]
                    ]
                ],[
                    'name' =>  [
                        'en' => 'Credit card',
                        'es' => 'Tarjeta de crédito'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Credit card', '-'),
                    'description' => null,
                    'code' => 2100,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Credit card',
                                'es' => 'Tarjeta de crédito'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Credit card', '-'),
                            'description' => null,
                            'code' => 2101,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]
                    ]
                ],[
                    'name' =>  [
                        'en' => 'Other current liabilities',
                        'es' => 'Otros pasivos corrientes'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Other current liabilities', '-'),
                    'description' => null,
                    'code' => 2500,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Federal Income Tax Payable',
                                'es' => 'Impuesto sobre la renta federal a pagar'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Federal Income Tax Payable', '-'),
                            'description' => null,
                            'code' => 2501,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Insurance payable',
                                'es' => 'Seguro a pagar'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Insurance payable', '-'),
                            'description' => null,
                            'code' => 2502,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Line of credit',
                                'es' => 'Línea de crédito'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Line of credit', '-'),
                            'description' => null,
                            'code' => 2503,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Loan payable',
                                'es' => 'Préstamo pagadero'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Loan payable', '-'),
                            'description' => null,
                            'code' => 2504,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other current liabilities',
                                'es' => 'Otros pasivos corrientes'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Other current liabilities', '-'),
                            'description' => null,
                            'code' => 2505,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Payroll clearing',
                                'es' => 'Compensación de nómina'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Payroll clearing', '-'),
                            'description' => null,
                            'code' => 2506,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Payroll tax payable',
                                'es' => 'Impuesto sobre la nómina a pagar'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Payroll tax payable', '-'),
                            'description' => null,
                            'code' => 2507,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Prepaid expenses payable',
                                'es' => 'Gastos pagados por adelantado'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Prepaid expenses payable', '-'),
                            'description' => null,
                            'code' => 2508,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Rents in trust - liability',
                                'es' => 'Alquileres en fideicomiso - responsabilidad'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Rents in trust - liability', '-'),
                            'description' => null,
                            'code' => 2509,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Sales tax payable',
                                'es' => 'Impuesto sobre las ventas a pagar'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Sales tax payable', '-'),
                            'description' => null,
                            'code' => 2510,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'State/local income tax payable',
                                'es' => 'Impuesto sobre la renta estatal / local pagadero'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('State/local income tax payable', '-'),
                            'description' => null,
                            'code' => 2511,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Trust accounts - liabilities',
                                'es' => 'Cuentas fiduciarias - pasivo'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Trust accounts - liabilities', '-'),
                            'description' => null,
                            'code' => 2512,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Undistributed Tips',
                                'es' => 'Propinas no distribuidas'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Undistributed Tips', '-'),
                            'description' => null,
                            'code' => 2513,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],[
                    'name' =>  [
                        'en' => 'Long term liabilities',
                        'es' => 'Pasivos a largo plazo'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Long term liabilities', '-'),
                    'description' => null,
                    'code' => 2800,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Notes payable',
                                'es' => 'Pagar'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Notes payable', '-'),
                            'description' => null,
                            'code' => 2801,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other long term liabilities',
                                'es' => 'Otros pasivos a largo plazo'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Other long term liabilities', '-'),
                            'description' => null,
                            'code' => 2802,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Shareholder notes payable',
                                'es' => 'Pagarés de accionistas'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Shareholder notes payable', '-'),
                            'description' => null,
                            'code' => 2803,
                            'visibility' => false,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],
            ],
        ]);
        
        AccountingHead::create([
            'name' =>  [
                'en' => 'Equities',
                'es' => 'Patrimonio'
            ],
            'incr_nature' => 'credit',
            'decr_nature' => 'debit',
            'slug' => strToSlug('Equities', '-'),
            'description' => null,
            'code' => 3,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'children' => [
                [
                    'name' =>  [
                        'en' => 'Equity',
                        'es' => 'Patrimonio'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Equity', '-'),
                    'description' => null,
                    'code' => 3000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Accumulated Adjustment',
                                'es' => 'Ajuste acumulado'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Accumulated Adjustment', '-'),
                            'description' => null,
                            'code' => 3001,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Common stock',
                                'es' => 'Acciones comunes'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Common stock', '-'),
                            'description' => null,
                            'code' => 3002,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Estimated Taxes',
                                'es' => 'Impuestos estimados'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Estimated Taxes', '-'),
                            'description' => null,
                            'code' => 3003,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Health Insurance Premium',
                                'es' => 'Prima del seguro médico'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Health Insurance Premium', '-'),
                            'description' => null,
                            'code' => 3004,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Health saving Account Contribution',
                                'es' => 'Contribución a la cuenta de ahorro de salud'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Health saving Account Contribution', '-'),
                            'description' => null,
                            'code' => 3005,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Opening balance equity',
                                'es' => 'Equidad del saldo inicial'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Opening balance equity', '-'),
                            'description' => null,
                            'code' => 3006,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Owner’s equity',
                                'es' => 'Capital del propietario'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Owner’s equity', '-'),
                            'description' => null,
                            'code' => 3007,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Paid-in capital or Surplus',
                                'es' => 'Capital pagado o excedente'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Paid-in capital or Surplus', '-'),
                            'description' => null,
                            'code' => 3008,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Partner contributions',
                                'es' => 'Contribuciones de socios'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Partner contributions', '-'),
                            'description' => null,
                            'code' => 3009,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Partner distributions',
                                'es' => 'Distribuciones de socios'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Partner distributions', '-'),
                            'description' => null,
                            'code' => 3010,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Partner’s equity',
                                'es' => 'Equidad del socio'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Partner’s equity', '-'),
                            'description' => null,
                            'code' => 3011,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Personal expense',
                                'es' => 'Gasto personal'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Personal expense', '-'),
                            'description' => null,
                            'code' => 3012,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Personal Income',
                                'es' => 'Renta personal'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Personal Income', '-'),
                            'description' => null,
                            'code' => 3013,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Preferred Stock',
                                'es' => 'Acciones preferentes'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Preferred Stock', '-'),
                            'description' => null,
                            'code' => 3014,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Retained earnings',
                                'es' => 'Ganancias retenidas'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Retained earnings', '-'),
                            'description' => null,
                            'code' => 3015,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Treasury stock',
                                'es' => 'Acciones de tesorería'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Treasury stock', '-'),
                            'description' => null,
                            'code' => 3016,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],
            ]
        ]);

        AccountingHead::create([
            'name' =>  [
                'en' => 'Income',
                'es' => 'Ingreso'
            ],
            'incr_nature' => 'credit',
            'decr_nature' => 'debit',
            'slug' => strToSlug('Income', '-'),
            'description' => null,
            'code' => 4,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'children' => [
                [
                    'name' =>  [
                        'en' => 'Incomes',
                        'es' => 'Ingresos'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Incomes', '-'),
                    'description' => null,
                    'code' => 4000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Discounts/refunds given',
                                'es' => 'Descuentos / reembolsos otorgados'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Discounts/refunds given', '-'),
                            'description' => null,
                            'code' => 4001,
                            'has_exception' => 1,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Non-profit income',
                                'es' => 'Ingresos sin fines de lucro'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Non-profit income', '-'),
                            'description' => null,
                            'code' => 4002,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other primary income',
                                'es' => 'Otros ingresos primarios'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Other primary income', '-'),
                            'description' => null,
                            'code' => 4003,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Sales of product income',
                                'es' => 'Ventas de ingresos por productos'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Sales of product income', '-'),
                            'description' => null,
                            'code' => 4004,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Service/fee income',
                                'es' => 'Ingresos por servicios / tarifas'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Service/fee income', '-'),
                            'description' => null,
                            'code' => 4005,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Unapplied Cash Payment Income',
                                'es' => 'Ingresos por pagos en efectivo no aplicados'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Unapplied Cash Payment Income', '-'),
                            'description' => null,
                            'code' => 4006,
                            'can_be_parent' => false,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ],
                ],
                [
                    'name' =>  [
                        'en' => 'Other Income',
                        'es' => 'Otros ingresos'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Other Income', '-'),
                    'description' => null,
                    'code' => 8000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Dividend income',
                                'es' => 'Los ingresos por dividendos'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Dividend income', '-'),
                            'description' => null,
                            'code' => 8001,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Interest earned',
                                'es' => 'Interés obtenido'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Interest earned', '-'),
                            'description' => null,
                            'code' => 8002,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other investment income',
                                'es' => 'Otros ingresos por inversiones'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Other investment income', '-'),
                            'description' => null,
                            'code' => 8003,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other miscellaneous income',
                                'es' => 'Otros ingresos varios'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Other miscellaneous income', '-'),
                            'description' => null,
                            'code' => 8004,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Tax-exempt interest',
                                'es' => 'Intereses exentos de impuestos'
                            ],
                            'incr_nature' => 'credit',
                            'decr_nature' => 'debit',
                            'slug' => strToSlug('Tax-exempt interest', '-'),
                            'description' => null,
                            'code' => 8005,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ],
                ]    
            ]
        ]);

        AccountingHead::create([
            'name' =>  [
                'en' => 'Expenses',
                'es' => 'Gastos'
            ],
            'incr_nature' => 'debit',
            'decr_nature' => 'credit',
            'slug' => strToSlug('Expenses', '-'),
            'description' => null,
            'code' => 5,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'children' => [             
                [
                    'name' =>  [
                        'en' => 'Cost of Goods Sold',
                        'es' => 'Costo de los bienes vendidos'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Cost of Goods Sold', '-'),
                    'description' => null,
                    'code' => 5000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Cost of labor - COS',
                                'es' => 'Costo de mano de obra - COS'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Cost of labor - COS', '-'),
                            'description' => null,
                            'code' => 5001,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Equipment rental - COS',
                                'es' => 'Alquiler de equipos - COS'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Equipment rental - COS', '-'),
                            'description' => null,
                            'code' => 5002,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other costs of service - COS',
                                'es' => 'Otros costos de servicio - COS'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other costs of service - COS', '-'),
                            'description' => null,
                            'code' => 5003,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Shipping, freight & delivery - COGS',
                                'es' => 'Envío, flete y entrega - COGS'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Shipping, freight & delivery - COGS', '-'),
                            'description' => null,
                            'code' => 5004,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Supplies & materials - COGS',
                                'es' => 'Suministros y materiales - COGS'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Supplies & materials - COGS', '-'),
                            'description' => null,
                            'code' => 5005,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ], [
                            'name' =>  [
                                'en' => 'Cost of Goods Sold',
                                'es' => 'Costo de los bienes vendidos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Cost of Goods Sold', '-'),
                            'description' => null,
                            'code' => 5006,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]
                    ]
                ],
                [
                    'name' =>  [
                        'en' => 'Expenses',
                        'es' => 'Gastos'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Expenses', '-'),
                    'description' => null,
                    'code' => 6000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Advertising/promotional',
                                'es' => 'Publicidad / promocional'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Advertising/promotional', '-'),
                            'description' => null,
                            'code' => 6001,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Auto',
                                'es' => 'Auto'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Auto', '-'),
                            'description' => null,
                            'code' => 6002,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Bad debt',
                                'es' => 'Deuda incobrable'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Bad debt', '-'),
                            'description' => null,
                            'code' => 6003,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Bank charges',
                                'es' => 'cargos bancarios'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Bank charges', '-'),
                            'description' => null,
                            'code' => 6004,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Charitable contributions',
                                'es' => 'Contribuciones caritativas'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Charitable contributions', '-'),
                            'description' => null,
                            'code' => 6005,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Communication',
                                'es' => 'Comunicación'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Communication', '-'),
                            'description' => null,
                            'code' => 6006,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Cost of labor',
                                'es' => 'Costo de mano de obra'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Cost of labor', '-'),
                            'description' => null,
                            'code' => 6007,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Dues & subscriptions',
                                'es' => 'Cuotas y suscripciones'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Dues & subscriptions', '-'),
                            'description' => null,
                            'code' => 6008,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Entertainment',
                                'es' => 'Entretenimiento'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Entertainment', '-'),
                            'description' => null,
                            'code' => 6009,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Entertainment meals',
                                'es' => 'Comidas de entretenimiento'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Entertainment meals', '-'),
                            'description' => null,
                            'code' => 6010,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Equipment rental',
                                'es' => 'Renta de equipo'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Equipment rental', '-'),
                            'description' => null,
                            'code' => 6011,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Finance Cost',
                                'es' => 'Costo financiero'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Finance Cost', '-'),
                            'description' => null,
                            'code' => 6012,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Insurance',
                                'es' => 'Seguro'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Insurance', '-'),
                            'description' => null,
                            'code' => 6013,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Interest paid',
                                'es' => 'Pago interesado'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Interest paid', '-'),
                            'description' => null,
                            'code' => 6014,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Legal & professional fees',
                                'es' => 'Honorarios legales y profesionales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Legal & professional fees', '-'),
                            'description' => null,
                            'code' => 6015,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Office/general administrative expenses',
                                'es' => 'Gastos de oficina / administrativos generales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Office/general administrative expenses', '-'),
                            'description' => null,
                            'code' => 6016,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other Business Expenses',
                                'es' => 'Otros gastos comerciales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other Business Expenses', '-'),
                            'description' => null,
                            'code' => 6017,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other miscellaneous service cost',
                                'es' => 'Otros costos de servicios diversos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other miscellaneous service cost', '-'),
                            'description' => null,
                            'code' => 6018,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Payroll expenses',
                                'es' => 'Gastos de nómina'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Payroll expenses', '-'),
                            'description' => null,
                            'code' => 6019,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Payroll Tax expenses',
                                'es' => 'Gastos de impuestos sobre la nómina'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Payroll Tax expenses', '-'),
                            'description' => null,
                            'code' => 6020,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Payroll wage expenses',
                                'es' => 'Gastos de salario de nómina'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Payroll wage expenses', '-'),
                            'description' => null,
                            'code' => 6021,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Promotional meals',
                                'es' => 'Comidas promocionales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Promotional meals', '-'),
                            'description' => null,
                            'code' => 6022,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Rent or lease of buildings',
                                'es' => 'Alquiler o arrendamiento de edificios'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Rent or lease of buildings', '-'),
                            'description' => null,
                            'code' => 6023,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Repair & maintenance',
                                'es' => 'Reparación y Mantenimiento'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Repair & maintenance', '-'),
                            'description' => null,
                            'code' => 6024,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Shipping, freight & delivery',
                                'es' => 'Envío, flete y entrega'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Shipping, freight & delivery', '-'),
                            'description' => null,
                            'code' => 6025,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Supplies & materials',
                                'es' => 'Suministros y materiales'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Supplies & materials', '-'),
                            'description' => null,
                            'code' => 6026,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Taxes paid',
                                'es' => 'Impuestos pagados'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Taxes paid', '-'),
                            'description' => null,
                            'code' => 6027,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Travel',
                                'es' => 'Viajar'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Travel', '-'),
                            'description' => null,
                            'code' => 6028,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Travel meals',
                                'es' => 'Comidas de viaje'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Travel meals', '-'),
                            'description' => null,
                            'code' => 6029,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Unapplied Cash Bill Payment Expense',
                                'es' => 'Gastos de pago de facturas en efectivo no aplicados'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Unapplied Cash Bill Payment Expense', '-'),
                            'description' => null,
                            'code' => 6030,
                            'can_be_parent' => false,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Utilities',
                                'es' => 'Utilidades'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Utilities', '-'),
                            'description' => null,
                            'code' => 6031,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ],
                [
                    'name' =>  [
                        'en' => 'Other Expense',
                        'es' => 'Otro gasto'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Other Expense', '-'),
                    'description' => null,
                    'code' => 9000,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Amortization',
                                'es' => 'Amortización'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Amortization', '-'),
                            'description' => null,
                            'code' => 9001,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Depreciation',
                                'es' => 'Depreciación'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Depreciation', '-'),
                            'description' => null,
                            'code' => 9002,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Exchange Gain or Loss',
                                'es' => 'Ganancia o pérdida de cambio'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Exchange Gain or Loss', '-'),
                            'description' => null,
                            'code' => 9003,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Gas, Fuels and lubricants',
                                'es' => 'Gas, combustibles y lubricantes'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Gas, Fuels and lubricants', '-'),
                            'description' => null,
                            'code' => 9004,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Home Office',
                                'es' => 'Oficina en casa'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Home Office', '-'),
                            'description' => null,
                            'code' => 9005,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Homeowner Rental Insurance',
                                'es' => 'Seguro de alquiler para propietarios'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Homeowner Rental Insurance', '-'),
                            'description' => null,
                            'code' => 9006,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Mortgage Interest Home Office',
                                'es' => 'Oficina en casa de interés hipotecario'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Mortgage Interest Home Office', '-'),
                            'description' => null,
                            'code' => 9007,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other Home Office Expenses',
                                'es' => 'Otros gastos de la oficina en casa'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other Home Office Expenses', '-'),
                            'description' => null,
                            'code' => 9008,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other miscellaneous expense',
                                'es' => 'Otros gastos diversos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other miscellaneous expense', '-'),
                            'description' => null,
                            'code' => 9009,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other vehicle expense',
                                'es' => 'Otro gasto de vehículo'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other vehicle expense', '-'),
                            'description' => null,
                            'code' => 9010,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Parking and tools',
                                'es' => 'Aparcamiento y herramientas'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Parking and tools', '-'),
                            'description' => null,
                            'code' => 9011,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Penalties & settlements',
                                'es' => 'Sanciones y acuerdos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Penalties & settlements', '-'),
                            'description' => null,
                            'code' => 9012,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Property Tax Home Office',
                                'es' => 'Oficina en casa de impuestos a la propiedad'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Property Tax Home Office', '-'),
                            'description' => null,
                            'code' => 9013,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Rent and Lease Home Office',
                                'es' => 'Alquiler y arrendamiento de oficina en casa'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Rent and Lease Home Office', '-'),
                            'description' => null,
                            'code' => 9014,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Repair and Maintenance Home Office',
                                'es' => 'Oficina en casa de reparación y mantenimiento'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Repair and Maintenance Home Office', '-'),
                            'description' => null,
                            'code' => 9015,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Utilities Home Office',
                                'es' => 'Utilidades Oficina en casa'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Utilities Home Office', '-'),
                            'description' => null,
                            'code' => 9016,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles',
                                'es' => 'Vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles', '-'),
                            'description' => null,
                            'code' => 9017,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles Insurance',
                                'es' => 'Seguro de vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles Insurance', '-'),
                            'description' => null,
                            'code' => 9018,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles Lease',
                                'es' => 'Arrendamiento de vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles Lease', '-'),
                            'description' => null,
                            'code' => 9019,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles Loan',
                                'es' => 'Préstamo de vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles Loan', '-'),
                            'description' => null,
                            'code' => 9020,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles Loan Interest',
                                'es' => 'Intereses del préstamo de vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles Loan Interest', '-'),
                            'description' => null,
                            'code' => 9021,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles Registration',
                                'es' => 'Registro de vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles Registration', '-'),
                            'description' => null,
                            'code' => 9022,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Vehicles Repair',
                                'es' => 'Reparación de vehículos'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Vehicles Repair', '-'),
                            'description' => null,
                            'code' => 9023,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Wash and Road Services',
                                'es' => 'Servicios de lavado y carreteras'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Wash and Road Services', '-'),
                            'description' => null,
                            'code' => 9024,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ]
            ],
        ]);
    
        // add new charts of accounts for tax, and inventory adjustments.
        $currentLab = AccountingHead::where('code', 2505)->first();
        $otherCurrentAs = AccountingHead::where('code', 1200)->first();
        $cogs = AccountingHead::where('code', 5006)->first();

        $currentLabChildren = AccountingHead::create([
            'name' =>  [
                'en' => 'Taxes to pay',
                'es' => 'Impuestos por pagar'
            ],
            'incr_nature' => 'credit',
            'decr_nature' => 'debit',
            'slug' => strToSlug('Taxes to pay', '-'),
            'description' => null,
            'code' => 2601,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'children' => [
                [
                    'name' =>  [
                        'en' => 'Sales tax payable',
                        'es' => 'impuesto sobre las ventas a pagar'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Sales tax payable', '-'),
                    'description' => null,
                    'code' => 2701,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],[
                    'name' =>  [
                        'en' => 'ITBIS payable',
                        'es' => 'ITBIS por pagar'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('ITBIS payable', '-'),
                    'description' => null,
                    'code' => 2702,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],[
                    'name' =>  [
                        'en' => 'ISC payable',
                        'es' => 'ISC por pagar'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('ISC payable', '-'),
                    'description' => null,
                    'code' => 2703,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],[
                    'name' =>  [
                        'en' => 'Tips payable',
                        'es' => 'Propinas por pagar'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Tips payable', '-'),
                    'description' => null,
                    'code' => 2704,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],[
                    'name' =>  [
                        'en' => 'Other type of tax payable',
                        'es' => 'Otro tipo de impuesto por pagar'
                    ],
                    'incr_nature' => 'credit',
                    'decr_nature' => 'debit',
                    'slug' => strToSlug('Other type of tax payable', '-'),
                    'description' => null,
                    'code' => 2705,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            ]
        ]);

        $currentLab->appendNode($currentLabChildren);

        $otherCurrentAsChildren = AccountingHead::create([
            'name' =>  [
                'en' => 'Current tax assets',
                'es' => 'Activos por impuestos corrientes'
            ],
            'incr_nature' => 'debit',
            'decr_nature' => 'credit',
            'slug' => strToSlug('Current tax assets', '-'),
            'description' => null,
            'code' => 1216,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'children' => [
                [
                    'name' =>  [
                        'en' => 'Taxes in favor',
                        'es' => 'Retenciones por pagar'
                    ],
                    'incr_nature' => 'debit',
                    'decr_nature' => 'credit',
                    'slug' => strToSlug('Taxes in favor', '-'),
                    'description' => null,
                    'code' => 1300,
                    'visibility' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'children' => [
                        [
                            'name' =>  [
                                'en' => 'Sales tax input',
                                'es' => 'Entrada de impuesto sobre las ventas'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Sales tax input', '-'),
                            'description' => null,
                            'code' => 1301,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'ITBIS in favor',
                                'es' => 'Retención de ITBIS por pagar'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('ITBIS in favor', '-'),
                            'description' => null,
                            'code' => 1302,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'ISC a favor',
                                'es' => 'Retención de ISR por pagar'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('ISC a favor', '-'),
                            'description' => null,
                            'code' => 1303,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],[
                            'name' =>  [
                                'en' => 'Other type of tax in favor',
                                'es' => 'Otro tipo de retención por pagar'
                            ],
                            'incr_nature' => 'debit',
                            'decr_nature' => 'credit',
                            'slug' => strToSlug('Other type of tax in favor', '-'),
                            'description' => null,
                            'code' => 1304,
                            'visibility' => true,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]
                ]
            ]
        ]);

        $otherCurrentAs->appendNode($otherCurrentAsChildren);

        $cogsChildren = AccountingHead::create([
            'name' =>  [
                'en' => 'Inventory adjustments',
                'es' => 'Ajustes de inventario'
            ],
            'incr_nature' => 'debit',
            'decr_nature' => 'credit',
            'slug' => strToSlug('Inventory adjustments', '-'),
            'description' => null,
            'code' => 5011,
            'visibility' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $cogs->appendNode($cogsChildren);
    }
}
