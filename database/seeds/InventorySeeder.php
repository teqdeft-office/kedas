<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventories')->insert([
            [
                'user_id' => '1',
                'name' => 'Rabin Bike Shaft',
                'reference' => 'RBS01',
                'description' => 'Rabin Bike Shaft description',
                'item_category_id' => '1',
                'tax_id' => null,
                'type_of_item' => 'simple',
                'account' => 'sales',
                'sale_price' => '12.00',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'name' => 'Rabin Car Shaft',
                'reference' => 'RCS01',
                'description' => 'Rabin Car Shaft description',
                'item_category_id' => '2',
                'tax_id' => '2',
                'type_of_item' => 'simple',
                'account' => 'sales',
                'sale_price' => '15.00',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => '1',
                'name' => 'Bike Tool Kit',
                'reference' => 'BTK01',
                'description' => 'Bike Tool Kit description',
                'item_category_id' => null,
                'tax_id' => '1',
                'type_of_item' => 'kit',
                'account' => 'sales',
                'sale_price' => '100.00',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
