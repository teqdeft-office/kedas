<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccountsRelatedFieldsToPayrollTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('with_holdings', function (Blueprint $table) {
            DB::statement("ALTER TABLE `with_holdings` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
            DB::statement("ALTER TABLE `with_holdings` CHANGE `type` `type` ENUM('AFP','SDSS','ISR','SS','HI','Others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");

            $table->foreignId('income_account')->after('type');
            $table->foreignId('type_account')->after('type');

            $table->foreign('income_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('benefits', function (Blueprint $table) {

            DB::statement("ALTER TABLE `benefits` CHANGE `expenses` `expenses` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
            DB::statement("ALTER TABLE `benefits` CHANGE `type` `type` ENUM('AFP','SDSS','ISR','SS','HI','Others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");

            $table->foreignId('expense_account')->after('type');
            $table->foreignId('type_account')->after('type');

            $table->foreign('expense_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('extra_hours', function (Blueprint $table) {

            DB::statement("ALTER TABLE `extra_hours` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
            DB::statement("ALTER TABLE `extra_hours` CHANGE `type` `type` ENUM('AFP','SDSS','ISR','SS','HI','Others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");

            $table->foreignId('income_account')->after('type');
            $table->foreignId('type_account')->after('type');

            $table->foreign('income_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('discounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `discounts` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
            DB::statement("ALTER TABLE `discounts` CHANGE `type` `type` ENUM('cash_advance','damages','uniform','cashier_missing','misc','others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");

            $table->foreignId('income_account')->after('type');
            $table->foreignId('type_account')->after('type');

            $table->foreign('income_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('with_holdings', function (Blueprint $table) {
            DB::statement("ALTER TABLE `with_holdings` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
            DB::statement("ALTER TABLE `with_holdings` CHANGE `type` `type` ENUM('AFP','SDSS','ISR','SS','HI','Others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");

            $table->dropForeign(['income_account']);
            $table->dropForeign(['type_account']);

            $table->dropColumn('income_account');
            $table->dropColumn('type_account');
        });

        Schema::table('benefits', function (Blueprint $table) {
            DB::statement("ALTER TABLE `benefits` CHANGE `expenses` `expenses` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
            DB::statement("ALTER TABLE `benefits` CHANGE `type` `type` ENUM('AFP','SDSS','ISR','SS','HI','Others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
            $table->dropForeign(['expense_account']);
            $table->dropForeign(['type_account']);

            $table->dropColumn('expense_account');
            $table->dropColumn('type_account');
        });

        Schema::table('extra_hours', function (Blueprint $table) {

            DB::statement("ALTER TABLE `extra_hours` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
            DB::statement("ALTER TABLE `extra_hours` CHANGE `type` `type` ENUM('AFP','SDSS','ISR','SS','HI','Others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");

            $table->dropForeign(['income_account']);
            $table->dropForeign(['type_account']);

            $table->dropColumn('income_account');
            $table->dropColumn('type_account');
        });

        Schema::table('discounts', function (Blueprint $table) {

            DB::statement("ALTER TABLE `discounts` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
            DB::statement("ALTER TABLE `discounts` CHANGE `type` `type` ENUM('cash_advance','damages','uniform','cashier_missing','misc','others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");

            $table->dropForeign(['income_account']);
            $table->dropForeign(['type_account']);

            $table->dropColumn('income_account');
            $table->dropColumn('type_account');
        });
    }
}
