<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnotherNullableForOptionalFieldsToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {

        });
        DB::statement("ALTER TABLE `employees` CHANGE `period` `period` ENUM('weekly','monthly','daily','biweekly','yearly','custom') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;");
        DB::statement("ALTER TABLE `employees` CHANGE `contract_type` `contract_type` ENUM('permanent','daily') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;");
        DB::statement("ALTER TABLE `employees` CHANGE `salary_type` `salary_type` ENUM('hourly','fixed') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;");
        DB::statement("ALTER TABLE `employees` CHANGE `type_of_account` `type_of_account` ENUM('checking','saving') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
         
        });
       DB::statement("ALTER TABLE `employees` CHANGE `period` `period` ENUM('weekly','monthly','daily','biweekly','yearly','custom') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        DB::statement("ALTER TABLE `employees` CHANGE `contract_type` `contract_type` ENUM('permanent','daily') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        DB::statement("ALTER TABLE `employees` CHANGE `salary_type` `salary_type` ENUM('hourly','fixed') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        DB::statement("ALTER TABLE `employees` CHANGE `type_of_account` `type_of_account` ENUM('checking','saving') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }
}
