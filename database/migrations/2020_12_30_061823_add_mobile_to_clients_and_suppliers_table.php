<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMobileToClientsAndSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->string('mobile')->nullable()->after('phone');
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->string('mobile')->nullable()->after('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->dropColumn('mobile');
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('mobile');
        });
    }
}
