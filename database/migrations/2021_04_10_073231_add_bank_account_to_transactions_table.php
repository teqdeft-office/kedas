<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBankAccountToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->foreignId('bank_account')->after('amount')->nullable();
            // Why because change in enum is not working by migration.
            DB::statement("ALTER TABLE `transactions` CHANGE `account` `account` ENUM('bank','business_credit_card','petty_cash') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;");


            $table->foreign('bank_account')->references('id')->on('user_accounting_heads')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['bank_account']);
            $table->dropColumn('bank_account');
            // Why because change in enum is not working by migration.
            DB::statement("ALTER TABLE `transactions` CHANGE `account` `account` ENUM('bank','business_credit_card','petty_cash') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;");
        });
    }
}
