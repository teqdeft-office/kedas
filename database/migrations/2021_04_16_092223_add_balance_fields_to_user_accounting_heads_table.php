<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBalanceFieldsToUserAccountingHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->decimal('opening_balance', 15, 2)->nullable()->after('can_be_parent');
            $table->decimal('total_balance', 15, 2)->nullable()->after('opening_balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->dropColumn('opening_balance');
            $table->dropColumn('total_balance');
        });
    }
}
