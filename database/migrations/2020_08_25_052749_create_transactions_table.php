<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id');
            $table->integer('contact_id')->nullable();
            $table->string('contact_type')->nullable();

            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->unsignedSmallInteger('frequency')->nullable();

            $table->enum('type', ['in', 'out', 'recurring']);
            $table->decimal('amount', 15, 2);
            $table->enum('account', ['bank', 'business_credit_card', 'petty_cash']);
            $table->enum('method', ['cash', 'deposit', 'transfer', 'check', 'credit_card', 'debit_card'])->nullable();
            $table->text('annotation')->nullable();
            $table->text('observation')->nullable();
            $table->unsignedBigInteger('receipt_number')->nullable();
            $table->unsignedBigInteger('voucher_number')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
