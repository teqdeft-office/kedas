<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->char('abv', 2)->default('')->comment('ISO 3661-1 alpha-2');
            $table->char('abv3', 3)->default(null)->nullable()->comment('ISO 3661-1 alpha-3');
            $table->char('abv3_alt', 3)->default(null)->nullable();
            $table->char('code', 3)->default(null)->nullable()->comment('ISO 3661-1 numeric');
            $table->string('slug', 100)->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
