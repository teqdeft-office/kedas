<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryAdjustmentsFieldsToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->decimal('unit_cost', 15, 2)->nullable()->after('sale_price');
            $table->foreignId('inventory_adjustment_id')->nullable()->after('estimate_id');
            $table->enum('inventory_adjustment_type', ['increase', 'decrease'])->nullable()->after('unit_cost');

            $table->foreign('inventory_adjustment_id')->references('id')->on('inventory_adjustments')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropForeign(['inventory_adjustment_id']);

            $table->dropColumn('unit_cost');
            $table->dropColumn('inventory_adjustment_id');
            $table->dropColumn('inventory_adjustment_type');
        });
    }
}
