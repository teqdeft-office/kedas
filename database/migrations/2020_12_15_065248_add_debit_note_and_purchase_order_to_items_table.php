<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDebitNoteAndPurchaseOrderToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->foreignId('debit_note_id')->after('estimate_id')->nullable();
            $table->foreignId('purchase_order_id')->after('debit_note_id')->nullable();

            $table->foreign('debit_note_id')->references('id')->on('debit_notes')->onDelete('cascade');
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders')->onDelete('cascade');
            $table->foreign('credit_note_id')->references('id')->on('credit_notes')->onDelete('cascade');
            $table->foreign('estimate_id')->references('id')->on('estimates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropForeign(['credit_note_id']);
            $table->dropForeign(['estimate_id']);
            $table->dropForeign(['debit_note_id']);
            $table->dropForeign(['purchase_order_id']);
            
            $table->dropColumn('debit_note_id');
            $table->dropColumn('purchase_order_id');
        });
    }
}
