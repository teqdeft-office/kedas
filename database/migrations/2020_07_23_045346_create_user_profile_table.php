<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id');
            $table->string('name');

            $table->enum('user_type', ['business_owner', 'independent', 'student', 'accountant', 'other']);

            $table->string('role');
            $table->string('image')->nullable();
            $table->string('signature')->nullable();
            $table->enum('language', ['en', 'es'])->default('en');

            $table->string('timezone')->nullable();           
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            
            $table->foreignId('sector_id');
            $table->foreignId('currency_id');

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade');
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
}
