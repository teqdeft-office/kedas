<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameStoreUsersWithPrimaryColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('store_users', 'store_user');
        Schema::dropIfExists('store_users');

        Schema::table('store_user', function (Blueprint $table) {
            $table->tinyInteger('primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_user', function (Blueprint $table) {
            $table->dropColumn('primary');
        });

        Schema::rename('store_user', 'store_users');
    }
}
