<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldsForUserAndCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile', function (Blueprint $table) {
            $table->dropColumn('language');
            $table->dropColumn('timezone');
            $table->dropColumn('user_type');
        });

        Schema::rename('user_profile', 'companies');

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('company_id')->nullable()->after('remember_token');
            $table->string('image')->nullable()->after('company_id');
            $table->enum('user_type', ['business_owner', 'independent', 'student', 'accountant', 'other'])->nullable()->after('image');
            $table->enum('user_role', ['super_admin', 'admin', 'company_user', 'app_user'])->default('admin')->after('user_type');
            $table->enum('language', ['en', 'es'])->default('en')->after('user_role');
            $table->string('timezone')->nullable()->after('language');

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->string('tax_id')->nullable()->after('role');
            $table->renameColumn('image', 'logo');
            $table->string('support_email')->nullable()->after('signature');
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->renameColumn('province', 'state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->enum('language', ['en', 'es'])->default('en')->after('signature');
            $table->string('timezone')->nullable()->after('language');
            $table->enum('user_type', ['business_owner', 'independent', 'student', 'accountant', 'other'])->nullable()->after('name');
        });

        Schema::rename('companies', 'user_profile');

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropColumn('company_id');
            $table->dropColumn('image');
            $table->dropColumn('user_type');
            $table->dropColumn('user_role');
            $table->dropColumn('language');
            $table->dropColumn('timezone');
            $table->dropSoftDeletes();
        });

        Schema::table('user_profile', function (Blueprint $table) {
            $table->dropColumn('tax_id');
            $table->dropColumn('support_email');
            $table->renameColumn('logo', 'image');
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->renameColumn('state', 'province');
        });
    }
}
