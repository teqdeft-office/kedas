<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNcfFieldsToCreditNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_notes', function (Blueprint $table) {
            $table->foreignId('ncf_id')->nullable()->after('company_id');
            $table->string('ncf_number', 11)->nullable()->after('ncf_id');

            $table->foreign('ncf_id')->references('id')->on('ncfs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_notes', function (Blueprint $table) {
            $table->dropForeign(['ncf_id']);
            $table->dropColumn('ncf_id');
            $table->dropColumn('ncf_number');
        });
    }
}
