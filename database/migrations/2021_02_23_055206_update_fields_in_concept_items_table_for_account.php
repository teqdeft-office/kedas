<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldsInConceptItemsTableForAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concept_items', function (Blueprint $table) {
            $table->unsignedInteger('concept_id')->nullable()->change();
            $table->foreignId('user_accounting_head_id')->nullable()->after('concept_id');

            $table->foreign('user_accounting_head_id')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->date('entry_date')->after('user_accounting_head_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concept_items', function (Blueprint $table) {
            $table->unsignedInteger('concept_id')->nullable(false)->change();
            $table->dropForeign(['user_accounting_head_id']);
            
            $table->dropColumn('user_accounting_head_id');
        });
        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->dropColumn('entry_date');
        });
    }
}
