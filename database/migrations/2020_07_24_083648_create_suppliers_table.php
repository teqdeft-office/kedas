<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('identification_type_id')->nullable();

            $table->string('tax_id')->nullable();
            $table->string('name');
            $table->string('email');
            $table->foreignId('sector_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['user_id', 'email']);
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade');
            $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
