<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSchemaForTranslatablePackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concepts', function (Blueprint $table) {
            $table->text('title')->change();
        });

        Schema::table('identification_types', function (Blueprint $table) {
            $table->text('name')->change();
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->text('name')->change();
        });

        Schema::table('currencies', function (Blueprint $table) {
            $table->text('name')->change();
        });

        Schema::table('sectors', function (Blueprint $table) {
            $table->text('name')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concepts', function (Blueprint $table) {
            $table->string('title')->change();
        });

        Schema::table('identification_types', function (Blueprint $table) {
            $table->string('name')->change();
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->string('name')->change();
        });

        Schema::table('currencies', function (Blueprint $table) {
            $table->string('name')->change();
        });

        Schema::table('sectors', function (Blueprint $table) {
            $table->string('name')->change();
        });
    }
}
