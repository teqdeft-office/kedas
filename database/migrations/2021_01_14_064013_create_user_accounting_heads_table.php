<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAccountingHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_accounting_heads', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->text('name');
            $table->string('slug');
            $table->mediumText('description')->nullable();

            $table->foreignId('user_id');
            $table->foreignId('company_id');
            $table->unsignedInteger('accounting_head_id')->nullable()->comment('null => Someone create their own head. id => Inherit from the main head.');

            $table->string('code')->nullable();
            $table->enum('nature', ['debit', 'credit']);
            $table->nestedSet();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('accounting_head_id')->references('id')->on('accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_accounting_heads');
    }
}
