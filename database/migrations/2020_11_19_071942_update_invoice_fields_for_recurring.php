<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInvoiceFieldsForRecurring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->date('expiration_date')->nullable()->change();
            $table->foreignId('recurring_invoice_id')->nullable()->after('frequency');
            $table->foreign('recurring_invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->date('expiration_date')->nullable(false)->change();
            $table->dropForeign(['recurring_invoice_id']);
            $table->dropColumn('recurring_invoice_id');
        });
    }
}
