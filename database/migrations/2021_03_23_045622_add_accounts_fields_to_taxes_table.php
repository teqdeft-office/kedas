<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccountsFieldsToTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->foreignId('sales_account')->nullable()->after('description');
            $table->foreignId('purchases_account')->nullable()->after('sales_account');

            $table->foreign('sales_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('purchases_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->dropForeign(['purchases_account']);
            $table->dropForeign(['sales_account']);
            
            $table->dropColumn('purchases_account');
            $table->dropColumn('sales_account');
        });
    }
}
