<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherNaturesToAccountingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_heads', function (Blueprint $table) {
            $table->dropColumn('nature');
            $table->enum('incr_nature', ['debit', 'credit'])->after('code');
            $table->enum('decr_nature', ['debit', 'credit'])->after('incr_nature');
        });

        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->dropColumn('nature');
            $table->enum('incr_nature', ['debit', 'credit'])->after('code');
            $table->enum('decr_nature', ['debit', 'credit'])->after('incr_nature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_heads', function (Blueprint $table) {
            $table->dropColumn('incr_nature');
            $table->dropColumn('decr_nature');
            $table->enum('nature', ['debit', 'credit'])->after('code');
        });

        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->dropColumn('incr_nature');
            $table->dropColumn('decr_nature');
            $table->enum('nature', ['debit', 'credit'])->after('code');
        });
    }
}
