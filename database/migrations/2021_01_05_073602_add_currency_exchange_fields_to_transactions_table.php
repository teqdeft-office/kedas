<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCurrencyExchangeFieldsToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->foreignId('exchange_currency_id')->nullable()->after('voucher_number');
            $table->decimal('exchange_currency_rate', 15, 2)->nullable()->after('exchange_currency_id');
            $table->foreign('exchange_currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('credit_notes', function (Blueprint $table) {
            $table->foreignId('exchange_currency_id')->nullable()->after('notes');
            $table->decimal('exchange_currency_rate', 15, 2)->nullable()->after('exchange_currency_id');
            $table->foreign('exchange_currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('debit_notes', function (Blueprint $table) {
            $table->foreignId('exchange_currency_id')->nullable()->after('internal_number');
            $table->decimal('exchange_currency_rate', 15, 2)->nullable()->after('exchange_currency_id');
            $table->foreign('exchange_currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('estimates', function (Blueprint $table) {
            $table->foreignId('exchange_currency_id')->nullable()->after('notes');
            $table->decimal('exchange_currency_rate', 15, 2)->nullable()->after('exchange_currency_id');
            $table->foreign('exchange_currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->foreignId('exchange_currency_id')->nullable()->after('notes');
            $table->decimal('exchange_currency_rate', 15, 2)->nullable()->after('exchange_currency_id');
            $table->foreign('exchange_currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['exchange_currency_id']);
            
            $table->dropColumn('exchange_currency_id');
            $table->dropColumn('exchange_currency_rate');
        });

        Schema::table('credit_notes', function (Blueprint $table) {
            $table->dropForeign(['exchange_currency_id']);
            
            $table->dropColumn('exchange_currency_id');
            $table->dropColumn('exchange_currency_rate');
        });

        Schema::table('debit_notes', function (Blueprint $table) {
            $table->dropForeign(['exchange_currency_id']);
            
            $table->dropColumn('exchange_currency_id');
            $table->dropColumn('exchange_currency_rate');
        });

        Schema::table('estimates', function (Blueprint $table) {
            $table->dropForeign(['exchange_currency_id']);
            
            $table->dropColumn('exchange_currency_id');
            $table->dropColumn('exchange_currency_rate');
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->dropForeign(['exchange_currency_id']);
            
            $table->dropColumn('exchange_currency_id');
            $table->dropColumn('exchange_currency_rate');
        });
    }
}
