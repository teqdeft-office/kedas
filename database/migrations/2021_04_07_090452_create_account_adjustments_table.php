<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_adjustments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('company_id');
            $table->date('adjustment_date')->nullable();
            $table->text('observations')->nullable();
            $table->text('reference')->nullable();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->integer('contact_id')->after('id')->nullable();
            $table->string('contact_type')->after('contact_id')->nullable();

            $table->foreignId('account_adjustment_id')->after('remission_id')->nullable();
            $table->foreign('account_adjustment_id')->references('id')->on('account_adjustments')->onUpdate('cascade')->onDelete('cascade');
            
            $table->text('description')->after('account_adjustment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->dropForeign(['account_adjustment_id']);
            $table->dropColumn('account_adjustment_id');
            
            $table->dropColumn('contact_id');
            $table->dropColumn('contact_type');
            $table->dropColumn('description');
        });

        Schema::dropIfExists('account_adjustments');
    }
}
