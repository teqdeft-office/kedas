<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('company_id');
            
            $table->string('name');
            $table->string('email');
            $table->string('type_of_id');
            $table->string('id_number');
            $table->date('hiring_date');
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->decimal('salary', 15, 2)->nullable();
            $table->decimal('insurance_cost', 15, 2)->nullable();
            $table->float('insurance_discount', 5, 2)->nullable();
            $table->date('final_date');
            $table->enum('status', ['working', 'not_working']);
            $table->decimal('afp', 15, 2)->nullable();
            $table->date('birth_date');
            $table->string('username');
            $table->string('password');
            $table->foreignId('department_id');
            $table->foreignId('insurance_brand_id')->nullable();
            $table->foreignId('designation_id');
            $table->foreignId('supervisor_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            // $table->unique(["email", "company_id"]);
            // $table->unique(["username", "company_id"]);
            // $table->unique(["id_number", "company_id"]);
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('insurance_brand_id')->references('id')->on('insurance_brands')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('designation_id')->references('id')->on('designations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('supervisor_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
