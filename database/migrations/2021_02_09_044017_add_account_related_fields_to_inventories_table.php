<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccountRelatedFieldsToInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->enum('type', ['inventory', 'services', 'non-inventory'])->default('services')->after('name');
            $table->string('sku')->nullable()->after('barcode');

            DB::statement("ALTER TABLE `inventories` CHANGE `type_of_item` `type_of_item` ENUM('simple', 'kit', 'item_with_variants') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'simple';");

            $table->date('init_quantity_date')->nullable()->after('quantity');
            $table->foreignId('asset_account')->nullable()->after('unit_cost');
            $table->foreignId('income_account')->nullable()->after('asset_account');
            $table->foreignId('expense_account')->nullable()->after('income_account');

            $table->foreign('asset_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('income_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('expense_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('sku');
            $table->dropColumn('init_quantity_date');

            DB::statement("ALTER TABLE `inventories` CHANGE `type_of_item` `type_of_item` ENUM('simple', 'kit', 'item_with_variants') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'simple';");
            
            $table->dropForeign(['asset_account']);
            $table->dropForeign(['income_account']);
            $table->dropForeign(['expense_account']);
            
            $table->dropColumn('asset_account');
            $table->dropColumn('income_account');
            $table->dropColumn('expense_account');
        });
    }
}
