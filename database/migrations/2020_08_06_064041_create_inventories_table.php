<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('name');
            $table->string('reference')->nullable();
            $table->text('description')->nullable();
            $table->foreignId('item_category_id')->nullable();
            $table->foreignId('tax_id')->nullable();
            $table->enum('type_of_item', ['simple', 'kit', 'item_with_variants']);
            $table->enum('account', ['ordinary_activities_income', 'sales', 'returns_on_sales', 'other_income', 'financial_income'])->nullable();
            $table->string('image')->nullable();

            $table->decimal('sale_price', 15, 2);

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade');
            $table->foreign('item_category_id')->references('id')->on('item_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
