<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithHoldingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('with_holdings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('company_id');
            $table->foreignId('employee_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('is_recurring')->default('0');
            $table->boolean('is_on_time')->default('0');
            $table->enum('type', ['AFP', 'SDSS', 'ISR', 'SS', 'HI', 'Others']);
            $table->enum('frequency_type', ['monthly', 'biweekly', 'weekly', 'daily']);
            $table->unsignedSmallInteger('frequency')->nullable();
            $table->text('observation')->nullable();
            $table->decimal('amount', 15, 2);
            $table->enum('incomes', ['accounts_receivable']);
    
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('with_holdings');
    }
}
