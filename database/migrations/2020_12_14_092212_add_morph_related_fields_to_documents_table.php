<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMorphRelatedFieldsToDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->integer('documentable_id')->after('id');
            $table->dropColumn('type');
            $table->string('documentable_type')->after('documentable_id');
            $table->renameColumn('path', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('documentable_id');
            $table->enum('type', ['payment_received']);
            $table->dropColumn('documentable_type'); 
            $table->renameColumn('name', 'path'); 
        });
    }
}
