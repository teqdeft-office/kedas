<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFixedAssetsToAccountingEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->foreignId('fixed_asset_id')->nullable()->after('remission_id');
            $table->foreign('fixed_asset_id')->references('id')->on('fixed_assets')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('fixed_assets', function (Blueprint $table) {
            $table->boolean('depr_should_cal')->default(true)->after('expense_account');
            DB::statement("ALTER TABLE `fixed_assets` CHANGE `cal_type` `cal_type` ENUM('straight_line','sum_of_digits','units_produced') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;");
        });

        Schema::table('accounting_heads', function (Blueprint $table) {
            $table->boolean('has_exception')->default(false)->after('decr_nature');
        });

        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->boolean('has_exception')->default(false)->after('decr_nature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->dropForeign(['fixed_asset_id']);
            
            $table->dropColumn('fixed_asset_id');
        });

        Schema::table('fixed_assets', function (Blueprint $table) {
            $table->dropColumn('depr_should_cal');
            DB::statement("ALTER TABLE `fixed_assets` CHANGE `cal_type` `cal_type` ENUM('straight_line','sum_of_digits','units_produced') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        });

        Schema::table('accounting_heads', function (Blueprint $table) {
            $table->dropColumn('has_exception');
        });

        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->dropColumn('has_exception');
        });
    }
}
