<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class AddNestedSetToConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concepts', function (Blueprint $table) {
            $table->unsignedInteger('_lft');
            $table->unsignedInteger('_rgt');
            $table->text('description')->after('title')->nullable();
        });
        Schema::table('items', function (Blueprint $table) {
            $table->renameColumn('sale_price', 'price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concepts', function (Blueprint $table) {
            $table->dropColumn('_lft');
            $table->dropColumn('_rgt');
            $table->dropColumn('description');
            // NestedSet::dropColumns($table);
        });
        Schema::table('items', function (Blueprint $table) {
            $table->renameColumn('price', 'sale_price');
        });
    }
}
