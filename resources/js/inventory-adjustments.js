/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');
const _ = require('lodash');

//An EventHub to share events between components
Vue.prototype.$hub = new Vue();
//Define a function to use existing laravel language
Vue.prototype.trans = (string, args) => {
    let value = _.get(window.i18n, string);

    let defaultVal = _.last(_.split(string, '.'));
    if (value) {
        _.eachRight(args, (paramVal, paramKey) => {
            value = _.replace(value, `:${paramKey}`, paramVal);
        });
    } else {
        value = defaultVal;
    }
    return value;
};
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('adjustments-component', require('./components/inventoryAdjustments/adjustments.vue').default);
Vue.component('update-adjustments-component', require('./components/inventoryAdjustments/update-adjustments.vue').default);
Vue.component('form-error', require('./components/commonComponents/formError.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#inventory-adjustments',
});
