/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./jquery-3.3.1.min');
require('./hammer.min');
require('./popper.min');
require('./bootstrap');

window.Vue = require('vue');
window.printThis = require('print-this');
const _ = require('lodash');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Popover
import Popover from 'vue-js-popover';
Vue.use(Popover);

//An EventHub to share events between components
Vue.prototype.$hub = new Vue();

//Shortcuts
Vue.use(require('vue-shortkey'), { prevent: ['input', 'textarea'] });

//Vue Toasted
import Toasted from 'vue-toasted';
Vue.use(Toasted);

//For success messages
let success = {
    theme: "toasted-primary",
    position: "top-right",
    duration: 4000,
};

//Register the toast with the custom message
Vue.toasted.register('success',
    (payload) => {
        // if there is a message show it with the message
        return payload.message;
    },
    success

);

//Error messages example
let errors = {
    type: 'error',
    theme: "toasted-primary",
    position: "top-right",
    duration: 4000
};

//Register the toast with the custom message
Vue.toasted.register('error',
    (payload) => {
        // if there is no message passed show default message
        if (!payload.message) {
            return "Something Went Wrong.."
        }

        // if there is a message show it with the message
        return payload.message;

    },
    errors

);

// for warning message example
let offlineToast = {
    theme: "toasted-primary",
    position: "top-right",
    className: "offline-toast-padding",
    action: {
        text: 'X',
        onClick: (e, toastObject) => {
            toastObject.goAway(0);
        }
    },
};

// register the toast with the custom message
Vue.toasted.register('offlineToast',
    (payload) => {
        // if there is a message show it with the message
        return payload.message;
    },
    offlineToast
);

// for warning 
let warningToast = {
    theme: "",
    position: "top-right",
    className: "warning-message",
    duration: 4000
};
Vue.toasted.register('warningToast',
    (payload) => {
        return payload.message;
    },
    warningToast
);

window.$cookies = require('vue-cookies');

//Define a function to use existing laravel language
Vue.prototype.trans = (string, args) => {
    let value = _.get(window.i18n, string);

    let defaultVal = _.last(_.split(string, '.'));
    if (value) {
        _.eachRight(args, (paramVal, paramKey) => {
            value = _.replace(value, `:${paramKey}`, paramVal);
        });
    } else {
        value = defaultVal;
    }
    return value;
};

Vue.prototype.appUrl = window.appConfig.appUrl;
Vue.prototype.app_name = window.appConfig.app_name;
Vue.prototype.dateFormat = window.appConfig.dateFormat;
Vue.prototype.publicPath = window.appConfig.publicPath;
Vue.prototype.offline = window.appConfig.offline;
Vue.prototype.appVersion = window.appConfig.appVersion;
Vue.prototype.currencySymbol = window.appConfig.currencySymbol;
Vue.prototype.currencyPosition = window.appConfig.currencyFormat;
Vue.prototype.thousandSeparator = window.appConfig.thousandSeparator;
Vue.prototype.decimalSeparator = window.appConfig.decimalSeparator;
Vue.prototype.numDec = window.appConfig.numDec;
Vue.prototype.timeFormat = window.appConfig.timeFormat;
Vue.prototype.timezone = window.appConfig.timezone;

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

//layouts component
Vue.component('nav-bar', require('./components/layouts/Navbar.vue').default);

// sales
Vue.component('sales-or-receives-component', require('./components/salesOrReceives/SalesOrRecivesComponent.vue').default);
Vue.component('cart-component', require('./components/salesOrReceives/cart/cartComponent.vue').default);
Vue.component('payment-input', require('./components/commonComponents/paymentInput.vue').default);
Vue.component('load-more', require('./components/commonComponents/loadMoreButton.vue').default);

//Loaders
Vue.component('pre-loader', require('./components/preLoaders/preLoader.vue').default);
Vue.component('update-loader', require('./components/preLoaders/updateLoader.vue').default);
Vue.component('circle-loader', require('./components/preLoaders/circleLoader.vue').default);
Vue.component('button-loader', require('./components/preLoaders/buttonLoader.vue').default);
Vue.component('roller-loader', require('./components/preLoaders/rollerloader.vue').default);

Vue.component('cart-payment-details', require('./components/salesOrReceives/cartPaymentDetails.vue').default);
Vue.component('invoice', require('./components/salesOrReceives/invoice.vue').default);

//Confirmation-modal
Vue.component('confirmation-modal', require('./components/confirmationDeleteModal/ConfirmationModal.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
