export const productConverter = (products) => {
    return products.map((product) => {            
        return product;
    });

};

export const concatProductArray = (responses) => {
    let products = [];
    for(let response of responses) {
       products.push(productConverter(response.data.products, response.data.variants));
    }
    return products.flat();
};