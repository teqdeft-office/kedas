@extends('layouts.dashboard')
@section('title', trans('labels.supplier'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.supplier_invoice') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.invoice') }},</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('invoices.create', ['type' => $type]) }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new') }} {{ __('labels.supplier_invoice') }}</a>
        </div>
       
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.filter') }}: <input id="supplier-filter" type="text" autocomplete="off" value=""></span>
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="supplier-invoice-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.supplier') }}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    <th>{{ __('labels.expiration_date') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.paid') }}</th>
                                    <th>{{ __('labels.to_pay') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.discount') }} {{ __('labels.amount') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                    <th>{{ __('labels.notes') }}</th>
                                    <th>{{ __('labels.tac') }}</th>
                                    <th>{{ __('labels.phone') }}</th>
                                    <th>{{ __('labels.baseTotal') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->internal_number }}</td>
                                    <td>{{ $invoice->contact->name }}</td>
                                    <td>{{ dateFormat($invoice->start_date) }}</td>
                                    <td>{{ dateFormat($invoice->expiration_date) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['total']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['paidAmount']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['supplierPendingAmount']) }}</td>
                                    <td>{{ $invoice->calculation['status'] }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['taxAmount']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['discountAmount']) }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('invoices.show', $invoice)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('invoices.edit', $invoice)}}">{{ __('labels.edit') }}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                    <td>{{ $invoice->notes }}</td>
                                    <td>{{ $invoice->tac }}</td>
                                    <td>{{ $invoice->contact->phone }}</td>
                                    <td>{{ $invoice->calculation['baseTotal'] }}</td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message">
                                            <p>{{ __('labels.you_have_not_yet_created_an').' '.__('labels.invoice') }}!</p>
                                        </div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('invoices.create', ['type' => $type]) }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.invoice') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                        <th>Contact</th>
                                        <th>Creation date</th>
                                        <th>Expiration date</th>
                                        <th>Total</th>
                                        <th>Paid</th>
                                        <th>Pending</th>
                                        <th>Status</th>
                                        <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection