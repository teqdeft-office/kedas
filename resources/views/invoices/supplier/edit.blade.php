@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.supplier').' '.trans('labels.invoice'))

@section('content')
<input type="hidden" name="resource_type" value="{{ $invoice->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit').' '.__('labels.supplier').' '.__('labels.invoice').' '.$invoice->internal_number }}</h2>
        <div class="default-form add-invoice-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['invoices.update', $invoice], 'id' => 'add-edit-invoices-form', 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <!-- <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center company-data"></div>
                        <div class="col-sm-4">
                            <div class="consumption-box">
                                <div class="consumption-bottom">
                                    <span>No.</span>
                                    <div class="consumption-number">{{ $invoice->internal_number }}</div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.supplier') }} <span>*</span></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', $invoice->contact_custom_id), ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' => 'required', 'style'=> $resource_associated ? 'pointer-events:none;' : ''], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="ncf">{{ __('labels.ncf') }} </label>
                            <input id="ncf_number" class="form-control" name="ncf_number" value="{{old('ncf_number', $invoice->ncf_number)}}" placeholder="{{ __('labels.ncf_number') }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="invoice_start_date" class="form-control" name="invoice_start_date" required="required" value="{{old('invoice_start_date', $invoice->start_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.expiration_date') }} <span>*</span></label>
                            <input id="invoice_expiration_date" name="invoice_expiration_date" class="form-control" required="required" value="{{old('invoice_expiration_date', $invoice->expiration_date)}}" autocomplete="off"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="notes">{{ __('labels.notes') }}</label>
                            <textarea class="form-control" name="notes" id="notes"
                            placeholder="Visible in document printing">{{ old('notes', $invoice->notes) }}</textarea>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="image">
                                        {{ __('labels.documents') }} ({{ __('labels.image') }})
                            </label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                        @if($invoice->currency)
                        <div class="currency_exchange_cnt col-md-6">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    <input type="text" id="exchange-currency" name="exchange_currency" class="form-control"
                                        value="{{old('exchange_currency', $invoice->currency->name.' - '.$invoice->currency->code.' ('.$invoice->currency->symbol.')' )}}"
                                        readonly />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{old('exchange_rate', $invoice->exchange_currency_rate)}}" readonly />
                                   
                                    <label class="total_currency_exchange_inner">1
                                        <span class="exchange_currency_label">{{ $invoice->currency->code }}</span>
                                        = {{ Auth::user()->company->currency->code }}
                                        ({{ Auth::user()->company->currency->symbol }})
                                        <span class="">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span>
                                    </label>
                                </div>

                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="default-table add-invoice-table with-concept-table">
                        <div class="list-table-custom">
                            <table class="table table-striped mb-0" id="concept_items">
                                <thead>
                                    <tr>
                                        <th class="th-concept">{{ __('labels.concept') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(old('concepts', [])))
                                        @foreach (old('concepts', []) as $key => $concept)
                                        @include('app.concept-row', ['row_number' => $key, 'concept_with_products' => true])
                                        @endforeach
                                    @else
                                        @foreach ($invoice->conceptItemsWithProducts as $key => $item)
                                        <tr>
                                            <input type="hidden" name="concepts[{{$key}}][id]" value="{{$item->custom_id ?: 0}}">
                                            <td class="td-concept">
                                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-{{$key}}">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <div class="hs-menu-inner">
                                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                                                            @if($trackedInventories->isNotEmpty())
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.inventory') }}</strong>
                                                            @foreach($trackedInventories as $inventory_account)
                                                                <a class="dropdown-item" data-value="inventory-{{$inventory_account->id}}" data-level="1" href="javascript::void(0)">
                                                                    <span class="account-name">{{$inventory_account->name}}</span>
                                                                    @if($inventory_account->level == 1)
                                                                    <span class="detail-type">{{ $inventory_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                            @endif
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                            @foreach($all_accounts_options as $all_account)
                                                                <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                    <span class="account-name">{{$all_account->custom_name}}</span>
                                                                    @if($all_account->level == 1)
                                                                    <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <input class="d-none" name="concepts[{{$key}}][item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('concepts[$key][item]', $item->inventory ? $item->inventory->custom_id : $item->userAccountingHead->custom_id) }}" />
                                                </div>
                                                @error('concepts[{{$key}}][item]')
                                                <span class="error" role="alert">{{ $message }}</span>
                                                @enderror
                                            </td>
                                            <td class="td-price">
                                                <input type="number" min="0.01" step="any" placeholder="Price" name="concepts[{{$key}}][price]" id="concepts[{{$key}}][price]" class="form-control" value="{{ old('concepts')[$key]['price'] ?: $item->price }}" required />
                                            </td>
                                            <td class="td-disc" style="text-align: center;">
                                                <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="concepts[{{$key}}][discount]" id="concepts[{{$key}}][discount]" class="form-control" value="{{ old('concepts')[$key]['discount'] ?: $item->discount }}" />
                                            </td>
                                            <td class="td-tax">
                                                {!! Form::select('concepts['.$key.'][tax]', $tax_options, old('concepts['.$key.'][tax]', $item->tax_id), ['class'=>"form-control small-single-search-selection", 'id'=>'concepts['.$key.'][tax]'], $tax_attributes) !!}
                                            </td>
                                            <td class="td-tax_amount">
                                                <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="concepts[{{$key}}][tax_amount]" id="concepts[{{$key}}][tax_amount]" class="form-control" readonly value="{{ old('concepts')[$key]['tax_amount'] ?: $item->calculation['taxAmount'] }}" />
                                            </td>
                                            <td class="td-quantity">
                                                <input type="number" placeholder="1" name="concepts[{{$key}}][quantity]" id="concepts[{{$key}}][quantity]" class="form-control" min="1" value="{{ old('concepts')[$key]['quantity'] ?: $item->quantity }}">
                                            </td>
                                            <td class="td-total">
                                                <span id="concepts[{{$key}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                            </td>
                                            <td>
                                                @if($key != 0)
                                                    <a class="delete-icon remove-concept-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <script type="text/javascript">
                                            id = "{{$key}}";
                                            generateHierarchySelect('concept-multilevelselect-'+id, 'input[name="concepts['+id+'][item]"]');
                                        </script>
                                        @endforeach
                                    @endif
                                </tbody>
                          
                            </table>
                        </div>
                        <a class="btn-custom add-concept-with-product-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    @if($invoice->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span>{{ $invoice->currency->code }}</span></th>
                                        <td><span>{{ $invoice->currency->code }} {{ $invoice->currency->symbol }}</span><span id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1
                                                <span class="exchange_currency_label">{{ $invoice->currency->code }}</span>
                                                = {{ Auth::user()->company->currency->code }}
                                                ({{ Auth::user()->company->currency->symbol }})
                                                <span
                                                    class="">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span>
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="invoice_total_hidden" id="invoice_total_hidden" value="{{ $invoice->calculation['total'] }}" />
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('invoices.index', ['type' => $type]) }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom documents-submit-btn">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/concepts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        var documents = {!! json_encode($invoice->documents) !!};
        addDropZone(documents,'image');
    </script>
@endpush