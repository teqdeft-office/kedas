@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.recurring_invoice'))

@section('content')
<input type="hidden" name="resource_type" value="{{ $invoice->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.modify').' '.ucfirst($type).' '. __('labels.invoice').' '.$invoice->internal_number }}</h2>
        <div class="default-form add-recurring-invoice-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['invoices.update', $invoice], 'id' => 'add-edit-invoices-form', 'enctype' => 'multipart/form-data')) }}
                @csrf
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.contact') }} <span>*</span></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', $invoice->contact_custom_id), ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' => 'required'], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="numbering">{{ __('labels.numbering') }} <span>*</span></label>
                            <input class="form-control" type="text" disabled name="invoice_number" value="{{ $invoice->internal_number }}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="observation">{{ __('labels.observation') }}</label>
                            <textarea class="form-control" name="observation" id="observation">{{ old('observation') ?: $invoice->observation }}</textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="notes">{{ __('labels.notes_of_invoice') }}</label>
                            <textarea class="form-control" name="notes" id="notes">{{ old('notes') ?: $invoice->notes }}</textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="recurring_invoice_start_date">{{ __('labels.start_date') }} <span>*</span></label>
                            <input id="recurring_invoice_start_date" class="form-control" name="invoice_start_date" required="required" value="{{old('invoice_start_date', $invoice->start_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="recurring_invoice_end_date">{{ __('labels.end_date') }} <span>*</span></label>
                            <input id="recurring_invoice_end_date" name="invoice_end_date" class="form-control" required="required" value="{{old('invoice_end_date', $invoice->end_date)}}" autocomplete="off"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="term">{{ __('labels.term') }}</label>
                            {!! Form::select('term', $term_options, old('term', $invoice->term), ['class'=>"form-control", 'id'=>"term", 'required' => 'required']) !!}
                            <span id="manual_days_section" style="display: none;">
                                <label for="manual_days">{{ __('labels.manual_days') }} <span>*</span></label>
                                <input type="number" name="manual_days" id="manual_days" class="form-control" min="1" max="4000" value="{{old('manual_days', $invoice->manual_days ?? 1)}}" />
                            </span>  
                        </div>
                        <!-- <div class="form-group col-sm-6">
                            <label for="recurring_invoice_expiration_date">{{ __('labels.expiration_date') }} <span>*</span></label>
                            <input id="recurring_invoice_expiration_date" name="invoice_expiration_date" class="form-control" required="required" value="{{old('invoice_expiration_date', $invoice->expiration_date)}}" autocomplete="off" disabled="disabled" />
                        </div> -->
                        <div class="form-group col-sm-6">
                            <div class="row">
                                <div class="col-md-12">
                                   <label for="frequency">{{ __('labels.frequency') }} <span>*</span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <select name="frequency_type" class="form-control">
                                        <option {{ $invoice->frequency_type == "month" ? 'selected="selected"' : '' }} value="month">{{ __('labels.month') }}</option>
                                        <option {{ $invoice->frequency_type == "day" ? 'selected="selected"' : '' }} value="day">{{ __('labels.day') }}</option>
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <input type="number" name="frequency" id="frequency" class="form-control" min="1" max="31" value="{{old('frequency', $invoice->frequency)}}" />
                                </div>
                            </div>
                        </div>
                        @if($invoice->currency)
                        <div class="currency_exchange_cnt col-md-6">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    <input type="text" id="exchange-currency" name="exchange_currency" class="form-control"
                                        value="{{old('exchange_currency', $invoice->currency->name.' - '.$invoice->currency->code.' ('.$invoice->currency->symbol.')' )}}"
                                        readonly />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{old('exchange_rate', $invoice->exchange_currency_rate)}}" readonly />
                                    <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $invoice->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span>
                                    </label>
                                </div>

                            </div>
                        </div>
                        @endif
                        @if($invoice->ncf)
                        <div class="form-group col-sm-6 custom-select-controller">
                            <label for="ncf_id">{{ __('labels.invoice_with') }}</label>
                            <input type="text" name="ncf_name" id="ncf_name" class="form-control" disabled="disabled" value="{{$invoice->ncf->name}}" />
                        </div>
                        @endif
                    </div>
                    <div class="default-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped mb-0" id="invoice_items">
                                <thead>
                                    <tr>
                                        <th class="th-item">{{ __('labels.item') }}</th>
                                        <th class="th-reference">{{ __('labels.reference') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <!-- <th class="th-description">{{ __('labels.description') }}</th> -->
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(old('products', [])))
                                        @foreach (old('products', []) as $key => $product)
                                            @include('app.item-row', ['row_number' => $key])
                                        @endforeach
                                    @else
                                        @foreach ($invoice->items as $key => $item)
                                        <tr>
                                            <input type="hidden" name="products[{{$key}}][id]" value="{{$item->id ?: 0}}">
                                            <td class="td-item">
                                                {!! Form::select('products['.$key.'][item]', $inventory_options, old('products[{{$key}}][item]', $item->inventory_id), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$key.'][item]', 'required' => 'required'], $inventory_attributes) !!}
                                            </td>
                                            <td class="td-reference">
                                                <input type="text" placeholder="{{ __('labels.reference') }}" name="products[{{$key}}][reference]" id="products[{{$key}}][reference]" class="form-control" value="{{ old('products')[$key]['reference'] ?: $item->reference }}" />
                                            </td>
                                            <td class="td-price">
                                                <input type="text" placeholder="{{ __('labels.unit_price') }}" name="products[{{$key}}][price]" id="products[{{$key}}][price]" class="form-control" readonly value="{{ old('products')[$key]['price'] ?: $item->price }}" />
                                            </td>
                                            <td class="td-disc" style="text-align: center;">
                                                <input type="number" min="0" max="99" placeholder="%" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" name="products[{{$key}}][discount]" id="products[{{$key}}][discount]" class="form-control" value="{{ old('products')[$key]['discount'] ?: $item->discount }}" />
                                            </td>
                                            <td class="td-tax">
                                                {!! Form::select('products['.$key.'][tax]', $tax_options, old('products['.$key.'][tax]', $item->tax_id), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$key.'][tax]'], $tax_attributes) !!}
                                            </td>
                                            <td class="td-tax_amount">
                                                <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="products[{{$key}}][tax_amount]" id="products[{{$key}}][tax_amount]" class="form-control" readonly value="{{ old('products')[$key]['tax_amount'] ?: $item->calculation['taxAmount'] }}" />
                                            </td>
                                            <!-- <td class="td-description">
                                                <textarea  placeholder="{{ __('labels.description') }}" name="products[{{$key}}][description]" id="products[{{$key}}][description]" class="form-control" readonly>{{ old('products')[$key]['description'] ?: $item->inventory->description }}</textarea>
                                            </td> -->
                                            <td class="td-quantity">
                                                <input type="number" placeholder="1" name="products[{{$key}}][quantity]" id="products[{{$key}}][quantity]" class="form-control" min="1" value="{{ old('products')[$key]['quantity'] ?: $item->quantity }}">
                                            </td>
                                            <td class="td-total">
                                                <span id="products[{{$key}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                            </td>
                                            <td>
                                                @if($key != 0)
                                                    <a class="delete-icon remove-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    @if($invoice->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span>{{ $invoice->currency->code }}</span></th>
                                        <td><span>{{ $invoice->currency->code }}
                                                {{ $invoice->currency->symbol }}</span><span
                                                id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $invoice->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span>
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="terms-conditions-form">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="privacy_policy">{{ __('labels.privacy_policy') }}</label>
                                <textarea class="form-control" name="privacy_policy" id="privacy_policy">{{ old('privacy_policy', $invoice->privacy_policy) }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('invoices.index', ['type' => $type]) }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/invoices.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush