@extends('layouts.dashboard')

@section('title', trans('labels.clone').' '.trans('labels.invoice'))

@section('content')
<input type="hidden" name="resource_type" value="{{ $invoice->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{__('labels.clone_this_invoice') }} </h2>
        <div class="default-form add-invoice-form">
            {{ Form::open(array('method'=>'POST', 'route' => ['invoices.store', ['type'=>$invoice->type]], 'id' => 'add-edit-invoices-form', 'enctype' => 'multipart/form-data')) }}
            <div class="generate-invoice">
                <div class="row no-margin">
                    <div class="col-sm-4">
                        <div class="form-group file-input file-input-medium">
                            @if(!Auth::user()->company->logo)
                            <label for="image" id="image-preview-label">
                                <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                {{ __('labels.logo') }}
                            </label>
                            @endif
                            @if(Auth::user()->company->logo)
                            <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                alt="{{ Auth::user()->name }}" />
                            @else
                            <img id="image-preview" src="" />
                            @endif
                            <input type="file" id="company-logo" name="logo" class="file-upload-preview"
                                accept="image/*" data-img_id="image-preview"
                                data-msg-accept="{{ __('messages.invalid_image_file_error') }}."
                                data-label_id="image-preview-label" />
                            <a class="upload-button" data-preview_section="company-logo"><i class="fa fa-pencil"
                                    aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                            @error('logo')
                            <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-8 text-center company-data">
                        <div class="add-cell">
                            <div class="consumption-box">
                                <div class="consumption-bottom">
                                    <span>{{ __('labels.no') }}.</span>
                                    <div class="consumption-number">{{ $next_invoice_number }}</div>
                                </div>
                            </div>
                            <div class="add-cell-center">
                                <h2>{{ Auth::user()->company->name }}</h2>
                                @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                <span>
                                    <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}
                                    </p>
                                </span>
                                @endif
                                @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                <span>
                                    <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}
                                    </p>
                                </span>
                                @endif
                                <span>
                                    <h3>{{ __('labels.phone') }}:</h3>
                                    <p>{{ Auth::user()->company->phone }}</p>
                                    @if(Auth::user()->company->mobile)
                                    <h3>{{ __('labels.mobile') }}:</h3>
                                    <p>{{ Auth::user()->company->mobile }}</p>
                                    @endif
                                </span>
                                @if(Auth::user()->company->support_email)
                                <span>
                                    <h3>{{ __('labels.email') }}:</h3>
                                    <p><a href="mailto:{{Auth::user()->company->support_email}}">
                                            {{ Auth::user()->company->support_email }} </a></p>
                                </span>
                                @endif
                                @if(Auth::user()->company->tax_id)
                                <span>
                                    <h3>{{ __('labels.tax_id') }}:</h3>
                                    <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="add-form row">
                    <div class="form-group col-sm-6">
                        <label for="contact" class="col-sm-12 creat-nw-cat">{{ __('labels.contact') }} <span>*</span> <a target="_blank" href="{{ route('clients.create') }}">{{ __('labels.create_new_contact') }}</a></label>
                        {!! Form::select('contact_id', $contact_options, old('contact_id', $invoice->contact_custom_id),
                        ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' => 'required',
                        'style'=> $resource_associated ? 'pointer-events:none;' : ''], $contact_attributes) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="date">{{ __('labels.date') }} <span>*</span></label>
                        <input id="invoice_start_date" class="form-control" name="invoice_start_date"
                            required="required" value="{{old('invoice_start_date', $invoice->start_date)}}"
                            autocomplete="off" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="contact-tax-id">{{ __('labels.tax_id') }}</label>
                        <input type="text" disabled class="form-control" name="contact-tax-id" id="contact-tax-id"
                            value="{{ $invoice->contact->tax_id }}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="contact-phone">{{ __('labels.phone') }}</label>
                        <input type="text" disabled class="form-control" name="contact-phone" id="contact-phone"
                            value="{{ $invoice->contact->phone }}" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="contact">{{ __('labels.term') }}</label>
                        {!! Form::select('term', $term_options, old('term', $invoice->term), ['class'=>"form-control",
                        'id'=>"term", 'required' => 'required']) !!}
                        <span id="manual_days_section" style="display: none;">
                            <label for="manual_days">{{ __('labels.manual_days') }} <span>*</span></label>
                            <input type="number" name="manual_days" id="manual_days" class="form-control" min="1"
                                max="90" value="{{old('manual_days', $invoice->manual_days ?? 1)}}" />
                        </span>
                    </div>
                    @if($invoice->ncf)
                    <div class="form-group col-sm-6 custom-select-controller">
                        <label for="ncf_id">{{ __('labels.ncf_number') }}</label>
                        <input type="text" name="ncf_number" id="ncf_number" class="form-control" disabled="disabled"
                            value="{{$invoice->ncf_number}}" />
                    </div>
                    @endif
                    <div class="form-group col-sm-6">
                        <label for="contact">{{ __('labels.expiration_date') }} <span>*</span></label>
                        <input id="invoice_expiration_date" name="invoice_expiration_date" class="form-control"
                            required="required" value="{{old('invoice_expiration_date', $invoice->expiration_date)}}"
                            autocomplete="off" disabled="disabled" />
                    </div>
                    @if($invoice->currency)
                    <div class="currency_exchange_cnt col-md-6">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                <input type="text" name="currency" class="form-control"
                                    value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                    readonly />
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                {!! Form::select('exchange_currency', $currency_options, old('exchange_currency',
                                    $invoice->exchange_currency_id), ['class'=>"form-control single-search-selection", 'id'=>"exchange-currency"],
                                    $currency_attributes) !!}
                                    @error('exchange_currency')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{ old('exchange_rate') ?: $invoice->exchange_currency_rate }}" autocomplete="off" min="0.01" step="any" />
                                    @error('exchange_rate')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                    <label class="currency_exchange_inner" style="display:none">1 <span
                                        class="exchange_currency_label"></span> =
                                    {{ Auth::user()->company->currency->code }}
                                    ({{ Auth::user()->company->currency->symbol }}) <span
                                        class="exchange_currency_rate"></span> </label>
                            </div>

                        </div>
                    </div>
                    @endif
                </div>
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped mb-0" id="invoice_items">
                            <thead>
                                <tr>
                                    <th class="th-item">{{ __('labels.item') }}</th>
                                    <th class="th-reference">{{ __('labels.reference') }}</th>
                                    <th class="th-price">{{ __('labels.price') }}</th>
                                    <th class="th-disc">{{ __('labels.disc') }} %</th>
                                    <th class="th-tax">{{ __('labels.tax') }}</th>
                                    <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                    <!-- <th class="th-description">{{ __('labels.description') }}</th> -->
                                    <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                    <th class="th-total">{{ __('labels.total') }}</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count(old('products', [])))
                                @foreach (old('products', []) as $key => $product)
                                @include('app.item-row', ['row_number' => $key])
                                @endforeach
                                @else
                                @foreach ($invoice->items as $key => $item)
                                <tr>
                                    <input type="hidden" name="products[{{$key}}][id]" value="{{$item->id ?: 0}}">
                                    <td class="td-item">
                                        {!! Form::select('products['.$key.'][item]', $inventory_options,
                                        old('products[{{$key}}][item]', $item->inventory_id), ['class'=>"form-control
                                        small-single-search-selection", 'id'=>'products['.$key.'][item]', 'required' =>
                                        'required'], $inventory_attributes) !!}
                                    </td>
                                    <td class="td-reference">
                                        <input type="text" placeholder="{{ __('labels.reference') }}"
                                            name="products[{{$key}}][reference]" id="products[{{$key}}][reference]"
                                            class="form-control"
                                            value="{{ old('products')[$key]['reference'] ?: $item->reference }}" />
                                    </td>
                                    <td class="td-price">
                                        <input type="text" placeholder="{{ __('labels.unit_price') }}"
                                            name="products[{{$key}}][price]" id="products[{{$key}}][price]"
                                            class="form-control" readonly
                                            value="{{ old('products')[$key]['price'] ?: $item->price }}" />
                                    </td>
                                    <td class="td-disc" style="text-align: center;">
                                        <input type="number" min="0" max="99" placeholder="%"
                                            pattern="^\d*(\.\d{0,2})?$"
                                            data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}"
                                            name="products[{{$key}}][discount]" id="products[{{$key}}][discount]"
                                            class="form-control"
                                            value="{{ old('products')[$key]['discount'] ?: $item->discount }}" />
                                    </td>
                                    <td class="td-tax">
                                        {!! Form::select('products['.$key.'][tax]', $tax_options,
                                        old('products['.$key.'][tax]', $item->tax_id), ['class'=>"form-control
                                        small-single-search-selection", 'id'=>'products['.$key.'][tax]'],
                                        $tax_attributes) !!}
                                    </td>
                                    <td class="td-tax_amount">
                                        <input type="text" placeholder="{{ __('labels.tax_amount') }}"
                                            name="products[{{$key}}][tax_amount]" id="products[{{$key}}][tax_amount]"
                                            class="form-control" readonly
                                            value="{{ old('products')[$key]['tax_amount'] ?: $item->calculation['taxAmount'] }}" />
                                    </td>
                                    {{-- <td class="td-description">
                                                <textarea  placeholder="{{ __('labels.description') }}" name="products[{{$key}}][description]" id="products[{{$key}}][description]" class="form-control" readonly>{{ old('products')[$key]['description'] ?: $item->inventory->description }}</textarea>
                                            </td> --}}
                                    <td class="td-quantity">
                                        <input type="number" placeholder="1" name="products[{{$key}}][quantity]"
                                            id="products[{{$key}}][quantity]" class="form-control" min="1"
                                            value="{{ old('products')[$key]['quantity'] ?: $item->quantity }}">
                                    </td>
                                    <td class="td-total">
                                        <span
                                            id="products[{{$key}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                    </td>
                                    <td>
                                        @if($key != 0)
                                        <a class="delete-icon remove-row" data-row_number="{{$key}}"><i
                                                class="fa fa-times" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>

                        </table>
                    </div>

                    <a class="btn-custom add-row mt-3 mb-3"><i class="fa fa-plus" aria-hidden="true"></i>
                        {{ __('labels.add_row') }}</a>
                </div>
                <div class="row no-margin">
                    <div class="col-sm-4">
                        <div class="form-group file-input file-input-medium">
                            @if(!Auth::user()->company->signature)
                            <label for="signature" id="signature-preview-label">
                                <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                {{ __('labels.signature') }}
                            </label>
                            @endif
                            @if(Auth::user()->company->signature)
                            <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}"
                                alt="{{ Auth::user()->name }}" />
                            @else
                            <img id="signature-preview" src="" />
                            @endif
                            <input type="file" id="company-sign" name="signature" class="file-upload-preview"
                                accept="image/*" data-img_id="signature-preview"
                                data-msg-accept="{{ __('messages.invalid_image_file_error') }}."
                                data-label_id="signature-preview-label" />
                            <a class="upload-button" data-preview_section="company-sign"><i class="fa fa-pencil"
                                    aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                            @error('signature')
                            <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table custom-total-table">
                            <table>
                                <tr>
                                    <th>{{ __('labels.base_total') }}</th>
                                    <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                </tr>
                                <tr>
                                    <th>{{ __('labels.discount') }}</th>
                                    <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                </tr>
                                <tr>
                                    <th>{{ __('labels.sub_total') }}</th>
                                    <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                </tr>
                                <tr>
                                    <th>{{ __('labels.total') }}</th>
                                    <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                </tr>
                                @if($invoice->currency)
                                <tr class="total_exchanged_rate">
                                    <th>{{ __('labels.total') }} <span>{{ $invoice->currency->code }}</span></th>
                                    <td><span>{{ $invoice->currency->code }}
                                            {{ $invoice->currency->symbol }}</span><span
                                            id="total-ex-currency-rate"></span></td>
                                </tr>
                                <tr class="total_currency_exchange-tr">
                                    <th>
                                        <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $invoice->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span>
                                        </label>
                                    </th>
                                </tr>
                                @endif
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="invoice_total_hidden" id="invoice_total_hidden"
                        value="{{ $invoice->calculation['total'] }}" />
                </div>
                <div class="terms-conditions-form">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-6">
                            <label for="tac">{{ __('labels.terms_and_conditions') }}</label>
                            <textarea class="form-control" name="tac" id="tac"
                                placeholder="Visible in document printing">{{ old('tac', $invoice->tac) }}</textarea>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label for="notes col-sm-12">{{ __('labels.notes') }}</label>
                            <textarea class="form-control" name="notes" id="notes"
                                placeholder="Visible in document printing">{{ old('notes', $invoice->notes) }}</textarea>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label for="res_text col-sm-12">{{ __('labels.resolution_text') }}</label>
                            <textarea class="form-control" name="res_text" id="res_text"
                                placeholder="Visible in document printing">{{ old('res_text', $invoice->res_text) }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="privacy_policy">{{ __('labels.privacy_policy') }}</label>
                            <textarea class="form-control" name="privacy_policy" id="privacy_policy">{{ old('privacy_policy', $invoice->privacy_policy) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group popup-btns">
                <a href="{{ route('invoices.index', ['type' => $type]) }}"><button type="button" name="cancel"
                        class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                <button type="submit" name="submit" class="btn-custom">{{ __('labels.save_changes') }}</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('public/js/invoices.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/currency-exchange.js') }}"></script>
@endpush
