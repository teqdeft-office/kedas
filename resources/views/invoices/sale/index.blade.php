@extends('layouts.dashboard')
@section('title', trans('labels.invoices'))
@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.invoices') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.invoice') }},</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('invoices.create', ['type' => $type]) }}" class="btn-custom"><i class="fa fa-plus"
                    aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.invoice') }}</a>
        </div>

        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.filter') }}: <input id="filter" type="text"
                                autocomplete="off" value=" "></span>
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text"
                                autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text"
                                autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table table-responsive">
                        <table class="table table-striped" id="invoice-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    <th>{{ __('labels.expiration_date') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.paid') }}</th>
                                    <th>{{ __('labels.pending') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.discount') }} {{ __('labels.amount') }}</th>
                                    <th>{{ __('labels.term') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                    <th style="display: none;">{{ __('labels.notes') }}</th>
                                    <th style="display: none;">{{ __('labels.tac') }}</th>
                                    <th style="display: none;">{{ __('labels.privacy_policy') }}</th>
                                    <th style="display: none;">{{ __('labels.phone') }}</th>
                                    <th style="display: none;">{{ __('labels.base_total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->internal_number }}</td>
                                    <td>{{ $invoice->contact->name }}</td>
                                    <td>{{ dateFormat($invoice->start_date) }}</td>
                                    <td>{{ dateFormat($invoice->expiration_date) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['total']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['paidAmount']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['pendingAmount']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['taxAmount']) }}</td>
                                    <td>{{ moneyFormat($invoice->calculation['discountAmount']) }}</td>
                                    <td>{{ $invoice->term_in_locale }}</td>
                                    <td>{{ $invoice->calculation['status'] }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a
                                                            href="{{route('invoices.show', $invoice)}}">{{ __('labels.view') }}</a>
                                                    </li>
                                                    <li><a
                                                            href="{{route('invoices.edit', $invoice)}}">{{ __('labels.edit') }}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="display: none;">{{ $invoice->notes }}</td>
                                    <td style="display: none;">{{ $invoice->tac }}</td>
                                    <td style="display: none;">{{ $invoice->privacy_policy }}</td>
                                    <td style="display: none;">{{ $invoice->contact->phone }}</td>
                                    <td style="display: none;">{{ $invoice->calculation['baseTotal'] }}</td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message">
                                            <p>{{ __('labels.you_have_not_yet_created').' '.__('labels.invoice') }}!</p>
                                        </div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('invoices.create', ['type' => $type]) }}"
                                                class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i>
                                                {{ __('labels.new').' '.__('labels.invoice') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    <th>{{ __('labels.expiration_date') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.paid') }}</th>
                                    <th>{{ __('labels.pending') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.actions') }}</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection