@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.invoice'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.invoice') }}</h2>
        <div class="default-form add-invoice-form">
            <form method="POST" action="{{ route('invoices.store', ['type' => $type]) }}" id="add-edit-invoices-form"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="remission_id" value="{{ $remission_id }}">
                <div class="generate-invoice">
                    <div class="row no-margin">
                        <div class="col-lg-4">
                            <div class="form-group file-input file-input-medium">
                                @if(!Auth::user()->company->logo)
                                <label for="image" id="image-preview-label">
                                    <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                    {{ __('labels.logo') }}
                                </label>
                                @endif
                                @if(Auth::user()->company->logo)
                                <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                    alt="{{ Auth::user()->company->name }}" />
                                @else
                                <img id="image-preview" src="" />
                                @endif
                                <input type="file" id="company-logo" name="logo" class="file-upload-preview"
                                    accept="image/*" data-img_id="image-preview"
                                    data-msg-accept="{{ __('messages.invalid_image_file_error') }}"
                                    data-label_id="image-preview-label" />
                                <a class="upload-button" data-preview_section="company-logo"><i class="fa fa-pencil"
                                        aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('logo')
                                <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-8 text-center company-data">
                            <div class="add-cell">
                                <div class="consumption-box">
                                    <div class="consumption-bottom">
                                        <span>{{ __('labels.no') }}.</span>
                                        <div class="consumption-number">{{ $next_invoice_number }}</div>
                                    </div>
                                </div>
                                <div class="add-cell-center">
                                    <h2>{{ Auth::user()->company->name }}</h2>
                                    @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">
                                            {{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                    @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">
                                            {{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3>
                                        <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->mobile)
                                        <h3>{{ __('labels.mobile') }}:</h3>
                                        <p>{{ Auth::user()->company->mobile }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                    <span>
                                        <h3>{{ __('labels.email') }}:</h3>
                                        <p><a href="mailto:{{Auth::user()->company->support_email}}">
                                                {{ Auth::user()->company->support_email }} </a></p>
                                    </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                    <span>
                                        <h3>{{ __('labels.tax_id') }}:</h3>
                                        <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact" class="col-sm-12 creat-nw-cat">{{ __('labels.contact') }} <span>*</span> <a target="_blank" href="{{ route('clients.create') }}">{{ __('labels.create_new_contact') }}</a></label>
                            <!-- <label for="contact">{{ __('labels.contact') }} <span>*</span></label> -->
                            {!! Form::select('contact_id', $contact_options, old('contact_id', null),
                            ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' =>
                            'required'], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="invoice_start_date" class="form-control" name="invoice_start_date"
                                required="required" value="{{old('invoice_start_date', getTodayDate())}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact-tax-id">{{ __('labels.tax_id') }}</label>
                            <input type="text" disabled class="form-control" name="contact-tax-id" id="contact-tax-id">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact-phone">{{ __('labels.phone') }}</label>
                            <input type="text" disabled class="form-control" name="contact-phone" id="contact-phone">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="term">{{ __('labels.term') }}</label>
                            {!! Form::select('term', $term_options, old('term', null), ['class'=>"form-control",
                            'id'=>"term", 'required' => 'required']) !!}
                            <span id="manual_days_section" style="display: none;">
                                <label for="manual_days">{{ __('labels.manual_days') }} <span>*</span></label>
                                <input type="number" name="manual_days" id="manual_days" class="form-control" min="1"
                                    max="4000" value="{{old('manual_days', 1)}}" />
                            </span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="invoice_expiration_date">{{ __('labels.expiration_date') }}
                                <span>*</span></label>
                            <input id="invoice_expiration_date" name="invoice_expiration_date" class="form-control"
                                required="required" value="{{old('invoice_expiration_date', getTodayDate())}}"
                                autocomplete="off" disabled="disabled" />
                        </div>
                        <div class="currency_exchange_cnt col-lg-6">
                            <div class="row">
                                <div class="form-group col-lg-4 col-md-12">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-lg-4 col-md-12 custom-select-controller">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    {!! Form::select('exchange_currency', $currency_options, old('exchange_currency',
                                    null), ['class'=>"form-control single-search-selection", 'id'=>"exchange-currency"],
                                    $currency_attributes) !!}
                                    @error('exchange_currency')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-lg-4 col-md-12">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{ old('exchange_rate') ?: null }}" autocomplete="off" min="0.01" step="any" />
                                    @error('exchange_rate')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                    <label class="currency_exchange_inner" style="display:none">1 <span
                                        class="exchange_currency_label"></span> =
                                    {{ Auth::user()->company->currency->code }}
                                    ({{ Auth::user()->company->currency->symbol }}) <span
                                        class="exchange_currency_rate"></span> </label>
                                </div>
                    
                            </div>
                        </div>
                        <div class="form-group col-lg-6 custom-select-controller">
                            <label for="ncf_id">{{ __('labels.ncf') }}</label>
                            {!! Form::select('ncf_id', $ncf_options, old('ncf_id', null), ['class'=>"form-control
                            single-search-selection", 'id'=>"ncf_id"]) !!}
                        </div>
                        <div class="form-group col-sm-6 custom-select-controller client-supplier-checkbox-cnt" style="margin-top:10px;">
                            <label class="container">{{ __('labels.include_shipping_cost') }}
                            <input type="checkbox"id="shippingInput"  >
                        </div>
                        <div id="shippingDiv" class="form-group col-sm-6 custom-select-controller" >
                            <label>{{ __('labels.shipping_cost') }}</label>
                            <input id="shippingValues" name="products[0][shipping_cost]" type="number" class="form-control shippingCost">
                        </div>
                        <div class="form-group col-lg-6 custom-select-controller">
                            <label for="barcode" class="col-sm-12">{{ __('labels.barcode') }} </label>
                            <input type="text" class="form-control" name="barcode" id="barcode" placeholder="{{ __('labels.scan_prod_code') }}" value="{{ old('barcode', null) }}" autocomplete="off">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.pdf') }}, {{ __('labels.image') }})</label>
                                <div class="form-control dropzone" id="document-dropzone">
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                    </div>

                    <div class="default-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped mb-0" id="invoice_items">
                                <thead>
                                    <tr>
                                        <th class="th-item">{{ __('labels.item') }}</th>
                                        <th class="th-reference">{{ __('labels.reference') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <!-- <th class="th-description">{{ __('labels.description') }}</th> -->
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="td-item">
                                            {!! Form::select('products[0][item]', $inventory_options,
                                            old('products[0][item]', null), ['class'=>"form-control
                                            small-single-search-selection", 'id'=>"products[0][item]", 'required' =>
                                            'required'], $inventory_attributes) !!}
                                        </td>
                                        <td class="td-reference">
                                            <input type="text" placeholder="{{ __('labels.reference') }}"
                                                name="products[0][reference]" id="reference" class="form-control"
                                                 value="{{ old('products')[0]['reference'] ?: null }}" />
                                        </td>
                                        <td class="td-price">
                                            <input type="text" placeholder="{{ __('labels.unit_price') }}"
                                                name="products[0][price]" id="unitprice" class="form-control" readonly
                                                value="{{ old('products')[0]['price'] ?: null }}" />
                                        </td>
                                        <td class="td-disc" style="text-align: center;">
                                            <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%"
                                                name="products[0][discount]" id="discount" class="form-control"
                                                value="{{ old('products')[0]['discount'] ?: 0 }}" />
                                        </td>
                                        <td class="td-tax">
                                            {!! Form::select('products[0][tax]', $tax_options, old('products[0][tax]',
                                            null), ['class'=>"form-control small-single-search-selection",
                                            'id'=>"products[0][tax]"], $tax_attributes) !!}
                                        </td>
                                        <td class="td-tax_amount">
                                            <input type="text" placeholder="{{ __('labels.tax_amount') }}"
                                                name="products[0][tax_amount]" id="products[0][tax_amount]"
                                                class="form-control" readonly
                                                value="{{ old('products')[0]['tax_amount'] ?: null }}" />
                                        </td>
                                        <td class="td-quantity">
                                            <input type="number" placeholder="1" name="products[0][quantity]"
                                                id="quantity" class="form-control" min="1"
                                                value="{{ old('products')[0]['quantity'] ?: 1 }}">
                                        </td>
                                        <td class="td-total">
                                            <span
                                                id="products[0][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    @foreach (old('products', []) as $key => $product)
                                    @if ($key == 0)
                                    @continue
                                    @endif
                                    @include('app.item-row', ['row_number' => $key])
                                    @endforeach
                                </tbody>

                            </table>
                        </div>

                        <a class="btn-custom add-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i>
                            {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row no-margin">
                        <div class="col-sm-4">
                            <div class="form-group file-input file-input-medium">
                                @if(!Auth::user()->company->signature)
                                <label for="signature" id="signature-preview-label">
                                    <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                    {{ __('labels.signature') }}
                                </label>
                                @endif
                                @if(Auth::user()->company->signature)
                                <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}"
                                    alt="{{ Auth::user()->name }}" />
                                @else
                                <img id="signature-preview" src="" />
                                @endif
                                <input type="file" id="company-sign" name="signature" class="file-upload-preview"
                                    accept="image/*" data-img_id="signature-preview"
                                    data-msg-accept="{{ __('messages.invalid_image_file_error') }}."
                                    data-label_id="signature-preview-label" />
                                <a class="upload-button" data-preview_section="company-sign"><i class="fa fa-pencil"
                                        aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('signature')
                                <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table custom-total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr >
                                        <th >{{ __('labels.shipping_cost') }}</th>
                                        <td id="shippingCost">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr class="total_exchanged_rate" style="display:none">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label"></span></th>
                                        <td> <span class="total-ex-currency-label-symbol"></span><span
                                                id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner" style="display:none">1 <span
                                                    class="exchange_currency_label"></span> =
                                                {{ Auth::user()->company->currency->code }}
                                                ({{ Auth::user()->company->currency->symbol }}) <span
                                                    class="exchange_currency_rate"></span> </label>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="terms-conditions-form">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="tac">{{ __('labels.terms_and_conditions') }}</label>
                                <textarea class="form-control" name="tac" id="tac">{{ old('tac', null) }}</textarea>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="notes col-sm-12">{{ __('labels.notes') }}</label>
                                <textarea class="form-control" name="notes"
                                    id="notes">{{ old('notes', null) }}</textarea>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="res_text col-sm-12">{{ __('labels.resolution_text') }}</label>
                                <textarea class="form-control" name="res_text"
                                    id="res_text">{{ old('res_text', null) }}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="privacy_policy">{{ __('labels.privacy_policy') }}</label>
                                <textarea class="form-control" name="privacy_policy" id="privacy_policy">{{ old('privacy_policy', null) }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <button type="submit" name="submit" class="btn-custom" value="save_and_pay">{{ __('labels.save_and_pay') }}</button>
                    <a href="{{ route('invoices.index', ['type' => $type]) }}"><button type="button" name="cancel"
                            class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom"  value="save">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    const mapInventoryWithMembership = {!! json_encode($mapInventoryWithMembership) !!};
    const mapMembershipWithDiscount = {!! json_encode($mapMembershipWithDiscount) !!};
</script>
<script type="text/javascript" src="{{ asset('js/invoices.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
<script type="text/javascript">
    addDropZone(null, 'both', 10);
</script>
<script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush