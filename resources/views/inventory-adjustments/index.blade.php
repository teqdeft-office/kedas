@extends('layouts.dashboard')

@section('title', trans('labels.inventory_adjustment'))

@section('content')
<div class="dashboard-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.inventory_adjustments') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.inventory_adjustment') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('inventory-adjustments.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.inventory_adjustment') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom inventory-adjustments-list-table products-table">
                        <table class="table table-striped" id="inventory-adjustments-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.sr_number') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.adjusted_total') }}</th>
                                    <th>{{ __('labels.observations') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($inventoryAdjustments as $key => $adjustment)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ dateFormat($adjustment->adjustment_date) }}</td>
                                    <td>{{ moneyFormat($adjustment->total) }}</td>
                                    <td class="descrip-notes"><span>{{ $adjustment->observations }}</span></td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('inventory-adjustments.show', $adjustment)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('inventory-adjustments.edit', $adjustment)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-inventory-adjustments-form-' . $adjustment->id }}" href="{{route('inventory-adjustments.destroy', $adjustment)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['inventory-adjustments.destroy', $adjustment], 'id' => "destroy-inventory-adjustments-form-{$adjustment->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="6" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.inventory_adjustment') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('inventory-adjustments.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.inventory_adjustment') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
