@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.item_category'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.item_category') }}</h2>
        <form method="POST" action="{{ route('item-categories.store') }}" id="add-edit-item-category-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">{{ __('labels.name') }} <span>*</span></label>
                                <input class="form-control" type="text" name="name" placeholder="{{ __('labels.category_name') }}" required value="{{ old('name') }}" autocomplete="off" />
                                @error('name')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                            <div class="form-group">
                                <label for="description">{{ __('labels.description') }}</label>
                                <textarea class="form-control" name="description" placeholder="{{ __('labels.category_description') }}" autocomplete="off">{{ old('description', null) }}</textarea>
                                @error('description')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group prduct-pic">
                                <div class="form-group file-input file-input-large">
                                    <label for="image" id="image-preview-label">
                                        <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                        {{ __('labels.icon') }}
                                    </label>
                                    <img src="" id="image-preview" style="display:none" />                            
                                    <input type="file" id="item-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label">
                                    <a class="upload-button" data-preview_section="item-image"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                    @error('image')
                                        <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('item-categories.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
