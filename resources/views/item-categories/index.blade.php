@extends('layouts.dashboard')

@section('title', trans('labels.item_categories'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.item_categories') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.item_category') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('import_resource', ['resource' => 'item-categories']) }}" class="btn-custom outline-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ __('labels.import_from_excel') }}</a>
            <a href="{{ route('export_resource', ['resource' => 'item-categories']) }}" class="btn-custom outline-btn"><i class="fa fa-download" aria-hidden="true"></i> {{ __('labels.export') }}</a>
            <a href="{{ route('item-categories.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.item_category') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="item-categories-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($itemCategories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ showIfAvailable($category->description) }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('item-categories.edit', $category)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-category-form-' . $category->id }}" href="{{route('item-categories.destroy', $category)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['item-categories.destroy', $category], 'id' => "destroy-category-form-{$category->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.category') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('item-categories.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.item_category') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
