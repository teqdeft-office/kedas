@extends('layouts.dashboard')

@section('title', trans('labels.fixed_assets'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.fixed_assets') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.fixed_asset') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            {{-- <a href="{{ route('import_resource', ['resource' => 'inventories']) }}" class="btn-custom outline-btn"><i
                    class="fa fa-upload" aria-hidden="true"></i> {{ __('labels.import_from_excel') }}</a>
            <a href="{{ route('export_resource', ['resource' => 'inventories']) }}" class="btn-custom outline-btn"><i
                    class="fa fa-download" aria-hidden="true"></i> {{ __('labels.export') }}</a> --}}
            <a href="{{ route('fixed-assets.create') }}" class="btn-custom"><i
                    class="fa fa-plus" aria-hidden="true"></i>
                {{ __('labels.fixed_assets') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="fixed-assets-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.serial_number') }}</th>
                                    <th>{{ __('labels.date_of_purchase') }}</th>
                                    <!-- <th>{{ __('labels.useful_life') }}</th> -->
                                    <th>{{ __('labels.price_of_purchase') }}</th>
                                    <th>{{ __('labels.legal_category') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($fixedAssets as $key => $fixedAsset)
                                <tr>
                                    <td>{{ $fixedAsset->name }}</td>
                                    <td>{{ showIfAvailable($fixedAsset->serial_number) }}</td>
                                    <td>{{ $fixedAsset->date_of_purchase ? dateFormat($fixedAsset->date_of_purchase) : 'N/A' }}</td>
                                    <td>{{ moneyFormat($fixedAsset->price) }}</td>
                                    {{-- <td>{{ $fixedAsset->legalCategory->name }}</td> --}}
                                    <td>{{ Config::get('constants.legal_category')[$fixedAsset->legal_category] }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle"
                                                data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a href="{{route('fixed-assets.show', $fixedAsset)}}">{{ __('labels.view') }}</a>
                                                    <li><a href="{{route('fixed-assets.edit', $fixedAsset)}}">{{ __('labels.edit') }}</a>
                                                    <li><a class="delete_resource" data-resource="{{ 'destroy-fixed-asset-form-' . $fixedAsset->id }}" href="{{route('fixed-assets.destroy', $fixedAsset)}}">{{ __('labels.delete') }}</a>
                                                    </li>
                                                    {{ Form::open(array('method'=>'DELETE','route' => ['fixed-assets.destroy', $fixedAsset], 'id' => "destroy-fixed-asset-form-{$fixedAsset->id}", 'style' => 'display: none')) }}
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="5" rowspan="2" align="center">
                                        <div class="message">
                                            <p>{{ __('labels.you_have_not_yet_created') }} {{ __('labels.fixed_assets') }}</p>
                                        </div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('fixed-assets.create') }}" class="btn-custom"><i
                                                    class="fa fa-plus" aria-hidden="true"></i>
                                                {{ __('labels.new') }} {{ __('labels.fixed_assets') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
