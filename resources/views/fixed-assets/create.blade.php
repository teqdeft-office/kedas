@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.fixed_asset'))

@section('content')

@inject('controller', 'App\Http\Controllers\Controller')

<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.fixed_asset') }}</h2>
        <div class="default-form add-product-form invoice-details-form">
            <form method="POST" action="{{ route('fixed-assets.store') }}" id="add-edit-fixed-assets-form" enctype="multipart/form-data">
                @csrf
                <div class="generate-invoice">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12">{{ __('labels.name') }} <span>*</span></label>
                                <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}"  value="{{ old('name') }}" autocomplete="off" required />
                                @error('name')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-sm-12">{{ __('labels.price_of_purchase') }} <span>*</span></label>
                                <input class="form-control" type="text" name="price" placeholder="{{ __('labels.price') }}"  value="{{ old('price') }}" autocomplete="off" required/>
                                @error('price')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="account" class="col-sm-12">{{ __('labels.account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="fixed-asset-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="fixed-asset-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="fixed-asset-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($fixed_assets_accounts_options as $fixed_assets_accounts_option)
                                                <a class="dropdown-item" data-value="{{$fixed_assets_accounts_option->id}}" data-level="{{$fixed_assets_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$fixed_assets_accounts_option->custom_name}}</span>
                                                    @if($fixed_assets_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $fixed_assets_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('account', null) }}" />
                                </div>
                                @error('account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="date_of_purchase">{{ __('labels.date_of_purchase') }}</label>
                                <input id="date_of_purchase" class="form-control" name="date_of_purchase" value="{{old('date_of_purchase', getTodayDate())}}"
                                autocomplete="off" />
                            </div>
                            
                            <div class="form-group">
                                <label for="location" class="col-sm-12">{{ __('labels.location') }}</label>
                                {!! Form::select('location', $location_options, '', ['class'=>"form-control", 'id'=>"location"]) !!}
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="serial_number" class="col-sm-12">{{ __('labels.serial_number') }}</label>
                                <input class="form-control" type="text" name="serial_number" placeholder="{{ __('labels.serial_number') }}" value="{{ old('serial_number') }}" autocomplete="off" />
                                @error('serial_number')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group create-new">
                                <label for="item_category" class="col-sm-12 creat-nw-cat">{{ __('labels.item_category') }}</label>
                                {!! Form::select('item_category_id', $item_category_options, '', ['class'=>"form-control single-search-selection", 'id'=>"item_category"]) !!}
                            </div>
                            <div class="form-group">
                                <label for="expense_account" class="col-sm-12">{{ __('labels.expenses') }} {{ __('labels.account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="expense-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="expense-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="expense-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($expense_accounts_options as $expense_accounts_option)
                                                <a class="dropdown-item" data-value="{{$expense_accounts_option->id}}" data-level="{{$expense_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$expense_accounts_option->custom_name}}</span>
                                                    @if($expense_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $expense_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="expense_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('expense_account', null) }}" />
                                </div>
                                @error('expense_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                {{-- <label for="legal_category" class="col-sm-12">{{ __('labels.legal_category') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="legal_category-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="legal_category-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="legal_category-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($all_accounts_options as $all_account)
                                                <a class="dropdown-item" data-value="{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$all_account->custom_name}}</span>
                                                    @if($all_account->level == 1)
                                                    <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="legal_category" readonly="readonly" aria-hidden="true" type="text" value="{{ old('legal_category') ?: null }}" />
                                </div> --}}
                                <label for="legal_category" class="col-sm-12">{{ __('labels.legal_category') }} <span>*</span></label>
                                {!! Form::select('legal_category', $legal_category_options, '', ['class'=>"form-control", 'id'=>"legal_category"]) !!}
                                @error('legal_category')
                                <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-12">{{ __('labels.description') }}</label>
                                <textarea class="form-control" name="description" placeholder="{{ __('labels.description') }}" autocomplete="off">{{ old('description', null) }}</textarea>
                                @error('description')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                            
                        </div>

                        <div class="col-lg-4 col-md-6 prduct-pic">
                            <div class="form-group file-input file-input-large">
                                <label for="image" id="image-preview-label">
                                    <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                    {{ __('labels.image_of_item') }}
                                </label>
                                <img src="" id="image-preview" />                            
                                <input type="file" id="inventory-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label">
                                <a class="upload-button" data-preview_section="inventory-image"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('image')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <p>{{ __('labels.total').' '. __('labels.price') }}</p>
                                <h3 id="item_total">{{ moneyFormat() }}</h3>
                            </div>
                        </div>

                    </div>
                    {{-- end of basic information --}}

                    <div class="row">
                        <div class="form-group col-md-12 register-income cust-register-income">
                            <h4>{{ __('labels.apply_depr_on_fixed_assets_line') }}</h4>
                            <div class="radio">
                                <input type="radio" name="depr_should_cal" id="depr_should_cal_on" value="1" {{ old('depr_should_cal', null) == '1' ? 'checked' : '' }} />
                                <label for="depr_should_cal_on">{{ __('labels.yes') }}</label>
                                <input type="radio" name="depr_should_cal" id="depr_should_cal_off" value="0" {{ old('depr_should_cal', null) == '0' ? 'checked' : '' }} />
                                <label for="depr_should_cal_off">{{ __('labels.no') }}</label>
                                @error('depr_should_cal')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    
                    <div id="cust-show-reg-income" class="row cust-show-reg-income">
                        <div class="form-group col-sm-12" id="depr_info_section" style="display: none;">
                            <div class="row cust-show-reg-income-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="cal_type" class="col-sm-12">{{ __('labels.cal_type') }} <span>*</span></label>
                                        {!! Form::select('cal_type', $cal_type_options, '', ['class'=>"form-control", 'id'=>"cal_type"]) !!}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="scrap_value">{{ __('labels.scrap_value') }}(%) <span>*</span></label>
                                        <input class="form-control" type="text" name="scrap_value" placeholder="{{ __('labels.scrap_value') }}(%)"  value="{{ old('scrap_value') }}" autocomplete="off"/>
                                        @error('scrap_value')
                                            <span class="error" role="alert">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="years">{{ __('labels.years') }} <span>*</span></label>
                                        <input type="number" name="years" id="years" class="form-control" min="1" max="30" value="{{old('years', 1)}}"/>
                                    </div>
                                </div>
                            </div>


                            <div id="unit_produced_info" style="display: none;">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="total_unit_produced" class="col-sm-12">{{ __('labels.total_unit_produced') }}</label>
                                    <input class="form-control" type="text" name="total_unit_produced" placeholder="{{ __('labels.total_unit_produced') }}" value="{{ old('total_unit_produced') }}" autocomplete="off" />
                                    @error('total_unit_produced')
                                        <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="total_unit_produced" class="col-sm-12">{{ __('labels.unit_produced_this_year_or_per_year') }}</label>
                                    <input class="form-control" type="text" name="unit_produced_per_year" placeholder="{{ __('labels.unit_produced_per_year') }}" value="{{ old('unit_produced_per_year') }}" autocomplete="off" />
                                    @error('unit_produced_per_year')
                                        <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                                <div class="wrap-checkbox-block cust-wrap-radio">
                                    <label for="same_for_year">{{ __('labels.same_for_year') }}</label>
                                    <div>

                                    {{ Form::radio('same_for_year', '1', old('same_for_year') == '1', array('id'=>'same_for_year_yes')) }}
                                    <label for="same_for_year_yes">Yes</label>
                                    {{ Form::radio('same_for_year', '0', old('same_for_year') == '0', array('id'=>'same_for_year_no')) }}
                                    <label for="same_for_year_no">No</label>
                                    @error('same_for_year')
                                        <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                </div> 

                                <div id="per_year_prod_section" style="display: none;">
                                    <div class="row">
                                        <div class="form-group  col-md-4">
                                            <label for="per_year_prod">{{ __('labels.per_year_prod') }} <span>*</span> ({{ __('labels.csv_format_help_text') }})</label>
                                            <input type="text" name="per_year_prod" id="per_year_prod" class="form-control" placeholder="5000,4000,3000" value="{{ old('per_year_prod')}}" />
                                            @error('per_year_prod')
                                                <span class="error" role="alert">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row cust-inside-row">
                                <div class="list-table-custom cust-show-reg-income-table">
                                    <div class="table-responsive">
                                        <h4>Depreciation Timeline</h4>
                                        <table class="table table-striped" id="depreciation_timeline_table" name="table">
                                            <thead>
                                                <tr>
                                                    <th>Year</th>
                                                    <th>Opening Value</th>
                                                    <th>Depreciation</th>
                                                    <th>Closing Value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row inventory-field-container" id="inventory_details">
                        <div class="col-sm-12">
                            <h3>{{ __('labels.depreciation_per_year') }}</h3>
                            
                            <table id="depreciation_items">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.percentage') }}</th>
                                        <th>{{ __('labels.for_year') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="td-year">
                                            <input type="text" placeholder="{{ __('labels.year') }}" name="depreciation_items[0][year]" id="depreciation_items[0][year]" class="form-control" readonly value="1" />
                                        </td>
                                        <td class="td-disc" style="text-align: center;">
                                            <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.percentage')]) }}" placeholder="%"
                                                name="depreciation_items[0][percentage]" id="percentage" class="form-control"
                                                value="{{ old('depreciation_items')[0]['percentage'] ?: null }}" required="required" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    @foreach (old('depreciation_items', []) as $key => $depreciation_item)
                                    @if ($key == 0)
                                    @continue
                                    @endif
                                    @include('app.depreciation-item-row', ['row_number' => $key])
                                    @endforeach
                                </tbody>
                            </table>
                            <a class="btn-custom add-depreciation-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i>
                            {{ __('labels.add_row') }}</a>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="legal_category" class="col-sm-12">{{ __('labels.legal_category') }} <span>*</span></label>
                                {!! Form::select('legal_category', $legal_category_options, '', ['class'=>"form-control", 'id'=>"legal_category"]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="percentage">{{ __('labels.percentage') }} <span>*</span></label>
                                <input class="form-control" type="text" name="percentage" placeholder="{{ __('labels.percentage') }}(%)" required value="{{ old('percentage') }}" autocomplete="off" />
                                @error('percentage')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="location" class="col-sm-12">{{ __('labels.location') }}</label>
                                {!! Form::select('location', $location_options, '', ['class'=>"form-control", 'id'=>"location"]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="book_value" class="col-sm-12">{{ __('labels.book_value') }} <span>*</span></label>
                                <input class="form-control" type="text" name="book_value" placeholder="{{ __('labels.book_value') }}" required value="{{ old('book_value') }}" autocomplete="off" />
                                @error('book_value')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="date_of_purchase">{{ __('labels.date_of_purchase') }}</label>
                                <input id="date_of_purchase" class="form-control" name="date_of_purchase" value="{{old('date_of_purchase', getTodayDate())}}"
                                autocomplete="off" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="final_date">{{ __('labels.final_date') }}</label>
                                <input id="final_date" class="form-control" name="final_date" value="{{old('final_date', getTodayDate())}}"
                                autocomplete="off" />
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="form-group popup-btns">                    
                    <a href="{{ route('inventories.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('js/fixed-assets.js') }}"></script>
@endpush