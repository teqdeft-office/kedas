@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.estimate'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.modify').' '.__('labels.estimate') }} {{ $estimate->internal_number }}</h2>
        <div class="default-form add-invoice-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['estimates.update', $estimate], 'id' => 'add-edit-estimates-form', 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <div class="row no-margin">
                        <div class="col-sm-4">
                            <div class="form-group file-input file-input-medium">
                                @if(!Auth::user()->company->logo)
                                    <label for="image" id="image-preview-label">
                                        <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                        {{ __('labels.logo') }}
                                    </label>
                                @endif                                
                                @if(Auth::user()->company->logo)
                                  <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}" alt="{{ Auth::user()->name }}" />
                                @else
                                  <img id="image-preview" src="" />
                                @endif                           
                                <input type="file" id="company-logo" name="logo" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}." data-label_id="image-preview-label"/>
                                <a class="upload-button" data-preview_section="company-logo"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('logo')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-8 text-center company-data">
                            <div class="add-cell">
                                <div class="consumption-box">
                                    <div class="consumption-bottom">
                                        <span>{{ __('labels.no') }}.</span>
                                        <div class="consumption-number">{{ $estimate->internal_number }}</div>
                                    </div>
                                </div>
                                <div class="add-cell-center">
                                    <h2>{{ Auth::user()->company->name }}</h2>
                                    @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                    @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3> <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->fax)
                                            <h3>{{ __('labels.fax') }}:</h3> <p>{{ Auth::user()->company->fax }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                        <span>
                                            <h3>{{ __('labels.email') }}:</h3> 
                                            <p><a href="mailto:{{Auth::user()->company->support_email}}"> {{ Auth::user()->company->support_email }} </a></p>
                                        </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                        <span>
                                            <h3>{{ __('labels.tax_id') }}:</h3> 
                                            <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.contact') }} <span>*</span></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', $estimate->contact_custom_id), ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' => 'required'], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="estimate_start_date" class="form-control" name="estimate_start_date" required="required" value="{{old('estimate_start_date', $estimate->start_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact-tax-id">{{ __('labels.tax_id') }}</label>
                            <input type="text" disabled class="form-control" name="contact-tax-id" id="contact-tax-id" value="{{ $estimate->contact->tax_id }}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.expiration_date') }} <span>*</span></label>
                            <input id="estimate_expiration_date" name="estimate_expiration_date" class="form-control" required="required" value="{{old('estimate_expiration_date', $estimate->expiration_date)}}" autocomplete="off"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact-phone">{{ __('labels.phone') }}</label>
                            <input type="text" disabled class="form-control" name="contact-phone" id="contact-phone" value="{{ $estimate->contact->phone }}" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="notes">{{ __('labels.notes') }}</label>
                            <textarea class="form-control" name="notes" id="notes" placeholder="{{ __('labels.visible_in_document_printing') }}">{{ old('notes', $estimate->notes) }}</textarea>
                        </div>
                        @if($estimate->currency)
                        <div class="currency_exchange_cnt col-md-6">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    <input type="text" id="exchange-currency" name="exchange_currency" class="form-control"
                                        value="{{old('exchange_currency', $estimate->currency->name.' - '.$estimate->currency->code.' ('.$estimate->currency->symbol.')' )}}"
                                        readonly />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{old('exchange_rate', $estimate->exchange_currency_rate)}}" readonly />
                                    <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $estimate->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($estimate->exchange_currency_rate,6,true) }}</span>
                                    </label>
                                </div>

                            </div>
                        </div>
                        @endif                    
                    </div>
                    <div class="default-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped" id="invoice_items">
                                <thead>
                                    <tr>
                                        <th class="th-item">{{ __('labels.item') }}</th>
                                        <th class="th-reference">{{ __('labels.reference') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <!-- <th class="th-description">{{ __('labels.description') }}</th> -->
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(old('products', [])))
                                        @foreach (old('products', []) as $key => $product)
                                            @include('app.item-row', ['row_number' => $key])
                                        @endforeach
                                    @else
                                        @foreach ($estimate->items as $key => $item)
                                        <tr>
                                            <input type="hidden" name="products[{{$key}}][id]" value="{{$item->id ?: 0}}">
                                            <td class="td-item">
                                                {!! Form::select('products['.$key.'][item]', $inventory_options, old('products[{{$key}}][item]', $item->inventory_id), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$key.'][item]', 'required' => 'required'], $inventory_attributes) !!}
                                            </td>
                                            <td class="td-reference">
                                                <input type="text" placeholder="{{ __('labels.reference') }}" name="products[{{$key}}][reference]" id="products[{{$key}}][reference]" class="form-control" value="{{ old('products')[$key]['reference'] ?: $item->reference }}" />
                                            </td>
                                            <td class="td-price">
                                                <input type="text" placeholder="{{ __('labels.unit_price') }}" name="products[{{$key}}][price]" id="products[{{$key}}][price]" class="form-control" readonly value="{{ old('products')[$key]['price'] ?: $item->price }}" />
                                            </td>
                                            <td class="td-disc" style="text-align: center;">
                                                <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="products[{{$key}}][discount]" id="products[{{$key}}][discount]" class="form-control" value="{{ old('products')[$key]['discount'] ?: $item->discount }}" />
                                            </td>
                                            <td class="td-tax">
                                                {!! Form::select('products['.$key.'][tax]', $tax_options, old('products['.$key.'][tax]', $item->tax_id), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$key.'][tax]'], $tax_attributes) !!}
                                            </td>
                                            <td class="td-tax_amount">
                                                <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="products[{{$key}}][tax_amount]" id="products[{{$key}}][tax_amount]" class="form-control" readonly value="{{ old('products')[$key]['tax_amount'] ?: $item->calculation['taxAmount'] }}" />
                                            </td>
                                            <!-- <td class="td-description">
                                                <textarea  placeholder="{{ __('labels.description') }}" name="products[{{$key}}][description]" id="products[{{$key}}][description]" class="form-control" readonly>{{ old('products')[$key]['description'] ?: $item->inventory->description }}</textarea>
                                            </td> -->
                                            <td class="td-quantity">
                                                <input type="number" placeholder="1" name="products[{{$key}}][quantity]" id="products[{{$key}}][quantity]" class="form-control" min="1" value="{{ old('products')[$key]['quantity'] ?: $item->quantity }}">
                                            </td>
                                            <td class="td-total">
                                                <span id="products[{{$key}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                            </td>
                                            <td>
                                                @if($key != 0)
                                                    <a class="delete-icon remove-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row no-margin">
                        <div class="col-sm-4">
                            <div class="form-group file-input file-input-medium">
                                @if(!Auth::user()->company->signature)
                                    <label for="signature" id="signature-preview-label">
                                        <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                        {{ __('labels.signature') }}
                                    </label>
                                @endif
                                @if(Auth::user()->company->signature)
                                  <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}" alt="{{ Auth::user()->name }}" />
                                @else
                                  <img id="signature-preview" src="" />
                                @endif 
                                <input type="file" id="company-sign" name="signature" class="file-upload-preview" accept="image/*" data-img_id="signature-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="signature-preview-label"/>
                                <a class="upload-button" data-preview_section="company-sign"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                @error('signature')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    @if($estimate->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span>{{ $estimate->currency->code }}</span></th>
                                        <td><span>{{ $estimate->currency->code }}
                                                {{ $estimate->currency->symbol }}</span><span
                                                id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $estimate->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($estimate->exchange_currency_rate,6,true) }}</span>
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('estimates.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/invoices.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush