@extends('layouts.dashboard')

@section('title', 'Withholdings')

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>New Withholding</h2>
        <form method="POST" action="{{ route('with-holdings.store') }}" id="add-edit-with-holdings-form">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Employee <span>*</span></label>
                                {!! Form::select('employee_id', $employee_options, '', ['class'=>"form-control single-search-selection", 'id'=>"employee_id"]) !!}
                                @error('employee')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                           
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="type_account" class="col-sm-12">{{ __('labels.types') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="type-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="type-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="type-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($type_accounts_options as $type_accounts_option)
                                                <a class="dropdown-item" data-value="{{$type_accounts_option->id}}" data-level="{{$type_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$type_accounts_option->custom_name}}</span>
                                                    @if($type_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $type_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="type_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('type_account', null) }}" />
                                </div>
                                @error('type_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Start Date <span>*</span></label>
                                 <input id="payroll_start_date" name="start_date" class="form-control"
                                required="required" value="{{old('payroll_start_date', '')}}"
                                autocomplete="off" />
                                @error('start_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">End Date <span>*</span></label>
                                <input id="payroll_end_date" name="end_date" class="form-control"
                                required="required" value="{{old('end_date', '')}}"
                                autocomplete="off" />
                                @error('end_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Observations </label>
                                <textarea class="form-control" id="observations" name="observation">{{old('observation')}}</textarea>
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Frequency <span>*</span></label>
                                <div class="wrap-two-cell-group">
                                    <div class="left">
                                        {!! Form::select('frequency_type', $frequency_type_option, '', ['class'=>"form-control col-5 single-search-selection", 'id'=>"frequency_type"]) !!}</div>
                                    <div class="right">
                                         <input type="text" class="form-control" name="frequency" id="frequency" value="{{old('frequency')}}">
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="wrap-checkbox-block">
                                @if (old('_token') === null)
                                    <input type="radio" name="recurring_type" id="recurring" value="recurring" checked>
                                @else
                                <input type="radio" name="recurring_type" id="recurring" value="recurring" {{ old("recurring_type") == "recurring" ? "checked" : "" }} >
                                @endif
                                <label for="recurring">Recurring</label>
                                <input type="radio" name="recurring_type" id="on_time" value="on_time" {{ old("recurring_type") == "on_time" ? "checked" : "" }} >
                                <label for="on_time">On Time</label>
                            </div> 
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Amount <span>*</span></label>
                                <input type="text" class="form-control" name="amount" id="contact" value="{{old('amount')}}">
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="income_account" class="col-sm-12">{{ __('labels.income') }} {{ __('labels.account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="income-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="income-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="income-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($income_accounts_options as $income_accounts_option)
                                                <a class="dropdown-item" data-value="{{$income_accounts_option->id}}" data-level="{{$income_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$income_accounts_option->custom_name}}</span>
                                                    @if($income_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $income_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="income_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('income_account', null) }}" />
                                </div>
                                @error('income_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('with-holdings.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
