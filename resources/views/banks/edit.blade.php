@extends('layouts.dashboard')

@section('title', trans('labels.edit') . ' ' . trans('labels.client'))

@section('content')
    <div class="dashboard-right">
        <div class="menu-icon">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div class="invoice-container">
            <h2>{{ __('labels.update') }} {{ titleCase($bank->bank_name) }} {{ __('labels.details') }}</h2>
            {{ Form::open(['method' => 'PUT', 'route' => ['banks.update', $bank], 'id' => 'add-edit-bank-form']) }}
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            {{-- <label for="number">{{ __('labels.identification_type') }}</label>
                            {!! Form::select('identification_type', $identification_type_options, $client->identification_type_id, ['class'=>"form-control single-search-selection", 'id'=>"identification_type"]) !!}
                            @error('identification_type')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror --}}
                            <label for="bank_name">{{ __('labels.bank_name') }} <span>*</span></label>
                            <input id="bank_name" class="form-control" type="text" name="bank_name"
                                placeholder="{{ __('labels.bank_name') }}"
                                value="{{ old('bank_name', $bank->bank_name) }}" autocomplete="off" />
                            @error('bank_name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="bank_date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="bank_date" class="form-control" name="bank_date" required="required"
                                value="{{ old('bank_date', $bank->bank_date) }}" autocomplete="off" />
                                @error('bank_date')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror                        </div>
                        <div class="form-group col-sm-6">
                            <label for="name_of_account">{{ __('labels.account_holder') }} <span>*</span></label>
                            <input id="name_of_account" class="form-control" type="text" name="name_of_account"
                                placeholder="{{ __('labels.account_holder') }}"
                                value="{{ old('name_of_account',$bank->name_of_account) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="account_balance">{{ __('labels.account_balance') }} <span>*</span></label>
                            <input id="account_balance" class="form-control" type="text" name="account_balance"
                                placeholder="{{ __('labels.account_balance') }}"
                                value="{{ old('account_balance',$bank->account_balance) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="account_number">{{ __('labels.account_number') }} <span>*</span></label>
                            <input id="account_number" class="form-control" type="text" name="account_number"
                                placeholder="{{ __('labels.account_number') }}"
                                value="{{ old('account_number',$bank->account_number) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="select_currency">{{ __('labels.select_currency') }} <span>*</span></label>
                            {!! Form::select('currency', $currency_options, $bank->currency, ['class'=>"form-control single-search-selection", 'id'=>"select_currency", 'required' => 'required']) !!}
                            @error('currency')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="reference">{{ __('labels.reference') }} <span>*</span></label>
                            <input id="reference" class="form-control" type="text" name="reference"
                                placeholder="{{ __('labels.reference') }}"
                                value="{{ old('reference',$bank->reference) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bank_address">{{ __('labels.bank_address') }} <span>*</span></label>
                            <input id="bank_address" class="form-control" type="text" name="bank_address"
                                placeholder="{{ __('labels.bank_address') }}"
                                value="{{ old('bank_address',$bank->bank_address) }}" autocomplete="off" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="email_address">{{ __('labels.email_address') }} </label>
                            <input id="email_address" class="form-control" type="text" name="email"
                                placeholder="{{ __('labels.email_address') }}"
                                value="{{ old('email',$bank->email) }}" autocomplete="off" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.phone') }} </label>
                            <input id="phone" class="form-control" type="text" name="phone"
                                placeholder="{{ __('labels.phone') }}"
                                value="{{ old('phone',$bank->phone) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="swift_code">{{ __('labels.swift_code') }} </label>
                            <input id="swift_code" class="form-control" type="text" name="swift_code"
                                placeholder="{{ __('labels.swift_code') }}"
                                value="{{ old('swift_code',$bank->swift_code) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="routing_number">{{ __('labels.routing_number') }}</label>
                            <input id="routing_number" class="form-control" type="text" name="routing_number"
                                placeholder="{{ __('labels.routing_number') }}"
                                value="{{ old('routing_number',$bank->routing_number) }}" autocomplete="off" />
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="intermediary">{{ __('labels.intermediary_bank') }}</label>
                            <input id="intermediary" class="form-control" type="text" name="intermediary"
                                placeholder="{{ __('labels.intermediary_bank') }}"
                                value="{{ old('intermediary',$bank->intermediary) }}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="account_type">{{ __('labels.account_type') }}</label>
                            <select class="form-control single-search-selection" value="{{ old('account_type', null) }}"  name="account_type" id='account_type' required ='required'>
                                <option value="">{{ __('labels.select_account_type')}}</option>
                                <option value="checking" @if ($bank->account_type == "checking") selected @endif>{{ __('labels.checking')}}</option>
                                <option value="saving" @if ($bank->account_type == "saving") selected @endif>{{ __('labels.saving')}}</option>
                              </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="notes">{{ __('labels.notes') }}</label>
                            <textarea placeholder="{{ __('labels.notes') }}" class="form-control" name="notes" id="notes">{{ old('notes', $bank->notes) }}</textarea>
                        </div>



                    </div>
                </div>
            </div>
            <div class="comments-section">
                <div class="form-group popup-btns">
                    <a href="{{ route('banks.index') }}"><button type="button" name="cancel"
                            class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
@endsection
