@extends('layouts.dashboard')

@section('title', trans('labels.banks'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.banks') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.bank') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('import_resource', ['resource' => 'banks']) }}" class="btn-custom outline-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ __('labels.import_from_excel') }}</a>
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('banks.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_bank') }}</a>
        </div>

        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="bank-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.bank_name') }}</th>
                                    <th>{{ __('labels.account_number') }}</th>
                                    <th>{{ __('labels.routing_number') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.account_holder') }}</th>
                                    <th>{{ __('labels.account_balance') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($banks as $bank)
                                <tr>
                                    <td>{{ $bank->bank_name }}</td>
                                    <td>{{ $bank->account_number }}</td>
                                    <td>{{ $bank->routing_number?:'N/A'}}</td>
                                    <td>{{ $bank->reference }}</td>
                                    <td>{{ $bank->name_of_account }}</td>
                                    <td>{{ moneyFormat($bank->account_balance,$bank->currencyInfo->symbol) }}</td>

                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('banks.show', $bank)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('banks.edit', $bank)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-banks-form-' . $bank->id }}" href="{{route('banks.destroy', $bank)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['banks.destroy', $bank], 'id' => "destroy-banks-form-{$bank->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.bank') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('banks.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_bank') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
