@extends('layouts.dashboard')

@section('title', trans('labels.show').' '.trans('labels.bank'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container user-invoice-view-details">
        <a class="arrow-back d-block" href="{{ route('banks.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ titleCase($bank->bank_name) }} Details</h2>
        <div class="default-form view-form-detail">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="number"><b>{{ __('labels.bank_name') }}:</b></label>
                        <span class="value-form">{{ $bank->bank_name }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.date') }}: </b></label>
                        <span class="value-form">{{ $bank->bank_date }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.account_holder') }}: </b></label>
                        <span class="value-form">{{ $bank->name_of_account }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.account_balance') }}: </b></label>
                        <span class="value-form">{{ moneyFormat(($bank->account_balance + $add_amount - $min_amount),$bank->currencyInfo->symbol) }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.account_number') }}: </b></label>
                        <span class="value-form">{{ $bank->account_number }}</span>
                    </div>

                   <!--  <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.currency') }}: </b></label>
                        <span class="value-form">{{ $bank->currency }}</span>
                    </div>
 -->
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.reference') }}: </b></label>
                        <span class="value-form">{{ $bank->reference }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.bank_address') }}: </b></label>
                        <span class="value-form">{{ $bank->bank_address }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.phone') }}: </b></label>
                        <span class="value-form">{{ $bank->phone }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.swift_code') }}: </b></label>
                        <span class="value-form">{{ $bank->swift_code }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.routing_number') }}: </b></label>
                        <span class="value-form">{{ $bank->routing_number }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.intermediary_bank') }}: </b></label>
                        <span class="value-form">{{ $bank->intermediary }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.routing_number') }}: </b></label>
                        <span class="value-form">{{ $bank->routing_number }}</span>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.notes') }}: </b></label>
                        <span class="value-form">{{ $bank->notes }}</span>
                    </div>
            </div>
        </div>

    </div>
    <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#invoice-received-payment">{{ __('labels.received_payments') }}</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoice-accounting">{{ __('labels.expenses') }}</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoice-credit-note-payment">{{ __('labels.pos') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="invoice-received-payment" class="tab-pane generate-invoice fade in active show">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">                           
                                    <table class="table table-striped">
                                        <thead>
                                            <th>{{ __('labels.date') }}</th>
                                            <th>{{ __('labels.cash_receipt') }} #</th>
                                            <th>{{ __('labels.payment_method') }}</th>
                                            <th>{{ __('labels.amount') }}</th>
                                            <th>{{ __('labels.annotation') }}</th>
                                        </thead>
                                        <tbody>
                                        @foreach($bank->transaction as $transaction)
                                        @if($transaction->type == 'in')
                                        <tr>
                                            <td>{{ $transaction->start_date }}</td>
                                            <td>{{ $transaction->receipt_number }}</td>
                                            <td>{{trans('constants'.'.'.$transaction->method)}}</td>
                                            <td>{{moneyFormat($transaction->amount)}}</td>
                                            <td>{{$transaction->annotation}}</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoice-accounting" class="invoice-accounting-tab-pane tab-pane fade generate-invoice">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    
                                    <!-- <span class="accounting-notification">Visualiza el movimiento contable de este comprobante. Puedes personalizar las cuentas contables y sus códigos aquí.</span> -->
                                    <div class="table-wrap">
                                        <table id="accounting-table" class="table table-striped">
                                        
                                        <thead>
                                            <th>{{ __('labels.date') }}</th>
                                            <th>{{ __('labels.cash_receipt') }} #</th>
                                            <th>{{ __('labels.payment_method') }}</th>
                                            <th>{{ __('labels.amount') }}</th>
                                            <th>{{ __('labels.annotation') }}</th>
                                        </thead>
                                        <tbody>
                                        @foreach($bank->transaction as $transaction)
                                        @if($transaction->type == 'out')
                                        <tr>
                                            <td>{{ $transaction->start_date }}</td>
                                            <td>{{ $transaction->receipt_number }}</td>
                                            <td>{{trans('constants'.'.'.$transaction->method)}}</td>
                                            <td>{{moneyFormat($transaction->amount)}}</td>
                                            <td>{{$transaction->annotation}}</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoice-credit-note-payment" class="tab-pane generate-invoice fade">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">                      
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>{{ __('labels.sr_number') }}</th>
                                                <th>{{ __('labels.date') }}</th>
                                                <th>{{ __('labels.amount') }}</th>
                                                <th>{{ __('labels.remaining') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
