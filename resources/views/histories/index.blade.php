@extends('layouts.dashboard')

@section('title', trans('labels.histories'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.log') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.histories') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            {{-- <a href="{{ route('histories.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.histories') }}</a> --}}
        </div>
        <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
            {{-- <span>{{ __('labels.filter') }}: <input id="history-filter" type="text" autocomplete="off"  value=" "></span>
            <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
            <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span> --}}
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="history-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.user') }}</th>
                                    <th>{{ __('labels.method') }}</th>
                                    <th>{{ __('labels.record').' '. __('labels.number')}}</th>
                                    <th>{{ __('labels.record').' '. __('labels.type')}}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    {{-- <th>{{ __('labels.actions') }}</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($histories as $estimate)
                                <tr>
                                    <td>{{ $estimate->id }}</td>
                                    <td>{{ $estimate->UserName }}</td>
                                    <td>{{ $estimate->action_type }}</td>
                                    <td>{{ $estimate->record_id }}</td>
                                    <td>{{ $estimate->record_type }}</td>
                                    <td>{{ dateFormat($estimate->created_at) }}</td>
                                    {{-- <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('estimates.show', $estimate)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('estimates.edit', $estimate)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-estimate-form-' . $estimate->id }}" href="{{route('estimates.destroy', $estimate)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['estimates.destroy', $estimate], 'id' => "destroy-estimate-form-{$estimate->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </div>
                                    </td> --}}
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.histories') }}!</p></div>
                                        
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
