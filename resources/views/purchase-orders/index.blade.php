@extends('layouts.dashboard')

@section('title', trans('labels.purchase_order'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.purchase_order') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">If you need help creating a purchase order.</span>
        </div>

        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('purchase-orders.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new') }} {{ __('labels.purchase_order') }}</a>
        </div>

        <div class="cal-filter-cnt mt-2 cal-filter-cust-btns" id="dates-filters" style="display:none">
            <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
            <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="purchase-order-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.expiration_date') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($purchaseOrders as $purchaseOrder)
                                <tr>
                                    <td>{{ $purchaseOrder->contact->name }}</td>
                                    <td>{{ dateFormat($purchaseOrder->start_date) }}</td>
                                    <td>{{ dateFormat($purchaseOrder->expiration_date) }}</td>
                                    <td>{{ moneyFormat($purchaseOrder->calculation['total']) }}</td>
                                    <td>{{ $purchaseOrder->status }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('purchase-orders.show', $purchaseOrder)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('purchase-orders.edit', $purchaseOrder)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-purchase-order-form-' . $purchaseOrder->id }}" href="{{route('purchase-orders.destroy', $purchaseOrder)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['purchase-orders.destroy', $purchaseOrder], 'id' => "destroy-purchase-order-form-{$purchaseOrder->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created') }} {{ __('labels.purchase_order') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('purchase-orders.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new') }} {{ __('labels.purchase_order') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
