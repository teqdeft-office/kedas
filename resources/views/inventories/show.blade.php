@extends('layouts.dashboard')

@section('title', trans('labels.show').' '.trans('labels.products_and_services'))

@section('content')

@inject('controller', 'App\Http\Controllers\Controller')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('inventories.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.products_and_services').' '.__('labels.details') }}</h2>
        <div class="default-form view-form-detail item-detail-view view-details-product-ser">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="col-lg-8 view-details-product-cell">
                        <div class="form-outer">
                    <div class="form-group">
                        <label for="name"><b>{{ __('labels.name') }} </b></label>
                        <span class="value-form">{{ showIfAvailable($inventory->name) }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="reference"><b>{{ __('labels.reference') }}</b></label>
                        <span class="value-form">{{ showIfAvailable($inventory->reference) }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="sale_price"><b>{{ __('labels.sale_price') }} </b></label>
                        <span class="value-form">{{ moneyFormat($inventory->sale_price) }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="tax"><b>{{ __('labels.tax') }} </b></label>
                        <span class="value-form">{{ $inventory->tax ? showIfAvailable($inventory->tax->formatted_name, 'None (0%)') : 'None (0%)' }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="type_of_item"><b>{{ __('labels.type_of_item') }}</b></label>
                        <span class="value-form">{{ showIfAvailable(titleCase($inventory->type_of_item)) }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="item_category"><b>{{ __('labels.item_category') }} </b></label>
                        <span class="value-form">{{ $inventory->itemCategory ? showIfAvailable($inventory->itemCategory->name) : 'N/A' }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="sku"><b>{{ __('labels.sku') }}</b></label>
                        <span class="value-form">{{ showIfAvailable($inventory->sku) }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="barcode"><b>{{ __('labels.barcode') }}</b></label>
                        <span class="value-form">{{ showIfAvailable($inventory->barcode) }}</span>
                    </div>
                </div>


                @if($inventory->type != $controller::INVENTORY)
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="income_account"><b>{{ __('labels.income_account') }} </b></label>
                            <span class="value-form">{{ $inventory->income_account ? $inventory->incomeAccount->name : 'N/A' }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="expense_account"><b>{{ __('labels.expense_account') }} </b></label>
                            <span class="value-form">{{ $inventory->expense_account ? $inventory->expenseAccount->name : 'N/A' }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="cost"><b>{{ __('labels.cost') }} </b></label>
                            <span class="value-form">{{ $inventory->cost ? moneyFormat($inventory->cost) : showIfAvailable($inventory->cost) }}</span>
                        </div>
                    </div>
                @endif

                @if($inventory->type == $controller::INVENTORY)
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="initial_quantity"><b>{{ __('labels.initial_quantity') }} </b></label>
                            <span class="value-form">{{ $inventory->init_quantity }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="current_quantity"><b>{{ __('labels.current_quantity') }} </b></label>
                            <span class="value-form">{{ $inventory->quantity }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="unit_cost"><b>{{ __('labels.unit_cost') }} </b></label>
                            <span class="value-form">{{ $inventory->unit_cost ? moneyFormat($inventory->unit_cost) : 'N/A' }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="asset_account"><b>{{ __('labels.asset_account') }} </b></label>
                            <span class="value-form">{{ $inventory->asset_account ? $inventory->assetAccount->name : 'N/A' }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="income_account"><b>{{ __('labels.income_account') }} </b></label>
                            <span class="value-form">{{ $inventory->income_account ? $inventory->incomeAccount->name : 'N/A' }}</span>
                        </div>
                    </div>
                    <div class="form-outer">
                        <div class="form-group">
                            <label for="expense_account"><b>{{ __('labels.expense_account') }} </b></label>
                            <span class="value-form">{{ $inventory->expense_account ? $inventory->expenseAccount->name : 'N/A' }}</span>
                        </div>
                    </div>
                @endif
                <div class="form-outer">
                    <div class="form-group">
                        <label for="description"><b>{{ __('labels.description') }} </b></label>
                        <span class="value-form">{{ showIfAvailable($inventory->description) }}</span>
                    </div>
                </div>
                @if($inventory->track_inventory)
                <p>{{ __('labels.inventory_tracked') }}</p>
                @endif
                </div>
                <div class="col-lg-4 view-details-img-cell">
                    <div class="form-group">
                        @if($inventory->image_url)
                          <img id="item-logo-preview" src="{{ asset($inventory->image_url) }}" alt="{{ $inventory->name }}" />
                          @else
                          <img src="{{ asset('images/dumy-item.png') }}">
                        @endif
                    </div>
                </div>
            </div>
            </div>
            
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
