@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.products_and_services'))

@section('content')

@inject('controller', 'App\Http\Controllers\Controller')

<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.product_and_service') }}</h2>
        <div class="default-form add-product-form invoice-details-form">
            <form method="POST" action="{{ route('inventories.store', ['type' => $type]) }}" id="add-edit-inventories-form" enctype="multipart/form-data">
                @csrf
                <div class="generate-invoice">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12">{{ __('labels.name') }} <span>*</span></label>
                                <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name') }}" autocomplete="off" />
                                @error('name')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="sale_price" class="col-sm-12">{{ __('labels.sale_price') }} <span>*</span></label>
                                <input class="form-control" type="text" name="sale_price" placeholder="{{ __('labels.sale_price') }}" required value="{{ old('sale_price') }}" autocomplete="off" />
                                @error('sale_price')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div> 
                            <div class="form-group">
                                <label for="sku" class="col-sm-12">{{ __('labels.sku') }}</label>
                                <input class="form-control" type="text" name="sku" placeholder="{{ __('labels.sku') }}" value="{{ old('sku') }}" autocomplete="off" />
                                @error('sku')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div> 
                            <div class="form-group create-new">
                                <label for="tax" class="col-sm-12 creat-nw-cat">{{ __('labels.tax') }} <span>*</span> <a target="_blank" href="{{ route('taxes.create') }}">{{ __('labels.create').' '.__('labels.new').' '.__('labels.tax') }}</a></label>
                                {!! Form::select('tax_id', $tax_options, '', ['class'=>"form-control single-search-selection", 'id'=>"tax", 'required' => 'required'], $tax_attributes) !!}
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-12">{{ __('labels.description') }}</label>
                                <textarea class="form-control" name="description" placeholder="{{ __('labels.description') }}" autocomplete="off">{{ old('description', null) }}</textarea>
                                @error('description')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label for="type_of_item" class="col-sm-12">{{ __('labels.type_of_item') }} <span>*</span></label>
                                <select class="form-control" name="type_of_item" id="type_of_item" required>
                                    <optgroup label="{{ __('labels.simple') }}">
                                        <option value="simple">{{ __('labels.simple_desc') }}.</option>
                                    </optgroup>
                                    <optgroup label="{{ __('labels.kit') }}">
                                        <option value="kit">{{ __('labels.kit_desc') }}.</option>
                                    </optgroup>
                                    <optgroup label="{{ __('labels.item_with_variants') }}">
                                        <option value="item_with_variants">{{ __('labels.item_with_variants_desc') }}.</option>
                                    </optgroup>
                                </select>
                            </div> --}}                      
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="reference" class="col-sm-12">{{ __('labels.reference') }}</label>
                                <input class="form-control" type="text" name="reference" placeholder="{{ __('labels.reference') }}" value="{{ old('reference') }}" autocomplete="off" />
                                @error('reference')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            @if($type != $controller::INVENTORY)
                            <div class="form-group">
                                <label for="cost" class="col-sm-12">{{ __('labels.cost') }}</label>
                                <input class="form-control" type="text" name="cost" placeholder="{{ __('labels.cost') }}" value="{{ old('cost') }}" autocomplete="off" />
                                @error('cost')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div> 
                            @endif
                            <div class="form-group">
                                <label for="barcode" class="col-sm-12">{{ __('labels.barcode') }} </label>
                                <input type="text" class="form-control" name="barcode" id="barcode" placeholder="{{ __('labels.barcode') }}" value="{{ old('barcode', null) }}" autocomplete="off">
                            </div>
                            <div class="form-group create-new">
                                <label for="item_category" class="col-sm-12 creat-nw-cat">{{ __('labels.item_category') }} <a target="_blank" href="{{ route('item-categories.create') }}">{{ __('labels.create_new_category') }}</a></label>
                                {!! Form::select('item_category_id', $item_category_options, '', ['class'=>"form-control single-search-selection", 'id'=>"item_category"]) !!}
                            </div>
                            <div class="form-group">
                                <label for="unit" class="col-sm-12">{{ __('labels.unit') }}</label>
                                {!! Form::select('unit', $unit_option, '', ['class'=>"form-control single-search-selection", 'id'=>"unit", 'required' => 'required']) !!}
                            </div>
                            @if($type == $controller::INVENTORY)
                            <div class="form-group">
                                <label for="membership" class="col-sm-12">{{ __('labels.membership') }}</label>
                                {!! Form::select('memberships', $membership_options, old('memberships', []), ['class'=>"form-control multiple-search-selection", 'multiple'=>'multiple', 'name'=>'memberships[]']) !!}
                                @error('memberships')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            @endif
                        </div>
                        <div class="col-lg-4 col-md-6 prduct-pic">
                            <div class="form-group file-input file-input-large">
                                <label for="image" id="image-preview-label">
                                    <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                    {{ __('labels.image_of_item') }}
                                </label>
                                <img src="" id="image-preview" />                            
                                <input type="file" id="inventory-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label">
                                <a class="upload-button" data-preview_section="inventory-image"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('image')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <p>{{ __('labels.total').' '. __('labels.price') }}</p>
                                <h3 id="item_total">{{ moneyFormat() }}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @if($type == $controller::INVENTORY)
                        <div class="col-lg-4 col-md-6 mt-4">
                            <div class="form-group" id="product_assets_accounts">
                                <label for="assets_account" class="col-sm-12">{{ __('labels.assets') }} {{ __('labels.account') }} * </label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="assets-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="assets-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="assets-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($assets_accounts_options as $assets_accounts_option)
                                                <a class="dropdown-item" data-value="{{$assets_accounts_option->id}}" data-level="{{$assets_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$assets_accounts_option->custom_name}}</span>
                                                    @if($assets_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $assets_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="asset_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('asset_account', null) }}" />
                                </div>
                                @error('asset_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-4 col-md-6 mt-4">
                            @if($type != $controller::INVENTORY)
                            <div class="form-group">
                                <div class="checkbox">
                                    {{ Form::checkbox('is_product_sale', old('is_product_sale', 1), old('is_product_sale', true), array('id'=>'is_product_sale')) }}
                                    <label for="is_product_sale">I sale this product/service</label>
                                </div>
                            </div>
                            @endif

                            <div class="form-group" id="product_for_sale_accounts" style="display: {{ $type == $controller::INVENTORY ? 'block' : 'none' }}">
                                <label for="income_account" class="col-sm-12">{{ __('labels.income') }} {{ __('labels.account') }} </label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="income-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="income-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="income-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($income_accounts_options as $income_accounts_option)
                                                <a class="dropdown-item" data-value="{{$income_accounts_option->id}}" data-level="{{$income_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$income_accounts_option->custom_name}}</span>
                                                    @if($income_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $income_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="income_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('income_account', null) }}" />
                                </div>
                                @error('income_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4">
                            @if($type != $controller::INVENTORY)
                            <div class="form-group">
                                <div class="checkbox">
                                    {{ Form::checkbox('is_product_purchase', old('is_product_purchase', 1), old('is_product_purchase', false), array('id'=>'is_product_purchase')) }}
                                    <label for="is_product_purchase">I purchase this product/service</label>
                                </div>
                            </div>
                            @endif

                            <div class="form-group" id="product_for_purchase_accounts" style="display: {{ $type == $controller::INVENTORY ? 'block' : 'none' }}">
                                <label for="type_of_item" class="col-sm-12">{{ __('labels.expenses') }} {{ __('labels.account') }} </label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="expense-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="expense-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="expense-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($expense_accounts_options as $expense_accounts_option)
                                                <a class="dropdown-item" data-value="{{$expense_accounts_option->id}}" data-level="{{$expense_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$expense_accounts_option->custom_name}}</span>
                                                    @if($expense_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $expense_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="expense_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('expense_account', null) }}" />
                                </div>
                                @error('expense_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    @if($type == $controller::INVENTORY)
                    {{-- <div class="form-group">
                        <div class="checkbox">
                            {{ Form::checkbox('track_inventory', old('track_inventory', 1), old('track_inventory'), array('id'=>'track_inventory')) }}
                            <label for="track_inventory">{{ __('labels.track_inventory') }}</label>
                        </div>
                    </div> --}}
                    <div class="row inventory-field-container" id="inventory_details">
                        <div class="col-sm-12">
                            <h3>{{ __('labels.inventory').' '. __('labels.details') }}</h3>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="init_quantity_date">{{ __('labels.date') }} <span>*</span></label>
                                <input name="init_quantity_date" id="init_quantity_date" class="form-control" value="{{ old('init_quantity_date', getTodayDate()) }}" required="required" autocomplete="off" />
                                @error('init_quantity_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="quantity">{{ __('labels.initial_quantity') }} <span>*</span></label>
                                <input type="number" min="0" step="1" name="quantity" id="quantity" class="form-control" value="{{ old('quantity', 0) }}" required="required">
                                @error('quantity')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="unit_cost">{{ __('labels.unit_cost') }} <span>*</span></label>
                                <input type="text" name="unit_cost" id="unit_cost" class="form-control" value="{{ old('unit_cost', null) }}" required="required">
                                @error('unit_cost')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="expiring_date">{{ __('labels.expiring_date') }} <span>*</span></label>
                                <input name="expiring_date" id="expiring_date" class="form-control" value="{{ old('expiring_date', getTodayDate()) }}" required="required" autocomplete="off" />
                                @error('expiring_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="form-group popup-btns">                    
                    <a href="{{ route('inventories.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
