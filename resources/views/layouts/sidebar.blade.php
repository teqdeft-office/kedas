<div class="side-blue-sec">
    <div id="accordion">
        <div class="card">
            <div class="card-header">
                <a id="dashboard_link" class="" href="{{ route('dashboard') }}">
                <span><img src="{{ asset('images/dashboard-icon.png') }}"></span>{{ __('labels.dashboard') }}
                </a>
            </div>
        </div>

        {{-- @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage sales')) --}}
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage sales') || Auth::user()->can('manage invoices')  || Auth::user()->can('manage recurring invoices')
        || Auth::user()->can('manage received invoices') || Auth::user()->can('manage credit notes') || Auth::user()->can('manage estimates')
        || Auth::user()->can('manage pos'))
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#sales">
                <span><img src="{{ asset('images/sales-icon.png') }}"></span>{{ __('labels.sales') }}
                </a>
            </div>
            <div id="sales" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage invoices') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('invoices.index', ['type' => 'sale']) }}" data-resource_slug="invoices">{{ __('labels.invoices') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage recurring invoices') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('invoices.index', ['type' => 'recurring']) }}" data-resource_slug="invoices-recurring">{{ __('labels.recurring_invoices') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage received invoices') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('transactions.index', ['type' => 'in']) }}" data-resource_slug="transactions-in">{{ __('labels.received_payments') }}</a></li>
                        @endif 
                        <li><a href="{{ route('receivable.index') }}">{{ __('labels.account_receivable') }}</a></li>
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage credit notes') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('credit-notes.index') }}" data-resource_slug="credit-notes">{{ __('labels.credit_notes') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage estimates') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('estimates.index') }}" data-resource_slug="estimates">{{ __('labels.estimates') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage remissions') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('remissions.index') }}" data-resource_slug="remissions">{{ __('labels.remissions') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage pos') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('pos.index') }}" data-resource_slug="pos" target="_blank">{{ __('labels.pos') }}</a></li>
                        @endif
                        <li><a href="{{ route('production.index') }}" data-resource_slug="production">{{ __('labels.order_production') }}</a></li>
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage table') || Auth::user()->can('manage sales'))
                        <li><a href="{{ route('restaurant-table.index') }}" data-resource_slug="restaurant-table">{{ __('labels.tables') }}</a></li>
                        @endif
                        <li><a href="{{ route('shipping.index') }}" data-resource_slug="shipping">{{ __('labels.shipping') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

        {{-- @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage expenses')) --}}
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage expenses') || Auth::user()->can('manage transactions-out')
        || Auth::user()->can('manage invoices-supplier') || Auth::user()->can('manage transactions-recurring') || Auth::user()->can('manage debit-notes')
        || Auth::user()->can('manage purchase_orders') || Auth::user()->can('manage payroll'))
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#expenses">
                <span><img src="{{ asset('images/cart.png') }}"></span>{{ __('labels.expenses') }}
                </a>
            </div>
            <div id="expenses" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage transactions-out') || Auth::user()->can('manage expenses'))
                        <li><a href="{{ route('transactions.index', ['type' => 'out']) }}" data-resource_slug="transactions-out">{{ __('labels.payments') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage invoices-supplier') || Auth::user()->can('manage expenses'))
                        <li><a href="{{ route('invoices.index', ['type' => 'supplier']) }}" data-resource_slug="invoices-supplier">{{ __('labels.supplier_invoice') }}</a></li>
                        @endif
                        <!-- <li><a href="javascript:void(0);">Quotations</a></li>
                        <li><a href="javascript:void(0);">Provider List</a></li> -->
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage transactions-recurring') || Auth::user()->can('manage expenses'))
                        <li><a href="{{ route('transactions.index', ['type' => 'recurring']) }}" data-resource_slug="transactions-recurring">{{ __('labels.recurring_payments') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage debit-notes') || Auth::user()->can('manage expenses'))
                        <li><a href="{{ route('debit-notes.index') }}" data-resource_slug="debit-notes">{{ __('labels.debit_notes') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() ||Auth::user()->can('manage purchase orders') || Auth::user()->can('manage expenses'))
                        <li><a href="{{ route('purchase-orders.index') }}" data-resource_slug="purchase-orders">{{ __('labels.purchase_orders') }}</a></li>
                        @endif
                        <li><a href="{{ route('coming-soon') }}">{{ __('labels.payroll') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

        {{-- @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage contacts')) --}}
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage contacts') || Auth::user()->can('manage clients')
        || Auth::user()->can('manage suppliers') || Auth::user()->can('manage employees'))
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#contacts">
                <span><img src="{{ asset('images/contact-icon.png') }}"></span>{{ __('labels.contacts') }}
                </a>
            </div>
            <div id="contacts" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage clients') || Auth::user()->can('manage contacts'))
                        <li><a href="{{ route('clients.index') }}" data-resource_slug="clients">{{ __('labels.clients') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage suppliers') || Auth::user()->can('manage contacts'))
                        <li><a href="{{ route('suppliers.index') }}" data-resource_slug="suppliers">{{ __('labels.suppliers') }}</a></li>
                        @endif
                        <li><a href="{{ route('employees.index') }}" data-resource_slug="employees">{{ __('labels.employees') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif
        
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage payroll'))
        <div class="card" style="display: {{ env('APP_ENV') == 'production' ? 'none' : 'block' }};">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#payroll">
                    <span><img src="{{ asset('images/sales-icon.png') }}"></span>PAYROLL
                </a>
            </div>
            <div id="payroll" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('benefits.index') }}" data-resource_slug="benefits">Benefits</a></li>
                        <li><a href="{{ route('with-holdings.index') }}" data-resource_slug="with-holdings">Withholdings</a></li>
                        <li><a href="{{ route('discounts.index') }}" data-resource_slug="discounts">Discounts</a></li>
                        <li><a href="{{ route('extra-hours.index') }}" data-resource_slug="extra-hours">Extra Hours</a></li>
                        <li><a href="{{ route('leaves.index') }}" data-resource_slug="leaves">{{ __('labels.leaves') }}</a></li>
                     <!--    <li><a href="" data-resource_slug="commissions">Commissions</a></li> -->
                        <li><a href="{{ route('coming-soon') }}" data-resource_slug="unemployment">Unemployment</a></li>
                        <li><a href="{{ route('payroll-reports.index') }}" data-resource_slug="payroll-reports">Payroll Reports</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

        {{-- @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage inventories')) --}}
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner()  || Auth::user()->can('manage inventories')
        || Auth::user()->can('manage inventory-adjustments') || Auth::user()->can('manage taxes') || Auth::user()->can('manage item-categories') )
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#inventory">
                    <span><img src="{{ asset('images/inventry-icon.png') }}"></span>{{ __('labels.inventory') }}
                </a>
            </div>
            <div id="inventory" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage inventories'))
                        <li><a href="{{ route('inventories.index') }}" data-resource_slug="inventories">{{ __('labels.products_services') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage inventory-adjustments'))
                        <li><a href="{{ route('inventory-adjustments.index') }}" data-resource_slug="inventory-adjustments">{{ __('labels.inventory_adjustments') }}</a></li>
                        @endif
                        <!-- <li><a href="javascript:void(0);">Inventory Value</a></li>
                        <li><a href="javascript:void(0);">Item Management</a></li>
                        <li><a href="javascript:void(0);">Find Order</a></li>
                        <li><a href="javascript:void(0);">Price Lists</a></li>
                        <li><a href="javascript:void(0);">Warehouse</a></li> -->
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage taxes'))
                        <li><a href="{{ route('taxes.index') }}" data-resource_slug="taxes">{{ __('labels.tax') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage item-categories'))
                        <li><a href="{{ route('item-categories.index') }}" data-resource_slug="item-categories">{{ __('labels.item_categories') }}</a></li>
                        @endif
                        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage unit'))
                        <li><a href="{{ route('units.index') }}" data-resource_slug="units">{{ __('labels.measurement_unit') }}</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @endif
        
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner() || Auth::user()->can('manage accounts'))
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#accounting">
                <span><img src="{{ asset('images/account-btn.png') }}"></span>{{ __('labels.accounting') }}
                </a>
            </div>
            <div id="accounting" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('accounting.charts') }}" data-resource_slug="chart-of-accounts">{{ __('labels.chart_of_accounts') }}</a></li>
                        <li><a href="{{ route('accounting.adjustments.index') }}" data-resource_slug="account-adjustments">{{ __('labels.accounting_adjustments') }}</a></li>
                        <li><a href="{{ route('accounting.journals') }}" data-resource_slug="account-journals">{{ __('labels.accounting_journals') }}</a></li>
                        <li><a href="{{ route('accounting.reports') }}" data-resource_slug="account-reports">{{ __('labels.accounting_reports') }}</a></li>

                        @env(['production'])
                        <li><a href="{{ route('coming-soon') }}">{{ __('labels.fixed_assets') }}</a></li>
                        @else
                        {{-- <li><a href="{{ route('accounting.general-reports') }}" data-resource_slug="general-reports">{{ __('labels.general_reports') }}</a></li> --}}
                        <li><a href="{{ route('fixed-assets.index') }}" data-resource_slug="fixed-assets">{{ __('labels.fixed_assets') }}</a></li>
                        @endenv
                        {{-- <li><a href="{{ route('banks.index') }}" data-resource_slug="banks">{{ __('labels.banks') }}</a></li> --}}
                        <li><a href="{{ route('coming-soon') }}">{{ __('labels.budgeting') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif
        
        {{-- only compay owner can manage settings and user management --}}
        @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner())
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#setting">
                <span><img src="{{ asset('images/setting-icon.png') }}"></span>{{ __('labels.settings') }}
                </a>
            </div>
            <div id="setting" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('users.edit') }}" data-resource_slug="settings-user">{{ __('labels.user_profile') }}</a></li>
                        <li><a href="{{ route('company.edit') }}" data-resource_slug="settings-company">{{ __('labels.company_profile') }}</a></li>
                        <li><a href="{{ route('ncfs.index') }}" data-resource_slug="ncfs">{{ __('labels.ncfs') }}</a></li>
                        <li><a href="{{ route('currency-exchange-rates.index') }}" data-resource_slug="currency-exchange-rates">{{ __('labels.exchange_rate') }}</a></li>
                        <li><a href="{{ route('memberships.index') }}" data-resource_slug="memberships">{{ __('labels.memberships') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#users_management">
                <span><img src="{{ asset('images/user-management-icon.png') }}"></span>{{ __('labels.users_management') }}
                </a>
            </div>
            <div id="users_management" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('user-management.users') }}" data-resource_slug="users-list">{{ __('labels.users') }}</a></li>
                        <li><a href="{{ route('roles.index') }}" data-resource_slug="roles-list">{{ __('labels.roles') }}</a></li>
                        <li><a href="{{ route('history.index') }}" data-resource_slug="history-list">{{ __('labels.log') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

        <!-- <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#employees">
                <span><img src="{{ asset('images/contact-icon.png') }}"></span>{{ __('labels.employees') }}
                </a>
            </div>
            
            <div id="employees" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('departments.index') }}" data-resource_slug="departments">{{ __('labels.departments') }}</a></li>
                        <li><a href="{{ route('designations.index') }}" data-resource_slug="designations">{{ __('labels.designations') }}</a></li>
                        <li><a href="{{ route('insurance-brands.index') }}" data-resource_slug="insurance-brands">{{ __('labels.insurance_brands') }}</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- <div class="card">
            <div class="card-header">
                <a  data-toggle="collapse" href="">
                <span><img src="{{ asset('images/bank-icon.png') }}"></span>Bank Details
                </a>
            </div>
        </div> -->
        <!-- <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#income">
                <span><img src="{{ asset('images/income-btn.png') }}"></span>Income
                </a>
            </div>
            <div id="income" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="javascript:void(0);">Income Receipts</a></li>
                        <li><a href="javascript:void(0);">Income Summary</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <a data-toggle="collapse" href="">
                <span><img src="{{ asset('images/report-icon.png') }}"></span>reports
                </a>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <a data-toggle="collapse" href="">
                <span><img src="{{ asset('images/pos-icon.png') }}"></span>pos
                </a>
            </div>
        </div> -->
    </div>
</div>