<header>
    <div class="header-left">
        <div class="logo-sec">
            <a href="{{ route('dashboard') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
        </div>
        <!-- <div class="search-sec">
            <form>
                <ul>
                    <li>
                        <input type="text" placeholder="Search">
                        <input type="submit">
                    </li>
                </ul>
            </form>
        </div> -->
        @if (Auth::user() && Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner())
        <div class="red-sec">            
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> {{ __('labels.create_a_new') }}</a>
            <div class="dropdown-menu">
                <ul>
                    <li><a href="{{ route('estimates.create') }}">{{ __('labels.estimate') }}</a></li>
                    <li><a href="{{ route('invoices.create', ['type' => 'sale']) }}">{{ __('labels.invoices') }}</a></li>
                    <!-- <li><a href="{{ route('invoices.create', ['type' => 'recurring']) }}">{{ __('labels.recurring_invoices') }}</a></li> -->
                    <li><a href="{{ route('invoices.create', ['type' => 'supplier']) }}">{{ __('labels.supplier_invoice') }}</a></li>
                    <li><a href="{{ route('transactions.index', ['type' => 'in']) }}">{{ __('labels.received_payments') }}</a></li>
                    <li><a href="{{ route('clients.create') }}">{{ __('labels.client') }}</a></li>
                    <li><a href="{{ route('suppliers.create') }}">{{ __('labels.supplier') }}</a></li>
                    <li><a href="{{ route('inventories.create') }}">{{ __('labels.products_services') }}</a></li>
                </ul>
            </div>
        </div>
        @endif
    </div>
    <div class="header-right">
        <div class="help-center">
            <ul>
                @include('includes.locale-selector')
                <!-- <li><span><img src="{{ asset('images/mail-icon.png') }}"></span><a href="javascript:void(0);">FAQ & Email Support</a></li> -->
                <li>
                    <div class="wh-chat-btn">
                        <a target="_blank" href="https://wa.me/+18492697581">
                            <img src="{{ asset('images/whatsup-icon.png')}}" alt="WA">
                            <span>
                                {{ __('labels.whatsapp_btn_txt') }}
                                {{-- Need help? Chat with us --}}
                            </span>
                        </a>
                    </div>
    
                    <div class="wh-chat-btn wa-float">
                        <a target="_blank" href="https://wa.me/+18492697581">
                            <img src="{{ asset('images/whatsup-icon.png')}}" alt="WA">
                            <span>
                                {{ __('labels.whatsapp_btn_txt') }}
                            </span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="john-sec">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
            <span>
                @if(Auth::user()->company && Auth::user()->company->logo)
                  <img src="{{ asset(Auth::user()->company->logo) }}" alt="{{ Auth::user()->name }}" />
                @else
                  <img src="{{ asset('images/john-sec.png') }}" alt="{{ Auth::user()->name }}" />
                @endif 
                
            </span>
            <span class="username">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu">
                <ul>
                    <li class="user-name-li"><a href="javascript:void(0);"><span>{{ Auth::user()->name }}</span></a></li>
                    <li><a href="javascript:void(0);">{{ Auth::user()->email }}</a></li>
                    @if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner())
                    <li><a href="{{ route('users.edit') }}">{{ __('labels.settings') }}</a></li>
                    @endif
                    <li class="signout"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('labels.sign_out') }}</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</header>