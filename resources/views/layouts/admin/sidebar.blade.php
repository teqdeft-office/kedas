<div class="side-blue-sec">
    <div id="accordion">
        <div class="card">
            <div class="card-header">
                <a id="dashboard_link" class="" href="{{ route('admin-dashboard') }}">
                <span><img src="{{ asset('images/dashboard-icon.png') }}"></span>{{ __('labels.dashboard') }}
                </a>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#setting">
                <span><img src="{{ asset('images/setting-icon.png') }}"></span>{{ __('labels.settings') }}
                </a>
            </div>
            <div id="setting" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('admin.edit') }}" data-resource_slug="settings-user">{{ __('labels.user_profile') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#users_management">
                <span><img src="{{ asset('images/user-management-icon.png') }}"></span>{{ __('labels.users_management') }}
                </a>
            </div>
            <div id="users_management" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('admin.users') }}" data-resource_slug="users-list">{{ __('labels.users') }}</a></li>
                        <li><a href="{{ route('admin.activity_logs') }}" data-resource_slug="activity-logs">{{ __('labels.activity_logs') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#content_management">
                <span><img src="{{ asset('images/content-management-icon.png') }}"></span>{{ __('labels.content_management') }}
                </a>
            </div>
            <div id="content_management" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                        <li><a href="{{ route('admin.countries') }}" data-resource_slug="countries-list">{{ __('labels.countries') }}</a></li>
                        <li><a href="{{ route('admin.sectors') }}" data-resource_slug="sectors-list">{{ __('labels.sectors') }}</a></li>
                        <li><a href="{{ route('admin.currencies') }}" data-resource_slug="currencies-list">{{ __('labels.currency') }}</a></li>
                        <li><a href="{{ route('admin.identification_types') }}" data-resource_slug="identification_types-list">{{ __('labels.identification_types') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>