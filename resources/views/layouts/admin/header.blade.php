<header>
    <div class="header-left">
        <div class="logo-sec">
            <a href="{{ route('admin-dashboard') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
        </div>
    </div>
    <div class="header-right">
        <div class="help-center">
            <ul>
                @include('includes.locale-selector')
            </ul>
        </div>
        <div class="john-sec">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
            <span>
                <img src="{{ asset('images/john-sec.png') }}" alt="{{ Auth::user()->name }}" />                
            </span>
            <span class="username">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu">
                <ul>
                    <li class="user-name-li"><a href="javascript:void(0);"><span>{{ Auth::user()->name }}</span></a></li>
                    <li><a href="javascript:void(0);">{{ Auth::user()->email }}</a></li>
                    <li><a href="{{ route('admin.edit') }}">{{ __('labels.settings') }}</a></li>
                    <li class="signout"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('labels.sign_out') }}</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</header>