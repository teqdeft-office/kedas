@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.tax'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit').' '.__('labels.tax') }}</h2>
        {{ Form::open(array('method'=>'PUT','route' => ['taxes.update', $tax], 'id' => 'add-edit-tax-form')) }}
            <div class="default-form add-new-contact-form edit-tax-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.tax').' '.__('labels.name') }}" required value="{{ old('name', $tax->name) }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="percentage">{{ __('labels.percentage') }} <span>*</span></label>
                            <input class="form-control" type="text" name="percentage" placeholder="{{ __('labels.percentage') }}(%)" required value="{{ old('percentage', $tax->percentage) }}" autocomplete="off" {{ $resource_associated ? "readonly" : ""}} />
                            @error('percentage')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="description">{{ __('labels.description') }}</label>
                            <textarea class="form-control" name="description" placeholder="{{ __('labels.tax').' '.__('labels.description') }}" autocomplete="off">{{ old('description', $tax->description) }}</textarea>
                            @error('description')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="form-group" id="tax_sales_account">
                                <label for="sales_account" class="col-sm-12">{{ __('labels.sales_account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="sales-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="sales-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="sales-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.taxes_to_pay') }}</strong>
                                            @foreach($taxes_to_pay_accounts_options as $taxes_to_pay_accounts_option)
                                                <a class="dropdown-item" data-value="{{$taxes_to_pay_accounts_option->id}}" data-level="{{$taxes_to_pay_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$taxes_to_pay_accounts_option->custom_name}}</span>
                                                    @if($taxes_to_pay_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $taxes_to_pay_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="sales_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('sales_account', $tax->sales_account) }}" />
                                </div>
                                @error('sales_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="form-group" id="tax_purchases_account">
                                <label for="purchases_account" class="col-sm-12">{{ __('labels.purchases_account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="purchases-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="purchases-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="purchases-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.taxes_in_favor') }}</strong>
                                            @foreach($taxes_in_favour_account_options as $taxes_in_favour_account_option)
                                                <a class="dropdown-item" data-value="{{$taxes_in_favour_account_option->id}}" data-level="{{$taxes_in_favour_account_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$taxes_in_favour_account_option->custom_name}}</span>
                                                    @if($taxes_in_favour_account_option->level == 1)
                                                    <span class="detail-type">{{ $taxes_in_favour_account_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.taxes_to_pay') }}</strong>
                                            @foreach($taxes_to_pay_accounts_options as $taxes_to_pay_accounts_option)
                                                <a class="dropdown-item" data-value="{{$taxes_to_pay_accounts_option->id}}" data-level="{{$taxes_to_pay_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$taxes_to_pay_accounts_option->custom_name}}</span>
                                                    @if($taxes_to_pay_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $taxes_to_pay_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="purchases_account" required="required" readonly="readonly" aria-hidden="true" type="text" value="{{ old('purchases_account', $tax->purchases_account) }}" />
                                </div>
                                @error('purchases_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('taxes.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
