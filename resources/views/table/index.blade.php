@extends('layouts.dashboard')

@section('title', trans('labels.tables'))

@section('content')
<style>
    .tabs-custom .no-footer .dataTables_length {
    margin-top: 21px;
}
</style>
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.tables') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.table') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('restaurant-table.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_table') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="table-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.table_name') }}</th>
                                    <th>{{ __('labels.zone') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.chairs_number') }}</th>
                                    <th>{{ __('labels.table_number') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($tables as $table)
                                <tr>
                                    <td>{{ $table->table_name }}</td>
                                    <td>
                                        <?php
                                        if($table->zone_id == 1){
                                        ?>
                                             {{ __('labels.main_room') }}
                                       <?php
                                        }elseif($table->zone_id == 2){
                                        ?>
                                           {{ __('labels.inside_room') }}
                                        <?php
                                        }elseif($table->zone_id == 3){
                                        ?>
                                            {{ __('labels.outside_room') }}
                                        <?php
                                        }else{
                                        ?>
                                            {{ __('labels.buffer_room') }}
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td>{{ $table->date }}</td>
                                    <td>{{ showIfAvailable($table->description) }}</td>
                                    <td>{{ $table->chairs_number }}</td>
                                    <td>{{ $table->chairs_number }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a
                                                    href="{{route('restaurant-table.show', $table)}}">{{ __('labels.view') }}</a>
                                                </li>
                                                <li><a href="{{route('restaurant-table.edit', $table)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource"
                                                    data-resource="{{ 'destroy-inventory-form-'.$table->id }}"
                                                    href="{{route('restaurant-table.destroy', $table)}}">{{ __('labels.delete') }}</a>
                                                </li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['restaurant-table.destroy', $table], 'id' => "destroy-table-form-{$table->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.table') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('restaurant-table.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.table') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
