@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.table'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.table') }}</h2>
        <form method="POST" action="{{ route('restaurant-table.store') }}" id="add-edit-table-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="table_name">{{ __('labels.table_name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="table_name" placeholder="{{ __('labels.table_name') }}" required value="{{ old('table_name') }}" autocomplete="off" />
                            @error('table_name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="table_date" class="form-control" name="date" required="required" value="{{old('date', getTodayDate(0))}}" autocomplete="off" />
                            @error('date')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="description">{{ __('labels.table_description') }}</label>
                            <textarea class="form-control" name="description" placeholder="{{ __('labels.table_description') }}" autocomplete="off">{{ old('description', null) }}</textarea>
                            @error('description')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="form-group" id="tax_purchases_account">
                                <label for="zone" class="col-sm-12">{{ __('labels.zone') }} </label>
                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="purchases-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="purchases-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="purchases-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value=" " data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_zone') }}</a>
                                            <a class="dropdown-item" data-value="1" data-level="" href="javascript::void(0)">
                                                <span class="account-name">{{ __('labels.main_room') }}</span>
                                            </a>
                                            <a class="dropdown-item" data-value="2" data-level="" href="javascript::void(0)">
                                                <span class="account-name">{{ __('labels.inside_room') }}</span>
                                            </a>
                                            <a class="dropdown-item" data-value="3" data-level="" href="javascript::void(0)">
                                                <span class="account-name">{{ __('labels.outside_room') }}</span>
                                            </a>
                                            <a class="dropdown-item" data-value="4" data-level="" href="javascript::void(0)">
                                                <span class="account-name">{{ __('labels.buffer_room') }}</span>
                                            </a>
                                        </div>
                                    </div>
                                    <input class="d-none" name="zone" required="required" readonly="readonly" aria-hidden="true" type="text" value="{{ old('zone') }}" />
                                </div>
                                @error('zone')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="form-group" id="tax_sales_account">
                                <label for="table_number">{{ __('labels.table_number') }}</label>
                                <input class="form-control" type="number" name="table_number" placeholder="{{ __('labels.table_number') }}" required value="{{ old('table_number') }}" autocomplete="off" />
                                @error('table_number')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="chairs_number">{{ __('labels.chairs_number') }}</label>
                            <input class="form-control" type="number" name="chairs_number" placeholder="{{ __('labels.chairs_number') }}" required value="{{ old('chairs_number') }}" autocomplete="off" />
                            @error('chairs_number')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6 col-md-6 prduct-pic">
                            <div class="form-group file-input file-input-large">
                                <a class="upload-button" data-preview_section="table-image">
                                <label for="table_image" id="image-preview-label" style="font-size: 22px; color: #2f3031;">
                                    {{ __('labels.table_image') }}
                                </label>
                                <img src="" id="image-preview" style="display: none;"/>
                               </a>
                                <input type="file" id="table-image" name="table_image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label">
                                @error('table_image')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                <div class="form-group popup-btns">
                    <button type="submit" name="submit" class="btn-custom" value="saveAndAdd">{{ __('labels.save_and_add_another') }}</button>
                    <a href="{{ route('restaurant-table.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom" value="save">{{ __('labels.save') }}</button>
                </div>
            </div>
        </form>

    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
