@extends('layouts.dashboard')

@section('title', trans('labels.show').' '.trans('labels.products_and_services'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('restaurant-table.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.table').' '.__('labels.details') }}</h2>
        <div class="action-btns-view">
            <a href="{{route('restaurant-table.edit', $table)}}" class="btn-custom"> {{ __('labels.edit_this_table') }}</a>
        </div>
        <div class="default-form view-form-detail item-detail-view view-details-product-ser">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="col-lg-8 view-details-product-cell">
                        <div class="form-outer">
                    <div class="form-group">
                        <label for="table_name"><b>{{ __('labels.table_name') }} </b></label>
                        <span class="value-form">{{ showIfAvailable($table->table_name) }}</span>
                    </div>
                </div>
                 <div class="form-outer">
                    <div class="form-group">
                        <label for="date"><b>{{ __('labels.date') }} </b></label>
                        <span class="value-form">{{ $table->date }}</span>
                    </div>
                </div>
                 <div class="form-outer">
                    <div class="form-group">
                        <label for="zone"><b>{{ __('labels.zone') }} </b></label>
                        @if ($table->zone_id == 1 )
                        <span class="value-form">Main Room</span>
                        @elseif ($table->zone_id == 2)
                        <span class="value-form">Inside Room</span>
                        @elseif ($table->zone_id == 3)
                        <span class="value-form">Outside Room</span>
                        @else
                        <span class="value-form">Buffer Room</span>
                        @endif
                    </div>
                </div>
                 <div class="form-outer">
                    <div class="form-group">
                        <label for="table_number"><b>{{ __('labels.table_number') }}</b></label>
                        <span class="value-form">{{ showIfAvailable($table->table_number) }}</span>
                    </div>
                </div>
                 <div class="form-outer">
                    <div class="form-group">
                        <label for="chairs_number"><b>{{ __('labels.chairs_number') }}</b></label>
                        <span class="value-form">{{ showIfAvailable(titleCase($table->chairs_number)) }}</span>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-group">
                        <label for="description"><b>{{ __('labels.description') }} </b></label>
                        <span class="value-form">{{ showIfAvailable($table->description) }}</span>
                    </div>
                </div>
                </div>
                <div class="col-lg-4 view-details-img-cell">
                    <div class="form-group">
                        @if($table->table_image)
                          <img id="item-logo-preview" src="{{ url('public/storage/tables/'.$table->table_image) }}" alt="{{ $table->name }}" />
                          @else
                          <img src="{{ asset('public/images/dumy-item.png') }}">
                        @endif
                    </div>
                </div>
            </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
