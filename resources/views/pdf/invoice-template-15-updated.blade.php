<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,500&display=swap">
    <title>{{ __('labels.invoice').'-'.$invoice->internal_number }}</title>
    <!-- CSS Reset : BEGIN -->
    <style type="text/css">
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    @media (max-width:600px) {
        .responsive-table {
            width: 100%;
            min-width: 100% !important;
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }
    }

    @media print,
    screen {
        @page {
            size: A4
        }

            {
            margin: 0;
        }

        html,
        body {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
        }

        body {
            position: relative;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        a[x-apple-data-detectors],
        /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img+div {
            display: none !important;
        }

        html,
        table,
        tr,
        td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
        }

        h2 {
            margin: 0;
            padding: 0
        }

        .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        img {
            max-width: 100%
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

        body {
            border: 0;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #303030;
            text-decoration: none;
        }

        table th,
        table td {
            background: transparent;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
            color: #303030;
            font-size: 15px;
            font-weight: 400 !important;
        }

        table strong {
            font-weight: 700 !important;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
        }

        table td {
            text-align: right;
            padding: 20px;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr td:first-child {
            border: none;
        }

        #pay-table thead th,
        #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff;
            font-weight: 700 !important;
            background: #0e68b1;
            padding: 6px;
            border: 3px solid #0e68b1;
        }

        #main-invoice-table tbody td,
        #pay-table tbody td {
            border: 2px solid #0e68b1;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
        }

        #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
        }

        #divider td {
            border: none !important;
        }

        #comapny-name td,
        #client-name td {
            font-size: 14px !important;
        }

        tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
        }

        table {
            -fs-table-paginate: paginate !important;
        }

        table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
        }

        #inner-main-table>td,
        #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
        }

        #inner-main-table table,
        #inner-main-table table tbody,
        #main-invoice-table table tbody {
            border: none !important;
        }

        #inner-main-table tfoot td,
        #inner-main-table tfoot tr,
        #inner-main-table tfoot {
            border: none;
        }

        table,
        tr,
        td,
        div {
            page-break-inside: avoid !important;
        }

        #main-invoice-table {
            position: relative;
        }

        #main-invoice-table thead {
            display: table-row-group;
        }

        tfoot {
            display: table-row-group;
        }

        table {
            page-break-after: auto
        }

        table tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        table td {
            page-break-inside: avoid;
            page-break-after: auto
        }

        table thead {
            display: table-header-group
        }

        table tfoot {
            display: table-footer-group
        }

        tr {
            page-break-inside: avoid;
            }
        
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
        <tr>
            <td align="center" valign="top">
                <!--[if (mso)|(IE)]>
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top" width="100%">
                            <![endif]-->
                <!--[if mso 16]>
                            <table width="600" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" valign="top" width="100%">
                                        <![endif]-->
                <table width="100%" style="max-width:1100px; margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tr style="border-bottom: 3px solid #0e68b1;">
                        <td style="padding:20px 0;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align:left; padding:0;">
                                        @if($user->company->logo)
                                        <img id="image-preview" src="{{ asset($user->company->logo) }}"
                                            alt="{{ $user->name }}" style="width: 200px;" />
                                        @endif
                                    </td>
                                    <td style="text-align:right; padding:0; font-size: 34px; color: #0e68b1;  line-height: 41px;
                                                                ">
                                                                <h2>{{ __('labels.invoice') }}</h2>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="border:none;">
                                                <td style="padding:0; border:none !important;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:none;">
                                                        <tbody style="border:none !important;">
                                                            <tr style="border:none;">
                                                                <td style="text-align:left; max-width: 37%; padding:0; vertical-align: text-top;"
                                                                    id="comapny-name">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody style="border: none !important;">
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    <h2
                                                                                        style="font-size: 17px; color: #0e68b1;   line-height: 34px;">
                                                                                        {{ $user->company->name }}
                                                                                    </h2>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if(addressFormat($user->company->address, 1)[0]
                                                                                    ??
                                                                                    null)
                                                                                    <span>{{ addressFormat($user->company->address, 1)[0] }}</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if(addressFormat($user->company->address, 1)[1]
                                                                                    ??
                                                                                    null)
                                                                                    <span>{{ addressFormat($user->company->address, 1)[1] }}</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    <span>{{ __('labels.phone') }}:
                                                                                    {{ $user->company->phone }}
                                                                                    @if($user->company->fax)
                                                                                    {{ __('labels.fax') }}:
                                                                                    {{ $user->company->fax }} @endif</span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if($user->company->support_email)
                                                                                    <span>{{ __('labels.support_email') }}: <a
                                                                                        href="mailto:{{$user->company->support_email}}">{{$user->company->support_email}}</a></span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if($user->company->tax_id)
                                                                                    <span>{{ __('labels.tax_id') }}:
                                                                                    {{ $user->company->tax_id }}</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; max-width: 29%; padding:0; vertical-align: text-top;"
                                                                    id="client-name">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody style="border: none !important;">
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    <h2
                                                                                        style="font-size: 17px; color: #303030; line-height: 34px;">
                                                                                        {{ $invoice->contact->name }}
                                                                                    </h2>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if($invoice->contact->address)
                                                                                    <span>{{ addressFormat($invoice->contact->address, 1)[0] }}</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if($invoice->contact->address &&
                                                                                    addressFormat($invoice->contact->address, 1)[1] ?? null)
                                                                                    <span>{{ addressFormat($invoice->contact->address, 1)[1] }}</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    <span>@if($invoice->contact->phone)
                                                                                    {{ __('labels.phone') }}:
                                                                                    {{ $invoice->contact->phone }}
                                                                                    @endif @if($invoice->contact->mobile)
                                                                                    {{ __('labels.mobile') }}: {{ $invoice->contact->mobile }}
                                                                                    @endif</span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if($invoice->contact->email)
                                                                                    <span>{{ __('labels.email') }}: <a
                                                                                        href="mailto:{{$invoice->contact->email}}">{{$invoice->contact->email}}</a></span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left; padding:0;">
                                                                                    @if($invoice->contact->tax_id)
                                                                                    <span>{{ __('labels.tax_id') }}:
                                                                                    {{ $invoice->contact->tax_id }}</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td style="padding:0; vertical-align: top;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody style="border: none !important;">
                                                                            <tr>
                                                                                <td
                                                                                    style="text-align: center; padding: 0; font-size: 17px; color: #ffffff; font-weight: 700 !important; background: #0e68b1; padding: 6px; border: 2px solid #0e68b1;">
                                                                                    {{ __('labels.invoice') }} {{ __('labels.no') }}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td
                                                                                    style="text-align: center; padding: 0; border: 2px solid #0e68b1; font-size: 26px; font-weight: 700;line-height: 73px; color: #303030;">
                                                                                    {{ $invoice->internal_number }}
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:20px 0;">
                                                    <table id="pay-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                            <tr>
                                                                <th>{{ __('labels.seller') }}</th>
                                                                <th>{{ __('labels.tax_id') }}</th>
                                                                <th>{{ __('labels.invoice_with') }}</th>
                                                                <th>{{ __('labels.ncf') }}</th>
                                                                <th>{{ __('labels.date') }}</th>
                                                                <th>{{ __('labels.way_to_pay') }}</th>
                                                                <th>{{ __('labels.exp_date') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>{{ $invoice->user->name }}</td>
                                                                <td>{{ showIfAvailable($user->company->tax_id)}} </td>
                                                                <td>{{ $invoice->ncf ? showIfAvailable($invoice->ncf->name) : 'N/A' }}</td>
                                                                <td>{{ $invoice->ncf ? showIfAvailable($invoice->ncf_number) : 'N/A' }}</td>
                                                                <td>{{ dateFormat($invoice->start_date) }}</td>
                                                                <td>{{ __('labels.cash') }}</td>
                                                                <td>{{ dateFormat($invoice->expiration_date) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0;">
                                                    <table id="main-invoice-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <thead style="display: table-row-group !important;">
                                                            <tr>
                                                                <th>{{ __('labels.qty') }}</th>
                                                                <th>{{ __('labels.ref') }}</th>
                                                                <th>{{ __('labels.item') }} {{ __('labels.name') }}</th>
                                                                <th>{{ __('labels.tax_amount') }}</th>
                                                                <th>{{ __('labels.price') }}</th>
                                                                <th>{{ __('labels.disc') }}%</th>
                                                                <th>{{ __('labels.total') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="border: none !important;">
                                                
                                                            @foreach($invoice->items as $key => $item)
                                                            <tr>
                                                                <td>{{$item->quantity}}</td>
                                                                <td>{{showIfAvailable($item->inventory->reference)}}</td>
                                                                <td>{{$item->inventory->name}}</td>
                                                                <td>{{moneyFormat($item->calculation['taxAmount'])}}</td>
                                                                <td>{{moneyFormat($item->price)}}</td>
                                                                <td>{{addZeros($item->discount)}}%</td>
                                                                <td>{{moneyFormat($item->calculation['total'])}}</td>
                                                            </tr>
                                                            @endforeach
                                            
                                                        </tbody>
                                                        <tfoot style="display: table-row-group">
                                                            <tr>
                                                                <td colspan="4"></td>
                                                                <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                    {{ __('labels.total') }} {{ __('labels.discount') }}
                                                                </td>
                                                                <td style="border: 2px solid #0e68b1;">
                                                                    {{moneyFormat($invoice->calculation['discountAmount'])}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4"></td>
                                                                <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                    {{ __('labels.sub_total') }}
                                                                </td>
                                                                <td style="border: 2px solid #0e68b1;">
                                                                    {{moneyFormat($invoice->calculation['subtotal'])}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4"></td>
                                                                <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                    {{ __('labels.tax') }}
                                                                </td>
                                                                <td style="border: 2px solid #0e68b1;">
                                                                    {{moneyFormat($invoice->calculation['taxAmount'])}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                </td>
                                                                <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                    {{ __('labels.total') }}
                                                                </td>
                                                                <td style="border: 2px solid #0e68b1;">
                                                                    {{moneyFormat($invoice->calculation['total'])}}
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:30px 0;">
                                                    <table width="300px" border="0" cellspacing="0" cellpadding="0" id="divider">
                                                        <tbody>
                                                            <tr>
                                                                <td
                                                                    style="padding:0; font-size: 14px;  border-bottom: 2px solid #0e68b1 !important; text-align:center; padding:0; font-weight: 400;">
                                                                    @if($user->company->signature)
                                                                    <img style="width: 193px; display:block; margin: 0 auto 10px;" id="signature-preview"
                                                                        src="{{ asset($user->company->signature) }}" alt="{{ $user->name }}" />@endif
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:center; padding:0; text-transform: capitalize;">
                                                                    {{ __('labels.elaborated_by') }}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                            <table width="auto" style="margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                                            <tr>
                                                @if($invoice->type == 'recurring')
                                                <td style="text-align:left; padding: 40px 0px 5px 0px; font-size: 13px;">
                                                    <strong style="font-weight: 700;">{{ __('labels.observations') }}:
                                                    </strong>{{ $invoice->observation }}
                                                </td>
                                                @else
                                                <td style="text-align:left; padding: 40px 0px 5px 0px; font-size: 13px;">
                                                    <strong style="font-weight: 700;">{{ __('labels.terms_and_conditions') }}:
                                                    </strong>{{ $invoice->tac }}
                                                </td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td style="text-align:left; padding: 5px 0px 5px 0px; font-size: 13px;">
                                                    <strong style="font-weight: 700;">{{ __('labels.notes') }}:
                                                    </strong>{{ $invoice->notes }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;  padding: 5px 0px 5px 0px; font-size: 13px;">
                                                    <strong style="font-weight: 700;">{{ __('labels.resolution_text') }}:
                                                    </strong>{{ $invoice->res_text }}
                                                </td>
                                            </tr>
                                            </tbody>
                                            </table>
                                            <!--[if mso 16]>
                                            </td>
                                            </tr>
                                        </table>
                                        <![endif]-->
                    <!--[if (mso)|(IE)]>
                                    </td>
                                </tr>
                            </table>
                            <![endif]-->
            </td>
        </tr>
    </table>
</body>

</html>
