<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <title>{{ __('labels.invoice').'-'.$invoice->internal_number }}</title>
    <!-- CSS Reset : BEGIN -->
    <style type="text/css">
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    @media screen,
    print {

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            font-family: 'Raleway', sans-serif !important;
            width: 100%;
            height: 100%;
        }

        body {
            position: relative;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        a[x-apple-data-detectors],
        /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img+div {
            display: none !important;
        }

        html,
        table,
        tr,
        td {
            margin: 0;
            padding: 0;
        }

        h2 {
            margin: 0;
            padding: 0
        }

        .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        img {
            max-width: 100%
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }
    }

    @media (max-width:1300px) {

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            font-family: 'Raleway', sans-serif !important;
            width: 100%;
            height: 100%;
        }

        body {
            position: relative;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        a[x-apple-data-detectors],
        /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img+div {
            display: none !important;
        }

        html,
        table,
        tr,
        td {
            margin: 0;
            padding: 0;
        }

        h2 {
            margin: 0;
            padding: 0
        }

        .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        img {
            max-width: 100%
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

        #main-invoice-table tbody tr th {
            padding: 8px;
        }

        #main-invoice-table tbody tr:even td {
            background: #d5e0ea;
        }

        #main-invoice-table tbody tr:first-child th {
            background: #3c81ba;
            color: #fff
        }

        #main-invoice-table tbody tr th {
            background: #436784;
            border-right: 2px solid #fff;
            color: #fff;
        }

        #main-invoice-table>tbody>tr:nth-child(even) td {
            background: #fff;
        }

        #main-invoice-table>tbody>tr:nth-child(odd) td {
            background: #d5e0ea;
            color: #000;
        }

        #main-invoice-table tbody tr:nth-child(2) td {
            background: #d5e0ea;
            color: #494c4d;
            text-align: center !important;
        }

        #main-invoice-table tbody tr:nth-child(3)~tr td {
            border: 2px solid #3b3e40;
        }

        #main-invoice-table tfoot tr td:nth-child(2) {
            font-weight: 700 !important;
        }

        #main-invoice-table tfoot tr:last-child td:last-child {
            font-weight: 700 !important;
        }

    }

    @media (max-width:600px) {
        .responsive-table {
            width: 100%;
            min-width: 100% !important;
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }
    }

    @media screen,
    print {

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #303030;
            text-decoration: none;
        }

        table th,
        table td {
            background: transparent;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
            color: #303030;
            font-size: 13px;
            line-height: 17px;
            font-weight: 500 !important;
        }

        table strong {
            font-weight: 700 !important;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
        }

        table td {
            text-align: right;
            padding: 20px;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr td:first-child {
            border: none;
        }

        #pay-table thead th,
        #main-invoice-table thead th tr:last-child {
            font-size: 15px !important;
            color: #ffffff;
            font-weight: 700 !important;
            background: #587891;
            border-top: none;
            border-bottom: none;
            color: #fff;
        }

        #main-invoice-table tbody td,
        #pay-table tbody td {
            border: none;
            text-align: center;
            padding: 8px;
            font-weight: 500 !important;
        }

        #pay-table thead th {
            background: #85c8ff;
            border: 3px solid #fff;
            border-top: none;
            border-bottom: none;
        }

        #pay-table thead th:first-child {
            border-left: none;
        }

        #pay-table thead th:last-child {
            border-right: none;
        }

        #pay-table tbody td {
            border: 3px solid #fff;
            background: #d5e0ea;
            border-top: none;
            border-bottom: none;
        }

        #pay-table tbody td:first-child {
            border-left: none;
        }

        #pay-table tbody td:last-child {
            border-right: none;
        }

        #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-size: 13px !important;
            padding: 8px;
        }

        #main-invoice-table>tbody>tr>td {
            border: 2px solid #fff;
            border-style: solid;
            border-bottom: none;
            border-top: none
        }

        #main-invoice-table tbody {
            border-bottom: 2px solid #ffffff;
        }

        #divider td {
            border: none !important;
        }

        #comapny-name td,
        #client-name td,
        #client-name td a,
        #comapny-name td a {
            font-size: 13px !important;
            color: #587891;
        }

        #main-invoice-table tfoot td {
            color: #587891;
        }


        #top-table tr td {
            background: #d5e0ea;
            border-right: 2px solid #fff;
        }

        #top-table tr:last-child td {
            color: #fff;
            background: #587891;
        }


        #top-table tr:first-child td {
            background: #43a1ed;
            color: #fff;
            font-weight: 700 !important;
        }

        #main-invoice-table>tbody>tr:nth-child(even) {
            background: #d5e0ea;
        }

        #main-invoice-table td {
            vertical-align: middle;
            font-size: 13px;
            line-height: 17px;
        }

        #main-invoice-table>tbody>tr>td {
            text-align: left;
        }

        #main-invoice-table>tbody>tr>td:last-child,
        #main-invoice-table>tbody>tr>td:nth-child(6) {
            text-align: right;
        }

        #main-invoice-table tbody tr th {
            padding: 8px;
        }

        #main-invoice-table tbody tr:even td {
            background: #d5e0ea;
        }

        #main-invoice-table tbody tr:first-child th {
            background: #3c81ba;
            color: #fff
        }

        #main-invoice-table tbody tr th {
            background: #436784;
            border-right: 2px solid #fff;
            color: #fff;
        }

        #main-invoice-table>tbody>tr:nth-child(even) td {
            background: #fff;
        }

        #main-invoice-table>tbody>tr:nth-child(odd) td {
            background: #d5e0ea;
            color: #000;
        }

        #main-invoice-table tbody tr:nth-child(2) td {
            background: #d5e0ea;
            color: #494c4d;
            text-align: center !important;
        }

        #main-invoice-table tbody tr:nth-child(2) {
            background: #fff !important;
        }

        #main-invoice-table tfoot tr td:nth-child(2) {
            font-weight: 700 !important;
            text-align: right;

        }

        #main-invoice-table tfoot tr:last-child td:last-child {
            font-weight: 700 !important;
            text-align: right !important;
        }

        #main-invoice-table tbody tr:nth-child(3)~tr td {
            border: 2px solid #3b3e40;
        }

        #main-invoice-table tfoot tr td:nth-child(2) {
            font-weight: 700 !important;
        }

        #main-invoice-table tfoot tr:last-child td:last-child {
            font-weight: 700 !important;
        }


    }

    </style>
</head>

<body class="template-invoice-six">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
        <tr>
            <td align="center" valign="top">
                <!--[if (mso)|(IE)]>

               <table width="600" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                     <td align="left" valign="top" width="100%">

            <![endif]-->

                <!--[if mso 16]>

               <table width="600" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                     <td align="left" valign="top" width="100%">

            <![endif]-->
                <table width="100%" style="max-width:1100px; margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tr style="border-bottom: 2px solid #587891;">
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align:left; padding:0;">
                                        @if($user->company->logo)
                                        <img id="image-preview" src="{{ asset($user->company->logo) }}"
                                            alt="{{ $user->company->name }}" style="width: 141px;" />
                                        @endif
                                    </td>
                                    <td
                                        style="text-align:right; padding:0; font-size: 34px; color: #587891;  line-height: 41px;">
                                        <h2>{{ __('labels.invoice') }}</h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr style="border-bottom: 2px solid #587891;">
                        <td style="padding:20px 0 20px 0;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                <tbody>
                                    <tr>
                                        <td style="text-align:left; max-width: 80%; padding:0; vertical-align: text-top;"
                                            id="comapny-name">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <h2
                                                                style="font-size: 17px; color: #587891;   line-height: 34px;">
                                                                {{ $user->company->name }}
                                                            </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if(addressFormat($user->company->address, 1)[0]
                                                            ??
                                                            null)
                                                            <span>{{ addressFormat($user->company->address, 1)[0] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if(addressFormat($user->company->address, 1)[1]
                                                            ??
                                                            null)
                                                            <span>{{ addressFormat($user->company->address, 1)[1] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <span>{{ __('labels.phone') }}:
                                                                {{ $user->company->phone }}
                                                                @if($user->company->fax)
                                                                {{ __('labels.fax') }}:
                                                                {{ $user->company->fax }} @endif
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($user->company->support_email)
                                                            <span>{{ __('labels.support_email') }}: <a
                                                                    href="mailto:{{$user->company->support_email}}">{{$user->company->support_email}}</a></span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($user->company->tax_id)
                                                            <span>{{ __('labels.tax_id') }}:
                                                                {{ $user->company->tax_id }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>

                                        <td style="text-align:left; max-width: 20%; padding:0; vertical-align: text-top;"
                                            id="client-name">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <h2
                                                                style="font-size: 17px; color: #587891; line-height: 34px;">
                                                                {{ $invoice->contact->name }}
                                                            </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->address)
                                                            <span>{{ addressFormat($invoice->contact->address, 1)[0] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->address &&
                                                            addressFormat($invoice->contact->address, 1)[1] ?? null)
                                                            <span>{{ addressFormat($invoice->contact->address, 1)[1] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <span>@if($invoice->contact->phone)
                                                                {{ __('labels.phone') }}:
                                                                {{ $invoice->contact->phone }}
                                                                @endif @if($invoice->contact->mobile)
                                                                {{ __('labels.mobile') }}:
                                                                {{ $invoice->contact->mobile }}
                                                                @endif</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->email)
                                                            <span>{{ __('labels.email') }}: <a
                                                                    href="mailto:{{$invoice->contact->email}}">{{$invoice->contact->email}}</a></span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->tax_id)
                                                            <span>{{ __('labels.tax_id') }}:
                                                                {{ $invoice->contact->tax_id }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>


                    <tr>
                        <td style="padding:0;">
                            <table id="main-invoice-table" width="100%" border="0" cellspacing="0" cellpadding="0">

                                <thead>
                                    <!-- <tr>
                                        <th>
                                            <table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Item </td></tr>
                                                </tbody>
                                            </table>
                                            </th>
                                        <th>
                                            <table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Price </td></tr>
                                                </tbody>
                                            </table>
                                        </th>
                                        <th>
                                            
                                        <table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Discount % </td></tr>
                                                </tbody>
                                            </table></th>

                                        <th><table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Tax %</td></tr>
                                                </tbody>
                                            </table></th>
                                        <th><table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Tax Amount</td></tr>
                                                </tbody>
                                            </table></th>
                                        <th><table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Qty</td></tr>
                                                </tbody>
                                            </table></th>
                                        <th><table id="top-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>Invoice For</td>
                                                    </tr>

                                                    <tr>
                                                        <td>NCF Name</td>
                                                    </tr>
                                                    <tr> <td>Total</td></tr>
                                                </tbody>
                                            </table></th>
                                    </tr> -->

                                </thead>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.seller') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.tax_id') }}</th>
                                        <th>{{ __('labels.invoice_with') }}</th>
                                        <th>{{ __('labels.ncf') }}</th>
                                        @endif
                                        <th>{{ __('labels.date') }}</th>
                                        @if($invoice->currency && ($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.currency') }}</th>
                                        @endif
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.way_to_pay') }}</th>
                                        @endif
                                        <th>{{ __('labels.exp_date') }}</th>
                                    </tr>

                                    <tr>
                                        <td>{{ $invoice->user->name }}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{ showIfAvailable($user->company->tax_id)}} </td>
                                        <td>{{ $invoice->ncf ? showIfAvailable($invoice->ncf->name) : 'N/A' }}</td>
                                        <td>{{ $invoice->ncf ? showIfAvailable($invoice->ncf_number) : 'N/A' }}</td>
                                        @endif
                                        <td>{{ dateFormat($invoice->start_date) }}</td>
                                        @if($invoice->currency && ($viewType ?? null) != 'production-order')
                                        <td>{{ $invoice->currency->code }}</td>
                                        @endif
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{ __('labels.cash') }}</td>
                                        @endif
                                        <td>{{ dateFormat($invoice->expiration_date) }}</td>
                                    </tr>

                                    <tr>
                                        <th>{{ __('labels.item') }} {{ __('labels.name') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.price') }}</th>
                                        <th>{{ __('labels.disc') }}%</th>
                                        <th>{{ __('labels.tax') }}%</th>
                                        <th>{{ __('labels.tax_amount') }}</th>
                                        @endif
                                        @if(($viewType ?? null) == 'production-order')
                                        <th>{{ __('labels.ref') }}</th>
                                        @endif
                                        <th>{{ __('labels.qty') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.total') }}</th>
                                        @endif
                                    </tr>
                                    @foreach($invoice->items as $key => $item)
                                    <tr>
                                        <td>{{$item->inventory->name}}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                        <td>{{addZeros($item->discount,null,2,true)}}%</td>
                                        <td>{{ addZeros(($item->tax ? $item->tax->percentage : 0),null,2,true) }}%</td>
                                        <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                        @endif
                                        @if(($viewType ?? null) == 'production-order')
                                        <td>{{showIfAvailable($item->reference)}}</td>
                                        @endif
                                        <td>{{$item->quantity}}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>


                                @if(($viewType ?? null) != 'production-order')
                                <tfoot>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.sub_total') }}
                                        </td>
                                        <td
                                            style="border: 2px solid #fff; font-weight: 500; text-align:right; color: #303030; background: #d5e0ea;">
                                            {{moneyFormat($invoice->calculation['subtotal'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.total') }} {{ __('labels.discount') }}
                                        </td>
                                        <td
                                            style="border: 2px solid #fff; font-weight: 500; color: #303030; text-align:right; background: #d5e0ea;">
                                            {{moneyFormat($invoice->calculation['discountAmount'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.tax') }}
                                        </td>
                                        <td
                                            style="border: 2px solid #fff; font-weight: 500; text-align:right; color: #303030; background: #fff;">
                                            {{moneyFormat($invoice->calculation['taxAmount'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        </td>

                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.total') }}
                                        </td>
                                        <td
                                            style="border: 2px solid #fff; font-weight: 700 !important text-align:right; color: #303030; background: #d5e0ea;">
                                            {{moneyFormat($invoice->calculation['total'],null,2,true)}}
                                        </td>
                                    </tr>
                                </tfoot>
                                @endif
                            </table>
                        </td>

                    </tr>

                    <tr>
                        <td style="padding:0;">
                            <table width="400px" style="margin:0 auto;" border="0" cellspacing="0" cellpadding="0"
                                id="divider">
                                <tbody>
                                    <tr>
                                        <td
                                            style="padding:0; font-size: 14px;  border-bottom: 2px solid #303030 !important; text-align:center; padding:0; font-weight: 500;">
                                            @if($user->company->signature)
                                            <img style="width: 193px; display:block; margin: 20px auto 20px;"
                                                id="signature-preview" src="{{ asset($user->company->signature) }}"
                                                alt="{{ $user->company->name }}" />@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; padding:5px 0 0 0; text-transform: capitalize;">
                                            {{ __('labels.elaborated_by') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <table width="auto" style="margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        @if($invoice->type == 'recurring')
                        <td
                            style="text-align:left; padding: 40px 0px 0 0px; font-weight: 700 !important; font-size: 13px; color: #4880b3;">
                            <strong style="font-weight: 700; color: #303030;">{{ __('labels.observations') }}: </strong>{{ $invoice->observation }}
                        </td>
                        @else
                        <td
                            style="text-align:left; padding: 40px 0px 0 0px; font-weight: 700 !important; font-size: 13px; color: #4880b3;">
                            <strong style="font-weight: 700; color: #303030;">{{ __('labels.terms_and_conditions') }}: </strong>{{ $invoice->tac }}
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td
                            style="text-align:left; padding: 5px 0px 0 0px; font-weight: 700 !important; font-size: 13px; color: #4880b3;">
                            <strong style="font-weight: 700; color: #303030;">{{ __('labels.notes') }}: </strong>{{ $invoice->notes }}
                        </td>
                    </tr>
                    <tr>
                        <td
                            style="text-align:left;  padding: 5px 0px 0 0px; font-weight: 700 !important; font-size: 13px; color: #4880b3;">
                            <strong style="font-weight: 700; color: #303030;">{{ __('labels.resolution_text') }}: </strong>{{ $invoice->res_text }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;  padding: 5px 0px 0 0px; font-weight: 700 !important; font-size: 13px; color: #4880b3;">
                            <strong style="font-weight: 700; color: #303030;">{{ __('labels.privacy_policy') }}: </strong>{{ $invoice->privacy_policy }}
                        </td>
                    </tr>
                 </tbody>
                </table>
                    <!--[if mso 16]>

            </td>

         </tr>

      </table>

            <![endif]-->

                    <!--[if (mso)|(IE)]>

            </td>

         </tr>

      </table>

            <![endif]-->
            </td>
        </tr>
    </table>

</body>

</html>
