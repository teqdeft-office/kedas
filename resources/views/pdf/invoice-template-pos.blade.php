<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <title>{{ __('labels.invoice').'-'.$invoice->internal_number }}</title>
    <!-- CSS Reset : BEGIN -->
    <style type="text/css">
        @media screen, print { 
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    html,
    body {
        margin: 0 auto !important;
        padding: 0 !important;
        font-family: 'Raleway', sans-serif !important;
        width: 100%;
        height: 100%;
    }

    body {
        position: relative;
        margin: 0 auto;
        color: #555555;
        background: #FFFFFF;
        font-size: 16px !important;
    }

    /* What it does: Stops email clients resizing small text. */
    * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }

    /* What it does: Centers email on Android 4.4 */
    div[style*="margin: 16px 0"] {
        margin: 0 !important;
    }

    /* What it does: Stops Outlook from adding extra spacing to tables. */
    table,
    td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
    }

    /* What it does: Fixes webkit padding issue. */
    table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
    }

    /* What it does: Uses a better rendering method when resizing images in IE. */
    img {
        -ms-interpolation-mode: bicubic;
    }

    /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
    a {
        text-decoration: none;
    }

    /* What it does: A work-around for email clients meddling in triggered links. */
    a[x-apple-data-detectors],
    /* iOS */
    .unstyle-auto-detected-links a,
    .aBn {
        border-bottom: 0 !important;
        cursor: default !important;
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    /* What it does: Prevents Gmail from changing the text color in conversation threads. */
    .im {
        color: inherit !important;
    }

    /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
    .a6S {
        display: none !important;
        opacity: 0.01 !important;
    }

    /* If the above doesn't work, add a .g-img class to any image in question. */
    img.g-img+div {
        display: none !important;
    }

    html,
    table,
    tr,
    td {
        margin: 0;
        padding: 0;
    }

    h2 {
        margin: 0;
        padding: 0
    }

    .responsive-table {
        width: calc(33.3% - 4px);
        display: inline-block;
        vertical-align: middle;
        text-align: left;
    }

    img {
        max-width: 100%
    }

    .conatiner-space {
        padding-left: 30px;
        padding-right: 30px;
    }
    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }

    a {
        color: #303030;
        text-decoration: none;
    }

    #invoice-POS {
        padding: 2mm;
        margin: 30px auto;
        width: 44mm;
        background: #FFF;
        border: 2px solid #303030;
    }

    table td {
        font-size: 11px;
        font-weight: 600;
        padding: 2px;
    }

    table th {
        font-size: 11px;
        font-weight: 700;
    }

    #invoice-POS #top .logo {
        height: 60px;
        width: 60px;
    }

    #invoice-POS>tbody>tr>td {
        padding: 2mm;
    }

    #company-address td {
        font-size: 11px;
    }

    #main-invoice-table th {
        white-space: pre;
        padding: 3px;
    }

    #main-invoice-table tr td:last-child {
        text-align:right;
    }

    }
    @media (max-width:600px) {
        .responsive-table {
            width: 100%;
            min-width: 100% !important;
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }
    }

    @media (max-width:1300px) {
        html,
    body {
        margin: 0 auto !important;
        padding: 0 !important;
        font-family: 'Raleway', sans-serif !important;
        width: 100%;
        height: 100%;
    }

    body {
        position: relative;
        margin: 0 auto;
        color: #555555;
        background: #FFFFFF;
        font-size: 16px !important;
    }

    /* What it does: Stops email clients resizing small text. */
    * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }

    /* What it does: Centers email on Android 4.4 */
    div[style*="margin: 16px 0"] {
        margin: 0 !important;
    }

    /* What it does: Stops Outlook from adding extra spacing to tables. */
    table,
    td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
    }

    /* What it does: Fixes webkit padding issue. */
    table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
    }

    /* What it does: Uses a better rendering method when resizing images in IE. */
    img {
        -ms-interpolation-mode: bicubic;
    }

    /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
    a {
        text-decoration: none;
    }

    /* What it does: A work-around for email clients meddling in triggered links. */
    a[x-apple-data-detectors],
    /* iOS */
    .unstyle-auto-detected-links a,
    .aBn {
        border-bottom: 0 !important;
        cursor: default !important;
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    /* What it does: Prevents Gmail from changing the text color in conversation threads. */
    .im {
        color: inherit !important;
    }

    /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
    .a6S {
        display: none !important;
        opacity: 0.01 !important;
    }

    /* If the above doesn't work, add a .g-img class to any image in question. */
    img.g-img+div {
        display: none !important;
    }

    html,
    table,
    tr,
    td {
        margin: 0;
        padding: 0;
    }

    h2 {
        margin: 0;
        padding: 0
    }

    .responsive-table {
        width: calc(33.3% - 4px);
        display: inline-block;
        vertical-align: middle;
        text-align: left;
    }

    img {
        max-width: 100%
    }

    .conatiner-space {
        padding-left: 30px;
        padding-right: 30px;
    }
    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }

    a {
        color: #303030;
        text-decoration: none;
    }

    #invoice-POS {
        padding: 2mm;
        margin: 30px auto;
        width: 44mm;
        background: #FFF;
        border: 2px solid #303030;
    }

    table td {
        font-size: 11px;
        font-weight: 600;
        padding: 2px;
    }

    table th {
        font-size: 11px;
        font-weight: 700;
    }

    #invoice-POS #top .logo {
        height: 60px;
        width: 60px;
    }

    #invoice-POS>tbody>tr>td {
        padding: 2mm;
    }

    #company-address td {
        font-size: 11px;
    }

    #main-invoice-table th {
        white-space: pre;
        padding: 3px;
    }

    #main-invoice-table tr td:last-child {
        text-align:right;
    }
    }

    </style>
</head>

<body>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
        <tr>
            <td align="center" valign="top">
                <!--[if (mso)|(IE)]>

               <table width="600" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                     <td align="left" valign="top" width="100%">

            <![endif]-->

                <!--[if mso 16]>

               <table width="600" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                     <td align="left" valign="top" width="100%">

            <![endif]-->
                <table id="invoice-POS" width="100%" style="margin: 0 auto; width: 44mm; padding: 2mm;" border="0"
                    cellspacing="0" cellpadding="0">

                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td style="text-align:right; width: 70%;">
                                            @if(Auth::user()->company->logo)
                                            <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                                alt="{{ Auth::user()->name }}" style="width: 60px;" />
                                            @endif
                                        </td>

                                        <td style="font-size: 14px; font-weight: 700; text-align:center;">
                                            Invoice #6
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td style="padding:0 0 10px 0;">
                            <table id="company-address" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td style="text-align:center; padding-bottom: 10px; font-size: 17px; font-weight: 700;">
                                            {{ Auth::user()->company->name }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:center;">
                                            MIAMI,
                                            Florida,Estados Unidos, 33195.
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:center;">
                                            Teléfono:
                                            7862343355 Fax: 7865345533
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:center;">
                                            Correo
                                            electrónico: <a href="mailto:support@kedasrd.com">support@kedasrd.com</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:center;">
                                            RNC:1314223383
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: 2px solid #303030;">
                        <td style="padding:3px; font-size: 12px; line-height: 17px; text-align:center; font-weight: 700;">
                            NCF Name/Number
                        </td>
                    </tr>
                    <tr class="main-inovice-row">
                        <td style="padding:0;">
                        <table id="main-invoice-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr style="border: 2px solid #303030; border-top:none; border-right:none; border-left:none;">
                                    <th>QTY x item x Price </th>
                                    <th>ITBIS/TAX</th>
                                    <th>TOTAL</th>
                                </tr>

                            </thead>
                            <tbody>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                            
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>

                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                            
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                            
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                            
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                            
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                                <tr>
                                    <td>2x item x 99</td>
                                    <td>$60.00</td>
                                    <td>$396.00</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr></tr>
                                <tr>
                                   <td colspan="2" style="text-align:right; font-weight: 700 !important;; padding-top: 14px; padding-right: 10px;">Discount: </td>
                                    <td style="text-align:right; padding-top: 10px;">$0.00</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:right; font-weight: 700 !important; padding-right: 14px;">Subtotal:</td>
                                    <td style="text-align:right; background: #fff;">$6,045.00</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:right; font-weight: 700 !important; padding-right: 14px;">ITBIS:</td>
                                    <td style="text-align:right;">$1,005.00</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-bottom: 20px; font-weight: 700 !important; text-align:right; padding-right: 14px; font-weight: 700 !important;">Total:</td>
                                    <td style="padding-bottom: 20px; text-align:right;">$7,050.0</td>
                                </tr>
                            </tfoot>
                            </table>
                        </td>
                    </tr>

                    <tr style="border: 2px solid #303030;">
                        <td style="padding:3px; font-size: 12px; line-height: 17px; text-align:center; font-weight: 600; color: #303030;">
                            Seller: <strong style="font-weight: 700;">User Name and Last Name</strong>
                        </td>
                    </tr>

                    <tr style="border: 2px solid #303030;">
                        <td style="padding:3px; font-size: 12px; line-height: 17px; text-align:center; font-weight: 600; color: #303030;">
                            Way of Pay: <strong style="font-weight: 700;">Cash</strong>
                        </td>
                    </tr>

                    <tr style="border: 2px solid #303030;">
                        <td style="padding:3px;; font-size: 12px; line-height: 17px; text-align:center; font-weight: 600; color: #303030;">
                            Client: <strong style="font-weight: 700;">Contact Name</strong>
                        </td>
                    </tr>
                </table>
                <!--End Invoice-->

                <!--[if mso 16]>

            </td>

         </tr>

      </table>

            <![endif]-->

                <!--[if (mso)|(IE)]>

            </td>

         </tr>

      </table>

            <![endif]-->
            </td>
        </tr>
    </table>

</body>

</html>

<script>
    function PrintElem() 
    {
        Popup($html);
    }

    function Popup(data) 
    {
        var myWindow = window.open('', 'Receipt', 'height=400,width=600');
        myWindow.document.write('<html><head><title>Receipt</title>');
        /*optional stylesheet*/ //myWindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        myWindow.document.write('<style type="text/css"> *, html {margin:0;padding:0;} </style>');
        myWindow.document.write('</head><body>');
        myWindow.document.write(data);
        myWindow.document.write('</body></html>');
        myWindow.document.close(); // necessary for IE >= 10

        myWindow.onload=function(){ // necessary if the div contain images

            myWindow.focus(); // necessary for IE >= 10
            myWindow.print();
            myWindow.close();
        };
    }
</script>
