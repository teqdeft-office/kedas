<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <title>{{ __('labels.'$invoice->type) }} {{ __('labels.invoice').'-'.$invoice->internal_number }}</title>
    <style type="text/css">
    @media print {
        body#body-invoice-template {
            width: 30cm;
            height: 40.7cm;
            margin: 0;
            padding: 0;
            font-size: 12px;
            page-break-inside: avoid;
            color: #000000;
            font-weight: 400;

        }

        .wrapper-invoice-template table {
            width: 100% !important;
            max-width: 100% !important;
            min-width: 100% !important;
            page-break-inside: avoid;
        }

        .wrapper-invoice-template table.client-table th,
        .wrapper-invoice-template table.item-table th{
        background: #0e68b1;
        border-bottom: 2px solid #0e68b1 !important;
        border-top: 2px solid #0e68b1 !important;
        color: #fff;
        }


        .wrapper-invoice-template table.item-table,
        .wrapper-invoice-template table.item-table th,
        .wrapper-invoice-template table.item-table td,
        .wrapper-invoice-template table.item-table tr,
        .wrapper-invoice-template table.client-table tr,
        .wrapper-invoice-template table.client-table th,
        .wrapper-invoice-template table.client-table td,
        .wrapper-invoice-template table.client-table{
            border: 1px solid #0e68b1 !important;
        }

        .wrapper-invoice-template table.item-table,
        .wrapper-invoice-template table.client-table{
            border: 2px solid #0e68b1 !important;
        }
    }

    body#body-invoice-template {
        margin: 0;
        padding: 0;
        font-size: 12px;
        page-break-inside: avoid;
        color: #000000;
        font-weight: 400;
        font-family: 'Roboto', sans-serif !important;
    }

    @page 
        {
        size: A4;
        margin: 1cm;
    }

    * {
        padding: 0px;
        margin: 0px;
    }

    .wrapper-invoice-template table {
        width: 95% !important;
        margin: 10px auto !important;
        page: rotated;
        page-break-inside: avoid;
    }

    .wrapper-invoice-template .wrapper {
        width: 100%;
        max-width: 700px;
        margin: 0 auto;
        padding: 10px;
    }

    .wrapper-invoice-template .wrapper table {
        width: 97%;
        margin: 10px;
    }

    .wrapper-invoice-template table#print-invoice-details {
        width: 90%;
        border-collapse: collapse;
        -webkit-print-color-adjust: exact;
    }

    .wrapper-invoice-template table#print-invoice-details th {
        background: #0e68b1;
        border-bottom: thin solid #0e68b1;
        color: #fff;
    }

    .wrapper-invoice-template table#print-invoice-details td {
        font-size: 12px;
        color: #000000;
        padding: 5px 10px;
        border-right: thin solid #0e68b1;
        border-bottom: thin solid #0e68b1;
    }

    .wrapper-invoice-template table#print-invoice-details tr:last-child td {
        border-bottom: none
    }

    table#print-invoice-details td:last-child {
        border-right: thin solid #0e68b1;
    }

    .wrapper-invoice-template table#print-invoice-details th {
        font-size: 12px;
        color: #fff;
        padding: 3px 7px;
        text-transform: uppercase;
    }

    .wrapper-invoice-template table#print-invoice-details .blank-td {
        width: 190px;
    }

    .wrapper-invoice-template table#print-invoice-details a {
        color: #000000;
        text-decoration: none;
    }

    .wrapper-invoice-template table#print-invoice-details .blank-tr-space {
        height: 40px;
    }

    .wrapper-invoice-template table#print-invoice-details img {
        max-width: 100%;
    }

    .wrapper-invoice-template table#print-invoice-details #image-preview {
        max-width: 220px;
        max-height: 276px;
        object-fit: cover;
        width: 100%;
    }

    .wrapper-invoice-template table#print-invoice-details #signature-preview {
        max-width: 100%;
        max-height: 100px;
        width: 100%;
        object-fit: none;
        display: inline-block;
    }

    .wrapper-invoice-template table#print-invoice-details .client-table,
    .wrapper-invoice-template table#print-invoice-details .item-table,
    .wrapper-invoice-template table#print-invoice-details .total-table {
        width: 100% !important;
        max-width: 100% !important;
        min-width: 100% !important;
    }

    .wrapper-invoice-template .elaborated-block,
    .wrapper-invoice-template .bold-h {
        font-weight: 700;
        font-size: 20px !important;
        text-transform: uppercase;
    }

    .wrapper-invoice-template .bold-h {
        font-size: 12px !important;
        text-transform: uppercase;
    }

    .wrapper-invoice-template  .pdf-add-cell > span {
        display: flex;
        justify-content: center;
        align-items: baseline;
    }
    .wrapper-invoice-template .add-cell-center {
        text-align: center;
        max-width: 308px;
        margin: 0 auto;
    }
    .wrapper-invoice-template .add-cell-center h2 {
        text-transform: capitalize;
        font-size: 24px;
        line-height: 30px;
        text-align: center;
        margin-bottom: 3px;
        color: #0e68b1;
    }

    .wrapper-invoice-template .add-cell-center span {
        display: flex;
        justify-content: center;
        align-items: baseline;
        text-align: center;
    }

    .wrapper-invoice-template .add-cell-center h3,
    .wrapper-invoice-template .add-cell-center p {
        font-size: 15px;
        line-height: 20px;
        display: inline-block;
        margin: 0rem;
    }

    .wrapper-invoice-template .add-cell-center h3 {
        font-weight: 700;
        margin-right: 4px;
    }

    .wrapper-invoice-template .add-cell-center span p + h3{
        margin-left: 8px;
    }

    .wrapper-invoice-template .add-cell-center span p,
    .wrapper-invoice-template .add-cell-center span p a {
        font-weight: 400;
    }
    </style>
</head>

<body id="body-invoice-template">
    <div class="wrapper-invoice-template">
        <table width="100%" cellpadding="0" cellspacing="0" id="print-invoice-details">
            <tr>
                <td style="border: none; padding: 0px;" width="100%">
                    <table>
                        <tr>
                            <td style="border: none; padding: 0px; width:33%;">

                                @if(Auth::user()->company->logo)
                                <span style="max-height:100%; max-width:100%; display:block; padding-right: 30px;">
                                    <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                        alt="{{ Auth::user()->name }}" />
                                </span>
                                @endif
                            </td>
                            <td style="text-align: center; border: none; width: 69%; vertical-align: top">
                                <div class="pdf-add-cell">
                                  <div class="add-cell-center">
                                   <h2>{{ Auth::user()->company->name }}</h2>
                                   @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                    @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3> <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->fax)
                                            <h3>{{ __('labels.fax') }}:</h3> <p>{{ Auth::user()->company->fax }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                        <span>
                                            <h3>{{ __('labels.email') }}:</h3> 
                                            <p><a href="mailto:{{Auth::user()->company->support_email}}"> {{ Auth::user()->company->support_email }} </a></p>
                                        </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                        <span>
                                            <h3>{{ __('labels.tax_id') }}:</h3> 
                                            <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                        </span>
                                    @endif
                                 </div>

                                </div>
                            </td>
                            <td style="text-align: center; border: none; vertical-align: top">{{ __('labels.invoice') }} <br>
                                <b> {{ __('labels.no') }}. {{ $invoice->internal_number }}</b> <br></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="blank-tr-space">
            </tr>
            <tr>
                <td style="border: none; margin: 0px; padding: 0px;">
                    <table class="client-table" cellpadding="0" cellspacing="0" style="border-top-left-radius: 15px;  border-top-right-radius: 15px;   box-shadow:0 0 0 1px #0e68b1;
                        overflow: hidden;">
                        <tr>
                            <th style=" text-align: right; width: 60px;">{{ __('labels.client') }}</th>
                            <td colspan="3">{{ $invoice->contact->name }}</td>
                            <th style="border-left: thin solid #0e68b1;"> {{ __('labels.issue_date') }} (dd / mm / yy) </th>
                        </tr>
                        <tr>
                            <th style="text-align: right; width: 60px;">{{ __('labels.tax_id') }}</th>
                            <td colspan="3">{{ $invoice->contact->tax_id }}</td>
                            <td style="text-align: center;"> {{ dateFormat($invoice->start_date) }} </td>
                        </tr>
                        <tr>
                            <th style="text-align: right; width: 60px">{{ __('labels.address') }}</th>
                            <td colspan="3">{{ addressFormat($invoice->contact->address) }}</td>
                            <th style="border-right: thin solid #a8a8a8; border-left: thin solid #a8a8a8;">{{ __('labels.expiration_date') }} (dd / mm / yy) </th>
                        </tr>
                        <tr>
                            <th style="text-align: right; width: 60px;">{{ __('labels.phone') }}</th>
                            <td class="blank-td">{{ $invoice->contact->phone }}</td>
                            <th>{{ __('labels.identification') }}</th>
                            <td class="blank-td">
                                {{ $invoice->contact->identificationInfo ? $invoice->contact->identificationInfo->name : '' }}
                            </td>
                            <td style="text-align: center;">{{ dateFormat($invoice->expiration_date) }}</td>
                        </tr>
                        @if($invoice->ncf)
                        <tr>
                            <th style="text-align: right; width: 60px;">{{ __('labels.ncf') }}</th>
                            <td class="blank-td">{{ showIfAvailable($invoice->ncf_number) }}</td>
                            <th>{{ __('labels.invoice_with') }}</th>
                            <td  class="blank-td">{{ showIfAvailable($invoice->ncf->name) }}</td>
                        </tr>
                        @endif
                    </table>
                </td>
            </tr>
            <tr class="blank-tr-space">
            </tr>
            <tr>
                <td style="border:none; margin: 0px; padding: 0px;">
                    <table class="item-table" cellpadding="0" cellspacing="0" style="border-top-left-radius: 15px;  border-top-right-radius: 15px;   box-shadow:0 0 0 1px #0e68b1;
                        overflow: hidden;">
                        <tr>
                            <th style="width: 400px; ">{{ __('labels.item') }}</th>
                            <th>{{ __('labels.reference') }}</th>
                            <th>{{ __('labels.price') }}</th>
                            <th>{{ __('labels.disc') }} %</th>
                            <th>{{ __('labels.tax') }}</th>
                            <th>{{ __('labels.tax_amount') }}</th>
                            <!-- <th>Description</th> -->
                            <th>{{ __('labels.quantity') }}</th>
                            <th>{{ __('labels.total') }}</th>
                        </tr>
                        @foreach($invoice->items as $item)
                        <tr>
                            <td style="width: 400px">{{$item->inventory->name}}</td>
                            <td style="text-align: center;">{{$item->inventory->reference}}</td>
                            <td style="text-align: center;">{{moneyFormat($item->price)}}</td>
                            <td style="text-align: center;">{{addZeros($item->discount)}}%</td>
                            <td style="text-align: center;">
                                {{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                            <td style="text-align: center;">{{moneyFormat($item->calculation['taxAmount'])}}</td>
                            <!-- <td style="text-align: center;">{{showIfAvailable($item->inventory->description)}}</td> -->
                            <td style="text-align: center;">{{$item->quantity}}</td>
                            <td style="text-align: center;">{{moneyFormat($item->calculation['total'])}}</td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>

                <td style="border: none; padding-top: 20px;">
                    <table class="total-table" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="border: none; margin: 0px; padding: 0px;" class="elaborated-block" colspan="3"
                                rowspan="{{count($invoice->calculation['taxInfo']) + 4}}">
                                <span
                                    style="border-bottom: thin solid #0e68b1; max-height:150px; max-width:300px; display:block; padding-bottom: 5px; margin-bottom: 5px;">
                                    @if(Auth::user()->company->signature)
                                    <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}"
                                        alt="{{ Auth::user()->name }}" />
                                    @endif
                                </span>
                                {{ __('labels.elaborated_by') }}
                            </td>
                            <td style="border: none; text-align: center;" class="bold-h">{{ __('labels.total') }}</td>
                            <td style="border:none; text-align: center;">
                                {{moneyFormat($invoice->calculation['baseTotal'])}}</td>
                        </tr>
                        <tr>
                            <td class="bold-h" style="border: none; text-align: center;">{{ __('labels.discount') }}</td>
                            <td style="border:none; text-align: center;">
                                {{moneyFormat($invoice->calculation['discountAmount'])}}</td>
                        </tr>
                        <tr>
                            <td class="bold-h" style="border: none; text-align: center;">{{ __('labels.sub_total') }}</td>
                            <td style="border:none; text-align: center;">
                                {{moneyFormat($invoice->calculation['subtotal'])}}</td>
                        </tr>
                        <tr class="tax_row">
                            <td class="bold-h" style="border: none; text-align: center;">{{ __('labels.tax') }}</td>
                            <td style="border: none; text-align: center;">
                                {{moneyFormat($invoice->calculation['taxAmount'])}}</td>
                        </tr>
                        {{-- @foreach($invoice->calculation['taxInfo'] as $taxInfo)
                            <tr class="tax_row">
                                <td class="bold-h" style="border: none; text-align: center;">{{$taxInfo['formatted_name']}}
                                </td>
                                <td style="border: none; text-align: center;">{{moneyFormat($taxInfo['tax'])}}</td>
                            </tr>
                        @endforeach --}}
            <tr>
                <th class="bold-h">{{ __('labels.total') }}</th>
                <th>{{moneyFormat($invoice->calculation['total'])}}</th>
            </tr>
        </table>
        </td>
        </tr>
        <tr class="blank-tr-space">
        </tr>
        <tr>
            <td style="border:none; margin: 0px; padding: 0px;" colspan="3">
                <table cellpadding="0" cellspacing="0" style="width: auto; margin: 0">
                    <tr>
                        <td style="border: none; width: 400px; margin: 0px; padding: 0px;">
                            <p><b>{{ __('labels.terms_and_conditions') }}: </b><span>{{ $invoice->tac }}</span></p><br />
                            <p><b>{{ __('labels.notes') }}: </b><span>{{ $invoice->notes }}</span></p><br />
                            <p><b>{{ __('labels.resolution_text') }}:</b><span>{{ $invoice->res_text }}</span></p><br />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        </table>
    </div>
</body>

</html>