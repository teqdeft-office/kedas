<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <title>{{ __('labels.invoice').'-'.$invoice->internal_number }}</title>
    <!-- CSS Reset : BEGIN -->
    <style type="text/css">
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    @media screen,
    print {/*

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            font-family: 'Raleway', sans-serif !important;
            width: 100%;
            height: 100%;
        }

        body {
            position: relative;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 16px !important;
        }

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        a {
            text-decoration: none;
        }

        a[x-apple-data-detectors],
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .im {
            color: inherit !important;
        }

        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        img.g-img+div {
            display: none !important;
        }

        html,
        table,
        tr,
        td {
            margin: 0;
            padding: 0;
        }

        h2 {
            margin: 0;
            padding: 0
        }

        .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        img {
            max-width: 100%
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #404040;
            text-decoration: none;
        }

        table th,
        table td {
            background: transparent;
            text-align: center;
            border: 2px solid #000;
            color: #404040;
            font-size: 14px;
            line-height: 17px;
            font-weight: 600 !important;
        }

        table strong {
            font-weight: 800 !important;
            color: #404040;
            display: inline-block;
        }

        table th {
            white-space: nowrap;
            font-weight: 700 !important;
        }

        table td {
            text-align: right;
            padding: 20px;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr td:first-child {
            border: none;
        }

        #main-invoice-table thead th tr:last-child {
            font-size: 15px !important;
            color: #ffffff;
            font-weight: 700 !important;
            background: #404040;
            border-top: none;
            border-bottom: none;
            color: #fff;
        }

        #main-invoice-table tbody td {
            border: none;
            text-align: center;
            padding: 3px;
            font-weight: 600 !important;
        }

        #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700;
            font-size: 14px !important;
            padding: 3px;
        }

        #main-invoice-table>tbody>tr>td {
            border: 2px solid #000;
            border-style: solid;
            border-bottom: none;
            border-top: none;
        }

        #main-invoice-table tbody {
            border-bottom: 2px solid #000;
        }

        #divider td {
            border: none !important;
        }

        #comapny-name td,
        #client-name td,
        #client-name td a,
        #comapny-name td a {
            font-size: 14px !important;
            color: #404040;
            line-height: 20px;
            font-weight: 600 !important;
        }

        #main-invoice-table tfoot td {
            color: #404040;
            text-transform: uppercase;
        }


        #top-table tr td {
            background: #d5e0ea;
            border-right: 2px solid #fff;
        }

        #top-table tr:last-child td {
            color: #fff;
            background: #404040;
        }


        #top-table tr:first-child td {
            background: #43a1ed;
            color: #fff;
            font-weight: 700 !important;
        }

        #main-invoice-table>tbody>tr:nth-child(odd) {
            background: #d9d9d9;
        }

        #main-invoice-table td {
            vertical-align: middle;
            font-size: 14px;
            line-height: 20px;
        }

        #main-invoice-table>tbody>tr>td {
            text-align: left;
        }

        #main-invoice-table>tbody>tr>td:last-child,
        #main-invoice-table>tbody>tr>td:nth-child(6) {
            text-align: right;
        }

        .inovice-top-wrap {
            text-align: right;
            display: inline-block;
            width: 198px !important;
            margin-top: 6px;
        }

        .inovice-top-wrap span {
            display: block;
            font-size: 14px;
            line-height: 17px;
            text-align: left;
            width: 100%;
            font-weight: 600;
        }

        .inovice-top-wrap span>span {
            display: inline-block;
            width: auto;
        }

        #main-invoice-table th {
            background: #002060;
            color: #fff;
            padding: 10px;
            text-transform: uppercase;
        }

        #main-invoice-table tr td:last-child {
            background: #d9d9d9;
        }

        .bg-inherit {
            background: inherit !important;
        }

        .text-align-right {
            text-align: right !important;
        }

        .text-align-left {
            text-align: left !important;
        }

        .text-align-center {
            text-align: center !important;
        }
        .inovice-top-wrap table td {
            padding: 0 !important;
            text-align:left;
        }
    */}

    @media (max-width:600px) {
        .responsive-table {
            width: 100%;
            min-width: 100% !important;
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }
    }

    @media (max-width:1300px) {


        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            font-family: 'Raleway', sans-serif !important;
            width: 100%;
            height: 100%;
        }

        body {
            position: relative;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        a[x-apple-data-detectors],
        /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img+div {
            display: none !important;
        }

        html,
        table,
        tr,
        td {
            margin: 0;
            padding: 0;
        }

        h2 {
            margin: 0;
            padding: 0
        }

        .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        img {
            max-width: 100%
        }

        .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #404040;
            text-decoration: none;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #404040;
            text-decoration: none;
        }

        table th,
        table td {
            background: transparent;
            text-align: center;
            border: 2px solid #000;
            color: #404040;
            font-size: 14px;
            line-height: 17px;
            font-weight: 600 !important;
        }

        table strong {
            font-weight: 800 !important;
            color: #404040;
        }

        table th {
            white-space: nowrap;
            font-weight: 700 !important;
        }

        table td {
            text-align: right;
            padding: 20px;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr td:first-child {
            border: none;
        }

        #main-invoice-table thead th tr:last-child {
            font-size: 15px !important;
            color: #ffffff;
            font-weight: 700 !important;
            background: #404040;
            border-top: none;
            border-bottom: none;
            color: #fff;
        }

        #main-invoice-table tbody td {
            border: none;
            text-align: center;
            padding: 3px;
            font-weight: 600 !important;
        }

        #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700;
            font-size: 14px !important;
            padding: 3px;
        }

        #main-invoice-table>tbody>tr>td {
            border: 2px solid #000;
            border-style: solid;
            border-bottom: none;
            border-top: none;
        }

        #main-invoice-table tbody {
            border-bottom: 2px solid #104876;
        }

        #divider td {
            border: none !important;
        }

        #comapny-name td,
        #client-name td,
        #client-name td a,
        #comapny-name td a {
            font-size: 14px !important;
            color: #404148;
            font-weight: 600 !important;
        }

        #main-invoice-table tfoot td {
            color: #404040;
            text-transform: uppercase;
        }


        #top-table tr td {
            background: #d5e0ea;
            border-right: 2px solid #fff;
        }

        #top-table tr:last-child td {
            color: #fff;
            background: #404040;
        }


        #top-table tr:first-child td {
            background: #43a1ed;
            color: #fff;
            font-weight: 700 !important;
        }

        #main-invoice-table>tbody>tr:nth-child(odd) {
            background: #d9d9d9;
        }

        #main-invoice-table td {
            vertical-align: middle;
            font-size: 14px;
            line-height: 17px;
        }

        #main-invoice-table>tbody>tr>td {
            text-align: left;
        }

        #main-invoice-table>tbody>tr>td:last-child,
        #main-invoice-table>tbody>tr>td:nth-child(6) {
            text-align: right;
        }

        .inovice-top-wrap {
            text-align: right;
            display: inline-block;
            width: 198px;
            margin-top: 6px;
        }

        .inovice-top-wrap span {
            display: block;
            font-size: 14px;
            line-height: 17px;
            text-align: left;
            width: 119px;
            font-weight: 600;
        }

        #main-invoice-table th {
            background: #002060;
            color: #fff;
            padding: 10px;
            text-transform: uppercase;
        }

        #main-invoice-table tr td:last-child {
            background: #d9d9d9;
        }

        .text-align-right {
            text-align: right !important;
        }

        .text-align-left {
            text-align: left !important;
        }

        .text-align-center {
            text-align: center !important;
        }

        .inovice-top-wrap span>span {
            display: inline-block;
            width: auto;
        }

        .inovice-top-wrap table td {
            padding: 0 !important;
        }

        .inovice-top-wrap table td {
            padding: 0 !important;
            text-align:left;
        }
         .bg-inherit {
            background: inherit !important;
        }

    }

    </style>
</head>

<body class="template-invoice-nine">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
        <tr>
            <td align="center" valign="top">
                <!--[if (mso)|(IE)]>

               <table width="600" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                     <td align="left" valign="top" width="100%">

            <![endif]-->

                <!--[if mso 16]>

               <table width="600" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                     <td align="left" valign="top" width="100%">

            <![endif]-->
                <table width="100%" style="max-width:1100px; margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding: 20px 0;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align:left; padding:0;">
                                        @if($user->company->logo)
                                        <img id="image-preview" src="{{ asset($user->company->logo) }}"
                                            alt="{{ $user->company->name }}" style="width: 141px;" />
                                        @endif
                                    </td>
                                    <td id="invoice-top"
                                        style="text-align:right; padding:0; font-size: 34px; color: #404040;  line-height: 41px;">
                                        <h2 style="color:#a5a2a2; font-weight: 400; text-transform:uppercase;">
                                            {{ __('labels.invoice') }}</h2>
                                        <div class="inovice-top-wrap">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="font-weight: 700 !important; font-size: 14px; line-height: 20px; text-transform: uppercase;">{{ __('labels.date') }}:</td>
                                                        <td style="font-weight: 600 !important; font-size: 12px;"> {{ dateFormat($invoice->start_date) }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-weight: 700 !important; font-size: 14px; line-height: 20px; text-transform: uppercase;">{{ __('labels.invoice') }} #:</td>
                                                        <td style="font-weight: 600 !important; font-size: 12px;"> {{ $invoice->internal_number }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-weight: 700 !important; font-size: 14px; line-height: 20px; text-transform: uppercase;">{{ __('labels.invoice_with') }}:</td>
                                                        <td style="font-weight: 600 !important; font-size: 12px;">{{ $invoice->ncf ? showIfAvailable($invoice->ncf->name) : 'N/A' }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-weight: 700 !important; font-size: 14px; line-height: 20px; text-transform: uppercase;">{{ __('labels.ncf') }}:</td>
                                                        <td style="font-weight: 600 !important; font-size: 12px;">{{ $invoice->ncf ? showIfAvailable($invoice->ncf_number) : 'N/A' }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-weight: 700 !important; font-size: 14px; line-height: 20px; text-transform: uppercase;">{{ __('labels.exp_date') }}:</td>
                                                        <td style="font-weight: 600 !important; font-size: 12px;">{{ dateFormat($invoice->expiration_date) }}</td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding:0px 0 20px 0;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                <tbody>
                                    <tr>
                                        <td style="text-align:left; width: 65%; padding:0; vertical-align: text-top;"
                                            id="comapny-name">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <h2
                                                                style="font-size: 27px; line-height: 39px; color: #404040; text-transform:uppercase;  line-height: 34px;">
                                                                {{ $user->company->name }}
                                                            </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if(addressFormat($user->company->address, 1)[0]
                                                            ??
                                                            null)
                                                            <span>{{ addressFormat($user->company->address, 1)[0] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if(addressFormat($user->company->address, 1)[1]
                                                            ??
                                                            null)
                                                            <span>{{ addressFormat($user->company->address, 1)[1] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <span>{{ __('labels.phone') }}:
                                                                {{ $user->company->phone }}
                                                                @if($user->company->fax)
                                                                {{ __('labels.fax') }}:
                                                                {{ $user->company->fax }} @endif
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($user->company->support_email)
                                                            <span>{{ __('labels.support_email') }}: <a
                                                                    href="mailto:{{$user->company->support_email}}">{{$user->company->support_email}}</a></span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($user->company->tax_id)
                                                            <span>{{ __('labels.tax_id') }}:
                                                                {{ $user->company->tax_id }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>

                                        <td style="text-align:left; width: 35%; padding:0; vertical-align: text-top;"
                                            id="client-name">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <h2
                                                                style="font-size: 17px; color: #404040; line-height: 34px;">
                                                                {{ $invoice->contact->name }}
                                                            </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->address)
                                                            <span>{{ addressFormat($invoice->contact->address, 1)[0] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->address &&
                                                            addressFormat($invoice->contact->address, 1)[1] ?? null)
                                                            <span>{{ addressFormat($invoice->contact->address, 1)[1] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <span>@if($invoice->contact->phone)
                                                                {{ __('labels.phone') }}:
                                                                {{ $invoice->contact->phone }}
                                                                @endif @if($invoice->contact->mobile)
                                                                {{ __('labels.mobile') }}:
                                                                {{ $invoice->contact->mobile }}
                                                                @endif</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->email)
                                                            <span>{{ __('labels.email') }}: <a
                                                                    href="mailto:{{$invoice->contact->email}}">{{$invoice->contact->email}}</a></span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->tax_id)
                                                            <span>{{ __('labels.tax_id') }}:
                                                                {{ $invoice->contact->tax_id }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>


                    <tr>
                        <td style="padding:0;">
                            <table id="main-invoice-table" width="100%" border="0" cellspacing="0" cellpadding="0">

                                <thead>
                                    <tr>
                                        <th>{{ __('labels.item') }} {{ __('labels.name') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.disc') }}%</th>
                                        <th>{{ __('labels.tax_amount') }}</th>
                                        <th>{{ __('labels.price') }}</th>
                                        @endif
                                        <th>{{ __('labels.qty') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.total') }}</th>
                                        @endif
                                    </tr>

                                </thead>
                                <tbody>
                                    @foreach($invoice->items as $key => $item)
                                    <tr>
                                        <td class="text-align-left">{{$item->inventory->name}}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td class="text-align-right">{{addZeros($item->discount,null,2,true)}}%</td>
                                        <td class="text-align-right">
                                            {{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                        <td class="text-align-right">{{moneyFormat($item->price,null,2,true)}}</td>
                                        @endif

                                        <td class="text-align-right {{ ($viewType ?? null) == 'production-order' ? 'bg-inherit' : '' }}">{{$item->quantity}}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td class="text-align-right">
                                            {{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                        @endif
                                    </tr>
                                    @endforeach

                                </tbody>
                                @if(($viewType ?? null) != 'production-order')
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.sub_total') }}
                                        </td>
                                        <td style="border: 2px solid #000; text-align:right; ">
                                            {{moneyFormat($invoice->calculation['subtotal'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.tax') }}
                                        </td>
                                        <td style="border: 2px solid #000; text-align:right;  background: #fff;">
                                            {{moneyFormat($invoice->calculation['taxAmount'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.total') }} {{ __('labels.discount') }}
                                        </td>
                                        <td style="border: 2px solid #000; text-align:right; background: #d9d9d9;">
                                            {{moneyFormat($invoice->calculation['discountAmount'],null,2,true)}}
                                        </td>
                                    </tr>


                                    <tr>
                                        <td colspan="3">
                                        </td>
                                        <td colspan="2" style="text-align:right; font-weight: 700 !important;">
                                            {{ __('labels.total') }}
                                        </td>
                                        <td style="border: 2px solid #000; text-align:right;  background: #d9d9d9;">
                                            {{moneyFormat($invoice->calculation['total'],null,2,true)}}
                                        </td>
                                    </tr>
                                </tfoot>
                                @endif
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td style="padding:0;">
                            <table width="400px" style="margin:0 auto;" border="0" cellspacing="0" cellpadding="0"
                                id="divider">
                                <tbody>
                                    <tr>
                                        <td
                                            style="padding:0; font-size: 14px;  border-bottom: 2px solid #000000 !important; text-align:center; padding:0; font-weight: 700;">
                                            @if($user->company->signature)
                                            <img style="width: 193px; display:block; margin: 20px auto;"
                                                id="signature-preview" src="{{ asset($user->company->signature) }}"
                                                alt="{{ $user->company->name }}" />@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td
                                            style="text-align:center; padding:5px 0 0 0; text-transform: capitalize !important;">
                                            {{ __('labels.elaborated_by') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <table width="auto" style="margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        @if($invoice->type == 'recurring')
                        <td
                            style="text-align:left; padding: 40px 0px 0px 0px; font-weight: 700 !important; font-size: 14px; line-height: 20px; color: #9bbb59;">
                            <strong
                                style="font-weight: 700 !important; color: #404040; margin-right: 5px;">{{ __('labels.observations') }}: </strong>
                            {{ $invoice->observation }}
                        </td>
                        @else
                        <td
                            style="text-align:left; padding: 40px 0px 0 0px; font-weight: 700 !important; font-size: 14px; line-height: 20px;color: #9bbb59;">
                            <strong
                                style="font-weight: 700 !important; color: #404040; margin-right: 5px;">{{ __('labels.terms_and_conditions') }}: </strong>{{ $invoice->tac }}
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td
                            style="text-align:left; padding: 0; font-weight: 700 !important; font-size: 14px; line-height: 20px; color: #9bbb59;">
                            <strong style="font-weight: 700 !important; color: #404040; margin-right: 5px;">{{ __('labels.notes') }}: </strong>{{ $invoice->notes }}
                        </td>
                    </tr>
                    <tr>
                        <td
                            style="text-align:left;  padding: 0; font-weight: 700 !important; line-height: 20px; font-size: 14px; color: #9bbb59;">
                            <strong
                                style="font-weight: 700 !important; color: #404040;margin-right: 5px;">{{ __('labels.resolution_text') }}: </strong>{{ $invoice->res_text }}
                        </td>
                    </tr>
                    <tr>
                        <td
                            style="text-align:left;  padding: 0; font-weight: 700 !important; line-height: 20px; font-size: 14px; color: #9bbb59;">
                            <strong
                                style="font-weight: 700 !important; color: #404040; margin-right: 5px;">{{ __('labels.privacy_policy') }}: </strong>{{ $invoice->privacy_policy }}
                        </td>
                    </tr>
                </tbody>
                </table>
                    <!--[if mso 16]>

            </td>

         </tr>

      </table>

            <![endif]-->

                    <!--[if (mso)|(IE)]>

            </td>

         </tr>

      </table>

            <![endif]-->
            </td>
        </tr>
    </table>

</body>

</html>
