@extends('layouts.admin.dashboard')

@section('title', trans('labels.dashboard'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="dashboard-max-w">
        <h2>{{ __('labels.dashboard') }}</h2>
    </div>
</div>
<div class="clearfix"></div>
@endsection
