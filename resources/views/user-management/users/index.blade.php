@inject('controller', 'App\Http\Controllers\Controller')

@extends('layouts.dashboard')

@section('title', trans('labels.users'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.users') }}</h2>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('user-management.users.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label') }} {{ __('labels.user') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="users-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.email') }}</th>
                                    <th>{{ __('labels.role') }}</th>
                                    <th class="to-show">{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ ucfirst($user->role() ? $user->role()->custom_name : trans('N/A')) }}</td>
                                    <td class="to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('user-management.users.edit', $user)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-user-form-' . $user->id }}" href="{{route('user-management.users.destroy', $user)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['user-management.users.destroy', $user], 'id' => "destroy-user-form-{$user->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="4" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.user') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('user-management.users.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label') }} {{ __('labels.user') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
