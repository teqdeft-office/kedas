@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.user'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit').' '.__('labels.user') }}</h2>
        {{ Form::open(array('method'=>'PUT','route' => ['user-management.users.update', $company_user], 'id' => 'edit-company-user-form')) }}
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name', $company_user->name) }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">{{ __('labels.email') }} <span>*</span></label>
                            <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email') }}" required value="{{ old('email', $company_user->email) }}" autocomplete="off" />
                            @error('email')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">{{ __('labels.password') }}</label>
                            <input class="form-control" type="password" name="password" placeholder="{{ __('labels.password') }}" autocomplete="off" id="password" />
                            @error('password')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">{{ __('labels.confirm_password') }}</label>
                            <input class="form-control" type="password" name="password_confirmation" placeholder="{{ __('labels.confirm_password') }}" autocomplete="off" id="password-confirm" />
                            @error('password_confirmation')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">{{ __('labels.role') }}</label>
                            {!! Form::select('role', $role_options, old('role', ($assignedRole ? $assignedRole->id : null)), ['class'=>"form-control", 'id'=>"role"]) !!}
                            @error('role')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">{{ __('labels.permissions') }} </label>
                            {!! Form::select('permissions', $permission_options, old('permissions', ($assignedPermissions ?: null)), ['class'=>"form-control multiple-search-selection", 'multiple'=>'multiple', 'name'=>'permissions[]']) !!}
                            @error('permissions')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('user-management.users') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
