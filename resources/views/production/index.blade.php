@extends('layouts.dashboard')
@section('title', trans('labels.invoices'))
@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.orders_production') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.orders_production') }},</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
        </div>

        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.filter') }}: <input id="order-production-filter" type="text" autocomplete="off"  value=""></span>
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text"
                                autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text"
                                autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table responsive-table">
                        <table class="table table-striped" id="order-production-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.estimated_delivery_date') }}</th>
                                    <th>{{ __('labels.terms') }}</th>
                                    <th>{{ __('labels.notes') }}</th>
                                    <th>{{ __('labels.invoice_number') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($productionOrders as $order)
                                <tr>
                                    <td>{{ $order->invoice->contact->name }}</td>
                                    <td>{{ dateFormat($order->invoice->start_date) }}</td>
                                    <td>{{ dateFormat($order->invoice->expiration_date) }}</td>
                                    <td>{{ $order->invoice->term }}</td>
                                    <td>{{ $order->invoice->notes }}</td>
                                    <td>{{ $order->invoice->internal_number }}</td>
                                    <td>{{ __('labels.' . $order->status) }}</td>
                                    <td class="">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a
                                                            href="{{route('invoices.show', $order)}}">{{ __('labels.view') }}</a>
                                                    </li>
                                                    <li><a
                                                            href="{{route('invoices.edit', $order)}}">{{ __('labels.edit') }}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message">
                                            <p>{{ __('labels.you_have_not_yet_created').' '.__('labels.invoice') }}!</p>
                                        </div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('invoices.create', ['type' => $type]) }}"
                                                class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i>
                                                {{ __('labels.new').' '.__('labels.invoice') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
