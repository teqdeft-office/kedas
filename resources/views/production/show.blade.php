@extends('layouts.dashboard')

@section('title', trans('labels.show') . ' ' . trans('labels.reception'))

@section('content')
    <div class="dashboard-right">
        <div class="menu-icon">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div class="invoice-container user-invoice-view-details">
            <a class="arrow-back d-block" href="{{ route('production.index') }}">{{ __('labels.back') }}</a>
            <h2>{{ __('labels.invoice') }} # {{ titleCase($production->invoice->internal_number) }} </h2>
            <div class="default-form view-form-detail">


                     <div class="invoice-detail-count recurring-invoice-details white-bg">
                        <div class="invoice-detail-count-item">
                            <span class="title">{{ __('labels.invoice_number') }}</span>
                            <span class="value total-value">{{ $production->invoice->internal_number }}</span>
                        </div>

                        <div class="invoice-detail-count-item">
                            <span class="title">{{ __('labels.creation_date') }}</span>
                            <span class="value paid">{{ dateFormat($production->invoice->start_date) }}</span>
                        </div>
                        <div class="invoice-detail-count-item">
                            <span class="title">{{ __('labels.estimated_delivery_date') }}</span>
                            <span class="value pending">{{ dateFormat($production->invoice->expiration_date) }}</span>
                        </div>
                    </div>

                    <div class="view-form-custom view-form-recurring white-bg sales-recurring-view">
                        <table class="table" style="max-width: 100%">
                            <tr>
                                <td><b>{{ __('labels.contact') }}</b></td>
                                <td>{{$production->invoice->contact->name}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Notes</b> </td>
                                <td>{{ $production->invoice->notes }}</td>

                            </tr>
                            <tr>
                                <td><b>{{ __('labels.seller') }}</b></td>
                                <td>{{ $production->invoice->user->name }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Condition</b></td>
                                <td>{{ $production->invoice->tac }}</td>
                            </tr>
                            <tr>
                                <td><b>{{ __('labels.creation_date') }}</b></td>
                                <td>{{ dateFormat($production->invoice->start_date) }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td> <b>{{__('labels.expiration_date')}}</b> </td>
                                <td>{{dateFormat($production->invoice->expiration_date)}} </td>
                                <td></td>
                                <td></td>
                                <td><b></b></td>
                                <td></td>
                                <td></td>
                            </tr>

                        </table>
                    </div>




                <div class="generate-invoice reception-view">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="list-table-custom  products-table">
                                <table class="table table-striped" id="reception-table">
                                    <thead>
                                        <tr>
                                            <th>{{ __('labels.item_name') }}</th>
                                            <th>{{ __('labels.reference') }}</th>
                                            <th>{{ __('labels.size') }}</th>
                                            <th>{{ __('labels.type_of_fabric') }}</th>
                                            <th>{{ __('labels.description') }}</th>
                                            <th>{{ __('labels.quantity') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($production->invoice->items as $key => $item)
                                        <tr>
                                            <td>{{$item->inventory->name}}</td>
                                            <td>{{$item->inventory->reference ?:'N/A' }}</td>
                                            <td>{{ 'N/A' }}</td>
                                            <td>{{ 'N/A' }}</td>
                                            <td>{{$item->inventory->description ?:'N/A' }}</td>
                                            <td>{{$item->quantity ?: 'N/A'}}</td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="form-group action-btns-view pull-right">

                    <a class="btn-custom update_status" data-resource="{{ 'mark_in_progress-' . $production->id }}" href="{{route('production.update', $production)}}">{{ __('labels.mark_in_progress') }}</a></li>
                    {{ Form::open(array('method'=>'PUT','route' => ['production.update', $production], 'id' => "mark_in_progress-{$production->id}", 'style' => 'display: none')) }}
                    <input type="hidden" name="status" value="mark_in_progress">
                    {!! Form::close() !!}

                    <a class="btn-custom update_status" data-resource="{{ 'mark_as_pending-' . $production->id }}" href="{{route('production.update', $production)}}">{{ __('labels.mark_as_pending') }}</a></li>
                    {{ Form::open(array('method'=>'PUT','route' => ['production.update', $production], 'id' => "mark_as_pending-{$production->id}", 'style' => 'display: none')) }}
                    <input type="hidden" name="status" value="mark_as_pending">
                    {!! Form::close() !!}

                    <a class="btn-custom update_status" data-resource="{{ 'mark_as_done-' . $production->id }}" href="{{route('production.update', $production)}}">{{ __('labels.mark_as_done') }}</a></li>
                    {{ Form::open(array('method'=>'PUT','route' => ['production.update', $production], 'id' => "mark_as_done-{$production->id}", 'style' => 'display: none')) }}
                    <input type="hidden" name="status" value="mark_as_done">
                    {!! Form::close() !!}

                    <a class="btn-custom update_status" data-resource="{{ 'mark_as_confirmed-' . $production->id }}" href="{{route('production.update', $production)}}">{{ __('labels.mark_as_confirmed') }}</a></li>
                    {{ Form::open(array('method'=>'PUT','route' => ['production.update', $production], 'id' => "mark_as_confirmed-{$production->id}", 'style' => 'display: none')) }}
                    <input type="hidden" name="status" value="mark_as_confirmed">
                    {!! Form::close() !!}

                </div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
@endsection

