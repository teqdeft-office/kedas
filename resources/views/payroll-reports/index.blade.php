@extends('layouts.dashboard')

@section('title', 'Payroll Reports')

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>Payroll Reports</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">If you need help creating a Payroll Reports.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style=""><div class="dt-buttons">
            <button class="dt-button buttons-excel buttons-html5 btn-custom" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Mark Selection As Paid</span></button> 
            <button class="dt-button buttons-excel buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in excel</span></button> 
            <button class="dt-button buttons-pdf buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in pdf</span></button> 
            <button class="dt-button buttons-pdf buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Print</span></button>
            </div>
            </div>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <div class="table table-responsive">
                      <table class="table table-striped" id="with-holdings-list-table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="checkbox">
                                            <input type="checkbox" name="checlall" id="checlall">
                                            <label for="checlall"></label>
                                        </div>
                                    </th>
                                    <th>Employee</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Base Salary</th>
                                    <th>Comissions</th>
                                    <th>Vacation</th>
                                    <th>Ex Hrs</th>
                                    <th>Bonus</th>
                                    <th>ISR</th>
                                    <th>HI</th>
                                    <th>Others</th>
                                    <th>AFP</th>
                                    <th>Uniform</th>
                                    <th>Cash Adv.</th>
                                    <th>Misc</th>
                                    <th>Total Disc</th>
                                    <th>Net Payment</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($employees as $key => $employee)
                                <tr>
                                    <td>
                                        <div class="checkbox">
                                            <input type="checkbox" name="item1" id="{{ $employee->id }}">
                                            <label for="{{ $employee->id }}"></label>
                                        </div>
                                    </td>
                                    <td>{{ $employee->name }}</td>
                                    <td>{{ dateFormat($employee->hiring_date) }}</td>
                                    <td>{{ dateFormat($employee->hiring_date) }}</td>
                                    <td>{{ moneyFormat($employee->salary) }}</td>
                                    <td>{{ moneyFormat($employee->all_benefits['commission']) }}</td>
                                    <td>{{ moneyFormat($employee->all_benefits['vacation']) }}</td>
                                    <td>{{ moneyFormat($employee->extra_hours_payment) }}</td>
                                    <td>{{ moneyFormat($employee->all_benefits['bonus']) }}</td>
                                    <td>{{ moneyFormat($employee->all_with_holdings['ISR']) }}</td>
                                    <td>{{ moneyFormat($employee->all_with_holdings['HI']) }}</td>
                                    <td>{{ moneyFormat($employee->all_with_holdings['Others']) }}</td>
                                    <td>{{ moneyFormat($employee->all_with_holdings['AFP']) }}</td>
                                    <td>{{ moneyFormat($employee->all_discounts['uniform']) }}</td>
                                    <td>{{ moneyFormat($employee->all_discounts['cash_advance']) }}</td>
                                    <td>{{ moneyFormat($employee->all_discounts['misc']) }}</td>
                                    <td>{{ moneyFormat($employee->all_discounts['total']) }}</td>
                                    <td>{{ moneyFormat($employee->net_payment) }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a class="paid_resource" data-resource="{{ 'paid-employee-form-' . $employee->id }}" href="{{route('employees.paid', $employee)}}">{{ __('labels.paid') }}</a></li>
                                                    {{ Form::open(array('method'=>'POST','route' => ['employees.paid', $employee], 'id' => "paid-employee-form-{$employee->id}", 'style' => 'display: none')) }}
                                                        <input type="hidden" name="total_payment" value="{{ $employee->net_payment }}" />
                                                        <input type="hidden" name="employee_id" value="{{ $employee->id }}" />
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="7" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.benefit') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('benefits.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.benefit') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse                                                    
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
