@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.leave_application'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.leave_application') }}</h2>
        <form method="POST" action="{{ route('leave-applications.store') }}" id="add-edit-leave-application-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            <label for="leave_type" class="col-sm-12 creat-nw-cat">{{ __('labels.leave_type') }} <span>*</span> <a target="_blank" href="{{ route('leave-types.create') }}">{{ __('labels.create_new_leave_type') }}</a></label>
                            {!! Form::select('leave_type_id', $leave_type_options, '', ['class'=>"form-control single-search-selection leave-application-type-drop", 'id'=>"leave_type_id"]) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="current_balance">{{ __('labels.current_balance') }} <span>*</span></label>
                            <input class="form-control" id="leaves_current_balance" type="text" name="current_balance" placeholder="{{ __('labels.current_balance') }}" required value="{{ old('current_balance') }}" autocomplete="off" readonly />
                            @error('current_balance')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.from_date') }} <span>*</span></label>
                            <input id="leave_from_date" class="form-control" name="from_date"
                                required="required" value="{{old('from_date')}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.to_date') }} <span>*</span></label>
                            <input id="leave_to_date" class="form-control" name="to_date"
                                required="required" value="{{old('to_date')}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="number_of_days">{{ __('labels.number_of_days') }} <span>*</span></label>
                            <input class="form-control" type="text" name="number_of_days" placeholder="{{ __('labels.number_of_days') }}" required value="{{ old('number_of_days') }}" autocomplete="off" readonly id="number_of_days" />
                            @error('number_of_days')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="purpose">{{ __('labels.purpose') }} <span>*</span></label>
                            <textarea class="form-control" name="purpose"
                                id="purpose">{{ old('purpose', null) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('item-categories.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
