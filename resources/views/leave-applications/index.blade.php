@extends('layouts.dashboard')

@section('title', trans('labels.leave_applications'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.leave_applications') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.apply_for_leave') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('leave-applications.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.apply_for_leave') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="leave-applications-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.leave_type') }}</th>
                                    <th>{{ __('labels.request_duration') }}</th>
                                    <th>{{ __('labels.number_of_days') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($leaveApplications as $application)
                                <tr>
                                    <td>{{ $application->leaveType->name }}</td>
                                    <td>{{ dateFormat($application->application_from_date) }} <b>{{ __('labels.to') }}</b> {{ dateFormat($application->application_to_date) }}<br/><span class="text-muted">{{ __('labels.application_date') }} : {{ dateFormat($application->created_at) }}</span></td>
                                    <td>{{ $application->number_of_days }}</td>
                                    <td>{{ $application->status_label }}</td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.leave_application') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('leave-applications.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.apply_for_leave') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
