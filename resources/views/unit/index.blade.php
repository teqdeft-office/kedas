

@extends('layouts.dashboard')

@section('title', trans('labels.measurement_unit'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.measurement_unit') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.measurement_unit') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            {{-- <a href="{{ route('import_resource', ['resource' => 'units']) }}" class="btn-custom outline-btn"><i
                    class="fa fa-upload" aria-hidden="true"></i> {{ __('labels.import_from_excel') }}</a>
            <a href="{{ route('export_resource', ['resource' => 'units']) }}" class="btn-custom outline-btn"><i
                    class="fa fa-download" aria-hidden="true"></i> {{ __('labels.export') }}</a> --}}
            <a href="{{ route('units.create') }}" class="btn-custom" id="create-new-unit-btn"><i
                    class="fa fa-plus" aria-hidden="true"></i>
                {{ __('labels.new').' '.__('labels.measurement_unit') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="unit-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.measurement_unit') }}</th>
                                    <th>{{ __('labels.symbol') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($units as $unit)
                                <tr>
                                    <td>{{ $unit->unit }}</td>
                                    <td>{{ $unit->shortcut }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a
                                                            href="{{route('units.show', $unit)}}">{{ __('labels.view') }}</a>
                                                    </li>
                                                    <li><a
                                                            href="{{route('units.edit', $unit)}}">{{ __('labels.edit') }}</a>
                                                    </li>
                                                    <li><a class="delete_resource"
                                                        data-resource="{{ 'destroy-inventory-form-' . $unit->id }}"
                                                        href="{{route('units.destroy', $unit)}}">{{ __('labels.delete') }}</a>
                                                </li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['units.destroy', $unit], 'id' => "destroy-inventory-form-{$unit->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection

<div class="welcome-popup-outer" id="inventory-type-container" style="display: none;">
    <!--step1-->
    <form id="inventory-type-form" onsubmit="return false;">
        <div class="steps-form step1">
            <div class="welcome-popup-bottom">
                <div class="welcome-msg text-center">
                    <h2>{{ __('labels.select_inventory_type') }}</h2>
                    <p></p>
                </div>
                <div class="custom-radio-container">
                    <div class="custom-radio">
                        <input type="radio" name="inventory_type" value="inventory" id="inventory">
                        <label for="inventory">
                            <img src="{{ asset('/images/f-label-1.png') }}">
                            {{ __('labels.inventory') }}
                        </label>
                    </div>
                    <div class="custom-radio">
                        <input type="radio" name="inventory_type" value="services" id="services">
                        <label for="services">
                            <img src="{{ asset('/images/f-label-2.png') }}">
                            {{ __('labels.services') }}
                        </label>
                    </div>
                    <div class="custom-radio">
                        <input type="radio" name="inventory_type" value="non-inventory" id="non_inventory">
                        <label for="non_inventory">
                            <img src="{{ asset('/images/f-label-1.png') }}">
                            {{ __('labels.non_inventory') }}
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

