@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.measurement_unit'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.measurement_unit') }}</h2>
        <form method="POST" action="{{ route('units.store') }}" id="add-edit-unit-form">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="unit">{{ __('labels.measurement_unit') }} <span>*</span></label>
                            <input class="form-control" type="text" name="unit" placeholder="{{ __('labels.measurement_unit') }}" required value="{{ old('unit') }}" autocomplete="off" />
                            @error('unit')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="shortcut">{{ __('labels.symbol') }} <span>*</span></label>
                            <input class="form-control" type="text" name="shortcut" placeholder="{{ __('labels.symbol') }}" required value="{{ old('shortcut') }}" autocomplete="off" />
                            @error('shortcut')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('units.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
