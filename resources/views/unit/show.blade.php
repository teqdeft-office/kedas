@extends('layouts.dashboard')

@section('title', trans('labels.show').' '.trans('labels.measurement_unit'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('units.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.measurement_unit').' '.__('labels.details') }}</h2>
        <div class="default-form view-form-detail item-detail-view view-details-product-ser">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="col-md-8 view-details-product-cell">
                        <div class="form-outer">
                            <div class="form-group">
                                <label for="tax"><b>{{ __('labels.measurement_unit') }} </b></label>
                                <span class="value-form">{{ $unit->unit }}</span>
                            </div>
                        </div>
                        <div class="form-outer">
                            <div class="form-group">
                                <label for="sale_price"><b>{{ __('labels.symbol') }} </b></label>
                                <span class="value-form">{{ $unit->shortcut }}</span>
                            </div>
                        </div>
                    </div>
            </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
