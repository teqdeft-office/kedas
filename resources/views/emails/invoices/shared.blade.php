@extends('emails.layout')
@section('content')
<tr>
	<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
			<tr>
				<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.hi') }},</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>{{ $invoice->company->name }}</b> {{ __('labels.shared_an') }} <b>{{ __('invoice '.$invoice->internal_number) }}</b> {{ __('labels.with_you') }}.</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.find_attachment_for_invoice_details') }}.</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"></p>
					<!-- Table section -->
					<!-- End of Table section -->
					<br>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.regards') }}, <br />{{ config('app.name', 'Kedas') }}</p>
				</td>
			</tr>
		</table>
	</td>
</tr>
@endsection