<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- CSRF Token -->
        <title>{{ config('app.name', 'Kedas') }} {{ __('labels.email') }}</title>
        <style>
            /* -------------------------------------
            INLINED WITH htmlemail.io/inline
            ------------------------------------- */
            /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
            table[class=body] h1 {
            font-size: 28px !important;
            margin-bottom: 10px !important;
            }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
            font-size: 16px !important;
            }
            table[class=body] .wrapper,
            table[class=body] .article {
            padding: 10px !important;
            }
            table[class=body] .content {
            padding: 0 !important;
            }
            table[class=body] .container {
            padding: 30px 0px 0px 0px; !important;
            width: 100% !important;
            }
            table[class=body] .main {
            border-left-width: 0 !important;
            border-radius: 0 !important;
            border-right-width: 0 !important;
            }
            table[class=body] .btn table {
            width: 100% !important;
            }
            table[class=body] .btn a {
            width: 100% !important;
            }
            table[class=body] .img-responsive {
            height: auto !important;
            max-width: 100% !important;
            width: auto !important;
            }
            }
            /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
            .ExternalClass {
            width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
            line-height: 100%;
            }
            .apple-link a {
            color: inherit !important;
            font-family: inherit !important;
            font-size: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            text-decoration: none !important;
            }
            .btn-primary table td:hover {
            background-color: #34495e !important;
            }
            .btn-primary a:hover {
            background-color: #34495e !important;
            border-color: #34495e !important;
            }
            }
        </style>
    </head>
    <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
            <tr>
                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top;display: block; margin: 0 auto; max-width: 580px; padding: 30px 10px 10px 10px; width: 580px; text-align:center"><img alt="{{ config('app.name', 'Kedas') }}" src="{{ asset('images/header-logo.png') }}"></td>
                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                    <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;">
                        <!-- START CENTERED WHITE CONTAINER -->
                        <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">{{ config('app.name', 'Kedas') }}</span>
                        <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
                            <!-- START MAIN CONTENT AREA -->
                            @yield('content')
                            <!-- END MAIN CONTENT AREA -->
                        </table>
                        <!-- START FOOTER -->
                        <div class="footer" style="clear: both; margin-top: 10px; text-align: center; width: 100%;">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                <!-- <tr>
                                    <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                                      <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"><a href="http://www.wildcard.earth" target="_blank">With Algera You Gain <strong>Time and And Tranquility</strong></a></span>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td style="padding-top:10px;"><span style="margin-right:10px;"><a href="#"><img src="images/app-store-icon.png"></a></span><span><a href="#"><img src="images/google-play-icon.png"></a></span>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td style="padding-top:10px;">If you do not receive this email, you can cancel <strong>your subscription here</strong></td>
                                    </tr>
                                    <tr>
                                    <td style="padding-top:10px;"><span style="margin-right:10px;"><a href="#"><img src="images/twitter.png"></a></span><span style="margin-right:10px;"><a href="#"><img src="images/facebook.png"></a></span>
                                      <span style="margin-right:10px;"><a href="#"><img src="images/instagram.png"></a></span>
                                      <span style="margin-right:10px;"><a href="#"><img src="images/video.png"></a></span>
                                    </td>
                                    </tr> -->
                                <tr>
                                    <td style="padding-top:10px;">© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.</td>
                                </tr>
                            </table>
                        </div>
                        <!-- END FOOTER -->
                        <!-- END CENTERED WHITE CONTAINER -->
                    </div>
                </td>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>