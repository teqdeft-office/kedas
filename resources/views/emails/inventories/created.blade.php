@extends('emails.layout')
@section('content')
<tr>
	<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
			<tr>
				<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.hi') }} {{$inventory->user->name}},</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.new_inventory_named') }} <b>{{$inventory->name}}</b> {{ __('labels.has_been_created') }}.</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"></p>
					<!-- <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>Reference - </b> {{ showIfAvailable($inventory->reference) }}</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>Sale Price - </b> {{ moneyFormat($inventory->sale_price) }}</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>Tax - </b> {{ $inventory->tax ? showIfAvailable($inventory->tax->formatted_name, 'None (0%)') : 'None (0%)' }}</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>{{ __('Type of item') }} - </b> {{ showIfAvailable(titleCase($inventory->type_of_item)) }}</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>{{ __('Item Category') }} - </b> {{ $inventory->itemCategory ? showIfAvailable($inventory->itemCategory->name) : 'N/A' }}</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>{{ __('Account') }} - </b> {{ showIfAvailable(titleCase($inventory->account)) }}</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><b>{{ __('Description') }} - </b> {{ showIfAvailable($inventory->description) }}</p> -->
					<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
						<tbody>
							<tr>
								<td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
									<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
										<tbody>
											<tr>
												<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> <a href="{{route('inventories.show', $inventory)}}" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">{{ __('labels.view').' '.__('labels.products_and_services') }}</a> </td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"></p>
					<!-- Table section -->
					<!-- End of Table section -->
					<br>
					<!-- <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">If you did not create an account, no further action is required.</p> -->
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.regards') }}, <br />{{ config('app.name', 'Kedas') }}</p>
				</td>
			</tr>
		</table>
	</td>
</tr>
@endsection