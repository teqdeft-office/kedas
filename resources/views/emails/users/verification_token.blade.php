@extends('emails.layout')
@section('content')
<tr>
	<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
			<tr>
				<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.hi') }} {{$user->name}},</p>
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.welcome_to') }} {{ config('app.name', 'Kedas') }}</p>

					<p>{{ __('labels.your_activation_code') }} <b>{{$user->email_token}}</b>.<br/>{{ __('labels.verification_code_line') }}</p>

					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"></p>
					<!-- Table section -->
					<!-- End of Table section -->
					<!-- <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">If you did not create an account, no further action is required.</p> -->
					<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">{{ __('labels.regards') }}, <br />{{ config('app.name', 'Kedas') }}</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	@endsection