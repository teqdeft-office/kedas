{{ __('labels.hi') }} {{$creditNote->contact->name}},
{{ __('labels.credit_note_received_line') }} {{moneyFormat($creditNote->calculation['total'])}}

{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.