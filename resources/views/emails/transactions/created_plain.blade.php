{{ __('labels.hi') }} {{$transaction->user->name}}, 
{{ __('labels.a_new_payment') }} <b>{{ __('labels.receipt').' #'.$transaction->receipt_number }}</b> {{ __('labels.has_been_received') }}.

{{ __('labels.copy_below_link_view').' '.__('labels.payment') }}.
<a href="{{route('transactions.show', $transaction)}}" target="_blank" >{{route('transactions.show', $transaction)}}</a> 

{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.