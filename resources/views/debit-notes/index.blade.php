@extends('layouts.dashboard')

@section('title', trans('labels.debit_notes'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.debit_notes') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.debit_note') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('debit-notes.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.debit_note') }}</a>
        </div>
      
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="debit-notes-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.remaining') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.discount') }} {{ __('labels.amount') }}</th>
                                    <th>{{ __('labels.comment') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($debitNotes as $debitNote)
                                <tr>
                                    <td>{{ $debitNote->contact->name }}</td>
                                    <td>{{ dateFormat($debitNote->start_date) }}</td>
                                    <td>{{ moneyFormat($debitNote->remaining) }}</td>
                                    <td>{{ moneyFormat($debitNote->calculation['total']) }}</td>
                                    <td>{{ moneyFormat($debitNote->calculation['taxAmount']) }}</td>
                                    <td>{{ moneyFormat($debitNote->calculation['discountAmount']) }}</td>
                                    <td>{{ $debitNote->notes }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('debit-notes.show', $debitNote)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('debit-notes.edit', $debitNote)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-debit-notes-form-' . $debitNote->id }}" href="{{route('debit-notes.destroy', $debitNote)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['debit-notes.destroy', $debitNote], 'id' => "destroy-debit-notes-form-{$debitNote->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.debit_note') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('debit-notes.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.debit_note') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
