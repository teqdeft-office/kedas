@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.debit_notes'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('debit-notes.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.debit_note') }}: {{ $debitNote->internal_number }}</h2>
        <div class="default-form view-form">
            <div class="generate-invoice">
                <div class="view-form-custom debit-note-view">
                    <table class="table">
                        <tr>
                            <td><b>{{ __('labels.client') }}</b></td>
                            @if($debitNote->contact instanceof App\Supplier)
                            <td><a href="{{route('suppliers.show', $debitNote->contact)}}">{{ $debitNote->contact->name }}</a></td>
                            @else
                            <td><a href="{{route('clients.show', $debitNote->contact)}}">{{ $debitNote->contact->name }}</a></td>
                            @endif
                            <td><b>{{ __('labels.date') }}</b></td>
                            <td>{{ dateFormat($debitNote->start_date) }}</td>
                            @if($debitNote->currency)
                            <td><b>{{ __('labels.currency') }}</b></td>
                            <td>{{ $debitNote->currency->code }}</td>
                            @endif
                            <tr>
                                <td><b>{{ __('labels.notes') }}</b></td>
                                <td class="descrip-notes"><span>{{ $debitNote->notes }}</span></td>
                            </tr>
                        </tr>
                    </table>
                </div>
                @if ($debitNote->documents->isNotEmpty())
                    <div class="form-group col-sm-12 col-lg-12 documents-container">
    					<label for="name">{{ __('labels.documents') }}: </label>
                        <div class="wrap-document-elements">
        					<div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
        					    @foreach($debitNote->documents->where('ext' ,'!=' , 'pdf') as $document)
        							<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        							<a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl" data-size="1024x1024">
        							<img src="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="thumbnail" alt="Image description" />
        						</a>
        						</figure>
                                @endforeach
        					</div>
                            <div class="pdf-documents">
        						@foreach($debitNote->documents->where('ext', 'pdf') as $document)
        						<a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
        						@endforeach
        					</div>
                        </div>
    					
    					<!-- Root element of PhotoSwipe. Must have class pswp. -->
    					<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    						<!-- Background of PhotoSwipe. 
    							It's a separate element, as animating opacity is faster than rgba(). -->
    						<div class="pswp__bg"></div>
    						<!-- Slides wrapper with overflow:hidden. -->
    						<div class="pswp__scroll-wrap">
    							<!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
    							<!-- don't modify these 3 pswp__item elements, data is added later on. -->
    							<div class="pswp__container">
    								<div class="pswp__item"></div>
    								<div class="pswp__item"></div>
    								<div class="pswp__item"></div>
    							</div>
    							<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
    							<div class="pswp__ui pswp__ui--hidden">
    								<div class="pswp__top-bar">
    									<!--  Controls are self-explanatory. Order can be changed. -->
    									<div class="pswp__counter"></div>
    									<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
    									<button class="pswp__button pswp__button--share" title="Share"></button>
    									<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
    									<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
    									<!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
    									<!-- element will get class pswp__preloader--active when preloader is running -->
    									<div class="pswp__preloader">
    										<div class="pswp__preloader__icn">
    											<div class="pswp__preloader__cut">
    												<div class="pswp__preloader__donut"></div>
    											</div>
    										</div>
    									</div>
    								</div>
    								<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
    									<div class="pswp__share-tooltip"></div>
    								</div>
    								<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
    								</button>
    								<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
    								</button>
    								<div class="pswp__caption">
    									<div class="pswp__caption__center"></div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
				@endif
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($debitNote->conceptItems as $item)
                                <tr>
                                    <td>{{$item->concept ? $item->concept->title : $item->userAccountingHead->name}}</td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount,2,true)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['baseTotal'] - $item->calculation['discountAmount'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                                @foreach($debitNote->items as $item)
                                <tr>
                                    <td><a href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a></td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount,2,true)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($debitNote->calculation['baseTotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{moneyFormat($debitNote->calculation['discountAmount'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($debitNote->calculation['subtotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.tax') }}</th>
                                        <td id="invoice_tax">{{moneyFormat($debitNote->calculation['taxAmount'],null,2,true)}}</td>
                                    </tr>
                                    {{-- @foreach($debitNote->calculation['taxInfo'] as $taxInfo)
                                    <tr class="tax_row">
                                        <th>{{$taxInfo['formatted_name']}}</th>
                                        <td>{{moneyFormat($taxInfo['tax'],null,2,true)}}</td>
                                    </tr>
                                    @endforeach --}}
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{moneyFormat($debitNote->calculation['total'],null,2,true)}}</td>
                                    </tr>
                                    @if($debitNote->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label">{{ $debitNote->currency->code }}</span></th>
                                        <td>
                                            <span class="total-ex-currency-label-symbol">{{ $debitNote->currency->code }} {{ $debitNote->currency->symbol }}</span><span id="total-ex-currency-rate">{{ addZeros($debitNote->exchange_rate_total,6,true) }}</span>
                                        </td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1 
                                                <span class="exchange_currency_label">{{ $debitNote->currency->code }}</span> 
                                                = {{ Auth::user()->company->currency->code }} ({{ Auth::user()->company->currency->symbol }}) 
                                                <span class="exchange_currency_rate">{{ addZeros($debitNote->exchange_currency_rate,6,true) }}</span> 
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
	initPhotoSwipeFromDOM('.documents');
</script>
@endpush