@extends('layouts.auth')

@section('title', trans('labels.register'))

@section('content')
<div class="reset-form-bg outer-form-section">
    <div class="inner-sec">
        <div class="form-logo"><a href="{{ route('register') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
            <h3>{{ __('labels.create_account') }}</h3>
            <form method="POST" action="{{ route('register') }}" class="custom-form-sign-up" id="sign-up-form">
                @csrf
                <ul>
                    <li>
                        <input class="form-control" type="text" name="name" placeholder="{{ __('labels.your_name') }}" required value="{{ old('name') }}" autocomplete="off" autofocus>
                        @error('name')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email_address') }}" required value="{{ old('email') }}" autocomplete="email">
                        @error('email')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-control" type="password" name="password" placeholder="{{ __('labels.password') }}" required autocomplete="off" id="password" />
                        @error('password')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-control" type="password" id="password-confirm" name="password_confirmation" placeholder="{{ __('labels.confirm_password') }}" required autocomplete="off">
                    </li>
                    <li>
                        <input type="submit" name="" value="{{ __('labels.register') }}" class="btn-custom">
                    </li>
                    <li>
                        <p>{{ __('labels.already_account') }}<a class="btn btn-link" href="{{ route('login') }}">{{ __('labels.sign_in_now') }}</a></p>
                    </li>
                    @include('includes.locale-selector')
                </ul>
            </form>
        </div>
    </div>
</div>
@endsection
