@extends('layouts.auth')

@section('title', trans('labels.reset_password'))

@section('content')
<div class="reset-form-bg outer-form-section">
    <div class="inner-sec">
        <div class="form-logo"><a href="{{ route('register') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
            <h3>{{ __('labels.reset_password') }}</h3>
            <form method="POST" action="{{ route('password.update') }}" class="custom-form-reset-password" id="reset-password-form">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                <ul>
                    <li>
                        <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email_address') }}" required autocomplete="email" value="{{ $email ?? old('email') }}" autofocus>
                        @error('email')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-control" type="password" name="password" placeholder="{{ __('labels.password') }}" required autocomplete="off" id="password" />
                        @error('password')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-control" type="password" id="password-confirm" name="password_confirmation" placeholder="{{ __('labels.confirm_password') }}" required autocomplete="off">
                    </li>
                    <li>
                        <input type="submit" name="" value="{{ __('labels.reset_password') }}" class="btn-custom">
                    </li>
                    <li>
                        <p>{{ __('labels.already_account') }}<a class="btn btn-link" href="{{ route('login') }}">{{ __('labels.sign_in_now') }}</a></p>
                    </li>
                    @include('includes.locale-selector')
                </ul>
            </form>
        </div>
    </div>
</div>
@endsection
