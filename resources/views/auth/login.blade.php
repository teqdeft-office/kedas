@extends('layouts.auth')

@section('title', trans('labels.login'))

@section('content')
<div class="reset-form-bg outer-form-section">
    <div class="inner-sec">
        <div class="form-logo"><a href="{{ route('register') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
            <h3>{{ __('labels.account_login') }}</h3>
            <form method="POST" action="{{ route('login') }}" class="custom-form-sign-in" id="sign-in-form">
                @csrf
                <ul>
                    <li>
                        <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email_address') }}" required autocomplete="email" value="{{ old('email', null) }}" autofocus>
                        @error('email')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-control" type="password" name="password" placeholder="{{ __('labels.password') }}" required autocomplete="off" id="password" />
                        @error('password')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('labels.remember_me') }}
                        </label>
                    </li>
                    <li>
                        <input type="hidden" name="lat" id="lat" value="{{ null }}">
                        <input type="hidden" name="lon" id="lon" value="{{ null }}">
                        <input type="submit" name="" value="{{ __('labels.login') }}" class="btn-custom">
                    </li>
                    <li>
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('labels.forgot_your_password') }}
                        </a>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ url('auth/google') }}">
                                    <img src="{{ asset('images/GButton.png') }}" alt="Login with google">
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('auth/facebook')}}">
                                    <img src="{{ asset('images/fbLogin.png') }}" alt="Login with facebook">
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>{{ __('labels.dont_have_account') }}<a class="btn btn-link" href="{{ route('register') }}">{{ __('labels.sign_up_now') }}</a></p>
                    </li>
                    @include('includes.locale-selector')
                </ul>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('js/location.js') }}"></script>
@endpush
