@extends('layouts.dashboard')

@section('title', trans('labels.accounting_reports'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.accounting_reports') }}</h2>
        <div class="tabs-custom reporting-tabs">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom products-table">
                        <div class="main-head-navigator">
                            <ul class="nav nav-tabs reporting-category" id="reporting-category" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="balance-sheet-tab" data-category="balance-sheet" data-toggle="tab" href="#balance-sheet" role="tab" aria-controls="balance-sheet" aria-selected="true">{{ __('labels.balance_sheet') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="income-statement-tab" data-category="income-statement" data-toggle="tab" href="#income-statement" role="tab" aria-controls="income-statement" aria-selected="true">{{ __('labels.income_statement') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="general-ledger-tab" data-category="general-ledger" data-toggle="tab" href="#general-ledger" role="tab" aria-controls="general-ledger" aria-selected="true">{{ __('labels.general_ledger_report') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="journal-report-tab" data-category="journal-report" data-toggle="tab" href="#journal-report" role="tab" aria-controls="journal-report" aria-selected="true">{{ __('labels.accounting_journal_report') }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="loader-container">
                            <span id="loader" style="display: none;"></span>
                        </div>
                        <div class="tab-content reporting" id="reporting">
                            <div class="tab-pane fade show active" id="balance-sheet" role="tabpanel" aria-labelledby="balance-sheet-tab">
                            </div>
                            <div class="tab-pane fade show" id="income-statement" role="tabpanel" aria-labelledby="income-statement-tab">
                            </div>
                            <div class="tab-pane fade show" id="general-ledger" role="tabpanel" aria-labelledby="general-ledger-tab">
                            </div>
                            <div class="tab-pane fade show" id="journal-report" role="tabpanel" aria-labelledby="journal-report-tab">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('js/accounting.js') }}"></script>
@endpush