@extends($isRawRequest ? 'layouts.raw' : 'layouts.dashboard' )

@section('title', trans('labels.accounting_journals'))

@section('content')

@if(!$isRawRequest)
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.accounting_journals') }}</h2>
@endif
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom products-table">
                        <table class="table table-striped" id="accounting-journals-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.document') }}</th>
                                    <th>{{ __('labels.account') }}</th>
                                    <th>{{ __('labels.debit') }}</th>
                                    <th>{{ __('labels.credit') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($journals as $key => $journal)
                                <tr>
                                    <td>{{ dateFormat($journal->adjustment_date) }}</td>
                                    <td><a href="{{$journal->resource_url}}">{{ $journal->resource_title }}</a></td>
                                    <td class="descrip-notes">
                                        @foreach($journal->accountingEntries as $accountingEntry)
                                            <div>{{ $accountingEntry->userAccountingHead->custom_name }}</div>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($journal->accountingEntries as $accountingEntry)
                                            <div>{{ $accountingEntry->nature == 'debit' ? moneyFormat($accountingEntry->amount,null,2,true) : '-' }}</div>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($journal->accountingEntries as $accountingEntry)
                                            <div>{{ $accountingEntry->nature == 'credit' ? moneyFormat($accountingEntry->amount,null,2,true) : '-' }}</div>
                                        @endforeach
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="5" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.no_records_found') }}</p></div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@if(!$isRawRequest)    
    </div>
</div>
<div class="clearfix"></div>
@endif

@endsection
