@inject('controller', 'App\Http\Controllers\Controller')

@extends('layouts.dashboard')

@section('title', trans('labels.chart_of_accounts'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container charts-block">
        <h2>{{ __('labels.chart_of_accounts') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.chart_of_accounts') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <!-- Button trigger modal -->
            <button type="button" class="btn-custom" data-toggle="modal" data-target="#addSecondaryChartOfAccountModal">{{ __('labels.new').' '.__('labels.account') }}</button>
        </div>
        <div class="tabs-custom chartsOfAccountsContent-tabs">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom products-table">
                        <div class="main-head-navigator">
                            <ul class="nav nav-tabs chartsOfAccounts" id="chartsOfAccounts" role="tablist">
                                @if($parent_accounting_heads->isEmpty())
                                    <p>{{ __('labels.dont_have_chart_accounts') }}</p>
                                @else
                                    @foreach ($parent_accounting_heads as $key => $parent_accounting_head)
                                        <li class="nav-item">
                                            <a class="nav-link {{$key == '0' ? 'active' : ''}}" id="ca-{{$parent_accounting_head->slug}}-tab" data-toggle="tab" href="#ca-{{$parent_accounting_head->slug}}" role="tab" aria-controls="ca-{{$parent_accounting_head->slug}}" aria-selected="true">{{$parent_accounting_head->custom_name}}</a>
                                            <a href="#" data-id="{{ $parent_accounting_head->id }}" data-name="{{ $parent_accounting_head->name }}"  data-description="{{ $parent_accounting_head->description }}" data-code="{{ $parent_accounting_head->custom_code }}" data-opening_balance="{{ $parent_accounting_head->opening_balance }}" class="edit-accounting-head" data-toggle="modal" data-target="#editChartOfAccountModal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a href="#" data-id="{{ $parent_accounting_head->id }}" data-name="{{ $parent_accounting_head->name }}"  data-description="{{ $parent_accounting_head->description }}" data-code="{{ $parent_accounting_head->custom_code }}" data-opening_balance="{{ $parent_accounting_head->opening_balance }}" class="view-accounting-head" data-toggle="modal" data-target="#viewChartOfAccountModal"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                            @if(!$parent_accounting_head->resource_associated)
                                            {{-- <a class="delete_resource" data-resource="destroy-account-head-{{ $parent_accounting_head->id }}" href="{{ route('charts.destroy', $parent_accounting_head) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            <form action="{{ route('charts.destroy', $parent_accounting_head) }}" method="POST" accept-charset="UTF-8" id="destroy-account-head-{{ $parent_accounting_head->id }}" style="display: none">
                                                @csrf
                                                @method('delete')
                                            </form> --}}
                                            @endif
                                        </li>
                                    @endforeach
                                    <li class="nav-item">
                                        <a class="nav-link" id="ca-{{$cost_account->slug}}-tab" data-toggle="tab" href="#ca-{{$cost_account->slug}}" role="tab" aria-controls="ca-{{$cost_account->slug}}" aria-selected="true">{{$cost_account->custom_name}}</a>
                                        <a href="#" data-id="{{ $cost_account->id }}" data-name="{{ $cost_account->name }}"  data-description="{{ $cost_account->description }}" data-code="{{ $cost_account->custom_code }}" data-opening_balance="{{ $cost_account->opening_balance }}" class="edit-accounting-head" data-toggle="modal" data-target="#editChartOfAccountModal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="#" data-id="{{ $cost_account->id }}" data-name="{{ $cost_account->name }}"  data-description="{{ $cost_account->description }}" data-code="{{ $cost_account->custom_code }}" data-opening_balance="{{ $cost_account->opening_balance }}" class="view-accounting-head" data-toggle="modal" data-target="#viewChartOfAccountModal"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </li>
                                @endif
                            </ul>
                            {{-- <button type="button" class="btn-custom add-primary-head-btn" data-toggle="modal" data-target="#addPrimaryChartOfAccountModal">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button> --}}
                        </div>
                        <div class="tab-content chartsOfAccountsContent" id="chartsOfAccountsContent">
                            @foreach($parent_accounting_heads as $key => $parent_accounting_head)
                            <div class="tab-pane fade show {{$key == '0' ? 'active' : ''}}" id="ca-{{$parent_accounting_head->slug}}" data-id="{{$parent_accounting_head->id}}" role="tabpanel" aria-labelledby="ca-{{$parent_accounting_head->slug}}-tab">
                                <table class="table">
                                    @forelse($accounting_heads->where('parent_id', $parent_accounting_head->id)->where('visibility', true)->where('code', '!=', $controller::COGSMAINCODE) as $sub_key => $sub_accounting_head)
                                    <tbody class="rowgroup">
                                        <tr class="row-header">
                                            <td class="cell" colspan="5">
                                                {{$sub_accounting_head->custom_name}}
                                                <!-- <i class="fa fa-question-circle" aria-hidden="true"></i> -->
                                                <a href="#" data-id="{{ $sub_accounting_head->id }}" data-name="{{ $sub_accounting_head->name }}"  data-description="{{ $sub_accounting_head->description }}" data-code="{{ $sub_accounting_head->custom_code }}" data-opening_balance="{{ $sub_accounting_head->opening_balance }}" class="edit-accounting-head" data-toggle="modal" data-target="#editChartOfAccountModal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <a href="#" data-id="{{ $sub_accounting_head->id }}" data-name="{{ $sub_accounting_head->name }}"  data-description="{{ $sub_accounting_head->description }}" data-code="{{ $sub_accounting_head->custom_code }}" data-opening_balance="{{ $sub_accounting_head->opening_balance }}" class="view-accounting-head" data-toggle="modal" data-target="#viewChartOfAccountModal"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                                @if(!$sub_accounting_head->resource_associated)
                                                {{-- 
                                                <a class="delete_resource" data-resource="destroy-account-head-{{ $sub_accounting_head->id }}" href="{{ route('charts.destroy', $sub_accounting_head) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                <form action="{{ route('charts.destroy', $sub_accounting_head) }}" method="POST" accept-charset="UTF-8" id="destroy-account-head-{{ $sub_accounting_head->id }}" style="display: none">
                                                    @csrf
                                                    @method('delete')
                                                </form>
                                                --}}
                                                @endif
                                            </td>
                                        </tr>
                                        @if($accounting_heads->where('parent_id', $sub_accounting_head->id)->isNotEmpty())
                                            @include('includes.chart-children', ['accounting_heads' => $accounting_heads->where('parent_id', $sub_accounting_head->id)->where('visibility', true), 'level' => 1])
                                        @else
                                        <tr class="tr-row">
                                            <!-- <td class="cell" colspan="1"></td> -->
                                            <td class="cell" colspan="4"><span class="wv-text wv-text--hint"><span class="chart-of-accounts__no-accounts-in-subgroup"><em class="wv-text--emphasized">{{ __('labels.dont_have_account_under_head', ['accounting_head_name' => $sub_accounting_head->name]) }}</em></span></span></td>
                                        </tr>
                                        @endif
                                        @empty
                                        <tr class="no-data-row" role="row">
                                            <td align="center">
                                                <div class="message">
                                                    <p>{{ __('labels.account_head_no_data') }}</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforelse
                                </table>
                            </div>
                            @endforeach
                            <div class="tab-pane fade show" id="ca-{{$cost_account->slug}}" data-id="{{$cost_account->id}}" role="tabpanel" aria-labelledby="ca-{{$cost_account->slug}}-tab">
                                <table class="table">
                                    <tbody class="rowgroup">
                                        <tr class="row-header">
                                            <td class="cell" colspan="5">
                                                {{$cost_account->custom_name}}
                                                <a href="#" data-id="{{ $cost_account->id }}" data-name="{{ $cost_account->name }}"  data-description="{{ $cost_account->description }}" data-code="{{ $cost_account->custom_code }}" data-opening_balance="{{ $cost_account->opening_balance }}" class="edit-accounting-head" data-toggle="modal" data-target="#editChartOfAccountModal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <a href="#" data-id="{{ $cost_account->id }}" data-name="{{ $cost_account->name }}"  data-description="{{ $cost_account->description }}" data-code="{{ $cost_account->custom_code }}" data-opening_balance="{{ $cost_account->opening_balance }}" class="view-accounting-head" data-toggle="modal" data-target="#viewChartOfAccountModal"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                            </td>
                                        </tr>
                                        @if($accounting_heads->where('parent_id', $cost_account->id)->isNotEmpty())
                                            @include('includes.chart-children', ['accounting_heads' => $accounting_heads->where('parent_id', $cost_account->id)->where('visibility', true), 'level' => 1])
                                        @else
                                        <tr class="tr-row">
                                            <!-- <td class="cell" colspan="1"></td> -->
                                            <td class="cell" colspan="4"><span class="wv-text wv-text--hint"><span class="chart-of-accounts__no-accounts-in-subgroup"><em class="wv-text--emphasized">{{ __('labels.dont_have_account_under_head', ['accounting_head_name' => $cost_account->name]) }}</em></span></span></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('js/accounting.js') }}"></script>
@endpush

@push('before-body-scripts')
<!-- Modal -->
<div class="modal fade chartOfAccountModal-modal" id="addPrimaryChartOfAccountModal" tabindex="-1" role="dialog" aria-labelledby="chartOfAccountModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="add-primary-accounting-head-form" onsubmit="return false;">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('labels.add_an_account') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div id="add-accounting-head-error"></div>
                        <!-- <label for="account-type">{{ __('labels.account_type') }} <span>*</span></label>
                        {!! Form::select('account-type', $accounting_type_options, '', ['class'=>"form-control single-search-selection", 'id'=>"account-type", 'required' => 'required']) !!} -->
                        <input type="hidden" name="node-type" value="primary">
                        <label>{{ __('labels.account_name') }} <span>*</span></label>
                        <input type="text" name="account-name" class="form-control">
                        <label>{{ __('labels.code') }} <span>*</span></label>
                        <input type="text" name="code" class="form-control">
                        <label>{{ __('labels.description') }}</label>
                        <textarea name="description" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('labels.cancel') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade chartOfAccountModal-modal" id="addSecondaryChartOfAccountModal" tabindex="-1" role="dialog" aria-labelledby="chartOfAccountModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="add-secondary-accounting-head-form" onsubmit="return false;">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('labels.add_an_account') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="add-accounting-head-error"></div>
                    <label for="account-type">{{ __('labels.account_type') }} <span>*</span></label>
                    {!! Form::select('account-type', $accounting_type_options, '', ['class'=>"form-control single-search-selection", 'id'=>"account-type", 'required' => 'required']) !!}
                    <input type="hidden" name="node-type" value="secondary">
                    <div class="account-head-control-container" style="display: none;">
                        <label>{{ __('labels.detail_type') }} <span>*</span></label>
                        <select id="account-control-options"></select>
                    </div>
                    <label>{{ __('labels.account_name') }} <span>*</span></label>
                    <input type="text" name="account-name" class="form-control" placeholder="{{ __('labels.account_name') }}">
                    <div class="account-head-control-container custom-subaccount" style="display: none;">
                        <div class="form-group">
                            <div class="checkbox">
                                {{ Form::checkbox('is_sub_account', old('is_sub_account', 1), old('is_sub_account'), array('id'=>'is_sub_account')) }}
                                <label for="is_sub_account">{{ __('labels.is_sub_account') }}</label>
                            </div>
                            <div id="multilevelselect" style="display: none;"></div>
                            @error('parent-account')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    {{-- <label>{{ __('labels.opening_balance') }}</label>
                    <input type="text" placeholder="{{ __('labels.opening_balance') }}" name="opening_balance" id="opening_balance" class="form-control" value="{{ old('opening_balance') ?: null }}" /> --}}

                    <label>{{ __('labels.code') }} <span>*</span></label>
                    <input type="text" name="code" placeholder="{{ __('labels.code') }}" class="form-control">
                    <label>{{ __('labels.description') }}</label>
                    <textarea name="description" placeholder="{{ __('labels.description') }}" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('labels.cancel') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade chartOfAccountModal-modal" id="editChartOfAccountModal" tabindex="-1" role="dialog" aria-labelledby="editChartOfAccountModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="edit-accounting-head-form" onsubmit="return false;">
                <div class="modal-header">
                    <h5 class="modal-title" id="editChartOfAccountModal">{{ __('labels.edit_account') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div id="edit-accounting-head-error"></div>
                        <input type="hidden" name="account-id" class="form-control">
                        <label>{{ __('labels.account_name') }} <span>*</span></label>
                        <input type="text" name="account-name" class="form-control" placeholder="{{ __('labels.account_name') }}">
                        {{-- <label>{{ __('labels.opening_balance') }}</label>
                        <input type="text" placeholder="{{ __('labels.opening_balance') }}" name="opening_balance" id="opening_balance" class="form-control" value="{{ old('opening_balance') ?: null }}" /> --}}

                        <label>{{ __('labels.code') }} <span>*</span></label>
                        <input type="text" name="code" placeholder="{{ __('labels.code') }}" class="form-control">
                        <label>{{ __('labels.description') }}</label>
                        <textarea name="description" placeholder="{{ __('labels.description') }}" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('labels.cancel') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade chartOfAccountModal-modal" id="viewChartOfAccountModal" tabindex="-1" role="dialog" aria-labelledby="viewChartOfAccountModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="view-accounting-head-form" onsubmit="return false;">
                <div class="modal-header">
                    <h5 class="modal-title" id="viewChartOfAccountModal">{{ __('labels.view_account') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body head-detail-container">
                    <label>{{ __('labels.account_name') }}</label>
                    <p id="account-name"></p>
                    <label>{{ __('labels.code') }}</label>
                    <p id="code"></p>
                    <label>{{ __('labels.description') }}</label>
                    <p id="description"></p>
                </div>
            </form>
        </div>
    </div>
</div>
@endpush