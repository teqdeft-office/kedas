@extends(Auth::check() ? (Auth::user()->isSuperAdmin() ? 'layouts.admin.dashboard' : 'layouts.dashboard') : 'errors.layout')

@section('title', trans('labels.dashboard'))

@section('content')
<div class="dashboard-right error-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="dashboard-max-w">

            <div class="error-page-wrap">
              
                    <div class="error-block">
                        <!-- <img src="{{ asset('images/waiting-to-resolve-error.png') }}"> -->
                        <img src="{{ asset('images/429-error.png') }}">
                        <!-- <h1>429</h1>
                        <h2>Page not found</h2> -->
                        <a class="button" href="{{ route('dashboard') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('labels.back_to_dashboard') }}</a>
                    </div>
          
            </div>
        </div>
    </div>
<div class="clearfix"></div>
@endsection
