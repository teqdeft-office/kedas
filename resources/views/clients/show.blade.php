@extends('layouts.dashboard')

@section('title', trans('labels.show').' '.trans('labels.client'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container user-invoice-view-details">
        <a class="arrow-back d-block" href="{{ route('clients.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ titleCase($client->name) }} Details</h2>
        <div class="default-form view-form-detail">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="number"><b>{{ __('labels.identification_type') }}:</b></label>
                        <span class="value-form">{{ $client->identificationInfo ? showIfAvailable($client->identificationInfo->name) : 'N/A' }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="taxid"><b>{{ __('labels.tax_id') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($client->tax_id) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="name"><b>{{ __('labels.name') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($client->name) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="email"><b>{{ __('labels.email') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($client->email) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                     <fieldset>
                        <label><b>{{ __('labels.address') }}: </b></label>
                        <span class="value-form">{{ addressFormat($client->address) }}</span>
                    </fieldset>
                </div>

                <div class="form-group col-sm-12 col-md-6">
                    <label for="department"><b>{{ __('labels.department') }}: </b></label>
                    <span class="value-form">{{ $client->sector ? showIfAvailable($client->sector->name) : 'N/A' }}</span>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="zip"><b>{{ __('labels.zip_code') }}: </b></label>
                    <span class="value-form">{{ $client->address ? showIfAvailable($client->address->zip) : 'N/A'  }}</span>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="mobile"><b>{{ __('labels.mobile') }}: </b></label>
                    <span class="value-form">{{ showIfAvailable($client->mobile) }}</span>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="phone"><b>{{ __('labels.phone') }}: </b></label>
                    <span class="value-form">{{ showIfAvailable($client->phone) }}</span>
                </div>
            </div>
            
        </div>
        <div class="tabs-custom default-form view-table-info">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#client-points">{{ __('labels.points') }}</a></li>
            </ul>
            <div class="tab-content">
                <div id="client-points" class="tab-pane generate-invoice fade in active show">
                    <div class="add-form">                           
                        <div class="default-table">
                            <div class="list-table-custom">
                                <table class="table table-striped">
                                    <thead>
                                        <th>{{ __('labels.date') }}</th>
                                        <th>{{ __('labels.invoice') }}</th>
                                        <th>{{ __('labels.points') }}</th>
                                        <th>{{ __('labels.point_percentage') }}</th>
                                    </thead>
                                    @forelse($client->points as $clientPoint)
                                    <tr>
                                        <td>{{dateFormat($clientPoint->created_at)}}</td>
                                        <td><a href="{{route('invoices.show', $clientPoint->invoice) }}">{{ __('labels.invoice') }} #{{ $clientPoint->invoice->internal_number }}</a></td>
                                        <td>{{ showIfAvailable($clientPoint->points) }}</td>
                                        <td>{{ showIfAvailable($clientPoint->point_percentage) }}</td>
                                    </tr>
                                    @empty
                                    <tr class="empty-row">
                                        <td colspan="5"><p>{{ __('messages.client_has_not_points') }}</p></td>
                                    </tr>
                                    @endforelse
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
