@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.client'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.client') }}</h2>
        <form method="POST" action="{{ route('clients.store') }}" id="add-edit-client-form">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            <label for="number">{{ __('labels.identification_type') }}</label>
                            {!! Form::select('identification_type', $identification_type_options, '', ['class'=>"form-control single-search-selection", 'id'=>"identification_type"]) !!}
                            @error('identification_type')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="taxid">{{ __('labels.tax_id') }}</label>
                            <input class="form-control" type="text" name="tax_id" placeholder="{{ __('labels.tax_id') }}" value="{{ old('tax_id') }}" autocomplete="off" />
                            @error('tax_id')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name') }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 client-supplier-checkbox-cnt">
                            <label class="container">{{ __('labels.client') }}
                                @if (old('_token') === null)
                                    <input type="checkbox" name="client" value="client" checked >
                                @else
                                    <input type="checkbox" name="client" value="client" {{ old("client") == "client" ? "checked" : "" }}  >
                                @endif
                            </label>
                            <label class="container">{{ __('labels.supplier') }}
                                <input type="checkbox" name="supplier" value="supplier" {{ old("supplier") == "supplier" ? "checked" : "" }} >
                            </label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">{{ __('labels.email') }} <span>*</span></label>
                            <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email_address') }}" required value="{{ old('email') }}" autocomplete="email" />
                            @error('email')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">{{ __('labels.company_name') }}</label>
                            <input class="form-control" type="text" name="company_name" placeholder="{{ __('labels.company_name') }}" value="{{ old('company_name') }}" />
                            @error('company_name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="department">{{ __('labels.department') }}</label>
                            {!! Form::select('sector', $sector_options, '', ['class'=>"form-control single-search-selection", 'id'=>"sector"]) !!}
                            @error('sector')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="membership">{{ __('labels.membership') }}</label>
                            {!! Form::select('membership_id', $membership_options, '', ['class'=>"form-control single-search-selection", 'id'=>"membership_id"]) !!}
                            @error('membership_id')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone">{{ __('labels.phone') }}</label>
                            <input class="form-control" type="tel" name="phone" placeholder="{{ __('labels.phone') }}" value="{{ old('phone') }}" autocomplete="off" />
                            @error('phone')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('labels.address') }}</label>
                            <input class="form-control" type="text" name="address" placeholder="{{ __('labels.address') }}" value="{{ old('address') }}" autocomplete="off" />
                            @error('address')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">{{ __('labels.city') }}</label>
                            <input class="form-control" type="text" name="city" placeholder="{{ __('labels.city') }}" value="{{ old('city') }}" autocomplete="off" />
                            @error('city')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="state">{{ __('labels.state') }}</label>
                            <input class="form-control" type="text" name="state" placeholder="{{ __('labels.state') }}" value="{{ old('state') }}" autocomplete="off" />
                            @error('state')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('labels.country') }}</label>
                            {!! Form::select('country', $country_options, '', ['class'=>"form-control single-search-selection", 'id'=>"country"]) !!}
                            @error('country')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="zip">{{ __('labels.zip_code') }}</label>
                            <input class="form-control" type="text" name="zip" placeholder="{{ __('labels.zip') }}" value="{{ old('zip') }}" autocomplete="off" />
                            @error('zip')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="mobile">{{ __('labels.mobile') }}</label>
                            <input class="form-control" type="tel" name="mobile" placeholder="{{ __('labels.mobile') }}" value="{{ old('mobile') }}" autocomplete="off" />
                            @error('mobile')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('clients.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>  
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
