@extends('layouts.dashboard')

@section('title', trans('labels.received_payments'))

@section('content')
<div class="dashboard-right cust-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.received_payments') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.received_payment') }}</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('transactions.create', ['type' => $type]) }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.payment_received') }}</a>
        </div>
       
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table table-responsive">
                        <table class="table table-striped" id="transactions-received-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.receipt_number') }}</th>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.details') }}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    <th>{{ __('labels.account') }}</th>
                                    <th>{{ __('labels.amount') }}</th>
                                    <th>{{ __('labels.annotation') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->receipt_number }}</td>
                                    <td>{{ $transaction->contact ? $transaction->contact->name : 'N/A' }} </td>
                                    <td>{{ __('labels.invoice_number') }}: {{ $transaction->invoices->implode('id', ', ') }}</td>
                                    <td>{{ dateFormat($transaction->start_date) }}</td>
                                    <td>{{ $transaction->bankAccount ? $transaction->bankAccount->name : 'N/A' }}</td>
                                    @if($transaction->currency)
                                    <td>{{ moneyFormat($transaction->currency->symbol) }}</td>
                                    @else
                                    <td>{{ moneyFormat($transaction->amount) }}</td>
                                    @endif
                                    <td>{{ showIfAvailable($transaction->annotation) }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('transactions.show', $transaction)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('transactions.edit', $transaction)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-transaction-form-' . $transaction->id }}" href="{{route('transactions.destroy', $transaction)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['transactions.destroy', $transaction], 'id' => "destroy-transaction-form-{$transaction->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_create_new').' '.__('labels.transaction') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('transactions.create', ['type' => $type]) }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.payment_received') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
