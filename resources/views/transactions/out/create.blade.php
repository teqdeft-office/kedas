@extends('layouts.dashboard')
@section('title', trans('labels.new').' '.trans('labels.payment'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.payment') }}</h2>
        <div class="default-form add-invoice-form">
            <form method="POST" id="add-payment-out-form" action="{{ route('transactions.store', ['type' => $type]) }}" enctype="multipart/form-data" >
                @csrf
                <div class="generate-invoice">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center company-data"></div>
                        <div class="col-sm-4">
                            <div class="consumption-box">
                                <div class="consumption-bottom">
                                    <span>{{ __('labels.no') }}.</span>
                                    <div class="consumption-number">{{ $next_number }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact" class="col-sm-12 creat-nw-cat">{{ __('labels.contact') }} <a target="_blank" href="{{ route('clients.create') }}">{{ __('labels.create_new_contact') }}</a></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', null), ['class'=>"form-control single-search-selection", 'id'=>"contact_id"], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="payment_received_start_date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="payment_received_start_date" class="form-control" name="start_date" required="required" value="{{old('start_date', getTodayDate())}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bank_account">{{ __('labels.bank_account') }} <span>*</span></label>
                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="account-multilevelselect">
                                <button type="button" class="btn btn-secondary dropdown-toggle" id="account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu" aria-labelledby="account-multilevelselect-button">
                                    <div class="hs-searchbox">
                                        <input type="text" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="hs-menu-inner">
                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                        @foreach($bank_accounts_options as $bank_accounts_option)
                                            <a class="dropdown-item" data-value="{{$bank_accounts_option->id}}" data-level="{{$bank_accounts_option->level}}" href="javascript::void(0)">
                                                <span class="account-name">{{$bank_accounts_option->custom_name}}</span>
                                                @if($bank_accounts_option->level == 1)
                                                <span class="detail-type">{{ $bank_accounts_option->parent->name }}</span>
                                                @endif
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                <input class="d-none" name="bank_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('bank_account', null) }}" />
                            </div>
                            @error('bank_account')
                            <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="waytopay">{{ __('labels.way_to_pay') }} <span>*</span></label>
                            {!! Form::select('payment_method', $payment_methods_options, old('payment_method', null), ['class'=>"form-control", 'id'=>"payment_method", 'required' => 'required']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="annotation">{{ __('labels.payment').' '.__('labels.annotation') }}</label>
                            <textarea class="form-control" id="annotation" name="annotation">{{old('annotation', null)}}</textarea>
                        </div>
                        
                        <div class="col-sm-6 form-group">
                            <label for="image">
                                        {{ __('labels.documents') }} ({{ __('labels.image') }})
                            </label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>

                        <div class="form-group col-sm-12 text-center register-income">
                            <h4>{{ __('labels.transaction_associate_with_invoice') }}</h4>
                            <p>{{ __('labels.remember_transaction_without_invoice') }}</p>
                            <div class="radio">
                                <input type="radio" name="transaction_with_invoice" id="transaction_with_invoice_on" value="1" {{ old('transaction_with_invoice', null) == '1' ? 'checked' : '' }} />
                                <label for="transaction_with_invoice_on">{{ __('labels.yes') }}</label>
                                <input type="radio" name="transaction_with_invoice" id="transaction_with_invoice_off" value="0" {{ old('transaction_with_invoice', null) == '0' ? 'checked' : '' }} />
                                <label for="transaction_with_invoice_off">{{ __('labels.no') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="default-table client-invoices-table with-concept-table" id="transaction_invoices_rows" style="display: none;">
                        <h5 id="debit_note_balance"></h5>
                        <div class="list-table-custom">
                            <table class="table table-striped" id="supplier_invoices">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.number') }}</th>
                                        <th>{{ __('labels.total') }}</th>
                                        <th>{{ __('labels.paid') }}</th>
                                        <th>{{ __('labels.pending') }}</th>
                                        <th>{{ __('labels.value_received') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="5">
                                        {{ __('labels.supplier_invoice_without_contact') }}
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                    <section id="transaction_concepts_rows" style="display: none;">
                        <div class="default-table add-invoice-table with-concept-table">
                            <div class="list-table-custom">
                                <table class="table table-striped mb-0" id="concept_items">
                                    <thead>
                                        <tr>
                                            <th class="th-concept">{{ __('labels.concept') }}</th>
                                            <th class="th-price">{{ __('labels.price') }}</th>
                                            <th class="th-disc">{{ __('labels.disc') }} %</th>
                                            <th class="th-tax">{{ __('labels.tax') }}</th>
                                            <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                            <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                            <th class="th-total">{{ __('labels.total') }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="td-concept">
                                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-0">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <div class="hs-menu-inner">
                                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                                                            @if(($concept_with_products ?? false) && $trackedInventories->isNotEmpty())
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.inventory') }}</strong>
                                                            @foreach($trackedInventories as $inventory_account)
                                                                <a class="dropdown-item" data-value="inventory-{{$inventory_account->id}}" data-level="1" href="javascript::void(0)">
                                                                    <span class="account-name">{{$inventory_account->custom_name}}</span>
                                                                    @if($inventory_account->level == 1)
                                                                    <span class="detail-type">{{ $inventory_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                            @endif
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                            @foreach($all_accounts_options as $all_account)
                                                                <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                    <span class="account-name">{{$all_account->custom_name}}</span>
                                                                    @if($all_account->level == 1)
                                                                    <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <input class="d-none" name="concepts[0][item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('concepts')[0]['item'] ?: null }}" />
                                                </div>
                                                @error('concepts[0][item]')
                                                <span class="error" role="alert">{{ $message }}</span>
                                                @enderror
                                            </td>
                                            <td class="td-price">
                                                <input type="number" min="0.01" step="any" placeholder="Price" name="concepts[0][price]" id="unitprice" class="form-control" value="{{ old('concepts')[0]['price'] ?: null }}" required />
                                            </td>
                                            <td class="td-disc" style="text-align: center;">
                                                <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="concepts[0][discount]" id="discount" class="form-control" value="{{ old('concepts')[0]['discount'] ?: 0 }}" />
                                            </td>
                                            <td class="td-tax">
                                                {!! Form::select('concepts[0][tax]', $tax_options, old('concepts[0][tax]', null), ['class'=>"form-control small-single-search-selection", 'id'=>"concepts[0][tax]"], $tax_attributes) !!}
                                            </td>
                                            <td class="td-tax_amount">
                                                <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="concepts[0][tax_amount]" id="concepts[0][tax_amount]" class="form-control" readonly value="{{ old('concepts')[0]['tax_amount'] ?: null }}" />
                                            </td>
                                            <td class="td-quantity">
                                                <input type="number" placeholder="1" name="concepts[0][quantity]" id="quantity" class="form-control" min="1" value="{{ old('concepts')[0]['quantity'] ?: 1 }}">
                                            </td>
                                            <td class="td-total">
                                                <span id="concepts[0][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                            </td>
                                            <td></td>
                                        </tr>
                                        @foreach (old('concepts', []) as $key => $concept)
                                            @if ($key == 0)
                                                @continue
                                            @endif
                                            @include('app.concept-row', ['row_number' => $key, 'concept_with_products' => false])
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <a class="btn-custom add-concept-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>

                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                            </div>
                            <div class="col-sm-8 total-table">
                                <div class="total-table">
                                    <table>
                                        <tr>
                                            <th>{{ __('labels.base_total') }}</th>
                                            <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('labels.discount') }}</th>
                                            <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('labels.sub_total') }}</th>
                                            <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('labels.total') }}</th>
                                            <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('transactions.index', ['type' => $type]) }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom transaction-submit documents-submit-btn" id="submit-btn">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/concepts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        addDropZone(null, 'image');
    </script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush