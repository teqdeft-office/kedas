@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.payment'))
@section('content')
<input type="hidden" name="resource_type" value="{{ $transaction->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block"
            href="{{ route('transactions.index', ['type' => $transaction->type]) }}">{{ __('labels.back') }}</a>
        @if($transaction->receipt_number)
        <h2>{{ __('labels.receipt').' #'.$transaction->receipt_number }}</h2>
        @elseif($transaction->voucher_number)
        <h2>{{ 'Voucher'.' #'.$transaction->voucher_number }}</h2>
        @endif
        @if($transaction->type=='in')
        <div class="action-btns-view">
            <a target="_blank" href="{{route('transactions.print', $transaction)}}" class="btn-custom"><i
                    class="fa fa-print"></i> {{ __('labels.print') }}</a>
        </div>
        @endif
        <div class="default-form view-form-detail">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="name"><b>{{ __('labels.contact') }}: </b></label>
                        <span
                            class="value-form">{{ $transaction->contact ? $transaction->contact->name : 'N/A' }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="tax"><b>{{ __('labels.details') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($transaction->detail_line) }}</span>
                    </div>
                </div>
                <div class="add-form row">
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="name"><b>{{ __('labels.account') }}: </b></label>
                        <span class="value-form">{{  $transaction->bankAccount ? $transaction->bankAccount->name : 'N/A' }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="tax"><b>{{ __('labels.amount') }}: </b></label>
                        <span class="value-form">{{ moneyFormat($transaction->amount,null,2,true) }}</span>
                    </div>
                </div>
                @if ($transaction->type == 'recurring')
                <div class="add-form row">
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="name"><b>{{ __('labels.start_date') }}: </b></label>
                        <span class="value-form">{{ dateFormat($transaction->start_date) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="tax"><b>{{ __('labels.end_date') }}: </b></label>
                        <span class="value-form">{{ dateFormat($transaction->end_date) }}</span>
                    </div>
                </div>
                @endif
                <div class="add-form row">
                    @if ($transaction->type != 'recurring')
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="name"><b>{{ __('labels.annotation') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($transaction->annotation) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="tax"><b>{{ __('labels.creation_date') }} </b></label>
                        <span class="value-form">{{ dateFormat($transaction->start_date) }}</span>
                    </div>
                    @else
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="name"><b>{{ __('labels.observations') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($transaction->observation) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="tax"><b>{{ __('labels.frequency').' '.__('labels.month') }}: </b></label>
                        <span class="value-form">{{ $transaction->frequency }}</span>
                    </div>
                    @endif
                    @if ($transaction->documents->isNotEmpty())
                        <div class="form-group col-sm-12 col-lg-12 documents-container">
                            <label for="name">{{ __('labels.documents') }}: </label>
                            <div class="wrap-document-elements">
                                <div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
                                    @foreach($transaction->documents->where('ext' ,'!=' , 'pdf') as $document)
                                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                        <a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl"
                                            data-size="1024x1024">
                                            <img src="{{ asset('public/storage/documents/'.$document->name) }}"
                                                itemprop="thumbnail" alt="Image description" />
                                        </a>
                                    </figure>
        							@endforeach
                                </div>
    							<div class="pdf-documents">
                                    @foreach($transaction->documents->where('ext', 'pdf') as $document)
                                    <a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img
                                            src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
                                    @endforeach
                                </div>

                                <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                                    <!-- Background of PhotoSwipe. 
        								It's a separate element, as animating opacity is faster than rgba(). -->
                                    <div class="pswp__bg"></div>
                                    <!-- Slides wrapper with overflow:hidden. -->
                                    <div class="pswp__scroll-wrap">
                                        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                        <div class="pswp__container">
                                            <div class="pswp__item"></div>
                                            <div class="pswp__item"></div>
                                            <div class="pswp__item"></div>
                                        </div>
                                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                        <div class="pswp__ui pswp__ui--hidden">
                                            <div class="pswp__top-bar">
                                                <!--  Controls are self-explanatory. Order can be changed. -->
                                                <div class="pswp__counter"></div>
                                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                                <button class="pswp__button pswp__button--share" title="Share"></button>
                                                <button class="pswp__button pswp__button--fs"
                                                    title="Toggle fullscreen"></button>
                                                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                                <div class="pswp__preloader">
                                                    <div class="pswp__preloader__icn">
                                                        <div class="pswp__preloader__cut">
                                                            <div class="pswp__preloader__donut"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                <div class="pswp__share-tooltip"></div>
                                            </div>
                                            <button class="pswp__button pswp__button--arrow--left"
                                                title="Previous (arrow left)">
                                            </button>
                                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                            </button>
                                            <div class="pswp__caption">
                                                <div class="pswp__caption__center"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        @if ($transaction->conceptItems->isNotEmpty())
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.concept') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transaction->conceptItems as $conceptItem)
                                <tr>
                                    <td>{{$conceptItem->concept ? $conceptItem->concept->title : $conceptItem->userAccountingHead->name}}</td>
                                    <td>{{ moneyFormat($conceptItem->price,null,2,true) }}</td>
                                    <td>{{addZeros($conceptItem->discount,2,true)}}%</td>
                                    <td>{{$conceptItem->tax ? addZeros($conceptItem->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($conceptItem->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{$conceptItem->quantity}}</td>
                                    <td>{{moneyFormat($conceptItem->calculation['baseTotal'] - $conceptItem->calculation['discountAmount'],null,2,true)}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.paid') }}</th>
                                    <th>{{ __('labels.pending') }}</th>
                                    <th>{{ __('labels.value_received') }}</th>
                                </tr>
                            </thead>
                            <?php //prePrint($transaction); ?>
                            <tbody>
                                @foreach ($transaction->invoices as $key => $invoice)
                                <tr>
                                    <input type="hidden" name="payments[{{$key}}][invoice_id]"
                                        value="{{ $invoice->id }}">
                                    <td>{{ $invoice->internal_number }}</td>
                                    <td>
                                        {{ moneyFormat($invoice->calculation['total'],null,2,true) }}
                                        @if($invoice->currency)
                                        <span class="exe_currency_label">({{ moneyFormat($invoice->exchange_rate_total, $invoice->currency->symbol, 6) }})</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ moneyFormat($invoice->calculation['paidAmount'],null,2,true) }}
                                        @if($invoice->currency)
                                        <span class="exe_currency_label">({{ moneyFormat($invoice->calculation['paidAmountInExe'], $invoice->currency->symbol, 6) }})</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ moneyFormat($invoice->calculation['pendingAmount'],null,2,true) }}
                                        @if($invoice->currency)
                                        <span class="exe_currency_label">({{ moneyFormat($invoice->calculation['pendingAmountInExe'], $invoice->currency->symbol, 6) }})</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ moneyFormat($invoice->pivot->amount,null,2,true) }}
                                        @if($invoice->currency)
                                        <span class="exe_currency_label">({{  moneyFormat($invoice->pivot->amount / $invoice->exchange_rate_total, $invoice->currency->symbol, 6) }})</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
            @if ($transaction->type != 'recurring')
            <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#transaction-accounting">{{ __('labels.accounting') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="transaction-accounting" class="transaction-accounting-tab-pane tab-pane fade generate-invoice in active show">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <div id="accounting-head" class="accounting-head">
                                        <span>{{ __('labels.accounting_sheet') }}</span>
                                        <span>{{ __('labels.date') }} {{ dateFormat($transaction->start_date) }}</span>
                                    </div>
                                    @if($transaction->accountingEntries->isNotEmpty())
                                    <a href="#" id="to-print" class="to-print"><i class="fa fa-print" aria-hidden="true"></i>{{ __('labels.print') }}</a>
                                    @endif
                                    <!-- <span class="accounting-notification">Visualiza el movimiento contable de este comprobante. Puedes personalizar las cuentas contables y sus códigos aquí.</span> -->
                                    <div class="table-wrap">
                                        <table id="accounting-table" class="table table-striped">
                                            <thead>
                                                <th>{{ __('labels.code') }}</th>
                                                <th>{{ __('labels.account') }}</th>
                                                <th>{{ __('labels.debit') }}</th>
                                                <th>{{ __('labels.credit') }}</th>
                                            </thead>
                                            @forelse($transaction->accountingEntries as $accountingEntry)
                                            <tr>
                                                <td>{{ $accountingEntry->userAccountingHead->custom_code ?: '-' }}</td>
                                                <td>{{ $accountingEntry->userAccountingHead->name }}</td>
                                                <td>{{ $accountingEntry->nature == 'debit' ? moneyFormat($accountingEntry->amount, null, 2, true) : '' }}</td>
                                                <td>{{ $accountingEntry->nature == 'credit' ? moneyFormat($accountingEntry->amount, null, 2, true) : '' }}</td>
                                            </tr>
                                            @empty
                                            <tr class="empty-row">
                                                <td colspan="4"><p>{{ __('messages.no_accounting_data') }}</p></td>
                                            </tr>
                                            @endforelse
                                            @if($transaction->accountingEntries->isNotEmpty() && count($transaction->debit_credit_amount))
                                                <tr>
                                                    <td colspan="2" style="text-align:right;"><strong>{{ __('labels.total') }}</strong></td>
                                                    <td><strong>{{ moneyFormat($transaction->debit_credit_amount['debit'], null, 2, true) }}</strong></td>
                                                    <td><strong>{{ moneyFormat($transaction->debit_credit_amount['credit'], null, 2, true) }}</strong></td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
initPhotoSwipeFromDOM('.documents');
</script>
@endpush