@extends('layouts.dashboard')
@section('title', trans('labels.new').' '.trans('labels.recurring_payment'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.recurring_payment') }}</h2>
        <div class="default-form add-invoice-form">
            <form method="POST" action="{{ route('transactions.store', ['type' => $type]) }}" id="add-edit-recurring-payment-form" enctype="multipart/form-data">
                @csrf
                <div class="generate-invoice">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center company-data"></div>
                        <div class="col-sm-4"></div>
                    </div>
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.contact') }} </label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', null), ['class'=>"form-control single-search-selection", 'id'=>"contact_id"], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="payment_recurring_start_date">{{ __('labels.start_date') }} <span>*</span></label>
                            <input id="payment_recurring_start_date" class="form-control" name="start_date" required="required" value="{{old('start_date', getTodayDate(1))}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bank_account">{{ __('labels.bank_account') }} <span>*</span></label>
                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="account-multilevelselect">
                                <button type="button" class="btn btn-secondary dropdown-toggle" id="account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu" aria-labelledby="account-multilevelselect-button">
                                    <div class="hs-searchbox">
                                        <input type="text" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="hs-menu-inner">
                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                        @foreach($bank_accounts_options as $bank_accounts_option)
                                            <a class="dropdown-item" data-value="{{$bank_accounts_option->id}}" data-level="{{$bank_accounts_option->level}}" href="javascript::void(0)">
                                                <span class="account-name">{{$bank_accounts_option->custom_name}}</span>
                                                @if($bank_accounts_option->level == 1)
                                                <span class="detail-type">{{ $bank_accounts_option->parent->name }}</span>
                                                @endif
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                <input class="d-none" name="bank_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('bank_account', null) }}" />
                            </div>
                            @error('bank_account')
                            <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="payment_recurring_end_date">{{ __('labels.end_date') }} <span>*</span></label>
                            <input id="payment_recurring_end_date" class="form-control" name="end_date" required="required" value="{{old('end_date', getTodayDate(1))}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="observation">{{ __('labels.observations') }}</label>
                            <textarea class="form-control" id="observation" name="observation">{{old('observation', null)}}</textarea>
                        </div>
                        <div class="form-group col-sm-6 mb-group-spacing">
                            <div class="row">
                                <div class="col-md-12">
                                   <label for="frequency">{{ __('labels.frequency') }} <span>*</span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <select name="frequency_type" class="form-control">
                                        <option value="month" {{old('frequency_type') == 'month' ? 'selected': ''}}>{{ __('labels.month') }}</option>
                                        <option value="day" {{old('frequency_type') == 'day' ? 'selected': ''}}>{{ __('labels.day') }}</option>
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <input type="number" name="frequency" id="frequency" class="form-control" min="1" value="{{old('frequency', 1)}}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="default-table with-concept-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped" id="concept_items">
                                <thead>
                                    <tr>
                                        <th class="th-concept">{{ __('labels.concept') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="td-concept">
                                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-0">
                                                <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                    <div class="hs-searchbox">
                                                        <input type="text" class="form-control" autocomplete="off">
                                                    </div>
                                                    <div class="hs-menu-inner">
                                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                                                        @if(($concept_with_products ?? false) && $trackedInventories->isNotEmpty())
                                                        <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.inventory') }}</strong>
                                                        @foreach($trackedInventories as $inventory_account)
                                                            <a class="dropdown-item" data-value="inventory-{{$inventory_account->id}}" data-level="1" href="javascript::void(0)">
                                                                <span class="account-name">{{$inventory_account->custom_name}}</span>
                                                                @if($inventory_account->level == 1)
                                                                <span class="detail-type">{{ $inventory_account->parent->name }}</span>
                                                                @endif
                                                            </a>
                                                        @endforeach
                                                        @endif
                                                        <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                        @foreach($all_accounts_options as $all_account)
                                                            <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                <span class="account-name">{{$all_account->custom_name}}</span>
                                                                @if($all_account->level == 1)
                                                                <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                                @endif
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <input class="d-none" name="concepts[0][item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('concepts')[0]['item'] ?: null }}" />
                                            </div>
                                            @error('concepts[0][item]')
                                            <span class="error" role="alert">{{ $message }}</span>
                                            @enderror
                                        </td>
                                        <td class="td-price">
                                            <input type="number" min="0.01" step="any" placeholder="{{ __('labels.price') }}" name="concepts[0][price]" id="unitprice" class="form-control" value="{{ old('concepts')[0]['price'] ?: null }}" required />
                                        </td>
                                        <td class="td-disc" style="text-align: center;">
                                            <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="concepts[0][discount]" id="discount" class="form-control" value="{{ old('concepts')[0]['discount'] ?: 0 }}" />
                                        </td>
                                        <td class="td-tax">
                                            {!! Form::select('concepts[0][tax]', $tax_options, old('concepts[0][tax]', null), ['class'=>"form-control small-single-search-selection", 'id'=>"concepts[0][tax]"], $tax_attributes) !!}
                                        </td>
                                        <td class="td-tax_amount">
                                            <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="concepts[0][tax_amount]" id="concepts[0][tax_amount]" class="form-control" readonly value="{{ old('concepts')[0]['tax_amount'] ?: null }}" />
                                        </td>
                                        <td class="td-quantity">
                                            <input type="number" placeholder="1" name="concepts[0][quantity]" id="quantity" class="form-control" min="1" value="{{ old('concepts')[0]['quantity'] ?: 1 }}">
                                        </td>
                                        <td class="td-total">
                                            <span id="concepts[0][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    @foreach (old('concepts', []) as $key => $concept)
                                        @if ($key == 0)
                                            @continue
                                        @endif
                                        @include('app.concept-row', ['row_number' => $key, 'concept_with_products' => false])
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-concept-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('transactions.index', ['type' => $type]) }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/concepts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush