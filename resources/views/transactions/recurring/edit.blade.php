@extends('layouts.dashboard')
@section('title', trans('labels.edit').' '.trans('labels.recurring_payment'))
@section('content')
<input type="hidden" name="resource_type" value="{{ $transaction->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        @if($transaction->receipt_number)
            <h2>{{ __('labels.receipt').' #'.$transaction->receipt_number }}</h2>
        @elseif($transaction->voucher_number)
            <h2>{{ __('labels.voucher').' #'.$transaction->voucher_number }}</h2>
        @else
            <h2>{{ __('labels.modify').' '.__('labels.transaction') }}</h2>
        @endif
        <div class="default-form add-transaction-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['transactions.update', $transaction], 'id' => 'add-edit-recurring-payment-form', 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.contact') }}</label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', $transaction->contact_custom_id), ['class'=>"form-control single-search-selection", 'id'=>"contact_id"], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="payment_recurring_start_date">{{ __('labels.start_date') }} <span>*</span></label>
                            <input id="payment_recurring_start_date" class="form-control" name="start_date" required="required" value="{{old('start_date', $transaction->start_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bank_account">{{ __('labels.bank_account') }} <span>*</span></label>
                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="account-multilevelselect">
                                <button type="button" class="btn btn-secondary dropdown-toggle" id="account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu" aria-labelledby="account-multilevelselect-button">
                                    <div class="hs-searchbox">
                                        <input type="text" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="hs-menu-inner">
                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                        @foreach($bank_accounts_options as $bank_accounts_option)
                                            <a class="dropdown-item" data-value="{{$bank_accounts_option->id}}" data-level="{{$bank_accounts_option->level}}" href="javascript::void(0)">
                                                <span class="account-name">{{$bank_accounts_option->custom_name}}</span>
                                                @if($bank_accounts_option->level == 1)
                                                <span class="detail-type">{{ $bank_accounts_option->parent->name }}</span>
                                                @endif
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                <input class="d-none" name="bank_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('bank_account', $transaction->bank_account) }}" />
                            </div>
                            @error('bank_account')
                            <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="payment_recurring_end_date">{{ __('labels.end_date') }} <span>*</span></label>
                            <input id="payment_recurring_end_date" class="form-control" name="end_date" required="required" value="{{old('end_date', $transaction->end_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="observation">{{ __('labels.observations') }}</label>
                            <textarea class="form-control" id="observation" name="observation">{{old('observation', $transaction->observation)}}</textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="row">
                                <div class="col-md-12">
                                   <label for="frequency">{{ __('labels.frequency') }} <span>*</span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <select name="frequency_type" class="form-control">
                                        <option {{ $transaction->frequency_type == "month" ? 'selected="selected"' : '' }} value="month">{{ __('labels.month') }}</option>
                                        <option {{ $transaction->frequency_type == "day" ? 'selected="selected"' : '' }} value="day">{{ __('labels.day') }}</option>
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <input type="number" name="frequency" id="frequency" class="form-control" min="1" max="31" value="{{old('frequency', $transaction->frequency)}}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="default-table with-concept-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped" id="concept_items">
                                <thead>
                                    <tr>
                                        <th class="th-concept">{{ __('labels.concept') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(old('concepts', [])))
                                        @foreach (old('concepts', []) as $key => $concept)
                                        @include('app.concept-row', ['row_number' => $key, 'concept_with_products' => false])
                                        @endforeach
                                    @else
                                        @foreach ($transaction->conceptItemsWithProducts as $key => $item)
                                        <tr>
                                            <input type="hidden" name="concepts[{{$key}}][id]" value="{{$item->custom_id ?: 0}}">
                                            <td class="td-concept">
                                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-{{$key}}">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <div class="hs-menu-inner">
                                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                                                            @if(($concept_with_products ?? false) && $trackedInventories->isNotEmpty())
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.inventory') }}</strong>
                                                            @foreach($trackedInventories as $inventory_account)
                                                                <a class="dropdown-item" data-value="inventory-{{$inventory_account->id}}" data-level="1" href="javascript::void(0)">
                                                                    <span class="account-name">{{$inventory_account->custom_name}}</span>
                                                                    @if($inventory_account->level == 1)
                                                                    <span class="detail-type">{{ $inventory_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                            @endif
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                            @foreach($all_accounts_options as $all_account)
                                                                <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                    <span class="account-name">{{$all_account->custom_name}}</span>
                                                                    @if($all_account->level == 1)
                                                                    <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <input class="d-none" name="concepts[{{$key}}][item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('concepts[$key][item]', $item->inventory ? $item->inventory->custom_id : $item->userAccountingHead->custom_id) }}" />
                                                </div>
                                                @error('concepts[{{$key}}][item]')
                                                <span class="error" role="alert">{{ $message }}</span>
                                                @enderror
                                            </td>
                                            <td class="td-price">
                                                <input type="number" min="0.01" step="any" placeholder="{{ __('labels.price') }}" name="concepts[{{$key}}][price]" id="concepts[{{$key}}][price]" class="form-control" value="{{ old('concepts')[$key]['price'] ?: $item->price }}" />
                                            </td>
                                            <td class="td-disc" style="text-align: center;">
                                                <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="concepts[{{$key}}][discount]" id="concepts[{{$key}}][discount]" class="form-control" value="{{ old('concepts')[$key]['discount'] ?: $item->discount }}" />
                                            </td>
                                            <td class="td-tax">
                                                {!! Form::select('concepts['.$key.'][tax]', $tax_options, old('concepts['.$key.'][tax]', $item->tax_id), ['class'=>"form-control small-single-search-selection", 'id'=>'concepts['.$key.'][tax]'], $tax_attributes) !!}
                                            </td>
                                            <td class="td-tax_amount">
                                                <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="concepts[{{$key}}][tax_amount]" id="concepts[{{$key}}][tax_amount]" class="form-control" readonly value="{{ old('concepts')[$key]['tax_amount'] ?: $item->calculation['taxAmount'] }}" />
                                            </td>
                                            <td class="td-quantity">
                                                <input type="number" placeholder="1" name="concepts[{{$key}}][quantity]" id="concepts[{{$key}}][quantity]" class="form-control" min="1" value="{{ old('concepts')[$key]['quantity'] ?: $item->quantity }}">
                                            </td>
                                            <td class="td-total">
                                                <span id="concepts[{{$key}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                            </td>
                                            <td>
                                                @if($key != 0)
                                                    <a class="delete-icon remove-concept-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <script type="text/javascript">
                                            id = "{{$key}}";
                                            generateHierarchySelect('concept-multilevelselect-'+id, 'input[name="concepts['+id+'][item]"]');
                                        </script>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-concept-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('transactions.index', ['type' => $type]) }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/concepts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush
