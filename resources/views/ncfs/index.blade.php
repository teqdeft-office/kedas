@extends('layouts.dashboard')

@section('title', trans('labels.ncfs'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.ncfs') }}</h2>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('ncfs.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i>
                {{__('labels.new_label') }} {{__('labels.ncf') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="ncfs-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($ncfs as $ncf)
                                <tr>
                                    <td>{{ $ncf->name }}</td>
                                    <td>{{ $ncf->ncf_number }}</td>
                                    <td>{{ showIfAvailable($ncf->description) }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle"
                                                data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a
                                                            href="{{route('ncfs.edit', $ncf)}}">{{ __('labels.edit') }}</a>
                                                    </li>
                                                    <li><a class="delete_resource"
                                                            data-resource="{{ 'destroy-ncf-form-' . $ncf->id }}"
                                                            href="{{route('ncfs.destroy', $ncf)}}">{{ __('labels.delete') }}</a>
                                                    </li>
                                                    {{ Form::open(array('method'=>'DELETE','route' => ['ncfs.destroy', $ncf], 'id' => "destroy-ncf-form-{$ncf->id}", 'style' => 'display: none')) }}
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="7" rowspan="2" align="center">
                                        <div class="message">
                                            <p>{{ __('labels.you_have_not_yet_created') }} {{ __('labels.ncf') }}!</p>
                                        </div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('ncfs.create') }}" class="btn-custom"><i
                                                    class="fa fa-plus" aria-hidden="true"></i> {{__('labels.new_label') }}
                                                {{__('labels.ncf') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection