@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.ncf'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{__('labels.new_label') }} {{__('labels.ncf') }}</h2>
        <form method="POST" action="{{ route('ncfs.store') }}" id="add-edit-ncf-form">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            <label for="name">{{__('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{__('labels.ncf') }} {{__('labels.name') }}" required value="{{ old('name', null) }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ncf_number">{{__('labels.ncf_number') }} <span>*</span></label>
                            <input id="ncf_number" class="form-control" name="ncf_number" placeholder="{{__('labels.ncf_number') }} {{ __('i.e A1234567890') }}" required="required" value="{{old('ncf_number', null)}}" autocomplete="off" maxlength="11" minlength="11" />
                            @error('ncf_number')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="description">{{__('labels.description') }}</label>
                            <textarea class="form-control" name="description" placeholder="{{__('labels.ncf') }} {{__('labels.description') }}" autocomplete="off">{{ old('description', null) }}</textarea>
                            @error('description')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                <div class="form-group popup-btns">
                    <a href="{{ route('ncfs.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{__('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{__('labels.save') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
