@extends('layouts.dashboard')

@section('title', trans('labels.show').' '.trans('labels.supplier'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container user-invoice-view-details">
        <a class="arrow-back d-block" href="{{ route('suppliers.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ titleCase($supplier->name) }} Details</h2>
        <div class="default-form view-form-detail supplier-view-table">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="number"><b>{{ __('labels.identification_type') }}:</b></label>
                        <span class="value-form">{{ $supplier->identificationInfo ? showIfAvailable($supplier->identificationInfo->name) : 'N/A' }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="taxid"><b>{{ __('labels.tax_id') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($supplier->tax_id) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="name"><b>{{ __('labels.name') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($supplier->name) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label for="email"><b>{{ __('labels.email') }}: </b></label>
                        <span class="value-form">{{ showIfAvailable($supplier->email) }}</span>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                     <fieldset>
                        <label><b>{{ __('labels.address') }}: </b></label>
                        <span class="value-form">{{ addressFormat($supplier->address) }}</span>
                    </fieldset>
                </div>

                <div class="form-group col-sm-12 col-lg-6">
                    <label for="department"><b>{{ __('labels.department') }}: </b></label>
                    <span class="value-form">{{ $supplier->sector ? showIfAvailable($supplier->sector->name) : 'N/A' }}</span>
                </div>
                <div class="form-group col-sm-12 col-lg-6">
                    <label for="zip"><b>{{ __('labels.zip_code') }}: </b></label>
                    <span class="value-form">{{ $supplier->address ? showIfAvailable($supplier->address->zip) : 'N/A'  }}</span>
                </div>
                <div class="form-group col-sm-12 col-lg-6">
                    <label for="mobile"><b>{{ __('labels.mobile') }}: </b></label>
                    <span class="value-form">{{ showIfAvailable($supplier->mobile) }}</span>
                </div>
                <div class="form-group col-sm-12 col-lg-6">
                    <label for="phone"><b>{{ __('labels.phone') }}: </b></label>
                    <span class="value-form">{{ showIfAvailable($supplier->phone) }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
