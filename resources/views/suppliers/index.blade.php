@extends('layouts.dashboard')

@section('title', trans('labels.suppliers'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.suppliers') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.supplier') }}</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('import_resource', ['resource' => 'suppliers']) }}" class="btn-custom outline-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ __('labels.import_from_excel') }}</a>
            <a href="{{ route('export_resource', ['resource' => 'suppliers']) }}" class="btn-custom outline-btn"><i class="fa fa-download" aria-hidden="true"></i> {{ __('labels.export_to_excel') }}</a>
            <a href="{{ route('suppliers.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_supplier') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="suppliers-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.email') }}</th>
                                    <th>{{ __('labels.tax_id') }}</th>
                                    <th>{{ __('labels.phone') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($suppliers as $supplier)
                                <tr>
                                    <td>{{ $supplier->name }}</td>
                                    <td>{{ $supplier->email }}</td>
                                    <td>{{ showIfAvailable($supplier->tax_id) }}</td>
                                    <td>{{ showIfAvailable($supplier->phone) }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('suppliers.show', $supplier)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('suppliers.edit', $supplier)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-supplier-form-' . $supplier->id }}" href="{{route('suppliers.destroy', $supplier)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['suppliers.destroy', $supplier], 'id' => "destroy-supplier-form-{$supplier->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="5" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_create_new').' '.__('labels.supplier') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('suppliers.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_supplier') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
