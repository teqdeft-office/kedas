@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.credit_note'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.modify').' '.__('labels.credit_note') }}</h2>
        <div class="default-form add-invoice-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['credit-notes.update', $creditNote], 'id' => 'edit-credit-notes-form', 'data-credit-note-id' => $creditNote->id, 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            <label for="contact">{{ __('labels.contact') }} <span>*</span></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', $creditNote->contact_custom_id), ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' => 'required'], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="credit_note_date" class="form-control" name="credit_note_date" required="required" value="{{old('credit_note_date', $creditNote->start_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="notes">{{ __('labels.notes') }}</label>
                            <textarea class="form-control" name="notes" id="notes">{{old('notes', $creditNote->notes)}}</textarea>
                        </div>
                        @if($creditNote->ncf)
                        <div class="form-group col-md-6  custom-select-controller">
                            <label for="ncf_id">{{ __('labels.ncf_number') }}</label>
                            <input type="text" name="ncf_number" id="ncf_number" class="form-control" disabled="disabled" value="{{$creditNote->ncf_number}}" />
                        </div>
                        @endif
                        @if($creditNote->currency)
                        <div class="currency_exchange_cnt col-md-6">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    <input type="text" id="exchange-currency" name="exchange_currency" class="form-control"
                                        value="{{old('exchange_currency', $creditNote->currency->name.' - '.$creditNote->currency->code.' ('.$creditNote->currency->symbol.')' )}}"
                                        readonly />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{old('exchange_rate', $creditNote->exchange_currency_rate)}}" readonly />
                                    <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $creditNote->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($creditNote->exchange_currency_rate,6,true) }}</span>
                                    </label>
                                </div>

                            </div>
                        </div>
                        @endif
                        <div class="col-md-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.pdf') }}, {{ __('labels.image') }})</label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                    </div>
                    <div class="default-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped" id="invoice_items">
                                <thead>
                                    <tr>
                                        <th class="th-item">{{ __('labels.item') }}</th>
                                        <th class="th-reference">{{ __('labels.reference') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <!-- <th class="th-description">{{ __('labels.description') }}</th> -->
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(old('products', [])))
                                        @foreach (old('products', []) as $key => $product)
                                            @include('app.item-row', ['row_number' => $key])
                                        @endforeach
                                    @else
                                        @foreach ($creditNote->items as $key => $item)
                                        <tr>
                                            <input type="hidden" name="products[{{$key}}][id]" value="{{$item->id ?: 0}}">
                                            <td class="td-item">
                                                {!! Form::select('products['.$key.'][item]', $inventory_options, old('products[{{$key}}][item]', $item->inventory_id), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$key.'][item]', 'required' => 'required'], $inventory_attributes) !!}
                                            </td>
                                            <td class="td-reference">
                                                <input type="text" placeholder="{{ __('labels.reference') }}" name="products[{{$key}}][reference]" id="products[{{$key}}][reference]" class="form-control" value="{{ old('products')[$key]['reference'] ?: $item->reference }}" />
                                            </td>
                                            <td class="td-price">
                                                <input type="text" placeholder="{{ __('labels.unit_price') }}" name="products[{{$key}}][price]" id="products[{{$key}}][price]" class="form-control" readonly value="{{ old('products')[$key]['price'] ?: $item->price }}" />
                                            </td>
                                            <td class="td-disc" style="text-align: center;">
                                                <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="products[{{$key}}][discount]" id="products[{{$key}}][discount]" class="form-control" value="{{ old('products')[$key]['discount'] ?: $item->discount }}" />
                                            </td>
                                            <td class="td-tax">
                                                {!! Form::select('products['.$key.'][tax]', $tax_options, old('products['.$key.'][tax]', $item->tax_id), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$key.'][tax]'], $tax_attributes) !!}
                                            </td>
                                            <td class="td-tax_amount">
                                                <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="products[{{$key}}][tax_amount]" id="products[{{$key}}][tax_amount]" class="form-control" readonly value="{{ old('products')[$key]['tax_amount'] ?: $item->calculation['taxAmount'] }}" />
                                            </td>
                                            <!-- <td class="td-description">
                                                <textarea  placeholder="{{ __('labels.description') }}" name="products[{{$key}}][description]" id="products[{{$key}}][description]" class="form-control" readonly>{{ old('products')[$key]['description'] ?: $item->inventory->description }}</textarea>
                                            </td> -->
                                            <td class="td-quantity">
                                                <input type="number" placeholder="1" name="products[{{$key}}][quantity]" id="products[{{$key}}][quantity]" class="form-control" min="1" value="{{ old('products')[$key]['quantity'] ?: $item->quantity }}">
                                            </td>
                                            <td class="td-total">
                                                <span id="products[{{$key}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                            </td>
                                            <td>
                                                @if($key != 0)
                                                    <a class="delete-icon remove-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    @if($creditNote->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span>{{ $creditNote->currency->code }}</span></th>
                                        <td><span>{{ $creditNote->currency->code }}
                                                {{ $creditNote->currency->symbol }}</span><span
                                                id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1
                                            <span class="exchange_currency_label">{{ $creditNote->currency->code }}</span>
                                            = {{ Auth::user()->company->currency->code }}
                                            ({{ Auth::user()->company->currency->symbol }})
                                            <span
                                                class="">{{ addZeros($creditNote->exchange_currency_rate,6,true) }}</span>
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="credit_note_total_hidden" id="credit_note_total_hidden" value="{{ $creditNote->calculation['total'] }}" />
                    </div>
                    <div class="default-table client-invoices-table">
                        <h2>{{ __('labels.credit_to_invoices') }}</h2>
                        <div class="list-table-custom">
                            <table class="table table-striped" id="client_invoices">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.number') }}</th>
                                        <th>{{ __('labels.total') }}</th>
                                        <th>{{ __('labels.paid') }}</th>
                                        <th>{{ __('labels.pending') }}</th>
                                        <th>{{ __('labels.amount') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('credit-notes.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom documents-submit-btn">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/invoices.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        var documents = {!! json_encode($creditNote->documents) !!};
        addDropZone(documents, 'both', 10);
    </script>
@endpush