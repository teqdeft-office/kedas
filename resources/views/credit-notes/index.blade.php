@extends('layouts.dashboard')

@section('title', trans('labels.credit_notes'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.credit_notes') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.credit_note') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('credit-notes.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.credit_note') }}</a>
        </div>
       
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="credit-notes-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.remaining') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.discount') }} {{ __('labels.amount') }}</th>
                                    <th>{{ __('labels.comment') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                    <th>{{ __('labels.notes') }}</th>
                                    <th>{{ __('labels.phone') }}</th>
                                    <th>{{ __('labels.baseTotal') }}</th>
                                    <th>{{ __('labels.tax_id') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($creditNotes as $creditNote)
                                <tr>
                                    <td>{{ $creditNote->contact->name }}</td>
                                    <td>{{ dateFormat($creditNote->start_date) }}</td>
                                    <td>{{ moneyFormat($creditNote->remaining) }}</td>
                                    <td>{{ moneyFormat($creditNote->calculation['total']) }}</td>
                                    <td>{{ moneyFormat($creditNote->calculation['taxAmount']) }}</td>
                                    <td>{{ moneyFormat($creditNote->calculation['discountAmount']) }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('credit-notes.show', $creditNote)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('credit-notes.edit', $creditNote)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-credit-notes-form-' . $creditNote->id }}" href="{{route('credit-notes.destroy', $creditNote)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['credit-notes.destroy', $creditNote], 'id' => "destroy-credit-notes-form-{$creditNote->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
</div>
                                    </td>
                                    <td>{{ $creditNote->notes }}</td>
                                    <td>{{ $creditNote->contact->phone }}</td>
                                    <td>{{ $creditNote->calculation['baseTotal'] }}</td>
                                    <td>{{ $creditNote->contact->tax_id }}</td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.credit_note') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('credit-notes.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.credit_note') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
