@extends('layouts.dashboard')
@section('title', trans('labels.new').' '.trans('labels.journal_entry'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.journal_entry') }}</h2>
        <div class="default-form add-invoice-form">
            <form method="POST" id="add-edit-account-adjustment-form" action="{{ route('accounting.adjustments.store') }}" enctype="multipart/form-data" >
                @csrf
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="account_adjustment_date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="account_adjustment_date" class="form-control" name="adjustment_date" required="required" value="{{old('adjustment_date', getTodayDate())}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="reference">{{ __('labels.reference') }}</label>
                            <input type="text" class="form-control" name="reference" id="reference" placeholder="{{ __('labels.reference') }}" value="{{old('reference', null)}}">
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="observations">{{ __('labels.observations') }}</label>
                            <textarea class="form-control" id="observations" name="observations">{{old('observations', null)}}</textarea>
                        </div>
                        
                        <div class="col-sm-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.image') }})</label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                    </div>
                    <section id="account_adjustment_rows">
                        <div class="default-table add-invoice-table with-concept-table">
                            <div class="list-table-custom">
                                <table class="table table-striped mb-0" id="account_adjustment_items">
                                    <thead>
                                        <tr>
                                            <th class="th-contact">{{ __('labels.contact') }}</th>
                                            <th class="th-description">{{ __('labels.description') }}</th>
                                            <th class="th-account">{{ __('labels.account') }}</th>
                                            <th class="th-debit">{{ __('labels.debit') }}</th>
                                            <th class="th-credit">{{ __('labels.credit') }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	{{-- <tr>
                                            <td>
                                            	{!! Form::select("items[0][contact_id]", $contact_options, old('items[0][contact_id]', null), ['class'=>"form-control single-search-selection", 'id'=>"items[0][contact_id]"], $contact_attributes) !!}
                                            </td>                                            
                                            <td>
                                            	<input type="text" placeholder="{{ __('labels.description') }}" name="items[0][description]" id="description" class="form-control"  value="{{ old('items')[0]['description'] ?: null }}" />
                                            </td>
                                            <td class="td-concept">
                                                <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-0">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <div class="hs-menu-inner">
                                                            <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                                                            <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                            @foreach($all_accounts_options as $all_account)
                                                                <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                    <span class="account-name">{{$all_account->custom_name}}</span>
                                                                    @if($all_account->level == 1)
                                                                    <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                                    @endif
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <input class="d-none" name="items[0][concept-item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('items')[0]['concept-item'] ?: null }}" />
                                                </div>
                                                @error('items[0][concept-item]')
                                                <span class="error" role="alert">{{ $message }}</span>
                                                @enderror
                                            </td>
                                            <td class="td-concept">
                                            	<input type="number" min="0.01" step="any" placeholder="{{ __('labels.debit_amount') }}" name="items[0][debit_amount]" class="form-control" value="{{ old('items')[0]['debit_amount'] ?: null }}" />
                                            </td>
                                            <td class="td-concept">
                                            	<input type="number" min="0.01" step="any" placeholder="{{ __('labels.credit_amount') }}" name="items[0][credit_amount]" class="form-control" value="{{ old('items')[0]['credit_amount'] ?: null }}" />
                                            </td>
                                        </tr> --}}
                                        @foreach (old('items', [0, 1]) as $key => $concept)
                                            @include('app.account-adjustment-row', ['row_number' => $key])
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    	<tr>
                                    		<td><a class="btn-custom add-account-adjustment-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a></td>
                                    		<td></td>
                                    		<td style="text-align: center;" class="total-table-foot">{{ __('labels.total') }}</td>
                                    		<td style="text-align: center;" class="total-table-foot" id="debit_total">{{ moneyFormat() }}</td>
                                    		<td style="text-align: center;" class="total-table-foot" id="credit_total">{{ moneyFormat() }}</td>
                                    		<td></td>
                                    	</tr>
                                    </tfoot>
                                </table>
                            </div>                           

                        </div>
                    </section>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('accounting.adjustments.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom transaction-submit documents-submit-btn" id="submit-btn">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
	<script type="text/javascript" src="{{ asset('js/account-adjustment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        addDropZone(null, 'both');
    </script>
@endpush