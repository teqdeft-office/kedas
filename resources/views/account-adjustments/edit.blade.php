@extends('layouts.dashboard')
@section('title', trans('labels.edit').' '.trans('labels.journal_entry'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit').' '.__('labels.journal_entry') }}</h2>
        <div class="default-form add-invoice-form">
        	{{ Form::open(array('method'=>'PUT', 'route' => ['accounting.adjustments.update', $accountAdjustment], 'id' => 'add-edit-account-adjustment-form', 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="account_adjustment_date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="account_adjustment_date" class="form-control" name="adjustment_date" required="required" value="{{old('adjustment_date', $accountAdjustment->adjustment_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="reference">{{ __('labels.reference') }}</label>
                            <input type="text" class="form-control" name="reference" id="reference" placeholder="{{ __('labels.reference') }}" value="{{old('reference', $accountAdjustment->reference)}}">
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="observations">{{ __('labels.observations') }}</label>
                            <textarea class="form-control" id="observations" name="observations">{{old('observations', $accountAdjustment->observations)}}</textarea>
                        </div>
                        
                        <div class="col-sm-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.image') }})</label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                    </div>
                    <section id="account_adjustment_rows">
                        <div class="default-table add-invoice-table with-concept-table">
                            <div class="list-table-custom">
                                <table class="table table-striped mb-0" id="account_adjustment_items">
                                    <thead>
                                        <tr>
                                            <th class="th-contact">{{ __('labels.contact') }}</th>
                                            <th class="th-description">{{ __('labels.description') }}</th>
                                            <th class="th-account">{{ __('labels.account') }}</th>
                                            <th class="th-debit">{{ __('labels.debit') }}</th>
                                            <th class="th-credit">{{ __('labels.credit') }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count(old('items', [])))
                                        @foreach (old('items', []) as $key => $concept)
                                            @include('app.account-adjustment-row', ['row_number' => $key])
                                        @endforeach
                                        @else
                                            @foreach ($accountAdjustment->accountingEntries as $key => $accountingEntry)
                                                <tr>
                                                    <input type="hidden" name="items[{{$key}}][id]" value="{{$accountingEntry->id ?: 0}}">
                                                    <td class="td-item">
                                                        {!! Form::select('items['.$key.'][contact_id]', $contact_options, old('items['.$key.'][contact_id]', $accountingEntry->contact ? $accountingEntry->contact->contact_custom_id : null), ['class'=>"form-control single-search-selection", 'id'=>'items['.$key.'][contact_id]'], $contact_attributes) !!}
                                                    </td>
                                                    <td>
                                                        <input type="text" placeholder="{{ __('labels.description') }}" name="items[{{$key}}][description]" id="items[{{$key}}][description]" class="form-control" value="{{ old('items')[$key]['description'] ?: $accountingEntry->description }}" />
                                                    </td>
                                                    <td class="td-concept">
                                                        <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-{{$key}}">
                                                            <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                            <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                                <div class="hs-searchbox">
                                                                    <input type="text" class="form-control" autocomplete="off">
                                                                </div>
                                                                <div class="hs-menu-inner">
                                                                    <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                                                    <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                                    @foreach($all_accounts_options as $all_account)
                                                                        <a class="dropdown-item" data-value="{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                            <span class="account-name">{{$all_account->custom_name}}</span>
                                                                            @if($all_account->level == 1)
                                                                            <span class="detail-type">{{ $all_account->parent->name }}</span>
                                                                            @endif
                                                                        </a>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @php
                                                            // print_r(old('items')[$key]['concept-item']);
                                                            @endphp
                                                            <input class="d-none" name="items[{{$key}}][concept-item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('items')[$key]['concept-item'] ?: $accountingEntry->user_accounting_head_id }}"  required="required" />
                                                        </div>
                                                        @error('items[{{$key}}][concept-item]')
                                                            <span class="error" role="alert">{{ $message }}</span>
                                                        @enderror
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <input type="number" min="0.01" step="any" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.debit_amount')]) }}" placeholder="{{ __('labels.debit_amount') }}" name="items[{{$key}}][debit_amount]" class="form-control"  value="{{ old('items')[$key]['debit_amount'] ?: ($accountingEntry->nature == 'debit' ? $accountingEntry->amount : null) }}" {{$accountingEntry->nature == "credit" ? 'readonly' : ''}} />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <input type="number" min="0.01" step="any" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.credit_amount')]) }}" placeholder="{{ __('labels.credit_amount') }}" name="items[{{$key}}][credit_amount]" class="form-control"  value="{{ old('items')[$key]['credit_amount'] ?: ($accountingEntry->nature == 'credit' ? $accountingEntry->amount : null) }}" {{$accountingEntry->nature == "debit" ? 'readonly' : ''}} />
                                                    </td>
                                                    <td><a class="delete-icon remove-account-adjustment-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                                </tr>
                                                <script type="text/javascript">
                                                    id = "{{$key}}";
                                                    generateHierarchySelect('concept-multilevelselect-'+id, 'input[name="items['+id+'][concept-item]"]');
                                                </script>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                    		<td><a class="btn-custom add-account-adjustment-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a></td>
                                    		<td></td>
                                    		<td style="text-align: center;" class="total-table-foot">{{ __('labels.total') }}</td>
                                    		<td style="text-align: center;" class="total-table-foot" id="debit_total">{{ moneyFormat() }}</td>
                                    		<td style="text-align: center;" class="total-table-foot" id="credit_total">{{ moneyFormat() }}</td>
                                    		<td></td>
                                    	</tr>
                                    </tfoot>
                                </table>
                            </div>                           

                        </div>
                    </section>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('accounting.adjustments.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom transaction-submit documents-submit-btn" id="submit-btn">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/account-adjustment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        var documents = {!! json_encode($accountAdjustment->documents) !!};
        addDropZone(documents, 'both', 10);
    </script>
@endpush