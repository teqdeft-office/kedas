<tr>
    <input type="hidden" name="products[{{$row_number}}][id]" value="{{ old('products')[$row_number]['id'] ?: 0 }}">
    <td class="td-item">
        {!! Form::select('products['.$row_number.'][item]', $inventory_options, old('products['.$row_number.'][item]', null), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$row_number.'][item]'], $inventory_attributes) !!}
    </td>
    <td class="td-reference">
        <input type="text" placeholder="{{ __('labels.reference') }}" name="products[{{$row_number}}][reference]" id="reference" class="form-control"  value="{{ old('products')[$row_number]['reference'] ?: null }}" />
    </td>
    <td class="td-price">
        <input type="text" placeholder="{{ __('labels.unit_price') }}" name="products[{{$row_number}}][price]" id="unitprice" class="form-control" readonly value="{{ old('products')[$row_number]['price'] ?: null }}"/>
    </td>
    <td class="td-disc" style="text-align: center;">
        <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" placeholder="%" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" name="products[{{$row_number}}][discount]" id="discount" class="form-control" value="{{ old('products')[$row_number]['discount'] ?: 0 }}" />
    </td>
    <td class="td-tax">
        {!! Form::select('products['.$row_number.'][tax]', $tax_options, old('products['.$row_number.'][tax]', null), ['class'=>"form-control small-single-search-selection", 'id'=>'products['.$row_number.'][tax]'], $tax_attributes) !!}
    </td>
    <td class="td-tax_amount">
        <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="products[{{$row_number}}][tax_amount]" id="products[{{$row_number}}][tax_amount]" class="form-control" readonly value="{{ old('products')[$row_number]['tax_amount'] ?: null }}" />
    </td>
    <td class="td-quantity">
        <input type="number" placeholder="1" name="products[{{$row_number}}][quantity]" id="products[{{$row_number}}][quantity]" class="form-control" min="1" value="{{ old('products')[$row_number]['quantity'] ?: 1 }}">
    </td>
    <td class="td-total">
        <span id="products[{{$row_number}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
    </td>
    <td>
        @if($row_number != 0)
            <a class="delete-icon remove-row" data-row_number="{{$row_number}}"><i class="fa fa-times" aria-hidden="true"></i></a>
        @endif
    </td>
</tr>