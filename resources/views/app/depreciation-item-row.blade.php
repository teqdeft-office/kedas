<tr>
	<input type="hidden" name="depreciation_items[{{$row_number}}][id]" value="{{ old('depreciation_items')[$row_number]['id'] ?: 0 }}">
    <td class="td-year">
        <input type="text" placeholder="{{ __('labels.year') }}" name="depreciation_items[{{$row_number}}][year]" id="depreciation_items[{{$row_number}}][year]" class="form-control" readonly value="{{ $row_number + 1 }}" />
    </td>
    <td class="td-disc" style="text-align: center;">
        <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.percentage')]) }}" placeholder="%" name="depreciation_items[{{$row_number}}][percentage]" id="depreciation_items[{{$row_number}}][percentage]" class="form-control" required="required" value="{{ old('depreciation_items')[$row_number]['percentage'] ?: null }}" />
    </td>
    <td>
    	@if($row_number != 0)
        	<a class="delete-icon remove-depr-row" data-row_number="{{$row_number}}"><i class="fa fa-times" aria-hidden="true"></i></a>
        @endif
    </td>
</tr>