@inject('controller', 'App\Http\Controllers\Controller')

@forelse ($invoices as $key => $invoice)
	@if($invoice->type == $controller::SUPPLIER)
	<tr>
	    <input type="hidden" name="payments[{{$key}}][invoice_id]" value="{{ $invoice->id }}">
	    <td>{{ $invoice->internal_number }}</td>
	    <td>
			{{ moneyFormat($invoice->calculation['total'],null,2,true) }}
			@if($invoice->currency)
			<span class="exe_currency_label">({{ moneyFormat($invoice->exchange_rate_total, $invoice->currency->symbol, 6) }})</span>
			@endif
		</td>
	    <td>
			{{ moneyFormat($invoice->calculation['paidAmount'],null,2,true) }}
			@if($invoice->currency)
			<span class="exe_currency_label">({{ moneyFormat($invoice->calculation['paidAmountInExe'], $invoice->currency->symbol, 6) }})</span>
			@endif
		</td>

	    {{-- If we are going to edit either the transaction or the debit note --}}
	    @if(!$transaction_id && $action == 'edit')
	    	<td>{{ moneyFormat($invoice->calculation['supplierPendingAmount'],null,2,true + ($invoice->debit_note_details[$invoice->id] ?? 0)) }}</td>
	    @else
	    	<td>
				{{ moneyFormat($invoice->calculation['supplierPendingAmount'],null,2,true) }}
				@if($invoice->currency)
				<span class="exe_currency_label">({{ moneyFormat($invoice->calculation['pendingAmountInExe'], $invoice->currency->symbol, 6) }})</span>
				@endif
			</td>
		@endif

	    <td>
	    	@if($action == 'create')
	    		{{-- check if the invoice have some pending amount --}}
				@if($invoice->calculation['supplierPendingAmount'] > 0)
					<label class="exe_currency">{{ Auth::user()->company->currency->symbol }}</label> <input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" min="0.01" max="{{ $invoice->calculation['supplierPendingAmount']  }}" value="{{ old('payments')[$key]['amount'] ?: null }}" step="any" />
			    @endif
			@else
		    	{{-- Going to edit the transaction for the invoice --}}
				@if($transaction_id)
				<label class="exe_currency">{{ Auth::user()->company->currency->symbol }}</label> <input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" max="{{ $invoice->calculation['supplierPendingAmount'] + ($invoice->transaction_details[$invoice->id] ?? 0) }}" value="{{ old('payments')[$key]['amount'] ?: ($invoice->transaction_details[$invoice->id] ?? 0) }}" step="any" />
		    	@else
		    		{{-- Going to edit the debit note for the invoice --}}
					@if($invoice->calculation['pendingAmountWithoutDebitNote'] > 0)
						<input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" max="{{ $invoice->calculation['pendingAmountWithoutDebitNote'] }}" value="{{ old('payments')[$key]['amount'] ?: ($invoice->debit_note_details[$invoice->id] ?? 0) }}" step="any" />
					@endif
		    	@endif
		    @endif
	    </td>
	</tr>
	@else
	<tr>
	    <input type="hidden" name="payments[{{$key}}][invoice_id]" value="{{ $invoice->id }}">
	    <td>{{ $invoice->internal_number }}</td>
	    <td>
			{{ moneyFormat($invoice->calculation['total'],null,2,true) }}
			@if($invoice->currency)
			<span class="exe_currency_label">({{ moneyFormat($invoice->exchange_rate_total, $invoice->currency->symbol, 6) }})</span>
			@endif
		</td>
	    <td>
			{{ moneyFormat($invoice->calculation['paidAmount'],null,2,true) }}
			@if($invoice->currency)
			<span class="exe_currency_label">({{ moneyFormat($invoice->calculation['paidAmountInExe'], $invoice->currency->symbol, 6) }})</span>
			@endif
		</td>

	    {{-- If we are going to edit either the transaction or the credit note --}}
	    @if(!$transaction_id && $action == 'edit')
	    	<td>{{ moneyFormat($invoice->calculation['pendingAmount'],null,2,true + ($invoice->credit_note_details[$invoice->id] ?? 0)) }}</td>
	    @else
	    	<td>
				{{ moneyFormat($invoice->calculation['pendingAmount'],null,2,true) }}
				@if($invoice->currency)
				<span class="exe_currency_label">({{ moneyFormat($invoice->calculation['pendingAmountInExe'], $invoice->currency->symbol, 6) }})</span>
				@endif
			</td>
		@endif

	    <td>
	    	@if($action == 'create')
	    		{{-- check if the invoice have some pending amount --}}
			    @if($invoice->calculation['pendingAmount'] > 0)
					<label class="exe_currency">{{ Auth::user()->company->currency->symbol }}</label> <input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" min="0.01" max="{{ $invoice->calculation['pendingAmount']  }}" value="{{ old('payments')[$key]['amount'] ?: null }}" step="any" />
			    @endif
			@else
			<?php //prePrint($invoice) ?>
		    	{{-- Going to edit the transaction for the invoice --}}
				@if($transaction_id)
				<label class="exe_currency">{{ Auth::user()->company->currency->symbol }}</label> <input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" max="{{ $invoice->calculation['pendingAmount'] + ($invoice->transaction_details[$invoice->id] ?? 0) }}" value="{{ old('payments')[$key]['amount'] ?: ($invoice->transaction_details[$invoice->id] ?? 0) }}" step="any" />
		    	@else
		    		{{-- Going to edit the credit note for the invoice --}}
		    			@if($invoice->calculation['pendingAmountWithoutCreditNote'] > 0)
			    			<input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" max="{{ $invoice->calculation['pendingAmountWithoutCreditNote'] }}" value="{{ old('payments')[$key]['amount'] ?: ($invoice->credit_note_details[$invoice->id] ?? 0) }}" step="any" />
			    		@endif
		    	@endif
		    @endif
		    
	    </td>
	</tr>
	@endif
	
@empty
	<tr class="empty-row">
		<td colspan="5">
    		<p>{{ __('labels.contact_have_no_invoices') }}</p>
		</td>
	</tr>
@endforelse