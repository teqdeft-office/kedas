{{-- to show the welcome pop up for new user. --}}

<div class="welcome-popup-outer">
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
	<!--step1-->
<form id="user-profile-form" onsubmit="return false;">
	<div class="steps-form step1">
		<a class="signout-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('labels.sign_out') }}<i class="fa fa-sign-out" title="sign out" aria-hidden="true"></i></a>
		<div class="welcome-popup-logo">
			<img src="{{ asset('/images/form-logo.png') }}">

		</div>
		<div class="welcome-popup-bottom">
			<div class="welcome-msg text-center">
				<h2>{{ __('labels.hi_welcome') }}</h2>
				<p>{{ __('labels.tell_us_about_yourself') }}</p>
			</div>
			<div class="custom-radio-container">
				<div class="custom-radio">
					<input type="radio" name="user_type" value="business_owner" id="activity1">
					<label for="activity1">
					<img src="{{ asset('/images/f-label-1.png') }}">
					{{ __('labels.own_a_business') }}
					</label>
				</div>
				<div class="custom-radio">
					<input type="radio" name="user_type" value="independent" id="activity2">
					<label for="activity2">
					<img src="{{ asset('/images/f-label-2.png') }}">
					{{ __('labels.an_independent') }}
					</label>
				</div>
				<div class="custom-radio">
					<input type="radio" name="user_type" value="student" id="activity3">
					<label for="activity3">
					<img src="{{ asset('/images/f-label-3.png') }}">
					{{ __('labels.an_student') }}
					</label>
				</div>
				<div class="custom-radio">
					<input type="radio" name="user_type" value="accountant" id="activity4">
					<label for="activity4">
					<img src="{{ asset('/images/f-label-4.png') }}">
					{{ __('labels.an_accountant') }}  
					</label>
				</div>
				<div class="custom-radio">
					<input type="radio" name="user_type" value="other" id="activity5">
					<label for="activity5">
					<img src="{{ asset('/images/f-label-5.png') }}">
					{{ __('labels.others') }}
					</label>
				</div>
			</div>
		</div>
	</div>
	<!--step2-->
	<div class="steps-form step2">
		<a class="signout-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('labels.sign_out') }}<i class="fa fa-sign-out" title="sign out" aria-hidden="true"></i></a>
		<div class="welcome-popup-logo">
			<a href="#"><img src="{{ asset('/images/form-logo.png') }}"></a>
		</div>
		<div class="welcome-popup-bottom">
			<div class="welcome-msg text-center">
				<h3>{{ __('labels.your_business_information') }}</h3>
				<p>{{ __('labels.tell_us_more_so_that_we_can_adapt_to_your_needs') }}</p>
				<div id="profile-update-error"></div>
				<div class="loader-container">
					<span id="loader" style="display: none;"></span>
				</div>
			</div>
				<div class="need-form">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{{ __('labels.your_name_or_the_name_of_your_business') }}" id="name" name="name" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{{ __('labels.role_in_your_company') }}" id="role" name="role" required>
					</div>
					<div class="form-group">
						{!! Form::select('sector', $sector_options, '', ['class'=>"form-control single-search-selection", 'id'=>"sector"]) !!}
					</div>
					<div class="form-group">
						{!! Form::select('currency', $currency_options, '', ['class'=>"form-control single-search-selection", 'id'=>"currency", 'required' => 'required']) !!}
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{{ __('labels.tax_id') }}" id="tax_id" name="tax_id" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{{ __('labels.address') }}" id="address" name="address" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{{ __('labels.city') }}" id="city" name="city">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{{ __('labels.state') }}" id="state" name="state">
					</div>
					<div class="form-group">
						{!! Form::select('country', $country_options, '', ['class'=>"form-control single-search-selection", 'id'=>"country", 'required' => 'required']) !!}
					</div>
					<div class="form-group">
						<input id="phone" name="phone" type="tel" class="form-control" placeholder="{{ __('labels.telephone') }}" required>
					</div>
					<div class="form-group">
						<input id="mobile" name="mobile" type="tel" class="form-control" placeholder="{{ __('labels.mobile') }}">
					</div>
				</div>
				<div class="back-next-btn">
					<button type="button" name="back" class="btn-custom previous-btn">{{ __('labels.back') }}</button>
					<button type="button" id="submit-user-profile-form" name="next" class="btn-custom">{{ __('labels.next') }}</button>
				</div>
		</div>
	</div>
	<!--step3-->
	<div class="steps-form step3">
		<button type="button" class="close user-profile-close" name="close-steps">×</button>
		<div class="welcome-popup-logo">
			<a href="#"><img src="{{ asset('/images/form-logo.png') }}"></a>
		</div>
		<div class="welcome-popup-bottom">
			<div class="welcome-msg text-center">
				<h2>{{ __('labels.you_are_all_set') }} </h2>
				<h3>{{ __('labels.ready_to_see_your_business_grow') }} </h3>
				<div class="buttons-group">
					<button type="button" name="firstinvoice" class="btn-custom user-profile-close">{{ __('labels.create_my_first_invoice') }}</button>
					<button type="button" name="start" class="btn-custom user-profile-close">{{ __('labels.start') }}</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>