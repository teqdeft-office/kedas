{{-- to show the charts of account at any level. --}}
@foreach ($accounting_heads as $accounting_head)
	<a class="dropdown-item" data-value="concept-{{$accounting_head->id}}" data-level="{{$accounting_head->level}}" href="javascript::void(0)">
                                      <span class="account-name">{{$accounting_head->custom_code}} - {{$accounting_head->name}}</span>
	    </a>
	
	@if($accounting_head->where('parent_id', $accounting_head->id))
	    @include('includes.chart-children-select', ['accounting_heads' => $accounting_head->where('parent_id', $accounting_head->id)->get(), 'level' => $level+1])
	@endif
@endforeach