{{-- to show the charts of account at any level. --}}
@foreach ($accounting_heads as $accounting_head)
	<tr class="tr-row level-{{$level}}">
	    <!-- <td class="cell" colspan="1"><span class="fs-exclude"></span></td> -->
	    <td class="cell" colspan="1">
	    	@if($level == 1)
	    	<i class="fa fa-caret-down carrot-icon" aria-hidden="true"></i>
	    	@else
	    	<i class="fa fa-minus" aria-hidden="true"></i>
	    	@endif
			<span class="chart-of-accounts-table__account-name-column">
	    <span class="fs-exclude">{{$accounting_head->custom_name }}</span><!-- <span class="wv-text wv-text--hint">{{"Last transaction on October 6, 2020"}}</span> --></span>
	    </td>
	    <td class="cell" colspan="1"><span class="fs-exclude">{{$accounting_head->description}}</span></td>
	    <td class="cell" colspan="1"></td>
	    <td class="cell cell--actions text-center" colspan="1">
	        <div class="action">
	            <a href="#" data-id="{{ $accounting_head->id }}" data-name="{{ $accounting_head->name }}"  data-description="{{ $accounting_head->description }}" data-code="{{ $accounting_head->custom_code }}" data-opening_balance="{{ $accounting_head->opening_balance }}" class="edit-accounting-head" data-toggle="modal" data-target="#editChartOfAccountModal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
	            @if(!$accounting_head->resource_associated)
	            {{-- 
	            <a class="delete_resource" data-resource="destroy-account-head-{{ $accounting_head->id }}" href="{{ route('charts.destroy', $accounting_head) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
	            <form action="{{ route('charts.destroy', $accounting_head) }}" method="POST" accept-charset="UTF-8" id="destroy-account-head-{{ $accounting_head->id }}" style="display: none">
	                @csrf
	                @method('delete')
	            </form>
	            --}}
	            @endif
	        </div>
	    </td>
	</tr>
	@if($accounting_head->where('parent_id', $accounting_head->id))
	    @include('includes.chart-children', ['accounting_heads' => $accounting_head->where('parent_id', $accounting_head->id)->get(), 'level' => $level+1])
	@endif
	@if($accounting_head->code == 1005)
		@include('includes.chart-banks', ['accounting_heads' => $user->company->banks->where('account_type', 'saving'), 'level' => $level+1])
	@endif
	@if($accounting_head->code == 1002)
		@include('includes.chart-banks', ['accounting_heads' => $user->company->banks->where('account_type', '!=', 'saving'), 'level' => $level+1])
	@endif
@endforeach