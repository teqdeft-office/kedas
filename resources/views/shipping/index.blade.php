@extends('layouts.dashboard')
@section('title', trans('labels.shipping'))
@section('content')
<div class="dashboard-right cust-dashboard-md-right">
 <div class="menu-icon">
  <i class="fa fa-bars" aria-hidden="true"></i>
 </div>
<div class="invoice-container">
    <h2>{{ __('labels.shipping') }}</h2>
    <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
     <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.invoice') }},</span>
    </div>
    <div class="tabs-custom">
    <div class="tab-content">
    <div class="tab-pane active">
    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
     <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
     <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
    </div>
    <div class="list-table-custom  products-table table-responsive">
    <table class="table table-striped" id="shipping-adjustments-list-table">
        <thead>
          <tr>
            <th>{{ __('labels.id') }}</th>
            <th>{{ __('labels.contact') }}</th>
            <th>{{ __('labels.creation_date') }}</th>
            <th>{{ __('labels.delivery_date') }}</th> 
            <th>{{ __('labels.invoice_number') }}</th> 
            <th>{{ __('labels.notes') }}</th>
            <th>{{ __('labels.status') }}</th>
            <th>{{ __('labels.actions') }}</th>
          </tr>
          </thead>
           <tbody>
            @foreach ($invoices as $invoice)
            <tr>
             <td>{{ $invoice->id }}</td>
             <td>{{ $invoice->contact->name }}</td>
             <td>{{ dateFormat($invoice->start_date) }}</td>
             <td>{{ dateFormat($invoice->expiration_date) }}</td>
             <td>{{ $invoice->internal_number }}</td>
             <td>{{ $invoice->notes }}</td>
             <td>{{ $invoice->calculation['status'] }}</td>
             <td class="to-show">
              <div class="inner-to-show">
               <i class="fa fa-sort-desc action-btn" class="dropdown-toggle"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
              <div class="dropdown-menu">
                <ul>
                 <li><a href="{{route('shipping.show', $invoice)}}">{{ __('labels.view') }}</a></li>
                </ul> 
              </div>
                 </div>
             </td>
            </tr>
            @endforeach
          </tbody>
          </table>
         </div>
        </div>
       </div>
    </div>
   </div>
  </div>
<div class="clearfix"></div>
@endsection