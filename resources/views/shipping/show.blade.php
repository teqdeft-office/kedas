@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.invoice'))
@section('content')
<input type="hidden" name="resource_type" value="">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block"href="{{ route('shipping.index') }}">{{ __('labels.back') }}</a>
          @foreach ($invoices as $invoice)
          <h2><b>{{ __('labels.invoice') }}  {{ $invoice->internal_number }}</b></h2>
          @endforeach
    </div>
        <div class="action-btns-view show-wrap-btns">
        <div class="show-wrap-btns-inner">
        <!-- <a class="btn-custom print-this-shipping" data-id="{{ $invoice->id }}" data-type="{{ $viewType }}"><i class="fa fa-print"></i> {{ __('labels.print') }}</a> -->
        <!-- <a role="button" data-toggle="modal" data-target="#ShippingEmailModal" href="javascript:void(0)" class="btn-custom"><i class="fa fa-envelope"></i> {{ __('labels.send_by_mail') }}</a>        -->
        <a href="#"class="btn-custom"><i class="fa fa-print"></i> </i> {{ __('labels.print') }}</a> 
        <a href="#" class="btn-custom" id="invoice-download-btn"><i class="fa fa-download"></i> {{ __('labels.download') }}</a>
        <a href="#" class="btn-custom"><i class="fa fa-envelope"></i> {{ __('labels.send_by_mail') }}</a>
        <a href="#" class="btn-custom" id="invoice-download-btn"> {{ __('labels.mark_as_shipped') }}</a>
    </div>
    </div>
        <div class="  products-table table-responsive">
         <div class="default-form view-form">
           <div class="generate-invoice">
            <div id="invoice-custom-wrap-block" class="view-form-custom custom-view-info-client">
             <table class="table class="cutom-td"">
             @foreach ($invoices as $invoice)
             <tr>
              <td class="cutom-td"><b>{{ __('labels.client') }}</b></td>
              <td>{{ $invoice->contact->name }}</td>
              <td><b>{{ __('labels.creation_date') }}</b></td>
              <td>{{ dateFormat($invoice->start_date) }}</td>
             </tr>
             <tr>
              @if($viewType != 'production-order')
              <td><b>{{ __('labels.tax_id') }}</b></td>
              <td>{{ $invoice->contact->tax_id }}</td>
              @endif
              <td><b>{{ __('labels.expiration_date') }}</b></td>
              <td>{{ dateFormat($invoice->expiration_date) }}</td>
             </tr>
             <tr>
              <td><b>{{ __('labels.phone') }}</b></td>
              <td>{{ $invoice->contact->phone }}</td>
              <td><b>{{ __('labels.term') }}</b></td>
              <td>{{ Config::get('constants.terms')[$invoice->term] }} {{ $invoice->term == 'manual' ? '('.$invoice->manual_days.' '.__('labels.days').')' : '' }} </td>
            </tr>
            @endforeach
            </tbody>
           </table>
         </div>
         <div class="default-table add-invoice-table">
           <div class="list-table-custom">
             <div class="generate-invoice">
              <table class="table table-striped">
                <thead>
                 <tr>
                  <th>{{ __('labels.item') }}</th>
                  <th>{{ __('labels.reference') }}</th>
                  <th>{{ __('labels.description') }}</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($invoice->items as $item)
                 <tr>
                  <td><a href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a></td>
                  <td>{{$item->reference}}</td>
                  <td>{{showIfAvailable($item->inventory->description)}}</td>
                 </tr>
                  @endforeach
                </tbody>
               </table>
            </div>
           </div>
         </div>
         <div class="terms-conditions-form">
             <div class="row">
                <div class="form-group col-sm-4">
                 <label for="tac">{{ __('labels.terms_and_conditions') }}</label>
                 <textarea class="form-control" name="tac">{{ old('tac', null) }}</textarea>
                </div>
                <div class="form-group col-sm-4">
                 <label for="notes col-sm-12">{{ __('labels.notes') }}</label>
                 <textarea class="form-control" name="notes" id="notes">{{ $invoice->notes }}</textarea>
                </div>
                <div class="form-group col-sm-4">
                 <label for="res_text col-sm-12">{{ __('labels.resolution_text') }}</label>
                 <textarea class="form-control" name="res_text" id="res_text">{{ old('res_text', null) }}</textarea>
                </div>
              </div>
             </div>
           </div>
         <div class="clearfix"></div>
         <div class="modal fade" id="ShippingEmailModal" tabindex="-1" role="dialog" aria-labelledby="shippingEmailModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
           <form id="send-shipping-mail" action="{{ route('invoices.mail', ['invoice' => $invoice, 'view' => $viewType]) }}" method="POST">
            @csrf
            <input type="hidden" name="template_number" value="15">
           <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="shippingEmailModalLabel">{{ __('labels.send_invoice_to_emails') }}</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
                 <span aria-hidden="true">&times;</span>
                 </button>
            </div>
            <div class="modal-body">
             <input type="text" class="form-control" required="required" name="emails" placeholder="{{ __('labels.separating_addresses_by_comma') }}"
                 value="{{ $invoice->contact->email ? $invoice->contact->email.',': null }}">
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary"
                 data-dismiss="modal">{{ __('labels.cancel') }}</button>
            <button type="submit" class="btn btn-primary">{{ __('labels.send') }}</button>
           </div>
          </div>
         </form>
        </div>
       </div>
       <div style="display:none">
       <div class="print-invoice-shipping">
       </div>
       </div>
@endsection

