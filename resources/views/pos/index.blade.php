@extends('layouts.pos')

@section('title', trans('labels.pos'))

@section('content')
<!-- <example-component></example-component> -->
<sales-or-receives-component :user="{{ Auth::user() }}"
							customer="{{ $contact_options }}"
							ncfs="{{ $ncf_options }}"
							currencies="{{ $currency_options }}"
							bank_accounts="{{ $bank_accounts_options }}"
							>								
</sales-or-receives-component>
@endsection
