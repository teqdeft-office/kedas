@extends('layouts.dashboard')

@section('title', trans('labels.remissions'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.remissions') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.remissions') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('remissions.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.remission') }}</a>
        </div>
        <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
            <span>{{ __('labels.filter') }}: <input id="remissions-filter" type="text" autocomplete="off"  value=" "></span>
            <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text" autocomplete="off"></span>
            <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text" autocomplete="off"></span>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table" id="remission-table">
                        <table class="table table-striped" id="remissions-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.number') }}</th>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($remissions as $remission)
                                <tr>
                                    <td>{{ $remission->internal_number }}</td>
                                    <td>{{ $remission->contact->name }}</td>
                                    <td>{{ dateFormat($remission->start_date) }}</td>
                                    <td>{{ moneyFormat($remission->calculation['total']) }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a href="{{route('remissions.show', $remission)}}">{{ __('labels.view') }}</a></li>
                                                    <li><a href="{{route('remissions.edit', $remission)}}">{{ __('labels.edit') }}</a></li>
                                                    <li><a class="delete_resource" data-resource="{{ 'destroy-remission-form-' . $remission->id }}" href="{{route('remissions.destroy', $remission)}}">{{ __('labels.delete') }}</a></li>
                                                    {{ Form::open(array('method'=>'DELETE','route' => ['remissions.destroy', $remission], 'id' => "destroy-remission-form-{$remission->id}", 'style' => 'display: none')) }}
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.remission') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('remissions.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.remission') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                    <th>Contact</th>
                                    <th>Creation date</th>
                                    <th>Expiration date</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Pending</th>
                                    <th>Status</th>
                                    <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
