@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.employee'))
@section('content')
<div class="dashboard-right">
   <div class="menu-icon">
      <i class="fa fa-bars" aria-hidden="true"></i>
   </div>
   <div class="invoice-container user-invoice-view-details">
      <a class="arrow-back d-block" href="{{ route('employees.index') }}">{{ __('labels.back') }}</a>
      <h2>{{ titleCase($employee->name) }} Details</h2>
      <div class="default-form view-form-detail">
         <div class="generate-invoice">
            <div class="add-form row">
               <div class="form-group col-sm-12 col-md-6">
                  <label for="name"><b>{{ __('labels.name') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->name) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="name"><b>{{ __('labels.id_number') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->id_number) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="name"><b>{{ __('labels.hiring_date') }}: </b></label>
                  <span class="value-form">{{ dateFormat($employee->hiring_date) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="salary"><b>{{ __('labels.email') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->email) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="mobile"><b>{{ __('labels.mobile') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->mobile) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="phone"><b>{{ __('labels.phone') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->phone) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="zip"><b>{{ __('labels.zip_code') }}: </b></label>
                  <span class="value-form">{{ $employee->address ? showIfAvailable($employee->address->zip) : 'N/A'  }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="salary"><b>{{ __('labels.salary') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->salary) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="insurance_cost"><b>{{ __('labels.insurance_cost') }}: </b></label>
                  <span class="value-form">{{ moneyFormat($employee->insurance_cost) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="insurance_discount"><b>{{ __('labels.insurance_discount') }}: </b></label>
                  <span class="value-form">{{ moneyFormat($employee->insurance_discount) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="name"><b>{{ __('labels.final_date') }}: </b></label>
                  <span class="value-form">{{ dateFormat($employee->final_date) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="name"><b>{{ __('labels.AFP') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->AFP) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="username"><b>{{ __('labels.username') }}: </b></label>
                  <span class="value-form">{{ showIfAvailable($employee->username) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="password"><b>{{ __('labels.password') }}: </b></label>
                  <span class="value-form">......</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="date_of_birth"><b>{{ __('labels.date_of_birth') }}: </b></label>
                  <span class="value-form">{{ dateFormat($employee->birth_date) }}</span>
               </div>
               <div class="form-group col-sm-12 col-md-6">
                  <label for="status"><b>{{ __('labels.status') }}: </b></label>
                  <span class="value-form">{{ Config::get('constants.emp_status')[$employee->status] }}</span>
               </div>
                <div class="form-group col-sm-12 col-md-6">
                  <fieldset>
                     <label><b>{{ __('labels.address') }}: </b></label>
                     <span class="value-form">{{ addressFormat($employee->address) }}</span>
                  </fieldset>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clearfix"></div>
@endsection