@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.employee'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_employee') }}</h2>
        <form method="POST" action="{{ route('employees.store') }}" id="add-edit-employee-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.id_type') }} <span>*</span></label>
                            <input class="form-control" type="text" name="type_of_id" placeholder="{{ __('labels.type_of_id') }}" required value="{{ old('type_of_id') }}" autocomplete="off" />
                            @error('type_of_id')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.id_number') }} <span>*</span></label>
                            <input class="form-control" type="text" name="id_number" placeholder="{{ __('labels.id_number') }}" required value="{{ old('id_number') }}" autocomplete="off" />
                            @error('id_number')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name') }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">{{ __('labels.email') }} <span>*</span></label>
                            <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email_address') }}" required value="{{ old('email') }}" autocomplete="email" />
                            @error('email')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('labels.address') }}</label>
                            <input class="form-control" type="text" name="address" placeholder="{{ __('labels.address') }}" value="{{ old('address') }}" autocomplete="off" />
                            @error('address')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="state">{{ __('labels.state') }}</label>
                            <input class="form-control" type="text" name="state" placeholder="{{ __('labels.state') }}" value="{{ old('state') }}" autocomplete="off" />
                            @error('state')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">{{ __('labels.city') }}</label>
                            <input class="form-control" type="text" name="city" placeholder="{{ __('labels.city') }}" value="{{ old('city') }}" autocomplete="off" />
                            @error('city')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="zip">{{ __('labels.zip_code') }}</label>
                            <input class="form-control" type="text" name="zip" placeholder="{{ __('labels.zip') }}" value="{{ old('zip') }}" autocomplete="off" />
                            @error('zip')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('labels.country') }}</label>
                            {!! Form::select('country', $country_options, '', ['class'=>"form-control single-search-selection", 'id'=>"country"]) !!}
                            @error('country')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone">{{ __('labels.phone') }}</label>
                            <input class="form-control" type="tel" name="phone" placeholder="{{ __('labels.phone') }}" value="{{ old('phone') }}" autocomplete="off" />
                            @error('phone')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="mobile">{{ __('labels.mobile_number') }} <span>*</span></label>
                            <input class="form-control" type="tel" name="mobile" placeholder="{{ __('labels.mobile') }}" value="{{ old('mobile') }}" autocomplete="off" />
                            @error('mobile')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.date_of_birth') }} <span>*</span></label>
                            <input id="date_of_birth" class="form-control" name="birth_date"
                                required="required" value="{{old('birth_date', '')}}"
                                autocomplete="off" />
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('labels.period') }}</label>
                            {!! Form::select('period', $period_options, '', ['class'=>"form-control single-search-selection", 'id'=>"period"]) !!}
                            @error('period')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('labels.contract_type') }}</label>
                            {!! Form::select('contract_type', $contract_type_options, '', ['class'=>"form-control single-search-selection", 'id'=>"period"]) !!}
                            @error('period')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bank_account">{{ __('labels.bank_account') }}</label>
                            <input class="form-control" type="text" name="bank_account" placeholder="{{ __('labels.bank_account') }}" value="{{ old('bank_account') }}" autocomplete="off" />
                            @error('bank_account')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bank_name">{{ __('labels.bank_name') }}</label>
                            <input class="form-control" type="text" name="bank_name" placeholder="{{ __('labels.bank_name') }}" value="{{ old('bank_name') }}" autocomplete="off" />
                            @error('bank_name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="type_of_account">{{ __('labels.type_of_account') }}</label>
                            {!! Form::select('type_of_account', $type_of_account_options, '', ['class'=>"form-control single-search-selection", 'id'=>"type_of_account"]) !!}
                            @error('type_of_account')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="note">{{ __('labels.note') }}</label>
                            <textarea class="form-control" name="note"
                                id="note">{{ old('note', null) }}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.admission_date') }} <span>*</span></label>
                            <input id="hiring_date" class="form-control" name="hiring_date"
                                required="required" value="{{old('hiring_date','')}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">{{ __('labels.role') }} <span>*</span></label>
                            {!! Form::select('role', $role_options, '', ['class'=>"form-control", 'id'=>"role"]) !!}

                            {{-- <input class="form-control" type="text" name="role" placeholder="{{ __('labels.role') }}" required value="{{ old('role') }}" autocomplete="off" /> --}}
                            @error('role')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="department" class="col-sm-12 creat-nw-cat">{{ __('labels.department') }} <span>*</span> <a target="_blank" href="{{ route('departments.create') }}">{{ __('labels.create_new_department') }}</a></label>
                            {!! Form::select('department_id', $department_options, '', ['class'=>"form-control single-search-selection", 'id'=>"department_id"]) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="AFP">{{ __('labels.salary') }} <span>*</span></label>
                            <input class="form-control" type="text" name="salary" placeholder="{{ __('labels.salary') }}" value="{{ old('salary') }}" autocomplete="new-salary" />
                            @error('salary')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="insurance_brand" class="col-sm-12 creat-nw-cat">{{ __('labels.insurance_company') }} <a target="_blank" href="{{ route('insurance-brands.create') }}">{{ __('labels.create_new_insurance_brand') }}</a></label>
                            {!! Form::select('insurance_brand_id', $insurance_options, '', ['class'=>"form-control single-search-selection", 'id'=>"insurance_brand_id"]) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="AFP">{{ __('labels.hours_price') }} </label>
                            <input class="form-control" type="text" name="hours_price" placeholder="{{ __('labels.hours_price') }}" value="{{ old('hours_price') }}" autocomplete="new-hours_price" />
                            @error('hours_price')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                         <div class="form-group col-md-6">
                            <label for="username">{{ __('labels.username') }} <span>*</span></label>
                            <input class="form-control" type="text" name="username" placeholder="{{ __('labels.username') }}" value="{{ old('username') }}" autocomplete="off" />
                            @error('username')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="AFP">{{ __('labels.password') }} <span>*</span></label>
                            <input class="form-control" type="password" name="password" placeholder="{{ __('labels.password') }}" value="{{ old('password') }}" autocomplete="new-password" />
                            @error('password')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="salary_type" class="col-sm-12 creat-nw-cat">{{ __('labels.salary_type') }} <span>*</span></label>
                            {!! Form::select('salary_type', $salary_type_options, '', ['class'=>"form-control single-search-selection", 'id'=>"salary_type"]) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.insurance_id') }} </label>
                            <input class="form-control" type="text" name="insurance_id" placeholder="{{ __('labels.insurance_id') }}" value="{{ old('insurance_id') }}" autocomplete="off" />
                            @error('insurance_id')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.driver_license') }} </label>
                            <input class="form-control" type="text" name="driver_license" placeholder="{{ __('labels.driver_license') }}" value="{{ old('driver_license') }}" autocomplete="off" />
                            @error('driver_license')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>   
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.blood_type') }} </label>
                            <input class="form-control" type="text" name="blood_type" placeholder="{{ __('labels.blood_type') }}" value="{{ old('blood_type') }}" autocomplete="off" />
                            @error('blood_type')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('labels.break_time') }} </label>
                            <input class="form-control" type="text" name="break_time" placeholder="{{ __('labels.break_time') }}" value="{{ old('break_time') }}" autocomplete="off" />
                            @error('break_time')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="status" class="col-sm-12 creat-nw-cat">{{ __('labels.status') }} <span>*</span></label>
                            {!! Form::select('status', $emp_status_options, '', ['class'=>"form-control single-search-selection", 'id'=>"status"]) !!}
                        </div>
                          <div class="col-lg-6 col-md-6">
                            <div class="form-group prduct-pic">
                                <div class="form-group file-input file-input-large">
                                    <label for="image" id="image-preview-label">
                                        <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                        {{ __('labels.image') }}
                                    </label>
                                    <img src="" id="image-preview" style="display:none" />                            
                                    <input type="file" id="item-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label">
                                    <a class="upload-button" data-preview_section="item-image"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                    @error('image')
                                        <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>                            
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.pdf') }}, {{ __('labels.image') }})</label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Start Time <span>*</span></label>
                                <input id="extra_hours_start_time" name="start_time" class="form-control"
                                required="required" value="{{old('start_time')}}"
                                autocomplete="off" type="text" />
                                @error('start_time')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">End Time <span>*</span></label>
                                <input id="extra_hours_end_time" name="end_time" class="form-control"
                                required="required" value="{{old('end_time')}}"
                                autocomplete="off" />
                                @error('end_time')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6 cust-wrap-time-cell">
                            <label>Working Days</label>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Monday</td>
                                        <td>       
                                            <label class="cust-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="monday" name="monday" value="1" {{ old("monday") == 1 ? "checked" : "" }}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tuesday</td>
                                        <td>       
                                        <label class="cust-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tuesday" name="tuesday" value="1" {{ old("tuesday") == 1 ? "checked" : "" }}>
                                            <span class="checkmark"></span>
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Wednesday</td>
                                        <td>       
                                        <label class="cust-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="wednesday" name="wednesday" value="1" {{ old("wednesday") == 1 ? "checked" : "" }}>
                                            <span class="checkmark"></span>
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Thursday</td>
                                        <td>       
                                        <label class="cust-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="thursday" name="thursday" value="1" {{ old("thursday") == 1 ? "checked" : "" }}> 
                                            <span class="checkmark"></span>
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Friday</td>
                                        <td>       
                                        <label class="cust-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="friday" name="friday" value="1" {{ old("friday") == 1 ? "checked" : "" }}> 
                                            <span class="checkmark"></span>
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Saturday</td>
                                        <td>       
                                        <label class="cust-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="saturday" name="saturday" value="1" {{ old("saturday") == 1 ? "checked" : "" }}> 
                                            <span class="checkmark"></span>
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sunday</td>
                                        <td>       
                                        <label class="cust-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="sunday" name="sunday" value="1" {{ old("sunday") == 1 ? "checked" : "" }}> 
                                            <span class="checkmark"></span>
                                        </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('employees.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>  
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        addDropZone(null, 'both', 10);
    </script>
@endpush