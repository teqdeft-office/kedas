@extends('layouts.dashboard')

@section('title', trans('labels.employees'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.employees') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.employee') }}</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
           <!--  <a href="{{ route('import_resource', ['resource' => 'employees']) }}" class="btn-custom outline-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ __('labels.import_from_excel') }}</a>
            <a href="{{ route('export_resource', ['resource' => 'employees']) }}" class="btn-custom outline-btn"><i class="fa fa-download" aria-hidden="true"></i> {{ __('labels.export_employees') }}</a> -->
            <a href="{{ route('employees.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_employee') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="employees-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.id_number') }}</th>
                                    <th>{{ __('labels.start_date') }}</th>
                                    <th>{{ __('labels.salary') }}</th>
                                    <th>{{ __('labels.phone') }}</th>
                                    <th>{{ __('labels.role') }}</th>
                                    <th>{{ __('labels.schedules') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($employees as $employee)
                                <tr>
                                    <td>{{ $employee->name }}</td>
                                    <td>{{ showIfAvailable($employee->id_number) }}</td>
                                    <td>{{ dateFormat($employee->start_date) }}</td>
                                    <td>{{ moneyFormat($employee->salary) }}</td>
                                    <td>{{ showIfAvailable($employee->phone) }}</td>
                                    <td>{{ $employee->role }}</td>
                                    <td>{{ timeFormat($employee->start_time)." - ".timeFormat($employee->end_time) }}</td>
                                    <td>{{ $employee->status }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a href="{{route('employees.show', $employee)}}">{{ __('labels.view') }}</a></li>
                                                    <li><a href="{{route('employees.edit', $employee)}}">{{ __('labels.edit') }}</a></li>
                                                    <li><a class="delete_resource" data-resource="{{ 'destroy-employee-form-' . $employee->id }}" href="{{route('employees.destroy', $employee)}}">{{ __('labels.delete') }}</a></li>
                                                    {{ Form::open(array('method'=>'DELETE','route' => ['employees.destroy', $employee], 'id' => "destroy-employee-form-{$employee->id}", 'style' => 'display: none')) }}
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_create_new').' '.__('labels.employee') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('employees.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_employee') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
