@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.account_receivable'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.account_receivable') }}</h2>
        <div class="default-form add-invoice-form">
            <form method="POST" action="{{ route('receivable.store') }}" id="add-edit-invoices-form"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="remission_id" value="{{ $remission_id }}">
                <div class="generate-invoice">
                    <div class="row no-margin">
                        <div class="col-lg-4">
                            <div class="form-group file-input file-input-medium">
                                @if(!Auth::user()->company->logo)
                                <label for="image" id="image-preview-label">
                                    <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                    {{ __('labels.logo') }}
                                </label>
                                @endif
                                @if(Auth::user()->company->logo)
                                <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                    alt="{{ Auth::user()->company->name }}" />
                                @else
                                <img id="image-preview" src="" />
                                @endif
                                <input type="file" id="company-logo" name="logo" class="file-upload-preview"
                                    accept="image/*" data-img_id="image-preview"
                                    data-msg-accept="{{ __('messages.invalid_image_file_error') }}"
                                    data-label_id="image-preview-label" />
                                <a class="upload-button" data-preview_section="company-logo"><i class="fa fa-pencil"
                                        aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('logo')
                                <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-8 text-center company-data">
                            <div class="add-cell">
                                <div class="consumption-box">
                                    <div class="consumption-bottom">
                                        <span>{{ __('labels.no') }}.</span>
                                        <div class="consumption-number">{{ $next_invoice_number }}</div>
                                    </div>
                                </div>
                                <div class="add-cell-center">
                                    <h2>{{ Auth::user()->company->name }}</h2>
                                    @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">
                                            {{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                    @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">
                                            {{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3>
                                        <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->mobile)
                                        <h3>{{ __('labels.mobile') }}:</h3>
                                        <p>{{ Auth::user()->company->mobile }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                    <span>
                                        <h3>{{ __('labels.email') }}:</h3>
                                        <p><a href="mailto:{{Auth::user()->company->support_email}}">
                                                {{ Auth::user()->company->support_email }} </a></p>
                                    </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                    <span>
                                        <h3>{{ __('labels.tax_id') }}:</h3>
                                        <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="contact" class="col-sm-12 creat-nw-cat">{{ __('labels.contact') }} <span>*</span> <a target="_blank" href="{{ route('clients.create') }}">{{ __('labels.create_new_contact') }}</a></label>
                            <!-- <label for="contact">{{ __('labels.contact') }} <span>*</span></label> -->
                            {!! Form::select('contact_id', $contact_options, old('contact_id', null),
                            ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' =>
                            'required'], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="invoice_start_date" class="form-control" name="invoice_start_date"
                                required="required" value="{{old('invoice_start_date', getTodayDate())}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact-tax-id">{{ __('labels.tax_id') }}</label>
                            <input type="text" disabled class="form-control" name="contact-tax-id" id="contact-tax-id">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact-phone">{{ __('labels.phone') }}</label>
                            <input type="text" disabled class="form-control" name="contact-phone" id="contact-phone">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="term">{{ __('labels.term') }}</label>
                            {!! Form::select('term', $term_options, old('term', null), ['class'=>"form-control",
                            'id'=>"term", 'required' => 'required']) !!}
                            <span id="manual_days_section" style="display: none;">
                                <label for="manual_days">{{ __('labels.manual_days') }} <span>*</span></label>
                                <input type="number" name="manual_days" id="manual_days" class="form-control" min="1"
                                    max="4000" value="{{old('manual_days', 1)}}" />
                            </span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="invoice_expiration_date">{{ __('labels.expiration_date') }}
                                <span>*</span></label>
                            <input id="invoice_expiration_date" name="invoice_expiration_date" class="form-control"
                                required="required" value="{{old('invoice_expiration_date', getTodayDate())}}"
                                autocomplete="off" disabled="disabled" />
                        </div>
                        <div class="currency_exchange_cnt col-lg-6">
                            <div class="row">
                                <div class="form-group col-lg-4 col-md-12">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-lg-4 col-md-12 custom-select-controller">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    {!! Form::select('exchange_currency', $currency_options, old('exchange_currency',
                                    null), ['class'=>"form-control single-search-selection", 'id'=>"exchange-currency"],
                                    $currency_attributes) !!}
                                    @error('exchange_currency')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-lg-4 col-md-12">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{ old('exchange_rate') ?: null }}" autocomplete="off" min="0.01" step="any" />
                                    @error('exchange_rate')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                    <label class="currency_exchange_inner" style="display:none">1 <span
                                        class="exchange_currency_label"></span> =
                                    {{ Auth::user()->company->currency->code }}
                                    ({{ Auth::user()->company->currency->symbol }}) <span
                                        class="exchange_currency_rate"></span> </label>
                                </div>
                    
                            </div>
                        </div>
                        <div class="form-group col-lg-6 custom-select-controller">
                            <label for="ncf_id">{{ __('labels.ncf') }}</label>
                            {!! Form::select('ncf_id', $ncf_options, old('ncf_id', null), ['class'=>"form-control
                            single-search-selection", 'id'=>"ncf_id"]) !!}
                        </div>
                         <div class="form-group col-lg-6">
                            <label for="bank_account">{{ __('labels.account') }} <span>*</span></label>
                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="account-multilevelselect">
                                <button type="button" class="btn btn-secondary dropdown-toggle" id="account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu" aria-labelledby="account-multilevelselect-button">
                                    <div class="hs-searchbox">
                                        <input type="text" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="hs-menu-inner">
                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                      @foreach($parent_accounting_heads as $key => $parent_accounting_head)
                                         @forelse($accounting_heads->where('parent_id', $parent_accounting_head->id)->where('visibility', true) as $sub_key => $sub_accounting_head)
                                     <a class="dropdown-item" data-value="concept-{{$sub_accounting_head->id}}" data-level="{{$sub_accounting_head->level}}" href="javascript::void(0)">
                                      <span class="account-name">{{$sub_accounting_head->custom_code}} - {{$sub_accounting_head->name}}</span>
                                       
                                        @if($accounting_heads->where('parent_id', $sub_accounting_head->id)->isNotEmpty())
                                            @include('includes.chart-children-select', ['accounting_heads' => $accounting_heads->where('parent_id', $sub_accounting_head->id)->where('visibility', true), 'level' => 1])
                                        @else
                                        
                                        <a class="dropdown-item" data-value="concept-{{$sub_accounting_head->id}}" data-level="{{$sub_accounting_head->level}}" href="javascript::void(0)">
                                      <span class="account-name">
                                        {{ __('labels.dont_have_account_under_head', ['accounting_head_name' => $sub_accounting_head->name]) }}
                                       </a>
                                        @endif
                                        @empty
                                       
                                    @endforelse


                                        @endforeach
                                    </div>
                                </div>
                                <input class="d-none" name="account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('bank_account', null) }}" />
                            </div>
                             </div>
                        <div class="col-sm-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.pdf') }}, {{ __('labels.image') }})</label>
                                <div class="form-control dropzone" id="document-dropzone">
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                    </div>

                   <div class="default-table add-invoice-table with-concept-table">
                        <div class="list-table-custom">
                            <table class="table table-striped mb-0" id="concept_items">
                                <thead>
                                    <tr>
                                        <th class="th-concept">{{ __('labels.concept') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="td-concept">
                                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-0">
                                                <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                                                    <div class="hs-searchbox">
                                                        <input type="text" class="form-control" autocomplete="off">
                                                    </div>
                                                    <div class="hs-menu-inner">
                                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                                                        @if($trackedInventories->isNotEmpty())
                                                        <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.inventory') }}</strong>
                                                        @foreach($trackedInventories as $inventory_account)
                                                            <a class="dropdown-item" data-value="inventory-{{$inventory_account->id}}" data-level="1" href="javascript::void(0)">
                                                                <span class="account-name">{{$inventory_account->name}}</span>
                                                                @if($inventory_account->level == 1)
                                                                <span class="detail-type">{{$inventory_account->custom_code}} -{{ $inventory_account->parent->name }}</span>
                                                                @endif
                                                            </a>
                                                        @endforeach
                                                        @endif
                                                        <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                                                        @foreach($all_accounts_options as $all_account)
                                                            <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                                                                <span class="account-name">{{$all_account->custom_code}} -{{$all_account->name}}</span>
                                                                @if($all_account->level == 1)
                                                                <span class="detail-type">{{$all_account->custom_code}} -{{ $all_account->parent->name }}</span>
                                                                @endif
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <input class="d-none" name="concepts[0][item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('concepts')[0]['item'] ?: null }}" />
                                            </div>
                                            @error('concepts[0][item]')
                                            <span class="error" role="alert">{{ $message }}</span>
                                            @enderror
                                        </td>
                                        <td class="td-price">
                                            <input type="number" min="0.01" step="any" placeholder="Price" name="concepts[0][price]" id="unitprice" class="form-control" value="{{ old('concepts')[0]['price'] ?: null }}" required />
                                        </td>
                                        <td class="td-disc" style="text-align: center;">
                                            <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="concepts[0][discount]" id="discount" class="form-control" value="{{ old('concepts')[0]['discount'] ?: 0 }}" />
                                        </td>
                                        <td class="td-tax">
                                            {!! Form::select('concepts[0][tax]', $tax_options, old('concepts[0][tax]', null), ['class'=>"form-control small-single-search-selection", 'id'=>"concepts[0][tax]"], $tax_attributes) !!}
                                        </td>
                                        <td class="td-tax_amount">
                                            <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="concepts[0][tax_amount]" id="concepts[0][tax_amount]" class="form-control" readonly value="{{ old('concepts')[0]['tax_amount'] ?: null }}" />
                                        </td>
                                        <td class="td-quantity">
                                            <input type="number" placeholder="1" name="concepts[0][quantity]" id="quantity" class="form-control" min="1" value="{{ old('concepts')[0]['quantity'] ?: 1 }}">
                                        </td>
                                        <td class="td-total">
                                            <span id="concepts[0][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    @foreach (old('concepts', []) as $key => $concept)
                                        @if ($key == 0)
                                            @continue
                                        @endif
                                        @include('app.concept-row', ['row_number' => $key, 'concept_with_products' => true])
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-concept-with-product-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table custom-total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr class="total_exchanged_rate" style="display:none">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label"></span></th>
                                        <td> <span class="total-ex-currency-label-symbol"></span><span
                                                id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner" style="display:none">1 <span
                                                    class="exchange_currency_label"></span> =
                                                {{ Auth::user()->company->currency->code }}
                                                ({{ Auth::user()->company->currency->symbol }}) <span
                                                    class="exchange_currency_rate"></span> </label>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                  
                <div class="form-group popup-btns">
                    <a href="{{ route('receivable.index') }}"><button type="button" name="cancel"
                            class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    const mapInventoryWithMembership = {!! json_encode($mapInventoryWithMembership) !!};
    const mapMembershipWithDiscount = {!! json_encode($mapMembershipWithDiscount) !!};
</script>
 <script type="text/javascript" src="{{ asset('js/receivable.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
<script type="text/javascript">
    addDropZone(null, 'both', 10);
</script>
<script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush