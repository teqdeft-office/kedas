@extends('layouts.dashboard')
@section('title', trans('labels.account_receivable'))
@section('content')
<div class="dashboard-right cust-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.account_receivable') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.account_receivable') }},</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style="display: none;"></div>
            <a href="{{ route('receivable.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new') }} {{ __('labels.account_receivable') }}</a>
        </div>
 
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="cal-filter-cnt cal-filter-cust-btns" id="dates-filters" style="display:none">
                        <span>{{ __('labels.from') }}: <input name="date-filter-min" id="date-filter-min" type="text"
                                autocomplete="off"></span>
                        <span>{{ __('labels.to') }}: <input name="date-filter-max" id="date-filter-max" type="text"
                                autocomplete="off"></span>
                    </div>
                    <div class="list-table-custom  products-table table-responsive">
                        <table class="table table-striped" id="account-recivable-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.sr_number') }}</th>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.creation_date') }}</th>
                                    <th>{{ __('labels.expiration_date') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th>{{ __('labels.paid') }}</th>
                                    <th>{{ __('labels.pending') }}</th>
                                    <th>{{ __('labels.status') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($accountAdjustments as $accountAdjustment)
                                <tr>
                                
                                    <td>{{ $accountAdjustment->id }}</td>
                                    <td>{{$accountAdjustment->contact }}</td>
                                    <td>{{ dateFormat($accountAdjustment->start_date) }}</td>
                                    <td>{{ dateFormat($accountAdjustment->expiration_date) }}</td>
                                    <td>{{ moneyFormat($accountAdjustment->total) }}</td>
                                    <td>{{ moneyFormat($accountAdjustment->item_paid) }}</td>
                                    <td>{{ moneyFormat($accountAdjustment->item_pending) }}</td>
                                    <td>
                                       {{$accountAdjustment->status }}
                                    </td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a href="{{route('receivable.show', $accountAdjustment->id)}}">{{ __('labels.view') }}</a></li>
                                                    <li><a href="#">{{ __('labels.edit') }}</a></li>
                                                    <li><a class="delete_resource" href="{{route('receivable.delete', $accountAdjustment->id)}}">{{ __('labels.delete') }}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                  
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="9" rowspan="2" align="center">
                                        <div class="message">
                                            <p>{{ __('labels.you_have_not_yet_created_an').' '.__('labels.account_receivable') }}!</p>
                                        </div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('receivable.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.account_receivable') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <!-- <th>Number</th>
                                        <th>Contact</th>
                                        <th>Creation date</th>
                                        <th>Expiration date</th>
                                        <th>Total</th>
                                        <th>Paid</th>
                                        <th>Pending</th>
                                        <th>Status</th>
                                        <th>Actions</th> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection