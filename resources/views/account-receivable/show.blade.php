@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.account_receivable'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('receivable.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.account_receivable').' '.$account_recivable->id  }}</h2>
        <div class="action-btns-view">
            <a target="_blank" href="#" class="btn-custom"><i
                    class="fa fa-print"></i> {{ __('labels.print') }}</a>
           <a target="_blank" href="#" class="btn-custom"><i class="fa fa-download" aria-hidden="true"></i> {{ __('labels.download') }}</a>
           <a target="_blank" href="#" class="btn-custom"><i class="fa fa-envelope" aria-hidden="true"></i> {{ __('labels.send_by_mail') }}</a>
           <a target="_blank" href="#" class="btn-custom"> {{ __('labels.add_payment') }}</a>
            <a target="_blank" href="#" class="btn-custom"> {{ __('labels.make_recurring') }}</a>
             <a target="_blank" href="#" class="btn-custom"> {{ __('labels.clone') }}</a>
              <a target="_blank" href="#" class="btn-custom"> {{ __('labels.edit') }}</a>
        </div>
        <div class="default-form view-form">
            <div class="generate-invoice">

                <div class="invoice-detail-count">
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.total_value') }}</span>
                 		<span class="value total-value">{{ moneyFormat($item_total,null,2,true) }}</span>
                    </div>
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.paid') }}</span>
                        <span class="value paid">{{ moneyFormat($item_paid,null,2,true) }}</span>
                    </div>
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.pending') }}</span>
                        <span class="value pending">{{ moneyFormat($item_pending,null,2,true) }}</span>
                    </div>
                </div>
                <div class="row">
                     <div class="col-sm-3">
                     	
                            <div class="form-group file-input file-input-medium">
                                @if(!Auth::user()->company->logo)
                                <label for="image" id="image-preview-label">
                                    <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                    {{ __('labels.logo') }}
                                </label>
                                @endif
                                @if(Auth::user()->company->logo)
                                <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                    alt="{{ Auth::user()->company->name }}" />
                                @else
                                <img id="image-preview" src="" />
                                @endif
                                <input type="file" id="company-logo" name="logo" class="file-upload-preview"
                                    accept="image/*" data-img_id="image-preview"
                                    data-msg-accept="{{ __('messages.invalid_image_file_error') }}"
                                    data-label_id="image-preview-label" />
                                <a class="upload-button" data-preview_section="company-logo"><i class="fa fa-pencil"
                                        aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('logo')
                                <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                       
                     </div>
                    <div class="col-sm-6 text-center company-data">
                             <div class="add-cell-center">
                                    <h2>{{ Auth::user()->company->name }}</h2>
                                    @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">
                                            {{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                    @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">
                                            {{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3>
                                        <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->mobile)
                                        <h3>{{ __('labels.mobile') }}:</h3>
                                        <p>{{ Auth::user()->company->mobile }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                    <span>
                                        <h3>{{ __('labels.email') }}:</h3>
                                        <p><a href="mailto:{{Auth::user()->company->support_email}}">
                                                {{ Auth::user()->company->support_email }} </a></p>
                                    </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                    <span>
                                        <h3>{{ __('labels.tax_id') }}:</h3>
                                        <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                    </span>
                                    @endif
                                </div>
                    </div> 
                    <div class="col-sm-3">
                        <div class="consumption-box">
                            <div class="consumption-bottom">
                                <span>{{ __('labels.no') }}.</span>
                                <div class="consumption-number">{{ $account_recivable->id }}</div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="view-form-custom supplier-invoice-view">
                    <table class="table">
                        <tr>
                            <td><b>{{ __('labels.client') }}</b></td>
                          	<td>{{$user->name}}</td>
                            <td><b>{{ __('labels.creation_date') }}</b></td>
                            <td>{{ $account_recivable->start_date }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.tax_id') }}</b></td>
                            <td>{{$user->tax_id}}</td>
                            <td><b>{{ __('labels.expiration_date') }}</td>
                             <td>{{ $account_recivable->expiration_date }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.phone') }}</b></td>
                             <td>{{$user->phone}}</td>
                              <td><label><b>{{ __('labels.term') }}</b></label></td>
                            <td>
                                <span>{{ $account_recivable->term }}</span>
                            </td>
                        </tr>
                   
                        <tr>
                             <td><label><b>{{ __('labels.term') }}</b></label></td>
                            <td class="descrip-notes">
                                <span>dsfsf</span>
                            </td>
                        </tr>
                    </table>
                </div>
              	 @if(!empty($documents))
                    <div class="form-group col-sm-12 col-lg-12 documents-container">
						<label for="name"><b>{{ __('labels.documents') }}: </b></label>
                        <div class="wrap-document-elements">
                          @foreach($documents as $document)
                          <?php
    						  	$ext=explode('.',$document->name);
    						  	if($ext!='pdf'){
    						  	 ?>
    						<div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
    						
    						  	
    								<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
    								<a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl" data-size="1024x1024">
        								<img src="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="thumbnail" alt="Image description" />
        							</a>
        							</figure>
        						</div>
    						<?php } else { ?>
    						<div class="pdf-documents">
    							<a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
    						</div>
    						<?php } ?>
        					@endforeach
    						
                        </div>
						
						<!-- Root element of PhotoSwipe. Must have class pswp. -->
						<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
							<!-- Background of PhotoSwipe. 
								It's a separate element, as animating opacity is faster than rgba(). -->
							<div class="pswp__bg"></div>
							<!-- Slides wrapper with overflow:hidden. -->
							<div class="pswp__scroll-wrap">
								<!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
								<!-- don't modify these 3 pswp__item elements, data is added later on. -->
								<div class="pswp__container">
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
								</div>
								<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
								<div class="pswp__ui pswp__ui--hidden">
									<div class="pswp__top-bar">
										<!--  Controls are self-explanatory. Order can be changed. -->
										<div class="pswp__counter"></div>
										<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
										<button class="pswp__button pswp__button--share" title="Share"></button>
										<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
										<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
										<!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
										<!-- element will get class pswp__preloader--active when preloader is running -->
										<div class="pswp__preloader">
											<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
													<div class="pswp__preloader__donut"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
										<div class="pswp__share-tooltip"></div>
									</div>
									<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
									</button>
									<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
									</button>
									<div class="pswp__caption">
										<div class="pswp__caption__center"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                         	<tbody>

                                @foreach($items as $item)
                                <tr>
                                   <td>{{$item->name }}</td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount)}}%</td>
                                    <?php if($item->percentage !=''){
                                    	$perc=$item->percentage .'%';
                                    } 
                                    else{
                                    	$perc="N/A";
	                                    }?>
                                    <td>{{$perc}}</td>
                                    <td>{{moneyFormat($item->tax_amount,null,2,true)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->total,null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                     <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($base_total,null,2,true)}} </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{moneyFormat($total_discount,null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($subtotal,null,2,true)}}</td>
                                    </tr>
                                    <tr class="tax_row">
                                        <th>{{ __('labels.tax') }}</th>
                                        <td id="invoice_tax">{{moneyFormat($total_tax,null,2,true)}}</td>
                                    </tr>
                                  <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }}</th>
                                        <td> {{moneyFormat($item_total,null,2,true)}}</td>
                                      
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                          
                                        </th>
                                    </tr>
                               </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
               
            </div>

            {{-- Show accounting and payment section --}}
            <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#invoice-received-payment">{{ __('labels.received_payments') }}</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoice-accounting">{{ __('labels.accounting') }}</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoice-credit-note-payment">{{ __('labels.credit_notes') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="invoice-received-payment" class="tab-pane generate-invoice fade in active show">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">                           
                                    <table class="table table-striped">
                                        <thead>
                                            <th>{{ __('labels.date') }}</th>
                                            <th>{{ __('labels.voucher_number') }} #</th>
                                            <th>{{ __('labels.payment_method') }}</th>
                                            <th>{{ __('labels.amount') }}</th>
                                            <th>{{ __('labels.annotation') }}</th>
                                        </thead>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoice-accounting" class="invoice-accounting-tab-pane tab-pane fade generate-invoice">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <div id="accounting-head" class="accounting-head">
                                        <span>{{ __('labels.accounting_sheet') }}</span>
                                        <span>{{ __('labels.date') }} </span>
                                    </div>
                                 
                                    <div class="table-wrap">
                                        <table id="accounting-table" class="table table-striped">
                                            <thead>
                                                <th>{{ __('labels.code') }}</th>
                                                <th>{{ __('labels.account') }}</th>
                                                <th>{{ __('labels.debit') }}</th>
                                                <th>{{ __('labels.credit') }}</th>
                                            </thead>
                                         
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoice-credit-note-payment" class="tab-pane generate-invoice fade">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">                      
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>{{ __('labels.sr_number') }}</th>
                                                <th>{{ __('labels.date') }}</th>
                                                <th>{{ __('labels.amount') }}</th>
                                                <th>{{ __('labels.remaining') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
	initPhotoSwipeFromDOM('.documents');
</script>
@endpush