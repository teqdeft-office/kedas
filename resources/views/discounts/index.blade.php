@extends('layouts.dashboard')

@section('title', 'Discount')

@section('content')
<div class="dashboard-right cust-dashboard-md-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>Discounts</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">If you need help creating a Discount.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style=""><div class="dt-buttons"><button class="dt-button buttons-excel buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in excel</span></button> <button class="dt-button buttons-pdf buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in pdf</span></button> </div></div>
            <a href="{{ route('discounts.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> New Discount</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="discounts-list-table">
                            <thead>
                                <tr>
                                    <th>Sr Number</th>
                                    <th>Employee</th>
                                    <th>Type</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Frequency</th>
                                    <th>Frequency Type</th>
                                    <th>Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                               @forelse ($discounts as $key => $discount)
                                <tr>
                                    <!-- <td>{{ $discount->internal_number }}</td> -->
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $discount->employee->name }}</td>
                                    <td>{{ $discount->type }}</td>
                                    <td>{{ dateFormat($discount->start_date) }}</td>
                                    <td>{{ dateFormat($discount->end_date) }}</td>
                                    <td>{{ showIfAvailable($discount->frequency) }}</td>
                                    <td>{{ $discount->frequency_type }}</td>
                                    <td>{{ $discount->amount }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('discounts.edit', $discount)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-discount-form-' . $discount->id }}" href="{{route('discounts.destroy', $discount)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['discounts.destroy', $discount], 'id' => "destroy-discount-form-{$discount->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="7" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.discount') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('discounts.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.discount') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
