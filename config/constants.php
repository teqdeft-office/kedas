<?php

return [
    'accounts' => [
        'ordinary_activities_income' => 'Ordinary Activities Income',
        'sales' => 'Sales',
        'returns_on_sales' => 'Returns on sales',
        'other_income' => 'Other Income',
        'financial_income' => 'Financial Income'
    ],
    'type_of_item' => [
    	'simple' => 'Create services or products.', 
    	'kit' => 'Groups a set of products or services into a single item.', 
    	'item_with_variants' => 'Quickly create products with variables such as size and color.'
    ],
    'languages' => [
        'en' => 'English', 
        'es' => 'Spanish'
    ],
    'terms' => [
        '0' => 'In cash',
        '8' => '8 days',
        '15' => '15 days',
        '30' => '30 days',
        '60' =>  '60 days',
        'manual' =>  'Manual'
    ],
    'payment_accounts' => [
        'bank' => 'Bank',
        'business_credit_card' => 'Business Credit Card',
        'petty_cash' => 'Petty Cash'
    ],
    'payment_methods' => [
        'cash' => 'Cash',
        'deposit' => 'Deposit',
        'transfer' => 'Transfer',
        'check' => 'Check',
        'credit_card' => 'Credit Card',
        'debit_card' => 'Debit Card'
    ],
    'user_roles' => [
        'super_admin' => 'Super admin',
        'admin' => 'Admin',
        'company_user' => 'Company user',
        'app_user' => 'App user'
    ],
    'primary' => [
        'zero' => '0',
        'one' => '1'
    ],
    'type_of_inventories' => [
        'inventory' => 'inventory',
        'services' => 'services',
        'non-inventory' => 'non-inventory'
    ],
    'benefit_type_option' => [
       'food' => 'Food',
       'commission' => 'Commission',
       'bonus' => 'Bonus', 
       'transport' => 'Transport',
       'vacation' => 'Vacation'
    ],
    'payroll_frequency_type_option' => [
       'monthly' => 'Monthly', 
       'biweekly' => 'Biweekly', 
       'weekly' => 'Weekly',
       'daily' => 'Daily'
    ],
    'payroll_incomes_option' => [
      'accounts_receivable' => 'Accounts Receivable'
    ],
    'payroll_expenses_option' => [
      'payroll_expenses' => 'Payroll Expenses'
    ],
    'with_holding_type_option' => [
        'AFP' => 'AFP',
        'SDSS' => 'SDSS',
        'ISR' => 'ISR',
        'SS' => 'SS', 
        'HI' => 'HI',
        'Others' => 'Others'
    ],
    'discount_type_option' => [
        'cash_advance' => 'Cash Advance', 
        'damages' => 'Damages',
        'uniform' => 'Uniform',
        'cashier_missing' => 'Cashier Missing',
        'misc' => 'Misc',
        'Others' => 'others'
    ],
    'extra_hour_type_option' => [
        'day ' => 'Day ',
        'night ' => 'Night ',
        'holiday' => 'Holiday',
        'day_off' => 'Day off',
        'over_the_limit' => 'Over The Limit'
    ],
    'employee_period' => [
       'monthly' => 'Monthly', 
       'weekly' => 'Weekly',
       'daily' => 'Daily',
       'biweekly' => 'Biweekly', 
       'yearly' => 'Yearly', 
       'custom' => 'Custom', 
    ],
    'contract_type' => [
       'permanent' => 'Permanent', 
       'temporary' => 'Temporary',
    ],
    'type_of_account' => [
       'checking' => 'Checking', 
       'saving' => 'saving',
    ],
    'salary_type' => [
       'hourly' => 'Hourly', 
       'fixed' => 'Fixed',
    ],
    'cal_type' => [
        'straight_line' => 'Straight line',
        'sum_of_digits' => 'Sum of digits',
        'units_produced' => 'Units produced'
    ],
    'legal_category' => [
        'cat_I' => 'Category I',
        'cat_II' => 'Category II',
        'cat_III' => 'Category III',
    ],
    'location' => [
        'main_office' => 'Main office'
    ],
    'leave_type_option' => [
       'vacation' => 'Vacation', 
       'sick_leave' => 'Sick Leave',
       'event_leave_or_other' => 'Event Leave or other',
    ],
    'emp_status' => [
       'active' => 'Active', 
       'inactive' => 'Inactive',
       'vacation' => 'Vacation',
    ],
];