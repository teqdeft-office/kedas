<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactPoint extends Model
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contact_points';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {}
    
    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * Get the invoice associated with the contact point.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

}