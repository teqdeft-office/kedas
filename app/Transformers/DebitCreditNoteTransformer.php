<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\StoreTransformer;

class DebitCreditNoteTransformer
{
	public function transform($notes)
	{
		// Create a top level instance somewhere
		$fractal = new Manager();

		$resource = new Collection($notes, function($notes) {
			return [
				'id' => $notes->id,
				'store' => (new StoreTransformer())->singleTransform($notes->company),
				'calculation' => $notes->calculation,
				'contact_id' => $notes->contact_id,
				'type' => $notes->type,
				'notes' => $notes->notes,
				'calculation' => $notes->calculation,
				'start_date' => $notes->start_date,
				'created_at' => $notes->created_at,
				'updated_at' => $notes->updated_at,
				'expiration_date' => $notes->expiration_date,
				'frequency' => $notes->frequency_type,
				'frequency_type' => $notes->frequency_type,
				'currency' => $notes->currency,
				'exchange_rate_total' => $notes->exchange_rate_total,
				'items' => @$notes->items,
			];
		});
		$resource = $fractal->createData($resource)->toArray();
		return $resource["data"];
	}

	public function singleTransform($notes)
	{	
		return [
			'id' => $notes->id,
			'contact_id' => $notes->contact_id,
			'type' => $notes->type,
			'notes' => $notes->notes,
			'calculation' => $notes->calculation,
			'start_date' => $notes->start_date,
			'created_at' => $notes->created_at,
			'updated_at' => $notes->updated_at,
			'expiration_date' => $notes->expiration_date,
			'frequency' => $notes->frequency_type,
			'frequency_type' => $notes->frequency_type,
			'currency' => $notes->currency,
			'exchange_rate_total' => $notes->exchange_rate_total,
			'items' => @$notes->items,

		];
	}
}