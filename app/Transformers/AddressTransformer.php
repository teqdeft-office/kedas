<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class AddressTransformer
{
	public function singleTransform($address)
	{
		return [
			'line_1' => $address->line_1,
			'line_2' => $address->line_2,
			'city' => $address->city,
			'state' => $address->state,
			'zip' => $address->zip,
			'country_id' => $address->country_id,
			'country_name' => $address->countryInfo->name
		];
	}
}