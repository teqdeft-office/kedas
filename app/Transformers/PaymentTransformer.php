<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\StoreTransformer;

class PaymentTransformer
{
	public function transform($transaction)
	{
		// Create a top level instance somewhere
		$fractal = new Manager();

		$resource = new Collection($transaction, function($transaction) {
			return [
				'id' => $transaction->id,
				'contact_id' => $transaction->contact_id,
				'store' => (new StoreTransformer())->singleTransform($transaction->company),
				'type' => $transaction->type,
				'method' => $transaction->method,
				'receipt_number' => $transaction->receipt_number,
				'voucher_number' => $transaction->voucher_number,
				'amount' => $transaction->amount,
				'annotation' => $transaction->annotation,
				'observation' => $transaction->observation,
				'account' => $transaction->account,
				'created_at' => $transaction->created_at,
				'start_date' => $transaction->start_date,
				'end_date' => $transaction->end_date,
				'frequency_type' => $transaction->frequency_type,
				'currency' => $transaction->currency,
				'bankAccount' => $transaction->bankAccount,
			];
		});
		$resource = $fractal->createData($resource)->toArray();
		return $resource["data"];
	}

	public function singleTransform($transaction)
	{	
		return [
			'id' => $transaction->id,
			'contact_id' => $transaction->contact_id,
			'store' => (new StoreTransformer())->singleTransform($transaction->company),
			'type' => $transaction->type,
			'method' => $transaction->method,
			'receipt_number' => $transaction->receipt_number,
			'amount' => $transaction->amount,
			'annotation' => $transaction->annotation,
			'observation' => $transaction->observation,
			'account' => $transaction->account,
			'created_at' => $transaction->created_at,
			'start_date' => $transaction->start_date,
			'end_date' => $transaction->end_date,
			'frequency_type' => $transaction->frequency_type,
			'currency' => $transaction->currency,
			'bankAccount' => $transaction->bankAccount,
			
		];
	}
}