<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use App\Interfaces\CommonConstants;

class UserAccountingHead extends Model implements CommonConstants
{
    use HasTranslations;
    use NodeTrait;

    public $translatable = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['resource_associated', 'custom_id', 'custom_name'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     * Get the user associate with the user/company accounting head.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the company associate with the user/company accounting head.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the accountingHead associate with the user/company accounting head.
     */
    public function accountingHead()
    {
        return $this->belongsTo('App\AccountingHead');
    }

    public function getResourceAssociatedAttribute() {
        return $this->accountingHead || $this->children->isNotEmpty();
    }

    public function getCustomIdAttribute() {
        return self::CONCEPT .'-'. $this->attributes['id'];
    }

    public function getCustomNameAttribute() {
        return $this->attributes['custom_code'] .' - '. $this->name;
    }

    /**
     * Get the accounting entry of the accounting head.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }
}
