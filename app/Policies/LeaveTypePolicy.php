<?php

namespace App\Policies;

use App\LeaveType;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class LeaveTypePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $LeaveType
     * @return mixed
     */
    public function view(User $user, LeaveType $leaveType)
    {
        return $user->company->id == $leaveType->company_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function update(User $user, LeaveType $leaveType)
    {
        return $user->id == $leaveType->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function delete(User $user, LeaveType $leaveType)
    {
        return $user->id == $leaveType->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function restore(User $user, LeaveType $leaveType)
    {
        return $user->id == $leaveType->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\LeaveType  $leaveType
     * @return mixed
     */
    public function forceDelete(User $user, LeaveType $leaveType)
    {
        return $user->id == $leaveType->user_id;
    }
}
