<?php

namespace App\Policies;

use App\AccountAdjustment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class AccountAdjustmentPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return mixed
     */
    public function view(User $user, AccountAdjustment $accountAdjustment)
    {
        return $user->company->id == $accountAdjustment->company_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return mixed
     */
    public function update(User $user, AccountAdjustment $accountAdjustment)
    {
        return $user->id == $accountAdjustment->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return mixed
     */
    public function delete(User $user, AccountAdjustment $accountAdjustment)
    {
        return $user->id == $accountAdjustment->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return mixed
     */
    public function restore(User $user, AccountAdjustment $accountAdjustment)
    {
        return $user->id == $accountAdjustment->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return mixed
     */
    public function forceDelete(User $user, AccountAdjustment $accountAdjustment)
    {
        return $user->id == $accountAdjustment->user_id;
    }
}
