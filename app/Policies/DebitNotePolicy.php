<?php

namespace App\Policies;

use App\DebitNote;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DebitNotePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\DebitNote  $debitNote
     * @return mixed
     */
    public function view(User $user, DebitNote $debitNote)
    {
        return $user->company->id == $debitNote->company_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\DebitNote  $debitNote
     * @return mixed
     */
    public function update(User $user, DebitNote $debitNote)
    {
        return $user->id == $debitNote->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\DebitNote  $debitNote
     * @return mixed
     */
    public function delete(User $user, DebitNote $debitNote)
    {
        return $user->id == $debitNote->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\DebitNote  $debitNote
     * @return mixed
     */
    public function restore(User $user, DebitNote $debitNote)
    {
        return $user->id == $debitNote->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\DebitNote  $debitNote
     * @return mixed
     */
    public function forceDelete(User $user, DebitNote $debitNote)
    {
        return $user->id == $debitNote->user_id;
    }
}
