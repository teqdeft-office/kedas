<?php 

namespace App\Providers; 

use App\Repository\EloquentRepositoryInterface; 
use App\Repository\NcfRepositoryInterface; 
use App\Repository\Eloquent\NcfRepository; 
use App\Repository\Eloquent\BaseRepository; 
use Illuminate\Support\ServiceProvider; 

/** 
* Class RepositoryServiceProvider 
* @package App\Providers 
*/ 
class RepositoryServiceProvider extends ServiceProvider 
{ 
    /** 
    * Register services. 
    * 
    * @return void  
    */ 
    public function register() 
    { 
       $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
       $this->app->bind(NcfRepositoryInterface::class, NcfRepository::class);
    }

    /**
    * Bootstrap services.
    *
    * @return void
    */
    public function boot()
    {
        //
    }
}