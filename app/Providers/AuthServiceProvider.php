<?php

namespace App\Providers;

use App\Policies\ClientPolicy;
use App\Policies\SupplierPolicy;
use App\Policies\TaxPolicy;
use App\Policies\ItemCategoryPolicy;
use App\Policies\InventoryPolicy;
use App\Policies\TransactionPolicy;
use App\Policies\CreditNotePolicy;
use App\Policies\EstimatePolicy;
use App\Policies\DebitNotePolicy;
use App\Policies\PurchaseOrderPolicy;
use App\Policies\UserAccountingHeadPolicy;
use App\Policies\RolePolicy;
use App\Policies\CompanyUserPolicy;
use App\Policies\InventoryAdjustmentPolicy;
use App\Policies\RemissionPolicy;
use App\Policies\AccountAdjustmentPolicy;
use App\Policies\MembershipPolicy;

use App\Client;
use App\Supplier;
use App\Tax;
use App\ItemCategory;
use App\Inventory;
use App\Transaction;
use App\CreditNote;
use App\Estimate;
use App\DebitNote;
use App\PurchaseOrder;
use App\UserAccountingHead;
use App\Role;
use App\User;
use App\InventoryAdjustment;
use App\Remission;
use App\Employee;
use App\Membership;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Client::class => ClientPolicy::class,
        Supplier::class => SupplierPolicy::class,
        Tax::class => TaxPolicy::class,
        ItemCategory::class => ItemCategoryPolicy::class,
        Inventory::class => InventoryPolicy::class,
        Invoice::class => InvoicePolicy::class,
        Transaction::class => TransactionPolicy::class,
        CreditNote::class => CreditNotePolicy::class,
        Estimate::class => EstimatePolicy::class,
        DebitNote::class => DebitNotePolicy::class,
        PurchaseOrder::class => PurchaseOrderPolicy::class,
        UserAccountingHead::class => UserAccountingHeadPolicy::class,
        Role::class => RolePolicy::class,
        User::class => CompanyUserPolicy::class,
        InventoryAdjustment::class => InventoryAdjustmentPolicy::class,
        Remission::class => RemissionPolicy::class,
        Employee::class => EmployeePolicy::class,
        AccountAdjustment::class => AccountAdjustmentPolicy::class,
        Membership::class => MembershipPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(now()->addDays(15));

        Passport::refreshTokensExpireIn(now()->addDays(30));

        // Implicitly grant "Super Admin" or admin(or owner) of the company role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::after(function ($user, $ability) {
            return ($user->isSuperAdmin() || $user->isCompanyOwner()) ? true : null;
        });
    }
}
