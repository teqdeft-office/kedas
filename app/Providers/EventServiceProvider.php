<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Illuminate\Auth\Events\Verified' => [
            'App\Listeners\WelcomeMail'
        ],
        'App\Events\ProfileCompleted' => [
            'App\Listeners\CreateCompanyRoles',
            'App\Listeners\CreateUserAccountingHeads',
        ],
        'App\Events\UserLoggedIn' => [
            'App\Listeners\SaveUserCoords'
        ],
        'App\Events\SaleInvoiceCreatedOrUpdated' => [
            'App\Listeners\CreateAccountingEntries',
        ],
        'App\Events\PaymentCreatedOrUpdated' => [
            'App\Listeners\CreateAccountingEntries',
        ],
        'App\Events\InventoryAdjustmentCreatedOrUpdated' => [
            'App\Listeners\CreateAccountingEntries',
        ],
        'App\Events\SupplierInvoiceCreatedOrUpdated' => [
            'App\Listeners\CreateAccountingEntries',
        ],
        'App\Events\InventoryCreatedOrUpdated' => [
            'App\Listeners\CreateAccountingEntries',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
