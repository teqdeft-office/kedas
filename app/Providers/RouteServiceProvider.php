<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use App\UserAccountingHead;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The path to the "dashboard" route for your application.
     *
     * @var string
     */
    public const DASHBOARD = '/dashboard';

    /**
     * The path to the "login" route for your application.
     *
     * @var string
     */
    public const LOGIN = '/login';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::pattern('adjustment', '[0-9]+');
        Route::pattern('chart', '[0-9]+');
        Route::pattern('country', '[0-9]+');
        Route::pattern('currency', '[0-9]+');
        Route::pattern('identification_type', '[0-9]+');
        Route::pattern('sector', '[0-9]+');
        Route::pattern('invoice', '[0-9]+');
        Route::pattern('transaction', '[0-9]+');
        Route::pattern('inventory', '[0-9]+');
        Route::pattern('inventory_adjustment', '[0-9]+');
        Route::pattern('item_category', '[0-9]+');
        Route::pattern('ncf', '[0-9]+');
        Route::pattern('id', '[0-9]+');
        Route::pattern('store', '[0-9]+');
        Route::pattern('tax', '[0-9]+');
        Route::pattern('role', '[0-9]+');
        Route::pattern('company_user', '[0-9]+');
        Route::pattern('client', '[0-9]+');
        Route::pattern('credit_note', '[0-9]+');
        Route::pattern('debit_note', '[0-9]+');
        Route::pattern('user', '[0-9]+');
        Route::pattern('department', '[0-9]+');
        Route::pattern('designation', '[0-9]+');
        Route::pattern('employee', '[0-9]+');
        Route::pattern('estimate', '[0-9]+');
        Route::pattern('insurance_brand', '[0-9]+');
        Route::pattern('template_no', '[0-9]+');
        Route::pattern('leave_application', '[0-9]+');
        Route::pattern('leave_type', '[0-9]+');
        Route::pattern('client_id', '[0-9]+');
        Route::pattern('purchase_order', '[0-9]+');
        Route::pattern('remission', '[0-9]+');
        Route::pattern('supplier', '[0-9]+');        

        parent::boot();

        Route::model('chart', UserAccountingHead::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
