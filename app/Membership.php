<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Membership extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'memberships';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($membership) {

        });
    }

    /**
     * Get the clients associated with the tax.
     */
    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    /**
     * Get the company associated with the tax.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the membership's inventories.
     */
    public function inventories()
    {
        return $this->belongsToMany('App\Inventory');
    }
}
