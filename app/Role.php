<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Spatie\Permission\Models\Role as RoleModel;
use App\Permission;
use App\History;
class Role extends RoleModel
{
	use HasTranslations;
	public $timestamps = true;
	public $translatable = ['custom_name'];
	protected $guard_name = 'web';

	protected $fillable = [
		'id',
		'name',
		'custom_name',
		'company_id',
		'guard_name'
	];

	/**
	 * The "booted" method of the model.
	 *
	 * @return void
	 */
	protected static function booted()
    {
        static::created(function ($role) {
            $createdata = History::createdata();
            $role->histories()->create($createdata);
        });
        static::updated(function($role){
            $updatedata = History::updatedata();
            $role->histories()->create($updatedata);
        });
        static::deleted(function($role) {
            $deletedata = History::deletedata();
            $role->histories()->create($deletedata);
            $role->permissions()->each(function($permission) {
                $permission->delete();
            });
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

	public function getCustomPermissionNames()
	{
		$permissions = $this->permissions->map(function($permission) {
			$permission->replicate()->toArray();
            $permission['custom_name'] = (array) json_decode($permission['custom_name']);
            return new Permission($permission->toArray());
		});
		return $permissions->pluck('custom_name');
	}
}
