<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WithHolding extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'with_holdings';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        /* static::deleted(function($tax) {
            $tax->inventories()->each(function($inventory) {
                $inventory->tax()->dissociate();
                $inventory->save();
            });
        }); */
    }

    /**
     * Get the company associated with the tax.
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function incomeAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'income_account');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function typeAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'type_account');
    }

}
