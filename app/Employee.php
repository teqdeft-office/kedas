<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use App\History;
class Employee extends Model
{
	use SoftDeletes;
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['all_benefits', 'extra_hours_payment', 'all_with_holdings', 'all_discounts', 'net_payment'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($employee) {
            if ($employee->address) {
                $employee->address()->delete();
            }
        });

        static::deleted(function($employee) {
            if ($employee->benefits) {
                $employee->benefits()->delete();
            }
        });

        static::deleted(function($employee) {
            if ($employee->withHoldings) {
                $employee->withHoldings()->delete();
            }
        });

        static::deleted(function($employee) {
            if ($employee->discounts) {
                $employee->discounts()->delete();
            }
        });

        static::deleted(function($employee) {
            if ($employee->extraHours) {
                $employee->extraHours()->delete();
            }
        });

        static::deleted(function($employee) {
            if ($employee->leaves) {
                $employee->leaves()->delete();
            }
        });

        static::created(function ($employee) {
            $createdata = History::createdata();
            $employee->histories()->create($createdata);
        });
        static::updated(function($employee){
            $updatedata = History::updatedata();
            $employee->histories()->create($updatedata);
        });
        static::deleted(function($employee) {
            $deletedata = History::deletedata();
            $employee->histories()->create($deletedata);
            if ($employee->address) {
                $employee->address()->delete();
            }
        });
    }

    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Set the employee's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    /**
     * Set the employee's email.
     *
     * @param  string  $value
     * @return void
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    /**
     * Get all of the employee's addresses.
     */
    public function address()
    {
        return $this->morphOne('App\Address', 'addressadder');
    }

    /**
     * Get the department information associated with the employee.
     */
    public function department() {
        return $this->belongsTo('App\Department');
    }

    /**
     * Get all of the leave application's employee.
     */
    public function leaveApplications()
    {
        return $this->hasMany('App\LeaveApplication');
    }

    /**
     * Get all of the owner who create this employee.
     */
    public function owner()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

     /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

     /**
     * Get the company's image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return $this->attributes['image'] ? Storage::url('profile-pics/'.$this->attributes['image']) : null;
    }

    /**
     * Get the transaction Benefit.
     */
    public function benefits()
    {
        return $this->hasMany('App\Benefit');
    }

    /**
     * Get the transaction withHolding.
     */
    public function withHoldings()
    {
        return $this->hasMany('App\withHolding');
    }

    /**
     * Get the transaction .
     */
    public function discounts()
    {
        return $this->hasMany('App\discount');
    }

     /**
     * Get the transaction extraHour.
     */
    public function extraHours()
    {
        return $this->hasMany('App\extraHour');
    }

    /**
     * Get the transaction leaves
     */
    public function leaves()
    {
        return $this->hasMany('App\leave');
    }

    public function getExtraHoursPaymentAttribute() {
        return $this->extraHours->sum('extra_hours_total');
    }

    public function getAllBenefitsAttribute() {
        $benefits = array();
        $benefits['commission'] = $this->benefits->where('type','commission')->sum('amount');
        $benefits['vacation'] = $this->benefits->where('type','vacation')->sum('amount');
        $benefits['vacation'] = $this->benefits->where('type','vacation')->sum('amount');
        $benefits['bonus'] = $this->benefits->where('type','bonus')->sum('amount');
        return $benefits;
    }

    public function getAllWithHoldingsAttribute() {
        $withHoldings = array();
        $withHoldings['ISR'] = $this->withHoldings->where('type','ISR')->sum('amount');
        $withHoldings['HI'] = $this->withHoldings->where('type','HI')->sum('amount');
        $withHoldings['Others'] = $this->withHoldings->where('type','Others')->sum('amount');
        $withHoldings['AFP'] = $this->withHoldings->where('type','AFP')->sum('amount');
        return $withHoldings;
    }

    public function getAllDiscountsAttribute() {
        $discounts = array();
        $discounts['uniform'] = $this->discounts->where('type','uniform')->sum('amount');
        $discounts['cash_advance'] = $this->discounts->where('type','cash_advance')->sum('amount');
        $discounts['misc'] = $this->discounts->where('type','misc')->sum('amount');
        $discounts['total'] = $this->discounts->sum('amount') + $this->withHoldings->sum('amount');
        return $discounts;
    }

    public function getNetPaymentAttribute() {
        return (($this->salary + $this->extraHours->sum('extra_hours_total') + $this->benefits->sum('amount')) - ($this->discounts->sum('amount') + $this->withHoldings->sum('amount')));
    }

    /**
     * Get user instance of the employee.
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'related_user_id');
    }

    /**
     * Get the user company information associated with the user.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

}
