<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;

class InventoryAdjustment extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventory_adjustments';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['total', 'debit_credit_amount'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($inventoryAdjustment) {
            $createdata = History::createdata();
            $inventoryAdjustment->histories()->create($createdata);
        });
        static::updated(function($inventoryAdjustment){
            $updatedata = History::updatedata();
            $inventoryAdjustment->histories()->create($updatedata);
        });
        static::deleted(function($inventoryAdjustment) {
            $deletedata = History::deletedata();
            $inventoryAdjustment->histories()->create($deletedata);
            $inventoryAdjustment->items()->delete();
            $inventoryAdjustment->accountingEntries()->delete();
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * List of items associated with an credit note.
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with(['inventory']);
    }

    public function getTotalAttribute() 
    {
        return $this->items->reduce(function ($sum, $item) {
            $needToAdjust = $item->unit_cost * $item->quantity;
            $sum = $item->inventory_adjustment_type == 'increase' ? $sum + $needToAdjust : $sum - $needToAdjust;
            return $sum;
        }, 0);
    }

    /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * Get the accounting entries of the invoice.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function getDebitCreditAmountAttribute() {
        return $debitCreditAmount = $this->accountingEntries->groupBy('nature')->map(function ($row) {
            return $row->sum('amount');
        });
    }
}
