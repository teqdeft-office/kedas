<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use App\History;

class Unit extends Model
{
    use SoftDeletes;
    use HasTranslations;
    public $timestamps = true;
    public $translatable = ['unit', 'shortcut'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'units';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    protected static function booted()
    {
        static::updated(function($unit){
            $updatedata = History::updatedata();
            $unit->histories()->create($updatedata);
        });
        static::deleted(function($unit) {
            $deletedata = History::deletedata();
            $unit->histories()->create($deletedata);
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
}
