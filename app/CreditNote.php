<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\History;

class CreditNote extends Model
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'credit_notes';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'exchange_rate_total'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($creditNote) {
            $creditNote->ncf_number = $creditNote->ncf ? ($creditNote->ncf->current_number ? getNextAlphaNumber($creditNote->ncf->current_number) : getNextAlphaNumber($creditNote->ncf->initial_number)) : null;
        });
        static::created(function ($creditNote) {
            $createdata = History::createdata();
            $creditNote->histories()->create($createdata);
        });
        static::updated(function($creditNote){
            $updatedata = History::updatedata();
            $creditNote->histories()->create($updatedata);
        });
        static::deleted(function($creditNote) {
            $deletedata = History::deletedata();
            $creditNote->histories()->create($deletedata);
            $creditNote->invoices()->detach();
            $creditNote->items()->delete();
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Set the credit note's ncf.
     *
     * @param  string  $value
     * @return void
     */
    public function setNcfNumberAttribute($value) {
        $this->attributes['ncf_number'] = $value ? strtoupper($value) : null;
    }
    
    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * List of items associated with an credit note.
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with(['tax', 'inventory']);
    }

    /**
     * The invoices that belong to the credit note.
     */
    public function invoices()
    {
        return $this->belongsToMany('App\Invoice')->withPivot('amount');
    }

    /**
     * Get the ncf with the credit note.
     */
    public function ncf()
    {
        return $this->belongsTo('App\Ncf');
    }

    /**
     * Get the company information associated with the credit note.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the user associated with the credit note.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCalculationAttribute() {

        $pluckedCalculation = $this->items->pluck('calculation');

        $invoiceSum = array();
        $taxInfo = array();
        $taxInfoArr = array();
        foreach ($pluckedCalculation as $plucked) {
            foreach ($plucked as $sub_key => $value) {
                if (!is_array($value)) {
                    if( ! array_key_exists($sub_key, $invoiceSum)) $invoiceSum[$sub_key] = 0;
                    $invoiceSum[$sub_key]+=$value;
                } else {
                    array_push($taxInfoArr, $value);
                }
            }
        }
        $taxInfoArr = collect($taxInfoArr)->filter()->groupBy('tax_id')->values()->toArray();

        if ($taxInfoArr) {
            foreach ($taxInfoArr as $taxInfoEle) {
                $temp = [];
                foreach ($taxInfoEle as $taxData) {
                    foreach ($taxData as $sub_key => $value) {
                        if( ! array_key_exists($sub_key, $temp)) $temp[$sub_key] = 0;
                        if ($sub_key === 'tax') {
                            $temp[$sub_key] += $value;
                        } else {
                            $temp[$sub_key] = $value;
                        }
                    }
                }
                array_push($taxInfo, $temp);
            }
        }

        $usedAmount = 0;
        foreach ($this->invoices as $invoice) {
            $usedAmount += $invoice->pivot->amount;
        }
        $remainingAmount = $invoiceSum['total'] - $usedAmount;

        $invoiceSum['remaining_amount'] = $remainingAmount;
        $invoiceSum['used_amount'] = $usedAmount;
        $invoiceSum = array_map('addZeros', $invoiceSum);

        $invoiceSum['taxInfo'] = $taxInfo;
        return $invoiceSum;
    }

    public function getExchangeRateTotalAttribute() {
        return $this->currency ? round(($this->getCalculationAttribute()['total'] / $this->attributes['exchange_currency_rate']), 6) : null;
    }

    /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * Get the employee with the user.
     */
    public function employee()
    {
        return $this->hasOne('App\Employee');
    }
}
