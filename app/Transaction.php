<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App;
use App\History;

class Transaction extends Model
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['detail_line', 'debit_credit_amount'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($transaction) {
            $user = Auth::user() ?: $transaction->user;
            $next_number = $user->company->transactions()->withTrashed() ? $user->company->transactions()->withTrashed()->where('type', $transaction->type)->count() + 1 : 1;
            switch ($transaction->type) {
                case 'in':
                    $transaction->receipt_number = $next_number;
                    break;
                case 'out':
                    $transaction->voucher_number = $next_number;
                    break;
                
                default:
                    # code...
                    break;
            }
        });
        static::created(function ($transaction) {
            $createdata = History::createdata();
            $transaction->histories()->create($createdata);
        });
        static::updated(function($transaction){
            $updatedata = History::updatedata();
            $transaction->histories()->create($updatedata);
        });
        static::deleted(function($transaction) {
            $deletedata = History::deletedata();
            $transaction->histories()->create($deletedata);
            $transaction->invoices()->detach();
            $transaction->conceptItems()->delete();
            $transaction->accountingEntries()->delete();
        });
    }

    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
    /**
     * The invoices that belong to the transaction.
     */
    public function invoices()
    {
        return $this->belongsToMany('App\Invoice')->withPivot('amount');
    }

    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * List of concepts items associated with a transaction.
     */
    public function conceptItems()
    {
        return $this->hasMany('App\ConceptItem')->with(['tax', 'concept']);
    }

    /**
     * Get the user information associated with the transaction.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Invoice details of a transaction.
     */
    public function getDetailLineAttribute() {
        return $this->invoices->isEmpty() ? $this->attributes['annotation'] : trans('labels.invoice_number').': '.$this->invoices->implode('internal_number', ', ');
        // return $this->invoices ? 'Invoice number: '.$this->invoices->implode('id', ', ') : null;
    }

    /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * Get the currency with the transaction.
     */
    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'exchange_currency_id');
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the bankAccount information associated with the estimate.
     */
    public function bankAccount()
    {
        return $this->belongsTo('App\UserAccountingHead', 'bank_account');
    }

    /**
     * Get the accounting entries of the invoice.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }

    public function getDebitCreditAmountAttribute() {
        return $debitCreditAmount = $this->accountingEntries->groupBy('nature')->map(function ($row) {
            return $row->sum('amount');
        });
    }
}
