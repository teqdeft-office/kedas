<?php

namespace App\Exports;

use Auth;
use Cache;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SuppliersExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
	public function __construct()
    {
        $this->identification_types = Cache::get('identification_types');
        $this->countries = Cache::get('countries');
        $this->sectors = Cache::get('sectors');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$user = Auth::user();
        return $user->company->suppliers;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Identification type',
            'Tax ID',
            'Email',
            'Address',
            'Country',
            'Department',
            'Zip',
            'Fax',
            'Phone',
            'Mobile',
            'Date'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'H' => 0,
            'I' => 0,
            'J' => '@',
            'K' => '@',
            'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    /**
    * @var Supplier $supplier
    */
    public function map($supplier): array
    {
        return [
            $supplier->name,
            $supplier->identification_type_id ? $this->identification_types->pluck('id', 'name')->search($supplier->identification_type_id) : null,
            $supplier->tax_id,
            $supplier->email,
            $supplier->address ? $supplier->address->line_1: '',
            $supplier->address ? $this->countries->pluck('id', 'name')->search($supplier->address->country_id): '',
            $supplier->sector_id ? $this->sectors->pluck('id', 'name')->search($supplier->sector_id) : null,
            $supplier->address ? $supplier->address->zip: '',
            $supplier->fax,
            $supplier->phone,
            $supplier->mobile,
            Date::dateTimeToExcel($supplier->created_at)
        ];
    }
}
