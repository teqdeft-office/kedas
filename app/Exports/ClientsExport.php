<?php

namespace App\Exports;

use Auth;
use Cache;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ClientsExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
	public function __construct()
    {
        $this->identification_types = Cache::get('identification_types');
        $this->countries = Cache::get('countries');
        $this->sectors = Cache::get('sectors');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$user = Auth::user();
        return $user->company->clients;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Identification type',
            'Tax ID',
            'Email',
            'Address',
            'Country',
            'Department',
            'Zip',
            'Fax',
            'Phone',
            'Mobile',
            'Date'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'H' => 0,
            'I' => 0,
            'J' => '@',
            'K' => '@',
            'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    /**
    * @var Client $client
    */
    public function map($client): array
    {
        return [
            $client->name,
            $client->identification_type_id ? $this->identification_types->pluck('id', 'name')->search($client->identification_type_id) : null,
            $client->tax_id,
            $client->email,
            $client->address ? $client->address->line_1: '',
            $client->address ? $this->countries->pluck('id', 'name')->search($client->address->country_id): '',
            $client->sector_id ? $this->sectors->pluck('id', 'name')->search($client->sector_id) : null,
            $client->address ? $client->address->zip: '',
            $client->fax,
            $client->phone,
            $client->mobile,
            Date::dateTimeToExcel($client->created_at)
        ];
    }
}
