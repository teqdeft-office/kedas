<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipping extends Model
{
    use SoftDeletes;

    protected $fillable = [
'user_id' , 'company_id', 'invoice_id' ,'shipping_cost','status'
    ];
    protected $table = 'shippings';

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    public function invoices()
    {
        return $this->belongsTo('App\invoice');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
