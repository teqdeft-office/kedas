<?php

namespace App\Validators\User;

use App\Validators\Validator;
use Config;

class ProfileValidator extends Validator 
{
    /**
     * Rules for Lab User registration
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for User registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
            'password.regex' => trans('messages.password_regex'),
            'toc.accepted' => trans('messages.toc_accepted'),
            'logo.dimensions' => trans('messages.dimensions_invalid'),
            'signature.dimensions' => trans('messages.dimensions_invalid'),
        ];
        $this->rules = [
            'name' => 'required|string|min:3|max:255',
            'language' => 'nullable|string|in:'.implode(array_keys(Config::get('constants.languages')), ',')
        ];
        switch ($validationFor) {
            case 'add':
            $additionalRules = [
                'role' => 'required|string|min:3|max:255',
                'sector' => 'required',
                'country' => 'required',
                'currency' => 'required',
                'address' => 'required|string|min:3|max:255',
                'city' => 'nullable|string|min:3|max:255',
                'phone' => 'required|string|min:10|max:19',
                'mobile' => 'nullable|string|min:10|max:19'
            ];
            break;
            case 'update':
            $additionalRules = [
                'current_password' => 'nullable|required_with:password|string|min:6',
                'password' => 'nullable|required_with:current_password|string|min:6|confirmed|different:current_password'
            ];
            break;
            case 'api_update':
            $additionalRules = [
                'name' => 'nullable|string|min:3|max:255',
                'current_password' => 'nullable|required_with:password|string|min:6',
                'password' => 'nullable|required_with:current_password|string|min:6|confirmed|different:current_password'
            ];
            break;
            case 'verify_account':
            $additionalRules = [
                'name' => 'nullable',
                'email' => 'required|string|email|max:255',
                'email_token' => 'required|string|max:4|min:4'
            ];
            break;
            case 'api_reset_password':
            $additionalRules = [
                'name' => 'nullable',
                'email' => 'required|string|email|max:255',
                'email_token' => 'required|string|max:4|min:4',
                'password' => 'nullable|string|min:6'
            ];
            break;
            case 'company':
            $additionalRules = [
                'role' => 'required|string|min:3|max:255',
                'address' => 'required|string|min:3|max:255',
                'city' => 'nullable|string|min:3|max:255',
                'country' => 'required',
                'phone' => 'required|string|min:10|max:19',
                // 'phone' => 'required|string|min:10|max:12|regex:/[0-9]{9}/',
                'logo' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
                    // |dimensions:min_width=300,min_height=300',
                'signature' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
                    // |dimensions:min_width=300,min_height=150',
            ];
            break;
            case 'api_user_update':
            $additionalRules = [
                'name' => 'nullable',
                'firstname' => 'required|string|min:3|max:80',
                'lastname' => 'required|string|min:3|max:80',
                'state' => 'required|string|min:2|max:255',
                'address' => 'nullable|string|min:1|max:255',
                'city' => 'required|string|min:3|max:255',
                'country' => 'required',
                'logo' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
            ];
            break;
            
            default:
            $additionalRules = [];
            break;
        }
        $this->rules = array_merge($this->rules, $additionalRules);
    }
}
