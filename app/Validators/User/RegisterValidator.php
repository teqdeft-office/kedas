<?php

namespace App\Validators\User;

use App\Validators\Validator;

class RegisterValidator extends Validator 
{
    /**
     * Rules for User registration
     *
     * @var array
     */
    protected $rules = [
        'name' => 'required|string|min:3|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6',
        'phone' => 'nullable|min:10|max:12|regex:/[0-9]{9}/'
    ];

    /**
     * Messages for User registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct()
    {
        $this->messages = [
            'password.regex' => trans('messages.password_regex')
        ];
    }

}