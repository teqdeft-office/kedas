<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class SupplierValidator extends Validator 
{
    /**
     * Rules for Supplier creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Supplier
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $client = null)
    {
        $this->messages = [
            'email.unique' => trans('validation.supplier_email'),
        ];
        $this->rules = [
            'name' => 'required|string|min:3|max:255',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('suppliers')->where(function ($query) {
                return $query->where('user_id', Auth::id());
            })],
            'address' => 'nullable|string|min:3|max:255',
            // 'phone' => 'nullable|min:10|max:12|regex:/[0-9]{9}/'
            'phone' => 'nullable|min:10|max:19',
            'mobile' => 'nullable|min:10|max:19'
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'email' => ['required', 'string', 'email', 'max:255', Rule::unique('suppliers')->where(function ($query) {
                    return $query->where('user_id', Auth::id());
                })->ignore($client->id)],
            ];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
