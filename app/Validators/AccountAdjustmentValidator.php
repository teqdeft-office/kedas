<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

class AccountAdjustmentValidator extends Validator 
{
    /**
     * Rules for Credit Note creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Credit Note
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($action = 'add')
    {
        $this->messages = [
            'adjustment_date.date_format' => trans('messages.date_format'),
            'items.required' => trans('messages.inventory_adjustment_without_items'),
            'items.*.concept-item.required' => trans('messages.please_select_account')
        ];
        $this->rules = [     
            'adjustment_date' => 'required|date_format:Y-m-d',
            'items' => 'required|array',
            'items.*' => 'required|min:2',
            'items.*.concept-item' => 'required',
            'observations' => 'nullable|string',
            'reference' => 'nullable|string'
        ];

        if ($action == 'update') {
            $additionalRules = [];
            $this->rules = array_merge($this->rules, $additionalRules);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
