<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Config;
use Auth;

class InvoiceValidator extends Validator 
{
    /**
     * Rules for Invoice creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Invoice
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'sale', $resource_associated = false, $forPos = false)
    {
        $this->messages = [
            'invoice_start_date.date_format' => trans('messages.date_format'),
            'invoice_expiration_date.date_format' => trans('messages.exp_date_format'),
            'products.required' => trans('messages.invoice_without_items'),
            'logo.dimensions' => trans('messages.dimensions_invalid'),
            'signature.dimensions' => trans('messages.dimensions_invalid'),
        ];
        $this->rules = [
            'invoice_start_date' => 'required|date_format:Y-m-d',
            // 'invoice_expiration_date' => 'required|date_format:Y-m-d|after_or_equal:invoice_start_date',
            'term' => 'nullable|string|in:'.implode(array_keys(Config::get('constants.terms')), ','),
            'manual_days' => 'bail|numeric|min:1|max:4000|required_if:term,manual',
            'tac' => 'nullable|string',
            'notes' => 'nullable|string',
            'exchange_currency' => 'nullable|required_with:exchange_rate',
            'exchange_rate' => 'nullable|required_with:exchange_currency|regex:/^\d+(\.\d{1,6})?$/|max:16',
        ];
        switch ($validationFor) {
            case 'recurring':
                $additionalRules = [
                    'products' => 'required',
                    'observation' => 'nullable|string',
                    'frequency' => 'numeric|min:1|string',
                    'frequency_type' => 'required|string|in:month,day',
                    'invoice_start_date' => 'required|date_format:Y-m-d|before_or_equal:invoice_end_date',
                    'invoice_end_date' => 'required|date_format:Y-m-d|after_or_equal:invoice_start_date',
                ];
                break;
            case 'supplier':
                $additionalRules = [
                    'concepts' => 'required',
                    'ncf_number' => 'nullable|string|regex:/^[A-Za-z]{1,1}\d{10}$/|min:3|max:255',
                ];
                break;
            
            default:
                $additionalRules = [
                    'products' => 'required',
                    'logo' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
                    // |dimensions:min_width=300,min_height=300',
                    'signature' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
                    // |dimensions:min_width=300,min_height=150',
                ];
                if (!$forPos && !Auth::user()->company->logo) {
                    $additionalRules = array_merge($additionalRules, [
                        'logo' => 'required|image|mimes:jpeg,bmp,png,jpg',
                        // |dimensions:min_width=300,min_height=300'
                    ]);
                }

                /*if (!Auth::user()->company->signature) {
                    $additionalRules = array_merge($additionalRules, [
                        'signature' => 'required|image|mimes:jpeg,bmp,png,jpg',
                        // |dimensions:min_width=300,min_height=150'
                    ]);
                }*/
                break;
        }
        if (!$resource_associated) {
            $additionalRules = array_merge($additionalRules, [
                'contact_id' => 'required'
            ]);
        }

        $this->rules = array_merge($this->rules, $additionalRules);
    }

    public function getRules() {
        return $this->rules;
    }
}
