<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

use Auth;
use App\Rules\InternationalChar;

class RoleValidator extends Validator 
{
    /**
     * Rules for tax creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Tax
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $role = null)
    {
        $this->messages = [];
        $this->rules = [
            'name' => ['required', 'string', 'min:3', 'max:255', new InternationalChar(false, true),  Rule::unique('roles')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id);
            })],
            // 'name' => 'required|string|regex:/^[a-zA-Z\s]+$/|min:3|max:255',
            'permissions' => 'required|array',
            'permissions.*' => 'required|distinct|min:1',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'name' => ['required', 'string', 'min:3', 'max:255', Rule::unique('roles')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })->ignore($role->id)],
            ];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
