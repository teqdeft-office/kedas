<?php

namespace App\Validators\Accounting;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use App\Rules\InternationalChar;

use Config;
use Auth;

class HeadValidator extends Validator 
{
    /**
     * Rules for Lab User registration
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for User registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $userAccountingHead = null)
    {
        $this->messages = [
            'slug.unique' => trans('messages.accounting_head_already_exists'),
            'node-type.in' => trans('messages.something_went_wrong'),
            'parent-account.required_if' => trans('validation.required_if', ['attribute' => trans('labels.account'), 'other' => trans('labels.account'), 'value' => trans('labels.sub_account')]),
            'account-level.max' => trans('validation.max.numeric', ['attribute' => trans('labels.account_level'), 'max' => 4])
        ];
        $this->rules = [
            'account-name' => ['required', 'string', 'min:3', 'max:255', new InternationalChar(false, true)],
            'description' => 'nullable|min:10',
            'opening_balance' => 'nullable|string|regex:/^\d+(\.\d{1,2})?$/|max:16',
        ];

        switch ($validationFor) {
            case 'add':
                $additionalRules = [
                    'node-type' => 'required|in:primary,secondary',
                    'account-type' => 'bail|string|required_if:node-type,secondary',
                    // |in:'. Auth::user()->company->accountingHeads->children->pluck('id')->implode(','),
                    // 'chart-type' => 'bail|required_if:node-type,secondary|in:control,detail|string',
                    'account-control' => 'nullable|required_if:chart-type,detail|string',
                    'is_sub_account' => 'nullable|boolean',
                    'parent-account' => 'bail|required_if:is_sub_account,1',
                    'account-level' => 'bail|nullable|required_with:parent-account|numeric|min:1|max:3',
                    'slug' => 'required|string',
                    /*'slug' => ['required', 'string', Rule::unique('user_accounting_heads', 'slug')->where(function ($query) {
                        return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                    })],*/
                    'code' => ['required', 'string', 'regex:/^[0-9]+$/i', Rule::unique('user_accounting_heads', 'custom_code')->where(function ($query) {
                        return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                    })]
                ];
                break;
            case 'edit':
                $additionalRules = [
                    /*'slug' => ['required', 'string', Rule::unique('user_accounting_heads', 'slug')->where(function ($query) {
                        return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                    })->ignore($userAccountingHead->id)],*/
                    'slug' => 'required|string',
                    'code' => ['required', 'string', 'regex:/^[0-9]+$/i', Rule::unique('user_accounting_heads', 'custom_code')->where(function ($query) {
                        return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                    })->ignore($userAccountingHead->id)]
                ];
                break;
            
            default:
                $additionalRules = [];
                break;
        }
        $this->rules = array_merge($this->rules, $additionalRules);
    }
}
