<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

use App\Company;
use App\Banks;

class BankValidator extends Validator
{
    /**
     * Rules for client creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Client registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $bank = null)
    {
        $this->messages = [
            'account_number.unique' => trans('validation.account_number'),
        ];
        $this->rules = [
            'account_number' => ['required', 'numeric', Rule::unique('banks')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id);
            })],
        ];

        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'account_number' => ['required', 'numeric', Rule::unique('banks')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })->ignore($bank->id)],
            ];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }

    }

    public function getRules() {
        return $this->rules;
    }
}
