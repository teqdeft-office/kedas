<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Config;

class TransactionValidator extends Validator 
{
    /**
     * Rules for Transaction creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Transaction
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'in', $action = 'add')
    {
        $this->messages = [
            'start_date.date_format' => trans('messages.date_format'),
            'payments.required_if' => trans('messages.transaction_without_items'),
            'concepts.required_if' => trans('messages.transaction_without_items'),
            'contact_id.required_if' => trans('messages.transaction_without_contacts'),
            //'document.dimensions' => trans('messages.dimensions_invalid')
        ];
        $this->rules = [         
            'start_date' => 'required|date_format:Y-m-d',
            'anotation' => 'nullable|string',
            'observation' => 'nullable|string',
            //'document.*' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
        ];

        if ($action == 'add') {
            $additionalRules = ['contact_id' => 'required'];
            $this->rules = array_merge($this->rules, $additionalRules);
        }

        switch ($validationFor) {
            case 'in':
                $additionalRules = [
                    // 'payment_account' => 'required|string|in:'.implode(array_keys(Config::get('constants.payment_accounts')), ','),
                    'payment_method' => 'required|string|in:'.implode(array_keys(Config::get('constants.payment_methods')), ','),
                    'bank_account' => 'required',
                    'payments' => 'required',
                ];
                break;
            case 'out':
                $additionalRules = [
                    // 'payment_account' => 'required|string|in:'.implode(array_keys(Config::get('constants.payment_accounts')), ','),
                    'payment_method' => 'required|string|in:'.implode(array_keys(Config::get('constants.payment_methods')), ','),

                    'bank_account' => 'required',
                    'transaction_with_invoice' => 'required',
                    'contact_id' => 'required_if:transaction_with_invoice,1',
                    'payments' => 'required_if:transaction_with_invoice,1',
                    'concepts' => 'required_if:transaction_with_invoice,0',
                ];
                break;
            case 'recurring':
                $additionalRules = [
                    // 'payment_account' => 'required|string|in:'.implode(array_keys(Config::get('constants.payment_accounts')), ','),
                    'bank_account' => 'required',
                    'start_date' => 'required|date_format:Y-m-d|before_or_equal:end_date',
                    'end_date' => 'required|date_format:Y-m-d|after_or_equal:start_date',
                    'frequency' => 'numeric|min:1|string',
                    'frequency_type' => 'required|string|in:month,day',
                    'concepts' => 'required',
                    'contact_id' => 'nullable'
                ];
                break;
            
            default:
                $additionalRules = [
                    'payments' => 'required'
                ];
                break;
        }
        $this->rules = array_merge($this->rules, $additionalRules);
    }

    public function getRules() {
        return $this->rules;
    }
}
