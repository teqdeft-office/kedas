<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class WithHoldingValidator extends Validator 
{
    /**
     * Rules for Item Category creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Item Category
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->rules = [
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d',
            'type_account' => 'required',
            'frequency_type' => 'required',
            'income_account' => 'required',
            'amount' => 'required',
        ];
    }

    public function getRules() {
        return $this->rules;
    }
}
