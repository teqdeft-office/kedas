<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

class TaxValidator extends Validator 
{
    /**
     * Rules for tax creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Tax
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
            'percentage.between' => trans('messages.invalid_percentage')
        ];
        $this->rules = [
            'name' => 'required|string|regex:/^[a-zA-Z\s]+$/|min:3|max:255',
            'percentage' => 'required|numeric|between:0.1,99.99',
            'description' => 'nullable|min:10|max:255',
            'sales_account' => 'required|string',
            'purchases_account' => 'required|string',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
