<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

class PurchaseOrderValidator extends Validator 
{
    /**
     * Rules for Purchase Order creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Purchase Order
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($action = 'add')
    {
        $this->messages = [
            'purchase_order_date.date_format' => trans('messages.date_format'),
            'purchase_order_expiration_date.date_format' => trans('messages.exp_date_format'),
            'concepts.required' => trans('messages.purchase_order_without_items'),
        ];
        $this->rules = [
            'contact_id' => 'required',
            'concepts' => 'required',
            'purchase_order_date' => 'required|date_format:Y-m-d|before_or_equal:purchase_order_expiration_date',
            'purchase_order_expiration_date' => 'required|date_format:Y-m-d|after_or_equal:purchase_order_date',
            'notes' => 'nullable|string',
            'tac' => 'nullable|string',
            'exchange_currency' => 'nullable|required_with:exchange_rate',
            'exchange_rate' => 'nullable|required_with:exchange_currency|regex:/^\d+(\.\d{1,6})?$/|max:16',
        ];

        if ($action == 'update') {
            $additionalRules = [];
            $this->rules = array_merge($this->rules, $additionalRules);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
