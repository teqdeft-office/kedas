<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

class TableValidator extends Validator
{
    /**
     * Rules for tax creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Tax
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
            'percentage.between' => trans('messages.invalid_percentage')
        ];
        $this->rules = [
            'table_name' => 'required|min:3|max:255',
            'date' => 'date_format:Y-m-d',
            'description' => 'nullable|min:10|max:255',
            'zone' => 'required|numeric',
            'table_number' => 'required|string',
            'chairs_number' => 'required|string'
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
