<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class EstimateValidator extends Validator 
{
    /**
     * Rules for Estimate creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Estimate CRUD
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
            'estimate_start_date.date_format' => trans('messages.date_format'),
            'estimate_expiration_date.date_format' => trans('messages.exp_date_format'),
            'products.required' => trans('messages.estimate_without_items'),
            'logo.dimensions' => trans('messages.dimensions_invalid'),
            'signature.dimensions' => trans('messages.dimensions_invalid'),
        ];
        $this->rules = [
            'contact_id' => 'required',
            'products' => 'required',
            'estimate_start_date' => 'required|date_format:Y-m-d|before_or_equal:estimate_expiration_date',
            'estimate_expiration_date' => 'required|date_format:Y-m-d|after_or_equal:estimate_start_date',
            'notes' => 'nullable|string',
            'exchange_currency' => 'nullable|required_with:exchange_rate',
            'exchange_rate' => 'nullable|required_with:exchange_currency|regex:/^\d+(\.\d{1,6})?$/|max:16',
            'signature' => 'nullable|image|mimes:jpeg,bmp,png,jpg'
        ];
        $additionalRules = [];

        if (!Auth::user()->company->logo) {
            $additionalRules = array_merge($additionalRules, [
                'logo' => 'required|image|mimes:jpeg,bmp,png,jpg'
                // |dimensions:min_width=300,min_height=300'
            ]);
        } else {
            $additionalRules = array_merge($additionalRules, [
                'logo' => 'nullable|image|mimes:jpeg,bmp,png,jpg'
                // |dimensions:min_width=300,min_height=300'
            ]);
        }

        /*if (!Auth::user()->company->signature) {
            $additionalRules = array_merge($additionalRules, [
                'signature' => 'required|image|mimes:jpeg,bmp,png,jpg'
                // |dimensions:min_width=300,min_height=150'
            ]);
        } else {
            $additionalRules = array_merge($additionalRules, [
                'signature' => 'nullable|image|mimes:jpeg,bmp,png,jpg'
                // |dimensions:min_width=300,min_height=150'
            ]);
        }*/

        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
        $this->rules = array_merge($this->rules, $additionalRules);
    }

    public function getRules() {
        return $this->rules;
    }
}
