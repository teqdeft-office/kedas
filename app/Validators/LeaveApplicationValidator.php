<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class LeaveApplicationValidator extends Validator 
{
    /**
     * Rules for tax creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Tax
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
            'from_date.date_format' => trans('messages.date_format'),
            'to_date.date_format' => trans('messages.date_format'),
            'leave_type_id.in' => trans('messages.invalid_leave_type'),
        ];

        $this->rules = [
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d',
            'purpose' => 'required|min:10|max:255',
            'leave_type_id' => 'required|in:'.Auth::user()->company->leaveTypes->implode('id', ','),
            'current_balance' => 'required',
            'number_of_days' => 'required',
        ];

        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
