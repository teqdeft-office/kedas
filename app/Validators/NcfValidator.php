<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

class NcfValidator extends Validator 
{
    /**
     * Rules for ncf creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Ncf
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
           'ncf_number.regex' => trans('messages.ncf_number_regex')
        ];
        $this->rules = [
            'name' => 'required|string|regex:/^[a-zA-Z\s]+$/|min:3|max:255',
            'ncf_number' => 'required|string|min:11|max:11|regex:/^[A-Za-z]{1,1}\d{10}$/',
            'description' => 'nullable|min:10|max:255'
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
