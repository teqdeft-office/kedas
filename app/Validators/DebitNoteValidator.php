<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;

class DebitNoteValidator extends Validator 
{
    /**
     * Rules for debit note creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for debit note
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($action = 'add')
    {
        $this->messages = [
            'debit_note_date.date_format' => trans('messages.date_format'),
            'concepts.required' => trans('messages.debit_notes_without_items')
        ];

        $this->rules = [
            'contact_id' => 'required',      
            'concepts' => 'required',      
            'debit_note_date' => 'required|date_format:Y-m-d',
            'exchange_currency' => 'nullable|required_with:exchange_rate',
            'exchange_rate' => 'nullable|required_with:exchange_currency|regex:/^\d+(\.\d{1,6})?$/|max:16',
        ];

        if ($action == 'update') {
            $additionalRules = [];
            $this->rules = array_merge($this->rules, $additionalRules);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
