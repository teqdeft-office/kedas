<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;
use Config;
use App\Rules\InternationalChar;
use App\Interfaces\CommonConstants;

class InventoryValidator extends Validator implements CommonConstants
{
    /**
     * Rules for Inventory creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Inventory
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $inventory = null, $type)
    {
        $this->messages = [
            'tax_id.in' => trans('messages.invalid_tax'),
            'item_category_id.in' => trans('messages.invalid_item_category'),
            'type_of_item.in' => trans('messages.invalid_type'),
            'image.dimensions' => trans('messages.dimensions_invalid'),
            'income_account.required_without' => trans('validation.required', ['attribute' => trans('labels.income_or_expense')]),
            'expense_account.required_without' => trans('validation.required', ['attribute' => trans('labels.income_or_expense')]),
        ];
        $this->rules = [
            'type' => 'required|string|in:'.implode(array_keys(Config::get('constants.type_of_inventories')), ','),

            'name' => ['required', 'string', 'min:3', 'max:255', new InternationalChar(false, true)],
            'sale_price' => 'required|string|regex:/^\d+(\.\d{1,2})?$/|max:16',
            'reference' => 'nullable|string|min:3|max:255',
            'description' => ['nullable', 'string', 'min:3', 'max:255', new InternationalChar(true, true)],

            'sku' => ['nullable', 'string', 'min:6', 'max:20', Rule::unique('inventories')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],
            'barcode' => ['nullable', 'string', 'min:6', 'max:20', Rule::unique('inventories')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],

            'item_category_id' => 'nullable|in:'.Auth::user()->company->itemCategories->implode('id', ','),
            'tax_id' => 'nullable|in:0,'.Auth::user()->company->taxes->implode('id', ', '),
            // 'type_of_item' => 'nullable|in:'.implode(array_keys(Config::get('constants.type_of_item')), ','),
            // 'account' => 'nullable|string|in:'.implode(array_keys(Config::get('constants.accounts')), ','),
            'image' => 'nullable|image|mimes:jpeg,bmp,png,jpg',

            // 'track_inventory' => 'sometimes',
            'cost' => 'nullable|string|regex:/^\d+(\.\d{1,2})?$/|max:16',

            // |dimensions:min_width=260,min_height=220',
        ];

        if ($type == self::INVENTORY) {
            $this->rules += [
                'asset_account' => 'required|string',
                'income_account' => 'required|string',
                'expense_account' => 'required|string',
                'memberships' => 'nullable|array|in:'.Auth::user()->company->memberships->implode('id', ','),
                'memberships.*' => 'nullable|distinct',
            ];
        } else {
            $this->rules += [
                'income_account' => 'bail|required_without:expense_account',
                'expense_account' => 'bail|required_without:income_account',
            ];
        }

        if (!$inventory && $type == self::INVENTORY) {
            $this->rules += [
                'init_quantity_date' => 'required|date_format:Y-m-d',
                'quantity' => 'required|numeric|min:0',
                'unit_cost' => 'required|string|regex:/^\d+(\.\d{1,2})?$/|max:16',
            ];
        }

        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'sku' => ['nullable', 'string', 'min:6', 'max:20', Rule::unique('inventories')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($inventory->id)],
                'barcode' => ['nullable', 'string', 'min:6', 'max:20', Rule::unique('inventories')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($inventory->id)],
            ];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
