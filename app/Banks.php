<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;

class Banks extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = [
        'bank_name',
        'bank_date',
        'name_of_account',
        'account_balance',
        'bank_address',
        'reference',
        'account_number',
        'currency',
        'phone',
        'email',
        'swift_code',
        'routing_number',
        'intermediary',
        'notes',
        'account_type'
    ];

    protected $with  = ['userRelation', 'transaction'];

    public function userRelation()
    {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function currencyInfo()
    {
        return $this->belongsTo('App\Currency', 'currency', 'id');
    }

    public function transaction()
    {
        return $this->hasMany('App\Transaction', 'bank_id');
    }
    // protected static function booted()
    // {
    //     static::created(function ($banks) {
    //         $createdata = History::createdata();
    //         $banks->histories()->create($createdata);
    //     });
    //     static::updated(function($banks){
    //         $updatedata = History::updatedata();
    //         $banks->histories()->create($updatedata);
    //     });
    //     static::deleted(function($banks) {
    //         $deletedata = History::deletedata();
    //         $banks->histories()->create($deletedata);
    //     });
    // }

    // public function histories()
    // {
    //     return $this->morphMany('App\History', 'record');
    // }

    protected static function booted()
    {
        static::created(function ($banks) {
            $createdata = History::createdata();
            $banks->histories()->create($createdata);
        });
        static::updated(function($banks){
            $updatedata = History::updatedata();
            $banks->histories()->create($updatedata);
        });
        static::deleted(function($banks) {
            $deletedata = History::deletedata();
            $banks->histories()->create($deletedata);
        });
    }

    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
}
