<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Interfaces\CommonConstants;
use App\History;
use Auth;
use Carbon\Carbon;

class Invoice extends Model implements CommonConstants
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'exchange_rate_total', 'debit_credit_amount', 'term_in_locale'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    
    protected static function booted()
    {
        static::creating(function ($invoice) {
            $user = Auth::user() ?: $invoice->user;
            $next_number = $user->company->invoices()->withTrashed() ? $user->company->invoices()->withTrashed()->where('type', $invoice->type)->count() + 1 : 1;
            $invoice->internal_number = $next_number;
            
            $invoice->ncf_number = $invoice->ncf ? ($invoice->type == self::SALE ? ($invoice->ncf->current_number ? getNextAlphaNumber($invoice->ncf->current_number) : getNextAlphaNumber($invoice->ncf->initial_number) ) : null) : $invoice->ncf_number;
        });

        static::created(function ($invoice){
            $user = Auth::user() ?: $invoice->user;
            $invoice->productionOrder()->create([
                "invoice_id"=>$invoice->id,
                "company_id"=>$user->company->id,
                "user_id"=>$user->id,
                "status"=>"pending",
            ]);
        });
        static::created(function ($invoice) {
            $createdata = History::createdata();
            $invoice->histories()->create($createdata);
        });
        static::updated(function($invoice){
            $updatedata = History::updatedata();
            $invoice->histories()->create($updatedata);
        });
        static::deleted(function($invoice) {
            $deletedata = History::deletedata();
            $invoice->histories()->create($deletedata);
            $invoice->conceptItems()->delete();
            $invoice->items()->delete();
            $invoice->accountingEntries()->delete();
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }


    /**
     * Set the invoice's ncf.
     *
     * @param  string  $value
     * @return void
     */
    public function setNcfNumberAttribute($value) {
        $this->attributes['ncf_number'] = $value ? strtoupper($value) : null;
    }
    
    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * List of items associated with an invoice.
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with(['tax', 'inventory']);
    }

    /**
     * The transactions that belong to the invoice.
     */
    public function transactions()
    {
        return $this->belongsToMany('App\Transaction')->withPivot('amount');
    }

    /**
     * List of concepts items associated with an invoice.
     */
    public function conceptItems()
    {
        return $this->hasMany('App\ConceptItem')->with(['tax', 'concept']);
    }

    /**
     * Get the user profile associated with the invoice.
     */
    public function userProfile()
    {
        return $this->hasOne('App\UserProfile', 'user_id', 'user_id');
    }

    /**
     * Get the user associated with the invoice.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The transactions that belong to the invoice.
     */
    public function creditNotes()
    {
        return $this->belongsToMany('App\CreditNote')->withPivot('amount');
    }

    /**
     * Get the ncf with the invoice.
     */
    public function ncf()
    {
        return $this->belongsTo('App\Ncf');
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function getCalculationAttribute() {
        if ($this->attributes['type'] != self::SUPPLIER) {
            $pluckedCalculation = $this->items->pluck('calculation');
        } else {
            $pluckedCalculation = $this->conceptItems->pluck('calculation')->merge($this->items->pluck('calculation'));
        }

        $invoiceSum = array();
        $taxInfo = array();
        $taxInfoArr = array();
        foreach ($pluckedCalculation as $plucked) {
            foreach ($plucked as $sub_key => $value) {
                if (!is_array($value)) {
                    if (!array_key_exists($sub_key, $invoiceSum)) $invoiceSum[$sub_key] = 0;
                    $invoiceSum[$sub_key] += $value;
                } else {
                    array_push($taxInfoArr, $value);
                }
            }
        }
        $taxInfoArr = collect($taxInfoArr)->filter()->groupBy('tax_id')->values()->toArray();

        if ($taxInfoArr) {
            foreach ($taxInfoArr as $taxInfoEle) {
                $temp = [];
                foreach ($taxInfoEle as $taxData) {
                    foreach ($taxData as $sub_key => $value) {
                        if( ! array_key_exists($sub_key, $temp)) $temp[$sub_key] = 0;
                        if ($sub_key === 'tax') {
                            $temp[$sub_key] += $value;
                        } else {
                            $temp[$sub_key] = $value;
                        }
                    }
                }
                array_push($taxInfo, $temp);
            }
        }

        $paidAmount = 0;
        foreach ($this->transactions as $transaction) {
            $paidAmount += $transaction->pivot->amount;
        }

        $creditNoteAmount = 0;
        $creditNoteAmount = $this->creditNotes ? $this->creditNotes->pluck('pivot')->sum('amount') : 0;

        $debitNoteAmount = 0;
        $debitNoteAmount = $this->debitNotes ? $this->debitNotes->pluck('pivot')->sum('amount') : 0;

        $invoiceSum['paidAmount'] = $paidAmount;
        $invoiceSum['pendingAmountWithoutCreditNote'] = $invoiceSum['total'] - $paidAmount;

        $invoiceSum['pendingAmountWithoutDebitNote'] = $invoiceSum['total'] - $paidAmount;
        
        $invoiceSum['pendingAmount'] = ($this->attributes['is_void'] ?? 0) ? 0.00 : $invoiceSum['total'] - $paidAmount - $creditNoteAmount;
        $invoiceSum['supplierPendingAmount'] = ($this->attributes['is_void'] ?? 0) ? 0.00 : $invoiceSum['total'] - $paidAmount - $debitNoteAmount;
        
        $invoiceSum['total'] = ($this->attributes['is_void'] ?? 0) ? 0.00 : $invoiceSum['total'];

        $invoiceSum = array_map('addZeros', $invoiceSum);

        $invoiceSum['paidAmountInExe'] = $this->currency ? $paidAmount / $this->attributes['exchange_currency_rate'] : $paidAmount;
        $invoiceSum['pendingAmountInExe'] = $this->currency ? $invoiceSum['pendingAmount'] / $this->attributes['exchange_currency_rate'] : $invoiceSum['pendingAmount'];
        $invoiceSum['supplierPendingAmountInExe'] = $this->currency ? $invoiceSum['supplierPendingAmount'] / $this->attributes['exchange_currency_rate'] : $invoiceSum['supplierPendingAmount'];

        $invoiceSum['taxInfo'] = $taxInfo;

        $nowDate = getTodayDate();
        if ($nowDate > $this->attributes['expiration_date']  && $invoiceSum['pendingAmount'] > 0 && ($this->attributes['is_void'] ?? 0) == 0) {
            $invoiceSum['status'] = trans('labels.overdue');
        } else {
            $invoiceSum['status'] = ($this->attributes['is_void'] ?? 0) ? trans('labels.voided') : ($invoiceSum['pendingAmount'] > 0 ? trans('labels.pending') : trans('labels.paid'));
        }

        return $invoiceSum;
    }

    public function getExchangeRateTotalAttribute() {
        return $this->currency ? round(($this->getCalculationAttribute()['total'] / $this->attributes['exchange_currency_rate']), 6) : null;
    }

    public function getDebitCreditAmountAttribute() {
        return $debitCreditAmount = $this->accountingEntries->groupBy('nature')->map(function ($row) {
            return $row->sum('amount');
        });
    }

    public function getTermInLocaleAttribute() {
        return $this->attributes['term'] == '0' ? trans('labels.cash') : (($this->attributes['term'] == 'manual' ? ($this->manual_days) : ($this->attributes['term'])) .' '. trans('labels.'.str_plural('day', $this->manual_days ?: $this->attributes['term'])) );
    }

    /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * Get the currency with the invoice.
     */
    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'exchange_currency_id');
    }

    /**
     * Get the accounting entries of the invoice.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }

    /**
     * The transactions that belong to the invoice.
     */
    public function debitNotes()
    {
        return $this->belongsToMany('App\DebitNote')->withPivot('amount');
    }

    /**
     * Get the contact point associated with the invoice.
     */
    public function contactPoint()
    {
        return $this->hasOne('App\ContactPoint');
    }

    
    /**
     * Get the production order associated with the invoice.
     */
    public function productionOrder()
    {
        return $this->hasOne('App\Production');
    }
    public function shippings()
    {
        return $this->hasOne('App\Shipping');
    }
}
