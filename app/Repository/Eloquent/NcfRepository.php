<?php

namespace App\Repository\Eloquent;

use App\Ncf;
use App\User;
use App\Company;
use App\Repository\NcfRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;   

class NcfRepository implements NcfRepositoryInterface
{

   /**
    * NcfRepository constructor.
    *
    * @param Ncf $model
    */
	public function __construct(Ncf $model)
	{
		// parent::__construct($model);
	}

	/**
	* @return Collection
	*/
   	public function all(): Collection
   	{
   		return $this->model->all();    
   	}

   	/**
    * @param array $attributes
    *
    * @return Model
    */
    public function create(Company $company, array $attributes): Ncf
    {
        // return $this->model->create($attributes);
        return $company->ncfs()->create($attributes);
    }

    /**
    * @param array $attributes
    *
    * @return Model
    */
    public function update(Ncf $ncf, array $attributes): bool
    {
        return $ncf->update($attributes);
    }

   	public function findforUser(User $user, $id = null) {
   		if ($id) {
	    	return $user->ncfs()->findOrFail($id);
   		} else {
   			return $user->ncfs;
   		}
	}

	public function findforCompany(Company $company, $id = null) {
   		if ($id) {
	    	return $company->ncfs()->findOrFail($id);
   		} else {
   			return $company->ncfs;
   		}
	}
}
