<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;

class Ncf extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ncfs';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($ncf) {
            $createdata = History::createdata();
            $ncf->histories()->create($createdata);
        });
        static::updated(function($ncf){
            $updatedata = History::updatedata();
            $ncf->histories()->create($updatedata);
        });
        static::deleted(function($ncf) {
            $deletedata = History::deletedata();
            $ncf->histories()->create($deletedata);
            $ncf->invoices()->each(function($invoice) {
                $invoice->ncf()->dissociate();
                $invoice->save();
            });
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * The invoice that belong to the ncf.
     */
    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    public function getNextNcfNumber() {
        $base_ten = base_convert($this->attributes['initial_number'], 36, 10);
        return base_convert($base_ten + 1, 10, 36);
	}
}
