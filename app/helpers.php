<?php
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

if (!function_exists('showIfAvailable')) {
	function showIfAvailable($value, $default = 'N/A') {
		return $value ?: $default;
	} 
}

if (!function_exists('titleCase')) {
	function titleCase($value) {
		return ucwords(strtolower(str_replace('_', ' ', $value)));
	} 
}

if (!function_exists('prePrint')) {
	function prePrint($arr) {
		echo "<pre>";
		print_r($arr);
		die;
	} 
}

if (!function_exists('addressFormat')) {
	function addressFormat($address, $split = false) {
		if (!$address) {
			return 'N/A';
		}
		$addressLine = trim(($address->line_1 ? $address->line_1.', ' : '').($address->line_2 ? $address->line_2.', ': '').($address->city ? $address->city.', ': '').($address->state ? $address->state.', ': '').($address->country_id ? $address->countryInfo->name.', ' : '').($address->zip ?: ''));
		if ($split) {
			$addressLine = [
				trim(($address->line_1 ? $address->line_1.', ' : '').($address->line_2 ? $address->line_2.'': ''), ', ').'.',
				trim(($address->city ? $address->city.', ': '').($address->state ? $address->state.', ': '').($address->country_id ? $address->countryInfo->name.', ' : '').($address->zip ?: ''), ', ').'.'				
			];
			return $addressLine;
		}
		return trim($addressLine, ',').'.';
	} 
}

if (!function_exists('strToSlug')) {
	function strToSlug($str, $slug = '_') {
		return (string) Str::of($str)->slug($slug);
	}
}

if (!function_exists('strToCamelCase')) {
	function strToCamelCase($str) {
		return camel_case($str);
	}
}

if (!function_exists('arrayEleToSlug')) {
	function arrayEleToSlug($array) {
		return array_map(function($value) {
			return strToSlug($value);
		}, $array);
	}
}

if (!function_exists('slugToStr')) {
	function slugToStr($str, $slug = '_') {
		return Str::title(str_replace($slug, ' ', $str));
	}
}

if (!function_exists('getItemCost')) {
	function getItemCost($price, $quantity = 1, $discount = 0, $tax = null) {
		$baseTotal = $price * $quantity;
		$discountAmount = 0.00;
		$taxAmount = 0.00;
		$taxInfo = [];

		if ($discount) {
			$discountAmount = $baseTotal * ($discount / 100);
		}
		if ($tax && $tax->percentage) {
			$taxAmount = ($baseTotal - $discountAmount) * ($tax->percentage / 100);
			$taxInfo['tax'] = addZeros($taxAmount);
			$taxInfo['tax_id'] = $tax->id;
			$taxInfo['tax_name'] = $tax->name;
			$taxInfo['formatted_name'] = $tax->formatted_name;
		}

		return [
			'baseTotal' => addZeros($baseTotal),
			'discountAmount' => addZeros($discountAmount),
			'taxAmount' => addZeros($taxAmount),
			'taxInfo' => $taxInfo,
			'subtotal' => addZeros($baseTotal - $discountAmount),
			'total' => addZeros(($baseTotal - $discountAmount) + $taxAmount)
		];
	}
}

if (!function_exists('addZeros')) {
	function addZeros($num, $decimalPoints = 2, $commaFormat = false) {
        // $num = floatval(number_format((float)$num, $decimalPoints, '.', '') + 0);
        $num += 0;
        $decimalPoints = strlen(substr(strrchr($num, "."), 1));
        $decimalPoints = !$decimalPoints || $decimalPoints == 1  ? 2 : $decimalPoints;
        $decimalPoints = $decimalPoints < 7 ? $decimalPoints : 6;
        // return $num;
		if ($commaFormat) {
			return number_format((float)$num, $decimalPoints, '.', ',');
		}
		return number_format((float)$num, $decimalPoints, '.', '');
	}
}

if (!function_exists('dateFormat')) {
	function dateFormat($date, $format = 'd/m/Y') {
		$appLocale = Auth::user()->getLocale();
		if ($appLocale == 'en') {
			$format = 'm/d/Y';
		}
		if (!$date) {
			return 'N/A';
		}
		return Carbon::parse($date)->format($format);
	}
}

if (!function_exists('moneyFormat')) {
	function moneyFormat($money = 0.00, $currency_symbol = null, $decimalPoints = 2, $commaFormat = false) {
		if ($commaFormat) {
			return $currency_symbol ? $currency_symbol.addZeros($money, $decimalPoints, true) : Auth::user()->company->currency->symbol.addZeros($money,  $decimalPoints, true);
		}
		return $currency_symbol ? $currency_symbol.addZeros($money, $decimalPoints) : Auth::user()->company->currency->symbol.addZeros($money,  $decimalPoints);
	}
}

if (!function_exists('escapeResponse')) {
	function escapeResponse($collection = []) {
		if ($collection) {
			$collection->map(function($model) {
				if ($model->getAttributes()) {
					return collect($model->getAttributes())->each(function($value, $attribute) use(&$model) {
						$model->{$attribute} = is_string($value) && $value !== '' ? htmlentities($value) :  $value;
					});
				}
				return $model;
            });
		}
		return $collection;
	}
}

if (!function_exists('mapOptionsBasedOnLocale')) {
	function mapOptionsBasedOnLocale($options = []) {
		return collect($options)->map(function($option) {
            return trans($option);
        })->toArray();
	}
}

if (!function_exists('getTodayDate')) {
	function getTodayDate($addDays = 0, $format = 'Y-m-d') {
		$client_tz = $_COOKIE['client_tz'] ?? date_default_timezone_get();
		return Carbon::now($client_tz)->addDays($addDays)->format($format);
	}
}

if (!function_exists('getNextAlphaNumber')) {
	function getNextAlphaNumber($code, $numberOfAlpha = 1) {
		$alphPart = $code[0];
		$numPart = substr($code, 0 + $numberOfAlpha);
		return $alphPart.str_pad(intval($numPart) + 1, strlen($numPart), '0', STR_PAD_LEFT);
	}
}
if (!function_exists('recursiveUnset')) {
	function recursiveUnset(&$array, $unwanted_key) {
        unset($array[$unwanted_key]);
        foreach ($array as &$value) {
            if (is_array($value)) {
                recursiveUnset($value, $unwanted_key);
            }
        }
    }
}

if (!function_exists('diffExistsInCollection')) {
	function diffExistsInCollection($collection1, $collection2) {
		return collect(array_dot($collection1))
				->diffAssoc(collect(array_dot($collection2)))
				->isNotEmpty();
	}
}
if (!function_exists('getAppLocale')) {
	function getAppLocale($request) {
		$locale = app()->getLocale();

		if ($request->user()) {
            $locale = $request->hasHeader("X-localization") ? $request->header("X-localization") : $request->user()->getLocale();
        } else {
            if ($request->wantsJson() || $request->ajax()) {
                $locale = $request->hasHeader("X-localization") ? $request->header("X-localization") : $locale;
            } else {
                $locale = Session::has('locale') ? session('locale') : (in_array($request->input('locale'), array_keys(Config::get('constants.languages'))) ? $request->input('locale') : $locale);
            }
        }
        return $locale;
	}
}
if (!function_exists('arrSumBasedOnSameKey')) {
	function arrSumBasedOnSameKey($a = [], $b = []) {
		$sums = array();
		foreach (array_keys($a + $b) as $key) {
			$sums[$key] = (isset($a[$key]) ? $a[$key] : 0) + (isset($b[$key]) ? $b[$key] : 0);
		}
		return $sums;
	}
}
if (!function_exists('timeFormat')) {
	function timeFormat($time, $format = 'H:i A') {
		return Carbon::parse($time)->format($format);
	}
}
if (!function_exists('getLastDayofMonth')) {
	function getLastDayofMonth($date = null) {
		$date = $date ?: Carbon::now(); //Current Date and Time
		$lastDayofMonth = Carbon::parse($date)->endOfMonth()->toDateString();
		return $lastDayofMonth;
	}
}

if (!function_exists('getFirstDayofMonth')) {
	function getFirstDayofMonth($date = null) {
		$date = $date ?: Carbon::now(); //Current Date and Time
		$lastDayofMonth = Carbon::parse($date)->startOfMonth()->toDateString();
		return $lastDayofMonth;
	}
}

if (!function_exists('computeDepreciation')) {
	function computeDepreciation($method, $cost, $scrapedPer, $years, $maxUnitsProduced = null, $unitsProducedPerYear = []) {
	    $years = intval($years);
	    $result = array_fill(0, $years, 0);

	    $scrapedVal = ($cost * $scrapedPer) / 100;

	    switch ($method) {
	        case 'straight_line':
	            $amount = ($cost - $scrapedVal) / $years;
	            $result = array_fill(0, $years, $amount);
	            break;
	        case 'declined_balance':
	            for ($i=0; $i < $years; $i++) {
	                $cost = abs(($result[$i - 1] ?? 0) - $cost);
	                $result[$i] = (2 * $cost) / $years;
	            }
	            break;
	        case 'sum_of_digits':
	            $sumOfYears = ($years * ($years + 1)) / 2;
	            $cost -= $scrapedVal;
	            for ($i=0; $i < $years; $i++) {
	                $result[$i] = ((($years - $i) * $cost) / $sumOfYears); 
	            }
	            break;
	        case 'units_produced':
	            $cost -= $scrapedVal;
	            $perProductCost = $cost / $maxUnitsProduced;
	            for ($i=0; $i < $years; $i++) {
	                $result[$i] = $unitsProducedPerYear[$i] * $perProductCost;
	            }
	            break;
	        
	        default:
	            throw new Exception("Error Processing Request", 1);
	            break;
	    }
	    return $result;
	}
}
if (!function_exists('mapMonthWithDepreciation')) {
	function mapMonthWithDepreciation($method, $cost, $scrapedPer, $years, $date_of_purchase, $maxUnitsProduced = null, $unitsProducedPerYear = [], $needExtraEntry = true) {
		$current_date = Carbon::parse("2021-07-27")->lastOfMonth();

		$dop = Carbon::parse($date_of_purchase);
        $firstDateOfPurchaseMonth = $dop->copy()->firstOfMonth();
        $dateOfFirstEntry = $dop->copy()->lastOfMonth();

        $totalNumberOfEntries = $years * 12;

        if ($dop != $firstDateOfPurchaseMonth) {
            $totalNumberOfEntries += 1;
        }

        $dateRange = [];
        for ($i = 0; $i < $totalNumberOfEntries; $i++) { 
            $tempDop = $i == 0 ? $dop->copy()->firstOfMonth() : $tempDop->copy()->firstOfMonth();
            $dateRange[$i]['date'] = $tempDop->copy()->lastOfMonth()->format('Y-m-d');
            $dateRange[$i]['amount'] = 0;
            $tempDop->addMonths(1);
        }

        $firstEntryDays = $dop->diffInDays($dateOfFirstEntry) + 1; // why plus one because it not include the from day.

        $deprResult = computeDepreciation($method, $cost, $scrapedPer, $years, $maxUnitsProduced, $unitsProducedPerYear);

        $yearWiseDeprResult = [];
        $totalDeprCalculated = 0;
        $totalDeprShouldBe = array_sum($deprResult);

        $yearWiseDataRange = array_chunk($dateRange, 12, true);
		
        foreach ($yearWiseDataRange as $key => $yearWiseDepr) {
            if (array_key_exists($key, $deprResult)) {
                $amountPerDay = addZeros($deprResult[$key] / 365);
                $amountPerMonth = addZeros($deprResult[$key] / 12);
                $amountPerYear = $deprResult[$key];
                foreach ($yearWiseDepr as $innerKey => &$yearWiseDeprData) {
                    if ($dop != $firstDateOfPurchaseMonth &&
                    	$innerKey == 0 && 
                    	$key == 0) {
                        $amount = round($amountPerDay * $firstEntryDays, 2);
                    } else {
                        // date of purchase on the first day of the month so month wise calculation
                        $amount = $amountPerMonth;
                    }
                    $totalDeprCalculated += $amount;
                    $yearWiseDeprData['amount'] = addZeros($amount);
                }
                $yearWiseDeprResult = array_merge($yearWiseDeprResult, $yearWiseDepr);
            }
        }

	    if ($totalDeprCalculated != $totalDeprShouldBe && $totalDeprCalculated < $totalDeprShouldBe) {
        	if ($needExtraEntry) {
	            $temp = [
	                "date" => Carbon::parse(last($yearWiseDeprResult)['date'])->addMonths(1)->lastOfMonth()->format('Y-m-d'),
	                "amount" => round($totalDeprShouldBe - $totalDeprCalculated, 2)
	            ];
	            array_push($yearWiseDeprResult, $temp);
	        } else {
	        	$yearWiseDeprResult[count($yearWiseDeprResult) - 1]['amount'] += round($totalDeprShouldBe - $totalDeprCalculated, 2);
        	}
        }
        return $yearWiseDeprResult;
	}
}