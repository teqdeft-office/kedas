<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use App\History;

class Company extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($company) {
            $createdata = History::createdata();
            $company->histories()->create($createdata);
        });
        static::updated(function($company){
            $updatedata = History::updatedata();
            $company->histories()->create($updatedata);
        });
        static::deleted(function($company) {
            $deletedata = History::deletedata();
            $company->histories()->create($deletedata);
            $company->roles()->each(function($role) {
                $role->delete();
            });
        });
        
    }

    /**
     * Get the company's logo.
     *
     * @return string
     */
    public function getLogoAttribute(){
        return $this->attributes['logo'] ? Storage::url('profile-pics/'.$this->attributes['logo']) : null;
    }

    /**
     * Get the user's signature image.
     *
     * @return string
     */
    public function getSignatureAttribute(){
        return $this->attributes['signature'] ? Storage::url('signatures/'.$this->attributes['signature']) : null;
    }

    /**
     * Get the owner associated with the owner profile.
     */
    public function owner()
    {
        return $this->hasOne('App\User');
    }

    /**
     * Get the currency associated with the company.
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency', 'currency_id');
    }

    /**
     * Get the address of the company
     */
    public function address()
    {
        return $this->morphOne('App\Address', 'addressadder');
    }

    /**
     * Get the roles associated with the company.
     */
    public function roles()
    {
        // return $this->hasMany('App\Role', 'company_id');
        return $this->hasMany('App\Role', 'company_id')->wherehas('permissions');
    }

    /**
     * Get the users list associate with the company.
     */
    public function users($include_owner = false)
    {
        return $include_owner ? $this->hasMany('App\User', 'company_id', 'id')->where('is_company_emp', false)->get() : $this->hasMany('App\User', 'company_id', 'id')->where('id', '!=', $this->attributes['user_id'])->where('is_company_emp', false)->get();
    }

    /**
     * Get all of the company's clients.
     */
    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    /**
     * Get all of the company's suppliers.
     */
    public function suppliers()
    {
        return $this->hasMany('App\Supplier');
    }

    /**
     * Get all of the company's taxes.
     */
    public function taxes()
    {
        return $this->hasMany('App\Tax');
    }

    /**
     * Get all of the company's item categories.
     */
    public function itemCategories()
    {
        return $this->hasMany('App\ItemCategory');
    }

    /**
     * Get all of the company's inventories.
     */
    public function inventories()
    {
        return $this->hasMany('App\Inventory')->with(['itemCategory']);
    }

    /**
     * Get all of the user's invoices.
     */
    public function invoices()
    {
        return $this->hasMany('App\Invoice')->with(['contact', 'items']);
    }

    /**
     * Get all of the user's transactions.
     */
    public function transactions()
    {
        return $this->hasMany('App\Transaction')->with(['contact']);
    }

    /**
     * Get all of the user's credit notes.
     */
    public function creditNotes()
    {
        return $this->hasMany('App\CreditNote')->with(['contact']);
    }

    /**
     * Get all of the user's estimates.
     */
    public function estimates()
    {
        return $this->hasMany('App\Estimate')->with(['contact']);
    }

    /**
     * Get all of the user's debit notes.
     */
    public function debitNotes()
    {
        return $this->hasMany('App\DebitNote')->with(['contact']);
    }

    /**
     * Get all of the user's Purchase Orders.
     */
    public function purchaseOrders()
    {
        return $this->hasMany('App\PurchaseOrder')->with(['contact']);
    }

    /**
     * Get all of the user's inventory adjustments.
     */
    public function inventoryAdjustments()
    {
        return $this->hasMany('App\InventoryAdjustment');
    }

    /**
     * Get all of the company's ncfs.
     */
    public function ncfs()
    {
        return $this->hasMany('App\Ncf');
    }

    /**
    * Get the users of the store.
    */
    public function clientUsers()
    {
        return $this->belongsToMany('App\User', 'store_user')->withPivot('priority');
    }

    /**
     * Get all of the company's currency exchange rates.
     */
    public function currencyRates()
    {
        return $this->hasMany('App\UserCurrencyRate');
    }

    /**
     * Get all of the company's accounting heads.
     */
    public function accountingHeads()
    {
        return $this->hasMany('App\UserAccountingHead');
    }

    /**
     * Get the sector associated with the company.
     */
    public function sector()
    {
        return $this->belongsTo('App\Sector', 'sector_id');
    }

    /**
     * Get all of the user's estimates.
     */
    public function remissions()
    {
        return $this->hasMany('App\Remission')->with(['contact']);
    }

    /**
     * Get all of the company's employee.
     */
    public function employees()
    {
        return $this->hasMany('App\Employee', 'company_id', 'id')->where('is_company_emp', true);
    }

    /**
     * Get all of the company's department.
     */
    public function departments()
    {
        return $this->hasMany('App\Department');
    }

    /**
     * Get all of the company's designation.
     */
    public function designations()
    {
        return $this->hasMany('App\Designation');
    }

    /**
     * Get all of the company's insurance brands.
     */
    public function insuranceBrands()
    {
        return $this->hasMany('App\InsuranceBrand');
    }

    /**
     * Get all of the company's leave types.
     */
    public function leaveTypes()
    {
        return $this->hasMany('App\LeaveType');
    }

    /**
     * Get all of the company's leave applications.
     */
    public function leaveApplications()
    {
        return $this->hasMany('App\LeaveApplication');
    }

    /**
     * Get all of the company's account adjustments.
     */
    public function accountAdjustments()
    {
        return $this->hasMany('App\AccountAdjustment');
    }

    /**
     * Get all of the company's benefit.
     */
    public function benefits()
    {
        return $this->hasMany('App\benefit');
    }

    /**
     * Get all of the company's WithHolding.
     */
    public function withHoldings()
    {
        return $this->hasMany('App\WithHolding');
    }

    /**
     * Get all of the company's Discount.
     */
    public function discounts()
    {
        return $this->hasMany('App\Discount');
    }

    /**
     * Get all of the company's benefit.
     */
    public function extraHours()
    {
        return $this->hasMany('App\ExtraHour');
    }

    /**
     * Get all of the company's fixedAssets.
     */
    public function fixedAssets()
    {
        return $this->hasMany('App\FixedAsset');
    }

    /**
     * Get all of the company's leave.
     */
    public function leave()
    {
        return $this->hasMany('App\Leave');
    }

    /**
     * Get all of the company's emloyee payment.
     */
    public function employeePayment()
    {
        return $this->hasMany('App\EmployeePayment');
    }

    /**
     * Get all of the company's memberships.
     */
    public function memberships()
    {
        return $this->hasMany('App\Membership');
    }
    public function tables()
    {
        return $this->hasMany('App\RestsurantTables');
    }
    
    /**
     * Get all of the company's  banks.
     */
    public function productionOrder()
    {
        return $this->hasMany('App\Production');
    }

    public function units()
    {
        return $this->hasMany('App\Unit');
    }
    public function histories()
    {
        return $this->hasMany('App\History')->orderBy('id', 'desc');
    }
    public function history()
    {
        return $this->morphMany('App\History', 'record');
    }

    public function banks()
    {
        return $this->hasMany('App\Banks');
    }
    public function shippings()
    {
    return $this->hasMany('App\Shipping');
    }
}
