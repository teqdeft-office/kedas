<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;
use App\Interfaces\CommonConstants;
use App\History;

class Item extends Model implements CommonConstants
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'custom_id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($item) {
            $createdata = History::createdata();
            $item->histories()->create($createdata);
        });
        static::updated(function($item){
            $updatedata = History::updatedata();
            $item->histories()->create($updatedata);
        });
        static::deleted(function($item) {
            $deletedata = History::deletedata();
            $item->histories()->create($deletedata);
            if (App::Environment('local')) {
                $item->forcedelete();
            } else {
                $item->delete();
            }
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Get the inventory associated with the invoice item.
     */
    public function inventory()
    {
        return $this->belongsTo('App\Inventory', 'inventory_id');
    }

    /**
     * Get the tax associated with the invoice item.
     */
    public function tax()
    {
        return $this->belongsTo('App\Tax', 'tax_id');
    }

    public function getCalculationAttribute() {
        $price = $this->attributes['price'] ?: $this->inventory->sale_price;
        $tax = $this->tax ? $this->tax->percentage : 0;
        $discount = $this->attributes['discount'];
        $quantity = $this->attributes['quantity'];
        
        return getItemCost($price, $quantity, $discount, $this->tax);
    }

    public function getCustomIdAttribute() {
        return self::INVENTORY .'-'. $this->attributes['id'];
    }
}
