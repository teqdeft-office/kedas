<?php

namespace App\Listeners;

use App\Events\SaleInvoiceCreatedOrUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\AccountingEntry;
use App\AccountingHead;
use App\Interfaces\CommonConstants;
use App;

class CreateAccountingEntries implements CommonConstants
{
    /**
     * The name of the connection the job should be sent to.
     *
     * @var string|null
     */
    public $connection = 'sync';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SaleInvoiceCreated  $event
     * @return void
     */
    public function handle($event)
    {
        $type = $event->type ?? self::INVOICE;

        // if (App::Environment(['production'])) {
        //     return true;
        // }
        // return true;
        try {
            switch ($type) {
                case self::SALEINVOICE:
                    $invoice = $event->invoice;
                    $invoice->load('items');

                    // Fetch the accounting head for this entry.
                    $companyAccountingHeads = $invoice->company->accountingHeads()->whereIn('code', [self::ACCOUNTSRECEIVABLECODE, self::INVENTORYCODE, self::COGSCODE])->pluck('id', 'code')->toArray();

                    $accountingEntries = collect([]);
                    $inventoryEntries = collect([]);

                    $accountingEntries->push([
                        'amount' => $invoice->calculation['total'],
                        'user_accounting_head_id' => $companyAccountingHeads[self::ACCOUNTSRECEIVABLECODE],
                        'nature' => 'debit'
                    ]);

                    foreach ($invoice->items as $key => $item) {
                        $accountingEntries->push([
                            'amount' => $item->calculation['subtotal'],
                            'user_accounting_head_id' => $item->inventory->income_account,
                            'nature' => 'credit'
                        ]);
                        if ($item->tax) {
                            $accountingEntries->push([
                                'amount' => $item->calculation['taxAmount'],
                                'user_accounting_head_id' => $item->tax->sales_account,
                                'nature' => 'credit'
                            ]); 
                        }

                        if ($item->inventory->track_inventory) {
                            $inventoryEntries->push([
                                'amount' => $item->inventory->unit_cost * $item->quantity, 
                                'user_accounting_head_id' => $companyAccountingHeads[self::INVENTORYCODE], 
                                'nature' => 'credit'
                            ]);
                            $inventoryEntries->push([
                                'amount' => $item->inventory->unit_cost * $item->quantity, 
                                'user_accounting_head_id' => $companyAccountingHeads[self::COGSCODE], 
                                'nature' => 'debit'
                            ]);
                        }
                    }

                    $accountingEntries = $accountingEntries->concat($inventoryEntries);

                    $accountingEntries = $accountingEntries->groupBy(function ($item) {
                        return $item['user_accounting_head_id'].'-'.$item['nature'];
                    })->mapWithKeys(function ($grouped, $key) {
                        return [
                            $key =>
                                [
                                    'amount' => $grouped->sum('amount'),
                                    'user_accounting_head_id' => $grouped->first()['user_accounting_head_id'],
                                    'nature' => $grouped->first()['nature']
                                ]
                        ];
                    })->values()->map(function($item, $key) {
                        $item['order'] = $key + 1;
                        return $item;
                    });

                    $debitCreditAmount = $accountingEntries->groupBy('nature')->map(function ($row) {
                        return $row->sum('amount');
                    });

                    // Check if the entry is valid or not.
                    if ($debitCreditAmount->isNotEmpty() && $debitCreditAmount['credit'] == $debitCreditAmount['debit']) {
                        if ($invoice->accountingEntries->isNotEmpty()) {
                            $invoice->load('accountingEntries');
                            $mapExistingEntries = $invoice->accountingEntries->groupBy(function ($item) {
                                return $item['user_accounting_head_id'].'-'.$item['nature'];
                            })->mapWithKeys(function ($grouped, $key) {
                                return [
                                    $key =>
                                        [
                                            'amount' => $grouped->sum('amount'),
                                            'user_accounting_head_id' => $grouped->first()['user_accounting_head_id'],
                                            'nature' => $grouped->first()['nature']
                                        ]
                                ];
                            })->values()->map(function($item, $key) {
                                $item['order'] = $key + 1;
                                return $item;
                            });
                            $diffExists = diffExistsInCollection($accountingEntries, $mapExistingEntries);
                            if ($diffExists) {
                                $invoice->accountingEntries()->forcedelete();
                                $invoice->accountingEntries()->createMany($accountingEntries);
                            }
                        } else {
                            $invoice->accountingEntries()->createMany($accountingEntries);
                        }
                    } else {
                        throw new \Exception(trans('messages.invalid_account_entry'), 500);
                    }
                    break;
                
                case self::INVENTORYADJUSTMENT:
                    $inventoryAdjustment = $event->inventoryAdjustment;

                    // prePrint($inventoryAdjustment->company);
                    $companyAccountingHeads = $inventoryAdjustment->company->accountingHeads()->whereIn('code', [self::INVENTORYCODE, self::INVENTORYADJUSTMENTSCODE])->pluck('id', 'code')->toArray();

                    if ($companyAccountingHeads) {
                        $accountingEntries = collect([]);

                        if ($inventoryAdjustment->total < 0) {
                            $accountingEntries = $accountingEntries->concat(collect([
                                ['amount' => abs($inventoryAdjustment->total), 'order' => 1, 'user_accounting_head_id' => $companyAccountingHeads[self::INVENTORYADJUSTMENTSCODE], 'nature' => 'debit'],
                                ['amount' => abs($inventoryAdjustment->total), 'order' => 2, 'user_accounting_head_id' => $companyAccountingHeads[self::INVENTORYCODE], 'nature' => 'credit']
                            ]));
                        } else {
                            // echo $inventoryAdjustment->id;
                            // echo "<br>";
                            $accountingEntries = $accountingEntries->concat(collect([
                                ['amount' => abs($inventoryAdjustment->total), 'order' => 1, 'user_accounting_head_id' => $companyAccountingHeads[self::INVENTORYCODE], 'nature' => 'debit'],
                                ['amount' => abs($inventoryAdjustment->total), 'order' => 2, 'user_accounting_head_id' => $companyAccountingHeads[self::INVENTORYADJUSTMENTSCODE], 'nature' => 'credit']
                            ]));
                        }

                        $debitCreditAmount = $accountingEntries->groupBy('nature')->map(function ($row) {
                            return $row->sum('amount');
                        });

                        // Check if the entry is valid or not.
                        if ($debitCreditAmount->isNotEmpty() && $debitCreditAmount['credit'] == $debitCreditAmount['debit']) {
                            if ($inventoryAdjustment->accountingEntries->isEmpty()) {
                                // going to create new entries.
                                $inventoryAdjustment->accountingEntries()->createMany($accountingEntries);
                            } else {
                                // going to update the entries.
                                foreach ($inventoryAdjustment->accountingEntries as $accountingEntry) {
                                    $newEntryInfo = $accountingEntries->where('order', $accountingEntry->order)->first();
                                    $accountingEntry->amount = $newEntryInfo['amount'];
                                    $accountingEntry->user_accounting_head_id = $newEntryInfo['user_accounting_head_id'];
                                    $accountingEntry->nature = $newEntryInfo['nature'];
                                    $accountingEntry->save();
                                }
                            }
                        } else {
                            throw new \Exception(trans('messages.invalid_account_entry'), 500);
                        }
                    }

                    break;

                case self::PAYMENT:
                    $transaction = $event->transaction;

                    // It means we are dealing with new records only.
                    if ($transaction->bank_account) {

                        $companyAccountingHeads = $transaction->company->accountingHeads()->whereIn('code', [self::ACCOUNTSRECEIVABLECODE, self::ACCOUNTSPAYABLECODE])->pluck('id', 'code')->toArray();

                        $accountingEntries = collect([]);
                        if ($transaction->type == 'in') {
                            $accountingEntries = $accountingEntries->concat(collect([
                                ['amount' => $transaction->amount, 'order' => 1, 'user_accounting_head_id' => $transaction->bank_account, 'nature' => 'debit'],
                                ['amount' => $transaction->amount, 'order' => 2, 'user_accounting_head_id' => $companyAccountingHeads[self::ACCOUNTSRECEIVABLECODE], 'nature' => 'credit']
                            ]));
                        } else {
                            if ($transaction->transaction_with_invoice) {
                                $accountingEntries = $accountingEntries->concat(collect([
                                    ['amount' => $transaction->amount, 'order' => 1, 'user_accounting_head_id' => $companyAccountingHeads[self::ACCOUNTSPAYABLECODE], 'nature' => 'debit'],
                                    ['amount' => $transaction->amount, 'order' => 2, 'user_accounting_head_id' => $transaction->bank_account, 'nature' => 'credit']
                                ]));
                            } else {
                                foreach ($transaction->conceptItems as $key => $conceptItem) {
                                    $accountingEntries->push([
                                        'amount' => $conceptItem->calculation['subtotal'],
                                        'user_accounting_head_id' => $conceptItem->user_accounting_head_id,
                                        'nature' => 'debit'
                                    ]);

                                    if ($conceptItem->tax) {
                                        $accountingEntries->push([
                                            'amount' => $conceptItem->calculation['taxAmount'],
                                            'user_accounting_head_id' => $conceptItem->tax->purchases_account,
                                            'nature' => 'debit'
                                        ]); 
                                    }
                                }

                                $accountingEntries->push([
                                    'amount' => $transaction->amount,
                                    'user_accounting_head_id' => $transaction->bank_account,
                                    'nature' => 'credit'
                                ]);
                            }
                        }



                        $debitCreditAmount = $accountingEntries->groupBy('nature')->map(function ($row) {
                            return $row->sum('amount');
                        });

                        // Check if the entry is valid or not.
                        if ($debitCreditAmount->isNotEmpty() && strval($debitCreditAmount['credit']) == strval($debitCreditAmount['debit'])) {
                            $accountingEntries = $accountingEntries->map(function($item, $key) {
                                $item['order'] = $key + 1;
                                return $item;
                            });

                            if ($transaction->accountingEntries->isNotEmpty()) {
                                $transaction->load('accountingEntries');
                                $mapExistingEntries = $transaction->accountingEntries->groupBy(function ($item) {
                                    return $item['user_accounting_head_id'].'-'.$item['nature'];
                                })->mapWithKeys(function ($grouped, $key) {
                                    return [
                                        $key =>
                                            [
                                                'amount' => $grouped->sum('amount'),
                                                'user_accounting_head_id' => $grouped->first()['user_accounting_head_id'],
                                                'nature' => $grouped->first()['nature']
                                            ]
                                    ];
                                })->values()->map(function($item, $key) {
                                    $item['order'] = $key + 1;
                                    return $item;
                                });
                                $diffExists = diffExistsInCollection($accountingEntries, $mapExistingEntries);
                                if ($diffExists) {
                                    $transaction->accountingEntries()->forcedelete();
                                    $transaction->accountingEntries()->createMany($accountingEntries);
                                }
                            } else {
                                $transaction->accountingEntries()->createMany($accountingEntries);
                            }
                        } else {
                            throw new \Exception(trans('messages.invalid_account_entry'), 500);
                        }
                    }
                    break;

                case self::SUPPLIERINVOICE:
                    $invoice = $event->invoice;
                    $invoice->load('items');
                    $invoice->load('conceptItems');

                    $companyAccountingHeads = $invoice->company->accountingHeads()->whereIn('code', [self::INVENTORYCODE, self::ACCOUNTSPAYABLECODE])->pluck('id', 'code')->toArray();

                    $accountingEntries = collect([]);
                    $inventoryEntries = collect([]);

                    foreach ($invoice->conceptItems as $key => $conceptItem) {
                        $accountingEntries->push([
                            'amount' => $conceptItem->calculation['subtotal'],
                            'user_accounting_head_id' => $conceptItem->user_accounting_head_id,
                            'nature' => 'debit'
                        ]);
                        if ($conceptItem->tax) {
                            $accountingEntries->push([
                                'amount' => $conceptItem->calculation['taxAmount'],
                                'user_accounting_head_id' => $conceptItem->tax->purchases_account,
                                'nature' => 'debit'
                            ]); 
                        }
                    }

                    foreach ($invoice->items as $key => $item) {
                        $accountingEntries->push([
                            'amount' => $item->calculation['subtotal'],
                            'user_accounting_head_id' => $item->inventory->expense_account ?? $companyAccountingHeads[self::INVENTORYCODE],
                            'nature' => 'debit'
                        ]);
                        if ($item->tax) {
                            $accountingEntries->push([
                                'amount' => $item->calculation['taxAmount'],
                                'user_accounting_head_id' => $item->tax->purchases_account,
                                'nature' => 'debit'
                            ]); 
                        }
                    }

                    $accountingEntries->push([
                        'amount' => $invoice->calculation['total'],
                        'user_accounting_head_id' => $companyAccountingHeads[self::ACCOUNTSPAYABLECODE],
                        'nature' => 'credit'
                    ]);

                    $accountingEntries = $accountingEntries->map(function($item, $key) {
                        $item['order'] = $key + 1;
                        return $item;
                    });

                    $debitCreditAmount = $accountingEntries->groupBy('nature')->map(function ($row) {
                        return $row->sum('amount');
                    });

                    // Check if the entry is valid or not.
                    if ($debitCreditAmount->isNotEmpty() && $debitCreditAmount['credit'] == $debitCreditAmount['debit']) {
                        if ($invoice->accountingEntries->isNotEmpty()) {
                            $invoice->load('accountingEntries');
                            $mapExistingEntries = $invoice->accountingEntries->map(function($item, $key) {
                                $item['order'] = $key + 1;
                                return $item;
                            });
                            $diffExists = diffExistsInCollection($accountingEntries, $mapExistingEntries);
                            if ($diffExists) {
                                $invoice->accountingEntries()->forcedelete();
                                $invoice->accountingEntries()->createMany($accountingEntries);
                            }
                        } else {
                            $invoice->accountingEntries()->createMany($accountingEntries);
                        }
                    } else {
                        throw new \Exception(trans('messages.invalid_account_entry'), 500);
                    }

                    break;

                case self::INVENTORY:
                    $inventory = $event->inventory;

                    $companyAccountingHeads = $inventory->company->accountingHeads()->whereIn('code', [self::INITADJINVENTORY])->pluck('id', 'code')->toArray();
                    if ($companyAccountingHeads) {

                        $accountingEntries = collect([]);

                        $accountingEntries = $accountingEntries->concat(collect([
                            ['amount' => $inventory->unit_cost * $inventory->init_quantity, 'order' => 1, 'user_accounting_head_id' => $inventory->asset_account, 'nature' => 'debit'],
                            ['amount' => $inventory->unit_cost * $inventory->init_quantity, 'order' => 2, 'user_accounting_head_id' => $companyAccountingHeads[self::INITADJINVENTORY], 'nature' => 'credit']
                        ]));

                        $debitCreditAmount = $accountingEntries->groupBy('nature')->map(function ($row) {
                            return $row->sum('amount');
                        });

                        // Check if the entry is valid or not.
                        if ($debitCreditAmount->isNotEmpty() && $debitCreditAmount['credit'] == $debitCreditAmount['debit']) {
                            if ($inventory->accountingEntries->isEmpty()) {
                                // going to create new entries.
                                $inventory->accountingEntries()->createMany($accountingEntries);
                            } else {
                                // going to update the entries.
                                foreach ($inventory->accountingEntries as $accountingEntry) {
                                    $newEntryInfo = $accountingEntries->where('order', $accountingEntry->order)->first();
                                    $accountingEntry->amount = $newEntryInfo['amount'];
                                    $accountingEntry->user_accounting_head_id = $newEntryInfo['user_accounting_head_id'];
                                    $accountingEntry->nature = $newEntryInfo['nature'];
                                    $accountingEntry->save();
                                }
                            }
                        } else {
                            throw new \Exception(trans('messages.invalid_account_entry'), 500);
                        }
                    }
                    break;

                default:
                    # code...
                    break;
            }
            
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Determine whether the listener should be queued.
     *
     * @param  \App\Events\OrderPlaced  $event
     * @return bool
     */
    public function shouldQueue(SaleInvoiceCreatedOrUpdated $event)
    {
        return false;
    }
}
