<?php

namespace App\Listeners;

use App\Events\ProfileCompleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Company;

class CreateCompanyRoles
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProfileCompleted  $event
     * @return void
     */
    public function handle(ProfileCompleted $event)
    {
        try {
            $user = $event->user;
            if ($user->company_id && $user->isCompanyOwner()) {
                // As we don't need to create role, the user can create their own roles.
                /*$company = Company::findOrFail($user->company_id);
                if ($company->roles->isEmpty()) {
                    $company->roles()->createMany([
                        ['name' => 'admin', 'custom_name' => ["en" => "admin", "es" => "administración"], 'company_id' => $company->id],
                        ['name' => 'manager', 'custom_name' => ["en" => "manager", "es" => "gerente"], 'company_id' => $company->id],
                        ['name' => 'employee', 'custom_name' => ["en" => "employee", "es" => "empleada"], 'company_id' => $company->id]
                    ]);
                }*/
            }
            return true;
        } catch (\Exception $e) {
            throw $e;
            return false;
        }
    }
}
