<?php
namespace App\Services;

use App\Services\Service;

use Illuminate\Http\Request;

use App\Transaction;
use App\Tax;
use App\ConceptItem;

use App\Validators\TransactionValidator;

use App\Exceptions\ServiceException;

use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionCreated;
use App\Events\PaymentCreatedOrUpdated;

class TransactionService extends Service
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        try {
            $input = $request->all();

            $user = $request->user();
            $contact = null;
            $transaction_with_invoice = $input['transaction_with_invoice'] ?? 0;

            if ($transaction_with_invoice) {
                $paymentItems = collect($input['payments'])->filter(function($row) {
                    return $row['invoice_id'] && (isset($row['amount']) && $row['amount'] > 0);
                })->values();
            } else {
                $paymentItems = collect($input['concepts'])->filter(function($row) {
                    return $row['item'] && $row['quantity'] && (isset($row['price']) && $row['price'] > 0);
                })->values();
            }

            if ($input['contact_id']) {
                list($contact_type, $contact_id) = explode('-', $input['contact_id']);
                $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

                if (!$contact) {
			    	throw new ServiceException(trans('validation.exists', ['attribute' => trans('labels.contact')]), 400);
			    }
            }

            if ($paymentItems->isEmpty()) {
			    throw new ServiceException(trans('messages.transaction_without_items'), 400);
            }

            if ($transaction_with_invoice) {
                $pluckedInvoices = $contact->invoices->pluck('calculation', 'id')->map(function($value, $key) {
                    return $value['pendingAmount'];
                })->toArray();

                $amountCorrect = $paymentItems->every(function ($value, $key) use ($pluckedInvoices) {
                    return $value['amount'] <= $pluckedInvoices[$value['invoice_id']];
                });

                if (!$amountCorrect) {
			    	throw new ServiceException(trans('messages.transaction_with_incorrect_amount'), 400);
                }

                $total_amount = $paymentItems->sum('amount');
            } else {
                foreach ($paymentItems as $key => $paymentItem) {
                    $tax = $paymentItem['tax'] ? Tax::findOrFail($paymentItem['tax']) : null;
                    $paymentItem['amount'] = getItemCost($paymentItem['price'], $paymentItem['quantity'], $paymentItem['discount'], $tax)['total'];
                    $paymentItems[$key] = $paymentItem;
                }
                $total_amount = $paymentItems->sum('amount');
            }

            $dataToInsert = [
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['start_date'],
                'end_date' => @$input['end_date'],
                'transaction_with_invoice' => $transaction_with_invoice,
                'type' => $type,
                'amount' => $total_amount,
                // 'account' => @$input['payment_account'],
                'frequency' => @$input['frequency'],
                'frequency_type' => @$input['frequency_type'],
                'method' => @$input['payment_method'],
                'annotation' => @$input['annotation'],
                'observation' => @$input['observation'],
                'bank_account' => $input['bank_account'],
            ];

            if ($contact) {
                $transaction = $contact->transactions()->create($dataToInsert);
            } else {
                $transaction = Transaction::create($dataToInsert);
            }

            if ($transaction_with_invoice) {
                $transaction->invoices()->attach($paymentItems->keyBy('invoice_id')->toArray());
            } else {
                $paymentItems = $paymentItems->map(function($item) {
                    list($item_type, $item_id) = explode('-', $item['item']);
                    $item['user_accounting_head_id'] = $item_id;
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    return $item;
                })->values()->toArray();
                $transaction->conceptItems()->createMany($paymentItems);
            }
           
            /* upload documents */
            if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                	// For apis
		        	if ($this->isApiCall) {
			            $name = 'transaction-' . uniqid() . '.' . $file->extension();
			            $file->move($dir, $name);
			            $documents_data[] = [
			                'name' => $name,
			            ];
		        	} else {
	                    if (File::exists(storage_path('tmp/'.$file))) {
	                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/transaction-'.$file));
	                        $documents_data[] = [
	                            'name' => 'transaction-'.$file,
	                        ];
	                    }
		        	}
                }

                $documents = $transaction->documents()->createMany($documents_data);
            }

            if ($transaction->type == 'in') {
                Mail::to($user->email)->send(new TransactionCreated($transaction));
            }

            if ($transaction->type != self::RECURRING) {
                event(new PaymentCreatedOrUpdated($transaction));
            }
        } catch (ServiceException | Exception $e) {
		    throw $e;
    	}

        return $transaction;    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        try {
	        $type = $transaction->type;
	        $input = $request->all();
            $user = $request->user();
            $contact = null;
            $transaction_with_invoice = $input['transaction_with_invoice'] ?? 0;

            if ($transaction_with_invoice) {
                $paymentItems = collect($input['payments'])->filter(function($row) {
                    return $row['invoice_id'] && (isset($row['amount']) && $row['amount'] > 0);
                })->values();
            } else {
                $paymentItems = collect($input['concepts'])->filter(function($row) {
                    return $row['item'] && $row['quantity'] && (isset($row['price']) && $row['price'] > 0);
                })->values();
            }

            if ($input['contact_id']) {
                list($contact_type, $contact_id) = explode('-', $input['contact_id']);
                $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

                if (!$contact) {
			    	throw new ServiceException(trans('validation.exists', ['attribute' => trans('labels.contact')]), 400);
                }
            }

            if ($paymentItems->isEmpty()) {
			    throw new ServiceException(trans('messages.transaction_without_items'), 400);
            }

            if ($transaction_with_invoice) {
                $pluckedInvoices = $contact->invoices->pluck('calculation', 'id')->map(function($value, $key) {
                    return $value['pendingAmount'];
                })->toArray();

                $pluckedAmount = $transaction->invoices->pluck('pivot')->pluck('amount', 'invoice_id')->toArray();

                $amountCorrect = $paymentItems->every(function ($value, $key) use ($pluckedInvoices, $pluckedAmount) {
                    return $value['amount'] <= (($pluckedAmount[$value['invoice_id']] ?? 0) + $pluckedInvoices[$value['invoice_id']]);
                });

                if (!$amountCorrect) {
			    	throw new ServiceException(trans('messages.transaction_with_incorrect_amount'), 400);
                }

                $total_amount = $paymentItems->sum('amount');
            } else {
                foreach ($paymentItems as $key => $paymentItem) {
                    $tax = $paymentItem['tax'] ? Tax::findOrFail($paymentItem['tax']) : null;
                    $paymentItem['amount'] = getItemCost($paymentItem['price'], $paymentItem['quantity'], $paymentItem['discount'], $tax)['total'];
                    $paymentItems[$key] = $paymentItem;
                }
                $total_amount = $paymentItems->sum('amount');

                $paymentItems = $paymentItems->map(function($item) {
                    list($item_type, $item_id) = explode('-', $item['item']);
                    $item['user_accounting_head_id'] = $item_id;
                    $item['tax_id'] = $item['tax'] ?: null;

                    if ($item['id']) {
                        list($row_type, $row_id) = explode('-', $item['id']);
                        $item['id'] = ($row_type === self::CONCEPT) ? $row_id : 0;
                    }
                    unset($item['item']);
                    unset($item['tax']);
                    return $item;
                })->values();

                $updateConceptItems = $paymentItems->filter(function($row) {
                    return $row['id'];
                })->values();

                $insertConceptItems = $paymentItems->filter(function($row) {
                    return !$row['id'];
                })->map(function($item) {
                    unset($item['id']);
                    return $item;
                })->values()->toArray();

                $deleteConceptItemsExcept = $updateConceptItems->pluck('id')->toArray();

                if ($paymentItems->isEmpty()) {
			    	throw new ServiceException(trans('messages.transaction_without_items'), 400);
                }
            }

            if ($contact) {
                $transaction->contact()->associate($contact);
            } else {
                $transaction->contact()->dissociate();
            }
            
            $dataToUpdate = [
                'start_date' => $input['start_date'],
                'end_date' => @$input['end_date'],
                'amount' => $total_amount,
                'frequency' => @$input['frequency'],
                'frequency_type' => @$input['frequency_type'],
                // 'account' => @$input['payment_account'],
                'method' => @$input['payment_method'],
                'annotation' => @$input['annotation'],
                'observation' => @$input['observation'],
                'transaction_with_invoice' => $transaction_with_invoice,
                'bank_account' => $input['bank_account'],
            ];

            $transaction->update($dataToUpdate);

            /* update documents start */
            $documents = $input['documents'] ?? [];
            $deleted_documents_ids = $input['deleted_documents_ids'] ?? [];

            if ($this->isApiCall) {

	            if ($deleted_documents_ids) {
	                $transaction->documents()->whereIn('id', $deleted_documents_ids)->delete();
	            }
	        
	            if ($documents) {
	                $dir = public_path("storage/documents");
	                if ( !\File::isDirectory($dir) ) { 
	                    \File::makeDirectory($dir, 493, true);
	                }

	                foreach ($documents as $file) {
	                    $name = 'transaction-' . uniqid() . '.' . $file->extension();
	                    $file->move($dir, $name);
	                    $documents_data[] = [
	                        'name' => $name,
	                    ];
	                }
	                $transaction->documents()->createMany($documents_data);
	            }

            } else {
	            if ($transaction->documents->isNotEmpty()) {
	                foreach ($transaction->documents as $document) {
	                    if (!in_array($document->name, $documents)) {
	                        $transaction->documents()->where('id', '=', $document->id)->delete();
	                    }
	                }
	            }
	        
	            $alreadyExistDocuments = $transaction->documents->pluck('name')->toArray();

	            $dir = public_path("storage/documents");

	            foreach ($request->input('documents', []) as $file) {
	            
	                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
	                    if (File::exists(storage_path('tmp/'.$file))) {
	                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/transaction-'.$file));
	                        $document_data = array('name' => 'transaction-'.$file);
	                        $transaction->documents()->create($document_data);
	                    }
	                }
	            }
            }

            /* update documents end */

            if ($transaction_with_invoice) {
                $transaction->invoices()->sync($paymentItems->keyBy('invoice_id')->toArray());
                $transaction->conceptItems()->delete();
            } else {
                if ($deleteConceptItemsExcept) {
                    $transaction->conceptItems()->whereNotIn('id', $deleteConceptItemsExcept)->delete();
                }

                if ($insertConceptItems) {
                    $transaction->conceptItems()->createMany($insertConceptItems);
                }

                if ($updateConceptItems) {
                    foreach ($updateConceptItems as $updateConceptItem) {
                        $updatedData = $updateConceptItem;
                        unset($updatedData['id']);
                        ConceptItem::findOrFail($updateConceptItem['id'])->update($updatedData);
                    }
                }
                $transaction->invoices()->sync([]);
            }

            if ($transaction->type != self::RECURRING) {
                event(new PaymentCreatedOrUpdated($transaction));
            }

        } catch (ServiceException | Exception $e) {
		    throw $e;
    	}

        return $transaction; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Transaction $transaction)
    {
    	try {
            $transaction->delete();
        } catch (ServiceException | Exception $e) {
		    throw $e;
    	}
    }
}