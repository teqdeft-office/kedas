<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class InternationalChar implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($multiline = false, $specialChar = false)
    {
        $this->multiline = $multiline;
        $this->specialChar = $specialChar;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $internationalChar = "a-zA-Z0-9";

        if ($this->specialChar) {
            $internationalChar .= "\-\–_’'‘\[\],.\(\)&><|\"";
        }
        
        $internationalChar .= " "; // add space
        $internationalChar .= "ÆÐƎƏƐƔĲŊŒẞÞǷȜæðǝəɛɣĳŋœĸſßþƿȝĄƁÇĐƊĘĦĮƘŁØƠŞȘŢȚŦŲƯY̨Ƴąɓçđɗęħįƙłøơşșţțŧųưy̨ƴÁÀÂÄǍĂĀÃÅǺĄÆǼǢƁĆĊĈČÇĎḌĐƊÐÉÈĖÊËĚĔĒĘẸƎƏƐĠĜǦĞĢƔáàâäǎăāãåǻąæǽǣɓćċĉčçďḍđɗðéèėêëěĕēęẹǝəɛġĝǧğģɣĤḤĦIÍÌİÎÏǏĬĪĨĮỊĲĴĶƘĹĻŁĽĿʼNŃN̈ŇÑŅŊÓÒÔÖǑŎŌÕŐỌØǾƠŒĥḥħıíìiîïǐĭīĩįịĳĵķƙĸĺļłľŀŉńn̈ňñņŋóòôöǒŏōõőọøǿơœŔŘŖŚŜŠŞȘṢẞŤŢṬŦÞÚÙÛÜǓŬŪŨŰŮŲỤƯẂẀŴẄǷÝỲŶŸȲỸƳŹŻŽẒŕřŗſśŝšşșṣßťţṭŧþúùûüǔŭūũűůųụưẃẁŵẅƿýỳŷÿȳỹƴźżžẓ";

        if ($this->multiline) {
            // $internationalChar .= "\n";
            $regex = "/^[$internationalChar]+$/m";
        } else {
            $regex = "/^[$internationalChar]+$/s";
        }

        return preg_match($regex, $value, $match);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.international_char');
    }
}
