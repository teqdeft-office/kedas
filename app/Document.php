<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;
use File;
use Auth;
use Storage;
use App\History;

class Document extends Model
{
    use SoftDeletes;
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['size', 'ext', 'url'];

    public function __construct(array $attributes = array())
    {
        $this->attributes['uploaded_by'] = Auth::id();
        parent::__construct($attributes);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($document) {
            $createdata = History::createdata();
            $document->histories()->create($createdata);
        });
        static::updated(function($document){
            $updatedata = History::updatedata();
            $document->histories()->create($updatedata);
        });
        static::deleted(function($document) {
            $deletedata = History::deletedata();
            $document->histories()->create($deletedata);
            if (App::Environment('local')) {
                $document->forcedelete();
            } else {
                $document->delete();
            }
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Set the document's uploaded_by.
     *
     * @param  string  $value
     * @return void
     */
    public function setUploadedByAttribute($value) {
        $this->attributes['uploaded_by'] = Auth::id();
    }

    /**
     * Get the document's url.
     *
     * @return string
     */
    public function getUrlAttribute(){
        return Storage::url('documents/'.$this->attributes['name']);
    }

    /**
     * Get the owning documentable model.
     */
    public function documentable()
    {
        return $this->morphTo();
    }

    /**
     * size of a document.
     */
    public function getSizeAttribute() {
        $fileSize =  $this->attributes['name'] ? File::size(public_path('storage/documents/'.$this->attributes['name'])) : null;
        return $fileSize;
    }

    /**
     * extension of a document.
     */
    public function getExtAttribute() {
        $ext = $this->attributes['name'] ? pathinfo(public_path('storage/documents/'.$this->attributes['name']), PATHINFO_EXTENSION) : null;
        return $ext;
    }

    /**
     * Get the document's owner.
     *
     * @return string
     */
    public function owner(){
        return $this->belongsTo('App\User', 'uploaded_by');
    }

}
