<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Interfaces\CommonConstants;
use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;


class History extends Model implements CommonConstants
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'histories';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public static function createdata(){
        $user = Auth::user();
        if($user->company) {
            $next_number = $user->company->histories()->withTrashed() ? $user->company->histories()->withTrashed()->where('user_id', $user->id)->count() + 1 : 1;
        }
        return $data = [
            'user_id' => $user->id,
            'company_id' => $user->company->id ?? null,
            'action_type' => 'create',
            'internal_number' => $next_number ?? 0
        ];
    }

    public static function updatedata(){
        $user = Auth::user();
        if($user->company) {
            $next_number = $user->company->histories()->withTrashed() ? $user->company->histories()->withTrashed()->where('user_id', $user->id)->count() + 1 : 1;
        }
        return $data = [
            'user_id' => $user->id,
            'company_id' => $user->company->id ?? null,
            'action_type' => 'update',
            'internal_number' => $next_number ?? 0
        ];
    }

    public static function deletedata(){
        $user = Auth::user();
        if($user->company) {
            $next_number = $user->company->histories()->withTrashed() ? $user->company->histories()->withTrashed()->where('user_id', $user->id)->count() + 1 : 1;
        }
        return $data = [
            'user_id' => $user->id,
            'company_id' => $user->company->id ?? null,
            'action_type' => 'delete',
            'internal_number' => $next_number ?? 0
        ];
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {

        static::creating(function ($history) {
            $user = Auth::user();
            // $next_number = $user->company->history()->withTrashed() ? $user->company->history()->withTrashed()->count() + 1 : 1;
            // $history->internal_number = $next_number;
        });

        static::deleted(function($history) {
            $history->conceptItems()->delete();
            $history->items()->delete();
        });
    }

    /**
     * Get the owning register model.
     */
    public function record()
    {
        return $this->morphTo();
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

        /**
     * Get the user associated with the invoice.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
