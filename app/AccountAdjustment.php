<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountAdjustment extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_adjustments';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['debit_credit_amount', 'total'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($inventoryAdjustment) {
            $inventoryAdjustment->documents()->delete();
            $inventoryAdjustment->accountingEntries()->delete();
        });
    }

    public function getTotalAttribute() 
    {
        return $this->getDebitCreditAmountAttribute()['debit'];
    }

    /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * Get the accounting entries of the invoice.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function getDebitCreditAmountAttribute() {
        return $debitCreditAmount = $this->accountingEntries->groupBy('nature')->map(function ($row) {
            return $row->sum('amount');
        });
    }
}
