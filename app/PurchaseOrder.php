<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\History;

class PurchaseOrder extends Model
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'purchase_orders';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'exchange_rate_total'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
   
    protected static function booted()
    {
        static::creating(function ($purchaseOrder) {
            $user = Auth::user();
            $next_number = $user->company->purchaseOrders()->withTrashed() ? $user->company->purchaseOrders()->withTrashed()->count() + 1 : 1;
            $purchaseOrder->internal_number = $next_number;
        });
        static::created(function ($purchaseOrder) {
            $createdata = History::createdata();
            $purchaseOrder->histories()->create($createdata);
        });
        static::updated(function($purchaseOrder){
            $updatedata = History::updatedata();
            $purchaseOrder->histories()->create($updatedata);
        });
        static::deleted(function($purchaseOrder) {
            $deletedata = History::deletedata();
            $purchaseOrder->histories()->create($deletedata);
            $purchaseOrder->conceptItems()->delete();
            $purchaseOrder->items()->delete();
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * List of concepts items associated with a purchase order.
     */
    public function conceptItems()
    {
        return $this->hasMany('App\ConceptItem')->with(['tax', 'concept', 'userAccountingHead']);
    }

    /**
     * List of items associated with a purchase order.
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with(['tax', 'inventory']);
    }

    public function getCalculationAttribute() {

        $pluckedCalculation = $this->conceptItems->pluck('calculation')->merge($this->items->pluck('calculation'));

        $invoiceSum = array();
        $taxInfo = array();
        $taxInfoArr = array();
        foreach ($pluckedCalculation as $plucked) {
            foreach ($plucked as $sub_key => $value) {
                if (!is_array($value)) {
                    if( ! array_key_exists($sub_key, $invoiceSum)) $invoiceSum[$sub_key] = 0;
                    $invoiceSum[$sub_key]+=$value;
                } else {
                    array_push($taxInfoArr, $value);
                }
            }
        }
        $taxInfoArr = collect($taxInfoArr)->filter()->groupBy('tax_id')->values()->toArray();

        if ($taxInfoArr) {
            foreach ($taxInfoArr as $taxInfoEle) {
                $temp = [];
                foreach ($taxInfoEle as $taxData) {
                    foreach ($taxData as $sub_key => $value) {
                        if( ! array_key_exists($sub_key, $temp)) $temp[$sub_key] = 0;
                        if ($sub_key === 'tax') {
                            $temp[$sub_key] += $value;
                        } else {
                            $temp[$sub_key] = $value;
                        }
                    }
                }
                array_push($taxInfo, $temp);
            }
        }

        $invoiceSum = array_map('addZeros', $invoiceSum);

        $invoiceSum['taxInfo'] = $taxInfo;
        return $invoiceSum;
    }
     /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    public function getExchangeRateTotalAttribute() {
        return $this->currency ? round(($this->getCalculationAttribute()['total'] / $this->attributes['exchange_currency_rate']), 2) : null;
    }

    /**
     * Get the currency with the invoice.
     */
    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'exchange_currency_id');
    }

    /**
     * Get the company information associated with the purchase order.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
