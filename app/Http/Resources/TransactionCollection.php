<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TransactionCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\Transaction';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($item, $key) {
            return $item->only(['id', 'receipt_number', 'voucher_number', 'contact', 'detail_line', 'start_date', 'end_date', 'frequency', 'frequency_type', 'amount', 'bankAccount', 'annotation', 'observation', 'created_at', 'updated_at']);
        })->values();
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    /*public function with($request)
    {
        return [
            'status' => true,
        ];
    }*/
}
