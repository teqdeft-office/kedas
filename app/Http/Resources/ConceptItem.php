<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Inventory as InventoryResource;
use App\Http\Resources\Tax as TaxResource;

class ConceptItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount' => $this->discount,
            'quantity' => $this->quantity,
            'reference' => $this->reference,
            'description' => $this->description,
            'tax' => new TaxResource($this->tax),
            'price' => $this->price,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
