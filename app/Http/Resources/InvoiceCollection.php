<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InvoiceCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\Invoice';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($item, $key) {
            // prePrint($item->only(['id', 'contact', 'date', 'calculation', 'created_at', 'updated_at']));
            return $item->only(['id', 'internal_number', 'contact', 'start_date', 'term', 'expiration_date', 'end_date', 'frequency', 'frequency_type', 'term_in_locale', 'notes', 'tac', 'privacy_policy', 'calculation', 'created_at', 'updated_at']);
        })->values();
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    /*public function with($request)
    {
        return [
            'status' => true,
        ];
    }*/
}
