<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InventoryAdjustmentCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\InventoryAdjustment';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
        // return $this->collection->map(function($item, $key) {
        //     return $item->only(['id', 'observations', 'adjustment_date', 'total', 'created_at', 'updated_at']);
        // })->values();
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    /*public function with($request)
    {
        return [
            'status' => true,
        ];
    }*/
}
