<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Tax extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'percentage' => $this->percentage,
            'description' => $this->description,
            'sales_account' => $this->sales_account,
            'purchases_account' => $this->purchases_account,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
