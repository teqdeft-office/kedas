<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Document as DocumentResource;
use App\Http\Resources\Item as ItemResource;

class InventoryAdjustment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'observations' => $this->observations,
            'adjustment_date' => $this->adjustment_date,
            'total' => moneyFormat($this->total),
            'documents' => DocumentResource::collection($this->documents),
            'items' => ItemResource::collection($this->items),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
