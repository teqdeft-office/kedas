<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Document as DocumentResource;
use App\Http\Resources\Item as ItemResource;
use App\Http\Resources\ConceptItem as ConceptItemResource;
use App\Http\Resources\Client as ClientResource;
use App\Http\Resources\Supplier as SupplierResource;
use Config;
use App\Supplier;

class Transaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'receipt_number' => $this->receipt_number,
            'voucher_number' => $this->voucher_number,

            'contact' => $this->contact instanceof Supplier ? new SupplierResource($this->contact) : new ClientResource($this->contact),
            'detail_line' => showIfAvailable($this->detail_line),

            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'frequency' => $this->frequency,
            'frequency_type' => $this->frequency_type,

            'amount' => $this->amount,
            'bankAccount' => $this->bankAccount,
            
            'annotation' => $this->annotation,
            'observation' => $this->observation,

            'documents' => DocumentResource::collection($this->documents),
            'concept_items' => ConceptItemResource::collection($this->conceptItems),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
