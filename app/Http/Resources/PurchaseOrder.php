<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Document as DocumentResource;
use App\Http\Resources\Item as ItemResource;
use App\Http\Resources\ConceptItem as ConceptItemResource;
use App\Http\Resources\Client as ClientResource;
use App\Http\Resources\Supplier as SupplierResource;


class PurchaseOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'internal_number' => $this->internal_number,
            'contact' => $this->contact,
            // 'contact' => $this->contact instanceof Supplier ? new SupplierResource($this->contact) : new ClientResource($this->contact),
            'start_date' => dateFormat($this->start_date),
            'expiration_date' => dateFormat($this->expiration_date),
            'tac' => $this->tac,
            'notes' => $this->notes,

            'currency' => $this->currency,
            'exchange_currency_rate' => $this->exchange_currency_rate,

            'items' => ItemResource::collection($this->items),
            'concept_items' => ConceptItemResource::collection($this->conceptItems),
            'documents' => DocumentResource::collection($this->documents),
            'calculation' => $this->calculation,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
