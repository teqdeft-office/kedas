<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InventoryCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\InventoryAdjustment';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($item, $key) {
            return $item->only(['id', 'name', 'reference', 'description', 'quantity', 'sale_price', 'unit_cost', 'cost', 'track_inventory', 'created_at', 'updated_at']);
        })->values();
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    /*public function with($request)
    {
        return [
            'status' => true,
        ];
    }*/
}
