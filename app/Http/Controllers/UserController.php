<?php

namespace App\Http\Controllers;

use App\User;
use App\UserProfile;
use App\Company;
use App\Role;
use App\MasterCategory;
use App\ItemCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Validators\User\ProfileValidator;

use App\Events\ProfileCompleted;
use App\Traits\ApiResponse;
use App\Unit;
use App\MainUnit;
use Lang;
use DB;
use Image;
use Cache;

class UserController extends Controller
{
    use ApiResponse;
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }	

    /**
     * Get the user profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $user = $request->user();

        try {
            if ($request->isMethod('post')) {
                if ($user->company) {

                    $response = [
                        'status' => false,
                        'message' => trans('messages.profile_already_completed'),
                        'messages' => []
                    ];
                    return $this->failResponse($response, 403);
                }
                DB::beginTransaction();
                $input = $request->all();
                $userUpdateValidator = new ProfileValidator;

                if (!$userUpdateValidator->with($input)->passes()) {
                    if ($request->wantsJson() || $request->ajax()) {
                        $response = [
                            'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                            'message'   => $userUpdateValidator->getErrors()[0],
                            'messages'  => $userUpdateValidator->getErrors()
                        ];
                        return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    return back()
                        ->withErrors($userUpdateValidator->getValidator())
                        ->with([
                            'message'   => $userUpdateValidator->getErrors()[0],
                            'alert-type' => 'error'
                        ])
                        ->withInput();
                }

                $data = $input;
                $data['sector_id'] = $input['sector'];
                $data['currency_id'] = $input['currency'];

                unset($data['sector']);
                unset($data['country']);
                unset($data['currency']);
                unset($data['address']);
                unset($data['city']);
                unset($data['user_type']);

                $data['user_id'] = $user->id;

                $company = new Company($data);
                $company->save();

                $mainUnits = MainUnit::select('unit', 'symbol')->get();
                foreach ($mainUnits as $key => $mainUnit) {
                    $units = json_decode($mainUnit->unit);
                    $shortcut = json_decode($mainUnit->symbol);
                    $unit = new Unit(['company_id' => $company->id,
                        'user_id' => $user->id
                        ]);
                    $unit->setTranslation('unit', 'en', $units->en)->setTranslation('unit', 'es', $units->es);
                    $unit->setTranslation('shortcut', 'en', $shortcut->en)->setTranslation('shortcut', 'es', $shortcut->es);
                    // $unit = new Unit($unitData);
                    $unit->save();
                }
                $masterCategories = MasterCategory::select('name')->get();
                foreach ($masterCategories as $key => $category) {
                    $categories = json_decode($category->name);
                    $item = new ItemCategory(['company_id' => $company->id,
                        'user_id' => $user->id
                        ]);
                    $item->setTranslation('name', 'en', $categories->en)->setTranslation('name', 'es', $categories->es);
                    $item->save();
                }


                $user->user_type = $input['user_type'];
                $user->company_id = $company->id;
                $user->is_category_added = 1;
                $user->is_unit_added = 1;
                $user->save();

                $address = $company->address()->create([
                    'line_1' => $input['address'],
                    'city' => $input['city'],
                    'state' => $input['state'],
                    'country_id' => $input['country']
                ]);
                
                DB::commit();

                event(new ProfileCompleted($user));

                $response = [
                    'message' => Lang::get('messages.user_profile_update'),
                    'success' => true
                ];

                if ($request->wantsJson() || $request->ajax()) {
                    return response()->json($response);
                }
                return redirect()->route('user_profile')->with($response);
            }
            return view('user.profile');
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $user = $request->user();        
        $language_options = config('constants.languages');
        return view('users.edit', compact('user', 'language_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        try {
            DB::beginTransaction();
            $input = $request->all();
            $userUpdateValidator = new ProfileValidator('update');

            if (!$userUpdateValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'messages'  => $userUpdateValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                $request->session()->flash('error', $userUpdateValidator->getErrors()[0]);
                return back()
                    ->withErrors($userUpdateValidator->getValidator())
                    ->with([
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            $data = $input;

            if (isset($data['password']) && $data['password']) {
                if (Hash::check($data['current_password'], $user->password)) {
                    $user->password = Hash::make($data['password']);
                } else {
                    return redirect()->back()
                        ->withInput()
                        ->withErrors(['current_password' => trans('messages.old_password_error')]);
                    // throw new \Exception(trans('messages.old_password_error'), 1);
                }
            }

            $user->name = $data['name'];
            $user->language = $data['language'];
            $user->save();

            app()->setlocale($data['language']);
            DB::commit();

            $response = [
                'success' => Lang::get('messages.user_profile_update')
            ];

            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response);
            }
            return redirect()->route('dashboard')->with($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function company(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $resource_associated = $company->invoices->isNotEmpty() || $company->transactions->isNotEmpty() || $company->creditNotes->isNotEmpty() || $company->estimates->isNotEmpty() || $company->debitNotes->isNotEmpty() || $company->purchaseOrders->isNotEmpty();
        return view('users.company', compact('user', 'company', 'resource_associated'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function companyUpdate(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        try {
            DB::beginTransaction();
            $input = $request->all();
            $userUpdateValidator = new ProfileValidator('company');

            if (!$userUpdateValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'messages'  => $userUpdateValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                echo "there";
                die;
                $request->session()->flash('error', $userUpdateValidator->getErrors()[0]);
                return back()
                    ->withErrors($userUpdateValidator->getValidator())
                    ->with([
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            $resource_associated = $company->invoices->isNotEmpty() || $company->transactions->isNotEmpty() || $company->creditNotes->isNotEmpty() || $company->estimates->isNotEmpty() || $company->debitNotes->isNotEmpty() || $company->purchaseOrders->isNotEmpty();

            $data = [
                'name' => $input['name'],
                'role' => $input['role'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'tax_id' => $input['tax_id'],
                'support_email' => $input['support_email'],
            ];

            if (!$resource_associated) {
                $data['currency_id'] = $input['currency'];
            }

            if (isset($input['logo'])) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                $data['logo'] = $filename;
            }

            if (isset($input['signature']) && $input['signature']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                $signatureInstance = Image::make($input['signature']);

                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }
                $data['signature'] = $filename;
            }

            $company->update($data);

            if ($company->address()) {
                $company->address()->updateOrCreate([], [
                    'line_1' => $input['address'],
                    'city' => $input['city'],
                    'state' => $input['state'],
                    'country_id' => $input['country'],
                    'zip' => $input['zip'],
                ]);
            } else {
                $company->address()->create([
                    'line_1' => $input['address'],
                    'city' => $input['city'],
                    'state' => $input['state'],
                    'country_id' => $input['country'],
                    'zip' => $input['zip'],
                ]);                
            }

            DB::commit();

            $response = [
                'success' => Lang::get('messages.company_info_update')
            ];

            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response);
            }
            return redirect()->route('dashboard')->with($response);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            DB::rollback();
            $response = [
                'error'     => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * delete the associated resources of the specified user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function deleteUser(Request $request, User $user) {
        try {
            DB::beginTransaction();
            if ($user->isCompanyOwner()) {
                $user->delete();
            }
            DB::commit();
            return response()->json($user);

        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => $e->getMessage()
            ];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
