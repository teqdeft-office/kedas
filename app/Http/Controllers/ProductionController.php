<?php

namespace App\Http\Controllers;

use App\Production;
use Illuminate\Http\Request;
use Config;

class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $productionOrders = $user->company->productionOrder;

        // dd($invoices);
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $productionOrders->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'internal_number' => $item->invoice->internal_number,
                    'contact' => $item->invoice->contact,
                    'start_date' => $item->invoice->start_date,
                    'term' => $item->invoice->term ? (Config::get('constants.terms')[$item->invoice->term].($item->invoice->term == 'manual' ? '('.$item->invoice->manual_days.' '.trans('labels.days').')' : '')) : null,
                    'end_date' => $item->invoice->end_date,
                    'frequency' => $item->invoice->frequency,
                    'frequency_type' => $item->invoice->frequency_type,
                    'expiration_date' => $item->invoice->expiration_date,
                    'calculation' => $item->invoice->calculation,
                    'term_in_locale' => $item->invoice->term_in_locale,
                    'notes' => $item->invoice->notes,
                    'tac' => $item->invoice->tac,
                    'privacy_policy' => $item->invoice->privacy_policy,
                    'status' => trans('labels.' . $item->status),
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        $type="sale";
        return view('production.index', compact('productionOrders', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function show(Production $production)
    {
        return view('production.show', compact('production'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function edit(Production $production)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Production $production)
    {
        $input=$request->input();
        if(!isset($input["status"])){
            return back()->withInput();
        }
        $status=$input["status"];
        switch ($status) {
            case 'mark_in_progress':
                $production->update(['status'=>'in_progress']);
                break;
            case 'mark_as_pending':
                $production->update(['status'=>'pending']);
                break;
            case 'mark_as_done':
                $production->update(['status'=>'done']);
                break;
            case 'mark_as_confirmed':
                $production->update(['status'=>'confirmed']);
                break;
            default:
                break;
        }

        $request->session()->flash('success', trans('messages.production_order_updated'));
        return redirect()->route('production.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Production $production)
    {
        try {
            $production->delete();
            $request->session()->flash('success', trans('messages.production_order_deleted'));
            return redirect()->route('production.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }
}
