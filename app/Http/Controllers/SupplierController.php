<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Supplier;
use App\Address;
use App\Validators\SupplierValidator;
use App\Validators\ClientValidator;

class SupplierController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Supplier::class, 'supplier');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $suppliers = $user->company->suppliers;
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $suppliers]);
        }
        return view('suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SupplierValidator $supplierValidator, ClientValidator $clientValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if((!$request->has('client')) && (!$request->has('supplier'))) {
                $request->session()->flash('error', trans('messages.client_or_supplier_require_one'));
                return back()->withInput();
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'contact_name' => $input['contact_name'],
                'contact_phone_number' => $input['contact_phone_number'],
                'user_id' => $user->id
            ];

            if($request->has('supplier')) {
                if (!$supplierValidator->with($input)->passes()) {
                    $request->session()->flash('error', $supplierValidator->getErrors()[0]);
                    return back()
                        ->withErrors($supplierValidator->getValidator())
                        ->withInput();
                }

                $supplier = $user->company->suppliers()->create($data);
                if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                    $addressData = [
                        'line_1' => $input['address'],
                        'zip' => $input['zip'],
                        'country_id' => $input['country'],
                        'state' => $input['state'],
                        'city' => $input['city'],
                    ];
                    $address = $supplier->address()->create($addressData);
                }
            }

            if($request->has('client')) {
                if (!$clientValidator->with($input)->passes()) {
                    $request->session()->flash('error', $clientValidator->getErrors()[0]);
                    return back()
                        ->withErrors($clientValidator->getValidator())
                        ->withInput();
                }
                unset($data['contact_name']);
                unset($data['contact_phone_number']);
                $client = $user->company->clients()->create($data);
                if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                    $addressData = [
                        'line_1' => $input['address'],
                        'zip' => $input['zip'],
                        'country_id' => $input['country'],
                        'state' => $input['state'],
                        'city' => $input['city'],
                    ];
                    $address = $client->address()->create($addressData);
                }
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.supplier_created'));
            return redirect()->route('suppliers.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return view('suppliers.show', compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        return view('suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            $supplierValidator = new SupplierValidator('update', $supplier);

            if (!$supplierValidator->with($input)->passes()) {
                $request->session()->flash('error', $supplierValidator->getErrors()[0]);
                return back()
                    ->withErrors($supplierValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'contact_name' => $input['contact_name'],
                'contact_phone_number' => $input['contact_phone_number'],
            ];

            $supplier->update($data);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];

                $supplier->address()->updateOrCreate([], $addressData);
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.supplier_updated'));
            return redirect()->route('suppliers.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Supplier $supplier)
    {
        try {
            if ($supplier->invoices->isEmpty() && $supplier->transactions->isEmpty() && $supplier->creditNotes->isEmpty() && $supplier->estimates->isEmpty() && $supplier->debitNotes->isEmpty() && $supplier->purchaseOrders->isEmpty()) {
                $supplier->forceDelete();
                $request->session()->flash('success', trans('messages.supplier_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('suppliers.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
