<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Validators\InvoiceValidator;

use App\Invoice;
use App\Supplier;
use App\Client;
use App\Item;
use App\User;

use App\Events\SaleInvoiceCreatedOrUpdated;
use App\Events\PaymentCreatedOrUpdated;

use DB;
use Cache;

class PosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the dashboard of the POS system.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $inventories = $user->company->inventories;
        $accounting_heads = $user->company->accountingHeads;
        $ncfs = $user->company->ncfs;

        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;

        $contacts = collect([]);
        $bank_accounts_options = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.pos_without_contacts'));
            return redirect()->route('clients.create');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.pos_without_products'));
            return redirect()->route('inventories.create');
        }

        /*if (!$user->company->logo) {
            $request->session()->flash('error', trans('messages.pos_without_logo'));
            return redirect()->route('company.edit');
        }*/

        $language_options = config('constants.languages');
        $currencies = Cache::get('currencies');
        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = $currencies->where('id', '<>', $user_currency_id)->map(function($item, $key) use($currency_rates) {
            $item['rate'] = isset($currency_rates[$item->id]) ? $currency_rates[$item->id] : '';
            return $item->only(['id', 'name', 'code', 'symbol', 'formatted_name', 'rate']);
        })->values();

        // $ncf_options = collect([null => trans('labels.select_ncf')] + $ncfs->pluck('name', 'id')->toArray());

        $ncf_options = $ncfs->map(function($item, $key) {
            return $item->only(['id', 'name']);
        });

        $contact_options = $contacts->map(function($item, $key) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            return $item;
        });

        $contact_attributes = $contact_options->mapWithKeys(function ($item) {
            return [$item->custom_id => [
                'data-phone' => $item->phone ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        // The following is used for Destination list.
        $bank_account = $accounting_heads->where('code', self::BANKCODE)->first();            
        $this->generateAccountsLevels($bank_account, $bank_accounts_options, 1);

        return view('pos.index', compact('user', 'language_options', 'contact_options', 'ncf_options', 'currency_options', 'bank_accounts_options'));
    }

    private function createInvoiceAndPayment($input) {
        $invoiceValidator = new InvoiceValidator(self::SALE, false, true);
        $user = User::find($input['createdBy']);

        $products = array_map(function($element) {
            $cartEle['inventory_id'] = $element['productID'];
            $cartEle['tax_id'] = $element['taxID'] ?: null;
            $cartEle['quantity'] = $element['quantity'];
            $cartEle['discount'] = @$element['discount'];
            return $cartEle;
        }, $input['cart']);

        $correctProducts = collect($products)->every(function($product) {
            return $product['inventory_id'] && $product['quantity'] && 
                (!isset($product['discount']) || (isset($product['discount']) && $product['discount'] > 0 && $product['discount'] < 100));
        });


        if (!$correctProducts) {
            throw new \Exception(trans('messages.something_went_wrong'), 1);
        }

        $data = [
            'user_id' => $input['createdBy'],
            'contact_id' => $input['customer']['custom_id'],
            'invoice_start_date' => $input['date'],
            'term' => '0',
            'invoice_expiration_date' => $input['date'],
            'paid_amount' => $input['paid_amount'],
            'payment_account' => 'bank',
            'payment_method' => 'cash',
            'products' => $products,
            'ncf_id' => $input['ncf_id'],
            'bank_account' => $input['bank_account'],
            'exchange_currency_rate' => $input['exchange_currency_rate'] ?? null,
            'exchange_currency_id' => $input['exchange_currency_id'] ?? null,
        ];

        if (!$invoiceValidator->with($data)->passes()) {
            throw ValidationException::withMessages($invoiceValidator->getErrors());
        }

        list($contact_type, $contact_id) = explode('-', $data['contact_id']);
        $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

        if (!$contact) {
            $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
            return back()->withInput();
        }

        $invoice = $contact->invoices()->create([
            'user_id' => $user->id,
            'company_id' => $user->company->id,
            'term' => $data['term'],
            'start_date' => $data['invoice_start_date'],
            'expiration_date' => $data['invoice_expiration_date'],
            'end_date' => @$data['invoice_end_date'],
            'type' => self::SALE,
            'ncf_id' => @$data['ncf_id'],
            'exchange_currency_rate' => @$data['exchange_currency_rate'] ?? null,
            'exchange_currency_id' => @$data['exchange_currency_id'] ?? null,
        ]);
        $invoice->items()->createMany($products);

        // decrease the quantity of each item's inventory while creating the invoice.
        $invoice->items->each(function($item) {
            $item->inventory->quantity = $item->inventory->quantity - $item->quantity;
            $item->inventory->save();
        });

        if ($invoice->ncf_number) {
            $invoice->ncf->current_number = $invoice->ncf_number;
            $invoice->ncf->save();
        }
        event(new SaleInvoiceCreatedOrUpdated($invoice));


        /*if (!$data['bank_account']) {
            throw new \Exception(trans('validation.required', ['attribute' => trans('labels.bank_account')]), 1);
        }*/

        if ($data['paid_amount'] > $invoice->calculation['total']) {
            throw new \Exception(trans('messages.invalid_invoice_transaction'), 1);
        }

        $accounting_heads = $user->company->accountingHeads;
        $cashOnHandAccount = $accounting_heads->where('code', self::CASHONHAND)->first();

        $transaction = $contact->transactions()->create([
            'user_id' => $user->id,
            'company_id' => $user->company->id,
            'start_date' => $data['invoice_start_date'],
            'end_date' => $data['invoice_start_date'],
            'transaction_with_invoice' => 1,
            'type' => 'in',
            'amount' => $data['paid_amount'],
            'method' => @$data['payment_method'],
            'annotation' => @$data['annotation'],
            'observation' => @$data['observation'],
            'bank_account' => $cashOnHandAccount->id,
            // 'bank_account' => $data['bank_account'],
        ]);

        $transaction->invoices()->attach([
            $invoice->id => ['amount' => $data['paid_amount']]
        ]);

        event(new PaymentCreatedOrUpdated($transaction));

        /* Save exchange rate */
        if (isset($data['exchange_currency_rate']) && $data['exchange_currency_rate'] && isset($data['exchange_currency_id']) && $data['exchange_currency_id']) {
            $user->company->currencyRates()->updateOrCreate(
                ['currency_id' => $data['exchange_currency_id'], 'user_id' => $user->id],
                ['rate' => $data['exchange_currency_rate']]
            );
        }
        
        return [
            'invoice_id' => $invoice->id,
            'transaction_id' => $transaction->id,
            'invoice_template' => view('pdf.invoice-template-15', compact('invoice', 'user'))->render()
        ];
    }

    /**
     * Create an order for the POS system.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createOrder(Request $request, $mode = 'online') {

        try {
            DB::beginTransaction();
            $input = $request->all();

            if ($mode != 'online') {
                $response = [];
                $time = array_column($input, 'time');
                array_multisort($time, SORT_ASC, $input);
                foreach ($input as $singleCart) {
                    array_push($response, $this->createInvoiceAndPayment($singleCart));
                }
            } else {
                $response = $this->createInvoiceAndPayment($input);
            }

            DB::commit();

            if ($response) {
                return $mode != 'online' ? response()->json([
                    'message' => trans('messages.sync_completed'),
                ]) : response()->json($response);
            } else {
                return response()->json([
                    'message' => trans('messages.something_went_wrong'),
                ], 500);
            }

        } catch (ValidationException $e) {
            DB::rollback();
            return response()->json([
                'message' => reset($e->errors()[0]),
            ], 422);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
