<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\ExtraHour;
use App\Validators\ExtraHourValidator;

use Config;

class ExtraHourController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Tax::class, 'tax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $extraHours = $user->company->extraHours;

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $extraHours->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'employee' => $item->employee,
                    'type' => $item->typeAccount->name,
                    'start_time' => $item->start_time,
                    'end_time' => $item->end_time,
                    'hours_price' => $item->hours_price,
                    'total_extra_hours' => $item->total_extra_hours,
                    'extra_hours_total' => $item->extra_hours_total,
                    'id' => $item->id,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('extra-hours.index', compact('extraHours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;

        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        // $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.extra_hour_type_option'));
        // $incomes_option = ['' => trans('labels.select_incomes_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_incomes_option'));

        $type_accounts_options = collect([]);
        $type_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
        $type_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $type_accounts)->pluck('id')->toArray();
        $type_accounts = $accounting_heads->whereIn('parent_id', $type_account_types)->values();

        $type_accounts->each(function($incomes_account) use($type_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $type_accounts_options, 1, true);
        });

        $income_accounts_options = collect([]);
        $income_accounts = $accounting_heads->where('code', self::LIABILITIESCODE)->pluck('id')->toArray();
        $incomes_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $income_accounts)->pluck('id')->toArray();
        $income_accounts = $accounting_heads->whereIn('parent_id', $incomes_account_types)->values();

        $income_accounts->each(function($incomes_account) use($income_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $income_accounts_options, 1, true);
        });
        return view('extra-hours.create', compact('employee_options', 'type_accounts_options', 'income_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ExtraHourValidator $extraHourValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$extraHourValidator->with($input)->passes()) {
                $request->session()->flash('error', $extraHourValidator->getErrors()[0]);
                return back()
                    ->withErrors($extraHourValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type_account' => $input['type_account'],
                'start_time' => $input['start_time'],
                'end_time' => $input['end_time'],
                'observation' => $input['observation'],
                'hours_price' => $input['hours_price'],
                'income_account' => $input['income_account'],
                'user_id' => $user->id
            ];

            $extraHours = $user->company->extraHours()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.extra_hour_created'));
            return redirect()->route('extra-hours.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ExtraHour $extraHour)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;   

        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();

        // $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.extra_hour_type_option'));
        // $incomes_option = ['' => trans('labels.select_incomes_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_incomes_option'));

        $type_accounts_options = collect([]);
        $type_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
        $type_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $type_accounts)->pluck('id')->toArray();
        $type_accounts = $accounting_heads->whereIn('parent_id', $type_account_types)->values();

        $type_accounts->each(function($incomes_account) use($type_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $type_accounts_options, 1, true);
        });

        $income_accounts_options = collect([]);
        $income_accounts = $accounting_heads->where('code', self::LIABILITIESCODE)->pluck('id')->toArray();
        $incomes_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $income_accounts)->pluck('id')->toArray();
        $income_accounts = $accounting_heads->whereIn('parent_id', $incomes_account_types)->values();

        $income_accounts->each(function($incomes_account) use($income_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $income_accounts_options, 1, true);
        });
        return view('extra-hours.edit', compact('extraHour', 'employee_options', 'type_accounts_options', 'income_accounts_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WithHolding  $WithHolding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExtraHour $extraHour, ExtraHourValidator $extraHourValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            
            if (!$extraHourValidator->with($input)->passes()) {
                $request->session()->flash('error', $extraHourValidator->getErrors()[0]);
                return back()
                    ->withErrors($extraHourValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type_account' => $input['type_account'],
                'start_time' => $input['start_time'],
                'end_time' => $input['end_time'],
                'observation' => $input['observation'],
                'hours_price' => $input['hours_price'],
                'income_account' => $input['income_account'],
            ];

            $extraHour->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.extra_hour_updated'));
            return redirect()->route('extra-hours.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\withHolding  $withHolding
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ExtraHour $extraHour)
    {
        try {
            $extraHour->delete();
            $request->session()->flash('success', trans('messages.extra_hour_deleted'));
            return redirect()->route('extra-hours.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
