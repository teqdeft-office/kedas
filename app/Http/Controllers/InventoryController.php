<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use ProVision\Searchable\SearchableModes;

use App\Inventory;
use App\Validators\InventoryValidator;
use App\Mail\InventoryCreated;

use App\Events\InventoryCreatedOrUpdated;

use DB;
use Auth;
use Config;
use Image;
use Cache;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Inventory::class, 'inventory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $barcode = $request->input('barcode') ?? null;

        $inventories = $barcode ? $user->company->inventories->where('barcode', $barcode)->values() : $user->company->inventories;
        if ($request->wantsJson() || $request->ajax()) {
            /* $jsonCollection = collect();
            $inventories->each(function ($item, $key) use ($jsonCollection) {
                prePrint($item->itemCategory->name);
                $jsonCollection->push([

                ]);
            }); */
            return response()->json(['data' => escapeResponse($inventories)]);
            // return response()->json(array_map('htmlentities', $inventories));
        }
        return view('inventories.index', compact('inventories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $taxes = $user->company->taxes;
        $memberships = $user->company->memberships;
        $type = $request->input('type') ?? self::SERVICES;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $accounting_heads = $user->company->accountingHeads;

        $all_accounts_options = collect([]);
        $assets_accounts_options = collect([]);
        $income_accounts_options = collect([]);
        $expense_accounts_options = collect([]);

        if ($type == self::INVENTORY) {
            $assets_accounts = $accounting_heads->where('code', self::INVENTORYCODE)->first();
            $income_accounts = $accounting_heads->where('code', self::SALESOFPRODUCTINCOMECODE)->first();
            $expense_accounts = $accounting_heads->where('code', self::SUPPLIESANDMATERIALSCODE)->first();
            
            $this->generateAccountsLevels($assets_accounts, $assets_accounts_options, 1, true);
            $this->generateAccountsLevels($income_accounts, $income_accounts_options, 1, true);
            $this->generateAccountsLevels($expense_accounts, $expense_accounts_options, 1, true);
        } else {
            $income_accounts = $accounting_heads->where('code', self::INCOMECODE)->pluck('id')->toArray();
            $income_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $income_accounts)->pluck('id')->toArray();
            $income_accounts = $accounting_heads->whereIn('parent_id', $income_account_types)->values();

            $income_accounts->each(function($income_account) use($income_accounts_options) {
                $this->generateAccountsLevels($income_account, $income_accounts_options, 1, true);
            });

            $expense_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
            $expenses_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $expense_accounts)->pluck('id')->toArray();
            $expense_accounts = $accounting_heads->whereIn('parent_id', $expenses_account_types)->values();

            $expense_accounts->each(function($expenses_account) use($expense_accounts_options) {
                $this->generateAccountsLevels($expenses_account, $expense_accounts_options, 1, true);
            });
        }

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
        $item_category_options = ['' => trans('labels.select_category')] + $user->company->itemCategories->pluck('name', 'id')->toArray();
        $account_options = ['' => trans('labels.select_account')] + $this->mapOptionsBasedOnLocale(Config::get('constants.accounts'));
        $membership_options = $user->company->memberships->pluck('name', 'id')->toArray();
        $unit_option = ['' => trans('labels.select_unit')] + $user->company->units->pluck('unit', 'id')->toArray();
        return view('inventories.create', compact('tax_options', 'item_category_options', 'account_options', 'tax_attributes', 'type', 'assets_accounts_options', 'income_accounts_options', 'expense_accounts_options', 'membership_options', 'unit_option'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $type = $input['type'] ?? self::SERVICES;

            $inventoryValidator = new InventoryValidator('add', null, $type);

            if ($type != self::INVENTORY) {
                $input['income_account'] = ($input['is_product_sale'] ?? false) ? ($input['income_account'] ?? null) : null;
                $input['expense_account'] = ($input['is_product_purchase'] ?? false) ? ($input['expense_account'] ?? null) : null;
                $input['asset_account'] = null;
            }

            if (!$inventoryValidator->with($input)->passes()) {
                $request->session()->flash('error', $inventoryValidator->getErrors()[0]);
                return back()
                    ->withErrors($inventoryValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'type' => $input['type'],

                'name' => $input['name'],
                'reference' => $input['reference'],
                'description' => $input['description'],
                'barcode' => $input['barcode'],
                'sku' => $input['sku'] ?? null,
                'item_category_id' => $input['item_category_id'],
                'tax_id' => (int)$input['tax_id'] ? $input['tax_id'] : null,
                'sale_price' => $input['sale_price'],
                'cost' => $input['cost'] ?? null,
                'unit_id' => $input['unit'], // add unit id
                // 'type_of_item' => $input['type_of_item'],
                'account' => $input['account'] ?? null,

                'track_inventory' => $type == self::INVENTORY ? 1 : 0,
                'quantity' => $input['quantity'] ?? null,
                'init_quantity' => $input['quantity'] ?? null,
                'init_quantity_date' => $input['init_quantity_date'] ?? null,
                'unit_cost' => $input['unit_cost'] ?? null,

                'asset_account' => $input['asset_account'] ?? null,
                'income_account' => $input['income_account'] ?? null,
                'expense_account' => $input['expense_account'] ?? null,
                'expiring_date' => $input['expiring_date'] ?? null,
                'user_id' => $user->id
            ];

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/items");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);

                $data['image'] = $filename;
            }

            $inventory = $user->company->inventories()->create($data);

            $memberships = $input['memberships'] ?? [];

            if ($memberships) {
                $inventory->memberships()->attach($memberships);
            }
            
            if ($type == self::INVENTORY) {
                event(new InventoryCreatedOrUpdated($inventory));
            }

            
            
            Mail::to($user->email)->send(new InventoryCreated($inventory));
            // Mail::to($user->email)->send(new InventoryCreated($inventory));

            DB::commit();

            $request->session()->flash('success', trans('messages.inventory_created'));
            return redirect()->route('inventories.index');

        } catch (\Exception $e) {
           dd($e);
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        return view('inventories.show', compact('inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory, Request $request)
    {
        $user = $request->user();
        $taxes = $user->company->taxes;
        $memberships = $user->company->memberships;

        $assignedMemberships = $inventory->memberships;

        $type = $inventory->type ?? self::SERVICES;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $accounting_heads = $user->company->accountingHeads;

        $all_accounts_options = collect([]);
        $assets_accounts_options = collect([]);
        $income_accounts_options = collect([]);
        $expense_accounts_options = collect([]);

        if ($type == self::INVENTORY) {
            $assets_accounts = $accounting_heads->where('code', self::INVENTORYCODE)->first();
            $income_accounts = $accounting_heads->where('code', self::SALESOFPRODUCTINCOMECODE)->first();
            $expense_accounts = $accounting_heads->where('code', self::SUPPLIESANDMATERIALSCODE)->first();
            
            $this->generateAccountsLevels($assets_accounts, $assets_accounts_options, 1, true);
            $this->generateAccountsLevels($income_accounts, $income_accounts_options, 1, true);
            $this->generateAccountsLevels($expense_accounts, $expense_accounts_options, 1, true);
        } else {
            $income_accounts = $accounting_heads->where('code', self::INCOMECODE)->pluck('id')->toArray();
            $income_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $income_accounts)->pluck('id')->toArray();
            $income_accounts = $accounting_heads->whereIn('parent_id', $income_account_types)->values();

            $income_accounts->each(function($income_account) use($income_accounts_options) {
                $this->generateAccountsLevels($income_account, $income_accounts_options, 1, true);
            });

            $expense_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
            $expenses_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $expense_accounts)->pluck('id')->toArray();
            $expense_accounts = $accounting_heads->whereIn('parent_id', $expenses_account_types)->values();

            $expense_accounts->each(function($expenses_account) use($expense_accounts_options) {
                $this->generateAccountsLevels($expenses_account, $expense_accounts_options, 1, true);
            });
        }

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
        $item_category_options = ['' => trans('labels.select_category')] + $user->company->itemCategories->pluck('name', 'id')->toArray();
        $account_options = ['' => trans('labels.select_account')] + Config::get('constants.accounts');
        $resource_associated = $inventory->items->isNotEmpty();
        $membership_options = $user->company->memberships->pluck('name', 'id')->toArray();
        $unit_option = ['' => trans('labels.select_unit')] + $user->company->units->pluck('unit', 'id')->toArray();
        return view('inventories.edit', compact('tax_options', 'item_category_options', 'account_options', 'inventory', 'tax_attributes', 'resource_associated', 'type', 'assets_accounts_options', 'income_accounts_options', 'expense_accounts_options', 'assignedMemberships', 'membership_options', 'unit_option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $resource_associated = $inventory->items->isNotEmpty();

            $inventoryValidator = new InventoryValidator('update', $inventory, $inventory->type);

            $input['type'] = $inventory->type;

            if ($inventory->type != self::INVENTORY) {
                $input['income_account'] = ($input['is_product_sale'] ?? false) ? ($input['income_account'] ?? null) : null;
                $input['expense_account'] = ($input['is_product_purchase'] ?? false) ? ($input['expense_account'] ?? null) : null;
                $input['asset_account'] = null;
            }

            if (!$inventoryValidator->with($input)->passes()) {
                $request->session()->flash('error', $inventoryValidator->getErrors()[0]);
                return back()
                    ->withErrors($inventoryValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'reference' => $input['reference'],
                'description' => $input['description'],
                'barcode' => $input['barcode'],
                'sku' => $input['sku'] ?? null,
                'item_category_id' => $input['item_category_id'],
                'tax_id' => (int)$input['tax_id'] ? $input['tax_id'] : null,
                'sale_price' => $input['sale_price'],
                'cost' => $input['cost'] ?? null,
                'unit_id' => $input['unit'], // add unit id
                // 'type_of_item' => $input['type_of_item'],
                'account' => $input['account'] ?? null,

                'asset_account' => $input['asset_account'] ?? null,
                'income_account' => $input['income_account'] ?? null,
                'expense_account' => $input['expense_account'] ?? null
            ];

            if(!$inventory->track && $inventory->quantity == null) {
                $data += [
                    'track_inventory' => $input['track_inventory'] ?? 0,
                    'quantity' => $input['quantity'] ?? null,
                    'init_quantity' => $input['quantity'] ?? null,
                    'unit_cost' => $input['unit_cost'] ?? null,
                ];
            }
            
            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/items");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);
                
                $data['image'] = $filename;
            }

            $inventory->update($data);

            $memberships = $input['memberships'] ?? [];
            $inventory->memberships()->sync($memberships);

            /*if ($type == self::INVENTORY) {
                event(new InventoryCreatedOrUpdated($inventory));
            }*/

            DB::commit();

            $request->session()->flash('success', trans('messages.inventory_updated'));
            return redirect()->route('inventories.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Inventory $inventory)
    {
        try {
            if ($inventory->items->isEmpty()) {
                $inventory->delete();
                $request->session()->flash('success', trans('messages.inventory_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('inventories.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }

    /**
     * Searching of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $user = $request->user();
        $input = $request->all();
        $limit = $input['limit'] ?? 28;
        $offset = $input['offset'] ?? 0;
        $searchValue = $input['searchValue'] ?? null;

        if ($searchValue) {
            $inventories = Inventory::search($searchValue)->where('company_id', $user->company->id)->get();
        } else {
            $inventories = $user->company->inventories;
        }
        $products = $inventories->slice($offset, $limit)->values();

        return response()->json([
            'products' => $products,
            'count' => $products->count(),
            'total_products' => $inventories->count(),
            'barcodeResultValue' => null
        ]);
    }
}
