<?php

namespace App\Http\Controllers;

use App\CreditNote;
use App\Supplier;
use App\Client;
use App\Item;

use App\Validators\CreditNoteValidator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\CreditNoteCreated;
use App\Mail\CreditNoteShared;

use DB;
use Auth;
use Config;
use Image;
use PDF;
use File;
use Cache;

class CreditNoteController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(CreditNote::class, 'credit_note');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $creditNotes = $user->company->creditNotes;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $creditNotes->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'contact' => $item->contact,
                    // 'contact_name' => $item->contact->name,
                    'start_date' => $item->start_date,
                    'remaining' => $item->calculation['remaining_amount'],
                    'total' => $item->calculation['total'],
                    'calculation' => $item->calculation,
                    'notes' => $item->notes,
                    'contact' => $item->contact,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('credit-notes.index', compact('creditNotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $currencies = Cache::get('currencies');

        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.credit_notes_without_contacts'));
            return redirect('dashboard');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.credit_notes_without_items'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;
        $ncfs = $user->company->ncfs;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
        $ncf_options = [null => trans('labels.select_ncf')] + $ncfs->pluck('name', 'id')->toArray();

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();

        return view('credit-notes.create', compact('inventories', 'tax_options', 'contact_options', 'contact_attributes', 'inventory_options', 'inventory_attributes', 'tax_attributes', 'ncf_options', 'currency_attributes', 'currency_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $creditNoteValidator = new CreditNoteValidator();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$creditNoteValidator->with($input)->passes()) {
                $request->session()->flash('error', $creditNoteValidator->getErrors()[0]);
                return back()
                    ->withErrors($creditNoteValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }
            $creditNoteItems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
            })->values()->toArray();

            if (!$creditNoteItems) {
                $request->session()->flash('error', trans('messages.credit_notes_without_items'));
                return back()
                    ->withInput();
            }

            $creditNote = $contact->creditNotes()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['credit_note_date'],
                'notes' => @$input['notes'],
                'ncf_id' => @$input['ncf_id'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $creditNote->items()->createMany($creditNoteItems);
            
            if ($creditNote->ncf_number) {
                $creditNote->ncf->current_number = $creditNote->ncf_number;
                $creditNote->ncf->save();
            }

            /* upload documents */
            if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/credit-note-'.$file));
                        $documents_data[] = [
                            'name' => 'credit-note-'.$file,
                        ];
                    }
                }

                $documents = $creditNote->documents()->createMany($documents_data);
            }

            Mail::to($contact->email)->send(new CreditNoteCreated($creditNote));

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.credit_note_created'));
            return redirect()->route('credit-notes.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function show(CreditNote $creditNote)
    { 
        //prePrint($creditNote->invoices->first()->calculation);
        return view('credit-notes.show', compact('creditNote'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, CreditNote $creditNote)
    {
        $creditNote->contact_custom_id = ($creditNote->contact instanceof Supplier ? 'supplier' : 'client') .'-'. $creditNote->contact->id;

        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();
        
        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.credit_notes_without_contacts'));
            return redirect('dashboard');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.credit_notes_without_items'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        return view('credit-notes.edit', compact('creditNote', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'inventory_options', 'inventory_attributes', 'tax_attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreditNote $creditNote)
    {
        $creditNoteValidator = new CreditNoteValidator('update');
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$creditNoteValidator->with($input)->passes()) {
                $request->session()->flash('error', $creditNoteValidator->getErrors()[0]);
                return back()
                    ->withErrors($creditNoteValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }

            $creditNoteItems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
                })->values();

            if ($creditNoteItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.credit_note_without_items'));
                return back()
                    ->withInput();
            }

            $updateCreditNoteItems = $creditNoteItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertCreditNoteItems = $creditNoteItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteCreditNoteItemsExcept = $updateCreditNoteItems->pluck('id')->toArray();

            $creditNote->contact()->associate($contact);

            $creditNote->update([
                'start_date' => $input['credit_note_date'],
                'notes' => @$input['notes']
            ]);

            $credit_note_total = $input['credit_note_total_hidden'] ?? $creditNote->calculation['total'];

            // associate credit note to invoices.
            $payments = $input['payments'] ?? [];
            $paymentItems = collect($payments)->filter(function($row) {
                return $row['invoice_id'] && (isset($row['amount']) && $row['amount'] > 0);
            })->values();

            // remove the association of credit note with other invoices. 
            if ($paymentItems->isEmpty()) {
                $creditNote->invoices()->detach();
            } else {
                $pluckedInvoices = $creditNote->contact->invoices->pluck('calculation', 'id')->map(function($value, $key) {
                    return $value['pendingAmountWithoutCreditNote'];
                })->toArray();

                $amountCorrect = $paymentItems->every(function ($value, $key) use ($pluckedInvoices) {
                    return $value['amount'] <= $pluckedInvoices[$value['invoice_id']];
                });

                if (!$amountCorrect) {
                    $request->session()->flash('error', trans('messages.credit_note_with_incorrect_amount'));
                    return back()->withInput();
                }

                $total_amount = $paymentItems->sum('amount');

                if ($total_amount > $credit_note_total) {
                    $request->session()->flash('error', trans('messages.credit_note_with_incorrect_amount'));
                    return back()->withInput();
                }
                $creditNote->invoices()->sync($paymentItems->keyBy('invoice_id')->toArray());
            }

            if ($deleteCreditNoteItemsExcept) {
                $creditNote->items()->whereNotIn('id', $deleteCreditNoteItemsExcept)->delete();
            }

            if ($insertCreditNoteItems) {
                $creditNote->items()->createMany($insertCreditNoteItems);
            }

            if ($updateCreditNoteItems) {
                foreach ($updateCreditNoteItems as $updateCreditNoteItem) {
                    $updatedDate = $updateCreditNoteItem;
                    unset($updatedDate['id']);
                    Item::findOrFail($updateCreditNoteItem['id'])->update($updatedDate);
                }
            }

            /* update documents start */
            $documents = $request->input('documents', []);

            if ($creditNote->documents->isNotEmpty()) {
                foreach ($creditNote->documents as $document) {
                    if (!in_array($document->name, $documents)) {
                        $creditNote->documents()->where('id', '=', $document->id)->delete();
                    }
                }
            }
        
            $alreadyExistDocuments = $creditNote->documents->pluck('name')->toArray();

            $dir = public_path("storage/documents");

            foreach ($request->input('documents', []) as $file) {
            
                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
                    if (File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/credit-note-'.$file));
                        $document_data = array('name' => 'credit-note-'.$file);
                        $creditNote->documents()->create($document_data);
                    }
                }
            }
            /* update documents end */

            DB::commit();

            $request->session()->flash('success', trans('messages.credit_note_updated'));
            return redirect()->route('credit-notes.index');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CreditNote $creditNote)
    {
        try {
            $creditNote->delete();
            $request->session()->flash('success', trans('messages.credit_note_deleted'));
            return redirect()->route('credit-notes.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }

    /**
     * Download the specified resource as PDF.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request, CreditNote $creditNote)
    {
        try {
            $user = $request->user();
            $pdf = PDF::loadView('pdf.credit-note', compact('creditNote', 'user'));
            $filename = 'credit-note-'.$creditNote->id.'.pdf';
            return $pdf->download($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Print the specified resource.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request, CreditNote $creditNote)
    {
        try {
            $user = $request->user();
            $showView = $request->input('view');
            if ($showView) {
                return view('pdf.credit-note', compact('creditNote', 'user'));
            }
            $filename = 'credit-note-'.$creditNote->id.'.pdf';
            $print = true;
            $pdf = PDF::loadView('pdf.credit-note', compact('creditNote', 'print', 'user'));
            $pdf->setOption('enable-javascript', true);
            // $pdf->setOption('javascript-delay', 13500);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->inline($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * email the specified resource.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function mail(Request $request, CreditNote $creditNote)
    {
        try {
            $user = $request->user();
            $input = $request->all();
            $emails = collect(explode(',', $input['emails']));
            $emails = $emails->map(function ($item, $key) {
                $item = trim(strtolower($item));
                return $item;
            })->uniqueStrict()->filter();

            if ($emails->isEmpty()) {
                $request->session()->flash('error', trans('messages.credit_note_mail_without_email'));
                return redirect()->route('credit-notes.show', ['creditNote' => $creditNote]);
            }

            $pdf = PDF::loadView('pdf.credit-note', compact('creditNote', 'user'));
            
            if ($pdf) {
                foreach ($emails as $recipient) {
                    Mail::to($recipient)->send(new CreditNoteShared($creditNote, $pdf->output()));
                }
            }

            $request->session()->flash('success', trans('messages.credit_note_mail_sent'));
            return redirect()->route('credit-notes.show', ['creditNote' => $creditNote]);
        }  catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
    }

}
