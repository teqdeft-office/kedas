<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Validator;

use Maatwebsite\Excel\HeadingRowImport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

use App\Imports\ClientsImport;
use App\Imports\SuppliersImport;
use App\Imports\InventoriesImport;
use App\Imports\ItemCategoriesImport;

/*HeadingRowFormatter::extend('custom', function($value) {
    return strtolower($value); 
});*/

class ImportController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Display a form to import the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showForm($resource)
    {
        switch ($resource) {
            case 'clients':
                $mustHaveFields = ['name', 'email'];
                break;
            case 'suppliers':
                $mustHaveFields = ['name', 'email'];
                break;
            case 'inventories':
                $mustHaveFields = ['name', 'sale price', 'tax', 'type of item'];
                break;
            case 'item-categories':
                $mustHaveFields = ['name'];
                break;
            
            default:
                $mustHaveFields = ['name', 'email'];
                break;
        }
        
        return view('import.import-form', compact('resource', 'mustHaveFields'));
    }

    /**
     * Process Excel data to import the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function processImport($resource, Request $request)
    {
        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                'file' => 'required|file|mimes:xls,xlsx'
            ],
            [
                // 'file.mimes' => trans('messages.invalid_excel_file_error')
            ]
            );

            if ($validator->fails()) {
                $request->session()->flash('error', $validator->errors()->all()[0]);
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $file = $request->file('file');

            // $extensions = array("xls", "xlsx", "xlm", "xla", "xlc", "xlt", "xlw");
            $extensions = array("xls", "xlsx");
            $result = array($file->getClientOriginalExtension());

            if (!in_array($result[0], $extensions)) {
                $request->session()->flash('error', trans('messages.invalid_excel_file_error'));
                return back();
            }

            $headings = (new HeadingRowImport)->toCollection($file)->flatten(1);

            switch ($resource) {
                case 'clients':
                    $mustHaveFields = ['name', 'email'];
                    $importInstance = new ClientsImport;
                    $redirectTo = 'clients.index';
                    break;
                case 'suppliers':
                    $mustHaveFields = ['name', 'email'];
                    $importInstance = new SuppliersImport;
                    $redirectTo = 'suppliers.index';
                    break;
                case 'inventories':
                    $mustHaveFields = ['name', 'sale price', 'tax', 'type of item'];
                    $importInstance = new InventoriesImport;
                    $redirectTo = 'inventories.index';
                    break;
                case 'item-categories':
                    $mustHaveFields = ['name'];
                    $importInstance = new ItemCategoriesImport;
                    $redirectTo = 'item-categories.index';
                    break;
                
                default:
                    $importInstance = null;
                    $mustHaveFields = ['name', 'email'];
                    $redirectTo = 'dashboard';
                    break;
            }

            $validFile = $headings->every(function ($value, $key) use ($mustHaveFields) {
                return count(array_intersect(arrayEleToSlug($mustHaveFields), $value->filter()->values()->toArray())) == count($mustHaveFields);
            });

            if (!$validFile) {
                $request->session()->flash('error', trans('labels.excel_fields_having_line')." ".implode(', ', $mustHaveFields)." fields.");
                return back();
            }

            $excel = Excel::import($importInstance, $file);
            $request->session()->flash('success', trans('messages.import_success'));
            return redirect()->route($redirectTo);

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $error = reset($failures);
            $validationError = $error->errors()[0];
            $request->session()->flash('error', $validationError." ".trans('labels.at_row')." ".$error->row());
            return back();
        }
    }
}
