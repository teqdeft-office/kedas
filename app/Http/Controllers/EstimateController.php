<?php

namespace App\Http\Controllers;

use App\Estimate;
use App\Supplier;
use App\Client;
use App\Item;
use App\Invoice;
use Carbon\Carbon;
use App\Validators\EstimateValidator;
use App\Mail\EstimateShared;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use DB;
use Auth;
use Config;
use Image;
use PDF;
use Cache;

class EstimateController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Estimate::class, 'estimate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $estimates = $user->company->estimates;
        foreach ($estimates as $key => $value) {
            if($value->expiration_date < Carbon::now()->format('Y-m-d')){
                $value->status = trans('labels.expired');;
            }else{
                $value->status = trans('labels.active');
            }
        }
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $estimates]);
        }
        return view('estimates.index', compact('estimates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $currencies = Cache::get('currencies');

        $next_estimate_number = $user->company->estimates()->withTrashed() ? $user->company->estimates()->withTrashed()->count() + 1 : 1;

        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.estimate_without_contacts'));
            return redirect('dashboard');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.estimate_without_items'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();

        return view('estimates.create', compact('next_estimate_number', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'inventory_options', 'inventory_attributes', 'tax_attributes',  'currency_attributes', 'currency_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estimateValidator = new EstimateValidator();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$estimateValidator->with($input)->passes()) {
                $request->session()->flash('error', $estimateValidator->getErrors()[0]);
                return back()
                    ->withErrors($estimateValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()
                    ->withInput();
            }

            $estimateitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
            })->values()->toArray();

            if (!$estimateitems) {
                $request->session()->flash('error', trans('messages.estimate_without_items'));
                return back()
                    ->withInput();
            }

            $estimate = $contact->estimates()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['estimate_start_date'],
                'expiration_date' => $input['estimate_expiration_date'],
                'notes' => @$input['notes'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $estimate->items()->createMany($estimateitems);

            if (isset($input['logo']) && $input['logo']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }
                $user->company->signature = $filename;
                $user->company->save();
            }

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.estimate_created'));
            return redirect()->route('estimates.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function show(Estimate $estimate, $type = self::SALE)
    {
        return view('estimates.show', compact('estimate', 'type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Estimate $estimate)
    {
        $estimate->contact_custom_id = ($estimate->contact instanceof Supplier ? 'supplier' : 'client') .'-'. $estimate->contact->id;

        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.estimate_without_contacts'));
            return redirect('dashboard');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.estimate_without_items'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        return view('estimates.edit', compact('estimate', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'inventory_options', 'inventory_attributes', 'tax_attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estimate $estimate)
    {
        $estimateValidator = new EstimateValidator('update');
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$estimateValidator->with($input)->passes()) {
                $request->session()->flash('error', $estimateValidator->getErrors()[0]);
                return back()
                    ->withErrors($estimateValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()
                    ->withInput();
            }

            $estimateitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
                })->values();
            $updateEstimateItems = $estimateitems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertEstimateItems = $estimateitems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteEstimateItemsExcept = $updateEstimateItems->pluck('id')->toArray();

            if (!$estimateitems) {
                $request->session()->flash('error', trans('messages.estimate_without_items'));
                return back()
                    ->withInput();
            }

            $estimate->contact()->associate($contact);

            $estimate->update([
                'start_date' => $input['estimate_start_date'],
                'expiration_date' => $input['estimate_expiration_date'],
                'notes' => $input['notes'] ?? null
            ]);

            if ($deleteEstimateItemsExcept) {
                $estimate->items()->whereNotIn('id', $deleteEstimateItemsExcept)->delete();
            }

            if ($insertEstimateItems) {
                $estimate->items()->createMany($insertEstimateItems);
            }

            if ($updateEstimateItems) {
                foreach ($updateEstimateItems as $updateEstimateItem) {
                    $updatedDate = $updateEstimateItem;
                    unset($updatedDate['id']);
                    Item::findOrFail($updateEstimateItem['id'])->update($updatedDate);
                }
            }

            if (isset($input['logo']) && $input['logo'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }

                $user->company->signature = $filename;
                $user->company->save();
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.estimate_updated'));
            return redirect()->route('estimates.index');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Estimate $estimate)
    {
        try {
            $estimate->delete();
            $request->session()->flash('success', trans('messages.estimate_deleted'));
            return redirect()->route('estimates.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }

    /**
     * Download the specified resource as PDF.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request, Estimate $estimate)
    {
        try {
            $user = $request->user();
            $pdf = PDF::loadView('pdf.estimate', compact('estimate', 'user'));
            $filename = 'estimate-'.$estimate->internal_number.'.pdf';
            return $pdf->download($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Print the specified resource.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request, Estimate $estimate)
    {
        try {
            $user = $request->user();
            $showView = $request->input('view');
            if ($showView) {
                return view('pdf.estimate', compact('estimate', 'user'));
            }
            $filename = 'estimate-'.$estimate->internal_number.'.pdf';
            $print = true;
            $pdf = PDF::loadView('pdf.estimate', compact('estimate', 'print', 'user'));
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->inline($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * email the specified resource.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function mail(Request $request, Estimate $estimate)
    {
        try {
            $user = $request->user();
            $input = $request->all();
            $emails = collect(explode(',', $input['emails']));
            $emails = $emails->map(function ($item, $key) {
                $item = trim(strtolower($item));
                return $item;
            })->uniqueStrict()->filter();

            if ($emails->isEmpty()) {
                $request->session()->flash('error', trans('messages.estimate_mail_without_email'));
                return redirect()->route('estimates.show', ['estimate' => $estimate]);
            }

            $pdf = PDF::loadView('pdf.estimate', compact('estimate', 'user'));
            
            if ($pdf) {
                foreach ($emails as $recipient) {
                    Mail::to($recipient)->send(new EstimateShared($estimate, $pdf->output()));
                }
            }

            $request->session()->flash('success', trans('messages.estimate_mail_sent'));
            return redirect()->route('estimates.show', ['estimate' => $estimate]);
        }  catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
    }

    public function getClone(Request $request, Estimate $estimate) {
        try {
            $itemsData = $estimate->items->toArray();
            $data = $estimate->attributesToArray();
            $data = array_except($data, ['created_at', 'updated_at', 'deleted_at']);
            $data['term'] = 0;
            $itemsData = $estimate->items->map(function($item){
                unset($item['estimate_id']);
                unset($item['created_at']);
                unset($item['updated_at']);
                unset($item['deleted_at']);
                return $item;
            })->toArray();

            // create invoice from estimate //
            $invoice = Invoice::create($data);
            $invoice->items()->createMany($itemsData);


            // delete estimate after creating invoice //
            $estimate->delete();
            $estimate->items->delete();

            $request->session()->flash('success', trans('messages.estimate_approved'));
            return redirect()->route('estimates.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
      }
}
