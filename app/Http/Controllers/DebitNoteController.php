<?php

namespace App\Http\Controllers;

use App\DebitNote;
use App\Supplier;
use App\Client;
use App\ConceptItem;
use App\Item;
use App\Inventory;

use App\Validators\DebitNoteValidator;
use Illuminate\Http\Request;

use DB;
use Auth;
use Config;
use Cache;
use File;

class DebitNoteController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(DebitNote::class, 'debit_note');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $debitNotes = $user->company->debitNotes;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $debitNotes->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'contact' => $item->contact,
                    // 'contact_name' => $item->contact->name,
                    'start_date' => $item->start_date,
                    'remaining' => $item->calculation['remaining_amount'],
                    'total' => $item->calculation['total'],
                    'calculation' => $item->calculation,
                    'notes' => $item->notes,
                    'contact' => $item->contact,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('debit-notes.index', compact('debitNotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $currencies = Cache::get('currencies');
        
        $next_number = $user->company->debitNotes()->withTrashed() ? $user->company->debitNotes()->withTrashed()->count() + 1 : 1;

        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.debit_notes_without_contacts'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products[trans('labels.inventories')] = $trackedInventories;
        }
        $concept_options_with_products[trans('labels.concepts')] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products[trans('labels.inventories')]->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products[trans('labels.concepts')]->pluck('title', 'custom_id')->toArray()
        ];

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        return view('debit-notes.create', compact('next_number', 'contact_options', 'contact_attributes', 'tax_options', 'tax_attributes', 'concept_options_with_products', 'concept_options_with_products_attr', 'currency_attributes', 'currency_options', 'trackedInventories', 'all_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $debitNoteValidator = new DebitNoteValidator();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$debitNoteValidator->with($input)->passes()) {
                $request->session()->flash('error', $debitNoteValidator->getErrors()[0]);
                return back()
                    ->withErrors($debitNoteValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $debitNoteItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($debitNoteItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.debit_notes_without_items'));
                return back()
                    ->withInput();
            }

            $debitNote = $contact->debitNotes()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['debit_note_date'],
                'notes' => @$input['notes'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $debitNote->conceptItems()->createMany($conceptItems);

            $debitNote->items()->createMany($inventoryItems);

            // decrease the quantity of each item's inventory while creating the debit note.
            $debitNote->items->each(function($item) {
                $item->inventory->quantity = $item->inventory->quantity - $item->quantity;
                $item->inventory->save();
            });

             /* upload documents */
             if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/debit-note-'.$file));
                        $documents_data[] = [
                            'name' => 'debit-note-'.$file,
                        ];
                    }
                }

                $documents = $debitNote->documents()->createMany($documents_data);
            }

             /* Save exchange rate */
             if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.debit_note_created'));
            return redirect()->route('debit-notes.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function show(DebitNote $debitNote)
    { 
        return view('debit-notes.show', compact('debitNote'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, DebitNote $debitNote)
    {
        $debitNote->contact_custom_id = ($debitNote->contact instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $debitNote->contact->id;

        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.debit_notes_without_contacts'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products['inventories'] = $trackedInventories;
        }
        $concept_options_with_products['concepts'] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products['inventories']->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products['concepts']->pluck('title', 'custom_id')->toArray()
        ];

        $debitNote->conceptItemsWithProducts = $debitNote->conceptItems->concat($debitNote->items);

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        return view('debit-notes.edit', compact('debitNote', 'tax_options', 'contact_options', 'contact_attributes', 'tax_attributes', 'concept_options_with_products', 'concept_options_with_products_attr', 'all_accounts_options', 'trackedInventories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DebitNote $debitNote)
    {
        $debitNoteValidator = new DebitNoteValidator('update');

        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$debitNoteValidator->with($input)->passes()) {
                $request->session()->flash('error', $debitNoteValidator->getErrors()[0]);
                return back()
                    ->withErrors($debitNoteValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::CONCEPT) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::INVENTORY) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $debitNoteItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($debitNoteItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.debit_note_without_items'));
                return back()
                    ->withInput();
            }

            // The following is for debitNote's items

            // map inventory id as key and quantity as value from submit form value.
            $debit_note_inventory_quantity = $inventoryItems->groupBy('inventory_id')->map(function($it) {
                return $it->sum('quantity');
            });
            // map inventory id as key and quantity as value from existing debitNote's item value.
            $existing_inventory_quantity = $debitNote->items->groupBy('inventory_id')->map(function($it) {
                return $it->sum('quantity');
            });

            // map inventory id as key and quantity as value as new quantity for eack inventory.
            $inventory_quantity = $debit_note_inventory_quantity->map(function($iiq, $inventoryId) use($existing_inventory_quantity){
                return (($existing_inventory_quantity[$inventoryId] ?? 0) - $iiq);
            })->union($existing_inventory_quantity->diffKeys($debit_note_inventory_quantity));

            $updateDebitNoteItems = $inventoryItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertDebitNoteItems = $inventoryItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteDebitNoteItemsExcept = $updateDebitNoteItems->pluck('id')->toArray();

            // The following is for debitNote's concept items
            $updateDebitNoteConceptItems = $conceptItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertDebitNoteConceptItems = $conceptItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteDebitNoteConceptItemsExcept = $updateDebitNoteConceptItems->pluck('id')->toArray();

            $debitNote->contact()->associate($contact);

            // Update basic information.
            $debitNote->update([
                'start_date' => $input['debit_note_date'],
                'notes' => @$input['notes']
            ]);

            if ($deleteDebitNoteItemsExcept) {
                $debitNote->items()->whereNotIn('id', $deleteDebitNoteItemsExcept)->delete();
            }

            if ($insertDebitNoteItems) {
                $debitNote->items()->createMany($insertDebitNoteItems);
            }

            if ($updateDebitNoteItems) {
                foreach ($updateDebitNoteItems as $updateDebitNoteItem) {
                    $updatedData = $updateDebitNoteItem;
                    unset($updatedData['id']);
                    Item::findOrFail($updateDebitNoteItem['id'])->update($updatedData);
                }
            }


            
            $debit_note_total = $input['debit_note_total_hidden'] ?? $debitNote->calculation['total'];
            // associate debit note to invoices.
            $payments = $input['payments'] ?? [];
            $paymentItems = collect($payments)->filter(function($row) {
             return $row['invoice_id'] && (isset($row['amount']) && $row['amount'] > 0);
            })->values();
 
            // remove the association of debit note with other invoices. 
            if ($paymentItems->isEmpty()) {
                $debitNote->invoices()->detach();
            } else {
                $pluckedInvoices = $debitNote->contact->invoices->pluck('calculation', 'id')->map(function($value, $key) {
                 return $value['pendingAmountWithoutDebitNote'];
                })->toArray();

                $amountCorrect = $paymentItems->every(function ($value, $key) use ($pluckedInvoices) {
                    return $value['amount'] <= $pluckedInvoices[$value['invoice_id']];
                });

                if (!$amountCorrect) {
                    $request->session()->flash('error', trans('messages.debit_note_with_incorrect_amount'));
                    return back()->withInput();
                }

                $total_amount = $paymentItems->sum('amount');

                if ($total_amount > $debit_note_total) {
                    $request->session()->flash('error', trans('messages.debit_note_with_incorrect_amount'));
                    return back()->withInput();
                }
                $debitNote->invoices()->sync($paymentItems->keyBy('invoice_id')->toArray());
            }

            if ($deleteDebitNoteConceptItemsExcept) {
                $debitNote->conceptItems()->whereNotIn('id', $deleteDebitNoteConceptItemsExcept)->delete();
            }

            if ($insertDebitNoteConceptItems) {
                $debitNote->conceptItems()->createMany($insertDebitNoteConceptItems);
            }

            if ($updateDebitNoteConceptItems) {
                foreach ($updateDebitNoteConceptItems as $updateDebitNoteConceptItem) {
                    $updatedData = $updateDebitNoteConceptItem;
                    unset($updatedData['id']);
                    ConceptItem::findOrFail($updateDebitNoteConceptItem['id'])->update($updatedData);
                }
            }

            if ($inventoryItems->isEmpty()) {
                $debitNote->items()->delete();
            }

            if ($conceptItems->isEmpty()) {
                $debitNote->conceptItems()->delete();
            }

            // update the quantity of each item's inventory according to mapped data.
            $inventory_quantity->each(function($quantity, $inventoryId) {
                $inventory = Inventory::findOrFail($inventoryId);
                $inventory->quantity = $inventory->quantity + $quantity;
                $inventory->save();
            });

            /* update documents start */
            $documents = $request->input('documents', []);

            if ($debitNote->documents->isNotEmpty()) {
                foreach ($debitNote->documents as $document) {
                    if (!in_array($document->name, $documents)) {
                        $debitNote->documents()->where('id', '=', $document->id)->delete();
                    }
                }
            }
        
            $alreadyExistDocuments = $debitNote->documents->pluck('name')->toArray();

            $dir = public_path("storage/documents");

            foreach ($request->input('documents', []) as $file) {
            
                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
                    if (File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/debit-note-'.$file));
                        $document_data = array('name' => 'debit-note-'.$file);
                        $debitNote->documents()->create($document_data);
                    }
                }
            }
            /* update documents end */

            DB::commit();

            $request->session()->flash('success', trans('messages.debit_note_updated'));
            return redirect()->route('debit-notes.index');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DebitNote $debitNote)
    {
        try {
            $debitNote->items()->each(function($debitNoteItem) {
                $debitNoteItem->inventory->quantity = $debitNoteItem->inventory->quantity + $debitNoteItem->quantity;
                $debitNoteItem->inventory->save();
            });
            $debitNote->delete();
            $request->session()->flash('success', trans('messages.debit_note_deleted'));
            return redirect()->route('debit-notes.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
