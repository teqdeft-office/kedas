<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\Supplier;
use App\Client;
use App\ConceptItem;
use App\Item;
use App\Inventory;

use App\Validators\PurchaseOrderValidator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use Config;
use Cache;
use File;

class PurchaseOrderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(PurchaseOrder::class, 'purchase_order');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $purchaseOrders = $user->company->purchaseOrders;
        foreach ($purchaseOrders as $key => $value) {
            if($value->expiration_date < Carbon::now()->format('Y-m-d')){
                $value->status = trans('labels.expired');;
            }else{
                $value->status = trans('labels.active');
            }
        }
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $purchaseOrders]);
        }
        return view('purchase-orders.index', compact('purchaseOrders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $currencies = Cache::get('currencies');
        $next_number = $user->company->purchaseOrders()->withTrashed() ? $user->company->purchaseOrders()->withTrashed()->count() + 1 : 1;

        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.purchase_orders_without_contacts'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products[trans('labels.inventories')] = $trackedInventories;
        }
        $concept_options_with_products[trans('labels.concepts')] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products[trans('labels.inventories')]->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products[trans('labels.concepts')]->pluck('title', 'custom_id')->toArray()
        ];

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        return view('purchase-orders.create', compact('next_number', 'contact_options', 'contact_attributes', 'tax_options', 'tax_attributes', 'concept_options_with_products', 'concept_options_with_products_attr', 'currency_attributes', 'currency_options', 'trackedInventories', 'all_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchaseOrderValidator = new PurchaseOrderValidator();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$purchaseOrderValidator->with($input)->passes()) {
                $request->session()->flash('error', $purchaseOrderValidator->getErrors()[0]);
                return back()
                    ->withErrors($purchaseOrderValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $purchaseOrderItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($purchaseOrderItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.purchase_orders_without_items'));
                return back()
                    ->withInput();
            }

            $purchaseOrder = $contact->purchaseOrders()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['purchase_order_date'],
                'expiration_date' => $input['purchase_order_expiration_date'],
                'tac' => @$input['tac'],
                'notes' => @$input['notes'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $purchaseOrder->conceptItems()->createMany($conceptItems);

            $purchaseOrder->items()->createMany($inventoryItems);

            /* upload documents */
            if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/purchase-order-'.$file));
                        $documents_data[] = [
                            'name' => 'purchase-order-'.$file,
                        ];
                    }
                }

                $documents = $purchaseOrder->documents()->createMany($documents_data);
            }

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.purchase_order_created'));
            return redirect()->route('purchase-orders.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder $purchaseOrder, $type = self::SUPPLIER)
    { 
        return view('purchase-orders.show', compact('purchaseOrder', 'type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $purchaseOrder, Request $request)
    {
        $purchaseOrder->contact_custom_id = ($purchaseOrder->contact instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $purchaseOrder->contact->id;

        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.purchase_orders_without_contacts'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];
        
        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products['inventories'] = $trackedInventories;
        }
        $concept_options_with_products['concepts'] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products['inventories']->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products['concepts']->pluck('title', 'custom_id')->toArray()
        ];

        $purchaseOrder->conceptItemsWithProducts = $purchaseOrder->conceptItems->concat($purchaseOrder->items);

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        return view('purchase-orders.edit', compact('purchaseOrder', 'tax_options', 'contact_options', 'contact_attributes', 'tax_attributes', 'concept_options_with_products', 'concept_options_with_products_attr', 'all_accounts_options', 'trackedInventories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseOrder $purchaseOrder)
    {
        $purchaseOrderValidator = new PurchaseOrderValidator('update');

        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$purchaseOrderValidator->with($input)->passes()) {
                $request->session()->flash('error', $purchaseOrderValidator->getErrors()[0]);
                return back()
                    ->withErrors($purchaseOrderValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::CONCEPT) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::INVENTORY) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $purchaseOrderItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($purchaseOrderItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.purchase_order_without_items'));
                return back()->withInput();
            }

            $updatePurchaseOrderItems = $inventoryItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertPurchaseOrderItems = $inventoryItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deletePurchaseOrderItemsExcept = $updatePurchaseOrderItems->pluck('id')->toArray();

            // The following is for purchaseOrder's concept items
            $updatePurchaseOrderConceptItems = $conceptItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertPurchaseOrderConceptItems = $conceptItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deletePurchaseOrderConceptItemsExcept = $updatePurchaseOrderConceptItems->pluck('id')->toArray();

            $purchaseOrder->contact()->associate($contact);

            // update basic information
            $purchaseOrder->update([
                'start_date' => $input['purchase_order_date'],
                'expiration_date' => $input['purchase_order_expiration_date'],
                'tac' => @$input['tac'],
                'notes' => @$input['notes']
            ]);

            if ($deletePurchaseOrderItemsExcept) {
                $purchaseOrder->items()->whereNotIn('id', $deletePurchaseOrderItemsExcept)->delete();
            }

            if ($insertPurchaseOrderItems) {
                $purchaseOrder->items()->createMany($insertPurchaseOrderItems);
            }

            if ($updatePurchaseOrderItems) {
                foreach ($updatePurchaseOrderItems as $updatePurchaseOrderItem) {
                    $updatedData = $updatePurchaseOrderItem;
                    unset($updatedData['id']);
                    Item::findOrFail($updatePurchaseOrderItem['id'])->update($updatedData);
                }
            }

            if ($deletePurchaseOrderConceptItemsExcept) {
                $purchaseOrder->conceptItems()->whereNotIn('id', $deletePurchaseOrderConceptItemsExcept)->delete();
            }

            if ($insertPurchaseOrderConceptItems) {
                $purchaseOrder->conceptItems()->createMany($insertPurchaseOrderConceptItems);
            }

            if ($updatePurchaseOrderConceptItems) {
                foreach ($updatePurchaseOrderConceptItems as $updatePurchaseOrderConceptItem) {
                    $updatedData = $updatePurchaseOrderConceptItem;
                    unset($updatedData['id']);
                    ConceptItem::findOrFail($updatePurchaseOrderConceptItem['id'])->update($updatedData);
                }
            }

            if ($inventoryItems->isEmpty()) {
                $purchaseOrder->items()->delete();
            }

            if ($conceptItems->isEmpty()) {
                $purchaseOrder->conceptItems()->delete();
            }

            /* update documents start */
            $documents = $request->input('documents', []);

            if ($purchaseOrder->documents->isNotEmpty()) {
                foreach ($purchaseOrder->documents as $document) {
                    if (!in_array($document->name, $documents)) {
                        $purchaseOrder->documents()->where('id', '=', $document->id)->delete();
                    }
                }
            }
        
            $alreadyExistDocuments = $purchaseOrder->documents->pluck('name')->toArray();

            $dir = public_path("storage/documents");

            foreach ($request->input('documents', []) as $file) {
            
                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
                    if (File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/purchase-order-'.$file));
                        $document_data = array('name' => 'purchase-order-'.$file);
                        $purchaseOrder->documents()->create($document_data);
                    }
                }
            }
            /* update documents end */

            DB::commit();

            $request->session()->flash('success', trans('messages.purchase_order_updated'));
            return redirect()->route('purchase-orders.index');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PurchaseOrder $purchaseOrder)
    {
        try {
            $purchaseOrder->delete();
            $request->session()->flash('success', trans('messages.purchase_order_deleted'));
            return redirect()->route('purchase-orders.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
