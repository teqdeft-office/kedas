<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Supplier;
use App\Client;
use App\Item;
use App\Invoice;
use App\ConceptItem;
use App\Tax;

use App\Validators\TransactionValidator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionCreated;
use App\Events\PaymentCreatedOrUpdated;

use App\Services\TransactionService;
use App\Exceptions\ServiceException;

use App\Http\Resources\TransactionCollection;

use DB;
use Auth;
use Config;
use Image;
use File;
use Cache;
use PDF;

class TransactionController extends Controller
{
    public function __construct(TransactionService $transactionService)
    {
        $this->authorizeResource(Transaction::class, 'transaction');
        $this->transactionService = $transactionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type)
    {
        $user = $request->user();

        $transactions = $user->company->transactions->where('type', $type)->values();

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = new TransactionCollection($transactions);
            /*$jsonCollection = collect();
            $transactions->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'id' => $item->id,
                    'receipt_number' => $item->receipt_number,
                    'voucher_number' => $item->voucher_number,

                    'contact' => $item->contact,
                    'detail_line' => showIfAvailable($item->detail_line),

                    'start_date' => $item->start_date,
                    'end_date' => $item->end_date,
                    'frequency' => $item->frequency,
                    'frequency_type' => $item->frequency_type,

                    'amount' => $item->amount,
                    'bank_account' => $item->bankAccount ? $item->bankAccount->name : 'N/A',
                    'annotation' => $item->annotation,
                    'observation' => $item->observation,
                ]);
            });*/
            return response()->json(['data' => $jsonCollection]);
        }
        return view('transactions.'.$type.'.index', compact('transactions', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $type)
    {
        $user = $request->user();
        $currencies = Cache::get('currencies');

        $user_currency = $user->company->currency;
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;        

        $next_number = $user->company->transactions()->withTrashed() ? $user->company->transactions()->withTrashed()->where('type', $type)->count() + 1 : 1;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty() && $type == 'in') {
            $request->session()->flash('error', trans('messages.transaction_without_contacts'));
            return redirect('dashboard');
        }

        $inventories = $user->company->inventories;
        $taxes = $user->company->taxes;
        $accounting_heads = $user->company->accountingHeads;

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        // remove payment_accounts_options and use accounting head instead.
        $payment_accounts_options = $this->mapOptionsBasedOnLocale(Config::get('constants.payment_accounts'));
        $payment_methods_options = $this->mapOptionsBasedOnLocale(Config::get('constants.payment_methods'));


        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();
        
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();
        
        // The following is used for concept list.
        $all_accounts_options = collect([]);
        $bank_accounts_options = collect([]);

        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        // The following is used for Destination list.
        $bank_account = $accounting_heads->where('code', self::BANKCODE)->first();
            
        $this->generateAccountsLevels($bank_account, $bank_accounts_options, 1);

        return view('transactions.'.$type.'.create', compact('next_number', 'contact_options', 'contact_attributes', 'type', 'payment_methods_options', 'tax_options', 'tax_attributes', 'currency_attributes', 'currency_options', 'all_accounts_options', 'bank_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {

        $transactionValidator = new TransactionValidator($type);
        $input = $request->all();

        if (!$transactionValidator->with($input)->passes()) {
            $request->session()->flash('error', $transactionValidator->getErrors()[0]);
            return back()
                ->withErrors($transactionValidator->getValidator())
                ->withInput();
        }

        try {
            DB::beginTransaction();
            $invoice = $this->transactionService->store($request, $type);
            DB::commit();
            $request->session()->flash('success', trans('messages.transaction_created'));
            return redirect()->route('transactions.index', ['type' => $type]);     
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return view('transactions.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction, Request $request)
    {
        $type = $transaction->type;
        $user = $request->user();
        $transaction->contact_custom_id = $transaction->contact ? (($transaction->contact instanceof Supplier ? 'supplier' : 'client') .'-'. $transaction->contact->id) : null;

        $next_number = $transaction->receipt_number ?: $transaction->voucher_number;
        
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty() && $type == 'in') {
            $request->session()->flash('error', trans('messages.transaction_without_contacts'));
            return redirect('dashboard');
        }

        $inventories = $user->company->inventories;
        $taxes = $user->company->taxes;
        $accounting_heads = $user->company->accountingHeads;

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        // remove payment_accounts_options and use accounting head instead.
        // $payment_accounts_options = $this->mapOptionsBasedOnLocale(Config::get('constants.payment_accounts'));
        $payment_methods_options = $this->mapOptionsBasedOnLocale(Config::get('constants.payment_methods'));

        // The following is used for concept list.
        $all_accounts_options = collect([]);
        $bank_accounts_options = collect([]);

        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        // The following is used for Destination list.
        $bank_account = $accounting_heads->where('code', self::BANKCODE)->first();
            
        $this->generateAccountsLevels($bank_account, $bank_accounts_options, 1);

        // why this because consistency for with other module.
        $transaction->conceptItemsWithProducts = $transaction->conceptItems->concat([]);

        return view('transactions.'.$transaction->type.'.edit', compact('next_number', 'contact_options', 'contact_attributes', 'payment_methods_options', 'transaction', 'type', 'tax_attributes', 'tax_options', 'all_accounts_options', 'bank_accounts_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {

        $type = $transaction->type;
        $input = $request->all();

        $transactionValidator = new TransactionValidator($transaction->type, 'update');

        if (!$transactionValidator->with($input)->passes()) {
            $request->session()->flash('error', $transactionValidator->getErrors()[0]);
            return back()
                ->withErrors($transactionValidator->getValidator())
                ->withInput();
        }

        try {
            DB::beginTransaction();
            $transaction = $this->transactionService->update($request, $transaction);
            DB::commit();
            $request->session()->flash('success', trans('messages.transaction_updated'));
            return redirect()->route('transactions.index', ['type' => $transaction->type]);

        } catch (ServiceException $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return redirect()->route('transactions.index', ['type' => $transaction->type]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Transaction $transaction)
    {
        try {
            DB::beginTransaction();
            $this->transactionService->destroy($request, $transaction);
            DB::commit();
            $request->session()->flash('success', trans('messages.transaction_deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
        } finally {
            return redirect()->route('transactions.index', ['type' => $transaction->type]);
        }
    }

    /**
     * Print the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request, Transaction $transaction)
    {
        try {
            $user = $request->user();
            $showView = $request->input('view');
            if ($showView) {
                return view('pdf.transaction-template', compact('transaction', 'user'));
            }
            $filename = 'transaction-'.$transaction->receipt_number.'.pdf';
            $print = true;
            $pdf = PDF::loadView('pdf.transaction-template', compact('transaction', 'print', 'user'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->inline($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}