<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use Image;
use App\ItemCategory;
use App\Validators\ItemCategoryValidator;

class ItemCategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ItemCategory::class, 'item_category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $itemCategories = $user->company->itemCategories;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $itemCategories->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'description' => $item->description
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('item-categories.index', compact('itemCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ItemCategoryValidator $itemCategoryValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$itemCategoryValidator->with($input)->passes()) {
                $request->session()->flash('error', $itemCategoryValidator->getErrors()[0]);
                return back()
                    ->withErrors($itemCategoryValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            // $data = [
            //     'name' => $input['name'],
            //     'description' => $input['description'],
            //     'user_id' => $user->id
            // ];
            
            $itemCategory = new ItemCategory([
                'company_id' => $user->company->id,
                'description' => $input['description'],
                'user_id' => $user->id
            ]);
            $itemCategory->setTranslation('name', 'en', $input['name'])->setTranslation('name', 'es', $input['name']);

            

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/item-categories");
                $path = $dir."/".$filename;
                
                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                $itemImgInstance->orientate()->save($path);

                $itemCategory->image = $filename;
                
            }

            // $itemCategory = $user->company->itemCategories()->create($data);
            $itemCategory->save();

            DB::commit();

            $request->session()->flash('success', trans('messages.item_category_created'));
            return redirect()->route('item-categories.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemCategory $itemCategory)
    {
        return view('item-categories.edit', compact('itemCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemCategory $itemCategory)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $itemCategoryValidator = new ItemCategoryValidator('update', $itemCategory);
            
            if (!$itemCategoryValidator->with($input)->passes()) {
                $request->session()->flash('error', $itemCategoryValidator->getErrors()[0]);
                return back()
                    ->withErrors($itemCategoryValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();
            $itemCategory->company_id = $user->company->id;
            $itemCategory->user_id = $user->id;
            $itemCategory->description =  $input['description'];
            $itemCategory->setTranslation('name', 'en', $input['name'])->setTranslation('name', 'es', $input['name']);

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/item-categories");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);
                
                $itemCategory->image = $filename;
            }

            $itemCategory->update($input);

            DB::commit();

            $request->session()->flash('success', trans('messages.item_category_updated'));
            return redirect()->route('item-categories.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ItemCategory $itemCategory)
    {
        try {
            $itemCategory->delete();
            $request->session()->flash('success', trans('messages.item_category_deleted'));
            return redirect()->route('item-categories.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
