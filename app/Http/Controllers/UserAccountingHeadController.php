<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Events\ProfileCompleted;
use App\Validators\Accounting\HeadValidator;

use App\UserAccountingHead;
use Illuminate\Support\Str;

use DB;
use Lang;
use Cache;
use App;

class UserAccountingHeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(UserAccountingHead::class, 'chart');
    }

    /**
     * Get the accounting charts
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*if (App::Environment(['development', 'production'])) {
            return view('app.coming-soon');
        }*/
    	$user = $request->user();
        event(new ProfileCompleted($user));

    	$accounting_heads = $user->company->accountingHeads;
        $parent_accounting_heads = $accounting_heads->whereNull('parent_id')->where('visibility', true);
        $cost_account = $accounting_heads->where('code', self::COGSMAINCODE)->first();

        // $accounting_type_options = ["" => trans('labels.select_account_type')] + $parent_accounting_heads->pluck('name', 'id')->toArray();

        $accounting_type_options = ["" => trans('labels.select_account_type')] + collect($parent_accounting_heads)->mapWithKeys(function($parent_accounting_head) {
            return [$parent_accounting_head['name'] => $parent_accounting_head->children->pluck('name', 'id')];
            // return [$parent_accounting_head->name => [$parent_accounting_head->children->pluck('name', 'id')->toArray()]];
        })->toArray();

        $need_cached_options = false;
        return view('accounting.charts', compact('user', 'accounting_heads', 'accounting_type_options', 'parent_accounting_heads', 'need_cached_options', 'cost_account'));
    }

    /**
     * Add a new accounting charts or head
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $input = $request->all();

        try {
            DB::beginTransaction();
            $input['slug'] = Str::slug($input['account-name'], '-');
            $accountingHeadValidator = new HeadValidator;

            if (!$accountingHeadValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'messages'  => $accountingHeadValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                return back()
                    ->withErrors($accountingHeadValidator->getValidator())
                    ->with([
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            $userAccountingHead = new UserAccountingHead;
            $userAccountingHead->setTranslation('name', 'en', $input['account-name'])->setTranslation('name', 'es', $input['account-name']);

            $userAccountingHead->description = $input['description'];
            $userAccountingHead->custom_code = $input['code'];
            $userAccountingHead->slug = $input['slug'];

            $userAccountingHead->company()->associate($user->company);
            $userAccountingHead->user()->associate($user);
            $userAccountingHead->visibility = true;
            // $userAccountingHead->opening_balance = $input['opening_balance'] ?? null;

            if ($input['node-type'] == self::PRIMARY) {
                $userAccountingHead->save();
            } else {
                $is_sub_account = $input['is_sub_account'] ?? false;
                if ($is_sub_account) {
                    $parentAccountingHead = $user->accountingHeads->where('id', $input['parent-account'])->first();
                } else {
                    $parentAccountingHead = $user->accountingHeads->where('id', $input['account-control'])->first();
                }

                if ($parentAccountingHead) {
                    $userAccountingHead->parent_id = $parentAccountingHead->id;
                    $userAccountingHead->incr_nature = $parentAccountingHead->incr_nature;
                    $userAccountingHead->decr_nature = $parentAccountingHead->decr_nature;
                    $userAccountingHead->has_exception = $parentAccountingHead->has_exception;
                    $userAccountingHead->save();

                    $parentAccountingHead->visibility = true;
                    $parentAccountingHead->save();
                } else {
                    throw new Exception(trans('messages.something_went_wrong'), 1);
                }
            }
            DB::commit();
            Cache::forget('user_accounting_heads_'.$user->company->id);
            Cache::rememberForever('user_accounting_heads_'.$user->company->id, function () use($user) {
                $user->company->load('accountingHeads');
                return $user->company->accountingHeads;
            });
            $response = [
                'message' => Lang::get('messages.accounting_head_created'),
                'success' => true
            ];

            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response);
            }

        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * Update the accounting charts or head
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generateControlOptions(Request $request, UserAccountingHead $userAccountingHead)
    {
        $this->authorize('view', $userAccountingHead);
        $input = $request->all();

        try {
            $type = $input['type'] ?? self::CONTROL;

            $account_control_attributes = [];
            if ($type == self::CONTROL) {
                $account_control_options = $userAccountingHead->children;
                $account_control_attributes = $account_control_options->mapWithKeys(function ($item) {
                    return [$item->id => [
                        'data-can_be_parent' => $item->can_be_parent
                    ]];
                })->toArray();
                $account_control_options = ["" => trans('labels.select_detail_type')] + $account_control_options->pluck('name', 'id')->toArray();
            } else {
                $account_control_options = collect([]);
                $this->generateAccountsLevels($userAccountingHead, $account_control_options);
            }

            return view('accounting.control-options', compact('account_control_options', 'type', 'account_control_attributes'));
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }
    
    /**
     * Update the accounting charts or head
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserAccountingHead $userAccountingHead)
    {
        $user = $request->user();
        $input = $request->all();
        
        try {
            DB::beginTransaction();
            $input['slug'] = Str::slug($input['account-name'], '-');
            $accountingHeadValidator = new HeadValidator('edit', $userAccountingHead);

            if (!$accountingHeadValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'messages'  => $accountingHeadValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                return back()
                    ->withErrors($accountingHeadValidator->getValidator())
                    ->with([
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            if ($userAccountingHead) {
                // $userAccountingHead->setTranslation('name', 'en', $input['account-name'])->setTranslation('name', 'es', $input['account-name']);
                $userAccountingHead->name = $input['account-name'];
                $userAccountingHead->description = $input['description'];
                $userAccountingHead->custom_code = $input['code'];
                // $userAccountingHead->opening_balance = $input['opening_balance'] ?? null;
                $userAccountingHead->slug = $userAccountingHead->accountingHead ? $userAccountingHead->slug : $input['slug'];

                $userAccountingHead->save();
                DB::commit();
                Cache::forget('user_accounting_heads_'.$user->company->id);
                Cache::rememberForever('user_accounting_heads_'.$user->company->id, function () use($user) {
                    $user->company->load('accountingHeads');
                    return $user->company->accountingHeads;
                });

                $response = [
                    'message' => Lang::get('messages.accounting_head_updated'),
                    'success' => true
                ];

                if ($request->wantsJson() || $request->ajax()) {
                    return response()->json($response);
                }

            } else {
                throw new \Exception(trans('messages.something_went_wrong'), 1);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserAccountingHead  $userAccountingHead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UserAccountingHead $userAccountingHead)
    {
        try {
            if (!$userAccountingHead->resource_associated) {
                $userAccountingHead->delete();
                $request->session()->flash('success', trans('messages.accounting_head_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('accounting.charts');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }

}
