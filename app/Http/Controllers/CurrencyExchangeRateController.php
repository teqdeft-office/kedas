<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CurrencyExchangeRateController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Ncf::class, 'ncf');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $currency_rates = $user->currencyRates;
        // prePrint($currency_rates);
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $currency_rates->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'currency' => $item->currency->name,
                    'rate' => $item->rate
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('user-currency-rates.index', compact('currency_rates'));
    }
}
