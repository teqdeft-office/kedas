<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

use Auth;
use DB;
use Image;
use File;
use Cache;
use Config;
use Lang;
use App;
// use Mail;
use App\Validators\InvoiceValidator;
use App\Mail\ReceivableShared;
use App\Mail\InvoiceCreated;
use App\Invoice;
use App\Supplier;
use App\Client;
use App\Item;
use App\ConceptItem;
use App\Inventory;
use App\User;
use App\Estimate;
use App\AccountingReceivable;
use PDF;
use App\AccountAdjustment;
use App\UserAccountingHead;
use Illuminate\Support\Str;
use App\AccountingEntry;
use App\Validators\AccountAdjustmentValidator;

class AccountReceivableController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(AccountAdjustment::class, 'account_adjustment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
       
        $user = $request->user();
        //$company = $user->company();
        $test=AccountingReceivable::test();
    

        // $invoices = $user->company->accountreceivable();  
        // print_r($invoices);die;

        $lang=Config::get('app.locale');
        $user = $request->user();
        $accountAdjustments = AccountingReceivable::query()
        ->where('user_id',$user->id)
        ->where('company_id',$user->company_id)
        ->where('deleted_at',null)
        ->get();

       
        foreach($accountAdjustments as $accountAdjustmen){
          // echo  $accountAdjustments->calculation;
            $id=$accountAdjustmen->id;
            $user_id=$accountAdjustmen->contact_id;
            $contact_type=$accountAdjustmen->contact_type;
            if($contact_type=='App\Client'){
                 $user=DB::table('clients')->where('id',$user_id)->first();
                 $accountAdjustmen->contact=$user->name;
            }
            else{
                 $user=DB::table('suppliers')->where('id',$user_id)->first();
                 $accountAdjustmen->contact=$user->name;
            }
             $items = DB::table('items')
                   ->join('inventories', 'inventories.id', '=', 'items.inventory_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('items.*', 'inventories.name','taxes.percentage')
                   ->get();
             $concept = DB::table('concept_items')
                   ->join('user_accounting_heads', 'user_accounting_heads.id', '=', 'concept_items.user_accounting_head_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'concept_items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('concept_items.*', 'user_accounting_heads.name','taxes.percentage')
                   ->get();
            $paid=DB::table('account_receivable_transaction')->where('account_receivable_id',$id)->sum('amount');
                   $item_discount=$discount_amount=$tax=$total_item=0;
                  $item_discount1=$discount_amount1=$tax1=$total_item1=0;
                  foreach($items as $item){
                         $total=$item->price;
                         if($item->discount !=''){
                              $discount=($item->price*$item->discount/100);
                              $discount_amount=$item->price-$discount;
                         }
                         else{
                              $discount_amount=$item->price;
                         }
                         if($item->percentage !=''){
                             $tax=$discount_amount*$item->percentage/100;
                         }
                         $total=$discount_amount+$tax;
                         $total_item=$total_item+$total;
                    }
                    foreach($concept as $con){
                         $total1=$con->price;
                         if($con->discount !=''){
                              $discount1=($con->price*$con->discount/100);
                              $discount_amount1=$con->price-$discount1;
                         }
                         else{
                              $discount_amount1=$con->price;
                         }
                         if($con->percentage !=''){
                             $tax1=$discount_amount1*$con->percentage/100;
                         }
                         $total1=$discount_amount1+$tax1;
                         $total_item1=$total_item1+$total1;
                    }  
                  $pending=($total_item+$total_item1)-$paid;
                  $accountAdjustmen->total=($total_item+$total_item1);
                  $accountAdjustmen->item_paid=$paid;
                  $accountAdjustmen->item_pending=$pending;
                  if($pending==0){
                      $accountAdjustmen->status='Paid';
                  }
                  else{
                       $accountAdjustmen->status='Overdue';
                  }
                 
        }
       
        
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $accountAdjustments->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'contact' => $item->contact,
                    'start_date' => $item->start_date,
                    'expiration_date' => $item->expiration_date,
                    'total' => $item->total,
                    'item_paid' => $item->item_paid,
                    'item_pending' => $item->item_pending,
                    'status' => $item->status,
                ]);
            });
         
            return response()->json(['data' => $jsonCollection]);
       }
         return view('account-receivable.index', compact('accountAdjustments'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $table = DB::select("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'databaseName' AND TABLE_NAME = 'tableName'");
if (!empty($table)) { echo $table[0]->AUTO_INCREMENT; }
        $user = $request->user();
        $statement = DB::select("SHOW TABLE STATUS LIKE 'account_receivable'");
        $next_invoice_number = $statement[0]->Auto_increment;
        //$next_invoice_number = DB::table('account_receivable')->getNextGeneratedId();

        $accounting_heads = $user->company->accountingHeads;
      
      
       //$where="parent_id=3308 or id= 3308";

       // $parent_accounting_heads = $accounting_heads->where('id', 3307);
       //   print_r($parent_accounting_heads);
        $parent_accounting_heads = $accounting_heads->whereNull('parent_id')->where('accounting_head_id','1')->where('visibility', true);
       
        $currencies = Cache::get('currencies');
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;
        $memberships = $user->company->memberships;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.invoice_without_contacts'));
            return redirect('dashboard');
        }

       

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: '',
                    'data-membership_id' => $item->membership && $item->membership->discount ? $item->membership_id : ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: '',
                    'data-membership_id' => $item->membership && $item->membership->discount ? $item->membership_id : ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();
        
        //$contact_options = ['' => trans('labels.select_contact')] + $contact_options->pluck('name', 'custom_id')->toArray();

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-unit_cost' => $item->unit_cost ?: 0,
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;
        $ncfs = $user->company->ncfs;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
        $term_options = $this->mapOptionsBasedOnLocale(Config::get('constants.terms'));
        $ncf_options = [null => trans('labels.select_ncf')] + $ncfs->pluck('name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products[trans('labels.inventories')] = $trackedInventories;
        }
        $concept_options_with_products[trans('labels.concepts')] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products[trans('labels.inventories')]->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products[trans('labels.concepts')]->pluck('title', 'custom_id')->toArray()
        ];

        $estimate_id = $request->input('estimate_id') ?? 0;
        $purchase_order_id = $request->input('purchase_order_id') ?? 0;
        $remission_id = $request->input('remission_id') ?? 0;

        $resourceData = null;
        if ($estimate_id || $purchase_order_id || $remission_id) {

            if (!$resourceData && $estimate_id) {
                $resourceData = $user->company->estimates()->where('id', $estimate_id)->first();
                $purchase_order_id = $remission_id = 0;
            }
            if (!$resourceData && $purchase_order_id) {
                $resourceData = $user->company->purchaseOrders()->where('id', $purchase_order_id)->first();
                $estimate_id = $remission_id = 0;
            }
            if (!$resourceData && $remission_id) {
                $resourceData = $user->company->remissions()->where('id', $remission_id)->whereNull('invoice_id')->first();
                $estimate_id = $purchase_order_id = 0;
            }

            if ($resourceData) {
                $resourceData->contact_custom_id = ($resourceData->contact instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $resourceData->contact->id;
                Session::flash('_old_input.contact_id', $request->old('contact_id') ?: $resourceData->contact_custom_id);
                Session::flash('_old_input.invoice_start_date', $request->old('invoice_start_date') ?: $resourceData->start_date);
                Session::flash('_old_input.invoice_expiration_date', $request->old('invoice_expiration_date') ?: $resourceData->expiration_date);
                Session::flash('_old_input.exchange_currency', $request->old('exchange_currency') ?: $resourceData->exchange_currency_id);
                Session::flash('_old_input.exchange_rate', $request->old('exchange_rate') ?: $resourceData->exchange_currency_rate);
                Session::flash('_old_input.notes', $request->old('notes') ?: $resourceData->notes);

                if ($purchase_order_id) {
                    $resourceConceptItems = $resourceData->conceptItems->map(function ($item) {
                        return $item->only(['price', 'discount', 'quantity', 'user_accounting_head_id', 'tax_id', 'calculation']);
                        })->map(function($data){
                            $data['item'] = self::CONCEPT .'-'. $data['user_accounting_head_id'];
                            $data['id'] = 0;
                            $data['tax_amount'] = $data['calculation']['taxAmount'];
                            $data['tax'] = $data['tax_id'];
                            unset($data['concept_id']);
                            unset($data['user_accounting_head_id']);
                            unset($data['calculation']);
                            unset($data['tax_id']);
                            return $data;
                    })->toArray();
                }

                $resourceDataItems = $resourceData->items->map(function ($item) {
                    return $item->only(['reference', 'price', 'discount', 'quantity', 'inventory_id', 'tax_id', 'calculation']);
                })->map(function($data) use($purchase_order_id, $estimate_id, $remission_id) {
                    if ($estimate_id || $remission_id) {
                        $data['item'] = $data['inventory_id'];
                    }
                    $data['id'] = 0;
                    $data['tax_amount'] = $data['calculation']['taxAmount'];
                    $data['tax'] = $data['tax_id'];
                    if ($purchase_order_id) {
                        unset($data['reference']);
                        $data['item'] = self::INVENTORY .'-'. $data['inventory_id'];
                    }
                    unset($data['inventory_id']);
                    unset($data['calculation']);
                    unset($data['tax_id']);
                    return $data;
                })->toArray();

                if ($purchase_order_id) {
                    Session::flash('_old_input.concepts', $request->old('concepts') ?: array_merge($resourceConceptItems,$resourceDataItems));
                } else {
                    Session::flash('_old_input.products', $request->old('products') ?: $resourceDataItems);
                }
            }
        }

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        $mapInventoryWithMembership = $inventories->mapWithKeys(function ($inventory) {
            return [$inventory->id => $inventory->memberships->pluck('id')->toArray()];
        })->toArray();

        $mapMembershipWithDiscount = $memberships->whereNotNull('discount')->pluck('discount', 'id')->toArray();

        return view('account-receivable.create', compact('next_invoice_number', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'term_options', 'inventory_options', 'inventory_attributes', 'tax_attributes', 'ncf_options','accounting_heads','parent_accounting_heads', 'concept_options_with_products', 'concept_options_with_products_attr', 'currency_attributes', 'currency_options', 'trackedInventories', 'all_accounts_options', 'remission_id', 'mapInventoryWithMembership', 'mapMembershipWithDiscount'));
    }

    public function store(Request $request, $type = self::SALE)
    {
            $date=date('Y-m-d h:i:s');
            DB::beginTransaction();
            $input = $request->all();
            // print_r($input);die;

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                    return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
                })->filter(function($row) {
                    return $row['quantity'] && $row['price'] && $row['item'];
                })->map(function($item) {
                    list($item_type, $item_id) = explode('-', $item['item']);
                    $item['user_accounting_head_id'] = $item_id;
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    unset($item['tax_amount']);
                    return $item;
                })->values()->toArray();

                $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                    return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
                })->filter(function($row) {
                    return $row['quantity'] && $row['price'] && $row['item'];
                })->map(function($item) {
                    list($item_type, $item_id) = explode('-', $item['item']);
                    $item['inventory_id'] = $item_id;
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    unset($item['description']);
                    unset($item['tax_amount']);
                    return $item;
                })->values()->toArray();

               

           if (!$input['account']) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.account')]));
                return back()
                    ->withInput();
            }
            $user = $request->user();
             list($account, $account_id) = explode('-', $input['account']);
             
            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();
            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()
                    ->withInput();
            }

            if (!$account_id) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.account')]));
                return back()
                    ->withInput();
            }
            if($contact_type=='client'){
                $t="App\Client";
            }else{
                 $t="App\Supplier";
            }
      DB::table('account_receivable')->insert(
           array(
                    'contact_id'=>$contact_id,
                      'contact_type'=>$t,
                       'user_id' => $user->id,
                      'company_id' => $user->company->id,
                      'account_id' => $account_id,
                      'term' => @$input['term'],
                      'manual_days' => @$input['term'] == 'manual' ? @$input['manual_days'] : null,
                      'start_date' => $input['invoice_start_date'],
                      'expiration_date' => $type == self::SALE ? (date('Y-m-d', strtotime($input['invoice_start_date']. ' + '.($input['term'] == 'manual' ? ($input['manual_days'] ?? 1) : ($input['term'] ?? 0) ).' days'))) : ($input['invoice_expiration_date'] ?? null),
                      'ncf_id' => @$input['ncf_id'],
                      'ncf_number' => $input['ncf_number'] ?? null,
                      'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                      'exchange_currency_id' => $input['exchange_currency'] ?? null,
                            'created_at'=>$date
           )
      );
         
      $id = DB::getPdo()->lastInsertId();;
      if($input['concepts']){
                $x=2;
        foreach($inventoryItems as $inventoryItem)
        {
          
          if($inventoryItem['tax_id']==''){
            $tax=null;
          }
          else{
            $tax=$inventoryItem['tax_id'];
          }
          DB::table('items')->insert(
               array(
                        'account_recivable_id' => $id,
                          'inventory_id' => $inventoryItem['inventory_id'],
                          'tax_id' => $tax,
                          'quantity' => $inventoryItem['quantity'],
                          'discount' => $inventoryItem['discount'],
                          'price' => $inventoryItem['price'],
               )
          );
        }
                foreach($conceptItems as $conceptItem)
                {
                    
                    if($conceptItem['tax_id']==''){
                        $tax=null;
                    }
                    else{
                        $tax=$conceptItem['tax_id'];
                    }
                    DB::table('concept_items')->insert(
                         array(
                                    'account_recivable_id' => $id,
                                    'user_accounting_head_id' => $conceptItem['user_accounting_head_id'],
                                    'tax_id' => $tax,
                                    'quantity' => $conceptItem['quantity'],
                                    'discount' => $conceptItem['discount'],
                                    'price' => $conceptItem['price'],
                         )
                    );
                }

      }


             /* upload documents */
             if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/account-recivable-'.$file));
                        DB::table('documents')->insert(
                         array(
                                    'documentable_id' => $id,
                                    'documentable_type' => 'App\Receivable',
                                    'name' => 'account-recivable-'.$file,
                                    'type' => 'document',
                                    'created_at' => $date,
                                    'updated_at' => $date,
                         )
                     );
                    }
                }
            }
             $items = DB::table('items')
                   ->join('inventories', 'inventories.id', '=', 'items.inventory_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('items.*', 'inventories.name','taxes.percentage')
                   ->get();
                          
             $concept = DB::table('concept_items')
                   ->join('user_accounting_heads', 'user_accounting_heads.id', '=', 'concept_items.user_accounting_head_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'concept_items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('concept_items.*', 'user_accounting_heads.name','taxes.percentage')
                   ->get();
           $x=2;
           $date1=date('Y-m-d');
           $total=0;
           $tax=0;
           foreach($items as $item){
               if($item->discount !=''){
                          $discount=($item->price*$item->discount/100);
                         
                          $discount_amount=$item->price-$discount;
                }
                else{
                          $discount_amount=$item->price;
               }
               
               $data=DB::table('accounting_entries')->insert(
                         array(
                                    'recivable_id' => $id,
                                    'amount' => $discount_amount,
                                    'user_accounting_head_id' => '3422',
                                    'order' => $x,
                                    'nature' => 'credit',
                                    'entry_date' => date('Y-m-d'),
                                    'created_at'=>date('Y-m-d h:i:s'),
                         )
              );
              if($item->percentage !=''){
                $x++;
                         $tax=$discount_amount*$item->percentage/100;
                        
                         $item->tax_amount=$tax;
                          $data=DB::table('accounting_entries')->insert(
                         array(
                                    'recivable_id' => $id,
                                    'amount' => $tax,
                                    'user_accounting_head_id' => '8959',
                                    'order' => $x,
                                    'nature' => 'credit',
                                    'entry_date' => date('Y-m-d'),
                                    'created_at'=>date('Y-m-d h:i:s'),
                         )
                     );
               }
               $t=$discount_amount+$tax;
               $total=$total+$t;
               $x++;
           }
           foreach($concept as $item){
               if($item->discount !=''){
                          $discount=($item->price*$item->discount/100);
                         
                          $discount_amount=$item->price-$discount;
                }
                else{
                          $discount_amount=$item->price;
               }
               
               $data=DB::table('accounting_entries')->insert(
                         array(
                                    'recivable_id' => $id,
                                    'amount' => $discount_amount,
                                    'user_accounting_head_id' => $item->user_accounting_head_id,
                                    'order' => $x,
                                    'nature' => 'credit',
                                    'entry_date' => date('Y-m-d'),
                                    'created_at'=>date('Y-m-d h:i:s'),
                         )
              );
              if($item->percentage !=''){
                $x++;
                         $tax=$discount_amount*$item->percentage/100;
                       
                         $item->tax_amount=$tax;
                          $data=DB::table('accounting_entries')->insert(
                         array(
                                    'recivable_id' => $id,
                                    'amount' => $tax,
                                    'user_accounting_head_id' => '8959',
                                    'order' => $x,
                                    'nature' => 'credit',
                                    'entry_date' => date('Y-m-d'),
                                    'created_at'=>date('Y-m-d h:i:s'),
                         )
                     );
               }
               $t=$discount_amount+$tax;
               $total=$total+$t;
               $x++;
           }
           $data=DB::table('accounting_entries')->insert(
                         array(
                                    'recivable_id' => $id,
                                    'amount' => $total,
                                    'user_accounting_head_id' => $account_id,
                                    'order' => 1,
                                    'nature' => 'debit',
                                    'entry_date' => date('Y-m-d'),
                                    'created_at'=>date('Y-m-d h:i:s'),
                         )
                         );


            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }
            DB::commit();
            $request->session()->flash('success', trans('messages.recivable_created'));
            return redirect()->route('receivable.index');
    }
     public function show(Request $request, $id)
    {
    
       
     
       $lang=Config::get('app.locale');
        $account_recivable= DB::table('account_receivable')->where('id',$id)->first();
        if($account_recivable){
            $id=$account_recivable->id;
            $user_id=$account_recivable->contact_id;
            $date=$account_recivable->start_date;
            $exchange_currency_id=$account_recivable->exchange_currency_id;
            if($exchange_currency_id !=''){
                 $currency= DB::table('currencies')->where('id',$exchange_currency_id)->first();
                  $d=json_decode($currency->name);
                  if($lang=='en'){
                       $account_recivable->currency_name=$d->en;
                 }
                 else{
                       $account_recivable->currency_name=$d->es;
                 }
                 $account_recivable->currency_code=$currency->code;
                 $account_recivable->symbol=$currency->symbol;
            }
            $payment_reciver = DB::table('account_receivable_transaction')
                   ->join('transactions', 'transactions.id', '=', 'account_receivable_transaction.transaction_id')
                   ->where('account_receivable_id',$id)
                   ->select('*','account_receivable_transaction.amount as total_amount',)
                   ->get();

              $acc=DB::table('accounting_entries')
                   ->join('user_accounting_heads', 'user_accounting_heads.id', '=', 'accounting_entries.user_accounting_head_id')
                   ->where('recivable_id',$id)
                   ->orderBy('order', 'asc')
                   ->select( 'user_accounting_heads.name','user_accounting_heads.custom_code as code','accounting_entries.*')
                   ->get();

            $contact_type=$account_recivable->contact_type;
            if($contact_type=='App\Client'){
                 $user=DB::table('clients')->where('id',$user_id)->first();
            }
            else{
                 $user=DB::table('suppliers')->where('id',$user_id)->first();
            }
            if($user){
                $documents=DB::table('documents')->where('documentable_id',$id)->where('documentable_type','App\Receivable')->get();
               
            $items = DB::table('items')
                   ->join('inventories', 'inventories.id', '=', 'items.inventory_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('items.*', 'inventories.name','taxes.percentage')
                   ->get();
                          
             $concept = DB::table('concept_items')
                   ->join('user_accounting_heads', 'user_accounting_heads.id', '=', 'concept_items.user_accounting_head_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'concept_items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('concept_items.*', 'user_accounting_heads.name','taxes.percentage')
                   ->get();
            $paid=DB::table('account_receivable_transaction')->where('account_receivable_id',$id)->sum('amount');
                   $total=$total1=0;
                   $discount=$discount1=0;
                   $tax=$tax1=0;
                   $discount_amount=$discount_amount1=0;
                   $item_total=$item_total1=0;
                   $item_paid=$item_paid1=0;
                   $item_pending=$item_pending1=0;
                   $base_total2=$base_total1=0;
                   $total_discount=$total_discount1=0;
                   $total_tax=$total_tax1=0;
                   $array=array();
                 foreach($items as $item){
                     $total=$item->price;
                     if($item->discount !=''){
                          $discount=($item->price*$item->discount/100);
                          $total_discount=$total_discount+$discount;
                          $discount_amount=$item->price-$discount;
                     }
                     else{
                          $discount_amount=$item->price;
                     }
                     if($item->percentage !=''){
                         $tax=$discount_amount*$item->percentage/100;
                         $total_tax=$total_tax+$tax;
                         $item->tax_amount=$tax;
                     }
                     else{

                         $item->tax_amount=0;
                     }
                     $base_total2=$item->price+$base_total2;
                     $total=($total-$discount);
                     $total=$discount_amount+$tax;
                     $item_total=$item_total+$total;
                     $item_paid=$item_paid+0;
                     $item_pending=$item_pending+($total-0);
                     $item->total=$total;
                     array_push($array,$item);
                 }
                // array_push($array,$items);
                 foreach($concept as $con){
                     $total=$con->price;
                     if($con->discount !=''){
                          $discount1=($con->price*$con->discount/100);
                          $total_discount1=$total_discount1+$discount1;
                          $discount_amount1=$con->price-$discount1;
                     }
                     else{
                          $discount_amount1=$con->price;
                     }
                     if($con->percentage !=''){
                         $tax1=$discount_amount1*$con->percentage/100;
                         $total_tax1=$total_tax1+$tax1;
                         $con->tax_amount=$tax1;
                     }
                     else{

                         $con->tax_amount=0;
                     }
                     $d=json_decode($con->name);
                     if($lang=='en'){
                       $con->name=$d->en;
                     }
                     else{
                       $con->name=$d->es;
                     }
                    
                     $base_total1=$con->price+$base_total1;
                     $total1=($total1-$discount1);
                     $total1=$discount_amount1+$tax1;
                     $item_total1=$item_total1+$total1;
                     $item_paid1=$item_paid1+0;
                     $item_pending1=$item_pending1+($total-0);
                     $con->total=$total1;
                     array_push($array,$con);
                 }
                  //array_merge($array,$concept);
                $items=$array;

                 $item_paid=$paid;

                 $subtotal=($base_total2+$base_total1)-($total_discount+$total_discount1);
                 $total_discount=$total_discount+$total_discount1;
                 $total_tax=$total_tax+$total_tax1;
                 $base_total=$base_total2+$base_total1;
                 $item_total=$item_total+$item_total1;
                 $item_pending=$item_total-$item_paid;

                   return view('account-receivable.show',compact('items', 'documents', 'account_recivable', 'user','item_total','item_paid','item_pending','base_total','total_discount','subtotal','total_tax','payment_reciver','acc'));
            }else{
               
              $request->session()->flash('error', trans('messages.no_data_found'));
                return back();
            }
        }else{

            $request->session()->flash('error', trans('messages.no_data_found'));
                return back();
        }
       
    }
     /**
     * Print the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    function getinvoive($id)
    {
        $lang=Config::get('app.locale');
        $account_recivable = AccountingReceivable::query()
        ->where('id',$id)
        ->first();

        if($account_recivable['contact_type']=='App\Client'){
             $user = Client::query()
            ->where('id',$account_recivable['contact_id'])
            ->first();
        }
        else{
             $user = Supplier::query()
            ->where('id',$account_recivable['contact_id'])
            ->first();
        }
       $seller = User::query()
            ->where('id',$account_recivable['user_id'])
            ->first();
        $account_recivable['seller']=$seller;
        $exchange_currency_id=$account_recivable['exchange_currency_id'];
        if($exchange_currency_id !=''){
            $currency= DB::table('currencies')->where('id',$exchange_currency_id)->first();
            $d=json_decode($currency->name);
            if($lang=='en'){
                       $account_recivable['currency_name']=$d->en;
            }
                 else{
                       $account_recivable['currency_name']=$d->es;
            }
            $account_recivable['currency_code']=$currency->code;
            $account_recivable['symbol']=$currency->symbol;
       }
        $account_recivable['user']=$user;
        $items = DB::table('items')
                   ->join('inventories', 'inventories.id', '=', 'items.inventory_id')
                   ->leftjoin('taxes', 'taxes.id', '=', 'items.tax_id')
                   ->where('account_recivable_id',$id)
                   ->select('items.*', 'inventories.name','taxes.percentage')
                   ->get();
        $total=$total1=0;

                   $discount=$discount1=0;
                   $tax=$tax1=0;
                   $discount_amount=$discount_amount1=0;
                   $item_total=$item_total1=0;
                   $item_paid=$item_paid1=0;
                   $item_pending=$item_pending1=0;
                   $base_total2=$base_total1=0;
                   $total_discount=$total_discount1=0;
                   $total_tax=$total_tax1=0;
                   $array=array();
        foreach($items as $item){
          if($item->discount !=''){
                $discount=($item->price*$item->discount/100);
                $discount_amount=$item->price-$discount;
                $item->discount_amount= $discount;
          }
          else{
                  $discount_amount=$item->price;
                  $item->discount_amount= 0;
          }
          if($item->percentage !=''){
                $tax=$discount_amount*$item->percentage/100;
                $item->tax_amount=$tax;
          }
          else{
                  $tax=0;
                  $item->tax_amount=0;
         }
         $total=$discount_amount+$tax;
         $item->total=$total;

         array_push($array,$item);
     }
        $concept = DB::table('concept_items')
              ->join('user_accounting_heads', 'user_accounting_heads.id', '=', 'concept_items.user_accounting_head_id')
              ->leftjoin('taxes', 'taxes.id', '=', 'concept_items.tax_id')
              ->where('account_recivable_id',$id)
              ->select('concept_items.*', 'user_accounting_heads.name','taxes.percentage')
              ->get();
                // array_push($array,$items);
              foreach($concept as $item){
                    if($item->discount !=''){
                              $discount=($item->price*$item->discount/100);
                              $item->discount_amount= $discount;
                              $discount_amount=$item->price-$discount;
                        }
                        else{
                                $discount_amount=$item->price;
                                $item->discount_amount= 0;
                        }
                        if($item->percentage !=''){
                              $tax=$discount_amount*$item->percentage/100;
                              $item->tax_amount=$tax;
                        }
                        else{
                                $tax=0;
                                $item->tax_amount=0;
                       }
                       $total=$discount_amount+$tax;
                       $item->total=$total;
                       
                     $d=json_decode($item->name);
                     if($lang=='en'){
                       $item->name=$d->en;
                     }
                     else{
                       $item->name=$d->es;
                     }
                     array_push($array,$item);
                 }
                 $discount=$base_total=$tax_amount=0;
                 foreach($array as $items){
                    $discount=$discount+$item->discount_amount;
                    $base_total=$base_total+$item->price;
                    $tax_amount=$tax_amount+$item->tax_amount;
                   
                 }
                 $account_recivable['items']=$array;
                 $account_recivable['base_total']=$base_total;
                 $account_recivable['discount']=$discount;
                 $account_recivable['sub_total']=$base_total-$discount;
                 $account_recivable['tax_amount']=$tax_amount;
                 $account_recivable['total']=($base_total-$discount+$tax_amount);
                 return $account_recivable;

    }


   public function print(Request $request, $id)
    {
        try {
            $user = $request->user();
            $showView = $request->input('display');
            
            $print = true;
            if ($request->has('view')) {
                $viewType = $request->input('view');
            } else {
                $viewType = 'view-order';
            }
            $recivable=$this->getinvoive($id);
            //return view('pdf.recivable-template-4', compact('recivable', 'print', 'user', 'viewType'));
          

            $filename = 'recivable-'.$recivable['id'].'.pdf';
            $pdf = PDF::loadView('pdf.recivable-template-4', compact('recivable', 'print', 'user', 'viewType'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->inline($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
     public function pdf(Request $request,  $id)
    {
        try {
            $user = $request->user();
            $showView = $request->input('display');
            
            $print = true;
            if ($request->has('view')) {
                $viewType = $request->input('view');
            } else {
                $viewType = 'view-order';
            }
            $recivable=$this->getinvoive($id);
            //return view('pdf.recivable-template-4', compact('recivable', 'print', 'user', 'viewType'));
          

            $filename = 'recivable-'.$recivable['id'].'.pdf';
            $pdf = PDF::loadView('pdf.recivable-template-4', compact('recivable', 'print', 'user', 'viewType'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->download($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
      /**
     * email the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function mail(Request $request, $id)
    {
        try {
            
            if ($request->has('view')) {
                $viewType = $request->input('view');
            } else {
                $viewType = 'view-order';
            }

            $input = $request->all();
            $emails = collect(explode(',', $input['emails']));
            $emails = $emails->map(function ($item, $key) {
                $item = trim(strtolower($item));
                return $item;
            })->uniqueStrict()->filter();

            if ($emails->isEmpty()) {
                $request->session()->flash('error', trans('messages.reciver_mail_without_email'));
                return back();
            }

            $user = $request->user();
            $recivable=$this->getinvoive($id);
            $pdf = PDF::loadView('pdf.recivable-template-4', compact('recivable', 'print', 'user', 'viewType'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            
            if ($pdf) {
                foreach ($emails as $recipient) {
                  print_r($emails);
                  die;
                    Mail::to($recipient)->send(new ReceivableShared($recivable, $pdf->output()));
                }
            }

            $request->session()->flash('success', trans('messages.invoice_mail_sent'));
           return back();
        }  catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
        $date=date('Y-m-d h:i:s');
        DB::table('account_receivable')->where('id', $id)->update(['deleted_at' => $date]);
        DB::table('items')->where('account_recivable_id', $id)->update(['deleted_at' => $date]);
        $request->session()->flash('success', trans('messages.recivable_deleted'));
         return redirect()->route('receivable.index');

    }

}
