<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoice;
use App\Supplier;
use App\Client;

use App\Jobs\GenerateInvoicesFromRecurringInvoice;
use App\Jobs\GeneratePaymentsFromRecurringPayment;
use App\Jobs\GenerateAccountEntriesFromFixedAssets;

use App\Traits\ApiResponse;

use Auth;
use Config;
use Validator;
use Artisan;
use Session;
use Cache;

class AppController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Invoice::class, 'invoices');
    }

    /**
     * return the row for next item for invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateItemRow(Request $request)
    {
        $input = $request->all();
        $user = $request->user();

        if (isset($input['id']) && $input['id']) {
            $row_number = $input['id'];
            $companyClients = $user->company->clients;
            $companySuppliers = $user->company->suppliers;
            $inventories = $user->company->inventories;

            $contacts = collect([]);
            $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

            $clients_attributes = collect([]);
            $suppliers_attributes = collect([]);

            $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
                $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
                if ($item instanceof Supplier) {
                    $suppliers_attributes[$item->custom_id] = [
                        'data-phone' => $item->phone ?: '',
                        'data-tax_id' => $item->tax_id ?: ''
                    ];
                } else {
                    $clients_attributes[$item->custom_id] = [
                        'data-phone' => $item->phone ?: '',
                        'data-tax_id' => $item->tax_id ?: ''
                    ];
                }
                return $item;
            });

            $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
            $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

            $contact_options = [
                '' => trans('labels.select_contact'), 
                trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
                trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
            ];

            $inventories = $user->company->inventories;

            $inventory_attributes = $inventories->mapWithKeys(function ($item) {
                return [$item->id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-sale_price' => $item->sale_price ?: '',
                    'data-unit_cost' => $item->unit_cost ?: 0,
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();

            $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

            $taxes = $user->company->taxes;

            $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
                return [$item->id => [
                    'data-percentage' => $item->percentage ?: 0
                ]];
            })->toArray();

            $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
            $term_options = Config::get('constants.terms');

            return view('app.item-row', compact('row_number', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'term_options', 'inventory_options', 'inventory_attributes', 'tax_attributes'));
        }        
    }

    /**
     * return the row for next item for invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function getContactInvoicesRows(Request $request)
    {
        $input = $request->all();
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'contact' => 'required|string|max:255',
            'type' => 'required|in:sale,supplier',
            'action' => 'in:create,edit'
        ]);
        if ($validator->fails()) {
            // return $validator->errors()->all()[0];
            return trans('messages.something_went_wrong');
        }

        $action = $input['action'] ?? 'create';
        $transaction_id = $input['transaction_id'] ?? null;
        $credit_note_id = $input['credit_note_id'] ?? null;
        $debit_note_id = $input['debit_note_id'] ?? null;

        list($contact_type, $contact_id) = explode('-', $input['contact']);
        $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

        if (!$contact) {
            return trans('validation.exists', ['attribute' => trans('labels.contact')]);
        }

        // checking that the logged in user is same who create the supplier or client.
        if ($contact->company_id == $user->company->id) {
            $invoices = $contact->invoices->where('is_void', false)->where('type', $input['type'])->values();
            // In case if someone edit the transaction or credit notes.
            if ($action == 'edit') {
                // if someone edit the transaction for either received from clients or paid to supplier
                if ($transaction_id) {
                    foreach ($invoices as &$invoice) {
                        $transaction_details = [];
                        if ($invoice->transactions->isNotEmpty()) {
                            // get the current transaction which is going to edit.
                            $invoice_transaction = $invoice->transactions->where('id', $transaction_id)->first();
                            $invoice->transaction_details = [$invoice->id => ($invoice_transaction && $invoice_transaction->pivot) ? $invoice_transaction->pivot->amount : 0];
                        }
                        $totalPendingAmountWithTransaction = $invoice->calculation['pendingAmount'] + ($invoice->transaction_details[$invoice->id] ?? 0);
                        $invoice->maxAttrVal = $invoice->currency ? addZeros($totalPendingAmountWithTransaction / $invoice->exchange_currency_rate) : $totalPendingAmountWithTransaction;
                    }
                }
                // if someone edit the credit_note either for clients or supplier
                if ($credit_note_id) {
                    foreach ($invoices as &$invoice) {
                        $credit_note_details = [];
                        if ($invoice->creditNotes->isNotEmpty()) {
                            // get the current credit note which is going to edit.
                            $credit_note_invoice = $invoice->creditNotes->where('id', $credit_note_id)->first();
                            $invoice->credit_note_details = [$invoice->id => ($credit_note_invoice && $credit_note_invoice->pivot) ? $credit_note_invoice->pivot->amount : 0];
                        }
                    }
                }

                 // if someone edit the debit_note either for clients or supplier
                 if ($debit_note_id) {
                    foreach ($invoices as &$invoice) {
                        $debit_note_details = [];
                        if ($invoice->debitNotes->isNotEmpty()) {
                            // get the current debit note which is going to edit.
                            $debit_note_invoice = $invoice->debitNotes->where('id', $debit_note_id)->first();
                            $invoice->debit_note_details = [$invoice->id => ($debit_note_invoice && $debit_note_invoice->pivot) ? $debit_note_invoice->pivot->amount : 0];
                        }
                    }
                }
            }

            return view('app.contact-invoice-rows', compact('invoices', 'action', 'transaction_id'));
        }
    }

    /**
     * set the locale for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function setLocale(Request $request)
    {
        try {
            $input = $request->all();
            $user = $request->user();
            $validator = Validator::make($request->all(), [
                'locale' => 'required|in:en,es'
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->errors()->all()[0]]);
            }
            if ($user) {
                $user->language = $input['locale'];
                $user->save();
            } else {
                Session::put('locale', $input['locale']);
            }

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }
    }

    /**
     * return the row for next concept for invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateConceptRow(Request $request)
    {
        $input = $request->all();
        $user = $request->user();

        if (isset($input['id']) && $input['id']) {
            $inventories = $user->company->inventories;
            $concepts = Cache::get('concepts');    
            $row_number = $input['id'];
            $concept_with_products = $input['concept_with_products'] ?? false;
            $taxes = $user->company->taxes;

            $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
                return [$item->id => [
                    'data-percentage' => $item->percentage ?: 0
                ]];
            })->toArray();

            $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

            $concept_options_with_products = [];

            $trackedInventories = $inventories->where('track_inventory', 1)->map(function($ti) {
                $ti->custom_id = self::INVENTORY .'-'. $ti->id;
                return $ti;
            });
            $childConcepts = $concepts->whereNotNull('parent_id')->map(function($ci) {
                $ci->custom_id = self::CONCEPT .'-'. $ci->id;
                return $ci;
            });

            if ($trackedInventories) {
                $concept_options_with_products[trans('labels.inventories')] = $trackedInventories;
            }
            $concept_options_with_products[trans('labels.concepts')] = $childConcepts;

            $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
                return collect($cp)->mapWithKeys(function ($item) {
                    return [$item->custom_id => [
                        'data-reference' => $item->reference ?: '',
                        'data-description' => $item->description ?: '',
                        'data-unit_cost' => $item->unit_cost ?: '',
                        'data-tax_id' => $item->tax_id ?: ''
                    ]];
                })->toArray();
            })->toArray();

            $concept_options_with_products = [
                '' => trans('labels.select_concept'), 
                trans('labels.inventories') => $concept_options_with_products[trans('labels.inventories')]->pluck('name', 'custom_id')->toArray(),
                trans('labels.concepts') => $concept_options_with_products[trans('labels.concepts')]->pluck('title', 'custom_id')->toArray()
            ];

            $accounting_heads = $user->company->accountingHeads;
            $all_accounts_options = collect([]);
            $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
            $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
            $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

            $all_accounts->each(function($all_account) use($all_accounts_options) {
                $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
            });

            return view('app.concept-row', compact('row_number', 'tax_options', 'tax_attributes', 'concept_options_with_products', 'concept_options_with_products_attr', 'concept_with_products', 'trackedInventories', 'all_accounts_options'));
        }        
    }

    /**
     * return the details of credit notes for a contact
     *
     * @return \Illuminate\Http\Response
     */
    public function getContactCreditNotes(Request $request)
    {
        $input = $request->all();
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'contact' => 'required|string|max:255'
        ]);
        if ($validator->fails()) {
            return trans('messages.something_went_wrong');
        }

        list($contact_type, $contact_id) = explode('-', $input['contact']);
        $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

        if (!$contact) {
            return trans('validation.exists', ['attribute' => trans('labels.contact')]);
        }

        $data = [
            'remaining_amount' => $contact->creditNotes->pluck('calculation')->sum('remaining_amount')
        ];
        
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * clear the cached version of the app data.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearCache(Request $request)
    {
        $cacheCleared = Artisan::call('config:cache');
        // $cacheCleared = Artisan::call('cache:clear');
        return response()->json(['success' => true]);
    }

    /**
     * coming soon page for the upcoming features.
     *
     * @return \Illuminate\Http\Response
     */
    public function comingSoon(Request $request)
    {
        return view('app.coming-soon');
    }

    /**
     * generate Invoices and Payments based on recurring entries.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateInvoicesPayments(Request $request) {
        // GenerateInvoicesFromRecurringInvoice::dispatch()->onQueue('high');
        // GeneratePaymentsFromRecurringPayment::dispatch()->onQueue('high');
        GenerateAccountEntriesFromFixedAssets::dispatch()->onQueue('high');
        die('The invoices and payments has been generated successfully.');
    }

    /**
     * return the details of Debit notes for a contact
     *
     * @return \Illuminate\Http\Response
     */
    public function getContactDebitNotes(Request $request)
    {
        $input = $request->all();
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'contact' => 'required|string|max:255'
        ]);
        if ($validator->fails()) {
            return trans('messages.something_went_wrong');
        }

        list($contact_type, $contact_id) = explode('-', $input['contact']);
        $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

        if (!$contact) {
            return trans('validation.exists', ['attribute' => trans('labels.contact')]);
        }

        $data = [
            'remaining_amount' => $contact->debitNotes->pluck('calculation')->sum('remaining_amount')
        ];
        
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * return the details of credit notes for a contact
     *
     * @return \Illuminate\Http\Response
     */
    public function getContactLeaveBalance(Request $request)
    {
        $input = $request->all();
        $user = $request->user();
        $taken_leaves = $user->leaveApplications->where('leave_type_id', $request->leave_type_id)->pluck('approved_leaves');
        $total_leaves = $user->company->leaveTypes->where('id', $request->leave_type_id)->sum('number_of_days');
        if (!$taken_leaves->isEmpty() && $total_leaves) {
            $remaining_leaves = $total_leaves - $taken_leaves[0];
            if (!$remaining_leaves) {
                return response()->json(['success' => false]);
            }
            return response()->json(['success' => true, 'data' => $remaining_leaves]);
        }
        else if ($taken_leaves->isEmpty() && $total_leaves) {
            return response()->json(['success' => true, 'data' => $total_leaves]);
        }
        else {
            return response()->json(['success' => false]);
        }
    }

    /**
     * return the row for next item for Account Adjustment.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateAccountAdjustmentRow(Request $request)
    {        
        $input = $request->all();
        $user = $request->user();

        if (isset($input['id']) && $input['id']) {
            $row_number = $input['id'];

            $companyClients = $user->company->clients;
            $companySuppliers = $user->company->suppliers;

            // $accounting_heads = Cache::get('user_accounting_heads_'.$user->company->id);

            $contacts = collect([]);
            $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();


            $all_accounts_options = Cache::remember('all_accounts_options_'.$user->company->id, 60 * 10, function() use($user) {
                $accounting_heads = $user->company->accountingHeads;
                $all_accounts_options = collect([]);

                $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
                $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
                $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

                $all_accounts->each(function($all_account) use($all_accounts_options) {
                    $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
                });
                return $all_accounts_options;
            });

            $clients_attributes = collect([]);
            $suppliers_attributes = collect([]);

            $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
                $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
                if ($item instanceof Supplier) {
                    $suppliers_attributes[$item->custom_id] = [
                        'data-phone' => $item->phone ?: '',
                        'data-tax_id' => $item->tax_id ?: ''
                    ];
                } else {
                    $clients_attributes[$item->custom_id] = [
                        'data-phone' => $item->phone ?: '',
                        'data-tax_id' => $item->tax_id ?: ''
                    ];
                }
                return $item;
            });

            $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
            $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

            $contact_options = [
                '' => trans('labels.select_contact'), 
                trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
                trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
            ];

            return view('app.account-adjustment-row', compact('row_number', 'contact_options', 'contact_attributes', 'all_accounts_options'));
        }        
    }

    /**
     * static content for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function staticContent(Request $request)
    { 
        try {
            $lang = app()->getLocale();
            $slice = $request->input('slice');
            $slice = strtolower(trim($slice));

            $files = glob(resource_path('lang/' . $lang . '/*.php'));
            $strings = [];
            foreach ($files as $file) {
                $name = basename($file, '.php');
                if ($name !== "lang") {
                    $new_keys = require $file;
                    $strings = array_merge($strings, [$name => $new_keys]);
                }
            }
            $response = $strings;

            $response['permissions'] = Cache::get('permissions')->map(function($permission) {
                return $permission->only('custom_name', 'id', 'name');
            });

            $response['countries'] = Cache::get('countries')->map(function($country) {
                return $country->only('name', 'id');
            });

            $response['currencies'] = Cache::get('currencies')->map(function($currency) {
                return $currency->only('name', 'id', 'formatted_name', 'symbol');
            });

            $response['sectors'] = Cache::get('sectors')->map(function($sector) {
                return $sector->only('name', 'id');
            });

            $response['user_types'] = [
                ['value' => "business_owner", 'text' => trans('labels.own_a_business')],
                ['value' => "independent", 'text' => trans('labels.an_independent')],
                ['value' => "student", 'text' => trans('labels.an_student')],
                ['value' => "accountant", 'text' => trans('labels.an_accountant')],
                ['value' => "other", 'text' => trans('labels.others')]
            ];

            $response['identification_types'] = Cache::get('identification_types')->map(function($sector) {
                return $sector->only('name', 'id');
            });

            if (array_key_exists($slice, $response)) {
                $response = $response[$slice];
            }

            return $this->successResponse([
                "message" => trans('messages.data_fetched'),
                "data" => $response
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }
    
    /**
     * return the row for next item for invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateDeprItemRow(Request $request)
    {
        $input = $request->all();
        $user = $request->user();

        if (isset($input['id']) && $input['id']) {
            $row_number = $input['id'];

            return view('app.depreciation-item-row', compact('row_number'));
        }        
    }

    public function getDepreciationTimeline(Request $request) {
        try {
            $input = $request->all();
            $result = [];

            $years = $input['years'];
            // $dop = $input['dop'];
            $total = $remaining_amount = intval($input['total']);
            $cal_type = $input['cal_type'] ?? 'straight_line';
            $scrap_percentage = $input['scrap_percentage'];

            $total_unit_produced = $input['total_unit_produced'] ?? null;
            $unit_produced_per_year = $input['unit_produced_per_year'] ?? null;
            $same_for_year = $input['same_for_year'] ?? null;
            $per_year_prod = $input['per_year_prod'] ?? null;

            $maxUnitsProduced = null;
            $unitsProducedPerYear = [];

            if ($input['cal_type'] == 'units_produced') {
                // prePrint($input);
                $maxUnitsProduced = $total_unit_produced;
                if ($same_for_year == "1") {
                    $unitsProducedPerYear = array_fill(0, $years, $unit_produced_per_year);
                } else {
                    $unitsProducedPerYear = explode(',', $per_year_prod);
                }
                $unitsProducedPerYear = array_map('trim', $unitsProducedPerYear);
            }

            $deprResult = computeDepreciation($cal_type, $total, $scrap_percentage, $years, $maxUnitsProduced, $unitsProducedPerYear);
            foreach ($deprResult as $key => $depValue) {
                $temp = [];
                $temp['year'] = $key + 1;
                $temp['opening_value'] = $remaining_amount;
                $temp['depreciation'] = $depValue;
                $temp['closing_value'] = $remaining_amount - $depValue;
                
                $remaining_amount = $temp['closing_value'];
                $result[$key] = $temp;
            }
            return response()->json(['data' => $result, 'success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }
    }

    public function deprCal()
    {
        
        // echo $dop->lastOfMonth()->format('Y-m-d');

        $dop = Carbon::parse($dop);
        $firstDateOfFirstEntry = $dop->copy()->firstOfMonth();
        $dateOfFirstEntry = $dop->copy()->lastOfMonth();
        $dopYearEnd =$dop->copy()->endOfYear();

        $firstEntryDays = $dateOfFirstEntry->diffInDays($dop) + 1;
        $firstYearEntriesCount = $dop->diffInMonths($dopYearEnd) + 1;

        $totalNumberOfEntries = $years * 12;

        if ($dop == $firstDateOfFirstEntry) {
            // if the date of purchase is the first day of the month.


        } else {
            // if the date of purchase is not the first day of the month.
            $totalNumberOfEntries += 1;

        }
        $rawDeprResult = [];

        foreach ($deprResult as $key => $value) {
            $temp = [];
            $amountPerDay = addZeros($value / 365);
            $amountPerMonth = addZeros($value / 12);
            $amountPerYear = $value;

            if ($key == 0) {
                for ($i=0; $i < $firstYearEntriesCount; $i++) { 
                    if ($i == 0 && $dop != $firstDateOfFirstEntry) {
                        $amount = $amountPerDay * $firstEntryDays;
                        $date = $dateOfFirstEntry;
                    } else {
                        $amount = $amountPerMonth;
                        $date = $dateOfFirstEntry;
                    }
                    // $temp['date'] = $dateOfFirstEntry;
                    // $temp['amount'] = $amount;
                    array_push($rawDeprResult, $amount);
                }
                $totalNumberOfEntries -= $firstYearEntriesCount;
            } else {
                $thisYearEntries = $totalNumberOfEntries > 12 ? 12 : $totalNumberOfEntries;

                for ($i=0; $i < $thisYearEntries; $i++) { 
                    $amount = $amountPerMonth;
                    $date = $dateOfFirstEntry;
                    // $temp['date'] = $dateOfFirstEntry;
                    // $temp['amount'] = $amount;
                    array_push($rawDeprResult, $amount + $totalNumberOfEntries);
                }
                $totalNumberOfEntries -= 12;
            }
        }

        if ($totalNumberOfEntries) {
            for ($i=0; $i < $totalNumberOfEntries; $i++) { 
                $amount = $amountPerMonth;
                $date = $dateOfFirstEntry;
                // $temp['date'] = $dateOfFirstEntry;
                // $temp['amount'] = $amount;
                array_push($rawDeprResult, $amount + $totalNumberOfEntries);
            }
        }
    }

    public function counter()
    {
        return view('app.counter');
    }

}
