<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Tax;
use App\Validators\TaxValidator;

class TaxController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Tax::class, 'tax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $taxes = $user->company->taxes;

        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $taxes]);
        }
        return view('taxes.index', compact('taxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;

        $taxes_to_pay_accounts_options = collect([]);
        $taxes_in_favour_account_options = collect([]);

        $taxes_to_pay_account = $accounting_heads->where('code', self::TAXTOPAYCODE)->first();
        $taxes_in_favour_account = $accounting_heads->where('code', self::TAXINFAVOURCODE)->first();
        
        $this->generateAccountsLevels($taxes_to_pay_account, $taxes_to_pay_accounts_options);
        $this->generateAccountsLevels($taxes_in_favour_account, $taxes_in_favour_account_options);

        return view('taxes.create', compact('taxes_to_pay_accounts_options', 'taxes_in_favour_account_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, TaxValidator $taxValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$taxValidator->with($input)->passes()) {
                $request->session()->flash('error', $taxValidator->getErrors()[0]);
                return back()
                    ->withErrors($taxValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'percentage' => $input['percentage'],
                'description' => $input['description'],
                'sales_account' => $input['sales_account'],
                'purchases_account' => $input['purchases_account'],
                'user_id' => $user->id
            ];

            $tax = $user->company->taxes()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.tax_created'));
            return redirect()->route('taxes.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Tax $tax)
    {
        $resource_associated = $tax->items->isNotEmpty() || $tax->conceptitems->isNotEmpty();

        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;

        $taxes_to_pay_accounts_options = collect([]);
        $taxes_in_favour_account_options = collect([]);

        $taxes_to_pay_account = $accounting_heads->where('code', self::TAXTOPAYCODE)->first();
        $taxes_in_favour_account = $accounting_heads->where('code', self::TAXINFAVOURCODE)->first();
        
        $this->generateAccountsLevels($taxes_to_pay_account, $taxes_to_pay_accounts_options);
        $this->generateAccountsLevels($taxes_in_favour_account, $taxes_in_favour_account_options);
        
        return view('taxes.edit', compact('tax', 'resource_associated', 'taxes_to_pay_accounts_options', 'taxes_in_favour_account_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tax $tax, TaxValidator $taxValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $resource_associated = $tax->items->isNotEmpty() || $tax->conceptitems->isNotEmpty();

            if (!$taxValidator->with($input)->passes()) {
                $request->session()->flash('error', $taxValidator->getErrors()[0]);
                return back()
                    ->withErrors($taxValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'percentage' => $resource_associated ? $tax->percentage : $input['percentage'],
                'description' => $input['description'],
                'sales_account' => $input['sales_account'],
                'purchases_account' => $input['purchases_account']
            ];

            $tax->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.tax_updated'));
            return redirect()->route('taxes.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tax $tax)
    {
        try {
            if ($tax->items->isEmpty() && $tax->conceptitems->isEmpty()) {
                $tax->delete();
                $request->session()->flash('success', trans('messages.tax_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('taxes.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
