<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;

use File;
use Validator;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }

    /**
     * Upload documents in temp folder.
     */
    public function uploadFiles(Request $request) {        

        $input = $request->all();
        $type = $input['type'];

        if ($type=='image')  {
            $rules = [
                'file' => 'required|mimes:jpeg,jpg,png,gif'
            ];
        }
        else if($type=='pdf') {
            $rules = [
                'file' => 'required|mimes:pdf|max:10240'
            ];
        }
        else {
            $rules = [
                'file' => 'required|mimes:jpeg,jpg,png,gif,pdf'
            ];
        }

        try {
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'error' => $validator->errors()->first(),
                ], 422);
            }
            $dir = storage_path('tmp');

            if( !File::isDirectory($dir) ) { 
                File::makeDirectory($dir, 493, true);
            }

            $file = $request->file('file');

            $name = uniqid() . '.' . $file->extension();

            $file->move($dir, $name);

            return response()->json([
                'status'        => true,
                'name'          => $name,
                'original_name' => $file->getClientOriginalName()
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * delete documents in temp folder.
     */
    public function deleteFile(Request $request) {
        
        try {
            $input = $request->all();
            $rules = [
                'name' => 'required|string'
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'error' => $validator->errors()->first(),
                ], 422);
            }
            
            $image = $input['name'];

            if (File::exists(storage_path('tmp/'.$image))) {
                File::delete(storage_path('tmp/'.$image));
                return response()->json([
                    'status'        => true,
                    'message'       => trans('messages.img_deleted')
                ]);
            } else {
                return response()->json([
                    'status'        => false,
                    'message'       => trans('messages.img_not_found')
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
    }
}