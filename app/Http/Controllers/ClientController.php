<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Client;
use App\Address;
use App\Validators\ClientValidator;
use App\Validators\SupplierValidator;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Client::class, 'client');
        // $this->middleware('preventBackHistory');
        // $this->authorizeResource(Client::class, 'client', [
        //     'except' => [ 'index', 'show' ],
        // ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $clients = $user->company->clients;

        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $clients]);
        }
        return view('clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $memberships = $user->company->memberships;

        $membership_options = ['' => trans('labels.select_membership')] + $memberships->pluck('name', 'id')->toArray();
        return view('clients.create', compact('membership_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ClientValidator $clientValidator, SupplierValidator $supplierValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if((!$request->has('client')) && (!$request->has('supplier'))) {
                $request->session()->flash('error', trans('messages.client_or_supplier_require_one'));
                return back()->withInput();
            }

            $user = $request->user();
            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'company_name' => $input['company_name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'membership_id' => $input['membership_id'] ?? null,
                'user_id' => $user->id
            ];

            if($request->has('client')) {
                if (!$clientValidator->with($input)->passes()) {
                    $request->session()->flash('error', $clientValidator->getErrors()[0]);
                    return back()
                        ->withErrors($clientValidator->getValidator())
                        ->withInput();
                }
                $client = $user->company->clients()->create($data);
                if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                    $addressData = [
                        'line_1' => $input['address'],
                        'zip' => $input['zip'],
                        'country_id' => $input['country'],
                        'state' => $input['state'],
                        'city' => $input['city'],
                    ];
                    $address = $client->address()->create($addressData);
                }
            }

            if($request->has('supplier')) {
                if (!$supplierValidator->with($input)->passes()) {
                    $request->session()->flash('error', $supplierValidator->getErrors()[0]);
                    return back()
                        ->withErrors($supplierValidator->getValidator())
                        ->withInput();
                }
                unset($data['company_name']);
                $suplier = $user->company->suppliers()->create($data);
                if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                    $addressData = [
                        'line_1' => $input['address'],
                        'zip' => $input['zip'],
                        'country_id' => $input['country'],
                        'state' => $input['state'],
                        'city' => $input['city'],
                    ];
                    $supplier_address = $suplier->address()->create($addressData);
                }
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.client_created'));
            return redirect()->route('clients.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Client $client)
    {
        $user = $request->user();
        $memberships = $user->company->memberships;

        $membership_options = ['' => trans('labels.select_membership')] + $memberships->pluck('name', 'id')->toArray();

        return view('clients.edit', compact('client', 'membership_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            $clientValidator = new ClientValidator('update', $client);

            if (!$clientValidator->with($input)->passes()) {
                $request->session()->flash('error', $clientValidator->getErrors()[0]);
                return back()
                    ->withErrors($clientValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'company_name' => $input['company_name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'membership_id' => $input['membership_id'] ?? null,
            ];

            $client->update($data);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];

                $client->address()->updateOrCreate([], $addressData);
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.client_updated'));
            return redirect()->route('clients.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Client $client)
    {
        try {
            if ($client->invoices->isEmpty() && $client->transactions->isEmpty() && $client->creditNotes->isEmpty() && $client->estimates->isEmpty() && $client->debitNotes->isEmpty() && $client->purchaseOrders->isEmpty()) {
                $client->forceDelete();
                $request->session()->flash('success', trans('messages.client_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('clients.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
