<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\MainUnit;
use App\Unit;
use App\MasterCategory;
use App\ItemCategory;
use Hash;
use App\Events\ProfileCompleted;
use App\Events\UserLoggedIn;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::DASHBOARD;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {

    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $input = $request->all();

        $user = $request->user();

        if ($user->isSuperAdmin()) {
            return redirect()->route('admin-dashboard');
        }

        event(new ProfileCompleted($user));
        event(new UserLoggedIn($user, [
            'lat' => $input['lat'],
            'lon' => $input['lon'],
        ]));

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect()->intended($this->redirectPath());
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        $email = $request->email;
        $user = User::whereEmail($email)->first();

        if ($user && $user->user_role == self::APP_USER) {
            return $this->sendFailedLoginResponse($request);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            $company = Company::where('user_id', $user->id)->first();
            if(!empty($user->company_id)){
                if($user->is_unit_added == 0){
                    $mainUnits = MainUnit::select('unit', 'symbol')->get();
                    foreach ($mainUnits as $key => $mainUnit) {
                        $units = json_decode($mainUnit->unit);
                        $shortcut = json_decode($mainUnit->symbol);
                        $unit = new Unit(['company_id' => $user->company_id,
                            'user_id' => $user->id
                            ]);
                        $unit->setTranslation('unit', 'en', $units->en)->setTranslation('unit', 'es', $units->es);
                        $unit->setTranslation('shortcut', 'en', $shortcut->en)->setTranslation('shortcut', 'es', $shortcut->es);
                        // $unit = new Unit($unitData);
                        $unit->save();
                    }
                    $user->is_unit_added = 1;
                    $user->save();
                }
                if($user->is_category_added == 0) {
                    $masterCategories = MasterCategory::select('name')->get();
                    foreach ($masterCategories as $key => $category) {
                        $categories = json_decode($category->name);
                        $item = new ItemCategory(['company_id' => $user->company_id,
                            'user_id' => $user->id
                            ]);
                        $item->setTranslation('name', 'en', $categories->en)->setTranslation('name', 'es', $categories->es);
                        $item->save();
                    }
                    $user->is_category_added = 1;
                    $user->save();
                }
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        
        // $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function customLogin(Request $request)
    {
        $this->validateLogin($request);

        $email = $request->email;
        $password = $request->password;
        $user = User::whereEmail($email)->first();

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($user) {

            if (Hash::check($request->password, $user->password, [])) {
                if (!$user->email_verified_at) {
                    return redirect()->back()->with('error', trans('messages.account_not_verified'));
                }
            } else {
                return redirect()->back()->with('error', trans('auth.failed'));
            }

            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                return $this->sendLoginResponse($request);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function guard() {
        return Auth::guard();
    }

    public function logout(Request $request) {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

}
