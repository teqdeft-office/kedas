<?php


namespace App\Http\Controllers\Auth;


use App\User;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Auth;
use App\MasterCategory;
use App\ItemCategory;
use App\Unit;
use App\MainUnit;
use App\Company;

class FacebookController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {

            $user = Socialite::driver('facebook')->user();

            $finduser = User::where('facebook_id', $user->getId())->first();

            if($finduser){

                // $company = Company::where('user_id', $user->id)->first();
                if(!empty($finduser->company_id)){
                    if($finduser->is_unit_added == 0){
                        $mainUnits = MainUnit::select('unit', 'symbol')->get();
                        foreach ($mainUnits as $key => $mainUnit) {
                            $units = json_decode($mainUnit->unit);
                            $shortcut = json_decode($mainUnit->symbol);
                            $unit = new Unit(['company_id' => $finduser->company_id,
                                'user_id' => $finduser->id
                                ]);
                            $unit->setTranslation('unit', 'en', $units->en)->setTranslation('unit', 'es', $units->es);
                            $unit->setTranslation('shortcut', 'en', $shortcut->en)->setTranslation('shortcut', 'es', $shortcut->es);
                            // $unit = new Unit($unitData);
                            $unit->save();
                        }
                        $finduser->is_unit_added = 1;
                        $finduser->save();
                    }
                    if($finduser->is_category_added == 0) {
                        $masterCategories = MasterCategory::select('name')->get();
                        foreach ($masterCategories as $key => $category) {
                            $categories = json_decode($category->name);
                            $item = new ItemCategory(['company_id' => $finduser->company_id,
                                'user_id' => $finduser->id
                                ]);
                            $item->setTranslation('name', 'en', $categories->en)->setTranslation('name', 'es', $categories->es);
                            $item->save();
                        }
                        $finduser->is_category_added = 1;
                        $finduser->save();
                    }
                    
                }

                Auth::login($finduser);

                return redirect('/dashboard');

            }else{
                $newUser = User::create([
                    'name' => $user->getName(),
                    'email' => $user->getEmail(),
                    'facebook_id'=> $user->getId(),
                    'email_verified_at' => now(),
                    'password' => encrypt(Date("YmdHis")),

                ]);

                Auth::login($newUser);

                return redirect('/dashboard');
            }

        } catch (Exception $e) {
            dd($e->getMessage());

            // return redirect('auth/facebook');
            return redirect('dashboard');


        }
    }
}
