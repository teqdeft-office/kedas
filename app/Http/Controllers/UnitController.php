<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Supplier;
use App\Client;
use App\Item;
use App\ConceptItem;
use App\Inventory;
use App\User;
use App\Estimate;
use App\PurchaseOrder;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Validators\InvoiceValidator;
use App\Mail\InvoiceShared;
use App\Mail\InvoiceCreated;
use App\Events\SaleInvoiceCreatedOrUpdated;
use App\Events\ProfileCompleted;

use DB;
use Auth;
use Config;
use Image;
use PDF;
use Cache;
use File;
use Session;

class UnitController extends Controller
{
    public function __construct()
    {
        // $this->authorizeResource(Unit::class, 'units');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type = self::SALE)
    {
        $user = $request->user();
        $units = $user->company->units;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $units->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'unit' => $item->unit,
                    'shortcut' => $item->shortcut
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('unit.index', compact('units', 'type'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        return view('unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $user = $request->user();

            // $data = [
            //     'unit' => $input['unit'],
            //     'shortcut' => $input['shortcut'],
            //     'user_id' => $user->id,
            //     'company_id' => $user->company_id
            // ];

            // $units = new Unit;

            // $unit = $units->create($data);
            $unit = new Unit(['company_id' => $user->company->id,
                        'user_id' => $user->id,
                        ]);
            $unit->setTranslation('unit', 'en', $input['unit'])->setTranslation('unit', 'es', $input['unit']);
            $unit->setTranslation('shortcut', 'en', $input['shortcut'])->setTranslation('shortcut', 'es', $input['shortcut']);
            // $unit = new Unit($unitData);
            $unit->save();
            DB::commit();

            $request->session()->flash('success', trans('messages.unit_created'));
            return redirect()->route('units.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        return view('unit.show', compact('unit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        $user = Auth::user();
        
        // $taxes = $user->company->taxes;

        return view('unit.edit', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $data = [
                'unit' => $input['unit'],
                'shortcut' => $input['shortcut'],
            ];
            $unit->setTranslation('unit', 'en', $input['unit'])->setTranslation('unit', 'es', $input['unit']);
            $unit->setTranslation('shortcut', 'en', $input['shortcut'])->setTranslation('shortcut', 'es', $input['shortcut']);
            // $unit = new Unit($unitData);
            $unit->update();

            DB::commit();

            $request->session()->flash('success', trans('messages.unit_update'));
            return redirect()->route('units.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Unit $unit)
    {
        try {
            if ($unit) {
                $unit->delete();
                $request->session()->flash('success', trans('messages.unit_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('units.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

}
