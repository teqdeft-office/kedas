<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Events\ProfileCompleted;
use App\Validators\Accounting\HeadValidator;

use App\UserAccountingHead;
use Illuminate\Support\Str;

use DB;
use Lang;


class AccountingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the accounting charts
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function charts(Request $request)
    {
        return view('app.coming-soon');
    	$user = $request->user();
        // event(new ProfileCompleted($user));

    	$accounting_heads = $user->company->accountingHeads;
        $accounting_heads_options = ["" => trans('labels.select_account_head')] + $accounting_heads->whereNull('parent_id')->mapWithKeys(function ($item) {
            return [$item['name'] => $item->children->pluck('name', 'id')];
        })->toArray();
        return view('accounting.charts', compact('user', 'accounting_heads', 'accounting_heads_options'));
    }

    /**
     * Add a new accounting charts or head
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addHead(Request $request)
    {
        $user = $request->user();
        $input = $request->all();

        try {
            DB::beginTransaction();
            $input['slug'] = Str::slug($input['account-name'], '-');
            $accountingHeadValidator = new HeadValidator;

            if (!$accountingHeadValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'messages'  => $accountingHeadValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                return back()
                    ->withErrors($accountingHeadValidator->getValidator())
                    ->with([
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            $parentAccountingHead = $user->accountingHeads->where('id', $input['account-type'])->first();

            if ($parentAccountingHead) {
                $userAccountingHead = new UserAccountingHead;
                $userAccountingHead->parent_id = $parentAccountingHead->id;
                $userAccountingHead->nature = $parentAccountingHead->nature;
                $userAccountingHead->description = $input['description'];
                $userAccountingHead->code = $input['code'];
                $userAccountingHead->slug = Str::slug($input['account-name'], '-');

                $userAccountingHead->setTranslation('name', 'en', $input['account-name'])->setTranslation('name', 'es', $input['account-name']);

                $userAccountingHead->company()->associate($user->company);
                $userAccountingHead->user()->associate($user);

                $userAccountingHead->save();
                DB::commit();

                $response = [
                    'message' => Lang::get('messages.accounting_head_created'),
                    'success' => true
                ];

                if ($request->wantsJson() || $request->ajax()) {
                    return response()->json($response);
                }

            } else {
                throw new Exception(trans('messages.something_went_wrong'), 1);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * Update the accounting charts or head
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateHead(Request $request, UserAccountingHead $userAccountingHead)
    {
        $this->authorize('update', $userAccountingHead);

        $user = $request->user();
        $input = $request->all();

        try {
            DB::beginTransaction();
            $input['slug'] = Str::slug($input['account-name'], '-');
            $accountingHeadValidator = new HeadValidator('edit', $userAccountingHead);

            if (!$accountingHeadValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'messages'  => $accountingHeadValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                return back()
                    ->withErrors($accountingHeadValidator->getValidator())
                    ->with([
                        'message'   => $accountingHeadValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            if ($userAccountingHead && $userAccountingHead->parent_id) {
                // $userAccountingHead->setTranslation('name', 'en', $input['account-name'])->setTranslation('name', 'es', $input['account-name']);
                $userAccountingHead->name = $input['account-name'];
                $userAccountingHead->description = $input['description'];
                $userAccountingHead->code = $input['code'];
                $userAccountingHead->slug = $userAccountingHead->accountingHead ? $userAccountingHead->slug : $input['slug'];

                $userAccountingHead->save();
                DB::commit();

                $response = [
                    'message' => Lang::get('messages.accounting_head_updated'),
                    'success' => true
                ];

                if ($request->wantsJson() || $request->ajax()) {
                    return response()->json($response);
                }

            } else {
                throw new Exception(trans('messages.something_went_wrong'), 1);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }
}
