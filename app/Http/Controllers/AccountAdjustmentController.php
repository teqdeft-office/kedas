<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use Image;
use File;
use Cache;
use App\Client;
use App\Supplier;
use App\AccountAdjustment;
use App\AccountingEntry;
use App\Validators\AccountAdjustmentValidator;

class AccountAdjustmentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(AccountAdjustment::class, 'account_adjustment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $accountAdjustments = $user->company->accountAdjustments;

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $accountAdjustments->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'id' => $item->id,
                    'adjustment_date' => $item->adjustment_date,
                    'observations' => $item->observations,
                    'reference' => $item->reference,
                    'total' => $item->total
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('account-adjustments.index', compact('accountAdjustments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $accounting_heads = $user->company->accountingHeads;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        $all_accounts_options = collect([]);

        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];
        Cache::put('all_accounts_options_'.$user->company->id, $all_accounts_options, 60 * 10); // 10 Minutes

        return view('account-adjustments.create', compact('contact_options', 'contact_attributes', 'all_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AccountAdjustmentValidator $accountAdjustmentValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$accountAdjustmentValidator->with($input)->passes()) {
                $request->session()->flash('error', $accountAdjustmentValidator->getErrors()[0]);
                return back()
                    ->withErrors($accountAdjustmentValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $accountAdjustmentItems = collect($input['items'])->filter(function($row) {
                return $row['concept-item'] && ($row['debit_amount'] > 0 || $row['credit_amount'] > 0);
            })->map(function($item, $key) use($input) {
                if ($item['contact_id']) {
                    list($contact_type, $contact_id) = explode('-', $item['contact_id']);
                    $item['contact_id'] = $contact_id;
                    $item['contact_type'] = $contact_type == self::SUPPLIER ? get_class(new Supplier) : get_class(new Client);
                }
                $item['user_accounting_head_id'] = $item['concept-item'];
                if ($item['debit_amount']) {
                    $item['nature'] = 'debit';
                    $item['amount'] = $item['debit_amount'];
                } else {
                    $item['nature'] = 'credit';
                    $item['amount'] = $item['credit_amount'];
                }
                $item['entry_date'] = $input['adjustment_date'];
                $item['order'] = $key + 1;
                unset($item['id']);
                unset($item['debit_amount']);
                unset($item['credit_amount']);
                unset($item['concept-item']);
                return $item;
            })->values();


            if ($accountAdjustmentItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.je_with_incorrect_info'));
                return back()->withInput();
            }

            $debitCreditAmount = $accountAdjustmentItems->groupBy('nature')->map(function ($row) {
                return $row->sum('amount');
            });

            // Check if the entry is valid or not.
            if ($debitCreditAmount->isEmpty()) {
                $request->session()->flash('error', trans('messages.je_with_no_amount'));
                return back()->withInput();
            }
            if (($debitCreditAmount['credit'] ?? 0) == ($debitCreditAmount['debit'] ?? 0)) {
                $accountAdjustment = $user->company->accountAdjustments()->create([
                    'adjustment_date' => $input['adjustment_date'],
                    'reference' => $input['reference'],
                    'observations' => $input['observations'],
                    'user_id' => $user->id
                ]);
                $accountAdjustment->accountingEntries()->createMany($accountAdjustmentItems);
            } else {
                $request->session()->flash('error', trans('messages.debit_credit_amount_incorrect'));
                return back()->withInput();
            }

            /* upload documents */
            if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/account-adjustment-'.$file));
                        $documents_data[] = [
                            'name' => 'account-adjustment-'.$file,
                        ];
                    }
                }

                $documents = $accountAdjustment->documents()->createMany($documents_data);
            }

            DB::commit();

            // Remove the cached version after create the journal entry.
            Cache::forget('all_accounts_options_'.$user->company->id);

            $request->session()->flash('success', trans('messages.journal_entry_created'));
            return redirect()->route('accounting.adjustments.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(AccountAdjustment $accountAdjustment)
    {
        // prePrint($accountAdjustment);
        return view('account-adjustments.show', compact('accountAdjustment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemCategory  $accountAdjustment
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, AccountAdjustment $accountAdjustment)
    {
        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $accounting_heads = $user->company->accountingHeads;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        $all_accounts_options = collect([]);

        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        Cache::put('all_accounts_options_'.$user->company->id, $all_accounts_options, 60 * 10); // 10 Minutes

        return view('account-adjustments.edit', compact('contact_options', 'contact_attributes', 'all_accounts_options', 'accountAdjustment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $accountAdjustment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountAdjustment $accountAdjustment)
    {
        $accountAdjustmentValidator = new AccountAdjustmentValidator('update');

        try {
            DB::beginTransaction();
            $input = $request->all();
            // prePrint($input);
            $user = $request->user();

            if (!$accountAdjustmentValidator->with($input)->passes()) {
                $request->session()->flash('error', $accountAdjustmentValidator->getErrors()[0]);
                return back()
                    ->withErrors($accountAdjustmentValidator->getValidator())
                    ->withInput();
            }

            $accountAdjustmentItems = collect($input['items'])->filter(function($row) {
                return $row['concept-item'] && ($row['debit_amount'] > 0 || $row['credit_amount'] > 0);
            })->map(function($item, $key) use($input) {
                if ($item['contact_id']) {
                    list($contact_type, $contact_id) = explode('-', $item['contact_id']);
                    $item['contact_id'] = $contact_id;
                    $item['contact_type'] = $contact_type == self::SUPPLIER ? get_class(new Supplier) : get_class(new Client);
                }
                $item['user_accounting_head_id'] = $item['concept-item'];
                if ($item['debit_amount']) {
                    $item['nature'] = 'debit';
                    $item['amount'] = $item['debit_amount'];
                } else {
                    $item['nature'] = 'credit';
                    $item['amount'] = $item['credit_amount'];
                }
                $item['entry_date'] = $input['adjustment_date'];
                $item['order'] = $key + 1;
                unset($item['debit_amount']);
                unset($item['credit_amount']);
                unset($item['concept-item']);
                return $item;
            })->values();


            if ($accountAdjustmentItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.je_with_incorrect_info'));
                return back()->withInput();
            }

            $debitCreditAmount = $accountAdjustmentItems->groupBy('nature')->map(function ($row) {
                return $row->sum('amount');
            });

            // Check if the entry is valid or not.
            if ($debitCreditAmount->isEmpty()) {
                $request->session()->flash('error', trans('messages.je_with_no_amount'));
                return back()->withInput();
            }
            if (($debitCreditAmount['credit'] ?? 0) == ($debitCreditAmount['debit'] ?? 0)) {

                $updateAccountAdjustmentItems = $accountAdjustmentItems->filter(function($row) {
                    return $row['id'];
                })->values();

                $insertAccountAdjustmentItems = $accountAdjustmentItems->filter(function($row) {
                    return !$row['id'];
                })->map(function($item) {
                    unset($item['id']);
                    return $item;
                })->values()->toArray();

                $deleteAccountAdjustmentItemsExcept = $updateAccountAdjustmentItems->pluck('id')->toArray();

                if ($deleteAccountAdjustmentItemsExcept) {
                    $accountAdjustmentItemsToDelete = $accountAdjustment->accountingEntries()->whereNotIn('id', $deleteAccountAdjustmentItemsExcept);
                    $accountAdjustmentItemsToDelete->delete();
                } else {
                    $accountAdjustment->accountingEntries()->delete();
                }

                if ($insertAccountAdjustmentItems) {
                    $accountAdjustment->accountingEntries()->createMany($insertAccountAdjustmentItems);
                }

                if ($updateAccountAdjustmentItems) {
                    foreach ($updateAccountAdjustmentItems as $updateAccountAdjustmentItem) {
                        $updatedData = $updateAccountAdjustmentItem;
                        unset($updatedData['id']);
                        AccountingEntry::findOrFail($updateAccountAdjustmentItem['id'])->update($updatedData);
                    }
                }

                $accountAdjustment->update([
                    'adjustment_date' => $input['adjustment_date'],
                    'reference' => $input['reference'],
                    'observations' => $input['observations']
                ]);
                
            } else {
                $request->session()->flash('error', trans('messages.debit_credit_amount_incorrect'));
                return back()->withInput();
            }

            /* update documents start */
            $documents = $request->input('documents', []);

            if ($accountAdjustment->documents->isNotEmpty()) {
                foreach ($accountAdjustment->documents as $document) {
                    if (!in_array($document->name, $documents)) {
                        $accountAdjustment->documents()->where('id', '=', $document->id)->delete();
                    }
                }
            }
        
            $alreadyExistDocuments = $accountAdjustment->documents->pluck('name')->toArray();

            $dir = public_path("storage/documents");

            foreach ($request->input('documents', []) as $file) {
            
                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
                    if (File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/account-adjustment-'.$file));
                        $document_data = array('name' => 'account-adjustment-'.$file);
                        $accountAdjustment->documents()->create($document_data);
                    }
                }
            }
            /* update documents end */

            DB::commit();

            // Remove the cached version after create the journal entry.
            Cache::forget('all_accounts_options_'.$user->company->id);

            $request->session()->flash('success', trans('messages.journal_entry_updated'));
            return redirect()->route('accounting.adjustments.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountAdjustment  $accountAdjustment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, AccountAdjustment $accountAdjustment)
    {
        try {
            $accountAdjustment->delete();
            $request->session()->flash('success', trans('messages.journal_entry_deleted'));
            return redirect()->route('accounting.adjustments.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
