<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Validators\FixedAssetValidator;

use App\FixedAsset;
use App\Depreciation;

use Auth;
use DB;
use Image;
use Config;
use Illuminate\Validation\ValidationException;

class FixedAssetController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(FixedAsset::class, 'fixed_asset');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $fixedAssets = $user->company->fixedAssets;
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $fixedAssets]);
        }
        return view('fixed-assets.index', compact('fixedAssets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();

        $accounting_heads = $user->company->accountingHeads;

        $fixed_assets_accounts_options = collect([]);
        $expense_accounts_options = collect([]);
        $all_accounts_options = collect([]);

        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        $fixed_assets_account = $accounting_heads->where('code', self::FIXEDASSETSCODE)->first();            
        $this->generateAccountsLevels($fixed_assets_account, $fixed_assets_accounts_options, 1);

        $expense_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
        $expenses_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $expense_accounts)->pluck('id')->toArray();
        $expense_accounts = $accounting_heads->whereIn('parent_id', $expenses_account_types)->values();

        $expense_accounts->each(function($expenses_account) use($expense_accounts_options) {
            $this->generateAccountsLevels($expenses_account, $expense_accounts_options, 1, true);
        });

        $item_category_options = ['' => trans('labels.select_category')] + $user->company->itemCategories->pluck('name', 'id')->toArray();
        $cal_type_options = $this->mapOptionsBasedOnLocale(Config::get('constants.cal_type'));
        $legal_category_options = $this->mapOptionsBasedOnLocale(Config::get('constants.legal_category'));
        $location_options = $this->mapOptionsBasedOnLocale(Config::get('constants.location'));
        return view('fixed-assets.create', compact('item_category_options', 'cal_type_options', 'fixed_assets_accounts_options', 'expense_accounts_options', 'legal_category_options', 'location_options', 'all_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, FixedAssetValidator $fixedAssetValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$fixedAssetValidator->with($input)->passes()) {
                $request->session()->flash('error', $fixedAssetValidator->getErrors()[0]);
                return back()
                    ->withErrors($fixedAssetValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            /*$depreciation_items = collect($input['depreciation_items'])->filter(function($row) {
                return $row['year'] && $row['percentage'];
            })->map(function ($item) {
                unset($item['id']);
                return $item;
            })->values();

            if ($depreciation_items->isEmpty()) {
                $request->session()->flash('error', trans('messages.fa_without_depreciation_items'));
                return back()->withInput();
            }*/

            $depr_should_cal = $input['depr_should_cal'] ?? 1;

            $data = [
                'name' => $input['name'],
                'serial_number' => $input['serial_number'] ?? null,
                'price' => $input['price'],
                'item_category_id' => $input['item_category_id'] ?? null,
                'account' => $input['account'] ?? null,
                'expense_account' => $input['expense_account'] ?? null,
                'date_of_purchase' => $input['date_of_purchase'] ?? null,
                'legal_category' => $input['legal_category'] ?? null,
                'location' => $input['location'] ?? null,
                'description' => $input['description'] ?? null,

                'depr_should_cal' => $depr_should_cal,

                'cal_type' => $input['cal_type'] ?? null,
                'years' => $input['years'] ?? null,
                'scrap_percentage' => $input['scrap_value'] ?? null,
                'user_id' => $user->id,

                /*'percentage' => $input['percentage'] ?? null,
                'book_value' => $input['book_value'] ?? null,
                'final_date' => $input['final_date'] ?? null,*/

            ];

            if ($input['cal_type'] == "units_produced") {
                $data['total_unit_produced'] = $input['total_unit_produced'];
                $data['unit_produced_per_year'] = $input['unit_produced_per_year'];
                $data['same_for_year'] = $input['same_for_year'];
                $data['per_year_prod'] = $input['per_year_prod'] ?? null;
                if ($input['same_for_year'] == "1") {
                    $data['per_year_prod'] = null;
                } else {
                    $dataValid = false;
                    $dataValid = count(explode(',', $data['per_year_prod'])) == $input['years'];
                    $dataValid *= collect(explode(',', $data['per_year_prod']))->every(function ($value, $key) {
                        return is_numeric($value);
                    });
                    if (!$dataValid) {
                        return back()
                            ->withErrors([
                                'per_year_prod' => 'This value is incorrect'
                            ])
                            ->withInput();
                        // throw ValidationException::withMessages(['per_year_prod' => 'This value is incorrect']);
                    }
                }
            }

            if (!$depr_should_cal) {
                $data['cal_type'] = null;
                $data['years'] = null;
                $data['scrap_percentage'] = null;
                $data['total_unit_produced'] = null;
                $data['unit_produced_per_year'] = null;
                $data['same_for_year'] = null;
                $data['per_year_prod'] = null;
            }

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/items");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                $itemImgInstance->orientate()->save($path);

                $data['image'] = $filename;
            }

            $fixedAsset = $user->company->fixedAssets()->create($data);

            // $depreciation = $fixedAsset->depreciation()->createMany($depreciation_items->toArray());

            DB::commit();

            $request->session()->flash('success', trans('messages.fixed_asset_created'));
            return redirect()->route('fixed-assets.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FixedAsset  $fixedAsset
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, FixedAsset $fixedAsset)
    {
        return view('fixed-assets.show', compact('fixedAsset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FixedAsset  $fixedAsset
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, FixedAsset $fixedAsset)
    {
        if ($fixedAsset->accountingEntries->isNotEmpty()) {
            $request->session()->flash('error', trans('messages.fa_cant_edit'));
            return redirect()->route('fixed-assets.index');
        }

        $user = $request->user();

        $accounting_heads = $user->company->accountingHeads;

        $fixed_assets_accounts_options = collect([]);
        $expense_accounts_options = collect([]);

        $fixed_assets_account = $accounting_heads->where('code', self::FIXEDASSETSCODE)->first();            
        $this->generateAccountsLevels($fixed_assets_account, $fixed_assets_accounts_options, 1);

        $expense_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
        $expenses_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $expense_accounts)->pluck('id')->toArray();
        $expense_accounts = $accounting_heads->whereIn('parent_id', $expenses_account_types)->values();

        $expense_accounts->each(function($expenses_account) use($expense_accounts_options) {
            $this->generateAccountsLevels($expenses_account, $expense_accounts_options, 1, true);
        });

        $item_category_options = ['' => trans('labels.select_category')] + $user->company->itemCategories->pluck('name', 'id')->toArray();
        $cal_type_options = $this->mapOptionsBasedOnLocale(Config::get('constants.cal_type'));
        $legal_category_options = $this->mapOptionsBasedOnLocale(Config::get('constants.legal_category'));
        $location_options = $this->mapOptionsBasedOnLocale(Config::get('constants.location'));
        return view('fixed-assets.edit', compact('item_category_options', 'cal_type_options', 'fixed_assets_accounts_options', 'expense_accounts_options', 'legal_category_options', 'location_options', 'fixedAsset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FixedAsset  $fixedAsset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FixedAsset $fixedAsset)
    {
        if ($fixedAsset->accountingEntries->isNotEmpty()) {
            $request->session()->flash('error', trans('messages.fa_cant_edit'));
            return redirect()->route('fixed-assets.index');
        }
        try {
            DB::beginTransaction();
            $input = $request->all();

            $fixedAssetValidator = new FixedAssetValidator('update', $fixedAsset);

            if (!$fixedAssetValidator->with($input)->passes()) {
                $request->session()->flash('error', $fixedAssetValidator->getErrors()[0]);
                return back()
                    ->withErrors($fixedAssetValidator->getValidator())
                    ->withInput();
            }

            /*$depreciation_items = collect($input['depreciation_items'])->filter(function($row) {
                return $row['year'] && $row['percentage'];
            })->values();*/

            $user = $request->user();

            $depr_should_cal = $input['depr_should_cal'] ?? 1;

            $data = [
                'name' => $input['name'],
                'serial_number' => $input['serial_number'] ?? null,
                'price' => $input['price'],
                'item_category_id' => $input['item_category_id'] ?? null,
                'account' => $input['account'] ?? null,
                'expense_account' => $input['expense_account'] ?? null,
                'date_of_purchase' => $input['date_of_purchase'] ?? null,
                'legal_category' => $input['legal_category'] ?? null,
                'location' => $input['location'] ?? null,
                'description' => $input['description'] ?? null,

                'depr_should_cal' => $depr_should_cal,

                'cal_type' => $input['cal_type'] ?? null,
                'years' => $input['years'] ?? null,
                'scrap_percentage' => $input['scrap_value'] ?? null,
                'user_id' => $user->id,

                /*'percentage' => $input['percentage'] ?? null,
                'book_value' => $input['book_value'] ?? null,
                'final_date' => $input['final_date'] ?? null,*/

            ];

            if ($input['cal_type'] == "units_produced") {
                $data['total_unit_produced'] = $input['total_unit_produced'];
                $data['unit_produced_per_year'] = $input['unit_produced_per_year'];
                $data['same_for_year'] = $input['same_for_year'];
                $data['per_year_prod'] = $input['per_year_prod'] ?? null;
                if ($input['same_for_year'] == "1") {
                    $data['per_year_prod'] = null;
                } else {
                    $dataValid = false;
                    $dataValid = count(explode(',', $data['per_year_prod'])) == $input['years'];
                    $dataValid *= collect(explode(',', $data['per_year_prod']))->every(function ($value, $key) {
                        return is_numeric($value);
                    });
                    if (!$dataValid) {
                        return back()
                            ->withErrors([
                                'per_year_prod' => 'This value is incorrect'
                            ])
                            ->withInput();
                        // throw ValidationException::withMessages(['per_year_prod' => 'This value is incorrect']);
                    }
                }
            }

            if (!$depr_should_cal) {
                $data['cal_type'] = null;
                $data['years'] = null;
                $data['scrap_percentage'] = null;
                $data['total_unit_produced'] = null;
                $data['unit_produced_per_year'] = null;
                $data['same_for_year'] = null;
                $data['per_year_prod'] = null;
            }

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/items");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                $itemImgInstance->orientate()->save($path);

                $data['image'] = $filename;
            }

            $fixedAsset->update($data);

            /*$updateDepreciationItems = $depreciation_items->filter(function($row) {
                return $row['id'];
            })->values();

            $insertDepreciationItems = $depreciation_items->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteDepreciationItems = $updateDepreciationItems->pluck('id')->toArray();

            if ($deleteDepreciationItems) {
                $depreciationItemsToDelete = $fixedAsset->depreciation()->whereNotIn('id', $deleteDepreciationItems);
                $depreciationItemsToDelete->delete();
            }

            if ($insertDepreciationItems) {
                $fixedAsset->depreciation()->createMany($insertDepreciationItems);
            }

            if ($updateDepreciationItems) {
                foreach ($updateDepreciationItems as $updateDepreciationItem) {
                    $updatedData = $updateDepreciationItem;
                    unset($updatedData['id']);
                    Depreciation::findOrFail($updateDepreciationItem['id'])->update($updatedData);
                }
            }*/

            DB::commit();

            $request->session()->flash('success', trans('messages.fixed_asset_updated'));
            return redirect()->route('fixed-assets.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FixedAsset  $fixedAsset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, FixedAsset $fixedAsset)
    {
        if ($fixedAsset->accountingEntries->isNotEmpty()) {
            $request->session()->flash('error', trans('messages.fa_cant_delete'));
            return redirect()->route('fixed-assets.index');
        }
        try {
            $fixedAsset->delete();
            $request->session()->flash('success', trans('messages.fixed_asset_deleted'));
            return redirect()->route('fixed-assets.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
