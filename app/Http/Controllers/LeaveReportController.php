<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\LeaveApplication;
use App\Validators\LeaveApplicationValidator;

class LeaveReportController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(LeaveApplication::class, 'leave_application');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $leaveReports = $user->company->leaveApplications;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $leaveReports->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'from_date' => $item->application_from_date,
                    'leave_type' => $item->leaveType->name,
                    'employee' => $item->employee ? $item->employee->name : $item->user->name,
                    'application_date' => $item->created_at,
                    'request_duration' => dateFormat($item->application_from_date)." ".trans('labels.to')." ". dateFormat($item->application_to_date),
                    'to_date' => $item->application_to_date,
                    'approve_by' => $item->approve_by,
                    'approve_date' => $item->approve_date,
                    'purpose' => $item->purpose,
                    'number_of_days' => $item->number_of_days,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leave-reports.index', compact('leaveReports'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        $user = $request->user();
        $leaveApplications = $user->company->leaveApplications;
        $data = $leaveApplications->map(function($item, $key) {
            $leave_types = $user->company->leaveType;
            $approved_leaves = $item->leaveApplications->where('status', 2)->sum('number_of_days');
            prePrint($approved_leaves);
            return $item;
        });
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $employees->each(function ($item, $key) use ($jsonCollection) {
                prePrint($item);
               /*  $taken_leaves = $item->sum('approved_leaves');
                $total_leaves = $item->number_of_days;
                if (!$taken_leaves->isEmpty() && !$total_leaves->isEmpty()) {
                    $remaining_leaves = $total_leaves[0] - $taken_leaves[0];
                }
                else if ($taken_leaves->isEmpty() && !$total_leaves->isEmpty()) {
                    $remaining_leaves = $total_leaves[0];
                }
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'leave_type' => $item->leaveType->name,
                    'number_of_days' => $item->number_of_days,
                    'remaining_leaves' => $remaining_leaves,
                    'taken_leaves' => $taken_leaves[0],
                ]); */
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leave-reports.summary', compact('employees'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_leave_report(Request $request)
    {
        $user = $request->user();
        $leaveReports = $user->leaveApplications;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $leaveReports->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'from_date' => $item->application_from_date,
                    'leave_type' => $item->leaveType->name,
                    'application_date' => $item->created_at,
                    'request_duration' => dateFormat($item->application_from_date)." ".trans('labels.to')." ". dateFormat($item->application_to_date),
                    'to_date' => $item->application_to_date,
                    'approve_by' => $item->approve_by,
                    'approve_date' => $item->approve_date,
                    'purpose' => $item->purpose,
                    'number_of_days' => $item->number_of_days,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leave-reports.my-leave', compact('leaveReports'));
    }
}
