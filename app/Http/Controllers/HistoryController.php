<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\History;
use Config;
use Image;
use Cache;
use App\User;

class HistoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(History::class, 'histories');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        
        $histories = $user->company->histories;

        foreach ($histories as $key => $value) {
            $histories[$key]->UserName = User::find($value->user_id)->name;
            if($value->action_type == 'create'){
                $value->action_type = trans('labels.create');
            }elseif($value->action_type == 'delete'){
                $value->action_type = trans('labels.delete');
            }elseif($value->action_type == 'update'){
                $value->action_type = trans('labels.update');
            }
        }
        
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $histories]);
        }
        return view('histories.index', compact('histories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {

   }

   /**
     * Display the specified resource.
     *
     * @param  \App\History  $table
     * @return \Illuminate\Http\Response
     */
    public function show($table)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\History  $table
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $table)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\History  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\History  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

    }


}
