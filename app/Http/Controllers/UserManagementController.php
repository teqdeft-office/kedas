<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Validators\User\CompanyUserValidator;

use App\User;
use App\Employee;

use Carbon\Carbon;

use DB;
use Cache;

class UserManagementController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(User::class, 'company_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $users = $company->users();

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $users->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'email' => $item->email,
                    'role' => ucfirst($item->role() ? $item->role()->custom_name : trans('N/A'))
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
                
        return view('user-management.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $roles = $company->roles;
        $permissions = Cache::get('permissions');

        $role_options = ['' => trans('labels.select_role')] + $roles->pluck('custom_name', 'id')->toArray();
        $permission_options = $permissions->pluck('custom_name', 'id')->toArray();

        return view('user-management.users.create', compact('role_options', 'permission_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CompanyUserValidator $companyUserValidator)
    {
        $user = $request->user();

        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$companyUserValidator->with($input)->passes()) {
                $request->session()->flash('error', $companyUserValidator->getErrors()[0]);
                return back()
                    ->withErrors($companyUserValidator->getValidator())
                    ->withInput();
            }

            $role = $input['role'] ?? null;
            $permissions = $input['permissions'] ?? null;

            $companyUser = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'is_category_added' => 1,
                'password' => Hash::make($input['password'])
            ]);

            $data = [
                'name' => $input['name'],
                'email' => $input['email'],
                'username' => $input['email'],
                'password' => Hash::make($input['password']),
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'is_company_emp' => false
            ];

            $employee = Employee::create($data);

            $companyUser->email_verified_at = Carbon::now();// as of now the email is verified after user is created.
            $companyUser->company_id = $user->company->id;
            $companyUser->user_role = self::COMPANY_USER; // set the new created user as company user.
            $companyUser->employee_id = $employee->id;
            $companyUser->is_unit_added = 1;
            $companyUser->save();

            $employee->related_user_id = $companyUser->id;
            $employee->save();

            // Todo check permission against the role. Give error in case no permission there for selected role.
            if ($role) {
                $companyUser->assignRole($role);
            }

            if ($permissions) {
                $companyUser->givePermissionTo(array_filter($permissions));
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.company_user_created'));
            return redirect()->route('user-management.users');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $company_user)
    {
        $user = $request->user();
        $company = $user->company;
        $roles = $company->roles;
        $permissions = Cache::get('permissions');
        $assignedPermissions = $company_user->permissions->pluck('id')->toArray();
        $assignedRole = $company_user->role();

        $role_options = ['' => trans('labels.select_role')] + $roles->pluck('custom_name', 'id')->toArray();
        $permission_options = $permissions->pluck('custom_name', 'id')->toArray();

        return view('user-management.users.edit', compact('role_options', 'permission_options', 'company_user', 'assignedPermissions', 'assignedRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $companyUser)
    {
        $user = $request->user();
        $companyUserValidator = new CompanyUserValidator('update', $companyUser);

        try {
            DB::beginTransaction();
            $input = $request->all();
            $passwordChange = false;

            if (!$companyUserValidator->with($input)->passes()) {
                $request->session()->flash('error', $companyUserValidator->getErrors()[0]);
                return back()
                    ->withErrors($companyUserValidator->getValidator())
                    ->withInput();
            }

            $companyUser->update([
                'name' => $input['name'],
                'email' => $input['email']
            ]);

            $companyUser->employee->update([
                'name' => $input['name'],
                'email' => $input['email']
            ]);

            if (isset($input['password']) && $input['password']) {
                $companyUser->password = Hash::make($input['password']);
                $passwordChange = true;
            }

            $companyUser->save();

            // Todo check permission against the role. Give error in case no permission there for selected role.
            $role = $input['role'] ?? [];            
            $companyUser->syncRoles($input['role']);

            $permissions = $input['permissions'] ?? [];
            $companyUser->syncPermissions(array_filter($permissions));

            if ($passwordChange) {
                // todo:- logout the company user after password change. 
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.company_user_updated'));
            return redirect()->route('user-management.users');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $companyUser)
    {
        try {
            $companyUser->permissions()->detach();
            $companyUser->roles()->detach();
            $companyUser->delete();
            $request->session()->flash('success', trans('messages.company_user_deleted'));
            return redirect()->route('user-management.users');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
