<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\RestsurantTables;
use App\Validators\TableValidator;
use Config;
use Image;
use Cache;

class TableController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(RestsurantTables::class, 'restaurantTables');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        // $table = $user->company->table;
        $tables = $user->company->tables;
        foreach ($tables as $key => $value) {
            if($value->zone_id == 1){
                $value->zone_id = trans('labels.main_room');
            }elseif($value->zone_id == 2){
                $value->zone_id = trans('labels.inside_room');
            }elseif($value->zone_id == 2){
                $value->zone_id = trans('labels.outside_room');
            }else{
                $value->zone_id = trans('labels.buffer_room');
            }
        }
        // dd($tables);
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $tables]);
        }
        return view('table.index', compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        // $companyClients = $user->company->clients;
        // $companySuppliers = $user->company->suppliers;
        // $inventories = $user->company->inventories;

        // $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();

        return view('table.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request, TableValidator $tableValidator)
   {
    
       try {
           DB::beginTransaction();
           $input = $request->all();

           if (!$tableValidator->with($input)->passes()) {
               $request->session()->flash('error', $tableValidator->getErrors()[0]);
               return back()
                   ->withErrors($tableValidator->getValidator())
                   ->withInput();
           }

           $user = $request->user();

           $data = [
               'table_name' => $input['table_name'],
               'date' => $input['date'],
               'description' => $input['description'],
               'zone_id' => $input['zone'],
               'table_number' => $input['table_number'],
               'chairs_number' => $input['chairs_number'],
               'user_id' => $user->id,
               'company_id' => $user->company->id
            //    'user_id' => $user->id
           ];

           if (isset($input['table_image']) && $input['table_image'])  {
            $filename = $user->id.'-table-'.substr( md5( $user->id . '-' . time() ), 0, 15) .'.jpg';
            $dir = public_path("storage/tables");
            $path = $dir."/".$filename;

            if( !\File::isDirectory($dir) ) {
                \File::makeDirectory($dir, 493, true);
            }

            $itemImgInstance = Image::make($input['table_image']);
            /*if ($itemImgInstance->height() > 300) {
                $itemImgInstance->fit(300)->save($path);
            } else {
            }*/
            $itemImgInstance->orientate()->save($path);

            $data['table_image'] = $filename;
        }

           $tables = new RestsurantTables;

           $table = $tables->create($data);

           DB::commit();

           $request->session()->flash('success', trans('messages.table_created'));
           
           switch($request->submit) {

                case 'save': 
                    return redirect()->route('restaurant-table.index');
                break;
            
                case 'saveAndAdd': 
                    return redirect()->route('restaurant-table.create');
                break;
            }

       } catch (\Exception $e) {
           DB::rollback();
           $request->session()->flash('error', $e->getMessage());
           return back()->withInput();
       }
   }

   /**
     * Display the specified resource.
     *
     * @param  \App\RestsurantTables  $table
     * @return \Illuminate\Http\Response
     */
    public function show($table)
    {
        $table = RestsurantTables::find($table);
        return view('table.show', compact('table'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RestsurantTables  $table
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $table)
    {
        $table = RestsurantTables::find($table);
        // $resource_associated = $tax->items->isNotEmpty() || $tax->conceptitems->isNotEmpty();

        // $user = $request->user();
        // $accounting_heads = $user->company->accountingHeads;

        return view('table.edit', compact('table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RestsurantTables  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $table)
    {
        try {
            DB::beginTransaction();
            $tables = RestsurantTables::find($table);
            $input = $request->all();

            $tableValidator = new TableValidator('update', $table);

            if (!$tableValidator->with($input)->passes()) {
                $request->session()->flash('error', $tableValidator->getErrors()[0]);
                return back()
                    ->withErrors($tableValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'table_name' => $input['table_name'],
                'date' => $input['date'],
                'description' => $input['description'],
                'zone_id' => $input['zone'],
                'table_number' => $input['table_number'],
                'chairs_number' => $input['chairs_number'],
                'user_id' => $user->id,
                'company_id' => $user->company->id,          
             //    'user_id' => $user->id
            ];

            if (isset($input['table_image']) && $input['table_image'])  {
                $filename = $user->id.'-table-'.substr( md5( $user->id . '-' . time() ), 0, 15) .'.jpg';
                $dir = public_path("storage/tables");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) {
                    \File::makeDirectory($dir, 493, true);
                }

                $itemImgInstance = Image::make($input['table_image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);

                $data['table_image'] = $filename;
            }


            $tables->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.table_updated'));
            return redirect()->route('restaurant-table.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RestsurantTables  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $table)
    {
        $table = RestsurantTables::find($table);
        try {
            if (!empty($table->table_name)) {
                $table->delete();
                $request->session()->flash('success', trans('messages.table_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('restaurant-table.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }


}
