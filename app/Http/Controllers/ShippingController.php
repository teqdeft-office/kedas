<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\User;
use App\Item;
use PDF;
use Cache;
class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type = self::SALE)
    {
        $user = $request->user();
        $invoices = $user->company->invoices->where('shipping_cost', '!=', '0')->values();      
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $invoices->each(function ($item, $key) use ($jsonCollection) {

                $jsonCollection->push([
                    'id' => $item->id,
                    'contact' => $item->contact,
                    'start_date' => $item->start_date,
                    'expiration_date' => $item->expiration_date,
                    'internal_number' => $item->internal_number,
                    'notes' => $item->notes,
                    'calculation' => $item->calculation,

                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('shipping.index', compact('invoices', 'type'));
    }
  
    public function show(Request $request, $invoices)
    {
        $user = $request->user();
        $invoices = Invoice::where('id' , $invoices)->get();
        if ($request->has('view')) 
        {
            $viewType = $request->input('view');
        } else
        {
            $viewType = 'view-order';
        }
        
        return view('shipping.show', compact('invoices', 'viewType'));
        
    }


}
