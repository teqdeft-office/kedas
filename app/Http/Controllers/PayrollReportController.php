<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Employee;
/* use App\Validators\TaxValidator; */

class PayrollReportController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Tax::class, 'tax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $employees = $user->company->employees;
        /* $employees = $data->map(function ($emp) {
            return [
                'id' => $emp->id,
                'name' => $emp->name,
                'hiring_date' => $emp->hiring_date,
                'salary' => $emp->salary,
            ];
        }); */

        /* if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $taxes]);
        } */
        return view('payroll-reports.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paid(Request $request)
    {
        try {
            $input = $request->all();
            $user = $request->user();

            $data = [
                        'amount' => $input['total_payment'],
                        'employee_id' => $input['employee_id'],
                        'user_id' => $user->id
                    ];

           $user->company->employeePayment()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.employee_payment_created'));
            return redirect()->route('payroll-reports.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }
}
