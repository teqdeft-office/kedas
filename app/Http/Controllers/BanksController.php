<?php

namespace App\Http\Controllers;

use App\Banks;
use App\Company;
use App\Validators\BankValidator;
use Illuminate\Http\Request;
use Auth;
use Cache;
use DB;

class BanksController extends Controller
{
    public function __construct()
    {
        // $this->authorizeResource(Banks::class, 'banks');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $banks = $user->company->banks;
        $add_amount = 0;
        $min_amount = 0;
        if(!empty($banks)){
            foreach ($banks as $key => $bank) {
                if(!empty($bank->transaction)){
                    foreach ($bank->transaction as $key => $value) {
                        if($value->type == 'in'){
                            $add_amount = $add_amount + $value->amount;
                        }elseif($value->type == 'out'){
                            $min_amount = $value->amount+$min_amount;
                        }
                    }
                    
                }
                $bank->account_balance = $bank->account_balance+$add_amount-$min_amount;
            }
        }
        
        
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $banks->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'bank_name' => $item->bank_name,
                    'account_number' => $item->account_number,
                    'routing_number' => $item->routing_number,
                    'reference' => $item->reference,
                    'name_of_account' => $item->name_of_account,
                    'account_balance' => $item->account_balance,
                    'created_at' => $item->created_at,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('banks.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('banks.create');
        // return view('banks.create', compact("currency_options"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            $bankValidator = new BankValidator('add');

            if (!$bankValidator->with($input)->passes()) {
                $request->session()->flash('error', $bankValidator->getErrors()[0]);
                return back()
                    ->withErrors($bankValidator->getValidator())
                    ->withInput();
            }


            $user = $request->user();
            $next_number = $user->company->banks()->withTrashed() ? $user->company->banks()->withTrashed()->count() + 1 : 1;
            $data = array(
                "bank_name" => $input["bank_name"],
                "bank_date" => $input["bank_date"],
                "name_of_account" => $input["name_of_account"],
                "account_balance" => $input["account_balance"],
                "account_number" => $input["account_number"],
                "currency" => $input["currency"],
                "bank_address" => $input["bank_address"],
                // "reference" => "BANK".$user->company->id.$user->id.$next_number,
                "phone" => $input["phone"],
                "email" => $input["email"],
                "swift_code" => $input["swift_code"],
                "routing_number" => $input["routing_number"],
                "intermediary" => $input["intermediary"],
                "notes" => $input["notes"],
                "account_type" => $input["account_type"],
                "reference" => $input["reference"] ?? "BANK".$user->company->id.$user->id.$next_number,
            );
            $bank = $user->company->banks()->create($data);
            DB::commit();
            $request->session()->flash('success', trans('messages.bank_created'));
            switch($request->submit) {
                case 'save':
                    return redirect()->route('banks.index');
                break;
                case 'save_new':
                    return redirect()->route('banks.create');
                break;
                default:
                    return redirect()->route('banks.index');
                break;
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bank  $banks
     * @return \Illuminate\Http\Response
     */
    public function show(banks $bank)
    {
        $add_amount = 0;
        $min_amount = 0;
        if(!empty($bank->transaction)){
            foreach ($bank->transaction as $key => $value) {
                if($value->type == 'in'){
                    $add_amount = $add_amount + $value->amount;
                }elseif($value->type == 'out'){
                    $min_amount = $value->amount+$min_amount;
                }
            }
            
        }
        return view('banks.show', compact('bank', 'add_amount', 'min_amount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bank  $banks
     * @return \Illuminate\Http\Response
     */
    public function edit(banks $bank)
    {

        return view('banks.edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bank  $banks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, banks $bank)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            $bankValidator = new BankValidator('update', $bank);

            if (!$bankValidator->with($input)->passes()) {
                $request->session()->flash('error', $bankValidator->getErrors()[0]);
                return back()
                    ->withErrors($bankValidator->getValidator())
                    ->withInput();
            }
            $user = $request->user();
            $next_number = $user->company->banks()->withTrashed() ? $user->company->banks()->withTrashed()->count() + 1 : 1;
            $data = array(
                "bank_name" => $input["bank_name"],
                "bank_date" => $input["bank_date"],
                "name_of_account" => $input["name_of_account"],
                "account_balance" => $input["account_balance"],
                "account_number" => $input["account_number"],
                "currency" => $input["currency"],
                "bank_address" => $input["bank_address"],
                "email" => $input["email"],
                "phone" => $input["phone"],
                "swift_code" => $input["swift_code"],
                "routing_number" => $input["routing_number"],
                "intermediary" => $input["intermediary"],
                "notes" => $input["notes"],
                "account_type" => $input["account_type"],
                "reference" => $input["reference"] ?? "BANK".$user->company->id.$user->id.$next_number,
            );
            $bank->update($data);
            DB::commit();
            $request->session()->flash('success', trans('messages.bank_updated'));
            return redirect()->route('banks.index');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\banks  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,banks $bank)
    {
        try {
            $bank->delete();
            $request->session()->flash('success', trans('messages.bank_deleted'));
            return redirect()->route('banks.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }
}
