<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Traits\ApiResponse;
use App\Validators\RatingValidator;

use App\Rating;

use DB;
use Auth;

class RatingController extends Controller
{
	use ApiResponse;

    /**
     * Save rating from kedas
     * 
     * @OA\Post(
     *     path="/api/v1/ratings",
     *     tags={"Rating"},
     *     operationId="saveRating",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="rating",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         name="review",
     *         in="query",
     *     ),
     *     @OA\Parameter(
     *         name="rating_id",
     *         in="query",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function store(Request $request, RatingValidator $ratingValidator)
    {
    	try {
    		DB::beginTransaction();
    		$input = $request->all();
            $input["user_id"] = $request->user()->id;
    		if (!$ratingValidator->with($input)->passes()) {
    			return $this->failResponse([
    				"message" => $ratingValidator->getErrors()[0],
    				"messages" => $ratingValidator->getErrors()
    			], 200);
    		}

    		$rating = Rating::create($input);
    		$rating->save();
    		DB::commit();
    		return $this->successResponse([
    			"message" => trans('messages.feedback'),
    			"data" => $rating
    		]);

    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }
}
