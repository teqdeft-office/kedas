<?php
class Controller
{
    /**
 * @license Apache 2.0
 */

/**
 * @OA\Info(
 *  description="Kedas is an accounting and billing system, designed to manage your business,
	make it grow and make the most of your time.  You can find out more about Kedas at [http://kedasrd.com/](http://kedasrd.com/) 
	<br><h3> Set header for X-localization = en or es for multiple language support </h3>",
 *     version="v1",
 *     title="KEDAS - Accounting Software",
 *     termsOfService="http://kedasrd.com/terms-conditions",
 *     @OA\Contact(
 *         email="info@kedas.com"
 *     )
 * )
 */
/**
 * 		@OA\Server(
 *     		description="teqdeft",
 *     		url=L5_SWAGGER_CONST_HOST
 * 		)
 *     	@SWG\SecurityScheme(
 *          securityDefinition="default",
 *          type="apiKey",
 *          in="header",
 *          name="Authorization"
 *      )
 */

}
