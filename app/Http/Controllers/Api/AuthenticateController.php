<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\Validators\User\RegisterValidator;
use App\Validators\User\ProfileValidator;
use App\Validators\User\LoginValidator;

use App\Mail\ApiUserVerify;
use App\Mail\ApiForgotPassword;

use App\Transformers\UserTransformer;

use DB;
use Auth;
use Config;
use GuzzleHttp;

use App\User;
use App\Traits\ApiResponse;

/**
 * Class Authenticate
 *
 * @package Authenticate\controllers
 *
 * @author  Teqdeft [https://www.teqdeft.com/]
 */
class AuthenticateController extends Controller
{
	use ApiResponse;

    /**
     * Register a new user to the kedas
     * 
     * @OA\Post(
     *     path="/api/v1/register",
     *     tags={"Authenticate"},
     *     operationId="register",
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
  	 *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The response code",
     *						   example="john smith"
     *                     ),
     *                     @OA\Property(
     *                         property="email",
     *                         type="string",
     *                         description="The response message",
     *						   example="teqdeft@yopmail.com"
     *                     ),
     *                     @OA\Property(
     *                         property="phone",
     *                         type="integer",
     *                         description="The response data",
     *						   example="1234567890"
     *                     ),
     *						@OA\Property(
     *                         property="password",
     *                         type="string",
     *                         description="The response data",
     *						   example="123456"
     *                     ),
     *                     @OA\Property(
     *                         property="localization",
     *                         type="string",
     *                         description="set the language",
     *                         example="en or es"
     *                     ),
     *                 )
     *             )
     *         }
     *     )
     * )
     */

    public function register(Request $request, RegisterValidator $registerValidator) {
    	try {
    		DB::beginTransaction();
    		$input = $request->all();
            $input['user_role'] = $input['user_role'] ?? self::APP_USER;

    		if (!$registerValidator->with($input)->passes()) {
    			return $this->failResponse([
    				"message" => $registerValidator->getErrors()[0],
    				"messages" => $registerValidator->getErrors()
    			], 200);
    		}
    		$input['password'] = Hash::make($input['password']);
    		$user = User::create($input);
    		$user->user_role = $input['user_role'];
    		$user->email_token = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
    		$user->save();

    		DB::commit();

    		Mail::to($user->email)->send(new ApiUserVerify($user));

    		return $this->successResponse([
    			"message" => trans('messages.reverify_success'),
    			"data" => (new UserTransformer())->singleTransform($user)
    		]);

    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

    /**
     * Login user to the kedas
     *  @ApiIgnore,
        @ApiOperation(value = "Already using authenticated"),
        @GetMapping(""/api/v1/login"),
     * @OA\Post(
     *     path="/api/v1/login",
     *     tags={"Authenticate"},
     *     operationId="login",
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *		@OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
  	 *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="email",
     *                         type="string",
     *                         description="The response message",
     *						   example="teqdeft@yopmail.com"
     *                     ),
     *						@OA\Property(
     *                         property="password",
     *                         type="string",
     *                         description="The response data",
     *						   example="123456"
     *                     ),
     *                     @OA\Property(
     *                         property="localization",
     *                         type="string",
     *                         description="set the language",
     *                         example="en or es"
     *                     ),
     *                 )
     *             )
     *         }
     *     )
     * )
     */

        public function login(Request $request, LoginValidator $loginValidator) 
        {
        	try{
        		$input = $request->all();
        		if (!$loginValidator->with($input)->passes()) {
        			return $this->failResponse([
        				"message" => $loginValidator->getErrors()[0],
        				"messages" => $loginValidator->getErrors()
        			], 200);
        		}
                $input['user_role'] = $input['user_role'] ?? self::APP_USER;

        		$user = User::where(['email' => $request->email, 'user_role' => $input['user_role']])->first();
        		if($user){
        			if ($user->email_token !== NULL && $user->email_verified_at == NULL) {
        				return $this->failResponse([
        					"message" => trans('messages.account_not_verified'),
        				], 200);
        			}
        			if ($user && Hash::check($request->password, $user->password)) {
        				$data = $this->getToken($request);
        				$token = json_decode((string) $data->getBody(), true);
                        $token['is_profile_updated'] = ($user->addresses()->count() > 0) ? true : false;
                            
        				$response = ["message" => trans('messages.login_success'),"data"=> $token];
        				return $this->successResponse($response);
        			}else{
                        $response = ['message'=> trans('messages.password_error')];
                        return $this->failResponse($response, 200);    
                    }
        		} else {
        			$response = ['message'=> trans('auth.failed')];
        			return $this->failResponse($response, 200);
        		}
        	} catch (\Exception $e) {
        		return $this->failResponse([
        			"message" => $e->getMessage(),
        		], 200);
        	}
        }

    /**
     * Logout a logged user from kedas
     * 
     * @OA\Post(
     *     path="/api/v1/logout",
     *     tags={"Authenticate"},
     *     operationId="logout",
     * 	   security={
     *  		{"passport": {}},
     *   	},
     *     @OA\Response(
     *         response=200,
     *         description="ok",
  	 *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function logout(Request $request) {
    	try {
    		$token = $request->user()->token();
    		$token->revoke();
    		$response = ['message'=> trans('messages.logout_success'), 'data' => []];
    		return $this->successResponse($response);
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

    /**
     * Forgot password
     * 
     * @OA\Post(
     *     path="/api/v1/forgot-password",
     *     tags={"Authenticate"},
     *     operationId="forgotPassword",
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *		@OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
  	 *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="email",
     *                         type="string",
     *                         description="The response message",
     *						   example="teqdeft@yopmail.com"
     *                     )
     *                 )
     *             )
     *         }
     *     )
     * )
     */
    public function forgotPassword(Request $request) {
    	try {
    		$input = $request->all();
    		$rules = array(
    			'email' => "required|email",
    		);
    		$validator = Validator::make($input, $rules);
    		if ($validator->fails()) {
    			$arr = array("message" => $validator->errors()->first());
    		} else {
    			try {
    				$user = User::where(['email' => $input['email'], 'user_role' => self::APP_USER])->first();
    				if ($user) {
    					$user->email_token = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
    					$user->save();
    					Mail::to($user->email)->send(new ApiForgotPassword($user));

    					DB::commit();
    					return $this->successResponse([
    						'message' => trans('messages.forgot_password_email'),
    						'data' => []
    					]);
    				} else {
    					$arr = ["message" => trans('messages.something_went_wrong')];        
    				}
    			} catch (\Swift_TransportException $ex) {
    				$arr = ["message" => $ex->getMessage()];
    			} catch (Exception $ex) {
    				$arr = ["message" => $ex->getMessage()];
    			}
    		}

    		return $this->failResponse($arr, 200);
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

    /**
     * Change password
     * 
     * @OA\Post(
     *     path="/api/v1/change-password",
     *     tags={"Authenticate"},
     *     operationId="changePassword",
     * 	   security={
     *  		{"passport": {}},
     *   	},
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *		@OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *     ),
  	 *		@OA\Parameter(
     *         name="current_password",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="password_confirmation",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
  	 *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The response message",
     *						   example="test john"
     *                 		),
     *                     @OA\Property(
     *                         property="current_password",
     *                         type="string",
     *                         description="The response message",
     *						   example="123456"
     *                 		),
     *                     @OA\Property(
     *                         property="password",
     *                         type="string",
     *                         description="The response message",
     *						   example="654321"
     *                 		),
	 *                     @OA\Property(
	 *                         property="password_confirmation",
	 *                         type="string",
	 *                         description="The response message",
	 *						   example="654321"
	 *                     )
	 *               )  
     *             )
     *         }
     *     )
     * )
     */
    public function changePassword(Request $request)
    {
    	try{
    		$input = $request->all();
            $user = $request->user();

    		$userUpdateValidator = new ProfileValidator('api_update');
    		if (!$userUpdateValidator->with($input)->passes()) {
    			return $this->failResponse([
    				"message" => $userUpdateValidator->getErrors()[0],
    				"messages" => $userUpdateValidator->getErrors()
    			], 200);
    		}
    		if ((Hash::check(request('current_password'), Auth::user()->password)) == false) {
    			$arr = ["message" => trans('messages.old_password_error')];
    		} else if ((Hash::check(request('password'), Auth::user()->password)) == true) {
    			$arr = ["message" => trans('messages.password_similiar_error')];
    		} else {
    			$user->password = Hash::make($input['password']);
                $user->save();

                // $user->token()->revoke(); 
                // $user->token()->delete(); 

    			return $this->successResponse(["message" => trans('messages.password_change_success')]);
    		}
    		return $this->failResponse($arr, 200);
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

    /**
     * Verify account before access
     * 
     * @OA\Post(
     *     path="/api/v1/verify-account",
     *     tags={"Authenticate"},
     *     operationId="verifyAccount",
     * 	   security={
     *  		{"passport": {}},
     *   	},
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
  	 *		@OA\Parameter(
     *         name="email_token",
     *         in="query",
     *         required=true,
     *     ),
     *		@OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
  	 *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="email_token",
     *                         type="string",
     *                         description="email token",
     *						   example="1111"
     *                 		),
	 *                     @OA\Property(
	 *                         property="email",
	 *                         type="string",
	 *                         description="enter email",
	 *						   example="teqdeft@yopmail.com"
	 *                     )
	 *               )  
     *             )
     *         }
     *     )
     * )
     */
    public function verifyAccount(Request $request) {
    	try{
    		$input = $request->all();
    		$userUpdateValidator = new ProfileValidator('verify_account');

    		if (!$userUpdateValidator->with($input)->passes()) {
    			return $this->failResponse([
    				"message" => $userUpdateValidator->getErrors()[0],
    				"messages" => $userUpdateValidator->getErrors()
    			], 200);
    		}

    		$user = User::where(['email' => $input['email'], 'user_role' => self::APP_USER])->first();

    		if ($user) {
                if ($user->hasVerifiedEmail()) {
                    $response = ['message'=> trans('messages.account_already_verified')];
                    return $this->failResponse($response, 200);
                }
                if ($user->email_token != $input['email_token']) {
                    $response = ['message'=> trans('messages.invalid_token')];
                    return $this->failResponse($response, 200);
                }

    			$user->email_token = null;
    			$user->email_verified_at = date('Y-m-d h:i:s');
    			$user->save();
    			$response = ["message" => trans('messages.register_verification_success')];
    			return $this->successResponse($response);
    		} else {
    			$response = ['message'=> trans('messages.something_went_wrong')];
    			return $this->failResponse($response, 200);
    		}
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }


    /**
     * Reset user password after forgot password step
     * 
     * @OA\Post(
     *     path="/api/v1/reset-password",
     *     tags={"Authenticate"},
     *     operationId="resetPassword",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *      @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="email_token",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="email_token",
     *                         type="string",
     *                         description="email token",
     *                         example="1111"
     *                      ),
     *                     @OA\Property(
     *                         property="email",
     *                         type="string",
     *                         description="enter email",
     *                         example="teqdeft@yopmail.com"
     *                     )
     *               )  
     *             )
     *         }
     *     )
     * )
     */
    public function resetPassword(Request $request) {
    	try{
    		$input = $request->all();

    		$userUpdateValidator = new ProfileValidator('api_reset_password');
    		if (!$userUpdateValidator->with($input)->passes()) {
    			return $this->failResponse([
    				"message" => $userUpdateValidator->getErrors()[0],
    				"messages" => $userUpdateValidator->getErrors()
    			], 200);
    		}

    		$user = User::where(['email_token' => $input['email_token'], 'email' => $input['email'], 'user_role' => self::APP_USER])->first();

    		if ($user) {
                $user->password = Hash::make($input['password']);
                $user->email_token = null;
                $user->save();
    			return $this->successResponse(["message" => trans('messages.password_change_success')]);

    		} else {
    			$response = ['message'=> trans('messages.invalid_otp')];
    			return $this->failResponse($response, 403);
    		}
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

	/**
     * Resend code before access
     * 
     * @OA\Post(
     *     path="/api/v1/resend-code",
     *     tags={"Authenticate"},
     *     operationId="resendCode",
     *     security={{"default": {}}},
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=""
     *     ),
     * )
     */
	public function resendCode(Request $request) {

		try {
			DB::beginTransaction();
            $input = $request->all();
			$rules = array(
				'email' => "required|email",
			);

			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
				return $this->failResponse([
					"message" => $validator->errors()->first()
				], 200);
			}

			$user = User::where(['email' => $input['email'] , 'user_role' => self::APP_USER])->first();
			if ($user) {
                $user->email_token = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
				$user->save();
				Mail::to($user->email)->send(new ApiUserVerify($user));

				DB::commit();
				return $this->successResponse([
					"message" => trans('messages.reverify_success'),
				]);
			} else{
				return $this->failResponse([
					"message" => trans('messages.something_went_wrong'),
				], 200);    
			}
		} catch (\Exception $e) {
			return $this->failResponse([
				"message" => $e->getMessage(),
			], 200);
		}
	}

	/**
	 * User login token
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	protected function getToken($request) {
		try{
			$http = new GuzzleHttp\Client;
			$data = $http->post(env('APP_URL', 'http://localhost/kedas').'/oauth/token', [
				'form_params' => [
					'grant_type' => 'password',
					'client_id' => env('CLIENT_ID'),
					'client_secret' => env('CLIENT_SECRET'),
					'username' => $request->email,
					'password' => $request->password,
		        // 'scope' => '*',
				],
			]);
			return $data;
		} catch( \Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * User login token
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function refreshToken(Request $request){
		try {
			$input = $request->all();
			if ($input['refresh_token']) {
				$http = new GuzzleHttp\Client;
				$data = $http->post(env('APP_URL', 'http://localhost/kedas').'/oauth/token', [
					'form_params' => [
						'grant_type' => 'refresh_token',
						'refresh_token' => $input['refresh_token'],
						'client_id' => env('CLIENT_ID'),
						'client_secret' => env('CLIENT_SECRET'),
					],
				]);
				$token = json_decode((string) $data->getBody(), true);
				return $this->successResponse([
					"message" => trans('messages.token_generated'),
					"data" => $token,
				]);
			} else {
				return $this->failResponse([
					"message" => trans("messages.something_went_wrong"),
				], 200);
			}
		} catch( \Exception $e){
			return $this->failResponse([
				"message" => $e->getMessage(),
			], 200);
		}
	}
}