<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\NcfValidator;

use App\Http\Resources\Ncf as NcfResource;
use App\Http\Resources\NcfCollection;

use App\Ncf;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

use App\Repository\NcfRepositoryInterface;

class NcfController extends Controller
{
    use ApiResponse;

    private $ncfRepository;
  
    public function __construct(NcfRepositoryInterface $ncfRepository)
    {
        $this->ncfRepository = $ncfRepository;
        $this->authorizeResource(Ncf::class, 'ncf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        $ncfs = $this->ncfRepository->findforCompany($company);

        if ($term) {
            $ncfs = $ncfs->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false
                && mb_strpos(strtolower($element->initial_number), strtolower($term)) === false
                && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $ncfs->paginate($perPage);
        $collection = new NcfCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, NcfValidator $ncfValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$ncfValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $ncfValidator->getErrors()[0],
                    "messages" => $ncfValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();
            $company = $user->company;

            $data = [
                'name' => $input['name'],
                'initial_number' => $input['ncf_number'],
                'description' => $input['description'],
                'user_id' => $user->id
            ];

            // $ncf = $user->company->ncfs()->create($data);
            $ncf = $this->ncfRepository->create($company, $data);

            DB::commit();

            return $this->successResponse([
                'data' => new NcfResource($ncf),
                'message' => trans('messages.ncf_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function show(Ncf $ncf)
    {
        // return new NcfResource($ncf);
        return $this->successResponse(['data' => new NcfResource($ncf)]);
        // return (new NcfResource($ncf))->additional(['status' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ncf $ncf, NcfValidator $ncfValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $resource_associated = $ncf->invoices->isNotEmpty();

            if (!$ncfValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $ncfValidator->getErrors()[0],
                    "messages" => $ncfValidator->getErrors(),
                ], 422);
            }

            $data = [
                'name' => $input['name'],
                'initial_number' => $resource_associated ? $ncf->initial_number : $input['ncf_number'],
                'description' => $input['description']
            ];

            $this->ncfRepository->update($ncf, $data);

            DB::commit();

            return $this->successResponse([
                'data' => new NcfResource($ncf),
                'message' => trans('messages.ncf_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Ncf $ncf)
    {
        try {
            if ($ncf->invoices->isEmpty()) {
                $ncf->delete();
                return $this->successResponse([
                    'message' => trans('messages.ncf_deleted')
                ]);
            } else {
                return $this->failResponse([
                    'message' => trans('messages.delete_prohibited')
                ], 403);
            }
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }
}