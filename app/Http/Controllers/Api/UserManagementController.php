<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

use App\Validators\User\CompanyUserValidator;

use App\Http\Resources\CompanyUser as CompanyUserResource;
use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\CompanyUserCollection;

use App\User;
use App\Role;

use App\Traits\ApiResponse;

use Carbon\Carbon;

use DB;
use Validator;

class UserManagementController extends Controller
{
    use ApiResponse;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(User::class, 'company_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $user = $request->user();
            $users = $user->company->users();

            $perPage = $request->input('per_page') ?? 10;
            $page = $request->input('page') ?? 1;

            $paginateData = $users->paginate($perPage);
            $collection = new CompanyUserCollection($paginateData->getCollection());
            $metaInfo = $paginateData->toArray();
            unset($metaInfo['data']);

            return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CompanyUserValidator $companyUserValidator)
    {
        $user = $request->user();

        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$companyUserValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $companyUserValidator->getErrors()[0],
                    "messages" => $companyUserValidator->getErrors(),
                ], 422);
            }

            $role = $input['role'] ?? null;
            $permissions = $input['permissions'] ?? null;

            $companyUser = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password'])
            ]);

            $companyUser->email_verified_at = Carbon::now(); // as of now the email is verified after user is created.
            $companyUser->company_id = $user->company->id;
            $companyUser->user_role = self::COMPANY_USER; // set the new created user as company user.
            $companyUser->save();

            // Todo check permission against the role. Give error in case no permission there for selected role.
            if ($role) {
                $companyUser->assignRole($role);
            }

            if ($permissions) {
                $companyUser->givePermissionTo(array_filter($permissions));
            }

            DB::commit();

            return $this->successResponse([
                'data' => new CompanyUserResource($companyUser),
                'message' => trans('messages.company_user_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $ncf
     * @return \Illuminate\Http\Response
     */
    public function show(User $companyUser)
    {
        return $this->successResponse(['data' => new CompanyUserResource($companyUser)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $companyUser)
    {
        $user = $request->user();
        $companyUserValidator = new CompanyUserValidator('update', $companyUser);

        try {
            DB::beginTransaction();
            $input = $request->all();
            $passwordChange = false;

            if (!$companyUserValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $companyUserValidator->getErrors()[0],
                    "messages" => $companyUserValidator->getErrors(),
                ], 422);
            }

            $companyUser->update([
                'name' => $input['name'],
                'email' => $input['email']
            ]);

            if (isset($input['password']) && $input['password']) {
                $companyUser->password = Hash::make($input['password']);
                $passwordChange = true;
            }

            $companyUser->save();

            // Todo check permission against the role. Give error in case no permission there for selected role.
            $role = $input['role'] ?? [];            
            $companyUser->syncRoles($input['role']);

            $permissions = $input['permissions'] ?? [];
            $companyUser->syncPermissions(array_filter($permissions));

            if ($passwordChange) {
                // todo:- logout the company user after password change. 
            }

            DB::commit();

            return $this->successResponse([
                'data' => new CompanyUserResource($companyUser),
                'message' => trans('messages.company_user_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $companyUser)
    {
        try {
            $companyUser->permissions()->detach();
            $companyUser->roles()->detach();
            $companyUser->delete();

            return $this->successResponse([
                'message' => trans('messages.company_user_deleted')
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles(Request $request)
    {
        try {

            $user = $request->user();
            $roles = $user->company->roles;

            $perPage = $request->input('per_page') ?? 10;
            $page = $request->input('page') ?? 1;

            $paginateData = $roles->paginate($perPage);
            $collection = RoleResource::collection($paginateData->getCollection());
            $metaInfo = $paginateData->toArray();
            unset($metaInfo['data']);

            return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRole(Request $request, Role $role)
    {
        if (!$request->user()->can('view', [Role::class, $role] )) {
            return $this->failResponse([
                "message" => trans('messages.account_access')
            ], 403);
        }

        return $this->successResponse(['data' => new RoleResource($role)]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignPermissionsToRole(Request $request, Role $role)
    {
        $user = $request->user();

        if (!$request->user()->can('update', [Role::class, $role] )) {
            return $this->failResponse([
                "message" => trans('messages.account_access')
            ], 403);
        }

        try {
            DB::beginTransaction();
            $input = $request->all();

            $validator = Validator::make($request->all(), [
                'permissions' => 'required|array',
                "permissions.*"  => 'required|string|distinct|in:'.Cache::get('permissions')->implode('id', ','),
            ]);

            if ($validator->fails()) {
                return $this->failResponse([
                    "message" => $validator->errors()->all()[0],
                    "messages" => $validator->errors()->all(),
                ], 422);
            }

            $role->syncPermissions($input['permissions']);

            DB::commit();

            return $this->successResponse([
                'data' => new RoleResource($role),
                'message' => trans('messages.role_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }
}