<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\InventoryAdjustmentValidator;

use App\Http\Resources\InventoryAdjustment as InventoryAdjustmentResource;
use App\Http\Resources\InventoryAdjustmentCollection;

use App\InventoryAdjustment;
use App\Item;
use App\Inventory;

use DB;
use Auth;
use Config;
use File;

use App\Traits\ApiResponse;

class InventoryAdjustmentController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(InventoryAdjustment::class, 'inventory_adjustment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $user = $request->user();
        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        $inventoryAdjustments = $user->company->inventoryAdjustments;

        if ($term) {
            $inventoryAdjustments = $inventoryAdjustments->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->total), strtolower($term)) === false 
                && mb_strpos(strtolower($element->adjustment_date), strtolower($term)) === false
                && mb_strpos(strtolower($element->observations), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $inventoryAdjustments->paginate($perPage);
        // prePrint($paginateData->getCollection());
        $collection = new InventoryAdjustmentCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, InventoryAdjustmentValidator $inventoryAdjustmentValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            // $input = json_decode($allData['data'], true);
            
            if (!$inventoryAdjustmentValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $inventoryAdjustmentValidator->getErrors()[0],
                    "messages" => $inventoryAdjustmentValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $inventoryAdjustmentItems = collect($input['adjustment_items'])->filter(function($row) {
                return $row['qty'] && $row['item_id'] && $row['unit_price'] && $row['unit_price'] > 0;
                })->map(function($item) {
                    $item['inventory_id'] = $item['item_id'];
                    $item['quantity'] = $item['qty'];
                    $item['inventory_adjustment_type'] = $item['adj_type'];
                    $item['unit_cost'] = $item['unit_price'];
                    unset($item['current_qty']);
                    unset($item['final_qty']);
                    unset($item['adj_total']);
                    unset($item['adj_type']);
                    unset($item['item_id']);
                    unset($item['qty']);
                    unset($item['unit_price']);
                    return $item;
            })->unique('inventory_id')->values()->toArray(); 

            if (!$inventoryAdjustmentItems) {
                return $this->failResponse([
                    "message" => trans('messages.inventory_adjustment_without_items'),
                    "messages" => [trans('messages.inventory_adjustment_without_items')],
                ], 400);
            }

            $inventoryAdjustment = $user->inventoryAdjustments()->create([
                'company_id' => $user->company->id,
                'adjustment_date' => $input['adjustment_date'],
                'observations' => $input['observations'] ?? null
            ]);

            $inventoryAdjustment->items()->createMany($inventoryAdjustmentItems);

            foreach ($inventoryAdjustment->items as $item) {
                $item->inventory->quantity = $item->inventory->quantity + ($item->inventory_adjustment_type == 'increase' ? $item->quantity : (-$item->quantity));
                $item->inventory->save();
            }

            /* upload documents */
            $documents = $input['documents'] ?? [];

            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'adjustment-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $inventoryAdjustment->documents()->createMany($documents_data);
            }

            DB::commit();

            return $this->successResponse([
                'data' => new InventoryAdjustmentResource($inventoryAdjustment),
                'message' => trans('messages.inventory_adjustment_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryAdjustment $inventoryAdjustment)
    {
        return $this->successResponse(['data' => new InventoryAdjustmentResource($inventoryAdjustment)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, inventoryAdjustment $inventoryAdjustment)
    {
        $inventoryAdjustmentValidator = new InventoryAdjustmentValidator('update');
        
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$inventoryAdjustmentValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $inventoryAdjustmentValidator->getErrors()[0],
                    "messages" => $inventoryAdjustmentValidator->getErrors(),
                ], 422);
            }
            
            $user = $request->user();

            $inventoryAdjustmentItems = collect($input['adjustment_items'])->filter(function($row) {
                    return $row['qty'] && $row['item_id'] && $row['unit_price'] && $row['unit_price'] > 0;
                })->map(function($item) {
                    $item['inventory_id'] = $item['item_id'];
                    $item['quantity'] = $item['qty'];
                    $item['inventory_adjustment_type'] = $item['adj_type'];
                    $item['unit_cost'] = $item['unit_price'];
                    unset($item['current_qty']);
                    unset($item['final_qty']);
                    unset($item['adj_total']);
                    unset($item['adj_type']);
                    unset($item['item_id']);
                    unset($item['qty']);
                    unset($item['unit_price']);
                    return $item;
            })->values(); 


            if ($inventoryAdjustmentItems->isEmpty()) {
                return $this->failResponse([
                    "message" => trans('messages.inventory_adjustment_without_items'),
                    "messages" => [trans('messages.inventory_adjustment_without_items')],
                ], 422);
            }


            $updateInventoryItems = $inventoryAdjustmentItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertInventoryItems = $inventoryAdjustmentItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteInventoryItemsExcept = $updateInventoryItems->pluck('id')->toArray();
            
             $inventoryAdjustment->update([
                'adjustment_date' => $input['adjustment_date'],
                'observations' => $input['observations'] ?? null
            ]);

            if ($deleteInventoryItemsExcept) {
                $deleteAdjustmentItems =  $inventoryAdjustment->items()->whereNotIn('id', $deleteInventoryItemsExcept)->get();

                // Update inventory quantity on delete items //
                foreach ($deleteAdjustmentItems as $single_item) {
                    if ($single_item->inventory_adjustment_type == 'increase') {
                        $single_item->inventory->decrement('quantity', $single_item->quantity);
                    } else{
                        $single_item->inventory->increment('quantity', $single_item->quantity);
                    }
                }

                $inventoryAdjustment->items()->whereNotIn('id', $deleteInventoryItemsExcept)->delete();
            }

            if ($insertInventoryItems) {
                $inventoryAdjustment->items()->createMany($insertInventoryItems);

                // Update inventory quantity on add new items //
                foreach ($inventoryAdjustment->items as $item) {
                    $item->inventory->quantity = $item->inventory->quantity + ($item->inventory_adjustment_type == 'increase' ? $item->quantity : (-$item->quantity));
                    $item->inventory->save();
                }

            }

            // Manage inventory quantity //
            $updateInventoryQuantityItems = collect($input['adjustment_items'])->map(function($adjustment_item) {
                // $inventory = Inventory::findOrFail($adjustment_item['item_id']);
                
                $adjustment_item['id'] = $adjustment_item['item_id'];
                $adjustment_item['quantity'] = $adjustment_item['adj_type'] == "increase" ? $adjustment_item['current_qty'] + $adjustment_item['qty']  : $adjustment_item['current_qty'] - $adjustment_item['qty'];
                return $adjustment_item;
            })->values();

            if ($updateInventoryItems) {
                foreach ($updateInventoryItems as $updateInventoryItems) {
                    $updatedData = $updateInventoryItems;
                    unset($updatedData['id']);
                    Item::findOrFail($updateInventoryItems['id'])->update($updatedData);
                }
            }

            if ($updateInventoryQuantityItems) {
                // Update Current Quantity //
                foreach ($updateInventoryQuantityItems as $updateInventoryQuantityItems) {
                    $updatedData = $updateInventoryQuantityItems;
                    Inventory::findOrFail($updateInventoryQuantityItems['id'])->update($updatedData);
                }
            }

            /* update documents start */
            $documents = $input['documents'] ?? [];
            $deleted_documents_ids = $input['deleted_documents_ids'] ?? [];

            if ($deleted_documents_ids) {
                $inventoryAdjustment->documents()->whereIn('id', $deleted_documents_ids)->delete();
            }
        
            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'adjustment-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $inventoryAdjustment->documents()->createMany($documents_data);
            }
            /* update documents end */

            DB::commit();

            return $this->successResponse([
                'data' => new InventoryAdjustmentResource($inventoryAdjustment),
                'message' => trans('messages.inventory_adjustment_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, inventoryAdjustment $inventoryAdjustment)
    {
        try {
            // Update Inventory items while delete //
            $inventoryAdjustmentItems = $inventoryAdjustment->items;

            foreach ($inventoryAdjustment->items as $item) {
                $item->inventory->quantity = $item->inventory->quantity + ($item->inventory_adjustment_type == 'increase' ? (-$item->quantity) : ($item->quantity));
                $item->inventory->save();
            }
            $inventoryAdjustment->delete();
            return $this->successResponse([
                'message' => trans('messages.inventory_adjustment_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}