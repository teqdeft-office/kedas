<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\ItemCategoryValidator;

use App\Http\Resources\ItemCategory as ItemCategoryResource;
use App\Http\Resources\ItemCategoryCollection;

use App\ItemCategory;

use DB;
use Auth;
use Config;
use Image;

use App\Traits\ApiResponse;

class ItemCategoryController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(ItemCategory::class, 'item_category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        $itemCategories = $user->company->itemCategories;

        if ($term) {
            $itemCategories = $itemCategories->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $itemCategories->paginate($perPage);
        $collection = new ItemCategoryCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ItemCategoryValidator $itemCategoryValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$itemCategoryValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $itemCategoryValidator->getErrors()[0],
                    "messages" => $itemCategoryValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'description' => $input['description'],
                'user_id' => $user->id
            ];            

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/item-categories");
                $path = $dir."/".$filename;
                
                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                $itemImgInstance->orientate()->save($path);

                $data['image'] = $filename;
                
            }

            $itemCategory = $user->company->itemCategories()->create($data);

            DB::commit();

            return $this->successResponse([
                'data' => new ItemCategoryResource($itemCategory),
                'message' => trans('messages.item_category_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ItemCategory $itemCategory)
    {
        return $this->successResponse(['data' => new ItemCategoryResource($itemCategory)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemCategory $itemCategory)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $itemCategoryValidator = new ItemCategoryValidator('update', $itemCategory);
            
            if (!$itemCategoryValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $itemCategoryValidator->getErrors()[0],
                    "messages" => $itemCategoryValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'description' => $input['description']
            ];

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/item-categories");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);
                
                $data['image'] = $filename;
            }

            $itemCategory->update($data);

            DB::commit();

            return $this->successResponse([
                'data' => new ItemCategoryResource($itemCategory),
                'message' => trans('messages.item_category_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ItemCategory $itemCategory)
    {
        try {
            $itemCategory->delete();
            return $this->successResponse([
                'message' => trans('messages.item_category_deleted')
            ]);

        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}