<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\Accounting\HeadValidator;

use App\UserAccountingHead;
use Illuminate\Support\Str;

use DB;
use Lang;
use Cache;
use App;

use App\Traits\ApiResponse;

class UserAccountingHeadController extends Controller
{
    use ApiResponse;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(UserAccountingHead::class, 'chart');
    }

    /**
     * Get the accounting charts
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$user = $request->user();
        $screen = $request->get('screen') ?? null;

        $all_accounts = $assets_accounts = $income_accounts = $expense_accounts = collect([]);
        $collection = collect([]);
    	$accounting_heads = $user->company->accountingHeads;

        $companyAccountingHeads = $user->company->accountingHeads()->whereIn('code', [self::TAXINFAVOURCODE, self::TAXTOPAYCODE, self::INVENTORYCODE, self::SALESOFPRODUCTINCOMECODE, self::SUPPLIESANDMATERIALSCODE, self::INCOMECODE, self::EXPENSESCODE, self::OTHERCURRENTASSETSCODE])->pluck('id', 'code')->toArray();

        switch ($screen) {
            case 'tax':
                $taxes_to_pay_account = UserAccountingHead::descendantsAndSelf($companyAccountingHeads[self::TAXTOPAYCODE])->toTree();
                $taxes_in_favour_account = UserAccountingHead::descendantsAndSelf($companyAccountingHeads[self::TAXINFAVOURCODE])->toTree();

                $collection['sales_accounts'] = $taxes_to_pay_account;
                $collection['purchases_accounts'] = $taxes_in_favour_account;
                break;

            case 'services':
                $income_accounts = UserAccountingHead::descendantsOf($companyAccountingHeads[self::INCOMECODE])->toTree($companyAccountingHeads[self::INCOMECODE]);
                $expenses_accounts = UserAccountingHead::descendantsOf($companyAccountingHeads[self::EXPENSESCODE])->toTree($companyAccountingHeads[self::EXPENSESCODE]);


                $collection['income_accounts'] = $income_accounts;
                $collection['expenses_accounts'] = $expenses_accounts;
                break;

            case 'inventory':
                $assets_accounts = UserAccountingHead::descendantsAndSelf($companyAccountingHeads[self::INVENTORYCODE])->toTree();
                $income_accounts = UserAccountingHead::descendantsAndSelf($companyAccountingHeads[self::SALESOFPRODUCTINCOMECODE])->toTree();
                $expenses_accounts = UserAccountingHead::descendantsAndSelf($companyAccountingHeads[self::SUPPLIESANDMATERIALSCODE])->toTree();

                $collection['assets_accounts'] = $assets_accounts;
                $collection['income_accounts'] = $income_accounts;
                $collection['expenses_accounts'] = $expenses_accounts;
                break;
            
            default:
                $collection['all_accounts'] = $accounting_heads->toTree();
                break;
        }


        return $this->successResponse(['data' => $collection]);
    }
}
