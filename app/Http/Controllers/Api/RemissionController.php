<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\RemissionValidator;

use App\Http\Resources\Remission as RemissionResource;
use App\Http\Resources\RemissionCollection;

use App\Remission;
use App\Item;

use Illuminate\Support\Facades\Mail;
use App\Mail\RemissionShared;

use DB;
use Auth;
use Config;
use Image;
use PDF;
use Cache;

use App\Traits\ApiResponse;

class RemissionController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Remission::class, 'remission');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $remissions = $user->company->remissions;

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $remissions->each(function ($remission, $key) {
            $remission->setAppends([]);
        });

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $remissions = $remissions->reject(function ($element) use ($term) {
                // search according to the data-tables.
                return 
                    mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['total']), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->internal_number)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->notes), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $remissions->paginate($perPage);
        $collection = new RemissionCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RemissionValidator $remissionValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$remissionValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $remissionValidator->getErrors()[0],
                    "messages" => $remissionValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $remissionitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
            })->values()->toArray();

            if (!$remissionitems) {
                return $this->failResponse([
                    "message" => trans('messages.remission_without_items'),
                    "messages" => [trans('messages.remission_without_items')],
                ], 400);
            }

            $remission = $contact->remissions()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['remission_start_date'],
                'expiration_date' => $input['remission_expiration_date'],
                'notes' => $input['notes'] ?? null,
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $remission->items()->createMany($remissionitems);
            
            /* upload logo and signature */
            if (isset($input['logo']) && $input['logo']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }
                $user->company->signature = $filename;
                $user->company->save();
            }

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            return $this->successResponse([
                'data' => new RemissionResource($remission),
                'message' => trans('messages.remission_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function show(Remission $remission)
    {
        return $this->successResponse(['data' => new RemissionResource($remission)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remission $remission)
    {
        if ($remission->invoice) {
            return $this->failResponse([
                "message" => trans('messages.edit_prohibited'),
                "messages" => [trans('messages.edit_prohibited')],
            ], 403);
        }

        $remissionValidator = new RemissionValidator('update');
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$remissionValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $remissionValidator->getErrors()[0],
                    "messages" => $remissionValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $remissionitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
                })->values();
            $updateRemissionItems = $remissionitems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertRemissionItems = $remissionitems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteRemissionItemsExcept = $updateRemissionItems->pluck('id')->toArray();

            if (!$remissionitems) {
                $request->session()->flash('error', trans('messages.remission_without_items'));
                return back()
                    ->withInput();
            }

            $remission->contact()->associate($contact);

            $remission->update([
                'start_date' => $input['remission_start_date'],
                'expiration_date' => $input['remission_expiration_date'],
                'notes' => $input['notes'] ?? null
            ]);

            if ($deleteRemissionItemsExcept) {
                $remission->items()->whereNotIn('id', $deleteRemissionItemsExcept)->delete();
            }

            if ($insertRemissionItems) {
                $remission->items()->createMany($insertRemissionItems);
            }

            if ($updateRemissionItems) {
                foreach ($updateRemissionItems as $updateRemissionItem) {
                    $updatedDate = $updateRemissionItem;
                    unset($updatedDate['id']);
                    Item::findOrFail($updateRemissionItem['id'])->update($updatedDate);
                }
            }

            if (isset($input['logo']) && $input['logo'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }

                $user->company->signature = $filename;
                $user->company->save();
            }

            DB::commit();

            return $this->successResponse([
                'data' => new RemissionResource($remission),
                'message' => trans('messages.remission_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Remission $remission)
    {
        if ($remission->invoice) {
            return $this->failResponse([
                "message" => trans('messages.delete_prohibited'),
                "messages" => [trans('messages.delete_prohibited')],
            ], 403);
        }
        
        try {
            $remission->delete();
            return $this->successResponse([
                'message' => trans('messages.remission_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}