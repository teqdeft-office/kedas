<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Events\ProfileCompleted;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;

use App\Validators\User\RegisterValidator;
use App\Validators\User\LoginValidator;

use DB;
use GuzzleHttp;

use App\User;
use App\Traits\ApiResponse;

class AuthController extends Controller
{
    use ApiResponse, AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['logout', 'refreshToken']);
    }

    public function register(Request $request, RegisterValidator $registerValidator) {

        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$registerValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $registerValidator->getErrors()[0],
                    "messages" => $registerValidator->getErrors()
                ], 422);
            }

            $input['password'] = Hash::make($input['password']);
            $user = User::create($input);
            $user->save();

            DB::commit();

            $user->sendEmailVerificationNotification();

            return $this->successResponse([
                "message" => trans('messages.reverify_success')
            ]);

        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    public function login(Request $request, LoginValidator $loginValidator) {

        try {
            $input = $request->all();

            if (!$loginValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $loginValidator->getErrors()[0],
                    "messages" => $loginValidator->getErrors()
                ], 422);
            }

            $user = User::whereEmail($input['email'])->first();

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if (method_exists($this, 'hasTooManyLoginAttempts') &&
                $this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return $this->sendLockoutResponse($request);
            }

            if ($user && $user->user_role == self::ADMIN) {

                if (Hash::check($request->password, $user->password, [])) {

                    $token = $this->getToken($request);
                    $data = json_decode((string) $token->getBody(), true);
                    $data['is_profile_completed'] = (bool)$user->company;
                    $data['is_account_verified'] = $user->hasVerifiedEmail();
                        
                    $response = [
                        "message" => trans('messages.login_success'),
                        "data" => $data
                    ];

                    // event(new ProfileCompleted($user));

                    $this->clearLoginAttempts($request);

                    return $this->successResponse($response);

                } else {
                    $response = ['message'=> trans('auth.failed')];
                    return $this->failResponse($response, 400);
                }

            } else {
                $response = ['message'=> trans('auth.failed')];
                return $this->failResponse($response, 400);
            }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    public function logout(Request $request) {

        try {
            $token = $request->user()->token();
            $token->revoke();
            $response = [
                'message'=> trans('messages.logout_success')
            ];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    public function forgotPassword(Request $request) {

        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                'email' => "required|string|email|max:255"
            ]);
            
            if ($validator->fails()) {
                return $this->failResponse([
                    "message" => $validator->errors()->all()[0],
                ], 422);
            }

            $user = User::whereEmail($input['email'])->first();

            if ($user) {
                $response = $this->broker()->sendResetLink(
                    $this->credentials($request)
                );

                if ($response == Password::RESET_LINK_SENT) {
                    return $this->successResponse(['message'=> trans($response)]);
                } else {
                    return $this->failResponse(['message'=> trans($response)], 429);
                }
            } else {
                $response = ['message'=> trans('passwords.user')];
                return $this->failResponse($response, 400);
            }
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * User login token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function getToken($request) {

        try {
            $http = new GuzzleHttp\Client;
            $data = $http->post(env('APP_URL', 'http://localhost/kedas').'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('CLIENT_ID'),
                    'client_secret' => env('CLIENT_SECRET'),
                    'username' => $request->email,
                    'password' => $request->password,
                    // 'scope' => '*',
                ],
            ]);
            return $data;
        } catch(\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * User login token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function refreshToken(Request $request){

        try {
            $input = $request->all();
            if ($input['refresh_token']) {
                $http = new GuzzleHttp\Client;
                $data = $http->post(env('APP_URL', 'http://localhost/kedas').'/oauth/token', [
                    'form_params' => [
                        'grant_type' => 'refresh_token',
                        'refresh_token' => $input['refresh_token'],
                        'client_id' => env('CLIENT_ID'),
                        'client_secret' => env('CLIENT_SECRET'),
                    ],
                ]);
                $token = json_decode((string) $data->getBody(), true);
                return $this->successResponse([
                    "message" => trans('messages.token_generated'),
                    "data" => $token,
                ]);
            } else {
                return $this->failResponse([
                    "message" => trans("messages.something_went_wrong"),
                ], 500);
            }
        } catch( \Exception $e){
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    public function resendVerifyEmail(Request $request) {
        try {

            $user = $request->user();
            $user->sendEmailVerificationNotification();

            return $this->successResponse([
                "message" => trans('messages.verification_link_sent'),
            ]);
        } catch( \Exception $e){
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }
}