<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\ClientValidator;

use App\Http\Resources\Client as ClientResource;
use App\Http\Resources\ClientCollection;

use App\Client;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class ClientController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Client::class, 'client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $clients = $user->company->clients;

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $clients = $clients->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->email), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->phone), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->mobile), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->company_name), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $clients->paginate($perPage);
        $collection = new ClientCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ClientValidator $clientValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$clientValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $clientValidator->getErrors()[0],
                    "messages" => $clientValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'company_name' => $input['company_name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'membership_id' => $input['membership_id'] ?? null,
                'user_id' => $user->id
            ];

            $client = $user->company->clients()->create($data);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];
                $address = $client->address()->create($addressData);
            }

            DB::commit();

            return $this->successResponse([
                'data' => new ClientResource($client),
                'message' => trans('messages.client_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return $this->successResponse(['data' => new ClientResource($client)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $clientValidator = new ClientValidator('update', $client);

            if (!$clientValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $clientValidator->getErrors()[0],
                    "messages" => $clientValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'company_name' => $input['company_name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'membership_id' => $input['membership_id'] ?? null,
            ];

            $client->update($data);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];

                $client->address()->updateOrCreate([], $addressData);
            }

            DB::commit();

            return $this->successResponse([
                'data' => new ClientResource($client),
                'message' => trans('messages.client_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Client $client)
    {
        try {
            if ($client->invoices->isEmpty() && $client->transactions->isEmpty() && $client->creditNotes->isEmpty() && $client->estimates->isEmpty() && $client->debitNotes->isEmpty() && $client->purchaseOrders->isEmpty()) {
                $client->forceDelete();
                return $this->successResponse([
                    'message' => trans('messages.client_deleted')
                ]);
            } else {
                return $this->failResponse([
                    'message' => trans('messages.delete_prohibited')
                ], 403);
            }
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}