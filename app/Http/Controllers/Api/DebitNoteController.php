<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\DebitNoteValidator;

use App\Http\Resources\DebitNote as DebitNoteResource;
use App\Http\Resources\DebitNoteCollection;

use App\DebitNote;
use App\Item;
use App\ConceptItem;
use App\Inventory;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class DebitNoteController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(DebitNote::class, 'debit_note');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $debitNotes = $user->company->debitNotes;

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $debitNotes->each(function ($debitNote, $key) {
            $debitNote->setAppends([]);
        });

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $debitNotes = $debitNotes->reject(function ($element) use ($term) {
                // search according to the data-tables.
                return 
                    mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['total']), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['remaining_amount']), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->notes), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $debitNotes->paginate($perPage);
        $collection = new DebitNoteCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DebitNoteValidator $debitNoteValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$debitNoteValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $debitNoteValidator->getErrors()[0],
                    "messages" => $debitNoteValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $debitNoteItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($debitNoteItems->isEmpty()) {
                return $this->failResponse([
                    "message" => trans('messages.debit_notes_without_items'),
                    "messages" => [trans('messages.debit_notes_without_items')],
                ], 400);
            }

            $debitNote = $contact->debitNotes()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['debit_note_date'],
                'notes' => @$input['notes'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $debitNote->conceptItems()->createMany($conceptItems);

            $debitNote->items()->createMany($inventoryItems);

            // decrease the quantity of each item's inventory while creating the debit note.
            $debitNote->items->each(function($item) {
                $item->inventory->quantity = $item->inventory->quantity - $item->quantity;
                $item->inventory->save();
            });

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            /* upload documents */
            $documents = $input['documents'] ?? [];

            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'debit-note-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $debitNote->documents()->createMany($documents_data);
            }

            DB::commit();

            return $this->successResponse([
                'data' => new DebitNoteResource($debitNote),
                'message' => trans('messages.debit_note_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function show(DebitNote $debitNote)
    {
        return $this->successResponse(['data' => new DebitNoteResource($debitNote)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DebitNote $debitNote)
    {
        $debitNoteValidator = new DebitNoteValidator('update');
        try {
            DB::beginTransaction();
            $input = $request->all();
            // prePrint($input);

            if (!$debitNoteValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $debitNoteValidator->getErrors()[0],
                    "messages" => $debitNoteValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::CONCEPT) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::INVENTORY) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $debitNoteItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($debitNoteItems->isEmpty()) {
                return $this->failResponse([
                    "message" => trans('messages.debit_notes_without_items'),
                    "messages" => [trans('messages.debit_notes_without_items')],
                ], 400);
            }

            // The following is for debitNote's items

            // map inventory id as key and quantity as value from submit form value.
            $debit_note_inventory_quantity = $inventoryItems->groupBy('inventory_id')->map(function($it) {
                return $it->sum('quantity');
            });
            // map inventory id as key and quantity as value from existing debitNote's item value.
            $existing_inventory_quantity = $debitNote->items->groupBy('inventory_id')->map(function($it) {
                return $it->sum('quantity');
            });

            // map inventory id as key and quantity as value as new quantity for eack inventory.
            $inventory_quantity = $debit_note_inventory_quantity->map(function($iiq, $inventoryId) use($existing_inventory_quantity){
                return (($existing_inventory_quantity[$inventoryId] ?? 0) - $iiq);
            })->union($existing_inventory_quantity->diffKeys($debit_note_inventory_quantity));

            $updateDebitNoteItems = $inventoryItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertDebitNoteItems = $inventoryItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteDebitNoteItemsExcept = $updateDebitNoteItems->pluck('id')->toArray();

            // The following is for debitNote's concept items
            $updateDebitNoteConceptItems = $conceptItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertDebitNoteConceptItems = $conceptItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteDebitNoteConceptItemsExcept = $updateDebitNoteConceptItems->pluck('id')->toArray();

            $debitNote->contact()->associate($contact);

            // Update basic information.
            $debitNote->update([
                'start_date' => $input['debit_note_date'],
                'notes' => @$input['notes']
            ]);

            if ($deleteDebitNoteItemsExcept) {
                $debitNote->items()->whereNotIn('id', $deleteDebitNoteItemsExcept)->delete();
            }

            if ($insertDebitNoteItems) {
                $debitNote->items()->createMany($insertDebitNoteItems);
            }

            if ($updateDebitNoteItems) {
                foreach ($updateDebitNoteItems as $updateDebitNoteItem) {
                    $updatedData = $updateDebitNoteItem;
                    unset($updatedData['id']);
                    Item::findOrFail($updateDebitNoteItem['id'])->update($updatedData);
                }
            }


            
            $debit_note_total = $input['debit_note_total_hidden'] ?? $debitNote->calculation['total'];
            // associate debit note to invoices.
            $payments = $input['payments'] ?? [];
            $paymentItems = collect($payments)->filter(function($row) {
             return $row['invoice_id'] && (isset($row['amount']) && $row['amount'] > 0);
            })->values();
 
            // remove the association of debit note with other invoices. 
            if ($paymentItems->isEmpty()) {
                $debitNote->invoices()->detach();
            } else {
                $pluckedInvoices = $debitNote->contact->invoices->pluck('calculation', 'id')->map(function($value, $key) {
                 return $value['pendingAmountWithoutDebitNote'];
                })->toArray();

                $amountCorrect = $paymentItems->every(function ($value, $key) use ($pluckedInvoices) {
                    return $value['amount'] <= $pluckedInvoices[$value['invoice_id']];
                });

                if (!$amountCorrect) {
                    return $this->failResponse([
                        "message" => trans('messages.debit_note_with_incorrect_amount'),
                        "messages" => [trans('messages.debit_note_with_incorrect_amount')],
                    ], 400);
                }

                $total_amount = $paymentItems->sum('amount');

                if ($total_amount > $debit_note_total) {
                    return $this->failResponse([
                        "message" => trans('messages.debit_note_with_incorrect_amount'),
                        "messages" => [trans('messages.debit_note_with_incorrect_amount')],
                    ], 400);
                }
                $debitNote->invoices()->sync($paymentItems->keyBy('invoice_id')->toArray());
            }

            if ($deleteDebitNoteConceptItemsExcept) {
                $debitNote->conceptItems()->whereNotIn('id', $deleteDebitNoteConceptItemsExcept)->delete();
            }

            if ($insertDebitNoteConceptItems) {
                $debitNote->conceptItems()->createMany($insertDebitNoteConceptItems);
            }

            if ($updateDebitNoteConceptItems) {
                foreach ($updateDebitNoteConceptItems as $updateDebitNoteConceptItem) {
                    $updatedData = $updateDebitNoteConceptItem;
                    unset($updatedData['id']);
                    ConceptItem::findOrFail($updateDebitNoteConceptItem['id'])->update($updatedData);
                }
            }

            if ($inventoryItems->isEmpty()) {
                $debitNote->items()->delete();
            }

            if ($conceptItems->isEmpty()) {
                $debitNote->conceptItems()->delete();
            }

            // update the quantity of each item's inventory according to mapped data.
            $inventory_quantity->each(function($quantity, $inventoryId) {
                $inventory = Inventory::findOrFail($inventoryId);
                $inventory->quantity = $inventory->quantity + $quantity;
                $inventory->save();
            });

            /* update documents start */
            $documents = $input['documents'] ?? [];
            $deleted_documents_ids = $input['deleted_documents_ids'] ?? [];

            if ($deleted_documents_ids) {
                $debitNote->documents()->whereIn('id', $deleted_documents_ids)->delete();
            }
        
            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'debit-note-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $debitNote->documents()->createMany($documents_data);
            }
            /* update documents end */

            DB::commit();

            return $this->successResponse([
                'data' => new DebitNoteResource($debitNote),
                'message' => trans('messages.debit_note_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DebitNote  $debitNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DebitNote $debitNote)
    {
        try {
            $debitNote->items()->each(function($debitNoteItem) {
                $debitNoteItem->inventory->quantity = $debitNoteItem->inventory->quantity + $debitNoteItem->quantity;
                $debitNoteItem->inventory->save();
            });
            $debitNote->delete();
            return $this->successResponse([
                'message' => trans('messages.debit_note_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}