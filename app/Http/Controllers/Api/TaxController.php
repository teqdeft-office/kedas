<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\TaxValidator;

use App\Http\Resources\Tax as TaxResource;
use App\Http\Resources\TaxCollection;

use App\Tax;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class TaxController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Tax::class, 'tax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $taxes = $user->company->taxes;

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $taxes = $taxes->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false && mb_strpos(strtolower($element->percentage), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $taxes->paginate($perPage);
        $collection = new TaxCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, TaxValidator $taxValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$taxValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $taxValidator->getErrors()[0],
                    "messages" => $taxValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'percentage' => $input['percentage'],
                'description' => $input['description'],
                'sales_account' => $input['sales_account'],
                'purchases_account' => $input['purchases_account'],
                'user_id' => $user->id
            ];

            $tax = $user->company->taxes()->create($data);

            DB::commit();

            return $this->successResponse([
                'data' => new TaxResource($tax),
                'message' => trans('messages.tax_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function show(Tax $tax)
    {
        return $this->successResponse(['data' => new TaxResource($tax)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tax $tax, TaxValidator $taxValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $resource_associated = $tax->items->isNotEmpty() || $tax->conceptitems->isNotEmpty();

            if (!$taxValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $taxValidator->getErrors()[0],
                    "messages" => $taxValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'percentage' => $resource_associated ? $tax->percentage : $input['percentage'],
                'description' => $input['description'],
                'sales_account' => $input['sales_account'],
                'purchases_account' => $input['purchases_account'],
            ];

            $tax->update($data);

            DB::commit();

            return $this->successResponse([
                'data' => new TaxResource($tax),
                'message' => trans('messages.tax_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tax $tax)
    {
        try {
            if ($tax->items->isEmpty() && $tax->conceptitems->isEmpty()) {
                $tax->delete();
                return $this->successResponse([
                    'message' => trans('messages.tax_deleted')
                ]);
            } else {
                return $this->failResponse([
                    'message' => trans('messages.delete_prohibited')
                ], 403);
            }
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}