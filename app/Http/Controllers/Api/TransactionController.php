<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\TransactionValidator;

use App\Http\Resources\Transaction as TransactionResource;
use App\Http\Resources\TransactionCollection;

use App\Transaction;
use App\Supplier;
use App\Client;
use App\Item;
use App\Invoice;
use App\ConceptItem;
use App\Tax;

use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionCreated;
use App\Events\PaymentCreatedOrUpdated;

use App\Services\TransactionService;
use App\Exceptions\ServiceException;

use DB;
use Auth;
use Config;
use Image;
use File;
use Cache;
use PDF;

use App\Traits\ApiResponse;

class TransactionController extends Controller
{
    use ApiResponse;

    protected $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->authorizeResource(Transaction::class, 'transaction');
        $this->transactionService = $transactionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type)
    {
        $user = $request->user();
        $transactions = $user->company->transactions->where('type', $type)->values();

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $transactions = $transactions->reject(function ($element) use ($term) {
                // search according to the data-tables.
                $searchCondition = 
                    mb_strpos(strtolower($element->receipt_number), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->voucher_number), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->detail_line), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->amount), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->bank_account), strtolower($term)) === false;

                if ($element->contact) {
                    $searchCondition = $searchCondition && mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false;
                }                   

                $searchCondition = $searchCondition && mb_strpos(strtolower($element->frequency), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->frequency_type), strtolower($term)) === false &&

                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->end_date)), strtolower($term)) === false &&

                    mb_strpos(strtolower($element->annotation), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->observation), strtolower($term)) === false;
                return $searchCondition;

            })->values();
        }

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $transactions->each(function ($transaction, $key) {
            $transaction->setAppends([]);
            $transaction->unsetRelation('invoices')->unsetRelation('conceptItems');
        });

        $paginateData = $transactions->paginate($perPage);
        $collection = new TransactionCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        $transactionValidator = new TransactionValidator($type);
        $input = $request->all();

        if (!$transactionValidator->with($input)->passes()) {
            return $this->failResponse([
                "message" => $transactionValidator->getErrors()[0],
                "messages" => $transactionValidator->getErrors(),
            ], 422);
        }

        try {
            DB::beginTransaction();
            $transaction = $this->transactionService->store($request, $type);
            DB::commit();
            return $this->successResponse([
                'data' => new TransactionResource($transaction),
                'message' => trans('messages.transaction_created')
            ]);
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return $this->successResponse(['data' => new TransactionResource($transaction)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $type = $transaction->type;
        $input = $request->all();

        $transactionValidator = new TransactionValidator($transaction->type, 'update');
        if (!$transactionValidator->with($input)->passes()) {
            return $this->failResponse([
                "message" => $transactionValidator->getErrors()[0],
                "messages" => $transactionValidator->getErrors(),
            ], 422);
        }

        try {
            DB::beginTransaction();
            $transaction = $this->transactionService->update($request, $transaction);
            
            // prePrint(new TransactionResource($transaction));
            DB::commit();
            return $this->successResponse([
                'data' => new TransactionResource($transaction),
                'message' => trans('messages.transaction_updated')
            ]);
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Transaction $transaction)
    {
        try {
            DB::beginTransaction();
            $this->transactionService->destroy($request, $transaction);
            DB::commit();
            return $this->successResponse([
                'message' => trans('messages.transaction_deleted')
            ]);
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }
}
