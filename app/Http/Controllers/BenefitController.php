<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Benefit;
use App\Validators\BenefitValidator;

use Config;

class BenefitController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Benefit::class, 'benefit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $benefits = $user->company->benefits;

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $benefits->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'employee' => $item->employee,
                    'type' => $item->typeAccount->name,
                    'start_date' => $item->start_date,
                    'end_date' => $item->end_date,
                    'frequency' => $item->frequency,
                    'frequency_type' => $item->frequency_type,
                    'observation' => $item->observation,
                    'amount' => $item->amount,
                    'id' => $item->id,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('benefits.index', compact('benefits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;

        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        // $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.benefit_type_option'));
        $frequency_type_option = ['' => trans('labels.select_frequency_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_frequency_type_option'));
        // $expenses_option = ['' => trans('labels.select_expenses_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_expenses_option'));

        $type_accounts_options = collect([]);
        $type_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
        $type_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $type_accounts)->pluck('id')->toArray();
        $type_accounts = $accounting_heads->whereIn('parent_id', $type_account_types)->values();

        $type_accounts->each(function($incomes_account) use($type_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $type_accounts_options, 1, true);
        });

        $expense_accounts_options = collect([]);
        $expense_accounts = $accounting_heads->where('code', self::LIABILITIESCODE)->pluck('id')->toArray();
        $expenses_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $expense_accounts)->pluck('id')->toArray();
        $expense_accounts = $accounting_heads->whereIn('parent_id', $expenses_account_types)->values();

        $expense_accounts->each(function($incomes_account) use($expense_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $expense_accounts_options, 1, true);
        });
        return view('benefits.create', compact('employee_options', 'frequency_type_option', 'expense_accounts_options', 'type_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BenefitValidator $benefitValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$benefitValidator->with($input)->passes()) {
                $request->session()->flash('error', $benefitValidator->getErrors()[0]);
                return back()
                    ->withErrors($benefitValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type_account' => $input['type_account'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'observation' => $input['observation'],
                'recurring_type' => $input['recurring_type'],
                'frequency_type' => $input['frequency_type'],
                'frequency' => $input['frequency'],
                'amount' => $input['amount'],
                'expense_account' => $input['expense_account'],
                'user_id' => $user->id
            ];

            $benefits = $user->company->benefits()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.benefit_created'));
            return redirect()->route('benefits.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Benefit $benefit)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;
        
        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        // $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.benefit_type_option'));
        $frequency_type_option = $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_frequency_type_option'));
        // $expenses_option = $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_expenses_option'));
        $type_accounts_options = collect([]);
        $type_accounts = $accounting_heads->where('code', self::EXPENSESCODE)->pluck('id')->toArray();
        $type_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $type_accounts)->pluck('id')->toArray();
        $type_accounts = $accounting_heads->whereIn('parent_id', $type_account_types)->values();

        $type_accounts->each(function($incomes_account) use($type_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $type_accounts_options, 1, true);
        });

        $expense_accounts_options = collect([]);
        $expense_accounts = $accounting_heads->where('code', self::LIABILITIESCODE)->pluck('id')->toArray();
        $expenses_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $expense_accounts)->pluck('id')->toArray();
        $expense_accounts = $accounting_heads->whereIn('parent_id', $expenses_account_types)->values();

        $expense_accounts->each(function($incomes_account) use($expense_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $expense_accounts_options, 1, true);
        });
        return view('benefits.edit', compact('benefit', 'employee_options', 'type_accounts_options', 'frequency_type_option', 'expense_accounts_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Benefit $benefit, BenefitValidator $benefitValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            
            if (!$benefitValidator->with($input)->passes()) {
                $request->session()->flash('error', $benefitValidator->getErrors()[0]);
                return back()
                    ->withErrors($benefitValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type_account' => $input['type_account'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'observation' => $input['observation'],
                'recurring_type' => $input['recurring_type'],
                'frequency_type' => $input['frequency_type'],
                'frequency' => $input['frequency'],
                'amount' => $input['amount'],
                'expense_account' => $input['expense_account'],
            ];

            $benefit->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.benefit_updated'));
            return redirect()->route('benefits.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Benefit $benefit)
    {
        try {
            $benefit->delete();
            $request->session()->flash('success', trans('messages.benefit_deleted'));
            return redirect()->route('benefits.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
