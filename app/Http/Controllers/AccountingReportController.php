<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;
use View;
use Carbon\Carbon;
use App\UserAccountingHead;

class AccountingReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('accounting.reporting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generalReports(Request $request)
    {
        return view('accounting.general-reports');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function journals(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $from_date = $request->input('from_date') ?? null;
        $to_date = $request->input('to_date') ?? null;

        $inventories = $company->inventories()->whereHas('accountingEntries')->orderBy('init_quantity_date', 'desc')->get(['init_quantity_date AS adjustment_date', 'id']);
        $inventories->each(function ($inventory) {
            $inventory->setAppends([]);
            $inventory->resource_url = route('inventories.show', [$inventory]);
            $inventory->resource_title = trans('labels.init_inventory_adjustment');
        });

        // ->map(function($inventory) {
        //     $inventory->resource_url = route('inventories.show', [$inventory]);
        //     $inventory->resource_title = trans('labels.init_inventory_adjustment');
        //     return $inventory;
        // });

        $invoices = $company->invoices()->where('is_void', 0)->whereHas('accountingEntries')->orderBy('start_date', 'desc')->get(['start_date AS adjustment_date', 'id', 'internal_number']);
        $invoices->each(function ($invoice) {
            $invoice->setAppends([]);
            $invoice->unsetRelation('items');
            $invoice->resource_url = route('invoices.show', [$invoice]);
            $invoice->resource_title = trans('labels.invoice')." ".$invoice->internal_number;
        });

        $inventoryAdjustments = $company->inventoryAdjustments()->whereHas('accountingEntries')->orderBy('adjustment_date', 'desc')->get(['adjustment_date', 'id']);

        $inventoryAdjustments->each(function ($inventoryAdjustment) {
            $inventoryAdjustment->setAppends([]);
            $inventoryAdjustment->resource_url = route('inventory-adjustments.show', [$inventoryAdjustment]);
            $inventoryAdjustment->resource_title = trans('labels.inventory_adjustment');
        });

        // ->map(function($inventoryAdjustment) {
        //     $inventoryAdjustment->resource_url = route('inventory-adjustments.show', [$inventoryAdjustment]);
        //     $inventoryAdjustment->resource_title = trans('labels.inventory_adjustment');
        //     return $inventoryAdjustment;
        // });

        $accountAdjustments = $company->accountAdjustments()->orderBy('adjustment_date', 'desc')->get(['adjustment_date', 'id']);

        $accountAdjustments->each(function ($accountAdjustment) {
            $accountAdjustment->setAppends([]);
            $accountAdjustment->resource_url = route('accounting.adjustments.show', [$accountAdjustment]);
            $accountAdjustment->resource_title = trans('labels.accounting_adjustment');
        });

        // ->map(function($accountAdjustment) {
        //     $accountAdjustment->resource_url = route('accounting.adjustments.show', [$accountAdjustment]);
        //     $accountAdjustment->resource_title = trans('labels.accounting_adjustment');
        //     return $accountAdjustment;
        // });

        $transactions = $company->transactions()->whereHas('accountingEntries')->orderBy('start_date', 'desc')->get(['start_date AS adjustment_date', 'id', 'receipt_number', 'voucher_number']);

        $transactions->each(function ($transaction) {
            $transaction->setAppends([]);
            $transaction->resource_url = route('transactions.show', [$transaction]);
            if($transaction->receipt_number) {
                $transaction->resource_title = trans('labels.receipt').' #'.$transaction->receipt_number;
            } else {
                $transaction->resource_title = trans('labels.voucher').' #'.$transaction->voucher_number;
            }
        });

        // ->map(function($transaction) {
        //     $transaction->resource_url = route('transactions.show', [$transaction]);
        //     if($transaction->receipt_number) {
        //         $transaction->resource_title = trans('labels.receipt').' #'.$transaction->receipt_number;
        //     } else {
        //         $transaction->resource_title = trans('labels.voucher').' #'.$transaction->voucher_number;
        //     }
        //     return $transaction;
        // });

        $fixedAssetsJournals = collect([]);
        $fixedAssets = $company->fixedAssets()->whereHas('accountingEntries')->orderBy('date_of_purchase', 'desc')->get(['date_of_purchase AS adjustment_date', 'id']);

        $fixedAssets->each(function ($fixedAsset) use ($fixedAssetsJournals) {
            $fixedAsset->setAppends([]);
            $fixedAsset->resource_url = route('fixed-assets.show', [$fixedAsset]);
            $fixedAsset->resource_title = trans('labels.fixed_asset');
            $fixedAsset->adjustment_date = Carbon::parse($fixedAsset->adjustment_date)->copy()->lastOfMonth()->format('Y-m-d');


            foreach ($fixedAsset->accountingEntries->groupBy('entry_date') as $key => $dateWiseEntry) {
                $temp = collect([]);
                $temp->resource_url = route('fixed-assets.show', [$fixedAsset]);
                $temp->resource_title = trans('labels.fixed_asset');
                $temp->adjustment_date = $key;
                $temp->accountingEntries = $dateWiseEntry;
                $fixedAssetsJournals->push($temp);
            }
        });

        $journals = collect([]);
        $journals = $journals->concat($invoices)->concat($inventoryAdjustments)->concat($accountAdjustments)->concat($transactions)->concat($inventories)->concat($fixedAssetsJournals)->sortByDate('adjustment_date')->values();

        if ($from_date && $to_date) {
            $journals = $journals->filter(function($journal) use ($from_date, $to_date) {
                return $journal->adjustment_date >= $from_date && $journal->adjustment_date <= $to_date;
            })->values();
        }

        $isRawRequest = (bool)($request->input('raw') ?? null);

        return view('accounting.journals', compact('journals', 'isRawRequest'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function balanceSheet(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        $to_date = $request->input('end_date') ?? null;

        $mapIdToAmount = $this->mapAccountHeadToAmount($company, null, $to_date);

        extract($this->computeProfitOrLoss($company, $mapIdToAmount));

        // accounting heads for balance sheet. 
        $assetsAccountingHeads = $company->accountingHeads()->whereIn('code', [self::ASSETSCODE])->get();
        $libAccountingHeads = $company->accountingHeads()->whereIn('code', [self::LIABILITIESCODE])->get();
        $equityAccountingHeads = $company->accountingHeads()->whereIn('code', [self::EQUITIESCODE])->get();

        $companyAccountingHeads = $assetsAccountingHeads->concat($libAccountingHeads)->concat($equityAccountingHeads)->pluck('id', 'code')->toArray();
        $mapIdToAmount[$companyAccountingHeads[self::EQUITIESCODE]] = ($mapIdToAmount[$companyAccountingHeads[self::EQUITIESCODE]] ?? 0) + $netIncome;

        $totalAssets = $mapIdToAmount[$companyAccountingHeads[self::ASSETSCODE]] ?? 0;
        $totalLib = $mapIdToAmount[$companyAccountingHeads[self::LIABILITIESCODE]] ?? 0;
        $totalEquity = $mapIdToAmount[$companyAccountingHeads[self::EQUITIESCODE]] ?? 0;

        $isRawRequest = (bool)($request->input('raw') ?? null);

        return view('accounting.balance-sheet', compact('assetsAccountingHeads', 'libAccountingHeads', 'equityAccountingHeads', 'mapIdToAmount', 'totalAssets', 'totalLib', 'totalEquity', 'isRawRequest'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function incomeStatement(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        $mapIdToAmount = $this->mapAccountHeadToAmount($company);
        // prePrint($mapIdToAmount);

        extract($this->computeProfitOrLoss($company, $mapIdToAmount));

        $companyAccountingHeads = $company->accountingHeads()->whereIn('code', [self::INCOMEMAINCODE, self::COGSMAINCODE, self::OTHERINCOMECODE, self::EXPENSESMAINCODE, self::EXPENSESCODE])->pluck('id', 'code')->toArray();

        // EXPENSESCODE
        $mapIdToAmount += [
            $companyAccountingHeads[self::INCOMEMAINCODE] => 0,
            $companyAccountingHeads[self::COGSMAINCODE] => 0,
            $companyAccountingHeads[self::OTHERINCOMECODE] => 0,
            $companyAccountingHeads[self::EXPENSESMAINCODE] => 0,
            $companyAccountingHeads[self::EXPENSESCODE] => 0,
        ];

        // accounting heads for income statement. 
        $accountingHeads = $company->accountingHeads()->whereIn('id', array_keys($mapIdToAmount))->get()->pluck('name', 'id')->toArray();
        $grossProfit = $mapIdToAmount[$companyAccountingHeads[self::INCOMEMAINCODE]] - $mapIdToAmount[$companyAccountingHeads[self::COGSMAINCODE]];
        $operatingProfit = $grossProfit - $mapIdToAmount[$companyAccountingHeads[self::EXPENSESCODE]];
        $netIncome = $operatingProfit + $mapIdToAmount[$companyAccountingHeads[self::OTHERINCOMECODE]];

        // accounting heads for income statement. 
        $gpAccountingHeads = $company->accountingHeads()->whereIn('code', [self::INCOMEMAINCODE, self::COGSMAINCODE])->get();
        $opAccountingHeads = $company->accountingHeads()->whereIn('code', [self::EXPENSESMAINCODE, self::OTHEREXPENSECODE])->get();
        $npAccountingHeads = $company->accountingHeads()->whereIn('code', [self::OTHERINCOMECODE])->get();

        $isRawRequest = (bool)($request->input('raw') ?? null);

        return view('accounting.income-statement', compact('gpAccountingHeads', 'opAccountingHeads', 'npAccountingHeads', 'mapIdToAmount', 'grossProfit', 'operatingProfit', 'netIncome', 'isRawRequest'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generalLedgerReport(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        $mapIdToAmount = $this->mapAccountHeadToAmount($company);

        extract($this->computeProfitOrLoss($company, $mapIdToAmount));

        // accounting heads for balance sheet. 
        $companyAccountingHeads = $company->accountingHeads()->whereIn('code', [self::EQUITIESCODE])->pluck('id', 'code')->toArray();
        $allAccountingHeads = $company->accountingHeads()->whereIn('code', [self::ASSETSCODE, self::LIABILITIESCODE, self::EQUITIESCODE, self::INCOMECODE, self::EXPENSESCODE])->get();

        $mapIdToAmount[$companyAccountingHeads[self::EQUITIESCODE]] = ($mapIdToAmount[$companyAccountingHeads[self::EQUITIESCODE]] ?? 0) + $netIncome;

        // prePrint($mapIdToAmount);
        $isRawRequest = (bool)($request->input('raw') ?? null);

        return view('accounting.general-ledger-report', compact('allAccountingHeads', 'mapIdToAmount', 'isRawRequest'));
    }

    private function mapAccountHeadToAmount($company, $from_date = null, $to_date = null) {
        $accountingHeadsHasEntriesOrOb = $company->accountingHeads()->where(function($q) {
            return $q->orWhereHas('accountingEntries')
                    ->orWhereNotNull('opening_balance');
        })->get();

        $mapIdToOb = $accountingHeadsHasEntriesOrOb->pluck('opening_balance', 'id')->map(function($ob) {
            return $ob ?: 0.00;
        })->toArray();

        $mapIdToAmount = [];

        // echo "<pre>";
        foreach ($accountingHeadsHasEntriesOrOb as $accountingHead) {
            // check if the accounting head has entries.

            if ($accountingHead->accountingEntries->isNotEmpty()) {
                // Based on start and end date get the accounting entries for a particular accounting head.
                $from_date = $from_date ?: "1970-01-01";
                $to_date = $to_date ?: getLastDayofMonth();

                $debitCreditGroupedAmount = $accountingHead->accountingEntries->whereBetween('entry_date', [$from_date, $to_date])->groupBy('nature')->map(function ($row) {
                        return $row->sum('amount');
                });
                // print_r($debitCreditGroupedAmount);
                $debitCreditAmount = collect(['debit' => 0, 'credit' => 0])->merge($debitCreditGroupedAmount);

                // check if the current accounting head is already visited.
                if (array_key_exists($accountingHead->id, $mapIdToAmount)) {
                    $mapIdToAmount[$accountingHead->id] += ($debitCreditAmount[$accountingHead->incr_nature] - $debitCreditAmount[$accountingHead->decr_nature]);
                } else {
                    $mapIdToAmount[$accountingHead->id] = ($debitCreditAmount[$accountingHead->incr_nature] - $debitCreditAmount[$accountingHead->decr_nature]);
                    // $mapIdToAmount[$accountingHead->id] = ($mapIdToOb[$accountingHead->id] ?? $accountingHead->opening_balance) + ($debitCreditAmount[$accountingHead->incr_nature] - $debitCreditAmount[$accountingHead->decr_nature]);
                }
            }
        }
        // die;

        $mapIdToAmount = arrSumBasedOnSameKey($mapIdToOb, $mapIdToAmount);

        foreach ($accountingHeadsHasEntriesOrOb as $accountingHead) {
            $ancestorsOfAccountHead = $accountingHead->ancestors()->reversed()->get();

            // get all ancestors of the current accounting in reversed order.
            if ($ancestorsOfAccountHead->isNotEmpty()) {
                
                foreach ($ancestorsOfAccountHead as $key => $ancestor) {
                    $prevBalance = $mapIdToAmount[$accountingHead->id] ?? 0;
                    if (array_key_exists($ancestor->id, $mapIdToAmount)) {
                        $mapIdToAmount[$ancestor->id] += $prevBalance;
                    } else {
                        $mapIdToAmount[$ancestor->id] = $prevBalance;
                    }
                }
            }
        }

        return $mapIdToAmount;
    }

    private function computeProfitOrLoss($company, $mapIdToAmount) {

        $companyAccountingHeads = $company->accountingHeads()->whereIn('code', [self::INCOMEMAINCODE, self::COGSMAINCODE, self::OTHERINCOMECODE, self::EXPENSESMAINCODE, self::PANDLCODE, self::EXPENSESCODE])->pluck('id', 'code')->toArray();

        $mapIdToAmount += [
            $companyAccountingHeads[self::INCOMEMAINCODE] => 0,
            $companyAccountingHeads[self::COGSMAINCODE] => 0,
            $companyAccountingHeads[self::OTHERINCOMECODE] => 0,
            $companyAccountingHeads[self::EXPENSESMAINCODE] => 0,
            $companyAccountingHeads[self::PANDLCODE] => 0,
            $companyAccountingHeads[self::EXPENSESCODE] => 0,
        ];

        // accounting heads for income statement. 
        $grossProfit = $mapIdToAmount[$companyAccountingHeads[self::INCOMEMAINCODE]] - $mapIdToAmount[$companyAccountingHeads[self::COGSMAINCODE]];
        $operatingProfit = $grossProfit - $mapIdToAmount[$companyAccountingHeads[self::EXPENSESCODE]];
        $netIncome = $operatingProfit + $mapIdToAmount[$companyAccountingHeads[self::OTHERINCOMECODE]];

        $mapIdToAmount[$companyAccountingHeads[self::PANDLCODE]] = $netIncome;

        return [
            'grossProfit' => $grossProfit,
            'operatingProfit' => $operatingProfit,
            'netIncome' => $netIncome,
            'mapIdToAmount' => $mapIdToAmount
        ];
    }
}