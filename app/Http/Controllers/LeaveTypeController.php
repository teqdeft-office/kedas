<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\LeaveType;
use App\Validators\LeaveTypeValidator;

use DB;

class LeaveTypeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(LeaveType::class, 'leave_type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $leaveTypes = $user->company->leaveTypes;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $leaveTypes->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'custom_name' => $item->custom_name,
                    'number_of_days' => $item->number_of_days,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leave-types.index', compact('leaveTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leave-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, LeaveTypeValidator $leaveTypeValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$leaveTypeValidator->with($input)->passes()) {
                $request->session()->flash('error', $leaveTypeValidator->getErrors()[0]);
                return back()
                    ->withErrors($leaveTypeValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();
            $leaveType = new leaveType(['name' => $input['name'], 'user_id' => $user->id, 'number_of_days' => $input['number_of_days']]);
            $leaveType->setTranslation('custom_name', 'en', $input['name'])->setTranslation('custom_name', 'es', $input['name']);
            $user->company->leaveTypes()->save($leaveType);

            DB::commit();

            $request->session()->flash('success', trans('messages.leave_type_created'));
            return redirect()->route('leave-types.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveType $leaveType)
    {
        return view('leave-types.edit', compact('leaveType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveType $leaveType)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $locale = app()->getLocale();
            $leaveTypeValidator = new LeaveTypeValidator('update', $leaveType);
            
            if (!$leaveTypeValidator->with($input)->passes()) {
                $request->session()->flash('error', $leaveTypeValidator->getErrors()[0]);
                return back()
                    ->withErrors($leaveTypeValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $leaveType->setTranslation('custom_name', $locale, $input['name']);
            $leaveType->number_of_days = $input['number_of_days'];
            $leaveType->save();

            DB::commit();

            $request->session()->flash('success', trans('messages.leave_type_updated'));
            return redirect()->route('leave-types.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LeaveType $leaveType)
    {
        try {
            $leaveType->delete();
            $request->session()->flash('success', trans('messages.leave_type_deleted'));
            return redirect()->route('leave-types.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
