<?php

namespace App\Http\Middleware;

use Closure;

class EscapeResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        /*if ($response->headers->get('content-type') == 'application/json') {
            $collection = $response->original;
            $collection->map(function($data) {
                echo "<pre>";
                print_r($data);
                die;

            });
            array_walk_recursive($collection, function(&$data) {
                $data = is_string($data) && $data !== '' ? htmlentities($data) :  $data;
            });
            $response->setContent($collection);
        }*/

        return $response;
        // htmlentities 
    }
}
