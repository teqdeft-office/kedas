<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Auth;

class RedirectForInvoices
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $invoice_type = $request->route('type') ?? 'sale';

        if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner()
            || (($invoice_type == 'sale' || $invoice_type == 'recurring') && Auth::user()->can('manage sales'))
            || ($invoice_type == 'supplier' && Auth::user()->can('manage expenses'))
        ) {
            return $next($request);
        } else {
            return redirect(RouteServiceProvider::DASHBOARD);
        }

    }
}
