<?php

namespace App\Imports;

use App\Inventory;
use App\Validators\InventoryValidator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Auth;

class InventoriesImport implements ToModel, WithValidation, WithHeadingRow, WithMapping, WithChunkReading
{
    use Importable;

    /**
     * Rules for client import
     *
     * @var array
     */
    protected $rules;

    public function __construct()
    {}

    /*public function batchSize(): int
    {
        return 1000;
    }*/

    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $row = array_map('trim', $row);
        $row = array_map(function($field) {
            return !empty($field) ? $field : null;
        }, $row);

        $manipulateKeys = ['tax', 'type_of_item', 'item_category', 'account'];
        $row = collect($row)->map(function($field, $key) use ($manipulateKeys) {
            return in_array($key, $manipulateKeys) ? strtolower($field) : $field;
        }, $row)->toArray();

        $spaceToUnderKey = ['type_of_item', 'account'];
        $row = collect($row)->map(function($field, $key) use ($spaceToUnderKey) {
            return in_array($key, $spaceToUnderKey) ? str_replace(' ', '_', $field) : $field;
        }, $row)->toArray();


        $row['item_category_id'] = (isset($row['item_category']) && $row['item_category']) ? Auth::user()->company->itemCategories->pluck('name', 'id')->search(function ($item) use($row) {return strtolower($item) == $row['item_category']; }) : null;
        $row['tax_id'] = (isset($row['tax']) && $row['tax']) ? (in_array($row['tax'], ['none', '0', '0%']) ? null : Auth::user()->company->taxes->pluck('name', 'id')->search(function ($item) use($row) {return strtolower($item) == $row['tax']; })) : null;

        return $row;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $inventory = new Inventory([
            'user_id'  => Auth::id(),
            'company_id'  => Auth::user()->company->id,
            'name'  => $row['name'],
            'reference' => $row['reference'],
            'barcode' => $row['barcode'],
            'description' => $row['description'],
            'sale_price' => $row['sale_price'],
            'type_of_item' => $row['type_of_item'],
            'account' => $row['account'],
            'item_category_id' => $row['item_category_id'],
            'tax_id' => (int)$row['tax_id'] ? $row['tax_id'] : null
        ]);
        $inventory->save();

        return $inventory; 
    }

    public function rules(): array
    {
        $inventoryValidator = new InventoryValidator;
        return $inventoryValidator->getRules();
    }
}
