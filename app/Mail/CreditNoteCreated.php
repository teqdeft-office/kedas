<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\CreditNote;

class CreditNoteCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The credit note instance.
     *
     * @var CreditNote
     */
    public $creditNote;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CreditNote $creditNote)
    {
        $this->creditNote = $creditNote;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('labels.credit_note_received').' | '.config('app.name', 'Laravel'))
                    ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                    ->view('emails.credit-notes.created')
                    ->text('emails.credit-notes.created_plain');
    }
}
