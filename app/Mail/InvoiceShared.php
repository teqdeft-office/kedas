<?php

namespace App\Mail;

use App\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceShared extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @var Invoice
     */
    public $invoice;

    /**
     * The pdf as attachement.
     *
     * @var stream $pdf
     */
    protected $pdf;

    /**
     * Create a new message instance.
     *
     * @param  \App\Invoice  $invoice
     * @return void
     */
    public function __construct(Invoice $invoice, $pdf)
    {
        $this->invoice = $invoice;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $filename = 'invoice-'.$this->invoice->internal_number.'.pdf';
        return $this->subject(trans('labels.invoice').' #'.$this->invoice->internal_number)
                    ->from(env('MAIL_FROM_ADDRESS'), $this->invoice->userProfile->name)
                    ->view('emails.invoices.shared')
                    ->text('emails.invoices.shared_plain')
                    ->attachData($this->pdf, $filename, [
                        'mime' => 'application/pdf',
                    ]);
    }
}
