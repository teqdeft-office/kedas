<?php

namespace App\Mail;

use App\CreditNote;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreditNoteShared extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The creditNote instance.
     *
     * @var Invoice
     */
    public $creditNote;

    /**
     * The pdf as attachement.
     *
     * @var stream $pdf
     */
    protected $pdf;

    /**
     * Create a new message instance.
     *
     * @param  \App\Invoice  $creditNote
     * @return void
     */
    public function __construct(CreditNote $creditNote, $pdf)
    {
        $this->creditNote = $creditNote;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $filename = 'credit-note-'.$this->creditNote->id.'.pdf';
        return $this->subject(trans('labels.credit_note').' #'.$this->creditNote->id)
                    ->from(env('MAIL_FROM_ADDRESS'), $this->creditNote->company->name)
                    ->view('emails.credit-notes.shared')
                    ->text('emails.credit-notes.shared_plain')
                    ->attachData($this->pdf, $filename, [
                        'mime' => 'application/pdf',
                    ]);
    }
}
