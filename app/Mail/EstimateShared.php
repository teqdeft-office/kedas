<?php

namespace App\Mail;

use App\Estimate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EstimateShared extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The estimate instance.
     *
     * @var Invoice
     */
    public $estimate;

    /**
     * The pdf as attachement.
     *
     * @var stream $pdf
     */
    protected $pdf;

    /**
     * Create a new message instance.
     *
     * @param  \App\Invoice  $estimate
     * @return void
     */
    public function __construct(Estimate $estimate, $pdf)
    {
        $this->estimate = $estimate;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $filename = 'estimate-'.$this->estimate->internal_number.'.pdf';
        return $this->subject(trans('labels.estimate').' #'.$this->estimate->internal_number)
                    ->from(env('MAIL_FROM_ADDRESS'), $this->estimate->company->name)
                    ->view('emails.estimates.shared')
                    ->text('emails.estimates.shared_plain')
                    ->attachData($this->pdf, $filename, [
                        'mime' => 'application/pdf',
                    ]);
    }
}
