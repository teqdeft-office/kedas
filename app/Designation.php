<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use App\History;

class Designation extends Model
{
    use SoftDeletes;
    use HasTranslations;
    public $translatable = ['custom_name'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'designations';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($designation) {
            $createdata = History::createdata();
            $designation->histories()->create($createdata);
        });
        static::updated(function($designation){
            $updatedata = History::updatedata();
            $designation->histories()->create($updatedata);
        });
        static::deleted(function($designation) {
            $deletedata = History::deletedata();
            $designation->histories()->create($deletedata);
            $designation->employees()->each(function($employee) {
                $employee->designation()->dissociate();
                $employee->save();
            });
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }


    /**
     * Get the employees associated with the designation.
     */
    public function employees()
    {
        return $this->hasMany('App\Employee');
    }
}
