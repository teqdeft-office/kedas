<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use App\History;

class InsuranceBrand extends Model
{
    use SoftDeletes;
    use HasTranslations;
    public $translatable = ['custom_name'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'insurance_brands';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
   
    protected static function booted()
    {
        static::created(function ($insuranceBrand) {
            $createdata = History::createdata();
            $insuranceBrand->histories()->create($createdata);
        });
        static::updated(function($insuranceBrand){
            $updatedata = History::updatedata();
            $insuranceBrand->histories()->create($updatedata);
        });
        static::deleted(function($insuranceBrand) {
            $deletedata = History::deletedata();
            $insuranceBrand->histories()->create($deletedata);
            $insuranceBrand->employees()->each(function($employee) {
                $employee->insuranceBrand()->dissociate();
                $employee->save();
            });
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Get the employees associated with the insuranceBrand.
     */
    public function employees()
    {
        return $this->hasMany('App\Employee');
    }
}
