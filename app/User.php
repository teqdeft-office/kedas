<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App;
use App\Role;
use App\Interfaces\CommonConstants;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Storage;

class User extends Authenticatable implements MustVerifyEmail, CommonConstants
{
    use Notifiable, HasRoles, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'google_id', 'facebook_id', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($user) {
            if (App::Environment('local')) {
                $user->company->address->forcedelete();

                $user->creditNotes()->each(function($creditNote) {
                    $creditNote->forcedelete();
                });
                $user->estimates()->each(function($estimate) {
                    $estimate->forcedelete();
                });
                $user->debitNotes()->each(function($debitNote) {
                    $debitNote->forcedelete();
                });
                $user->purchaseOrders()->each(function($purchaseOrder) {
                    $purchaseOrder->forcedelete();
                });
                $user->inventoryAdjustments()->each(function($inventoryAdjustment) {
                    $inventoryAdjustment->forcedelete();
                });
                $user->invoices()->each(function($invoice) {
                    $invoice->forcedelete();
                });
                $user->transactions()->each(function($transaction) {
                    $transaction->forcedelete();
                });
                $user->ncfs()->each(function($ncf) {
                    $ncf->forcedelete();
                });
                $user->clients()->each(function($client) {
                    $client->forcedelete();
                });
                $user->suppliers()->each(function($supplier) {
                    $supplier->forcedelete();
                });
                $user->taxes()->each(function($tax) {
                    $tax->forcedelete();
                });
                $user->itemCategories()->each(function($itemCategory) {
                    $itemCategory->forcedelete();
                });
                $user->inventories()->each(function($inventory) {
                    $inventory->forcedelete();
                });
                $user->company->forcedelete();
                $user->remissions()->each(function($estimate) {
                    $remission->forcedelete();
                });
            }
        });
    }

    /**
     * Set the user's email.
     *
     * @param  string  $value
     * @return void
     */
    public function setEmailAttribute($value) {
        $this->attributes['email'] = strtolower($value);
    }

    /**
     * Get the user profile information associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    /**
     * Get the user company information associated with the user.
     */
    public function company()
    {
        return $this->isCompanyOwner() ? $this->hasOne('App\Company') : $this->belongsTo('App\Company');
    }

    /**
     * Get the user company information associated with the user.
     */
    public function isCompanyOwner()
    {
        return $this->attributes['company_id'] && $this->attributes['user_role'] == self::ADMIN;
    }

    /**
     * Get all of the user's addresses.
     */
    public function addresses()
    {
        return $this->morphMany('App\Address', 'addressadder');
    }

    public function address() {
        return $this->addresses->where('type', 'general')->first();
    }

    /**
     * Get all of the user's clients.
     */
    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    /**
     * Get all of the user's suppliers.
     */
    public function suppliers()
    {
        return $this->hasMany('App\Supplier');
    }

    /**
     * Get all of the user's taxes.
     */
    public function taxes()
    {
        return $this->hasMany('App\Tax');
    }

    /**
     * Get all of the user's item categories.
     */
    public function itemCategories()
    {
        return $this->hasMany('App\ItemCategory');
    }

    /**
     * Get all of the user's inventories.
     */
    public function inventories()
    {
        return $this->hasMany('App\Inventory');
    }

    /**
     * Get all of the user's invoices.
     */
    public function invoices()
    {
        return $this->hasMany('App\Invoice')->with(['contact', 'items']);
    }

    /**
     * Get all of the user's transactions.
     */
    public function transactions()
    {
        return $this->hasMany('App\Transaction')->with(['contact']);
    }

    /**
     * Get all of the user's credit notes.
     */
    public function creditNotes()
    {
        return $this->hasMany('App\CreditNote')->with(['contact']);
    }

    /**
     * Get all of the user's estimates.
     */
    public function estimates()
    {
        return $this->hasMany('App\Estimate')->with(['contact']);
    }

    public function getLocale() {
        return $this->language ?: App::getLocale();
    }

    /**
     * Get all of the user's debit notes.
     */
    public function debitNotes()
    {
        return $this->hasMany('App\DebitNote')->with(['contact']);
    }

    /**
     * Get all of the user's Purchase Orders.
     */
    public function purchaseOrders()
    {
        return $this->hasMany('App\PurchaseOrder')->with(['contact']);
    }

    /**
     * Get all of the user's Ncfs.
     */
    public function ncfs()
    {
        return $this->hasMany('App\Ncf');
    }

    /**
     * Get all of the user's inventory adjustments.
     */
    public function inventoryAdjustments()
    {
        return $this->hasMany('App\InventoryAdjustment');
    }
    
    /*
     * Get the role of the user.
     */
    public function role()
    {
        if ($this->roles->isNotEmpty()) {
            $role = $this->roles->first()->replicate()->toArray();
            $role['custom_name'] = (array) json_decode($role['custom_name']);
            $role['id'] = $this->roles->first()->id;
            return new Role($role);
        }
        return null;
    }

    /**
     * Get the user company information associated with the user.
     */
    public function isSuperAdmin()
    {
        return $this->attributes['user_role'] == self::SUPERADMIN;
    }

    /**
    * Get the stores of the user.
    */
    public function stores()
    {
        return $this->belongsToMany('App\Company', 'store_user')->withPivot('priority', 'primary');
    }

    /**
     * Get all of the user's currency exchange rates.
     */
    public function currencyRates()
    {
        return $this->hasMany('App\UserCurrencyRate');
    }

    /**
     * Get all of the user's currency exchange rates.
     */
    public function accountingHeads()
    {
        return $this->hasMany('App\UserAccountingHead');
    }

    /**
    * Get primary store of the user.
    */
    public function primaryStore()
    {
        return $this->attributes['user_role'] == self::APP_USER ? $this->stores()->wherePivot('primary', '1')->first() : null;
    }

    /**
    * Get primary store of the user.
    */
    public function getClientInfo()
    {
        return $this->primaryStore() ? $this->hasOne('App\Client', 'email', 'email')->where('company_id', $this->primaryStore()->id)->first() : null;
    }

    /**
     * Get all of the user's estimates.
     */
    public function remissions()
    {
        return $this->hasMany('App\Remission')->with(['contact']);
    }

    /**
     * Get all of the user's leave applications.
     */
    public function leaveApplications()
    {
        return $this->hasMany('App\LeaveApplication');
    }

    /**
     * Get the user's image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return $this->attributes['image'] ? Storage::url('profile-pics/'.$this->attributes['image']) : null;
    }

    /**
     * Get user instance of the employee.
     */
    public function employee()
    {
        return $this->hasOne('App\Employee', 'related_user_id');
    }

}
