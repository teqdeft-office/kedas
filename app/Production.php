<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;

class Production extends Model
{
    use SoftDeletes;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = [
        'status',
        'invoice_id',
        'company_id',
        'user_id',
        'status'
    ];


    /**
     * Get the production order associated with the invoice.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    protected static function booted()
    {
        static::created(function ($productionOrder) {
            $createdata = History::createdata();
            $productionOrder->histories()->create($createdata);
        });
        static::updated(function($productionOrder){
            $updatedata = History::updatedata();
            $productionOrder->histories()->create($updatedata);
        });
        static::deleted(function($productionOrder) {
            $deletedata = History::deletedata();
            $productionOrder->histories()->create($deletedata);
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
}
