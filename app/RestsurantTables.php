<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;
class RestsurantTables extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_tables';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = ['formatted_name'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */


    protected static function booted()
    {
        static::created(function ($tables) {
            $createdata = History::createdata();
            $tables->histories()->create($createdata);
        });
        static::updated(function($tables){
            $updatedata = History::updatedata();
            $tables->histories()->create($updatedata);
        });
        static::deleted(function($tables) {
            $deletedata = History::deletedata();
            $tables->histories()->create($deletedata);
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
    // public function histories()
    // {
    //     return $this->morphMany('App\History', 'record');
    // }
    // protected static function booted()
    // {
    //     static::deleted(function($tax) {
    //         $tax->inventories()->each(function($inventory) {
    //             $inventory->tax()->dissociate();
    //             $inventory->save();
    //         });
    //     });
    // }

    /**
     * The inventory that belong to the tax.
     */
    // public function inventories()
    // {
    //     return $this->hasMany('App\Inventory');
    // }

    // public function getFormattedNameAttribute() {
    //     return $this->attributes['name'].' ('.$this->attributes['percentage'].'%)';
    // }

    /**
     * Get the items associated with the tax.
     */
    // public function items()
    // {
    //     return $this->hasMany('App\Item');
    // }

    /**
     * Get the concept items associated with the tax.
     */
    // public function conceptItems()
    // {
    //     return $this->hasMany('App\ConceptItem');
    // }

    /**
     * Get the company associated with the tax.
     */
    // public function company()
    // {
    //     return $this->belongsTo('App\Company');
    // }
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
