<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class UserProfile extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the user's profile image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return $this->attributes['image'] ? Storage::url('profile-pics/'.$this->attributes['image']) : null;
    }

    /**
     * Get the company's logo.
     *
     * @return string
     */
    public function getLogoAttribute(){
        return $this->attributes['logo'] ? Storage::url('profile-pics/'.$this->attributes['logo']) : null;
    }

    /**
     * Get the user's signature image.
     *
     * @return string
     */
    public function getSignatureAttribute(){
        return $this->attributes['signature'] ? Storage::url('signatures/'.$this->attributes['signature']) : null;
    }

    /**
     * Get the user associated with the user profile.
     */
    public function user()
    {
        return $this->hasOne('App\User', 'user_id');
    }

    /**
     * Get the currency associated with the user profile.
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency', 'currency_id');
    }
}
