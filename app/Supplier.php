<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;

class Supplier extends Model
{
	use SoftDeletes;
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['contact_custom_id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($client) {
            $createdata = History::createdata();
            $client->histories()->create($createdata);
        });
        static::updated(function($client){
            $updatedata = History::updatedata();
            $client->histories()->create($updatedata);
        });
        static::deleted(function($client) {
            $deletedata = History::deletedata();
            $client->histories()->create($deletedata);
            if ($client->address) {
                $client->address()->delete();
            }
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Set the supplier's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    /**
     * Set the supplier's email.
     *
     * @param  string  $value
     * @return void
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    /**
     * Get all of the supplier's addresses.
     */
    public function address()
    {
        return $this->morphOne('App\Address', 'addressadder');
    }

    /**
     * Get the information of supplier's identification_type.
     */
    public function identificationInfo()
    {
        return $this->belongsTo('App\IdentificationType', 'identification_type_id');
    }

    /**
     * Get the information of supplier's sector.
     */
    public function sector()
    {
        return $this->belongsTo('App\Sector', 'sector_id');
    }

    /**
     * Get the supplier's invoices.
     */
    public function invoices()
    {
        return $this->morphMany('App\Invoice', 'contact');
    }

    /**
     * Get the supplier's transactions.
     */
    public function transactions()
    {
        return $this->morphMany('App\Transaction', 'contact');
    }

    /**
     * Get the supplier's creditNotes.
     */
    public function creditNotes()
    {
        return $this->morphMany('App\CreditNote', 'contact');
    }

    /**
     * Get the supplier's estimates.
     */
    public function estimates()
    {
        return $this->morphMany('App\Estimate', 'contact');
    }
    
    /**
     * Get the supplier's Debit Notes.
     */
    public function debitNotes()
    {
        return $this->morphMany('App\DebitNote', 'contact');
    }

    /**
     * Get the supplier's Purchase Orders.
     */
    public function purchaseOrders()
    {
        return $this->morphMany('App\PurchaseOrder', 'contact');
    }

    /**
     * Get the supplier's remissions.
     */
    public function remissions()
    {
        return $this->morphMany('App\Remission', 'contact');
    }

    public function getContactCustomIdAttribute() {
        return 'supplier-'.$this->attributes['id'];
    }

    /**
     * Get the membership associated with the supplier.
     */
    public function membership()
    {
        return $this->belongsTo('App\Membership');
    }
}
