<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;
class Tax extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'taxes';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['formatted_name'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($tax) {
            $createdata = History::createdata();
            $tax->histories()->create($createdata);
        });
        static::updated(function($tax){
            $updatedata = History::updatedata();
            $tax->histories()->create($updatedata);
        });
        static::deleted(function($tax) {
            $deletedata = History::deletedata();
            $tax->histories()->create($deletedata);
            $tax->inventories()->each(function($inventory) {
                $inventory->tax()->dissociate();
                $inventory->save();
            });
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * The inventory that belong to the tax.
     */
    public function inventories()
    {
        return $this->hasMany('App\Inventory');
    }

    public function getFormattedNameAttribute() {
        return $this->attributes['name'].' ('.$this->attributes['percentage'].'%)';
    }

    /**
     * Get the items associated with the tax.
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }

    /**
     * Get the concept items associated with the tax.
     */
    public function conceptItems()
    {
        return $this->hasMany('App\ConceptItem');
    }

    /**
     * Get the company associated with the tax.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
