<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;
use App\Interfaces\CommonConstants;

class ConceptItem extends Model implements CommonConstants
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'concept_items';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'custom_id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($conceptItem) {
            $createdata = History::createdata();
            $conceptItem->histories()->create($createdata);
        });
        static::updated(function($conceptItem){
            $updatedata = History::updatedata();
            $conceptItem->histories()->create($updatedata);
        });
        static::deleted(function($conceptItem) {
            $deletedata = History::deletedata();
            $conceptItem->histories()->create($deletedata);
            if (App::Environment('local')) {
                $conceptItem->forcedelete();
            } else {
                $conceptItem->delete();
            }
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Get the concept associated with the concept item.
     */
    public function concept()
    {
        return $this->belongsTo('App\Concept', 'concept_id');
    }

    /**
     * Get the concept associated with the concept item.
     */
    public function userAccountingHead()
    {
        return $this->belongsTo('App\UserAccountingHead', 'user_accounting_head_id');
    }

    /**
     * Get the tax associated with the concept item.
     */
    public function tax()
    {
        return $this->belongsTo('App\Tax', 'tax_id');
    }

    public function getCalculationAttribute() {
        $price = $this->attributes['price'];
        $tax = $this->tax ? $this->tax->percentage : 0;
        $discount = $this->attributes['discount'];
        $quantity = $this->attributes['quantity'];
        return getItemCost($price, $quantity, $discount, $this->tax);
    }

    public function getCustomIdAttribute() {
        return self::CONCEPT .'-'. $this->attributes['id'];
    }

    /**
     * Get the invoice associated with the concept item.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    /**
     * Get the transaction associated with the concept item.
     */
    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    /**
     * Get the debit Note associated with the concept item.
     */
    public function debitNote()
    {
        return $this->belongsTo('App\DebitNote');
    }

    /**
     * Get the purchase Order associated with the concept item.
     */
    public function purchaseOrder()
    {
        return $this->belongsTo('App\PurchaseOrder');
    }
}
