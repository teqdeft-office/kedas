<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use App\Interfaces\CommonConstants;

class Concept extends Model implements CommonConstants
{
    use HasTranslations, NodeTrait;

    public $timestamps = false;

    public $translatable = ['title'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['custom_id'];

    /*public function getParentIdName()
	{
	    return 'parent';
	}*/

    public function getCustomIdAttribute() {
        return self::CONCEPT .'-'. $this->attributes['id'];
    }
}
