<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriptions extends Model
{
    use SoftDeletes;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = [
        'user_id',
        'company_id',
        'membership_id',
        'creation_date',
        'expiration_date',

    ];

    public function membership(){
        return $this->belongsTo('App\Memberships');
    }
}