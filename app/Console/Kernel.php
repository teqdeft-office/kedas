<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\GenerateInvoicesFromRecurringInvoice;
use App\Jobs\GeneratePaymentsFromRecurringPayment;
use App\Jobs\GenerateAccountEntriesFromFixedAssets;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'America/Los_Angeles';
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('telescope:prune')->daily();
        $schedule->command('telescope:clear')->tuesdays();
        // $schedule->command('inspire')->hourly();
        // The following command is used to clear the app logs on every tuesday.
        $schedule->command('logs:clear')->tuesdays();
        // The following command is used to clear the cron logs on every month on the 1st at 00:00.
        $schedule->command('logs:clear --file=cron')->monthly();

        // The following command is used to clear the content of tmp folder on every month on the 1st at 00:00.
        $schedule->command('folder:empty')->monthly();

        // The following command is used to generated invoices from the recurring invoices daily at 00:01.
        $filePath = storage_path("/logs/cron.log");

        $schedule->job(new GeneratePaymentsFromRecurringPayment, 'high')
            ->dailyAt('00:05')
            ->before(function () {
                // print_r('Task is about to start...');
            })
            ->after(function () {
                // print_r('Task is complete...');
            })
            ->onSuccess(function () {
                // print_r('The task succeeded...');
            })
            ->onFailure(function () {
                // print_r('The task failed...');
            })
            // ->timezone(env('APP_TIMEZONE', 'America/Los_Angeles'))
            ->environments(['local', 'development', 'production'])
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->appendOutputTo($filePath);
            
        $schedule->job(new GenerateInvoicesFromRecurringInvoice, 'high')
            ->dailyAt('00:05')
            ->before(function () {
                // print_r('Task is about to start...');
            })
            ->after(function () {
                // print_r('Task is complete...');
            })
            ->onSuccess(function () {
                // print_r('The task succeeded...');
            })
            ->onFailure(function () {
                // print_r('The task failed...');
            })
            // ->timezone(env('APP_TIMEZONE', 'America/Los_Angeles'))
            ->environments(['local', 'development', 'production'])
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->appendOutputTo($filePath);
            // ->emailOutputTo('teqdeft@gmail.com')
            // ->emailOutputOnFailure('teqdeft@gmail.com');

        $schedule->job(new GenerateAccountEntriesFromFixedAssets, 'high')
            ->lastDayOfMonth('00:05')
            ->before(function () {
                // print_r('Task is about to start...');
            })
            ->after(function () {
                // print_r('Task is complete...');
            })
            ->onSuccess(function () {
                // print_r('The task succeeded...');
            })
            ->onFailure(function () {
                // print_r('The task failed...');
            })
            // ->timezone(env('APP_TIMEZONE', 'America/Los_Angeles'))
            ->environments(['local', 'development', 'production'])
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->appendOutputTo($filePath);
            // ->emailOutputTo('teqdeft@gmail.com')
            // ->emailOutputOnFailure('teqdeft@gmail.com');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
