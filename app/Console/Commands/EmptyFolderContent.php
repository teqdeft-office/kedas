<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EmptyFolderContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'folder:empty {--folder=tmp} {--fullpath=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will empty the application\'s storage folder content';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $folder = $this->option('folder');
        $fullpath = $this->option('fullpath');
        
        $files = $fullpath == 'true' ? glob($folder.'/*.*') : glob('storage/'.$folder.'/*.*');

        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $this->info($folder.' folder has been cleaned.');
    }
}
