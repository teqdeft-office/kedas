<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;

class Address extends Model
{
	use SoftDeletes;
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        
        static::created(function ($address) {
            $createdata = History::createdata();
            $address->histories()->create($createdata);
        });
        static::updated(function($address){
            $updatedata = History::updatedata();
            $address->histories()->create($updatedata);
        });
        static::deleted(function($address) {
            $deletedata = History::deletedata();
            $address->histories()->create($deletedata);
            if (App::Environment('local')) {
                $address->forcedelete();
            } else {
                $address->delete();
            }
        });
    }

    /**
     * Get the owning addressadder model.
     */
    public function addressadder()
    {
        return $this->morphTo();
    }

    /**
     * Get the associated country information.
     */
    public function countryInfo()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
}
