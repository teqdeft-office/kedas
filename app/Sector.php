<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use App\History;
class Sector extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    public $timestamps = false;

    protected static function booted()
    {
        static::created(function ($sectors) {
            $createdata = History::createdata();
            $sectors->histories()->create($createdata);
        });
        static::updated(function($sectors){
            $updatedata = History::updatedata();
            $sectors->histories()->create($updatedata);
        });
        static::deleted(function($sectors) {
            $deletedata = History::deletedata();
            $sectors->histories()->create($deletedata);
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
}
