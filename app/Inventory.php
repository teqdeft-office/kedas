<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use ProVision\Searchable\Traits\SearchableTrait as SearchableTrait;
use App\Interfaces\CommonConstants;
use App;
use App\ResourceMembership;
use App\History;

class Inventory extends Model implements CommonConstants
{
    use SearchableTrait, SoftDeletes;
    /**
     * @inheritDoc
     */
    protected function getSearchableTitleColumns(): array
    {
        return [
            'name',
            'description',
            'barcode'
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getSearchableContentColumns(): array
    {
        return [
            'description',
            'barcode'
        ];
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventories';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_url', 'calculation', 'is_out_of_stock', 'custom_id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($inventory) {
            $createdata = History::createdata();
            $inventory->histories()->create($createdata);
        });
        static::updated(function($inventory){
            $updatedata = History::updatedata();
            $inventory->histories()->create($updatedata);
        });
        static::deleted(function($inventory) {
            $deletedata = History::deletedata();
            $inventory->histories()->create($deletedata);
            $inventory->unIndexRecord();
            /*if (App::Environment('local')) {
                $inventory->forcedelete();
            } else {
                $inventory->delete();
            }*/
        });
    }

    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    public function getImageUrlAttribute() {
    	return $this->attributes['image'] ? Storage::url('items/'.$this->attributes['image']) : null;
    }

    /**
     * Get the tax information associated with the inventory.
     */
    public function tax() {
        return $this->belongsTo('App\Tax');
    }

    /**
     * Get the tax information associated with the inventory.
     */
    public function itemCategory() {
        return $this->belongsTo('App\ItemCategory');
    }

    /**
     * Get the items associated with the inventory.
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }

    /**
     * Get the user information associated with the inventory.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getCalculationAttribute() {
        $price = $this->sale_price;
        $price = $this->attributes['sale_price'];
        $discount = 0;
        $quantity = 1;
        return getItemCost($price, $quantity, $discount, $this->tax);
    }

    public function getCustomIdAttribute() {
        return self::INVENTORY .'-'. $this->attributes['id'];
    }

    public function getIsOutOfStockAttribute() {
        return $this->attributes['track_inventory'] && !$this->attributes['quantity'];
    }

    /**
     * prevent update quantity but it can create 
     *
     * @param  string  $value
     * @return void
     */
    public function setQuantityAttribute($value)
    {
        if (!$this->track_inventory && $this->quantity == null) {
            return;
        }
        $this->attributes['quantity'] = $value;
    }

    /**
     * Get the accounting entries of the invoice.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }

    public function getDebitCreditAmountAttribute() {
        return $debitCreditAmount = $this->accountingEntries->groupBy('nature')->map(function ($row) {
            return $row->sum('amount');
        });
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function company() {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function assetAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'asset_account');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function incomeAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'income_account');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function expenseAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'expense_account');
    }

    /**
     * Get the inventory's memberships.
     */
    public function memberships()
    {
        return $this->belongsToMany('App\Membership');
    }

    public function subscription()
    {
        return $this->morphMany('App\Subscriptions', 'contact');
    }

    public function getOneSubcription($membership_id) {
        return $this->subscription()->where('membership_id','=', $membership_id)->first();
    }

}
