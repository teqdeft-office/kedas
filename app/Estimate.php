<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Estimate extends Model
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estimates';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'exchange_rate_total'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($estimate) {
            $user = Auth::user();
            $next_number = $user->company->estimates()->withTrashed() ? $user->company->estimates()->withTrashed()->count() + 1 : 1;
            $estimate->internal_number = $next_number;
        });

        static::deleted(function($estimate) {
            $estimate->items()->delete();
        });
    }
    
    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * List of items associated with an estimate.
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with(['tax', 'inventory']);
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the user associated with the estimate.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCalculationAttribute() {

        $pluckedCalculation = $this->items->pluck('calculation');

        $estimateSum = array();
        $taxInfo = array();
        $taxInfoArr = array();
        foreach ($pluckedCalculation as $plucked) {
            foreach ($plucked as $sub_key => $value) {
                if (!is_array($value)) {
                    if( ! array_key_exists($sub_key, $estimateSum)) $estimateSum[$sub_key] = 0;
                    $estimateSum[$sub_key]+=$value;
                } else {
                    array_push($taxInfoArr, $value);
                }
            }
        }
        $taxInfoArr = collect($taxInfoArr)->filter()->groupBy('tax_id')->values()->toArray();

        if ($taxInfoArr) {
            foreach ($taxInfoArr as $taxInfoEle) {
                $temp = [];
                foreach ($taxInfoEle as $taxData) {
                    foreach ($taxData as $sub_key => $value) {
                        if( ! array_key_exists($sub_key, $temp)) $temp[$sub_key] = 0;
                        if ($sub_key === 'tax') {
                            $temp[$sub_key] += $value;
                        } else {
                            $temp[$sub_key] = $value;
                        }
                    }
                }
                array_push($taxInfo, $temp);
            }
        }

        $estimateSum = array_map('addZeros', $estimateSum);

        $estimateSum['taxInfo'] = $taxInfo;
        return $estimateSum;
    }

    public function getExchangeRateTotalAttribute() {
        return $this->currency ? round(($this->getCalculationAttribute()['total'] / $this->attributes['exchange_currency_rate']), 6) : null;
    }

    /**
     * Get the currency with the invoice.
     */
    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'exchange_currency_id');
    }
}
