<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Country extends Model
{
    use HasTranslations;

    protected $table = 'countries';

    public $timestamps = false;
    
    public $translatable = ['name'];

    protected $fillable = [
        'name', 'abv', 'abv3', 'abv3_alt', 'code', 'slug'
    ];

    /**
     * Order by field from translation table instead of local table
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $locale
     * @param string $orderField
     * @param string $orderType
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderByTranslation($query, $locale, $orderField, $orderType = 'ASC')
    {
        $translationTable = $this->getTranslationsTable();
        $table = $this->getTable();

        return $query->join("{$translationTable} as t", "t.{$this->getRelationKey()}", "=", "{$table}.id")
            ->where('locale', $locale)
            ->groupBy("{$table}.id")
            ->select("{$table}.*")
            ->orderBy("t.{$orderField}", $orderType)
            ->with('translations');
    }
}
