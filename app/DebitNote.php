<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\History;

class DebitNote extends Model
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'debit_notes';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['calculation', 'exchange_rate_total'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($debitNote) {
            $user = Auth::user();
            $next_number = $user->company->debitNotes()->withTrashed() ? $user->company->debitNotes()->withTrashed()->count() + 1 : 1;
            $debitNote->internal_number = $next_number;
        });
        static::created(function ($debitNote) {
            $createdata = History::createdata();
            $debitNote->histories()->create($createdata);
        });
        static::updated(function($debitNote){
            $updatedata = History::updatedata();
            $debitNote->histories()->create($updatedata);
        });
        static::deleted(function($debitNote) {
            $deletedata = History::deletedata();
            $debitNote->histories()->create($deletedata);
            $debitNote->conceptItems()->delete();
            $debitNote->items()->delete();
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
    
    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }

    /**
     * List of concepts items associated with a debit note.
     */
    public function conceptItems()
    {
        return $this->hasMany('App\ConceptItem')->with(['tax', 'concept']);
    }

    /**
     * List of items associated with a debit note.
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with(['tax', 'inventory']);
    }

    public function getCalculationAttribute() {

        $pluckedCalculation = $this->conceptItems->pluck('calculation')->merge($this->items->pluck('calculation'));

        $invoiceSum = array();
        $taxInfo = array();
        $taxInfoArr = array();
        foreach ($pluckedCalculation as $plucked) {
            foreach ($plucked as $sub_key => $value) {
                if (!is_array($value)) {
                    if( ! array_key_exists($sub_key, $invoiceSum)) $invoiceSum[$sub_key] = 0;
                    $invoiceSum[$sub_key]+=$value;
                } else {
                    array_push($taxInfoArr, $value);
                }
            }
        }
        $taxInfoArr = collect($taxInfoArr)->filter()->groupBy('tax_id')->values()->toArray();

        if ($taxInfoArr) {
            foreach ($taxInfoArr as $taxInfoEle) {
                $temp = [];
                foreach ($taxInfoEle as $taxData) {
                    foreach ($taxData as $sub_key => $value) {
                        if( ! array_key_exists($sub_key, $temp)) $temp[$sub_key] = 0;
                        if ($sub_key === 'tax') {
                            $temp[$sub_key] += $value;
                        } else {
                            $temp[$sub_key] = $value;
                        }
                    }
                }
                array_push($taxInfo, $temp);
            }
        }

        $usedAmount = 0;
        foreach ($this->invoices as $invoice) {
            $usedAmount += $invoice->pivot->amount;
        }
        $remainingAmount = $invoiceSum['total'] - $usedAmount;

        $invoiceSum['remaining_amount'] = $remainingAmount;
        $invoiceSum['used_amount'] = $usedAmount;

        $invoiceSum = array_map('addZeros', $invoiceSum);

        $invoiceSum['taxInfo'] = $taxInfo;
        return $invoiceSum;
    }

    public function getExchangeRateTotalAttribute() {
        return $this->currency ? round(($this->getCalculationAttribute()['total'] / $this->attributes['exchange_currency_rate']), 6) : null;
    }

    /**
     * Get the transaction documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * Get the currency with the invoice.
     */
    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'exchange_currency_id');
    }

     /**
     * The invoices that belong to the debit note.
     */
    public function invoices()
    {
        return $this->belongsToMany('App\Invoice')->withPivot('amount');
    }

    /**
     * Get the company information associated with the estimate.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

}
