<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\History;

class Client extends Model
{
	use SoftDeletes;
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['contact_custom_id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($client) {
            $createdata = History::createdata();
            $client->histories()->create($createdata);
        });
        static::updated(function($client){
            $updatedata = History::updatedata();
            $client->histories()->create($updatedata);
        });
        static::deleted(function($client) {
            $deletedata = History::deletedata();
            $client->histories()->create($deletedata);
            if ($client->address) {
                $client->address()->delete();
            }
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }

    /**
     * Set the client's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    /**
     * Set the client's email.
     *
     * @param  string  $value
     * @return void
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    /**
     * Get all of the client's addresses.
     */
    public function address()
    {
        return $this->morphOne('App\Address', 'addressadder');
    }

    /**
     * Get the information of client's identification_type.
     */
    public function identificationInfo()
    {
        return $this->belongsTo('App\IdentificationType', 'identification_type_id');
    }

    /**
     * Get the information of client's sector.
     */
    public function sector()
    {
        return $this->belongsTo('App\Sector', 'sector_id');
    }

    /**
     * Get the client's invoices.
     */
    public function invoices()
    {
        return $this->morphMany('App\Invoice', 'contact');
    }

    /**
     * Get the client's transactions.
     */
    public function transactions()
    {
        return $this->morphMany('App\Transaction', 'contact');
    }

    /**
     * Get the client's creditNotes.
     */
    public function creditNotes()
    {
        return $this->morphMany('App\CreditNote', 'contact');
    }

    /**
     * Get the client's estimates.
     */
    public function estimates()
    {
        return $this->morphMany('App\Estimate', 'contact');
    }

    /**
     * Get the client's Debit Notes.
     */
    public function debitNotes()
    {
        return $this->morphMany('App\DebitNote', 'contact');
    }

    /**
     * Get the client's Purchase Orders.
     */
    public function purchaseOrders()
    {
        return $this->morphMany('App\PurchaseOrder', 'contact');
    }

    /**
     * Get the client's remissions.
     */
    public function remissions()
    {
        return $this->morphMany('App\Remission', 'contact');
    }

    public function getContactCustomIdAttribute() {
        return 'client-'.$this->attributes['id'];
    }

    /**
     * Get the membership associated with the client.
     */
    public function membership()
    {
        return $this->belongsTo('App\Membership');
    }

    /**
     * Get the client's points.
     */
    public function points()
    {
        return $this->morphMany('App\ContactPoint', 'contact');
    }
}
