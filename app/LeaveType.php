<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use Storage;
use App\History;
class LeaveType extends Model
{
    use SoftDeletes;
    use HasTranslations;
    public $translatable = ['custom_name'];


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leave_types';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
   /*  protected static function booted()
    {
        static::deleted(function($itemCategory) {
            $itemCategory->inventories()->each(function($inventory) {
                $inventory->itemCategory()->dissociate();
                $inventory->save();
            });
        });
    } */


    protected static function booted()
    {
        static::created(function ($leaveTypes) {
            $createdata = History::createdata();
            $leaveTypes->histories()->create($createdata);
        });
        static::updated(function($leaveTypes){
            $updatedata = History::updatedata();
            $leaveTypes->histories()->create($updatedata);
        });
        static::deleted(function($leaveTypes) {
            $deletedata = History::deletedata();
            $leaveTypes->histories()->create($deletedata);
        });
    }
    public function histories()
    {
        return $this->morphMany('App\History', 'record');
    }
    /**
     * Get all of the leave type's applications.
     */
    public function leaveApplications()
    {
        return $this->hasMany('App\LeaveApplications');
    }
}
